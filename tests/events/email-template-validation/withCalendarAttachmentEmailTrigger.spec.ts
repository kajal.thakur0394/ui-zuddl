import {
  test,
  Page,
  BrowserContext,
  APIRequestContext,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import {
  getEventDetails,
  createNewEvent,
  inviteAttendeeByAPI,
  enableMagicLinkInEvent,
  inviteSpeakerByApi,
} from "../../../util/apiUtil";
import { DataUtl } from "../../../util/dataUtil";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventRole from "../../../enums/eventRoleEnum";
import EventEntryType from "../../../enums/eventEntryEnum";
import { emailTemplateValidation } from "../../../util/emailTemplateValidation";
import { UserRegFormData } from "../../../test-data/userRegForm";
import createEventPayloadData from "../../../util/create_event_payload.json";
import { deleteFile, parseICS } from "../../../util/commonUtil";
import { validateIcsData } from "../../../util/validation-util";
import { getIcsAttachment } from "../../../util/emailUtil";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";
import { SpeakerData } from "../../../test-data/speakerData";

test.describe
  .parallel("Validation of email triggered with Calendar invite @withCalendarAttachmnetEmailTrigger", () => {
  let org_session: Page;
  let org_browser_context: BrowserContext;
  let attendee_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let userDefaultRegFieldData: object;
  let attendee_email_add: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let emailBody: string;
  let emailInvite: emailTemplateValidation;
  let speakerUser: SpeakerData;
  let emailAttachmentFilename: string;

  test.beforeEach(async () => {
    // attendee_email_add = DataUtl.getRandomAttendeeEmail()
    userDefaultRegFieldData = new UserRegFormData(attendee_email_add)
      .userRegFormDataForDefaultFields;
    org_browser_context = await BrowserFactory.getBrowserContext({});
    organiserApiContext = org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    // await updateEventEntryType(organiserApiContext,event_id,landing_page_id,EventEntryType.REG_BASED)
    eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.REG_BASED
    );
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
  });

  test.afterEach(async () => {
    // deleteFile(emailAttachmentFilename);
    await org_browser_context?.close();
  });

  test("TC001: Validating Calendar attachment recieved after inviting attendee", async ({}) => {
    await test.step("Enabling Email notification", async () => {
      await enableDisableEmailTrigger(
        organiserApiContext,
        event_id,
        EventSettingID.EventInvitationFromEmailFromPeopleSection,
        true,
        EventRole.ATTENDEE,
        true
      );
    });
    await test.step("Registering attendee", async () => {
      userInfoDto = new UserInfoDTO(
        DataUtl.getRandomAttendeeEmail(),
        "",
        "Ankur",
        "QA",
        false
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.NOTEXISTS
      );
      await inviteAttendeeByAPI(
        organiserApiContext,
        eventInfoDto.eventId,
        userInfoDto.userEmail
      );
    });

    await test.step("Validating attachment ICS file from email recieved after registering", async () => {
      emailAttachmentFilename = await getIcsAttachment(
        userInfoDto.userEmail,
        eventInfoDto.getInviteEmailSubj()
      );
      console.log("Filename of ICS is:", emailAttachmentFilename);
      const emailAttachmentData = parseICS(emailAttachmentFilename);
      eventInfoDto.startDateTime = createEventPayloadData.startDateTime;
      await validateIcsData(emailAttachmentData, eventInfoDto);
    });
  });

  test("TC002: Validating Calendar attachment recieved after inviting speaker", async ({}) => {
    await test.step("Enabling Email notification", async () => {
      await enableDisableEmailTrigger(
        organiserApiContext,
        event_id,
        EventSettingID.EventInvitationFromEmailFromPeopleSection,
        true,
        EventRole.SPEAKER,
        true
      );
    });
    await test.step("Inviting Speaker", async () => {
      speakerUser = new SpeakerData(
        DataUtl.getRandomAttendeeEmail(),
        "Ankur",
        "Anand"
      );
      console.log(`Speaker data to add is ${speakerUser}`);
      await inviteSpeakerByApi(
        organiserApiContext,
        speakerUser.getSpeakerDataObject(),
        eventInfoDto.eventId,
        true
      ); // true denote to send email
    });

    await test.step("Validating attachment ICS file from email recieved after registering", async () => {
      emailAttachmentFilename = await getIcsAttachment(
        speakerUser["email"],
        eventInfoDto.getInviteEmailSubj()
      );
      console.log("Filename of ICS is:", emailAttachmentFilename);
      const emailAttachmentData = parseICS(emailAttachmentFilename);
      eventInfoDto.startDateTime = createEventPayloadData.startDateTime;
      await validateIcsData(emailAttachmentData, eventInfoDto);
    });
  });
});
