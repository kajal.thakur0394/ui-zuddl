import {
  test,
  BrowserContext,
  APIRequestContext,
  expect,
  APIResponse,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import {
  getEventDetails,
  createNewEvent,
  inviteAttendeeByAPI,
  enableMagicLinkInEvent,
} from "../../../util/apiUtil";
import { DataUtl } from "../../../util/dataUtil";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventRole from "../../../enums/eventRoleEnum";
import EventEntryType from "../../../enums/eventEntryEnum";
import { emailTemplateValidation } from "../../../util/emailTemplateValidation";
import createEventPayloadData from "../../../util/create_event_payload.json";
import {
  validateCalenderLink,
  validateEventStartDate,
  validateEventStartTime,
  validateIcsData,
  validateMagicLink,
} from "../../../util/validation-util";
import { fetchMailasaurEmailObject } from "../../../util/emailUtil";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";
const ical = require("node-ical");

test.describe
  .parallel("Validation of attendee email triggered on registration @attendeeEmailTemplate", () => {
  let org_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let landing_page_id;
  let emailInvite: emailTemplateValidation;

  test.beforeEach(async () => {
    org_browser_context = await BrowserFactory.getBrowserContext({});
    organiserApiContext = org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    // await updateEventEntryType(organiserApiContext,event_id,landing_page_id,EventEntryType.REG_BASED)
    eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.REG_BASED
    );
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;

    //enable invitation emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.ATTENDEE,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
  });

  test.afterEach(async () => {
    await org_browser_context?.close();
  });

  test("TC001: Validating Name of attendee, Event Title, Event Start Date and Time  in Email Invite triggered after registration", async () => {
    await test.step("user registering to the event", async () => {
      userInfoDto = new UserInfoDTO(
        DataUtl.getRandomAttendeeEmail(),
        "",
        "Ankur",
        "Qa",
        false
      );
      await inviteAttendeeByAPI(
        organiserApiContext,
        eventInfoDto.eventId,
        userInfoDto.userEmail,
        true,
        userInfoDto.userFirstName,
        userInfoDto.userLastName
      );
    });

    await test.step("Fetching Email body from email recieved after registering", async () => {
      let fetched_email = await fetchMailasaurEmailObject(
        userInfoDto.userEmail,
        eventInfoDto.getInviteEmailSubj()
      );
      emailInvite = new emailTemplateValidation(fetched_email);
    });

    await test.step("Fetching attendee name from email recieved after registering", async () => {
      let attendeeName = await emailInvite.fetchAttendeeName();
      console.log("Attendee name recieved:", attendeeName);
      let expectedAttendeeName = `${userInfoDto.userFirstName} ${userInfoDto.userLastName}`;
      console.log("expectedAttendeeName: ", expectedAttendeeName);
      expect(
        attendeeName,
        "Expecting attendee Name to macth with provided one"
      ).toContain(expectedAttendeeName);
    });

    await test.step("Fetching Event title from email recieved after registering", async () => {
      let eventTitle = await emailInvite.fetchEventTitle();
      console.log("Event title recieved", eventTitle);
      expect(eventTitle, "Verifying Event title").toContain(
        eventInfoDto.eventTitle
      );
    });
    await test.step("Verifying Event start Date from email recieved after registering", async () => {
      eventInfoDto.startDateTime = createEventPayloadData.startDateTime;
      await validateEventStartDate(emailInvite, eventInfoDto);
    });

    await test.step("Verifying Event start time from email recieved after registering", async () => {
      await validateEventStartTime(emailInvite, eventInfoDto);
    });
  });

  test("TC002: Validating Go to event button in Email Invite triggered after registration", async () => {
    await test.step("user registering to the event", async () => {
      userInfoDto = new UserInfoDTO(
        DataUtl.getRandomAttendeeEmail(),
        "",
        "Ankur",
        "QA",
        false
      );
      await inviteAttendeeByAPI(
        organiserApiContext,
        eventInfoDto.eventId,
        userInfoDto.userEmail
      );
    });
    await test.step("Fetching Email body from email recieved after registering", async () => {
      let fetched_email = await fetchMailasaurEmailObject(
        userInfoDto.userEmail,
        eventInfoDto.getInviteEmailSubj()
      );
      emailInvite = new emailTemplateValidation(fetched_email);
    });
    await test.step("Fetching goto Button link and text from email recieved after registering", async () => {
      let gotoButtonText, GoToButtonLink;
      let gotoButtonContents = await emailInvite.getGoToButtonlink();
      gotoButtonText = gotoButtonContents[0];
      GoToButtonLink = gotoButtonContents[1];
      console.log(gotoButtonText);
      expect(gotoButtonText, "Verifying text of goTo button").toContain(
        "Go to Event"
      );
      await validateMagicLink(GoToButtonLink, eventInfoDto);
    });
  });

  // This fails sometimes as ICS links are not downloadable
  // Bug ID: https://linear.app/zuddl/issue/QAR-1231/ics-files-are-not-getting-downloaded-on-email-triggered-on-attendee
  test("TC003: Validating ICS files in Email Invite triggered after registration", async () => {
    await test.step("user registering to the event", async () => {
      userInfoDto = new UserInfoDTO(
        DataUtl.getRandomAttendeeEmail(),
        "",
        "Ankur",
        "QA",
        false
      );
      await inviteAttendeeByAPI(
        organiserApiContext,
        eventInfoDto.eventId,
        userInfoDto.userEmail
      );
    });
    await test.step("Fetching Email body from email recieved after registering", async () => {
      let fetched_email = await fetchMailasaurEmailObject(
        userInfoDto.userEmail,
        eventInfoDto.getInviteEmailSubj()
      );
      emailInvite = new emailTemplateValidation(fetched_email);
    });
    await test.step("Fetching ICS files link and text from email recieved after registering", async () => {
      let ICScontents = await emailInvite.fetchIcsContents();
      let outlookinvite_link = ICScontents[0];
      let appleinvite_link = ICScontents[2];
      let outlookinvitetext = ICScontents[1];
      let appleinvitetext = ICScontents[3];
      console.log("ICS Contents:", ICScontents);
      expect(outlookinvitetext, "Verifying text of outlook ICS file").toContain(
        "Outlook"
      );
      expect(appleinvitetext, "Verifying text of apple ICS file").toContain(
        "Apple"
      );

      // Use IcsParser to download and parse the ics file contents
      const appleIcsData = await ical.async.fromURL(appleinvite_link);
      console.log("Apple ics data: ", appleIcsData);
      const outlookIcsData = await ical.async.fromURL(outlookinvite_link);
      console.log("Outlook ics Data: ", outlookIcsData);
      eventInfoDto.startDateTime = createEventPayloadData.startDateTime;

      await test.step("Verifying Outlook ICS files link and text from email recieved after registering", async () => {
        await validateIcsData(outlookIcsData, eventInfoDto);
      });

      await test.step("Verifying Apple ICS files link and text from email recieved after registering", async () => {
        await validateIcsData(appleIcsData, eventInfoDto);
      });
    });
  });

  test("TC004: Validating calender invites in Email Invite triggered after registration", async () => {
    await test.step("user registering to the event", async () => {
      userInfoDto = new UserInfoDTO(
        DataUtl.getRandomAttendeeEmail(),
        "",
        "Ankur",
        "QA",
        false
      );
      await inviteAttendeeByAPI(
        organiserApiContext,
        eventInfoDto.eventId,
        userInfoDto.userEmail
      );
    });
    await test.step("Fetching Email body from email recieved after registering", async () => {
      let fetched_email = await fetchMailasaurEmailObject(
        userInfoDto.userEmail,
        eventInfoDto.getInviteEmailSubj()
      );
      emailInvite = new emailTemplateValidation(fetched_email);
    });
    await test.step("Fetching ICS files link and text from email recieved after registering", async () => {
      let ICScontents = await emailInvite.fetchCalenderInvites();
      let googleinvite_link = ICScontents[0];
      let yahooinvite_link = ICScontents[2];
      let googleinvitetext = ICScontents[1];
      let yahooinvitetext = ICScontents[3];
      console.log("ICS Contents:", ICScontents);
      expect(googleinvitetext, "Verifying text of outlook ICS file").toContain(
        "Google"
      );
      expect(yahooinvitetext, "Verifying text of apple ICS file").toContain(
        "Yahoo"
      );

      await test.step("Verifying Outlook ICS files link and text from email recieved after registering", async () => {
        await validateCalenderLink(googleinvite_link, eventInfoDto.eventId);
      });

      await test.step("Verifying Apple ICS files link and text from email recieved after registering", async () => {
        await validateCalenderLink(yahooinvite_link, eventInfoDto.eventId);
      });
    });
  });

  test("TC005: Validating able to add attendee in 1st try", async () => {
    test.info().annotations.push({
      type: "P0",
      description:
        "https://linear.app/zuddl/issue/QAT-109/unable-to-add-attendee-in-1st-try-p0-fix",
    });
    const attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let addAttendeeApiResp: APIResponse;
    await test.step("Adding new attendee by add attendee form API call", async () => {
      addAttendeeApiResp = await inviteAttendeeByAPI(
        organiserApiContext,
        eventInfoDto.eventId,
        attendeeEmail
      );
    });

    await test.step("validate that API response of adding an attendee is succcesfull and gives 200 status", async () => {
      expect(addAttendeeApiResp.status()).toBe(200);
    });
  });
});
