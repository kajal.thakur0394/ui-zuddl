import {
  test,
  BrowserContext,
  APIRequestContext,
  expect,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import {
  getEventDetails,
  inviteSpeakerByApi,
  updateEventDateTime,
  registerUserToEventbyApi,
  getRegistrationId,
  checkInAttendee,
  createNewEvent,
  enableMagicLinkInEvent,
  cancelEvent,
} from "../../../util/apiUtil";
import { DataUtl } from "../../../util/dataUtil";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventRole from "../../../enums/eventRoleEnum";
import EventEntryType from "../../../enums/eventEntryEnum";
import { emailReminderTemplateValidation } from "../../../util/emailTemplateValidation";
import createEventPayloadData from "../../../util/create_event_payload.json";
import {
  validateEmailHeaderOfThankYouForComing,
  validateEmailReminder,
  validateIcsData,
} from "../../../util/validation-util";
import {
  fetchMailasaurEmailObject,
  getIcsAttachment,
} from "../../../util/emailUtil";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";
import { SpeakerData } from "../../../test-data/speakerData";
import EventReminderHeader from "../../../enums/EventReminderHeaderEnum";
import { EventController } from "../../../controller/EventController";
import { getTimezoneList } from "../../../test-data/timezone-testdata";
import { parseICS } from "../../../util/commonUtil";

test.describe
  .parallel("Validation of event reminder emails triggered for attendees  @attendeeReminderScenarios @EmailTriggered", () => {
  test.describe.configure({ retries: 2 });
  let org_browser_context: BrowserContext;
  let event_title: string;
  let event_id: any;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let landing_page_id;
  // let emailInvite: emailReminderTemplateValidation;
  let speakerUser: SpeakerData;
  let eventStartTime: Date;
  let eventEndTime: Date;

  const timezone = getTimezoneList()[1]; // currently Asia/Kolkata, thus IST
  const timezoneCode = "(IST)";

  test.beforeEach(async () => {
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    organiserApiContext = org_browser_context.request;
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title: event_title,
      timezone: timezone,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
    eventInfoDto.startDateTime = createEventPayloadData.startDateTime;
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "Ankur",
      "QA",
      false
    );
    await registerUserToEventbyApi(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    speakerUser = new SpeakerData(
      DataUtl.getRandomAttendeeEmail(),
      "Ankur",
      "Anand"
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      true
    ); // true denote to send email
    let registrationId = await getRegistrationId(
      organiserApiContext,
      event_id,
      userInfoDto.userEmail
    );
    console.log("registrationId:", registrationId);
    // await checkInAttendee(organiserApiContext, event_id, registrationId);
  });

  test.afterEach(async () => {
    await cancelEvent(organiserApiContext, event_id);
    await org_browser_context?.close();
  });

  test("TC001: Validating Reminder ~ 1 Day to Go   @attendee @invited @withICS", async ({}) => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-701",
    });

    await test.step("Updating Start time of event", async () => {
      eventStartTime = new Date();
      eventStartTime.setDate(eventStartTime.getDate() + 1);
      eventStartTime.setSeconds(eventStartTime.getSeconds() + 30);
      console.log("New event Start Date time", eventStartTime.toISOString());
      eventEndTime = new Date();
      eventEndTime.setDate(eventEndTime.getDate() + 1);
      eventEndTime.setHours(eventEndTime.getHours() + 2);
      console.log("New event end Date time", eventEndTime.toISOString());
      await updateEventDateTime(
        organiserApiContext,
        event_id,
        eventInfoDto.landingPageId,
        eventStartTime.toISOString(),
        eventEndTime.toISOString()
      );
      eventInfoDto.startDateTime = eventStartTime.toISOString();
      eventInfoDto.startDateTime = eventStartTime.toISOString();
    });
    // When we are updating time of event all email triggers are reset, so we have to call reminer notification after updating Time.
    await test.step("Enabling Email reminder notification", async () => {
      await enableDisableEmailTrigger(
        organiserApiContext,
        event_id,
        EventSettingID.EmailReminderBefore1Day,
        true,
        EventRole.ATTENDEE,
        true
      );
    });

    let fetched_email;

    await test.step("Validating Email reminder", async () => {
      fetched_email = await fetchMailasaurEmailObject(
        userInfoDto.userEmail,
        `1 Day to Go • ${eventInfoDto.eventTitle}`,
        true,
        100
      );

      let emailInvite = new emailReminderTemplateValidation(fetched_email);
      await validateEmailReminder(
        emailInvite,
        eventInfoDto,
        EventReminderHeader.EmailReminderBefore1Day,
        timezoneCode
      );
    });

    await test.step("Verify ICS Details", async () => {
      let emailAttachmentFilename = await getIcsAttachment(
        userInfoDto.userEmail,
        `1 Day to Go • ${eventInfoDto.eventTitle}`
      );
      console.log("Filename of ICS is:", emailAttachmentFilename);
      const emailAttachmentData = parseICS(emailAttachmentFilename);
      await validateIcsData(emailAttachmentData, eventInfoDto);
    });
  });

  test("TC002: Validating Reminder ~ 1 Hour to Go   @attendee @invited @withICS", async ({}) => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-701",
    });

    await test.step("Updating Start time of event", async () => {
      eventStartTime = new Date();
      eventStartTime.setHours(eventStartTime.getHours() + 1);
      eventStartTime.setSeconds(eventStartTime.getSeconds() + 30);
      console.log("New event Start Date time", eventStartTime.toISOString());
      eventEndTime = new Date();
      eventEndTime.setHours(eventEndTime.getHours() + 2);
      console.log("New event end Date time", eventEndTime.toISOString());
      await updateEventDateTime(
        organiserApiContext,
        event_id,
        eventInfoDto.landingPageId,
        eventStartTime.toISOString(),
        eventEndTime.toISOString()
      );
      eventInfoDto.startDateTime = eventStartTime.toISOString();
      eventInfoDto.startDateTime = eventStartTime.toISOString();
    });
    // When we are updating time of event all email triggers are reset, so we have to call reminer notification after updating Time.
    await test.step("Enabling Email reminder notification", async () => {
      await enableDisableEmailTrigger(
        organiserApiContext,
        event_id,
        EventSettingID.EmailReminderBefore1Hour,
        true,
        EventRole.ATTENDEE,
        true
      );
    });
    await test.step("Validating Email reminder", async () => {
      let fetched_email = await fetchMailasaurEmailObject(
        userInfoDto.userEmail,
        `1 Hour to Go • ${eventInfoDto.eventTitle}`,
        true,
        100
      );
      let emailInvite = new emailReminderTemplateValidation(fetched_email);
      await validateEmailReminder(
        emailInvite,
        eventInfoDto,
        EventReminderHeader.EmailReminderBefore1Hour,
        timezoneCode
      );
    });
    await test.step("Verify ICS Details", async () => {
      let emailAttachmentFilename = await getIcsAttachment(
        userInfoDto.userEmail,
        `1 Hour to Go • ${eventInfoDto.eventTitle}`
      );
      console.log("Filename of ICS is:", emailAttachmentFilename);
      const emailAttachmentData = parseICS(emailAttachmentFilename);
      await validateIcsData(emailAttachmentData, eventInfoDto);
    });
  });

  test("TC003: Validating Reminder ~ 1 Day to Go   @speaker @invited @withICS", async ({}) => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-701",
    });

    await test.step("Inviting Speaker and checking it in", async () => {});
    await test.step("Updating Start time of event", async () => {
      eventStartTime = new Date();
      eventStartTime.setDate(eventStartTime.getDate() + 1);
      eventStartTime.setSeconds(eventStartTime.getSeconds() + 40);
      console.log("New event Start Date time", eventStartTime.toISOString());
      eventEndTime = new Date();
      eventEndTime.setDate(eventEndTime.getDate() + 1);
      eventEndTime.setHours(eventEndTime.getHours() + 2);
      console.log("New event end Date time", eventEndTime.toISOString());
      await updateEventDateTime(
        organiserApiContext,
        event_id,
        eventInfoDto.landingPageId,
        eventStartTime.toISOString(),
        eventEndTime.toISOString()
      );
      eventInfoDto.startDateTime = eventStartTime.toISOString();
    });
    // When we are updating time of event all email triggers are reset, so we have to call reminer notification after updating Time.
    await test.step("Enabling Email reminder notification", async () => {
      await enableDisableEmailTrigger(
        organiserApiContext,
        event_id,
        EventSettingID.EmailReminderBefore1Day,
        true,
        EventRole.SPEAKER,
        true
      );
    });
    await test.step("Validating Email reminder", async () => {
      let fetched_email = await fetchMailasaurEmailObject(
        speakerUser["email"],
        `1 Day to Go • ${eventInfoDto.eventTitle}`,
        true,
        100
      );
      let emailInvite = new emailReminderTemplateValidation(fetched_email);
      await validateEmailReminder(
        emailInvite,
        eventInfoDto,
        EventReminderHeader.EmailReminderBefore1Day,
        timezoneCode
      );
    });

    await test.step("Verify ICS Details", async () => {
      let emailAttachmentFilename = await getIcsAttachment(
        speakerUser["email"],
        `1 Day to Go • ${eventInfoDto.eventTitle}`
      );
      console.log("Filename of ICS is:", emailAttachmentFilename);
      const emailAttachmentData = parseICS(emailAttachmentFilename);
      await validateIcsData(emailAttachmentData, eventInfoDto);
    });
  });

  test("TC004: Validating Reminder ~ 1 Hour to Go   @speaker @invited @withICS", async ({}) => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-701",
    });

    await test.step("Updating Start time of event", async () => {
      eventStartTime = new Date();
      eventStartTime.setHours(eventStartTime.getHours() + 1);
      eventStartTime.setSeconds(eventStartTime.getSeconds() + 50);
      console.log("New event Start Date time", eventStartTime.toISOString());
      eventEndTime = new Date();
      eventEndTime.setHours(eventEndTime.getHours() + 2);
      console.log("New event end Date time", eventEndTime.toISOString());
      await updateEventDateTime(
        organiserApiContext,
        event_id,
        eventInfoDto.landingPageId,
        eventStartTime.toISOString(),
        eventEndTime.toISOString()
      );
      eventInfoDto.startDateTime = eventStartTime.toISOString();
    });
    // When we are updating time of event all email triggers are reset, so we have to call reminer notification after updating Time.
    await test.step("Enabling Email reminder notification", async () => {
      await enableDisableEmailTrigger(
        organiserApiContext,
        event_id,
        EventSettingID.EmailReminderBefore1Hour,
        true,
        EventRole.SPEAKER,
        true
      );
    });
    await test.step("Validating Email reminder", async () => {
      let fetched_email = await fetchMailasaurEmailObject(
        speakerUser["email"],
        `1 Hour to Go • ${eventInfoDto.eventTitle}`,
        true,
        100
      );
      let emailInvite = new emailReminderTemplateValidation(fetched_email);
      await validateEmailReminder(
        emailInvite,
        eventInfoDto,
        EventReminderHeader.EmailReminderBefore1Hour,
        timezoneCode
      );
    });

    await test.step("Verify ICS Details", async () => {
      let emailAttachmentFilename = await getIcsAttachment(
        speakerUser["email"],
        `1 Hour to Go • ${eventInfoDto.eventTitle}`
      );
      console.log("Filename of ICS is:", emailAttachmentFilename);
      const emailAttachmentData = parseICS(emailAttachmentFilename);
      await validateIcsData(emailAttachmentData, eventInfoDto);
    });
  });

  test("TC005: Validating Reminder ~ 7 Days to Go   @attendee @registered @withICS", async ({}) => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-701",
    });

    await test.step("Updating Start time of event", async () => {
      eventStartTime = new Date();
      eventStartTime.setDate(eventStartTime.getDate() + 7);
      eventStartTime.setSeconds(eventStartTime.getSeconds() + 30);
      console.log("New event Start Date time", eventStartTime.toISOString());
      eventEndTime = new Date();
      eventEndTime.setDate(eventEndTime.getDate() + 7);
      eventEndTime.setHours(eventEndTime.getHours() + 2);
      console.log("New event end Date time", eventEndTime.toISOString());
      await updateEventDateTime(
        organiserApiContext,
        event_id,
        eventInfoDto.landingPageId,
        eventStartTime.toISOString(),
        eventEndTime.toISOString()
      );
      eventInfoDto.startDateTime = eventStartTime.toISOString();
      eventInfoDto.startDateTime = eventStartTime.toISOString();
    });
    // When we are updating time of event all email triggers are reset, so we have to call reminer notification after updating Time.
    await test.step("Enabling Email reminder notification", async () => {
      await enableDisableEmailTrigger(
        organiserApiContext,
        event_id,
        EventSettingID.EmailReminderBefore7Day,
        true,
        EventRole.ATTENDEE,
        true
      );
    });
    await test.step("Validating Email reminder", async () => {
      let fetched_email = await fetchMailasaurEmailObject(
        userInfoDto.userEmail,
        `7 Days to Go • ${eventInfoDto.eventTitle}`,
        true,
        100
      );

      let emailInvite = new emailReminderTemplateValidation(fetched_email);
      await validateEmailReminder(
        emailInvite,
        eventInfoDto,
        EventReminderHeader.EmailReminderBefore7Days,
        timezoneCode
      );
    });

    await test.step("Verify ICS Details", async () => {
      let emailAttachmentFilename = await getIcsAttachment(
        userInfoDto.userEmail,
        `7 Days to Go • ${eventInfoDto.eventTitle}`
      );
      console.log("Filename of ICS is:", emailAttachmentFilename);
      const emailAttachmentData = parseICS(emailAttachmentFilename);
      await validateIcsData(emailAttachmentData, eventInfoDto);
    });
  });
});
