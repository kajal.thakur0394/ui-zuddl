import test, {
  APIRequestContext,
  BrowserContext,
  Locator,
  Page,
  expect,
} from "@playwright/test";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { createNewEvent, updateEventType } from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import EventType from "../../../enums/eventTypeEnum";
import { DataUtl } from "../../../util/dataUtil";
import { EventSetupPage } from "../../../page-objects/new-org-side-pages/event-setup";
import { EventController } from "../../../controller/EventController";

test.describe
  .parallel("@registration @organiser @change-event-type", async () => {
  let eventTitle: string;
  let eventId;
  let attendeeLandingPage: string;
  let speakerLandingpPage: string;
  let eventInfoDto: EventInfoDTO;
  let attendeePage: Page;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let organiserPage: Page;

  // test.describe.configure({ retries: 2 });

  test.beforeEach(async ({ context }) => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});

    orgApiContext = await orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: eventTitle,
    });
    eventInfoDto = new EventInfoDTO(eventId, eventTitle, true); // true to denote magic link enabled
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
    speakerLandingpPage = eventInfoDto.speakerLandingPage;
    // get driver for attendee
    attendeePage = await context.newPage();
    organiserPage = await orgBrowserContext.newPage();
  });

  test.afterEach(async () => {
    console.log("In teardown, closing the browser context");
    await orgBrowserContext?.close();
  });
  test("TC001 Organiser should be able to change the event type to hybrid from virtual using API", async () => {
    test.info().annotations.push({
      type: "P1",
      description:
        "https://linear.app/zuddl/issue/QAT-180/issue-with-changing-event-type-copy-p1",
    });
    await test.step(`Updating event type to Hybrid from virtual using API and verify resp status == 200 `, async () => {
      await updateEventType(orgApiContext, eventId, EventType.HYBRID);
    });
  });

  test("TC002: Organiser should be able to change the event type to hybrid from virtual using UI", async () => {
    test.info().annotations.push({
      type: "P1",
      description:
        "https://linear.app/zuddl/issue/QAT-180/issue-with-changing-event-type-copy-p1",
    });
    let setupPage = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/event/${eventId}/event-setup/basic`;
    await organiserPage.goto(setupPage);
    let eventSetupPage: EventSetupPage = new EventSetupPage(organiserPage);
    await eventSetupPage.changeEventType(EventType.HYBRID, eventId);
    await organiserPage.reload();
    await eventSetupPage.verifyEventType(EventType.HYBRID);
  });

  test("TC003: Organiser should be able to change the event time using UI", async () => {
    test.info().annotations.push({
      type: "P0",
      description:
        "https://linear.app/zuddl/issue/QAT-136/unable-to-edit-event-time-p0-new-org-side",
    });
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: eventTitle,
      add_end_date_from_today: 3,
    });
    let setupPage = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/event/${eventId}/event-setup/basic`;
    await organiserPage.goto(setupPage);
    // await organiserPage.pause();
    let eventSetupPage: EventSetupPage = new EventSetupPage(organiserPage);
    await eventSetupPage.changeEventStartTimeFromUI("11", "33", "AM", eventId);
    // await organiserPage.reload();
    // await organiserPage.waitForLoadState("networkidle");
    // await organiserPage.waitForLoadState("domcontentloaded");
    // await organiserPage.waitForLoadState("load");
    // await eventSetupPage.verifyEventStartTime("03", "33", "AM");
    let eventController = new EventController(orgApiContext, eventId);
    await eventController.validateEventStartTimeFromApi("11", "33", "AM");
  });
});
