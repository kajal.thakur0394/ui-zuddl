import {
  APIRequestContext,
  BrowserContext,
  expect,
  test,
} from "@playwright/test";

import BrowserFactory from "../../../util/BrowserFactory";
import { EventController } from "../../../controller/EventController";
import { DataUtl } from "../../../util/dataUtil";
import EventEntryType from "../../../enums/eventEntryEnum";
import { OnboardingCheckComponent } from "../../../page-objects/events-pages/live-side-components/OnboardingCheckComponent";
import { updateEventLandingPageDetails } from "../../../util/apiUtil";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@browser and network check`, async () => {
  let orgBrowserContext: BrowserContext;
  let eventId;
  let orgApiContext: APIRequestContext;
  let eventController: EventController;
  let attendeeEmail: string;
  let speakerEmail: string;
  let eventinfodto: EventInfoDTO;
  let orgPage;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiContext = orgBrowserContext.request;

    //1. create event id
    await test.step(`create a new event`, async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
      });
      console.log(`event id is ${eventId}`);
    });

    eventController = new EventController(orgApiContext, eventId);
    //2. enable magic link for this event
    await test.step("Enable magic link for this event", async () => {
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
      });
    });

    //3. invite new attendee
    await test.step(`invite new attendee to the event`, async () => {
      attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
      console.log(attendeeEmail);
      await eventController.inviteAttendeeToTheEvent(attendeeEmail);
    });

    // invite new speaker to the event
    await test.step(`invite new speaker to the event`, async () => {
      speakerEmail = DataUtl.getRandomSpeakerMailosaurEmail();
      console.log("speakerEmail: " + speakerEmail);
      await eventController.inviteSpeakerToTheEventByApi(
        speakerEmail,
        "pramila",
        "speakerS1"
      );
    });

    // onboarding check for attendee, speaker, organizer access group
    await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
      true
    );
    await eventController.getAccessGroupController.handleOnboardingChecksForSpeaker(
      true
    );
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`TC001: verify onboarding check displaying for attendee`, async ({
    page,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "P1",
      description:
        "https://linear.app/zuddl/issue/QAT-273/if-onboarding-check-is-enabled-from-access-group-its-working-fine-for",
    });

    let onboarding = new OnboardingCheckComponent(page);
    const attendeeMagicLink1 = await QueryUtil.fetchMagicLinkFromDB(
      eventId,
      attendeeEmail
    );
    await page.goto(await attendeeMagicLink1);
    await page.getByText("Enter Now").click();
    await onboarding.passNetworkBrowserCheck();
    await page.reload();
    expect(await onboarding.isBrowserNetworkCheckVisible()).toBeTruthy();
  });

  test(`TC002: Verify onboarding check displayed for speaker`, async ({
    page,
  }) => {
    //linear ticket info
    test.info().annotations.push({
      type: "P1",
      description:
        "https://linear.app/zuddl/issue/QAT-274/if-onboarding-check-is-enabled-from-access-group-its-working-fine-for",
    });
    let onboarding = new OnboardingCheckComponent(page);
    const speakerMagicLink = QueryUtil.fetchMagicLinkForSpeakerFromDB(
      eventId,
      speakerEmail
    );
    console.log("speakerMagicLink: " + speakerMagicLink);
    await page.goto(await speakerMagicLink);
    await page.getByText("Enter Now").click();
    await onboarding.passNetworkBrowserCheck();
    await page.reload();
    expect(await onboarding.isBrowserNetworkCheckVisible()).toBeTruthy();
  });

  test(`TC003: Verify onboarding check not displayed for organizer`, async ({
    page,
  }) => {
    //linear ticket infor
    test.info().annotations.push({
      type: "P2",
      description:
        "https://linear.app/zuddl/issue/QAT-277/verify-organisers-do-not-see-onboarding-checks-when-login-to-the-event",
    });
    let onboarding = new OnboardingCheckComponent(page);
    orgPage = await orgBrowserContext.newPage();
    eventinfodto = new EventInfoDTO(eventId, "Event Title");
    await orgPage.goto(eventinfodto.attendeeLandingPage);
    await orgPage.getByText("Enter Now").click();
    expect(await onboarding.isBrowserNetworkCheckVisible()).toBeTruthy();
  });

  test(`TC004: Verify onboarding check not displayed`, async ({ page }) => {
    test.info().annotations.push({
      type: "P2",
      description:
        "https://linear.app/zuddl/issue/QAT-278/verify-onboarding-checks-only-appears-when-user-logins-for-first-time",
    });
    let onboarding = new OnboardingCheckComponent(page);
    const attendeeMagicLink1 = QueryUtil.fetchMagicLinkFromDB(
      eventId,
      attendeeEmail
    );
    await page.goto(await attendeeMagicLink1);
    await page.getByText("Enter Now").click();
    await page.reload();
    await expect(await onboarding.isBrowserNetworkCheckVisible()).toBeTruthy();
  });
});
