import {
  APIRequestContext,
  expect,
  test,
  BrowserContext,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { EventController } from "../../../controller/EventController";
import {
  addAttendeeRecordInCsv,
  getSignedS3Url,
  processAttendeeCsvOnEndPoint,
  updateEventLandingPageDetails,
  waitForCsvFileProcessingToComplete,
} from "../../../util/apiUtil";
import { DataUtl } from "../../../util/dataUtil";
import EventEntryType from "../../../enums/eventEntryEnum";
import { deleteFile } from "../../../util/commonUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel("@downgrade-user @eventrole", async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventController: EventController;
  let eventId;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = orgBrowserContext.request;

    //create new event
    eventId = await EventController.generateNewEvent(orgApiContext, {
      event_title: DataUtl.getRandomEventTitle(),
    });

    //creating event controller to create a new event
    eventController = new EventController(orgApiContext, eventId);

    //enable magic link to the event
    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isSpeakermagicLinkEnabled: true,
      isMagicLinkEnabled: true,
    });
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test("TC001 downgrading a speaker to attendee from orgside -> attendee invite by API", async ({
    page,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-198/downgrade-speaker-to-attendee",
    });

    //add a user as speaker to the event
    let userTestEmail = DataUtl.getRandomSpeakerMailosaurEmail();
    await eventController.inviteSpeakerToTheEventByApi(
      userTestEmail,
      "prateekSpeaker",
      "automation"
    );
    //verify in DB that user role for this event is of Speaker
    //fetch event role data for this user

    await test.step(`Running validations to verify user has assigned speaker role`, async () => {
      await test.step(`verifying Event role for user for this event is SPEAKER`, async () => {
        const event_role_data_for_user_before_downgrade =
          await QueryUtil.fetchUserEventRoleData(eventId, userTestEmail);

        const event_role_before_downgrade =
          event_role_data_for_user_before_downgrade["role"];
        console.log(
          `the event role fetched from db before downgrade is ${event_role_before_downgrade}`
        );
        //assert that it is equal to speaker
        expect(
          event_role_before_downgrade,
          "expecting event role to be speaker"
        ).toBe("SPEAKER");
      });

      await test.step(`verify access group id assigned is of speaker access group`, async () => {
        const speaker_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Speakers"
          );
        const thisUserAccessGroupId =
          await QueryUtil.fetchUserAccessGroupIdForEvent(
            userTestEmail,
            eventId
          );
        expect(
          thisUserAccessGroupId,
          "expecting user access group to be speaker access group in DB"
        ).toBe(speaker_access_group_id);
      });

      await test.step(`verify this user has entry in speaker table for this event`, async () => {
        const isEntryPresentInSpeakerTable =
          await QueryUtil.verifyUserEmailIsPresentInSpeakerTableForAnEvent(
            userTestEmail,
            eventId
          );
        expect(
          isEntryPresentInSpeakerTable,
          "expecting user to be present in speaker tabel for this event"
        ).toBe(true);
      });
    });

    //fetch speaker magic link
    const speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
      eventId,
      userTestEmail
    );

    //verify speaker can navigate to speaker magic link and sees enter now button
    await page.goto(speakerMagicLink);
    const enterNowButton = page.locator("text=Enter Now");
    await expect(enterNowButton).toBeVisible();

    //now add the same email as attendee via API
    await eventController.inviteAttendeeToTheEvent(userTestEmail);

    //fetch event role data for this user after downgrade
    const event_role_data_for_user_after_downgrade =
      await QueryUtil.fetchUserEventRoleData(eventId, userTestEmail);
    const event_role_after_downgrade =
      event_role_data_for_user_after_downgrade["role"];

    //assert that it is equal to ATTENDEE

    await test.step(`Running validations to ensure SPEAKER TO ATTENDEE ROLE downgrade is succesfull`, async () => {
      await test.step(`Expecting Event Role table to have ROLE as ATTENDEE for this event`, async () => {
        expect(
          event_role_after_downgrade,
          "expecting event role to be attendee in event_role table"
        ).toBe("ATTENDEE");
      });

      await test.step(`Verify ACCESS GROUP has changed from Speaker to Attendee for this user`, async () => {
        const speaker_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Speakers"
          );
        console.log(`speaker access group id : ${speaker_access_group_id}`);

        const attendee_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Attendees"
          );
        console.log(`attendee access group id : ${attendee_access_group_id}`);

        const userAccessGroupIdFromDb =
          await QueryUtil.fetchUserAccessGroupIdForEvent(
            userTestEmail,
            eventId
          );
        console.log(
          `user access group id fetched from db for this event is ${userAccessGroupIdFromDb}`
        );
        expect(
          userAccessGroupIdFromDb,
          "expecting the access group id for this user for this event to be of ATTENDEE "
        ).toBe(attendee_access_group_id);
      });

      //checking speaker table for account id entry for event id
      await test.step(`Expecting Speaker table to not have record for this user for this event anymore`, async () => {
        const isUserNotPresent =
          await QueryUtil.verifyUserEmailIsNotPresentInSpeakerTableForAnEvent(
            userTestEmail,
            eventId
          );
        expect(
          isUserNotPresent,
          "expecting user to not be present in speaker table for this event"
        ).toBe(true);
      });
    });

    //user again reloads the speaker magic link and now this time it should not work
    await page.goto(speakerMagicLink);
    await expect(enterNowButton).not.toBeVisible();

    //now fetching magic link from db for attendee
    let attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
      eventId,
      userTestEmail
    );
    //now user visits magic link for attendee and it should work
    await page.goto(attendeeMagicLink);
    await expect(
      enterNowButton,
      "expecting enter now button to be visible on visiting attendee landing page"
    ).toBeVisible();
  });

  test("TC002 verify if speaker register himself as speaker on live event site, the downgrade should not happen", async ({
    page,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-231/verify-if-speaker-register-himself-as-speaker-on-live-event-site-the",
    });
    //add a user as speaker to the event
    let userTestEmail = DataUtl.getRandomSpeakerMailosaurEmail();
    await eventController.inviteSpeakerToTheEventByApi(
      userTestEmail,
      "prateekSpeaker",
      "automation"
    );

    //fetch event role data for this user
    const event_role_data_for_user_before_downgrade =
      await QueryUtil.fetchUserEventRoleData(eventId, userTestEmail);

    const event_role_before_downgrade =
      event_role_data_for_user_before_downgrade["role"];
    console.log(
      `the event role fetched from db before downgrade is ${event_role_before_downgrade}`
    );
    //assert that it is equal to speaker
    expect(
      event_role_before_downgrade,
      "expecting event role to be speaker"
    ).toBe("SPEAKER");

    //fetch speaker magic link
    const speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
      eventId,
      userTestEmail
    );

    //verify speaker can navigate to speaker magic link and sees enter now button
    await page.goto(speakerMagicLink);
    const enterNowButton = page.locator("text=Enter Now");
    await expect(enterNowButton).toBeVisible();

    //now add the same email as attendee via API and do not check the status code
    const registerUserApiRes =
      await eventController.registerUserToEventWithRegistrationApi(
        {
          firstName: "prateek",
          lastName: "automation",
          email: userTestEmail,
        },
        false
      );

    //assert user registration API responded with 406 status code
    expect(
      registerUserApiRes.status(),
      "expecting registration API response to return 406"
    ).toBe(406);

    //assert user registration API response to have message that this user is already added as speaker
    const registerUserApiResJson = await registerUserApiRes.json();
    expect(
      registerUserApiResJson["description"],
      "expecting response body to have messager that user is already added as speaker or moderator"
    ).toBe("You are already registered for this event.");

    //fetch event role data for this user after downgrade
    const event_role_data_for_user_after_downgrade_attempt =
      await QueryUtil.fetchUserEventRoleData(eventId, userTestEmail);
    const event_role_after_downgrade =
      event_role_data_for_user_after_downgrade_attempt["role"];

    //assert that it is equal to ATTENDEE
    expect(
      event_role_after_downgrade,
      "expecting event role to still be speaker"
    ).toBe("SPEAKER");
  });

  test("TC003 verify if existing speaker gets added by import attendee csv, that record should fail to process with proper message", async ({
    page,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-223/verify-downgrade-any-user-from-import-csv-is-not-possible",
    });
    //add a user as speaker to the event
    let userTestEmail = DataUtl.getRandomSpeakerMailosaurEmail();
    //create an attendee csv for this test
    let csvFileToAddDataIn = DataUtl.getRandomCsvName();
    let uploadedFileNameOnS3: string;
    let csvProcessingRespJson: object;
    await eventController.inviteSpeakerToTheEventByApi(
      userTestEmail,
      "prateekSpeaker",
      "automation"
    );
    //verify in DB that user role for this event is of Speaker

    //fetch event role data for this user
    const event_role_data_for_user_before_downgrade =
      await QueryUtil.fetchUserEventRoleData(eventId, userTestEmail);

    const event_role_before_downgrade =
      event_role_data_for_user_before_downgrade["role"];
    console.log(
      `the event role fetched from db before downgrade is ${event_role_before_downgrade}`
    );
    //assert that it is equal to speaker
    expect(
      event_role_before_downgrade,
      "expecting event role to be speaker"
    ).toBe("SPEAKER");

    //fetch speaker magic link
    const speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
      eventId,
      userTestEmail
    );

    //verify speaker can navigate to speaker magic link and sees enter now button
    await page.goto(speakerMagicLink);
    const enterNowButton = page.locator("text=Enter Now");
    await expect(enterNowButton).toBeVisible();

    console.log(`Using csv file -> ${csvFileToAddDataIn}`);
    await test.step(`Adding attendee record in ${csvFileToAddDataIn}`, async () => {
      await addAttendeeRecordInCsv(csvFileToAddDataIn, userTestEmail);
    });

    await test.step(`Uploading csv to s3 and get the signed url`, async () => {
      uploadedFileNameOnS3 = await getSignedS3Url(
        orgApiContext,
        csvFileToAddDataIn
      );
    });

    await test.step(`Deleting the csv, as we have uploaded it to s3`, async () => {
      deleteFile(csvFileToAddDataIn);
    });

    await test.step(`Processing the csv to upload attendee on zuddl endpoint`, async () => {
      await processAttendeeCsvOnEndPoint(
        orgApiContext,
        eventId.toString(),
        uploadedFileNameOnS3
      );
    });

    await test.step(`Now waiting for csv processing to complete and validating the log url contains expected message`, async () => {
      csvProcessingRespJson = await waitForCsvFileProcessingToComplete(
        orgApiContext,
        eventId.toString(),
        uploadedFileNameOnS3
      );

      //validating log url to have expected message
      const logUrl = csvProcessingRespJson["logUrl"];
      expect(logUrl, `expecting logurl to not be null`).not.toBeNull();
      console.log(`log url is ${logUrl}`);
      const errorMessage = await (await orgApiContext.get(logUrl)).text();
      expect(
        errorMessage,
        `expecting logurl to contain error message  ${errorMessage}`
      ).toContain(`- Email already added as speaker`);
      expect(
        errorMessage,
        `expecting logurl to contain error message  ${errorMessage}`
      ).toContain(`- Email already added as organizer`);
      expect(
        errorMessage,
        `expecting logurl to contain error message  ${errorMessage}`
      ).toContain(`- Email already added as moderator`);
      expect(
        errorMessage,
        `expecting logurl to contain error message  ${errorMessage}`
      ).toContain(`- Already registered for this event`);
    });
  });
});
