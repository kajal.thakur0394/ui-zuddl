import {
  APIRequestContext,
  Page,
  expect,
  test,
  BrowserContext,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { EventController } from "../../../controller/EventController";
import { updateEventLandingPageDetails } from "../../../util/apiUtil";
import { DataUtl } from "../../../util/dataUtil";
import EventEntryType from "../../../enums/eventEntryEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel("@upgrade-user @eventrole", async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let orgPage: Page;
  let eventController: EventController;
  let eventId;
  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = orgBrowserContext.request;
    //create new event
    eventId = await EventController.generateNewEvent(orgApiContext, {
      event_title: DataUtl.getRandomEventTitle(),
    });
    //creating event controller to create a new event
    eventController = new EventController(orgApiContext, eventId);

    //enable magic link to the event
    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isSpeakermagicLinkEnabled: true,
      isMagicLinkEnabled: true,
    });
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test("TC001 verify an ATTENDEE of an event can be upgraded to SPEAKER of that event", async ({
    page,
  }) => {
    let userEmail: string;
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-228/upgrade-attendee-to-speaker",
    });

    await test.step(`Organiser adds a new attendee to this event by using API`, async () => {
      userEmail = DataUtl.getRandomAttendeeEmail();
      await eventController.inviteAttendeeToTheEvent(userEmail);
    });

    await test.step(`Attendee gets the magic link and login to the event so that his event role gets created`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        userEmail
      );
      await page.goto(attendeeMagicLink);
      await page.click("text=Enter Now");
    });

    await test.step(`Run Validations to verify user has assigned ATTENDEE role`, async () => {
      // event role check
      await test.step(`verify event role table to have this user role as ATTENDEE`, async () => {
        const userEventRoleDataBeforeUpgrade =
          await QueryUtil.fetchUserEventRoleData(eventId, userEmail);
        const userEventRoleBeforeUpgrade =
          userEventRoleDataBeforeUpgrade["role"];
        console.log(`event role fetched ${userEventRoleBeforeUpgrade}`);
        expect(
          userEventRoleBeforeUpgrade,
          `expecting user role to be ATTENDEE ${eventId}`
        ).toBe("ATTENDEE");
      });

      //access group check
      await test.step(`verify user access group assigned is of ATTENDEE`, async () => {
        const attendee_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Attendees"
          );
        const thisUserAccessGroupId =
          await QueryUtil.fetchUserAccessGroupIdForEvent(userEmail, eventId);
        expect(
          thisUserAccessGroupId,
          "expecting user access group to be ATTENDEE access group in DB"
        ).toBe(attendee_access_group_id);
      });

      //event registration table check
      await test.step(`verify user has entry in event registration table`, async () => {
        const isEntryPresentInEventRegistrationTable =
          await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
            userEmail,
            eventId
          );
        expect(
          isEntryPresentInEventRegistrationTable,
          "expecting user email to be present in event registrable table for given event"
        ).toBe(true);
      });
    });

    await test.step(`Now organiser adds the same attendee email as speaker for the same event`, async () => {
      userEmail = DataUtl.getRandomAttendeeEmail();
      await eventController.inviteSpeakerToTheEventByApi(
        userEmail,
        "PrateekSpeaker",
        "Automation"
      );
    });

    await test.step(`Run Validations to verify user role has upgraded to SPEAKER role from ATTENDEE`, async () => {
      // event role check
      await test.step(`verify event role table to have this user role as SPEAKER`, async () => {
        const userEventRoleDataAfterUpgrade =
          await QueryUtil.fetchUserEventRoleData(eventId, userEmail);
        const userEventRoleAfterUpgrade = userEventRoleDataAfterUpgrade["role"];
        console.log(`event role fetched ${userEventRoleAfterUpgrade}`);
        expect(
          userEventRoleAfterUpgrade,
          `expecting user role to be SPEAKER ${eventId}`
        ).toBe("SPEAKER");
      });

      //access group check
      await test.step(`verify user access group assigned is of SPEAKER`, async () => {
        const speaker_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Speakers"
          );
        const thisUserAccessGroupId =
          await QueryUtil.fetchUserAccessGroupIdForEvent(userEmail, eventId);
        expect(
          thisUserAccessGroupId,
          "expecting user access group to be SPEAKER access group in DB"
        ).toBe(speaker_access_group_id);
      });

      //event registration table check
      await test.step(`verify user has no entry in event registration table`, async () => {
        const isEntryNotPresentInEventRegistrationTable =
          await QueryUtil.verifyUserEmailIsNotPresentInEventRegTableForGivenEvent(
            userEmail,
            eventId
          );
        expect(
          isEntryNotPresentInEventRegistrationTable,
          "expecting user email to be not present in event registrable table for given event"
        ).toBe(true);
      });

      //speaker table check
      await test.step(`verify user email has entry in speaker table for given event`, async () => {
        const isEntryPresentInSpeakerTable =
          await QueryUtil.verifyUserEmailIsPresentInSpeakerTableForAnEvent(
            userEmail,
            eventId
          );

        expect(
          isEntryPresentInSpeakerTable,
          "expecting user email to be present in speaker table for given event"
        ).toBe(true);
      });
    });
  });
});
