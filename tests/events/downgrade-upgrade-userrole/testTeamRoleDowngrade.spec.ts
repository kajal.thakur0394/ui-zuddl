import {
  APIRequestContext,
  Page,
  expect,
  test,
  BrowserContext,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import {
  userVerifyOtpByApi,
  userVerifyTeamInvitationUsingOtp,
} from "../../../util/apiUtil";
import { DataUtl } from "../../../util/dataUtil";
import { TeamController } from "../../../controller/TeamsController";
import { OrganisationController } from "../../../controller/OrganisationController";
import BrowserFactory from "../../../util/BrowserFactory";
import {
  EventLicenseDetailsDto,
  LicenseDetailsDto,
  WebinarLicenseDetailsDto,
} from "../../../interfaces/OrganisationControllerInterface";
import BillngPeriodEnum from "../../../enums/billingPeriodEnum";
import ProductType from "../../../enums/productTypeEnum";
import PlanType from "../../../enums/planTypeEnum";
import { MemberRolesEnum } from "../../../enums/MemberRolesEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe("@downgrade-user @teamrole", async () => {
  //test.describe.configure({ retries: 2 });
  let adminBrowserContext: BrowserContext;
  let adminApiContext: APIRequestContext;
  let adminPage: Page;
  let eventController: EventController;
  let eventId;
  let organisationIdForTeamAutomation;
  let adminEmail: string;
  let teamMemberContext: BrowserContext;
  let teamMemberAPIContext: APIRequestContext;
  let teamMemberPage: Page;
  let temMemberEmail: string;
  let organisationId: string;
  let generalTeamId: string;
  let organisationName: string;
  let defaultEventLicenseDetailsDto: EventLicenseDetailsDto;
  let defaultWebinarLicenseDetailsDto: WebinarLicenseDetailsDto;
  let defaultLicenseDetailDtoEvents: LicenseDetailsDto;
  let defaultLicenseDetailDtoWebinar: LicenseDetailsDto;
  let createOrganizationDetailsData;

  let teamController: TeamController;
  let organisationController: OrganisationController;

  test.beforeAll(async () => {
    await test.step("Initialize admin browser context, admin page and admin API context.", async () => {
      adminBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      adminPage = await adminBrowserContext.newPage();
      adminApiContext = adminPage.request;
    });

    await test.step("Get random organisation name.", async () => {
      organisationName = DataUtl.getRandomOrganisationName();
    });

    await test.step("Get random admin email.", async () => {
      adminEmail = DataUtl.getRandomOrganisationOwnerEmail();
    });

    await test.step("Set default event license details.", async () => {
      defaultEventLicenseDetailsDto = {
        planStartDate: new Date().toISOString(),
        planEndDate: new Date(
          new Date().setDate(new Date().getDate() + 3)
        ).toISOString(),
        teamSize: 10,
        cloudStorageSizeInGb: 2,
        noOfAttendees: 100,
        perEventLengthInHours: 500,
        enabledEventTypes: ["STANDARD"],
      };
    });

    await test.step("Set default webinar license details.", async () => {
      defaultWebinarLicenseDetailsDto = {
        planStartDate: new Date().toISOString(),
        planEndDate: new Date(
          new Date().setDate(new Date().getDate() + 3)
        ).toISOString(),
        noOfTeams: 10,
        cloudStorageSizeInGb: 2,
        noOfAttendees: 100,
        restrictionRenewPeriod: BillngPeriodEnum.MONTHLY,
        attendeesBufferPercentage: 20,
      };
    });

    await test.step("Set default license details - events.", async () => {
      defaultLicenseDetailDtoEvents = {
        productType: ProductType.EVENT, //enum
        planType: PlanType.ATTENDEE_CUSTOM, //enum
        billingPeriodUnit: BillngPeriodEnum.MONTHLY,
        eventLicenseDetailsDto: defaultEventLicenseDetailsDto,
      };
    });

    await test.step("Set default license details - webinar.", async () => {
      defaultLicenseDetailDtoWebinar = {
        productType: ProductType.WEBINAR, //enum
        planType: PlanType.CUSTOM, //enum
        billingPeriodUnit: BillngPeriodEnum.MONTHLY,
        webinarLicenseDetailsDto: defaultWebinarLicenseDetailsDto,
      };
    });

    await test.step("Set default license details - webinar.", async () => {
      createOrganizationDetailsData = {
        firstName: "PrateeekAutomation",
        lastName: "OwnerQA",
        email: adminEmail,
        organizationName: organisationName,
        licenseDetailsDto: defaultLicenseDetailDtoEvents,
      };
    });

    await test.step(`Creating new organisation having EVENT Plan with name : ${organisationName} and owner email : ${adminEmail}`, async () => {
      organisationController = new OrganisationController(adminApiContext);
      const createOrgApiResp =
        await organisationController.createNewOrganization(
          createOrganizationDetailsData
        );
      const createOrgApiRespJson = await createOrgApiResp.json();
      organisationId = createOrgApiRespJson["id"];
      console.log(
        `New organisation is created witd org id : ${organisationId}`
      );
      teamController = new TeamController(adminApiContext, organisationId);
    });

    await test.step(`OTP verificaiton.`, async () => {
      const otpVerificationIdForAdmin = await userVerifyTeamInvitationUsingOtp(
        adminPage.request,
        adminEmail
      );

      console.log(`admin otp verification id is ${otpVerificationIdForAdmin}`);

      //fetch otp for admin
      const otpCodeForAdmin = await QueryUtil.fetchLoginOTPByVerificationId(
        adminEmail,
        otpVerificationIdForAdmin
      );
      //verify otp for admin
      console.log(`fetched otp for admin  is ${otpCodeForAdmin}`);
      await userVerifyOtpByApi(
        adminPage.request,
        otpVerificationIdForAdmin,
        otpCodeForAdmin
      );
    });
  });

  test.beforeEach(async () => {
    await test.step("Create an event.", async () => {
      eventId = await EventController.generateNewEvent(adminApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
      });
    });

    await test.step("Initialize event controller.", async () => {
      eventController = new EventController(adminApiContext, eventId);
    });

    await test.step(`Creating browser context and page for team member`, async () => {
      teamMemberContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      teamMemberAPIContext = teamMemberContext.request;
      teamMemberPage = await teamMemberContext.newPage();
      teamController = new TeamController(
        adminPage.request,
        organisationIdForTeamAutomation
      );
    });

    await test.step(`generating team member email id for the case`, async () => {
      temMemberEmail = DataUtl.getRandomAttendeeEmail();
    });
  });

  test.afterEach(async () => {
    await BrowserFactory.closeBrowserContext(teamMemberContext);
  });
  test.afterAll(async () => {
    await BrowserFactory.closeBrowserContext(adminBrowserContext);
  });

  test("TC001 Downgrading ORGANISER role added from team to ATTENDEE by calling API to invite ATTENDEE for an event", async () => {
    //team member context

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-197/downgrade-org-to-attendee",
    });

    await test.step(`Organiser adds a new random email to his team with organiser role for event`, async () => {
      await test.step(`Fetch default/general Team Id`, async () => {
        generalTeamId = await teamController.fetchGeneralTeamId();
      });
      //add a new member to team with organiser role to events
      const memberToAddDetails = {
        firstName: "Prateek",
        lastName: "automation",
        email: temMemberEmail,
        productRole: MemberRolesEnum.ORGANIZER,
        myTeamList: [{ teamId: generalTeamId }],
      };
      //add a new member to team with organiser role to events
      await organisationController.addNewMemberToOrganisation(
        memberToAddDetails
      );
    });

    await test.step(`Newly added member will login via otp to verify his invitation`, async () => {
      //verify the event role for the person is of organiser
      //user verifies his invitation by authenticating using otp
      const otpVerificationIdForOrganiser =
        await userVerifyTeamInvitationUsingOtp(
          teamMemberPage.request,
          temMemberEmail
        );
      //fetch otp by verification id
      const otpCodeForOrganiser = await QueryUtil.fetchLoginOTPByVerificationId(
        temMemberEmail,
        otpVerificationIdForOrganiser
      );
      console.log(`fetched otp for organiser is ${otpCodeForOrganiser}`);
      //now organiser verify his otp
      await userVerifyOtpByApi(
        teamMemberPage.request,
        otpVerificationIdForOrganiser,
        otpCodeForOrganiser
      );
    });

    await test.step(`Verify that the added team member user role is Organiser for the event created`, async () => {
      await test.step(`verify event role table to have ORGANIZER role for this user for this event`, async () => {
        //now fetch this user event role data before downgrade
        const userEventRoleDataBeforeDowngrade =
          await QueryUtil.fetchUserEventRoleData(eventId, temMemberEmail);
        const userEventRoleBeforeDowngrade =
          userEventRoleDataBeforeDowngrade["role"];
        console.log(
          `event role fetched before downgrade is ${userEventRoleBeforeDowngrade}`
        );
        expect(
          userEventRoleBeforeDowngrade,
          "expecting user role to be ORGANISER"
        ).toBe("ORGANIZER");
      });

      await test.step(`verify the ACCESS GROUP ROLE to be of ORGANIZER for this event`, async () => {
        const organizer_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Organizers"
          );
        const thisUserAccessGroupId =
          await QueryUtil.fetchUserAccessGroupIdForEvent(
            temMemberEmail,
            eventId
          );
        expect(
          thisUserAccessGroupId,
          "expecting user access group to be ORGANIZER access group in DB"
        ).toBe(organizer_access_group_id);
      });
    });

    await test.step(`Now organiser of this event will add the newly added team member as attendee for this event`, async () => {
      //now team admin will add this newly added organiser to his team to this event which should downgrade his role
      await eventController.inviteAttendeeToTheEvent(temMemberEmail);
    });

    await test.step(`Run Validations to verify user got assigned ATTENDEE role for this event`, async () => {
      // event role check
      await test.step(`verify event role table to have this user role as ATTENDEE`, async () => {
        const userEventRoleDataAfterDowngrade =
          await QueryUtil.fetchUserEventRoleData(eventId, temMemberEmail);
        const userEventRoleAfterUpgrade =
          userEventRoleDataAfterDowngrade["role"];
        console.log(`event role fetched ${userEventRoleAfterUpgrade}`);
        expect(
          userEventRoleAfterUpgrade,
          `expecting user role to be ATTENDEE ${eventId}`
        ).toBe("ATTENDEE");
      });

      //access group check
      await test.step(`verify user access group assigned is of ATTENDEE`, async () => {
        const attendee_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Attendees"
          );
        const thisUserAccessGroupId =
          await QueryUtil.fetchUserAccessGroupIdForEvent(
            temMemberEmail,
            eventId
          );
        expect(
          thisUserAccessGroupId,
          "expecting user access group to be ATTENDEE access group in DB"
        ).toBe(attendee_access_group_id);
      });

      //event registration table check
      await test.step(`verify user has entry in event registration table`, async () => {
        const isEntryPresentInEventRegistrationTable =
          await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
            temMemberEmail,
            eventId
          );
        expect(
          isEntryPresentInEventRegistrationTable,
          "expecting user email to be present in event registrable table for given event"
        ).toBe(true);
      });
    });

    await test.step(`Now organiser creates a new event and verify the team member there gets the organiser role only`, async () => {
      //create new event 2
      let newEventId = await EventController.generateNewEvent(adminApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
      });

      await test.step(`Verify the team member role for this event remains ORGANIZER`, async () => {
        //now fetch this user event role data before downgrade
        const userEventRoleData = await QueryUtil.fetchUserEventRoleData(
          newEventId,
          temMemberEmail
        );
        const userEventRole = userEventRoleData["role"];
        console.log(`event role fetched is ${userEventRole}`);
        expect(
          userEventRole,
          `expecting user role to still be ORGANISER for the event ${newEventId}`
        ).toBe("ORGANIZER");
      });
    });
  });

  test("TC002 Downgrading ORGANISER role added from team to SPEAKER by calling API to invite SPEAKER for an event", async () => {
    //team member context
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-196/downgrade-org-to-speaker",
    });
    await test.step(`Fetch default/general Team Id`, async () => {
      generalTeamId = await teamController.fetchGeneralTeamId();
    });
    //add a new member to team with organiser role to events

    await test.step(`Organiser adds a new random email to his team with organiser role for event`, async () => {
      //add a new member to team with organiser role to events
      const memberToAddDetails = {
        firstName: "Prateek",
        lastName: "automation",
        email: temMemberEmail,
        productRole: MemberRolesEnum.ORGANIZER,
        myTeamList: [{ teamId: generalTeamId }],
      };
      //add a new member to team with organiser role to events
      await organisationController.addNewMemberToOrganisation(
        memberToAddDetails
      );
    });

    await test.step(`Newly added member will login via otp to verify his invitation`, async () => {
      //verify the event role for the person is of organiser
      //user verifies his invitation by authenticating using otp
      const otpVerificationIdForOrganiser =
        await userVerifyTeamInvitationUsingOtp(
          teamMemberPage.request,
          temMemberEmail
        );
      //fetch otp by verification id
      const otpCodeForOrganiser = await QueryUtil.fetchLoginOTPByVerificationId(
        temMemberEmail,
        otpVerificationIdForOrganiser
      );
      console.log(`fetched otp for organiser is ${otpCodeForOrganiser}`);
      //now organiser verify his otp
      await userVerifyOtpByApi(
        teamMemberPage.request,
        otpVerificationIdForOrganiser,
        otpCodeForOrganiser
      );
    });

    await test.step(`Verify that the added team member user role is Organiser for the event created`, async () => {
      await test.step(`verify event role table to have ORGANIZER role for this user for this event`, async () => {
        //now fetch this user event role data before downgrade
        const userEventRoleDataBeforeDowngrade =
          await QueryUtil.fetchUserEventRoleData(eventId, temMemberEmail);
        const userEventRoleBeforeDowngrade =
          userEventRoleDataBeforeDowngrade["role"];
        console.log(
          `event role fetched before downgrade is ${userEventRoleBeforeDowngrade}`
        );
        expect(
          userEventRoleBeforeDowngrade,
          "expecting user role to be ORGANISER"
        ).toBe("ORGANIZER");
      });

      await test.step(`verify the ACCESS GROUP ROLE to be of ORGANIZER for this event`, async () => {
        const organizer_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Organizers"
          );
        const thisUserAccessGroupId =
          await QueryUtil.fetchUserAccessGroupIdForEvent(
            temMemberEmail,
            eventId
          );
        expect(
          thisUserAccessGroupId,
          "expecting user access group to be ORGANIZER access group in DB"
        ).toBe(organizer_access_group_id);
      });
    });

    await test.step(`Now organiser of this event will add the newly added team member as speaker for this event`, async () => {
      //now team admin will add this newly added organiser to his team to this event which should downgrade his role
      await eventController.inviteSpeakerToTheEventByApi(
        temMemberEmail,
        "prateekSpeaker",
        "Automation"
      );
    });

    await test.step(`Run Validations to verify user got assigned SPEAKER role for this event`, async () => {
      // event role check
      await test.step(`verify event role table to have this user role as SPEAKER`, async () => {
        const userEventRoleDataAfterDowngrade =
          await QueryUtil.fetchUserEventRoleData(eventId, temMemberEmail);
        const userEventRoleAfterUpgrade =
          userEventRoleDataAfterDowngrade["role"];
        console.log(`event role fetched ${userEventRoleAfterUpgrade}`);
        expect(
          userEventRoleAfterUpgrade,
          `expecting user role to be SPEAKER ${eventId}`
        ).toBe("SPEAKER");
      });

      //access group check
      await test.step(`verify user access group assigned is of ATTENDEE`, async () => {
        const speaker_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Speakers"
          );
        const thisUserAccessGroupId =
          await QueryUtil.fetchUserAccessGroupIdForEvent(
            temMemberEmail,
            eventId
          );
        expect(
          thisUserAccessGroupId,
          "expecting user access group to be SPEAKER access group in DB"
        ).toBe(speaker_access_group_id);
      });

      //speaker table check
      await test.step(`verify this user has entry in speaker table for this event`, async () => {
        const isEntryPresentInSpeakerTable =
          await QueryUtil.verifyUserEmailIsPresentInSpeakerTableForAnEvent(
            temMemberEmail,
            eventId
          );
        expect(
          isEntryPresentInSpeakerTable,
          "expecting user to be present in speaker tabel for this event"
        ).toBe(true);
      });
    });
    await test.step(`Now organiser creates a new event and verify the team member there gets the organiser role only`, async () => {
      //create new event 2
      let newEventId = await EventController.generateNewEvent(adminApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
      });

      await test.step(`Verify the team member role for this event remains ORGANIZER `, async () => {
        //now fetch this user event role data before downgrade
        const userEventRoleData = await QueryUtil.fetchUserEventRoleData(
          newEventId,
          temMemberEmail
        );
        const userEventRole = userEventRoleData["role"];
        console.log(`event role fetched is ${userEventRole}`);
        expect(
          userEventRole,
          `expecting user role to still be ORGANISER for the event ${newEventId}`
        ).toBe("ORGANIZER");
      });
    });
  });

  test("TC003 Downgrading MODERATOR role added from team to ATTENDEE by calling API to invite ATTENDEE for an event", async () => {
    //team member context

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-224/downgrade-moderator-to-attendee",
    });
    await test.step(`Fetch default/general Team Id`, async () => {
      generalTeamId = await teamController.fetchGeneralTeamId();
    });
    await test.step(`Organiser adds a new random email to his team with organiser role for event`, async () => {
      //add a new member to team with organiser role to events
      const memberToAddDetails = {
        firstName: "Prateek",
        lastName: "automation",
        email: temMemberEmail,
        productRole: MemberRolesEnum.MODERATOR,
        myTeamList: [{ teamId: generalTeamId }],
      };
      //add a new member to team with organiser role to events
      await organisationController.addNewMemberToOrganisation(
        memberToAddDetails
      );
    });

    await test.step(`Newly added member will login via otp to verify his invitation`, async () => {
      //verify the event role for the person is of organiser
      //user verifies his invitation by authenticating using otp
      const otpVerificationIdForOrganiser =
        await userVerifyTeamInvitationUsingOtp(
          teamMemberPage.request,
          temMemberEmail
        );
      //fetch otp by verification id
      const otpCodeForOrganiser = await QueryUtil.fetchLoginOTPByVerificationId(
        temMemberEmail,
        otpVerificationIdForOrganiser
      );
      console.log(`fetched otp for organiser is ${otpCodeForOrganiser}`);
      //now organiser verify his otp
      await userVerifyOtpByApi(
        teamMemberPage.request,
        otpVerificationIdForOrganiser,
        otpCodeForOrganiser
      );
    });

    await test.step(`Verify that the added team member user role is MODERATOR for the event created`, async () => {
      await test.step(`verify event role table to have MODERATOR role for this user for this event`, async () => {
        //now fetch this user event role data before downgrade
        const userEventRoleDataBeforeDowngrade =
          await QueryUtil.fetchUserEventRoleData(eventId, temMemberEmail);
        const userEventRoleBeforeDowngrade =
          userEventRoleDataBeforeDowngrade["role"];
        console.log(
          `event role fetched before downgrade is ${userEventRoleBeforeDowngrade}`
        );
        expect(
          userEventRoleBeforeDowngrade,
          "expecting user role to be MODERATOR"
        ).toBe("MODERATOR");
      });

      await test.step(`verify the ACCESS GROUP ROLE to be of MODERATOR for this event`, async () => {
        const moderator_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Moderators"
          );
        const thisUserAccessGroupId =
          await QueryUtil.fetchUserAccessGroupIdForEvent(
            temMemberEmail,
            eventId
          );
        expect(
          thisUserAccessGroupId,
          "expecting user access group to be MODERATOR access group in DB"
        ).toBe(moderator_access_group_id);
      });
    });

    await test.step(`Now organiser of this event will add the newly added team member as attendee for this event`, async () => {
      //now team admin will add this newly added organiser to his team to this event which should downgrade his role
      await eventController.inviteAttendeeToTheEvent(temMemberEmail);
    });

    await test.step(`Run Validations to verify user got assigned ATTENDEE role for this event`, async () => {
      // event role check
      await test.step(`verify event role table to have this user role as ATTENDEE`, async () => {
        const userEventRoleDataAfterDowngrade =
          await QueryUtil.fetchUserEventRoleData(eventId, temMemberEmail);
        const userEventRoleAfterUpgrade =
          userEventRoleDataAfterDowngrade["role"];
        console.log(`event role fetched ${userEventRoleAfterUpgrade}`);
        expect(
          userEventRoleAfterUpgrade,
          `expecting user role to be ATTENDEE ${eventId}`
        ).toBe("ATTENDEE");
      });

      //access group check
      await test.step(`verify user access group assigned is of ATTENDEE`, async () => {
        const attendee_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Attendees"
          );
        const thisUserAccessGroupId =
          await QueryUtil.fetchUserAccessGroupIdForEvent(
            temMemberEmail,
            eventId
          );
        expect(
          thisUserAccessGroupId,
          "expecting user access group to be ATTENDEE access group in DB"
        ).toBe(attendee_access_group_id);
      });

      //event registration table check
      await test.step(`verify user has entry in event registration table`, async () => {
        const isEntryPresentInEventRegistrationTable =
          await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
            temMemberEmail,
            eventId
          );
        expect(
          isEntryPresentInEventRegistrationTable,
          "expecting user email to be present in event registrable table for given event"
        ).toBe(true);
      });
    });

    await test.step(`Now organiser creates a new event and verify the team member there gets the MODERATOR role only`, async () => {
      //create new event 2
      let newEventId = await EventController.generateNewEvent(adminApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
      });

      await test.step(`Verify the team member role for this event remains MODERATOR`, async () => {
        //now fetch this user event role data before downgrade
        const userEventRoleData = await QueryUtil.fetchUserEventRoleData(
          newEventId,
          temMemberEmail
        );
        const userEventRole = userEventRoleData["role"];
        console.log(`event role fetched is ${userEventRole}`);
        expect(
          userEventRole,
          `expecting user role to still be MODERATOR for the event ${newEventId}`
        ).toBe("MODERATOR");
      });
    });
  });

  test("TC004 Downgrading MODERATOR role added from team to SPEAKER by calling API to invite SPEAKER for an event", async () => {
    //team member context

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-225/downgrade-moderator-to-speaker",
    });
    await test.step(`Fetch default/general Team Id`, async () => {
      generalTeamId = await teamController.fetchGeneralTeamId();
    });

    await test.step(`Organiser adds a new random email to his team with organiser role for event`, async () => {
      temMemberEmail = DataUtl.getRandomAttendeeEmail();
      //add a new member to team with organiser role to events

      const memberToAddDetails = {
        firstName: "Prateek",
        lastName: "automation",
        email: temMemberEmail,
        productRole: MemberRolesEnum.MODERATOR,
        myTeamList: [{ teamId: generalTeamId }],
      };
      //add a new member to team with organiser role to events
      await organisationController.addNewMemberToOrganisation(
        memberToAddDetails
      );
    });

    await test.step(`Newly added member will login via otp to verify his invitation`, async () => {
      //verify the event role for the person is of organiser
      //user verifies his invitation by authenticating using otp
      const otpVerificationIdForOrganiser =
        await userVerifyTeamInvitationUsingOtp(
          teamMemberPage.request,
          temMemberEmail
        );
      //fetch otp by verification id
      const otpCodeForOrganiser = await QueryUtil.fetchLoginOTPByVerificationId(
        temMemberEmail,
        otpVerificationIdForOrganiser
      );
      console.log(`fetched otp for organiser is ${otpCodeForOrganiser}`);
      //now organiser verify his otp
      await userVerifyOtpByApi(
        teamMemberPage.request,
        otpVerificationIdForOrganiser,
        otpCodeForOrganiser
      );
    });

    await test.step(`Verify that the added team member user role is MODERATOR for the event created`, async () => {
      await test.step(`verify event role table to have MODERATOR role for this user for this event`, async () => {
        //now fetch this user event role data before downgrade
        const userEventRoleDataBeforeDowngrade =
          await QueryUtil.fetchUserEventRoleData(eventId, temMemberEmail);
        const userEventRoleBeforeDowngrade =
          userEventRoleDataBeforeDowngrade["role"];
        console.log(
          `event role fetched before downgrade is ${userEventRoleBeforeDowngrade}`
        );
        expect(
          userEventRoleBeforeDowngrade,
          "expecting user role to be MODERATOR"
        ).toBe("MODERATOR");
      });

      await test.step(`verify the ACCESS GROUP ROLE to be of MODERATOR for this event`, async () => {
        const moderator_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Moderators"
          );
        const thisUserAccessGroupId =
          await QueryUtil.fetchUserAccessGroupIdForEvent(
            temMemberEmail,
            eventId
          );
        expect(
          thisUserAccessGroupId,
          "expecting user access group to be MODERATOR access group in DB"
        ).toBe(moderator_access_group_id);
      });
    });

    await test.step(`Now organiser of this event will add the newly added team member as SPEAKER for this event`, async () => {
      //now team admin will add this newly added organiser to his team to this event which should downgrade his role
      await eventController.inviteSpeakerToTheEventByApi(
        temMemberEmail,
        "prateekSpeaker",
        "Automation"
      );
    });

    await test.step(`Run Validations to verify user got assigned SPEAKER role for this event`, async () => {
      // event role check
      await test.step(`verify event role table to have this user role as SPEAKER`, async () => {
        const userEventRoleDataAfterDowngrade =
          await QueryUtil.fetchUserEventRoleData(eventId, temMemberEmail);
        const userEventRoleAfterUpgrade =
          userEventRoleDataAfterDowngrade["role"];
        console.log(`event role fetched ${userEventRoleAfterUpgrade}`);
        expect(
          userEventRoleAfterUpgrade,
          `expecting user role to be SPEAKER ${eventId}`
        ).toBe("SPEAKER");
      });

      //access group check
      await test.step(`verify user access group assigned is of ATTENDEE`, async () => {
        const speaker_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Speakers"
          );
        const thisUserAccessGroupId =
          await QueryUtil.fetchUserAccessGroupIdForEvent(
            temMemberEmail,
            eventId
          );
        expect(
          thisUserAccessGroupId,
          "expecting user access group to be SPEAKER access group in DB"
        ).toBe(speaker_access_group_id);
      });

      //speaker table check
      await test.step(`verify this user has entry in speaker table for this event`, async () => {
        const isEntryPresentInSpeakerTable =
          await QueryUtil.verifyUserEmailIsPresentInSpeakerTableForAnEvent(
            temMemberEmail,
            eventId
          );
        expect(
          isEntryPresentInSpeakerTable,
          "expecting user to be present in speaker tabel for this event"
        ).toBe(true);
      });
    });

    await test.step(`Now organiser creates a new event and verify the team member there gets the MODERATOR role only`, async () => {
      //create new event 2
      let newEventId = await EventController.generateNewEvent(adminApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
      });

      await test.step(`Verify the team member role for this event remains MODERATOR`, async () => {
        //now fetch this user event role data before downgrade
        const userEventRoleData = await QueryUtil.fetchUserEventRoleData(
          newEventId,
          temMemberEmail
        );
        const userEventRole = userEventRoleData["role"];
        console.log(`event role fetched is ${userEventRole}`);
        expect(
          userEventRole,
          `expecting user role to still be MODERATOR for the event ${newEventId}`
        ).toBe("MODERATOR");
      });
    });
  });

  test("TC005 Downgrading ORGANISER who has created event to ATTENDEE should be restricted", async ({}) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-195/downgrade-user-who-created-the-event",
    });
    await test.step(`Verify that the event role for the organiser who has created this event is ORGANIZER`, async () => {
      //now fetch this user event role data before downgrade
      const userEventRoleDataBeforeDowngrade =
        await QueryUtil.fetchUserEventRoleData(eventId, adminEmail);
      const userEventRoleBeforeDowngrade =
        userEventRoleDataBeforeDowngrade["role"];
      console.log(`event role fetched ${userEventRoleBeforeDowngrade}`);
      expect(
        userEventRoleBeforeDowngrade,
        `expecting user role to be ORGANIZER ${eventId}`
      ).toBe("ORGANIZER");
    });

    await test.step(`Now organiser try to add himself to the event as ATTENDEE by API`, async () => {
      await test.step(`Verify an error message is shown`, async () => {
        const addAttendeeResp = await eventController.inviteAttendeeToTheEvent(
          adminEmail
        );
        expect(
          addAttendeeResp.status(),
          "expecting add attendee API to return 406 status code"
        ).toBe(406);
        const addAttendeeRespJson = await addAttendeeResp.json();
        const descriptionInApiRespBoy = addAttendeeRespJson["description"];
        expect(
          descriptionInApiRespBoy,
          "expecting api response to have description indicating organiser who created event can not be downgraded"
        ).toContain("downgrade the organizer who created the event");
      });
    });

    await test.step(`Verify that the organiser who has created this event still has ORGANISER ROLE`, async () => {
      //now fetch this user event role data before downgrade
      const userEventRoleDataBeforeDowngrade =
        await QueryUtil.fetchUserEventRoleData(eventId, adminEmail);
      const userEventRoleBeforeDowngrade =
        userEventRoleDataBeforeDowngrade["role"];
      console.log(`event role fetched ${userEventRoleBeforeDowngrade}`);
      expect(
        userEventRoleBeforeDowngrade,
        `expecting user role to be ORGANIZER ${eventId}`
      ).toBe("ORGANIZER");
    });
  });
});
