import {
  APIRequestContext,
  Page,
  expect,
  test,
  BrowserContext,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import {
  updateEventLandingPageDetails,
  userVerifyOtpByApi,
  userVerifyTeamInvitationUsingOtp,
} from "../../../util/apiUtil";
import { DataUtl } from "../../../util/dataUtil";
import { TeamController } from "../../../controller/TeamsController";
import EventEntryType from "../../../enums/eventEntryEnum";
import { OrganisationController } from "../../../controller/OrganisationController";
import BrowserFactory from "../../../util/BrowserFactory";
import { MemberRolesEnum } from "../../../enums/MemberRolesEnum";
import {
  EventLicenseDetailsDto,
  LicenseDetailsDto,
  WebinarLicenseDetailsDto,
} from "../../../interfaces/OrganisationControllerInterface";
import BillngPeriodEnum from "../../../enums/billingPeriodEnum";
import ProductType from "../../../enums/productTypeEnum";
import PlanType from "../../../enums/planTypeEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe("@upgrade-user @teamrole", async () => {
  test.describe.configure({ retries: 2 });

  let adminBrowserContext: BrowserContext;
  let adminApiContext: APIRequestContext;
  let adminPage: Page;
  let eventController: EventController;
  let eventId;
  let adminEmail: string;
  let organisationController: OrganisationController;
  let teamController: TeamController;
  let organisationId: string;
  let generalTeamId: string;
  let organisationName: string;
  let defaultEventLicenseDetailsDto: EventLicenseDetailsDto;
  let defaultWebinarLicenseDetailsDto: WebinarLicenseDetailsDto;
  let defaultLicenseDetailDtoEvents: LicenseDetailsDto;
  let defaultLicenseDetailDtoWebinar: LicenseDetailsDto;
  let createOrganizationDetailsData;

  test.beforeAll(async ({ browser }) => {
    await test.step("Initialize admin browser context, admin page and admin API context.", async () => {
      adminBrowserContext = await browser.newContext();
      adminPage = await adminBrowserContext.newPage();
      adminApiContext = adminPage.request;
    });

    await test.step("Get random organisation name.", async () => {
      organisationName = DataUtl.getRandomOrganisationName();
    });

    await test.step("Get random admin email.", async () => {
      adminEmail = DataUtl.getRandomOrganisationOwnerEmail();
    });

    await test.step("Set default event license details.", async () => {
      defaultEventLicenseDetailsDto = {
        planStartDate: new Date().toISOString(),
        planEndDate: new Date(
          new Date().setDate(new Date().getDate() + 3)
        ).toISOString(),
        teamSize: 10,
        cloudStorageSizeInGb: 2,
        noOfAttendees: 100,
        perEventLengthInHours: 500,
        enabledEventTypes: ["STANDARD"],
      };
    });

    await test.step("Set default webinar license details.", async () => {
      defaultWebinarLicenseDetailsDto = {
        planStartDate: new Date().toISOString(),
        planEndDate: new Date(
          new Date().setDate(new Date().getDate() + 3)
        ).toISOString(),
        noOfTeams: 10,
        cloudStorageSizeInGb: 2,
        noOfAttendees: 100,
        restrictionRenewPeriod: BillngPeriodEnum.MONTHLY,
        attendeesBufferPercentage: 20,
      };
    });

    await test.step("Set default license details - events.", async () => {
      defaultLicenseDetailDtoEvents = {
        productType: ProductType.EVENT, //enum
        planType: PlanType.ATTENDEE_CUSTOM, //enum
        billingPeriodUnit: BillngPeriodEnum.MONTHLY,
        eventLicenseDetailsDto: defaultEventLicenseDetailsDto,
      };
    });

    await test.step("Set default license details - webinar.", async () => {
      defaultLicenseDetailDtoWebinar = {
        productType: ProductType.WEBINAR, //enum
        planType: PlanType.CUSTOM, //enum
        billingPeriodUnit: BillngPeriodEnum.MONTHLY,
        webinarLicenseDetailsDto: defaultWebinarLicenseDetailsDto,
      };
    });

    await test.step("Set default license details - webinar.", async () => {
      createOrganizationDetailsData = {
        firstName: "PrateeekAutomation",
        lastName: "OwnerQA",
        email: adminEmail,
        organizationName: organisationName,
        licenseDetailsDto: defaultLicenseDetailDtoEvents,
      };
    });

    await test.step(`Creating new organisation having EVENT Plan with name : ${organisationName} and owner email : ${adminEmail}`, async () => {
      organisationController = new OrganisationController(adminApiContext);
      const createOrgApiResp =
        await organisationController.createNewOrganization(
          createOrganizationDetailsData
        );
      const createOrgApiRespJson = await createOrgApiResp.json();
      organisationId = createOrgApiRespJson["id"];
      console.log(
        `New organisation is created witd org id : ${organisationId}`
      );
      teamController = new TeamController(adminApiContext, organisationId);
    });

    await test.step(`OTP verificaiton.`, async () => {
      const otpVerificationIdForAdmin = await userVerifyTeamInvitationUsingOtp(
        adminPage.request,
        adminEmail
      );

      console.log(`admin otp verification id is ${otpVerificationIdForAdmin}`);

      //fetch otp for admin
      const otpCodeForAdmin = await QueryUtil.fetchLoginOTPByVerificationId(
        adminEmail,
        otpVerificationIdForAdmin
      );
      //verify otp for admin
      console.log(`fetched otp for admin  is ${otpCodeForAdmin}`);
      await userVerifyOtpByApi(
        adminPage.request,
        otpVerificationIdForAdmin,
        otpCodeForAdmin
      );
    });
  });

  test.beforeEach(async () => {
    await test.step("Create an event.", async () => {
      eventId = await EventController.generateNewEvent(adminApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
      });
    });

    await test.step("Initialize event controller.", async () => {
      eventController = new EventController(adminApiContext, eventId);
    });

    await test.step("Enable magic link for Speaker and Attendee.", async () => {
      await updateEventLandingPageDetails(adminApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isSpeakermagicLinkEnabled: true,
        isMagicLinkEnabled: true,
      });
    });
  });

  test.afterEach(async () => {});

  test.afterAll(async ({ browser }) => {
    await BrowserFactory.closeBrowserContext(browser);
  });

  test("TC001 Upgrading ATTENDEE role to ORGANISER role in Teams", async ({
    browser,
  }) => {
    //team organiser context and page
    let teamMemberContext = await browser.newContext();
    let teamMemberPage = await teamMemberContext.newPage();

    // attendee context and page
    let attendeeContext = await browser.newContext();
    let attendeePage = await attendeeContext.newPage();

    //attendee email
    let userEmail: string;

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-226/upgrade-attendee-to-organiser-sanity",
    });

    await test.step(`Organiser adds a new attendee to this event by using API`, async () => {
      userEmail = DataUtl.getRandomAttendeeEmail();
      await eventController.inviteAttendeeToTheEvent(userEmail);
    });

    await test.step(`Attendee gets the magic link and login to the event so that his event role gets created`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        userEmail
      );
      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.click("text=Enter Now");
    });

    await test.step(`Run Validations to verify user has assigned ATTENDEE role`, async () => {
      // event role check
      await test.step(`verify event role table to have this user role as ATTENDEE`, async () => {
        const userEventRoleDataBeforeUpgrade =
          await QueryUtil.fetchUserEventRoleData(eventId, userEmail);
        const userEventRoleBeforeUpgrade =
          userEventRoleDataBeforeUpgrade["role"];
        console.log(`event role fetched ${userEventRoleBeforeUpgrade}`);
        expect(
          userEventRoleBeforeUpgrade,
          `expecting user role to be ATTENDEE ${eventId}`
        ).toBe("ATTENDEE");
      });

      //access group check
      await test.step(`verify user access group assigned is of ATTENDEE`, async () => {
        const attendee_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Attendees"
          );
        const thisUserAccessGroupId =
          await QueryUtil.fetchUserAccessGroupIdForEvent(userEmail, eventId);
        expect(
          thisUserAccessGroupId,
          "expecting user access group to be ATTENDEE access group in DB"
        ).toBe(attendee_access_group_id);
      });

      //event registration table check
      await test.step(`verify user has entry in event registration table`, async () => {
        const isEntryPresentInEventRegistrationTable =
          await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
            userEmail,
            eventId
          );
        expect(
          isEntryPresentInEventRegistrationTable,
          "expecting user email to be present in event registrable table for given event"
        ).toBe(true);
      });
    });

    await test.step(`Organiser adds the attendee email to his team with organiser role for event product`, async () => {
      // teamController = new TeamController(
      //   adminPage.request,
      //   organisationIdForTeamAutomation
      // );
      await test.step(`Fetch default/general Team Id`, async () => {
        generalTeamId = await teamController.fetchGeneralTeamId();
      });
      //add a new member to team with organiser role to events
      const memberToAddDetails = {
        firstName: "Prateek",
        lastName: "Admin",
        email: userEmail,
        productRole: MemberRolesEnum.ORGANIZER,
        myTeamList: [{ teamId: generalTeamId }],
      };
      await organisationController.addNewMemberToOrganisation(
        memberToAddDetails
      );
    });

    await test.step(`Now added member will login to setup.zuddl via otp to verify his invitation`, async () => {
      //verify the event role for the person is of organiser
      //user verifies his invitation by authenticating using otp
      const otpVerificationIdForOrganiser =
        await userVerifyTeamInvitationUsingOtp(
          teamMemberPage.request,
          userEmail
        );
      //fetch otp by verification id
      const otpCodeForOrganiser = await QueryUtil.fetchLoginOTPByVerificationId(
        userEmail,
        otpVerificationIdForOrganiser
      );
      console.log(`fetched otp for organiser is ${otpCodeForOrganiser}`);
      //now organiser verify his otp
      await userVerifyOtpByApi(
        teamMemberPage.request,
        otpVerificationIdForOrganiser,
        otpCodeForOrganiser
      );
    });

    await test.step(`Verify that the added team member user role is ORGANIZER for the event created`, async () => {
      await test.step(`verify event role table to have ORGANIZER role for this user for this event`, async () => {
        //now fetch this user event role data before downgrade
        const userEventRoleDataAfterUpgrade =
          await QueryUtil.fetchUserEventRoleData(eventId, userEmail);
        const userEventRoleAfterUpgrade = userEventRoleDataAfterUpgrade["role"];
        console.log(
          `event role fetched after upgrade is ${userEventRoleAfterUpgrade}`
        );
        expect(
          userEventRoleAfterUpgrade,
          "expecting user role to be ORGANIZER"
        ).toBe("ORGANIZER");
      });

      await test.step(`verify the ACCESS GROUP ROLE to be of ORGANIZER for this event`, async () => {
        const organiszer_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Organizers"
          );
        const thisUserAccessGroupId =
          await QueryUtil.fetchUserAccessGroupIdForEvent(userEmail, eventId);
        expect(
          thisUserAccessGroupId,
          "expecting user access group to be ORGANIZER access group in DB"
        ).toBe(organiszer_access_group_id);
      });

      //event registration table check
      // await test.step(`verify user has no entry in event registration table`, async () => {
      //   const isEntryNotPresentInEventRegistrationTable =
      //     await QueryUtil.verifyIfUserEmailIsNotActiveInEventRegTableForGivenEvent(
      //       userEmail,
      //       eventId
      //     );
      //   expect(
      //     isEntryNotPresentInEventRegistrationTable,
      //     "expecting user email to be not present in event registrable table for given event"
      //   ).toBe(false);
      // });
    });
  });

  test("TC002 Upgrading ATTENDEE role to MODERATOR role in Teams", async ({
    browser,
  }) => {
    //team organiser context and page
    let teamMemberContext = await browser.newContext();
    let teamMemberPage = await teamMemberContext.newPage();

    // attendee context and page
    let attendeeContext = await browser.newContext();
    let attendeePage = await attendeeContext.newPage();

    //attendee email
    let userEmail: string;

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-229/upgrade-attendee-to-moderator",
    });

    await test.step(`Organiser adds a new attendee to this event by using API`, async () => {
      userEmail = DataUtl.getRandomAttendeeEmail();
      await eventController.inviteAttendeeToTheEvent(userEmail);
    });

    await test.step(`Attendee gets the magic link and login to the event so that his event role gets created`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        userEmail
      );
      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.click("text=Enter Now");
    });

    await test.step(`Verify user event role of the user is set to ATTENDEE for this event`, async () => {
      const userEventRoleDataBeforeUpgrade =
        await QueryUtil.fetchUserEventRoleData(eventId, userEmail);
      const userEventRoleBeforeUpgrade = userEventRoleDataBeforeUpgrade["role"];
      console.log(`event role fetched ${userEventRoleBeforeUpgrade}`);
      expect(
        userEventRoleBeforeUpgrade,
        `expecting user role to be ATTENDEE ${eventId}`
      ).toBe("ATTENDEE");
    });

    await test.step(`Organiser adds the attendee email to his team with moderator role for event product`, async () => {
      // teamController = new TeamController(
      //   adminPage.request,
      //   organisationIdForTeamAutomation
      // );
      await test.step(`Fetch default/general Team Id`, async () => {
        generalTeamId = await teamController.fetchGeneralTeamId();
      });
      //add a new member to team with organiser role to events
      const memberToAddDetails = {
        firstName: "prateekOrganiser",
        lastName: "automation",
        email: userEmail,
        productRole: MemberRolesEnum.MODERATOR,
        myTeamList: [{ teamId: generalTeamId }],
      };
      //   await organisationController.addNewMemberToOrganisation({
      //     firstName: "prateekOrganiser",
      //     lastName: "automation",
      //     email: userEmail,
      //     productRole: ProductRoleType.MODERATOR,
      //   });
      // });
      await organisationController.addNewMemberToOrganisation(
        memberToAddDetails
      );

      await test.step(`Now added member will login to setup.zuddl via otp to verify his invitation`, async () => {
        //verify the event role for the person is of organiser
        //user verifies his invitation by authenticating using otp
        const otpVerificationIdForOrganiser =
          await userVerifyTeamInvitationUsingOtp(
            teamMemberPage.request,
            userEmail
          );
        //fetch otp by verification id
        const otpCodeForOrganiser =
          await QueryUtil.fetchLoginOTPByVerificationId(
            userEmail,
            otpVerificationIdForOrganiser
          );
        console.log(`fetched otp for organiser is ${otpCodeForOrganiser}`);
        //now organiser verify his otp
        await userVerifyOtpByApi(
          teamMemberPage.request,
          otpVerificationIdForOrganiser,
          otpCodeForOrganiser
        );
      });

      await test.step(`Verify that the added team member user role is MODERATOR for the event created`, async () => {
        await test.step(`verify event role table to have MODERATOR role for this user for this event`, async () => {
          //now fetch this user event role data before downgrade
          const userEventRoleDataAfterUpgrade =
            await QueryUtil.fetchUserEventRoleData(eventId, userEmail);
          const userEventRoleAfterUpgrade =
            userEventRoleDataAfterUpgrade["role"];
          console.log(
            `event role fetched before downgrade is ${userEventRoleAfterUpgrade}`
          );
          expect(
            userEventRoleAfterUpgrade,
            "expecting user role to be MODERATOR"
          ).toBe("MODERATOR");
        });

        await test.step(`verify the ACCESS GROUP ROLE to be of MODERATOR for this event`, async () => {
          const moderator_access_group_id =
            await eventController.getAccessGroupController.fetchAccessGroupId(
              "Moderators"
            );
          const thisUserAccessGroupId =
            await QueryUtil.fetchUserAccessGroupIdForEvent(userEmail, eventId);
          expect(
            thisUserAccessGroupId,
            "expecting user access group to be MODERATOR access group in DB"
          ).toBe(moderator_access_group_id);
        });

        //event registration table check
        // await test.step(`verify user has no entry in event registration table`, async () => {
        //   const isEntryNotPresentInEventRegistrationTable =
        //     await QueryUtil.verifyIfUserEmailIsNotActiveInEventRegTableForGivenEvent(
        //       userEmail,
        //       eventId
        //     );
        //   expect(
        //     isEntryNotPresentInEventRegistrationTable,
        //     "expecting user email to be not present in event registrable table for given event"
        //   ).toBe(false);
        // });
      });
    });
  });

  test("TC003 Upgrading SPEAKER role to ORGANIZER role in Teams", async ({
    browser,
  }) => {
    test.fail(
      true,
      "Known Issue: https://linear.app/zuddl/issue/BUG-1814/if-a-speaker-is-upgraded-to-organizermoderator-by-adding-him-into"
    );
    //team organiser context and page
    let teamMemberContext = await browser.newContext();
    let teamMemberPage = await teamMemberContext.newPage();

    //attendee email
    let userEmail: string;

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-227/upgrade-speaker-to-organiser-sanity",
    });
    test.info().annotations.push({
      type: "known bug",
      description:
        "https://linear.app/zuddl/issue/BUG-1814/if-a-speaker-is-upgraded-to-organizermoderator-by-adding-him-into",
    });

    await test.step(`Organiser adds a new SPEAKER to this event by using API`, async () => {
      userEmail = DataUtl.getRandomSpeakerEmail();
      await eventController.inviteSpeakerToTheEventByApi(
        userEmail,
        "PrateekSpeaker",
        "Automation"
      );
    });

    await test.step(`Run Validations to verify user role  is SPEAKER`, async () => {
      // event role check
      await test.step(`verify event role table to have this user role as SPEAKER`, async () => {
        const userEventRoleDataAfterUpgrade =
          await QueryUtil.fetchUserEventRoleData(eventId, userEmail);
        const userEventRoleAfterUpgrade = userEventRoleDataAfterUpgrade["role"];
        console.log(`event role fetched ${userEventRoleAfterUpgrade}`);
        expect(
          userEventRoleAfterUpgrade,
          `expecting user role to be SPEAKER ${eventId}`
        ).toBe("SPEAKER");
      });

      //access group check
      await test.step(`verify user access group assigned is of SPEAKER`, async () => {
        const speaker_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Speakers"
          );
        const thisUserAccessGroupId =
          await QueryUtil.fetchUserAccessGroupIdForEvent(userEmail, eventId);
        expect(
          thisUserAccessGroupId,
          "expecting user access group to be SPEAKER access group in DB"
        ).toBe(speaker_access_group_id);
      });
      //speaker table check
      await test.step(`verify user email has entry in speaker table for given event`, async () => {
        const isEntryPresentInSpeakerTable =
          await QueryUtil.verifyUserEmailIsPresentInSpeakerTableForAnEvent(
            userEmail,
            eventId
          );

        expect(
          isEntryPresentInSpeakerTable,
          "expecting user email to be present in speaker table for given event"
        ).toBe(true);
      });
    });

    await test.step(`Organiser adds the SPEAEKR email to his team with organiser role for event product`, async () => {
      // teamController = new TeamController(
      //   adminPage.request,
      //   organisationIdForTeamAutomation
      // );
      //add a new member to team with organiser role to events
      // await teamController.addNewMemberToOrganisation({
      //   firstName: "prateekOrganiser",
      //   lastName: "automation",
      //   email: userEmail,
      //   productRole: ProductRoleType.ORGANIZER,
      // });
      await test.step(`Fetch default/general Team Id`, async () => {
        generalTeamId = await teamController.fetchGeneralTeamId();
      });
      //add a new member to team with organiser role to events
      const memberToAddDetails = {
        firstName: "prateekOrganiser",
        lastName: "automation",
        email: userEmail,
        productRole: MemberRolesEnum.ORGANIZER,
        myTeamList: [{ teamId: generalTeamId }],
      };
    });

    await test.step(`Now added member will login to setup.zuddl via otp to verify his invitation`, async () => {
      //verify the event role for the person is of organiser
      //user verifies his invitation by authenticating using otp
      const otpVerificationIdForOrganiser =
        await userVerifyTeamInvitationUsingOtp(
          teamMemberPage.request,
          userEmail
        );
      //fetch otp by verification id
      const otpCodeForOrganiser = await QueryUtil.fetchLoginOTPByVerificationId(
        userEmail,
        otpVerificationIdForOrganiser
      );
      console.log(`fetched otp for organiser is ${otpCodeForOrganiser}`);
      //now organiser verify his otp
      await userVerifyOtpByApi(
        teamMemberPage.request,
        otpVerificationIdForOrganiser,
        otpCodeForOrganiser
      );
    });

    await test.step(`Verify that the added team member user role is ORGANIZER for the event created`, async () => {
      await test.step(`verify event role table to have ORGANIZER role for this user for this event`, async () => {
        //now fetch this user event role data before downgrade
        const userEventRoleDataAfterUpgrade =
          await QueryUtil.fetchUserEventRoleData(eventId, userEmail);
        const userEventRoleAfterUpgrade = userEventRoleDataAfterUpgrade["role"];
        console.log(
          `event role fetched after upgrade is ${userEventRoleAfterUpgrade}`
        );
        expect(
          userEventRoleAfterUpgrade,
          "expecting user role to be ORGANIZER"
        ).toBe("ORGANIZER");
      });

      await test.step(`verify the ACCESS GROUP ROLE to be of ORGANIZER for this event`, async () => {
        const organiszer_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Organizers"
          );
        const thisUserAccessGroupId =
          await QueryUtil.fetchUserAccessGroupIdForEvent(userEmail, eventId);
        expect(
          thisUserAccessGroupId,
          "expecting user access group to be ORGANIZER access group in DB"
        ).toBe(organiszer_access_group_id);
      });

      //speaker table check
      await test.step(`verify user email has no entry in speaker table for given event`, async () => {
        const isEntryNotPresentInSpeakerTable =
          await QueryUtil.verifyUserEmailIsNotPresentInSpeakerTableForAnEvent(
            userEmail,
            eventId
          );

        expect(
          isEntryNotPresentInSpeakerTable,
          "expecting user email to not be present in speaker table for given event"
        ).toBe(true);
      });
    });
  });

  test("TC004 Upgrading SPEAKER role to MODERATOR role in Teams", async ({
    browser,
  }) => {
    test.fail(
      true,
      "Known Issue: https://linear.app/zuddl/issue/BUG-1814/if-a-speaker-is-upgraded-to-organizermoderator-by-adding-him-into"
    );
    //team organiser context and page
    let teamMemberContext = await browser.newContext();
    let teamMemberPage = await teamMemberContext.newPage();

    //attendee email
    let userEmail: string;
    let teamController: TeamController;

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-234/upgrade-speaker-to-moderator",
    });
    test.info().annotations.push({
      type: "known bug",
      description:
        "https://linear.app/zuddl/issue/BUG-1814/if-a-speaker-is-upgraded-to-organizermoderator-by-adding-him-into",
    });

    await test.step(`Organiser adds a new SPEAKER to this event by using API`, async () => {
      userEmail = DataUtl.getRandomSpeakerEmail();
      await eventController.inviteSpeakerToTheEventByApi(
        userEmail,
        "PrateekSpeaker",
        "Automation"
      );
    });

    await test.step(`Run Validations to verify user role  is SPEAKER`, async () => {
      // event role check
      await test.step(`verify event role table to have this user role as SPEAKER`, async () => {
        const userEventRoleDataAfterUpgrade =
          await QueryUtil.fetchUserEventRoleData(eventId, userEmail);
        const userEventRoleAfterUpgrade = userEventRoleDataAfterUpgrade["role"];
        console.log(`event role fetched ${userEventRoleAfterUpgrade}`);
        expect(
          userEventRoleAfterUpgrade,
          `expecting user role to be SPEAKER ${eventId}`
        ).toBe("SPEAKER");
      });

      //access group check
      await test.step(`verify user access group assigned is of SPEAKER`, async () => {
        const speaker_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Speakers"
          );
        const thisUserAccessGroupId =
          await QueryUtil.fetchUserAccessGroupIdForEvent(userEmail, eventId);
        expect(
          thisUserAccessGroupId,
          "expecting user access group to be SPEAKER access group in DB"
        ).toBe(speaker_access_group_id);
      });
      //speaker table check
      await test.step(`verify user email has entry in speaker table for given event`, async () => {
        const isEntryPresentInSpeakerTable =
          await QueryUtil.verifyUserEmailIsPresentInSpeakerTableForAnEvent(
            userEmail,
            eventId
          );

        expect(
          isEntryPresentInSpeakerTable,
          "expecting user email to be present in speaker table for given event"
        ).toBe(true);
      });
    });

    await test.step(`Organiser adds the SPEAEKR email to his team with organiser role for event product`, async () => {
      // teamController = new TeamController(
      //   adminPage.request,
      //   organisationIdForTeamAutomation
      // );
      //add a new member to team with organiser role to events
      await test.step(`Fetch default/general Team Id`, async () => {
        generalTeamId = await teamController.fetchGeneralTeamId();
      });
      //add a new member to team with organiser role to events
      const memberToAddDetails = {
        firstName: "prateekOrganiser",
        lastName: "automation",
        email: userEmail,
        productRole: MemberRolesEnum.MODERATOR,
        myTeamList: [{ teamId: generalTeamId }],
      };
      await organisationController.addNewMemberToOrganisation(
        memberToAddDetails
      );
    });

    await test.step(`Now added member will login to setup.zuddl via otp to verify his invitation`, async () => {
      //verify the event role for the person is of organiser
      //user verifies his invitation by authenticating using otp
      const otpVerificationIdForOrganiser =
        await userVerifyTeamInvitationUsingOtp(
          teamMemberPage.request,
          userEmail
        );
      //fetch otp by verification id
      const otpCodeForOrganiser = await QueryUtil.fetchLoginOTPByVerificationId(
        userEmail,
        otpVerificationIdForOrganiser
      );
      console.log(`fetched otp for organiser is ${otpCodeForOrganiser}`);
      //now organiser verify his otp
      await userVerifyOtpByApi(
        teamMemberPage.request,
        otpVerificationIdForOrganiser,
        otpCodeForOrganiser
      );
    });

    await test.step(`Verify that the added team member user role is MODERATOR for the event created`, async () => {
      await test.step(`verify event role table to have MODERATOR role for this user for this event`, async () => {
        //now fetch this user event role data before downgrade
        const userEventRoleDataAfterUpgrade =
          await QueryUtil.fetchUserEventRoleData(eventId, userEmail);
        const userEventRoleAfterUpgrade = userEventRoleDataAfterUpgrade["role"];
        console.log(
          `event role fetched before downgrade is ${userEventRoleAfterUpgrade}`
        );
        expect(
          userEventRoleAfterUpgrade,
          "expecting user role to be MODERATOR"
        ).toBe("MODERATOR");
      });

      await test.step(`verify the ACCESS GROUP ROLE to be of MODERATOR for this event`, async () => {
        const moderator_access_group_id =
          await eventController.getAccessGroupController.fetchAccessGroupId(
            "Moderators"
          );
        const thisUserAccessGroupId =
          await QueryUtil.fetchUserAccessGroupIdForEvent(userEmail, eventId);
        expect(
          thisUserAccessGroupId,
          "expecting user access group to be MODERATOR access group in DB"
        ).toBe(moderator_access_group_id);
      });

      //speaker table check
      await test.step(`verify user email has no entry in speaker table for given event`, async () => {
        const isEntryNotPresentInSpeakerTable =
          await QueryUtil.verifyUserEmailIsNotPresentInSpeakerTableForAnEvent(
            userEmail,
            eventId
          );

        expect(
          isEntryNotPresentInSpeakerTable,
          "expecting user email to not be present in speaker table for given event"
        ).toBe(true);
      });
    });
  });
});
