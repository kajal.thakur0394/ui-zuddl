import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { AVModal } from "../../../page-objects/events-pages/live-side-components/AvModal";
import { RoomModule } from "../../../page-objects/events-pages/site-modules/Rooms";
import {
  createNewEvent,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { ScreenSharingOptions } from "../../../enums/screensharingOptionEnum";
import VenueOptions from "../../../enums/venueOptionsEnums";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import { GreenRoom } from "../../../page-objects/events-pages/site-modules/GreenRoom";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel(
  `@rooms @public @legacyBackstage @screenshare @AV`,
  {
    tag: "@smoke",
  },
  async () => {
    let eventId;
    let orgBrowserContext: BrowserContext;
    let orgApiContext: APIRequestContext;
    let attendeeOneBrowserContext: BrowserContext;
    let attendeeTwoBrowserContext: BrowserContext;
    let eventController: EventController;
    let roomListingPage: string;
    let attendeeLandingPage: string;
    let speakerLandingPage: string;
    let speakerOneBrowserContext: BrowserContext;
    let speakerTwoBrowserContext: BrowserContext;

    test.beforeEach(async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({});
      orgApiContext = orgBrowserContext.request;
      eventId = await createNewEvent({
        api_request_context: orgApiContext,
        event_title: "room automation",
      });
      attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });

      attendeeTwoBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });

      speakerOneBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });

      speakerTwoBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });

      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
      });

      eventController = new EventController(orgApiContext, eventId);
      //add rooms
      await eventController.getRoomsController.addRoomsToEvent({
        seatsPerRoom: 10,
        numberOfRooms: 1,
        roomCategory: "PUBLIC",
      });

      let stageId = await eventController.getDefaultStageId();
      await eventController.disableStudioAsBackstage(stageId);

      let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
      attendeeLandingPage = eventInfoDto.attendeeLandingPage;
      speakerLandingPage = eventInfoDto.speakerLandingPage;
      roomListingPage = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/l/event/${eventId}/discussions`;
    });

    test.afterEach(async () => {
      await orgBrowserContext?.close();
      await attendeeOneBrowserContext?.close();
      await attendeeTwoBrowserContext?.close();
      await speakerOneBrowserContext?.close();
      await speakerTwoBrowserContext?.close();
    });

    test("TC001: @screenshare Verify attendee and organiser are able to share their screen when inside the room", async () => {
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-462/attendee-and-organiser-are-able-to-share-their-entire-screen",
      });
      let attendeeOnePage = await attendeeOneBrowserContext.newPage();
      let attendeeTwoPage = await attendeeTwoBrowserContext.newPage();
      let organiserPage = await orgBrowserContext.newPage();
      let attendeeOneRoom: RoomModule;
      let attendeeTwoRoom: RoomModule;
      let organiserRoom: RoomModule;

      const attendeeOneFirstName = "prateekAttendeeOne";
      const attendeeOneLastName = "QA";

      const attendeeTwoFirstName = "prateekAttendeeTwo";
      const attendeeTwoLastName = "QA";

      const organiserFirstName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
      const organiserLastName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

      let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
      let attendeeEmailTwo = DataUtl.getRandomAttendeeEmail();

      await test.step(`organiser invites attendee  1 to the event`, async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeOneFirstName,
          lastName: attendeeOneLastName,
          phoneNumber: "1234567899",
          email: attendeeEmailOne,
        });
      });

      await test.step(`organiser invites attendee  2 to the event`, async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeTwoFirstName,
          lastName: attendeeTwoLastName,
          phoneNumber: "1234567899",
          email: attendeeEmailTwo,
        });
      });

      await eventController.disableOnboardingChecksForAttendee();
      await test.step(`attendee 1 logs into the room`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeEmailOne
        );
        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.waitForURL(/lobby/);
        await attendeeOnePage.goto(roomListingPage);

        let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
        await roomone_locator.click();

        const attendeeOneAvModal = new AVModal(attendeeOnePage);
        await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
        await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
      });

      await test.step(`attendee 2 logs into the room`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeEmailTwo
        );
        await attendeeTwoPage.goto(magicLink);
        await attendeeTwoPage.click(`text=Enter Now`);
        await attendeeTwoPage.waitForURL(/lobby/);
        await attendeeTwoPage.goto(roomListingPage);
        let roomone_locator = attendeeTwoPage.locator("text=Enter").nth(0);
        await roomone_locator.click();

        const attendeeTwoAvModal = new AVModal(attendeeTwoPage);
        await attendeeTwoAvModal.isVideoStreamLoadedOnAVModal();
        await attendeeTwoAvModal.clickOnJoinButtonOnAvModal();
      });
      await test.step(`organiser logs into the room`, async () => {
        await organiserPage.goto(attendeeLandingPage);
        await organiserPage.click(`text=Enter Now`);
        await organiserPage.goto(roomListingPage);

        let roomone_locator = organiserPage.locator("text=Enter").nth(0);
        await roomone_locator.click();

        const organiserAvModal = new AVModal(organiserPage);
        await organiserAvModal.isVideoStreamLoadedOnAVModal();
        await organiserPage.waitForTimeout(2000);
        await organiserAvModal.clickOnJoinButtonOnAvModal();
      });

      // presence and visibility of AV streams
      await test.step(`verify attendee 1 sees attendee 2 and organiser stream inside room`, async () => {
        attendeeOneRoom = new RoomModule(attendeeOnePage);
        await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await attendeeOneRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
        await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify attendee 2 sees attendee 1 and organiser stream inside room`, async () => {
        attendeeTwoRoom = new RoomModule(attendeeTwoPage);
        await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
        await attendeeTwoRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify organiser 1 sees attendee 1 and attendee 2 stream inside room`, async () => {
        organiserRoom = new RoomModule(organiserPage);
        await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await organiserRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
        await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
      });

      //verify every one is able to turn off their audio and video
      await test.step(`attendee one turns off the video and audio`, async () => {
        // when someone is already joined, the attendee who is joining will have its audio muted
        await expect(
          attendeeOneRoom.getStreamOptionsComponent
            .unmuteaudioButtonForMyStream,
          "expecting unmute button is visible to attendee one as his audio is already muted"
        ).toBeVisible();
        //now mute the video
        await test.step(`attendee 1 muting the video`, async () => {
          await attendeeOneRoom.getStreamOptionsComponent.muteMyVideo();
        });
      });
      await test.step(`attendee two turns off the video and audio`, async () => {
        // when someone is already joined, the attendee who is joining will have its audio muted
        await expect(
          attendeeTwoRoom.getStreamOptionsComponent
            .unmuteaudioButtonForMyStream,
          "expecting unmute button is visible to attendee one as his audio is already muted"
        ).toBeVisible();
        //now mute the video
        await test.step(`attendee 2 muting the video`, async () => {
          await attendeeTwoRoom.getStreamOptionsComponent.muteMyVideo();
        });
      });
      await test.step(`organiser turns off the video and audio`, async () => {
        await organiserRoom.getStreamOptionsComponent.muteMyAudio();
        await organiserRoom.getStreamOptionsComponent.muteMyVideo();
      });

      // presence and visibility of AV streams after every one has muted their audio and video
      await test.step(`verify attendee 1 sees attendee 2 and organiser stream inside room`, async () => {
        await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await attendeeOneRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
        await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify attendee 2 sees attendee 1 and organiser stream inside room`, async () => {
        await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
        await attendeeTwoRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify organiser 1 sees attendee 1 and attendee 2 stream inside room`, async () => {
        await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await organiserRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
        await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
      });

      await test.step(`attendee 1 starts screensharing his entire screen while being present inside the room`, async () => {
        await test.step("attendee  1 clicks on screenshare and choose the right option", async () => {
          await attendeeOneRoom.getStreamOptionsComponent.clickOnScreenShareButton();
          await attendeeOneRoom.getStreamOptionsComponent.selectScreenSharingOption(
            ScreenSharingOptions.YOUR_SCREEN,
            VenueOptions.LEGACYBACKSTAGE
          );
        });

        await test.step(`verify attendee 1 sees his screen share stream`, async () => {
          await attendeeOneRoom.verifyCountOfStreamInsideRoomMatches(4);
        });
        await test.step(`verify attendee 2 sees attendee 1 screen share stream`, async () => {
          await attendeeTwoRoom.verifyCountOfStreamInsideRoomMatches(4);
        });
        await test.step(`verify organiser  sees attendee 1 screen share stream`, async () => {
          await organiserRoom.verifyCountOfStreamInsideRoomMatches(4);
        });
      });

      await test.step(`organiser  starts screensharing his entire screen while being present inside the room`, async () => {
        await test.step("organiser clicks on screenshare and choose the right option", async () => {
          await organiserRoom.getStreamOptionsComponent.clickOnScreenShareButton();
          await organiserRoom.getStreamOptionsComponent.selectScreenSharingOption(
            ScreenSharingOptions.YOUR_SCREEN,
            VenueOptions.LEGACYBACKSTAGE
          );
        });

        await test.step(`verify attendee 1 sees organiser screen share stream`, async () => {
          await attendeeOneRoom.verifyCountOfStreamInsideRoomMatches(5);
        });
        await test.step(`verify attendee 2 sees organiser screen share stream`, async () => {
          await attendeeTwoRoom.verifyCountOfStreamInsideRoomMatches(5);
        });
        await test.step(`verify organiser  sees his own screen share stream`, async () => {
          await organiserRoom.verifyCountOfStreamInsideRoomMatches(5);
        });
      });

      await test.step(`attendee 1 now stops his screenshare`, async () => {
        await test.step(`attendee  1 clicks on stop screenshare button`, async () => {
          await attendeeOneRoom.getStreamOptionsComponent.clickToStopScreenShare();
        });

        await test.step(`verify attendee 1 now do not see his screenshare stream`, async () => {
          await attendeeOneRoom.verifyCountOfStreamInsideRoomMatches(4);
        });

        await test.step(`verify attendee 2 now do not see attendee 1 screenshare stream`, async () => {
          await attendeeTwoRoom.verifyCountOfStreamInsideRoomMatches(4);
        });

        await test.step(`verify organiser now do not see attendee 1 screenshare stream`, async () => {
          await organiserRoom.verifyCountOfStreamInsideRoomMatches(4);
        });
      });

      await test.step(`organiser 1 now also stops his screenshare`, async () => {
        await test.step(`organiser  1 clicks on stop screenshare button`, async () => {
          await organiserRoom.getStreamOptionsComponent.clickToStopScreenShare();
        });

        await test.step(`verify attendee 1 now do not organsier screenshare stream`, async () => {
          await attendeeOneRoom.verifyCountOfStreamInsideRoomMatches(3);
        });

        await test.step(`verify attendee 2 now do not see organsier screenshare stream`, async () => {
          await attendeeTwoRoom.verifyCountOfStreamInsideRoomMatches(3);
        });

        await test.step(`verify organiser now do not see his screenshare stream`, async () => {
          await organiserRoom.verifyCountOfStreamInsideRoomMatches(3);
        });
      });

      // Now every one exits from the room
      await test.step(`verify attendee 1 is able to exit the room and land on room listing page`, async () => {
        await attendeeOneRoom.clickOnExitRoom();
        await attendeeOneRoom.handleExitConfirmationPopup(true);
      });

      // attendee two exit from the room
      await test.step(`verify attendee 2 is able to exit the room and land on room listing page`, async () => {
        await attendeeTwoRoom.clickOnExitRoom();
        await attendeeTwoRoom.handleExitConfirmationPopup(true);
      });

      // organiser exits from the room
      await test.step(`verify organiser is able to exit the room and land on room listing page`, async () => {
        await organiserRoom.clickOnExitRoom();
        await organiserRoom.handleExitConfirmationPopup(true);
      });
    });

    test("TC002: @screenshare Verify speaker is able to share his screen when inside the room", async () => {
      test.info().annotations.push(
        {
          type: "TC",
          description:
            "https://linear.app/zuddl/issue/QAT-467/verify-speaker-can-screenshare-his-screen-inside-room",
        },
        {
          type: "TC",
          description:
            "https://linear.app/zuddl/issue/QAT-484/verify-when-screenpdfwebsite-is-being-shared-verify-zoom-icon-appears",
        }
      );
      let speakerOnePage = await speakerOneBrowserContext.newPage();
      let attendeeOnePage = await attendeeOneBrowserContext.newPage();
      let organiserPage = await orgBrowserContext.newPage();
      let speakerOneRoom: RoomModule;
      let attendeeOneRoom: RoomModule;
      let organiserRoom: RoomModule;

      const speakerOneFirstName = "namrataSpeakerOne";
      const speakerOneLastName = "QA";

      const attendeeOneFirstName = "namrataAttendeeOne";
      const attendeeOneLastName = "QA";

      const organiserFirstName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
      const organiserLastName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

      let speakerEmailOne = DataUtl.getRandomAdminEmail();
      let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();

      await test.step(`organiser invites speaker 1 to the event`, async () => {
        await eventController.inviteSpeakerToTheEventByApi(
          speakerEmailOne,
          speakerOneFirstName,
          speakerOneLastName
        );
      });

      await test.step(`organiser invites attende 1 to the event`, async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeOneFirstName,
          lastName: attendeeOneLastName,
          phoneNumber: "1234567899",
          email: attendeeEmailOne,
        });
      });

      await eventController.disableOnboardingChecksForSpeaker();

      await test.step(`speaker 1 logs into the room`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          eventId,
          speakerEmailOne
        );
        await speakerOnePage.goto(magicLink);
        await speakerOnePage.click(`text=Enter Now`);
        await speakerOnePage.goto(roomListingPage);

        let roomone_locator = speakerOnePage.locator("text=Enter").nth(0);
        await roomone_locator.click();

        const speakerAvModal = new AVModal(speakerOnePage);
        await speakerAvModal.isVideoStreamLoadedOnAVModal();
        await speakerAvModal.clickOnJoinButtonOnAvModal();
      });

      await test.step(`attende 1 logs into the room`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeEmailOne
        );
        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(roomListingPage);

        let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
        await roomone_locator.click();

        const speakerAvModal = new AVModal(attendeeOnePage);
        await speakerAvModal.isVideoStreamLoadedOnAVModal();
        await speakerAvModal.clickOnJoinButtonOnAvModal();
      });

      await test.step(`organiser logs into the room`, async () => {
        await organiserPage.goto(speakerLandingPage);
        await organiserPage.click(`text=Enter Now`);
        await organiserPage.goto(roomListingPage);

        let roomone_locator = organiserPage.locator("text=Enter").nth(0);
        await roomone_locator.click();

        const organiserAvModal = new AVModal(organiserPage);
        await organiserAvModal.isVideoStreamLoadedOnAVModal();
        await organiserPage.waitForTimeout(2000);
        await organiserAvModal.clickOnJoinButtonOnAvModal();
      });

      // presence and visibility of AV streams
      await test.step(`verify speaker 1 sees attende 1 and organiser stream inside room`, async () => {
        speakerOneRoom = new RoomModule(speakerOnePage);
        await speakerOneRoom.verifyUserStreamIsVisible(speakerOneFirstName);
        await speakerOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await speakerOneRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify attende 1 sees speaker 1 and organiser stream inside room`, async () => {
        attendeeOneRoom = new RoomModule(attendeeOnePage);
        await attendeeOneRoom.verifyUserStreamIsVisible(speakerOneFirstName);
        await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify organiser 1 sees speaker 1 and attendee 1 stream inside room`, async () => {
        organiserRoom = new RoomModule(organiserPage);
        await organiserRoom.verifyUserStreamIsVisible(speakerOneFirstName);
        await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
      });

      // presence and visibility of AV streams after every one has muted their audio and video
      await test.step(`verify speaker 1 sees attendee 1 and organiser stream inside room`, async () => {
        await speakerOneRoom.verifyUserStreamIsVisible(speakerOneFirstName);
        await speakerOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await speakerOneRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify attendee 1 sees speaker 1 and organiser stream inside room`, async () => {
        await attendeeOneRoom.verifyUserStreamIsVisible(speakerOneFirstName);
        await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify organiser 1 sees speaker 1 and attendee 1 stream inside room`, async () => {
        await organiserRoom.verifyUserStreamIsVisible(speakerOneFirstName);
        await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
      });

      await test.step(`speaker 1 starts screensharing his entire screen while being present inside the room`, async () => {
        await test.step("speaker  1 clicks on screenshare and choose the right option", async () => {
          await speakerOneRoom.getStreamOptionsComponent.clickOnScreenShareButton();
          await speakerOneRoom.getStreamOptionsComponent.selectScreenSharingOption(
            ScreenSharingOptions.YOUR_SCREEN,
            VenueOptions.LEGACYBACKSTAGE
          );
        });

        await test.step(`verify speaker 1 sees his screen share stream`, async () => {
          await speakerOneRoom.verifyCountOfStreamInsideRoomMatches(4);
        });
        await test.step(`verify attendee 1 sees speaker 1 screen share stream`, async () => {
          await attendeeOneRoom.verifyCountOfStreamInsideRoomMatches(4);
        });
        await test.step(`verify organiser  sees attendee 1 screen share stream`, async () => {
          await organiserRoom.verifyCountOfStreamInsideRoomMatches(4);
        });
        await test.step("verify other user can see the zoom button and is able to click it", async () => {
          await attendeeOneRoom.verifyZoomButtonIsActive();
          await organiserRoom.verifyZoomButtonIsActive();
          await organiserPage.keyboard.press("Escape");
          await attendeeOnePage.keyboard.press("Escape");
        });
      });

      await test.step(`organiser  starts screensharing his entire screen while being present inside the room`, async () => {
        await test.step("organiser clicks on screenshare and choose the right option", async () => {
          await organiserRoom.getStreamOptionsComponent.clickOnScreenShareButton();
          await organiserRoom.getStreamOptionsComponent.selectScreenSharingOption(
            ScreenSharingOptions.YOUR_SCREEN,
            VenueOptions.LEGACYBACKSTAGE
          );
        });

        await test.step(`verify speaker 1 sees organiser screen share stream`, async () => {
          await speakerOneRoom.verifyCountOfStreamInsideRoomMatches(5);
        });
        await test.step(`verify attendee 1 sees organiser screen share stream`, async () => {
          await attendeeOneRoom.verifyCountOfStreamInsideRoomMatches(5);
        });
        await test.step(`verify organiser  sees his own screen share stream`, async () => {
          await organiserRoom.verifyCountOfStreamInsideRoomMatches(5);
        });
      });

      await test.step(`speaker 1 now stops his screenshare`, async () => {
        await test.step(`speaker  1 clicks on stop screenshare button`, async () => {
          await speakerOneRoom.getStreamOptionsComponent.clickToStopScreenShare();
        });

        await test.step(`verify speaker 1 now do not see his screenshare stream`, async () => {
          await speakerOneRoom.verifyCountOfStreamInsideRoomMatches(4);
        });

        await test.step(`verify attendee 1 now do not see speaker 1 screenshare stream`, async () => {
          await attendeeOneRoom.verifyCountOfStreamInsideRoomMatches(4);
        });

        await test.step(`verify organiser now do not see speaker 1 screenshare stream`, async () => {
          await organiserRoom.verifyCountOfStreamInsideRoomMatches(4);
        });
      });

      await test.step(`organiser 1 now also stops his screenshare`, async () => {
        await test.step(`organiser  1 clicks on stop screenshare button`, async () => {
          await organiserRoom.getStreamOptionsComponent.clickToStopScreenShare();
        });

        await test.step(`verify speaker 1 now do not organsier screenshare stream`, async () => {
          await speakerOneRoom.verifyCountOfStreamInsideRoomMatches(3);
        });

        await test.step(`verify attendee 1 now do not see organsier screenshare stream`, async () => {
          await attendeeOneRoom.verifyCountOfStreamInsideRoomMatches(3);
        });

        await test.step(`verify organiser now do not see his screenshare stream`, async () => {
          await organiserRoom.verifyCountOfStreamInsideRoomMatches(3);
        });
      });

      // Now every one exits from the room
      await test.step(`verify speaker 1 is able to exit the room and land on room listing page`, async () => {
        await speakerOneRoom.clickOnExitRoom();
        await speakerOneRoom.handleExitConfirmationPopup(true);
      });

      // speaker two exit from the room
      await test.step(`verify attendee 1 is able to exit the room and land on room listing page`, async () => {
        await attendeeOneRoom.clickOnExitRoom();
        await attendeeOneRoom.handleExitConfirmationPopup(true);
      });

      // organiser exits from the room
      await test.step(`verify organiser is able to exit the room and land on room listing page`, async () => {
        await organiserRoom.clickOnExitRoom();
        await organiserRoom.handleExitConfirmationPopup(true);
      });
    });

    test("TC003: Verify if organiser chooses to disable screen sharing then verify other user. || @screenSharing", async () => {
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-485/if-organiser-chooses-to-disable-screen-sharing-then-verify-other-user",
      });
      let speakerOnePage = await speakerOneBrowserContext.newPage();
      let attendeeOnePage = await attendeeOneBrowserContext.newPage();
      let organiserPage = await orgBrowserContext.newPage();
      let speakerOneRoom: RoomModule;
      let attendeeOneRoom: RoomModule;
      let organiserRoom: RoomModule;

      const speakerOneFirstName = "namrataSpeakerOne";
      const speakerOneLastName = "QA";

      const attendeeOneFirstName = "namrataAttendeeOne";
      const attendeeOneLastName = "QA";

      const organiserFirstName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

      let speakerEmailOne = DataUtl.getRandomAdminEmail();
      let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();

      await test.step(`organiser invites speaker 1 to the event`, async () => {
        await eventController.inviteSpeakerToTheEventByApi(
          speakerEmailOne,
          speakerOneFirstName,
          speakerOneLastName
        );
      });

      await test.step(`organiser invites attende 1 to the event`, async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeOneFirstName,
          lastName: attendeeOneLastName,
          phoneNumber: "1234567899",
          email: attendeeEmailOne,
        });
      });

      await eventController.disableOnboardingChecksForSpeaker();

      await test.step(`speaker 1 logs into the room`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          eventId,
          speakerEmailOne
        );
        await speakerOnePage.goto(magicLink);
        await speakerOnePage.click(`text=Enter Now`);
        await speakerOnePage.goto(roomListingPage);

        let roomone_locator = speakerOnePage.locator("text=Enter").nth(0);
        await roomone_locator.click();

        const speakerAvModal = new AVModal(speakerOnePage);
        await speakerAvModal.isVideoStreamLoadedOnAVModal();
        await speakerAvModal.clickOnJoinButtonOnAvModal();
      });

      await test.step(`attende 1 logs into the room`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeEmailOne
        );
        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(roomListingPage);

        let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
        await roomone_locator.click();

        const speakerAvModal = new AVModal(attendeeOnePage);
        await speakerAvModal.isVideoStreamLoadedOnAVModal();
        await speakerAvModal.clickOnJoinButtonOnAvModal();
      });

      await test.step(`organiser logs into the room`, async () => {
        await organiserPage.goto(speakerLandingPage);
        await organiserPage.click(`text=Enter Now`);
        await organiserPage.goto(roomListingPage);

        let roomone_locator = organiserPage.locator("text=Enter").nth(0);
        await roomone_locator.click();

        const organiserAvModal = new AVModal(organiserPage);
        await organiserAvModal.isVideoStreamLoadedOnAVModal();
        await organiserPage.waitForTimeout(2000);
        await organiserAvModal.clickOnJoinButtonOnAvModal();
      });

      // presence and visibility of AV streams
      await test.step(`verify speaker 1 sees attende 1 and organiser stream inside room`, async () => {
        speakerOneRoom = new RoomModule(speakerOnePage);
        await speakerOneRoom.verifyUserStreamIsVisible(speakerOneFirstName);
        await speakerOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await speakerOneRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify attende 1 sees speaker 1 and organiser stream inside room`, async () => {
        attendeeOneRoom = new RoomModule(attendeeOnePage);
        await attendeeOneRoom.verifyUserStreamIsVisible(speakerOneFirstName);
        await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify organiser 1 sees speaker 1 and attendee 1 stream inside room`, async () => {
        organiserRoom = new RoomModule(organiserPage);
        await organiserRoom.verifyUserStreamIsVisible(speakerOneFirstName);
        await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
      });

      // presence and visibility of AV streams after every one has muted their audio and video
      await test.step(`verify speaker 1 sees attendee 1 and organiser stream inside room`, async () => {
        await speakerOneRoom.verifyUserStreamIsVisible(speakerOneFirstName);
        await speakerOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await speakerOneRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify attendee 1 sees speaker 1 and organiser stream inside room`, async () => {
        await attendeeOneRoom.verifyUserStreamIsVisible(speakerOneFirstName);
        await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify organiser 1 sees speaker 1 and attendee 1 stream inside room`, async () => {
        await organiserRoom.verifyUserStreamIsVisible(speakerOneFirstName);
        await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
      });

      await test.step(`verify speaker and attende able to screenshare while organiser  disabled screensharing`, async () => {
        await organiserRoom.getStreamOptionsComponent.disableScreenShareOption();
        await organiserPage.waitForTimeout(2000);
        await speakerOneRoom.getStreamOptionsComponent.clickOnScreenShareButton();
        await speakerOneRoom.verifyScreenShareDisabled();
        await speakerOneRoom.closeModal();
        await attendeeOneRoom.getStreamOptionsComponent.clickOnScreenShareButton();
        await attendeeOneRoom.verifyScreenShareDisabled();
        await attendeeOneRoom.closeModal();
      });

      // Now every one exits from the room
      await test.step(`verify speaker 1 is able to exit the room and land on room listing page`, async () => {
        await speakerOneRoom.clickOnExitRoom();
        await speakerOneRoom.handleExitConfirmationPopup(true);
      });

      // speaker two exit from the room
      await test.step(`verify attendee 1 is able to exit the room and land on room listing page`, async () => {
        await attendeeOneRoom.clickOnExitRoom();
        await attendeeOneRoom.handleExitConfirmationPopup(true);
      });

      // organiser exits from the room
      await test.step(`verify organiser is able to exit the room and land on room listing page`, async () => {
        await organiserRoom.clickOnExitRoom();
        await organiserRoom.handleExitConfirmationPopup(true);
      });
    });

    test("TC004: Verify Screen share functionality in Legacy-Backstage. || @screenshare @screenSharing", async () => {
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-203",
      });

      let attendeePage = await attendeeOneBrowserContext.newPage();
      let organiserPage = await orgBrowserContext.newPage();

      const attendeeFirstName = "AutomationAttendeeOne";
      const attendeeLastName = "QA";

      const organiserFirstName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

      let attendeeEmail = DataUtl.getRandomAttendeeEmail();

      let stageId = await eventController.getDefaultStageId();
      let stagePageURI = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/l/event/${eventId}/stages/${stageId}`;

      let organiserStagePage: StagePage;
      let attendeeStagePage: StagePage;

      let organiserGreenRoom: GreenRoom;

      await test.step(`Organiser disables onboarding checks and invites Attendee to the event`, async () => {
        await eventController.disableOnboardingChecksForAttendee();

        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          phoneNumber: "1234567899",
          email: attendeeEmail,
        });
      });

      await test.step(`Attendee joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeEmail
        );

        await attendeePage.goto(magicLink);
        await attendeePage.click(`text=Enter Now`);
        await attendeePage.goto(stagePageURI);
        attendeeStagePage = new StagePage(attendeePage);
      });

      await test.step(`Organizer joins the event and join backstage via default stage.`, async () => {
        await organiserPage.goto(attendeeLandingPage);
        await organiserPage.click(`text=Enter Now`);
        await organiserPage.goto(stagePageURI);
        organiserStagePage = new StagePage(organiserPage);
        const orgAvModal =
          await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
        await orgAvModal.isVideoStreamLoadedOnAVModal();
        await orgAvModal.clickOnJoinButtonOnAvModal();

        await test.step(`Verify Organiser's presence in backstage.`, async () => {
          organiserGreenRoom = new GreenRoom(organiserPage);
          await organiserGreenRoom.isGreenRoomVisible();
          await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(1);
        });
      });

      await test.step("Organiser joins the stage.", async () => {
        await organiserGreenRoom.sendUserToOnstageFromBackStageUsingRaiseHand(
          organiserFirstName
        );
      });

      await test.step("Organsier clicks on screenshare and choose the Screen-share option.", async () => {
        await organiserGreenRoom.getStreamOptionsComponent.clickOnScreenShareButton();
        await organiserGreenRoom.getStreamOptionsComponent.selectScreenSharingOption(
          ScreenSharingOptions.YOUR_SCREEN,
          VenueOptions.LEGACYBACKSTAGE
        );
      });

      await test.step(`Organizer initiate session and joins the stage.`, async () => {
        await organiserGreenRoom.disableDryRun();
        await organiserGreenRoom.startTheSession();
      });

      await test.step(`Verify Attendee sees 1 user stream and 1 Screen-sharing`, async () => {
        await attendeeStagePage.getAvComponent.verifyCountOfStreamInsideRoomMatches(
          2
        );
      });

      await test.step(`Organiser clicks on stop screenshare button`, async () => {
        await organiserGreenRoom.getStreamOptionsComponent.clickToStopScreenShare();
      });

      await test.step(`Verify Attendee only see 1 user stream`, async () => {
        await attendeeStagePage.getAvComponent.verifyCountOfStreamInsideRoomMatches(
          1
        );
      });

      await test.step(`Organiser ends the session.`, async () => {
        await organiserGreenRoom.endTheSession();
      });
    });
  }
);
