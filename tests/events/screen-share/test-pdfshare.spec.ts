import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import BrowserFactory from "../../../util/BrowserFactory";
import EventEntryType from "../../../enums/eventEntryEnum";
import { EventController } from "../../../controller/EventController";
import { ScreenSharingOptions } from "../../../enums/screensharingOptionEnum";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import { RoomModule } from "../../../page-objects/events-pages/site-modules/Rooms";
import { GreenRoom } from "../../../page-objects/events-pages/site-modules/GreenRoom";
import { AVModal } from "../../../page-objects/events-pages/live-side-components/AvModal";
import {
  createNewEvent,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import VenueOptions from "../../../enums/venueOptionsEnums";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel(
  `@rooms @public @AV`,
  {
    tag: "@smoke",
  },
  async () => {
    let eventId;
    let orgBrowserContext: BrowserContext;
    let orgApiContext: APIRequestContext;
    let attendeeOneBrowserContext: BrowserContext;
    let attendeeTwoBrowserContext: BrowserContext;
    let eventController: EventController;
    let roomListingPage: string;
    let attendeeLandingPage: string;
    let speakerLandingPage: string;
    let speakerBrowserContext: BrowserContext;

    test.beforeEach(async () => {
      await test.step(`Initialize Organiser Browser context, Organiser API context.`, async () => {
        orgBrowserContext = await BrowserFactory.getBrowserContext({});
        orgApiContext = orgBrowserContext.request;
        eventId = await createNewEvent({
          api_request_context: orgApiContext,
          event_title: "room automation",
        });
      });

      await test.step(`Initialize Ateendee Browser contexts.`, async () => {
        attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
          laodOrganiserCookies: false,
        });

        attendeeTwoBrowserContext = await BrowserFactory.getBrowserContext({
          laodOrganiserCookies: false,
        });
      });

      await test.step(`Initialize Speaker Browser context.`, async () => {
        speakerBrowserContext = await BrowserFactory.getBrowserContext({
          laodOrganiserCookies: false,
        });
      });

      await test.step(`Enable magic link.`, async () => {
        await updateEventLandingPageDetails(orgApiContext, eventId, {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
        });
      });

      await test.step(`Add Rooms.`, async () => {
        eventController = new EventController(orgApiContext, eventId);
        await eventController.getRoomsController.addRoomsToEvent({
          seatsPerRoom: 10,
          numberOfRooms: 1,
          roomCategory: "PUBLIC",
        });
      });

      await test.step(`Get landing pages and room listing page.`, async () => {
        let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);

        attendeeLandingPage = eventInfoDto.attendeeLandingPage;
        speakerLandingPage = eventInfoDto.speakerLandingPage;

        roomListingPage = `${
          DataUtl.getApplicationTestDataObj().baseUrl
        }/l/event/${eventId}/discussions`;
      });
    });

    test.afterEach(async () => {
      await test.step(`Close Contexts.`, async () => {
        await orgBrowserContext?.close();
        await attendeeOneBrowserContext?.close();
        await attendeeTwoBrowserContext?.close();
        await speakerBrowserContext?.close();
      });
    });

    test("TC001: Verify PDF share functionality in Rooms- Organiser is able to share | Attendee is not able to share. || @screenshare @pdfSharing @roomsScreenShare", async () => {
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-465",
      });

      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-466",
      });

      let attendeeOnePage = await attendeeOneBrowserContext.newPage();
      let attendeeTwoPage = await attendeeTwoBrowserContext.newPage();
      let organiserPage = await orgBrowserContext.newPage();

      let attendeeOneRoom: RoomModule;
      let attendeeTwoRoom: RoomModule;
      let organiserRoom: RoomModule;

      const attendeeOneFirstName = "AutomationAttendeeOne";
      const attendeeOneLastName = "QA";

      const attendeeTwoFirstName = "AutomationAttendeeTwo";
      const attendeeTwoLastName = "QA";

      const organiserFirstName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

      let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
      let attendeeEmailTwo = DataUtl.getRandomAttendeeEmail();

      await test.step(`organiser invites attendee  1 to the event`, async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeOneFirstName,
          lastName: attendeeOneLastName,
          phoneNumber: "1234567899",
          email: attendeeEmailOne,
        });
      });

      await test.step(`organiser invites attendee  2 to the event`, async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeTwoFirstName,
          lastName: attendeeTwoLastName,
          phoneNumber: "1234567899",
          email: attendeeEmailTwo,
        });
      });

      await eventController.disableOnboardingChecksForAttendee();
      await test.step(`attendee 1 logs into the room`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeEmailOne
        );
        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.waitForURL(/lobby/);
        await attendeeOnePage.goto(roomListingPage);

        let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
        await roomone_locator.click();

        const attendeeOneAvModal = new AVModal(attendeeOnePage);
        await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
        await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
      });

      await test.step(`attendee 2 logs into the room`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeEmailTwo
        );
        await attendeeTwoPage.goto(magicLink);
        await attendeeTwoPage.click(`text=Enter Now`);
        await attendeeTwoPage.waitForURL(/lobby/);
        await attendeeTwoPage.goto(roomListingPage);
        let roomone_locator = attendeeTwoPage.locator("text=Enter").nth(0);
        await roomone_locator.click();

        const attendeeTwoAvModal = new AVModal(attendeeTwoPage);
        await attendeeTwoAvModal.isVideoStreamLoadedOnAVModal();
        await attendeeTwoAvModal.clickOnJoinButtonOnAvModal();
      });
      await test.step(`organiser logs into the room`, async () => {
        await organiserPage.goto(attendeeLandingPage);
        await organiserPage.click(`text=Enter Now`);
        await organiserPage.goto(roomListingPage);

        let roomone_locator = organiserPage.locator("text=Enter").nth(0);
        await roomone_locator.click();

        const organiserAvModal = new AVModal(organiserPage);
        await organiserAvModal.isVideoStreamLoadedOnAVModal();
        await organiserPage.waitForTimeout(2000);
        await organiserAvModal.clickOnJoinButtonOnAvModal();
      });

      // presence and visibility of AV streams
      await test.step(`verify attendee 1 sees attendee 2 and organiser stream inside room`, async () => {
        attendeeOneRoom = new RoomModule(attendeeOnePage);
        await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await attendeeOneRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
        await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify attendee 2 sees attendee 1 and organiser stream inside room`, async () => {
        attendeeTwoRoom = new RoomModule(attendeeTwoPage);
        await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
        await attendeeTwoRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify organiser 1 sees attendee 1 and attendee 2 stream inside room`, async () => {
        organiserRoom = new RoomModule(organiserPage);
        await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await organiserRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
        await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
      });

      //verify every one is able to turn off their audio and video
      await test.step(`attendee one turns off the video and audio`, async () => {
        // when someone is already joined, the attendee who is joining will have its audio muted
        await expect(
          attendeeOneRoom.getStreamOptionsComponent
            .unmuteaudioButtonForMyStream,
          "expecting unmute button is visible to attendee one as his audio is already muted"
        ).toBeVisible();
        //now mute the video
        await test.step(`attendee 1 muting the video`, async () => {
          await attendeeOneRoom.getStreamOptionsComponent.muteMyVideo();
        });
      });
      await test.step(`attendee two turns off the video and audio`, async () => {
        // when someone is already joined, the attendee who is joining will have its audio muted
        await expect(
          attendeeTwoRoom.getStreamOptionsComponent
            .unmuteaudioButtonForMyStream,
          "expecting unmute button is visible to attendee one as his audio is already muted"
        ).toBeVisible();
        //now mute the video
        await test.step(`attendee 2 muting the video`, async () => {
          await attendeeTwoRoom.getStreamOptionsComponent.muteMyVideo();
        });
      });
      await test.step(`organiser turns off the video and audio`, async () => {
        await organiserRoom.getStreamOptionsComponent.muteMyAudio();
        await organiserRoom.getStreamOptionsComponent.muteMyVideo();
      });

      // presence and visibility of AV streams after every one has muted their audio and video
      await test.step(`verify attendee 1 sees attendee 2 and organiser stream inside room`, async () => {
        await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await attendeeOneRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
        await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify attendee 2 sees attendee 1 and organiser stream inside room`, async () => {
        await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
        await attendeeTwoRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify organiser 1 sees attendee 1 and attendee 2 stream inside room`, async () => {
        await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
        await organiserRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
        await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
      });

      await test.step(`Verify attendee do not get an option of sharing pdf inside room`, async () => {
        await attendeeOneRoom.getStreamOptionsComponent.clickOnScreenShareButton();
        await attendeeOneRoom.getStreamOptionsComponent.verifyGivenScreenSharingOptionIsNotVisible(
          ScreenSharingOptions.PDF_FILE
        );
        await attendeeOneRoom.getStreamOptionsComponent.clickOnScreenShareButton();
      });
      await test.step(`organiser  starts starts presending pdf through screen share inside the room`, async () => {
        await test.step("organsier  clicks on screenshare and choose the right option", async () => {
          await organiserRoom.getStreamOptionsComponent.clickOnScreenShareButton();
          await organiserRoom.getStreamOptionsComponent.selectScreenSharingOption(
            ScreenSharingOptions.PDF_FILE,
            VenueOptions.ROOMS
          );
        });

        await test.step(`verify attendee 1 sees 3 user stream and 1 pdf presentation stream`, async () => {
          await attendeeOneRoom.verifyCountOfStreamInsideRoomMatches(3);
          await attendeeOneRoom.verifyPdfStreamCountMatches(1);
        });
        await test.step(`verify attendee 2 sees 3 user stream and 1 pdf presentation stream`, async () => {
          await attendeeTwoRoom.verifyCountOfStreamInsideRoomMatches(3);
          await attendeeTwoRoom.verifyPdfStreamCountMatches(1);
        });
        await test.step(`verify organiser  sees 3 user stream and 1 pdf presentation stream`, async () => {
          await organiserRoom.verifyCountOfStreamInsideRoomMatches(3);
          await organiserRoom.verifyPdfStreamCountMatches(1);
        });
      });

      await test.step(`organiser 1 now stops sharing screen`, async () => {
        await test.step(`organiser  1 clicks on stop screenshare button`, async () => {
          await organiserRoom.getStreamOptionsComponent.clickToStopScreenShare();
        });

        await test.step(`verify attendee 1 now only see 3 user stream and no pdf stream`, async () => {
          await attendeeOneRoom.verifyCountOfStreamInsideRoomMatches(3);
          await attendeeOneRoom.verifyPdfStreamCountMatches(0);
        });

        await test.step(`verify attendee 2 now only see 3 user stream and no pdf stream`, async () => {
          await attendeeTwoRoom.verifyCountOfStreamInsideRoomMatches(3);
          await attendeeTwoRoom.verifyPdfStreamCountMatches(0);
        });

        await test.step(`verify organiser now only see 3 user stream and no pdf stream`, async () => {
          await organiserRoom.verifyCountOfStreamInsideRoomMatches(3);
          await organiserRoom.verifyPdfStreamCountMatches(0);
        });
      });

      // Now every one exits from the room
      await test.step(`verify attendee 1 is able to exit the room and land on room listing page`, async () => {
        await attendeeOneRoom.clickOnExitRoom();
        await attendeeOneRoom.handleExitConfirmationPopup(true);
      });

      // attendee two exit from the room
      await test.step(`verify attendee 2 is able to exit the room and land on room listing page`, async () => {
        await attendeeTwoRoom.clickOnExitRoom();
        await attendeeTwoRoom.handleExitConfirmationPopup(true);
      });

      // organiser exits from the room
      await test.step(`verify organiser is able to exit the room and land on room listing page`, async () => {
        await organiserRoom.clickOnExitRoom();
        await organiserRoom.handleExitConfirmationPopup(true);
      });
    });

    test("TC002: Verify PDF share functionality in Rooms - Speaker is able to share. || @screenshare @pdfSharing @roomsScreenShare", async () => {
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-469",
      });

      let speakerOnePage = await speakerBrowserContext.newPage();
      let attendeeOnePage = await attendeeOneBrowserContext.newPage();
      let organiserPage = await orgBrowserContext.newPage();

      let speakerRoom: RoomModule;
      let attendeeRoom: RoomModule;
      let organiserRoom: RoomModule;

      const speakerFirstName = "AutomationSpeakerOne";
      const speakerLastName = "QA";

      const attendeeFirstName = "AutomationAttendeeOne";
      const attendeeLastName = "QA";

      const organiserFirstName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

      let speakerEmail = DataUtl.getRandomAdminEmail();
      let attendeeEmail = DataUtl.getRandomAttendeeEmail();

      await test.step(`organiser invites speaker 1 to the event`, async () => {
        await eventController.inviteSpeakerToTheEventByApi(
          speakerEmail,
          speakerFirstName,
          speakerLastName
        );
      });

      await test.step(`organiser invites attende 1 to the event`, async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          phoneNumber: "1234567899",
          email: attendeeEmail,
        });
      });

      await eventController.disableOnboardingChecksForSpeaker();

      await test.step(`speaker 1 logs into the room`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          eventId,
          speakerEmail
        );
        await speakerOnePage.goto(magicLink);
        await speakerOnePage.click(`text=Enter Now`);
        await speakerOnePage.goto(roomListingPage);

        let roomone_locator = speakerOnePage.locator("text=Enter").nth(0);
        await roomone_locator.click();

        const speakerAvModal = new AVModal(speakerOnePage);
        await speakerAvModal.isVideoStreamLoadedOnAVModal();
        await speakerAvModal.clickOnJoinButtonOnAvModal();
      });

      await test.step(`attende 1 logs into the room`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeEmail
        );
        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(roomListingPage);

        let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
        await roomone_locator.click();

        const speakerAvModal = new AVModal(attendeeOnePage);
        await speakerAvModal.isVideoStreamLoadedOnAVModal();
        await speakerAvModal.clickOnJoinButtonOnAvModal();
      });

      await test.step(`organiser logs into the room`, async () => {
        await organiserPage.goto(speakerLandingPage);
        await organiserPage.click(`text=Enter Now`);
        await organiserPage.goto(roomListingPage);

        let roomone_locator = organiserPage.locator("text=Enter").nth(0);
        await roomone_locator.click();

        const organiserAvModal = new AVModal(organiserPage);
        await organiserAvModal.isVideoStreamLoadedOnAVModal();
        await organiserPage.waitForTimeout(2000);
        await organiserAvModal.clickOnJoinButtonOnAvModal();
      });

      // presence and visibility of AV streams
      await test.step(`verify speaker 1 sees attendee 1 and organiser stream inside room`, async () => {
        speakerRoom = new RoomModule(speakerOnePage);
        await speakerRoom.verifyUserStreamIsVisible(speakerFirstName);
        await speakerRoom.verifyUserStreamIsVisible(attendeeFirstName);
        await speakerRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify attendee 1 sees speaker 1 and organiser stream inside room`, async () => {
        attendeeRoom = new RoomModule(attendeeOnePage);
        await attendeeRoom.verifyUserStreamIsVisible(speakerFirstName);
        await attendeeRoom.verifyUserStreamIsVisible(attendeeFirstName);
        await attendeeRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify organiser 1 sees speaker 1 and attendee 1 stream inside room`, async () => {
        organiserRoom = new RoomModule(organiserPage);
        await organiserRoom.verifyUserStreamIsVisible(speakerFirstName);
        await organiserRoom.verifyUserStreamIsVisible(attendeeFirstName);
        await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
      });

      // presence and visibility of AV streams after every one has muted their audio and video
      await test.step(`verify speaker 1 sees attendee 1 and organiser stream inside room`, async () => {
        await speakerRoom.verifyUserStreamIsVisible(speakerFirstName);
        await speakerRoom.verifyUserStreamIsVisible(attendeeFirstName);
        await speakerRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify attendee 1 sees speaker 1 and organiser stream inside room`, async () => {
        await attendeeRoom.verifyUserStreamIsVisible(speakerFirstName);
        await attendeeRoom.verifyUserStreamIsVisible(attendeeFirstName);
        await attendeeRoom.verifyUserStreamIsVisible(organiserFirstName);
      });
      await test.step(`verify organiser 1 sees speaker 1 and attendee 1 stream inside room`, async () => {
        await organiserRoom.verifyUserStreamIsVisible(speakerFirstName);
        await organiserRoom.verifyUserStreamIsVisible(attendeeFirstName);
        await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
      });

      await test.step(`organiser  starts starts presending pdf through screen share inside the room`, async () => {
        await test.step("organsier  clicks on screenshare and choose the right option", async () => {
          await organiserRoom.getStreamOptionsComponent.clickOnScreenShareButton();
          await organiserRoom.getStreamOptionsComponent.selectScreenSharingOption(
            ScreenSharingOptions.PDF_FILE,
            VenueOptions.ROOMS
          );
        });

        await test.step(`verify speaker 1 sees 3 user stream and 1 pdf presentation stream`, async () => {
          await speakerRoom.verifyCountOfStreamInsideRoomMatches(3);
          await speakerRoom.verifyPdfStreamCountMatches(1);
        });
        await test.step(`verify attendee 1 sees 3 user stream and 1 pdf presentation stream`, async () => {
          await attendeeRoom.verifyCountOfStreamInsideRoomMatches(3);
          await attendeeRoom.verifyPdfStreamCountMatches(1);
        });
        await test.step(`verify organiser  sees 3 user stream and 1 pdf presentation stream`, async () => {
          await organiserRoom.verifyCountOfStreamInsideRoomMatches(3);
          await organiserRoom.verifyPdfStreamCountMatches(1);
        });
      });

      await test.step(`organiser 1 now stops sharing screen`, async () => {
        await test.step(`organiser  1 clicks on stop screenshare button`, async () => {
          await organiserRoom.getStreamOptionsComponent.clickToStopScreenShare();
        });

        await test.step(`verify speaker 1 now only see 3 user stream and no pdf stream`, async () => {
          await speakerRoom.verifyCountOfStreamInsideRoomMatches(3);
          await speakerRoom.verifyPdfStreamCountMatches(0);
        });

        await test.step(`verify attendee 1 now only see 3 user stream and no pdf stream`, async () => {
          await attendeeRoom.verifyCountOfStreamInsideRoomMatches(3);
          await attendeeRoom.verifyPdfStreamCountMatches(0);
        });

        await test.step(`verify organiser now only see 3 user stream and no pdf stream`, async () => {
          await organiserRoom.verifyCountOfStreamInsideRoomMatches(3);
          await organiserRoom.verifyPdfStreamCountMatches(0);
        });
      });

      // Now every one exits from the room
      await test.step(`verify speaker 1 is able to exit the room and land on room listing page`, async () => {
        await speakerRoom.clickOnExitRoom();
        await speakerRoom.handleExitConfirmationPopup(true);
      });

      // speaker two exit from the room
      await test.step(`verify attendee 1 is able to exit the room and land on room listing page`, async () => {
        await attendeeRoom.clickOnExitRoom();
        await attendeeRoom.handleExitConfirmationPopup(true);
      });

      // organiser exits from the room
      await test.step(`verify organiser is able to exit the room and land on room listing page`, async () => {
        await organiserRoom.clickOnExitRoom();
        await organiserRoom.handleExitConfirmationPopup(true);
      });
    });

    test("TC003: Verify PDF share functionality in Legacy-Backstage. || @screenshare @pdfSharing", async () => {
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-209",
      });

      let attendeePage = await attendeeOneBrowserContext.newPage();
      let organiserPage = await orgBrowserContext.newPage();

      const attendeeFirstName = "AutomationAttendeeOne";
      const attendeeLastName = "QA";

      const organiserFirstName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

      let attendeeEmail = DataUtl.getRandomAttendeeEmail();

      let stageId = await eventController.getDefaultStageId();
      let stagePageURI = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/l/event/${eventId}/stages/${stageId}`;
      await eventController.disableStudioAsBackstage(stageId);

      let organiserStagePage: StagePage;
      let attendeeStagePage: StagePage;

      let organiserGreenRoom: GreenRoom;

      await test.step(`Organiser disables onboarding checks and invites Attendee to the event`, async () => {
        await eventController.disableOnboardingChecksForAttendee();

        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          phoneNumber: "1234567899",
          email: attendeeEmail,
        });
      });

      await test.step(`Attendee joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeEmail
        );

        await attendeePage.goto(magicLink);
        await attendeePage.click(`text=Enter Now`);
        await attendeePage.goto(stagePageURI);
        attendeeStagePage = new StagePage(attendeePage);
      });

      await test.step(`Organizer joins the event and join backstage via default stage.`, async () => {
        await organiserPage.goto(attendeeLandingPage);
        await organiserPage.click(`text=Enter Now`);
        await organiserPage.goto(stagePageURI);
        organiserStagePage = new StagePage(organiserPage);
        const orgAvModal =
          await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
        await orgAvModal.isVideoStreamLoadedOnAVModal();
        await orgAvModal.clickOnJoinButtonOnAvModal();

        await test.step(`Verify Organiser's presence in backstage.`, async () => {
          organiserGreenRoom = new GreenRoom(organiserPage);
          await organiserGreenRoom.isGreenRoomVisible();
          await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(1);
        });
      });

      await test.step("Organiser joins the stage.", async () => {
        await organiserGreenRoom.sendUserToOnstageFromBackStageUsingRaiseHand(
          organiserFirstName
        );
      });

      await test.step("Organsier clicks on screenshare and choose the PDF option.", async () => {
        await organiserGreenRoom.getStreamOptionsComponent.clickOnScreenShareButton();
        await organiserGreenRoom.getStreamOptionsComponent.selectScreenSharingOption(
          ScreenSharingOptions.PDF_FILE,
          VenueOptions.LEGACYBACKSTAGE
        );
      });

      await test.step(`Organizer initiate session and joins the stage.`, async () => {
        await organiserGreenRoom.disableDryRun();
        await organiserGreenRoom.startTheSession();
      });

      await test.step(`Verify Attendee sees 1 user stream and 1 pdf presentation stream`, async () => {
        await attendeeStagePage.getAvComponent.verifyCountOfStreamInsideRoomMatches(
          1
        );
        await attendeeStagePage.getAvComponent.verifyPdfStreamCountMatches(1);
      });

      await test.step(`Organiser clicks on stop screenshare button`, async () => {
        await organiserGreenRoom.getStreamOptionsComponent.clickToStopScreenShare();
      });

      await test.step(`Verify Attendee only see 1 user stream and no pdf stream`, async () => {
        await attendeeStagePage.getAvComponent.verifyCountOfStreamInsideRoomMatches(
          1
        );
        await attendeeStagePage.getAvComponent.verifyPdfStreamCountMatches(0);
      });

      await test.step(`Organiser ends the session.`, async () => {
        await organiserGreenRoom.endTheSession();
      });
    });
  }
);
