import test, {
  APIRequestContext,
  BrowserContext,
  Page,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { inviteSpeakerByApi } from "../../../util/apiUtil";
import { EventSeriesController } from "../../../controller/EventSeriesController";
import EventSeriesCommunicationEnum from "../../../enums/EventSeriesCommunicationEnum";
import { EventSeriesPage } from "../../../page-objects/new-org-side-pages/EventSeriesPage";
import { SpeakerData } from "../../../test-data/speakerData";

const thumbnailPath = "eventSeries.jpeg";

test.describe.parallel("@event-series", async () => {
  test.describe.configure({ retries: 2 });

  let attendeeEmail: string;
  let organiserBrowserContext: BrowserContext;
  let organiserApiContext: APIRequestContext;
  let organiserPage: Page;
  let seriesId: string;
  let seriesSetupPageObject: EventSeriesPage;
  let seriesController: EventSeriesController;

  test.beforeEach(async () => {
    await test.step("Initialize Organiser browser, API context and page.", async () => {
      organiserBrowserContext = await BrowserFactory.getBrowserContext({});
      organiserApiContext = organiserBrowserContext.request;
      organiserPage = await organiserBrowserContext.newPage();
    });

    await test.step("Initialize EventSeries page object and Event controller.", async () => {
      await test.step("Create new series.", async () => {
        seriesId = await new EventSeriesPage(organiserPage).createSeries(
          "AutomationSeries",
          thumbnailPath
        );

        seriesSetupPageObject = new EventSeriesPage(organiserPage, seriesId);

        seriesController = new EventSeriesController(
          organiserApiContext,
          seriesId
        );
      });

      await test.step("Add Attendees to the Series.", async () => {
        attendeeEmail = await seriesController.getRandomAttendeeEmail();
        await seriesController.addAttendeesToSeries([attendeeEmail]);
        await seriesSetupPageObject.verifyAttendeeCountInPeopleSection();
      });
    });
  });

  test.afterEach(async () => {
    await test.step("Close Organiser browser and page.", async () => {
      await organiserPage?.close();
      await organiserBrowserContext?.close();
    });
  });

  test("TC001: Enable add event communication and verify the email and calendar block.", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-713",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-714",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-717",
    });

    await test.step("Enable 'Add event communication' setting.", async () => {
      await seriesController.changeCommunicationSettings(
        EventSeriesCommunicationEnum.EVENT_SERIES_NEW_EVENT_ADDED,
        true
      );
    });

    await test.step("Creating new event.", async () => {
      await seriesSetupPageObject.addEventToTheSeries(
        organiserApiContext,
        "SeriesTest Event",
        0
      );
    });

    await test.step("Verify the email received for all attendees.", async () => {
      await seriesController.verifyAddEventEmailReceivedByUser([attendeeEmail]);
    });

    await test.step("Verify the invitaions is attached to every email.", async () => {
      await seriesController.verifyCalendarBlocksAreAttachedWithEmail(
        [attendeeEmail],
        "invitation.ics"
      );
    });
  });

  test("TC002: Enable add webinar communication and verify the email and calendar block.", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-715",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-716",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-717",
    });

    await test.step("Enable 'Add event communication' setting.", async () => {
      await seriesController.changeCommunicationSettings(
        EventSeriesCommunicationEnum.EVENT_SERIES_NEW_WEBINAR_ADDED,
        true
      );
    });

    await test.step("Adding new Webinar in Series.", async () => {
      await seriesSetupPageObject.addWebinarToTheSeriesByApi(
        organiserApiContext,
        "EventSeries-TestWebinar"
      );
    });

    await test.step("Verify the email received for all attendees.", async () => {
      await seriesController.verifyAddWebinarEmailReceivedByUser([
        attendeeEmail,
      ]);
    });

    await test.step("Verify the invitaions is attached to every email.", async () => {
      await seriesController.verifyCalendarBlocksAreAttachedWithEmail(
        [attendeeEmail],
        "invitation.ics",
        true
      );
    });
  });

  test("TC003: Verify attendees added to series are also added to Ongoing/Upcoming Event and verify if attendee and speaker is able to access the live event side of ongoing Event.", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-704",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-705",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-710",
    });

    let attendeeEmail2;
    let attendeeBrowserContext: BrowserContext;
    let attendeePage: Page;

    let speakerEmail;
    let speakerBrowserContext: BrowserContext;
    let speakerPage: Page;

    await test.step("Enable 'Add event communication' setting.", async () => {
      await seriesController.changeCommunicationSettings(
        EventSeriesCommunicationEnum.EVENT_SERIES_NEW_EVENT_ADDED,
        true
      );

      await seriesController.changeCommunicationSettings(
        EventSeriesCommunicationEnum.EVENT_SERIES_CONSOLIDATED_INVITATION_CONFIRMATION,
        true
      );
    });

    let eventId;
    await test.step("Creating new event.", async () => {
      eventId = await seriesSetupPageObject.addEventToTheSeries(
        organiserApiContext,
        "TestSeries-Event"
      );
    });

    await test.step("Verify Series attendees are added to Upcoming Event.", async () => {
      await seriesController.verifySeriesAttendeesAreAdded(seriesId, eventId);
    });

    await test.step("Add another attendee to the Series and verify if the attendee is added to the event.", async () => {
      attendeeEmail2 = await seriesController.getRandomAttendeeEmail();
      await seriesController.addAttendeesToSeries([attendeeEmail2]);
      await seriesController.verifySeriesAttendeesAreAdded(seriesId, eventId);
    });

    await test.step("Add speaker to the event.", async () => {
      speakerEmail = await seriesController.getRandomSpeakerEmail();

      await inviteSpeakerByApi(
        organiserApiContext,
        new SpeakerData(
          speakerEmail,
          "AuomationSpeaker",
          "1"
        ).getSpeakerDataObject(),
        eventId,
        true
      );
    });

    await test.step("Initialize Attendee browser context and page.", async () => {
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage = await attendeeBrowserContext.newPage();
    });

    await test.step("Initialize Speaker browser context and page.", async () => {
      speakerBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      speakerPage = await speakerBrowserContext.newPage();
    });

    await test.step("Fetch Attendee magic link and verify if Live event side is accessible.", async () => {
      let magicLink =
        await seriesController.getMagicLinkFromSeriesInvitationEmail(
          attendeeEmail2
        );

      // await seriesController.verifyLiveEventSideIsAccessibleForAttendee(
      //   magicLink,
      //   attendeePage
      // );
    });

    await test.step("Fetch Speaker magic link and verify if Live event side is accessible.", async () => {
      let magicLink =
        await seriesController.getMagicLinkFromEventInvitationEmail(
          speakerEmail
        );

      // await seriesController.verifyLiveEventSideIsAccessibleForSpeaker(
      //   magicLink,
      //   speakerPage
      // );
    });
  });

  test("TC004: Verify attendees added to series are also added to Ongoing/Upcoming Webinar and verify if attendee and speaker is able to access the live event side of ongoing Webinar.", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-706",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-707",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-711",
    });

    let attendeeBrowserContext: BrowserContext;
    let attendeePage: Page;
    let attendeeEmail2;

    let speakerEmail;
    let speakerBrowserContext: BrowserContext;
    let speakerPage: Page;

    await test.step("Enable 'Add Webinar communication' and 'Event Invitation' setting.", async () => {
      await seriesController.changeCommunicationSettings(
        EventSeriesCommunicationEnum.EVENT_SERIES_NEW_WEBINAR_ADDED,
        true
      );

      await seriesController.changeCommunicationSettings(
        EventSeriesCommunicationEnum.EVENT_SERIES_CONSOLIDATED_INVITATION_CONFIRMATION,
        true
      );
    });

    let webinarId: string;
    await test.step("Creating new Webinar.", async () => {
      webinarId = await seriesSetupPageObject.addWebinarToTheSeriesByApi(
        organiserApiContext,
        "EventSeries-TestWebinar"
      );
    });

    await test.step("Verify Series attendees are added to Upcoming Webinar.", async () => {
      await seriesController.verifySeriesAttendeesAreAdded(seriesId, webinarId);
    });

    await test.step("Add another attendee to the Series and verify if the attendee is added to the Webinar.", async () => {
      attendeeEmail2 = await seriesController.getRandomAttendeeEmail();
      await seriesController.addAttendeesToSeries([attendeeEmail2]);
      await seriesController.verifySeriesAttendeesAreAdded(seriesId, webinarId);
    });

    await test.step("Initialize Speaker browser context and page.", async () => {
      speakerBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      speakerPage = await speakerBrowserContext.newPage();
    });

    await test.step("Add speaker to the Webinar.", async () => {
      speakerEmail = await seriesController.getRandomSpeakerEmail();

      await inviteSpeakerByApi(
        organiserApiContext,
        new SpeakerData(
          speakerEmail,
          "AuomationSpeaker",
          "1"
        ).getSpeakerDataObject(),
        webinarId,
        true
      );
    });

    await test.step("Initialize Attendee browser, API context and page.", async () => {
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage = await attendeeBrowserContext.newPage();
    });

    await test.step("Fetch attendee magic link and verify if Live event side is accessible.", async () => {
      let magicLink =
        await seriesController.getMagicLinkFromSeriesInvitationEmail(
          attendeeEmail2,
          true
        );

      // await seriesController.verifyLiveEventSideIsAccessibleForAttendee(
      //   magicLink,
      //   attendeePage,
      //   true
      // );
    });

    await test.step("Fetch Speaker magic link and verify if Live event side is accessible.", async () => {
      let magicLink =
        await seriesController.getMagicLinkFromEventInvitationEmail(
          speakerEmail
        );

      // await seriesController.verifyLiveEventSideIsAccessibleForSpeaker(
      //   magicLink,
      //   speakerPage,
      //   true
      // );
    });
  });
});
