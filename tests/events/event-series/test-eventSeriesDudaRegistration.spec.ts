import {
  APIRequestContext,
  BrowserContext,
  Page,
  test,
} from "@playwright/test";
import { DudaLandingPage } from "../../../page-objects/events-pages/duda-website/dudaLandingPage";
import { RegistrationWidgetIframe } from "../../../page-objects/events-pages/duda-website/registrationIframe";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { EventSeriesController } from "../../../controller/EventSeriesController";
import EventSeriesCommunicationEnum from "../../../enums/EventSeriesCommunicationEnum";
import { EventSeriesPage } from "../../../page-objects/new-org-side-pages/EventSeriesPage";

test.describe(`@duda @registration @event-series`, async () => {
  let dudaPublishedSeriesId;
  let dudaPublishedAttendeeLandingPage: string;
  let orgBrowserContext: BrowserContext;
  let organiserApiContext: APIRequestContext;
  let seriesController: EventSeriesController;
  let organiserPage: Page;
  let seriesSetupPageObject: EventSeriesPage;
  const thumbnailPath = "eventSeries.jpeg";

  test.beforeEach(async () => {
    await test.step("Initialize Organiser browser, API context and page.", async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({});
      organiserApiContext = orgBrowserContext.request;
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step("Fetch eventSeries Id.", async () => {
      dudaPublishedSeriesId = await DataUtl.getApplicationTestDataObj()[
        "seriesIdForEventSeriesTesting"
      ];
    });

    await test.step("Fetch duda page link.", async () => {
      dudaPublishedAttendeeLandingPage =
        DataUtl.getApplicationTestDataObj()[
          "dudaPublishedTemplateForEventSeriesRegistration"
        ];
    });

    seriesSetupPageObject = new EventSeriesPage(
      organiserPage,
      dudaPublishedSeriesId
    );

    seriesController = new EventSeriesController(
      organiserApiContext,
      dudaPublishedSeriesId
    );
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`TC001: New attendee registers through widget, gets Registration Confirmed email`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-745",
    });

    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;
    let attendeeEmail = await seriesController.getRandomAttendeeEmail();
    console.log(`Attendee email -> ${attendeeEmail}`);

    await test.step("Enable 'Add event communication' setting.", async () => {
      await seriesController.changeCommunicationSettings(
        EventSeriesCommunicationEnum.EVENT_SERIES_CONSOLIDATED_REGISTRATION_CONFIRMATION,
        true
      );
    });

    await test.step("Creating new event.", async () => {
      await seriesSetupPageObject.addEventToTheSeries(
        organiserApiContext,
        "QAT-745 Event"
      );
    });

    await test.step(`Loading the duda attendee landing page and click on register button to get registration iframe`, async () => {
      await organiserPage.goto(dudaPublishedAttendeeLandingPage);
      dudaAttendeeLandingPage = new DudaLandingPage(organiserPage);
      registrationWidgetIframe =
        await dudaAttendeeLandingPage.clickOnRegisterButton();
    });

    await test.step(`Fill up the registration form`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.fillUpTheRegistrationForm(
        {
          firstName: "Automation",
          lastName: "TestAttendee",
          email: attendeeEmail,
        }
      );
    });

    await test.step(`Verify that successfull registration message is displayed.`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.verifySuccessFullRegistrationMessageIsVisible();
    });

    await test.step(`Fetching the attendee magic link after user registration`, async () => {
      await seriesController.verifyRegistrationConfirmationEmail(
        attendeeEmail,
        organiserPage
      );
    });

    await test.step("Remove attendee from Series.", async () => {
      await seriesController.removeAttendeeFromSeries(attendeeEmail);
    });
  });
  //     page,
  //   }) => {
  //     /**
  //      * register a user via api
  //      * verify he has registered by getting his reg id
  //      * attendee gets the magic link
  //      * but we delete the attendee via api
  //      * now attendee loads the attendee magic link
  //      * attendee should not see enter now button
  //      * attendee should see register button as the magic link is no more valid
  //      */
  //     let dudaAttendeeLandingPage: DudaLandingPage;
  //     let registrationWidgetIframe: RegistrationWidgetIframe;
  //     let attendeeMagicLink: string;
  //     let dbUtil = new DBUtil(page);
  //     let userRegistrationId;

  //     await test.step(`Updaing landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
  //       await updateEventLandingPageDetails(
  //         organiserApiContext,
  //         dudaPublishedEventId,
  //         {
  //           eventEntryType: EventEntryType.REG_BASED,
  //           isMagicLinkEnabled: true,
  //         }
  //       );
  //     });
  //     const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
  //     const attendeeInfoDto = new UserInfoDTO(
  //       attendeeEmail,
  //       "",
  //       "prateek",
  //       "automation",
  //       false
  //     );
  //     const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", true);
  //     const eventRoleDto = new UserEventRoleDto(
  //       attendeeInfoDto,
  //       eventInfoDto,
  //       EventRole.NOTEXISTS,
  //       false
  //     );

  //     await test.step(`Registering user via API to this event`, async () => {
  //       await registerUserToEventbyApi(
  //         organiserApiContext,
  //         dudaPublishedEventId,
  //         attendeeEmail
  //       );
  //     });

  //     await test.step(`fetching user registration id from db`, async () => {
  //       const userRegRecord =
  //         await dbUtil.fetchUserDataFromEventRegAccountAndEventRoleTable(
  //           dudaPublishedEventId,
  //           attendeeEmail
  //         );
  //       userRegistrationId = userRegRecord["registration_id"];
  //       expect(
  //         userRegistrationId,
  //         `expecting ${userRegistrationId} to be not undefined`
  //       ).not.toBeUndefined();
  //     });

  //     await test.step(`fetching magic link from the db for user`, async () => {
  //       attendeeMagicLink = await dbUtil.fetchDudaAttendeeMagicLinkFromDB(
  //         dudaPublishedEventId,
  //         attendeeEmail,
  //         dudaPublishedAttendeeLandingPage
  //       );
  //     });

  //     await test.step(`delete the user via api`, async () => {
  //       await deleteAttendeeFromEvent(
  //         organiserApiContext,
  //         dudaPublishedEventId,
  //         attendeeEmail,
  //         userRegistrationId
  //       );
  //     });

  //     await test.step(`attendee tries to use his old magic link and it should not work since he is deleted`, async () => {
  //       dudaAttendeeLandingPage = new DudaLandingPage(page);
  //       await dudaAttendeeLandingPage.load(attendeeMagicLink);
  //       await expect(
  //         dudaAttendeeLandingPage.enterNowButton,
  //         "expecting enter now button to not be visibl since magic link is no more valid"
  //       ).not.toBeVisible();
  //       await expect(
  //         dudaAttendeeLandingPage.registerButton,
  //         "expecting register now button to be visible since magic link will not work"
  //       ).toBeVisible();
  //     });
  //   });

  //   test(`TC012: @attendee invited attendee can login to event with the recieved magic link`, async ({
  //     page,
  //   }) => {
  //     /**
  //      * register a user via api
  //      * verify he has registered by getting his reg id
  //      * attendee gets the magic link
  //      * but we delete the attendee via api
  //      * now attendee loads the attendee magic link
  //      * attendee should not see enter now button
  //      * attendee should see register button as the magic link is no more valid
  //      */
  //     let dudaAttendeeLandingPage: DudaLandingPage;
  //     let registrationWidgetIframe: RegistrationWidgetIframe;
  //     let attendeeMagicLink: string;
  //     let dbUtil = new DBUtil(page);
  //     let userRegistrationId;

  //     await test.step(`Updaing landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
  //       await updateEventLandingPageDetails(
  //         organiserApiContext,
  //         dudaPublishedEventId,
  //         {
  //           eventEntryType: EventEntryType.INVITE_ONLY,
  //           isMagicLinkEnabled: true,
  //         }
  //       );
  //     });
  //     const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
  //     const attendeeInfoDto = new UserInfoDTO(
  //       attendeeEmail,
  //       "",
  //       "prateek",
  //       "automation",
  //       false
  //     );
  //     const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", true);
  //     const eventRoleDto = new UserEventRoleDto(
  //       attendeeInfoDto,
  //       eventInfoDto,
  //       EventRole.NOTEXISTS,
  //       false
  //     );

  //     await test.step(`Invite user via API to this event`, async () => {
  //       await inviteAttendeeByAPI(
  //         organiserApiContext,
  //         dudaPublishedEventId,
  //         attendeeEmail,
  //         false
  //       );
  //     });

  //     await test.step(`fetching magic link from the db for user`, async () => {
  //       attendeeMagicLink = await dbUtil.fetchDudaAttendeeMagicLinkFromDB(
  //         dudaPublishedEventId,
  //         attendeeEmail,
  //         dudaPublishedAttendeeLandingPage
  //       );
  //     });

  //     await test.step(`verify if user uses his magic link he should be able to enter into invite only event`, async () => {
  //       dudaAttendeeLandingPage = new DudaLandingPage(page);
  //       await dudaAttendeeLandingPage.load(attendeeMagicLink);
  //       // await dudaAttendeeLandingPage.clickOnEnterNowButton();
  //       await dudaAttendeeLandingPage.isLobbyLoaded();
  //     });
  //   });

  //   test(`TC013: @attendee not invited user should not be able to login to the invite only event`, async ({
  //     page,
  //   }) => {
  //     /**
  //      * in an invite only event
  //      * verify if an uninvited user tries to login
  //      * he sees restricted entry screen for attendee
  //      */
  //     let dudaAttendeeLandingPage: DudaLandingPage;
  //     let registrationWidgetIframe: RegistrationWidgetIframe;

  //     await test.step(`Updaing landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
  //       await updateEventLandingPageDetails(
  //         organiserApiContext,
  //         dudaPublishedEventId,
  //         {
  //           eventEntryType: EventEntryType.INVITE_ONLY,
  //           isMagicLinkEnabled: true,
  //         }
  //       );
  //     });
  //     const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();

  //     await test.step(`Attendee loads the landing page and click on register button to open iframe`, async () => {
  //       dudaAttendeeLandingPage = new DudaLandingPage(page);
  //       await dudaAttendeeLandingPage.load(dudaPublishedAttendeeLandingPage);
  //       registrationWidgetIframe =
  //         await dudaAttendeeLandingPage.clickOnRegisterButton();
  //     });
  //     await test.step(`attendee clicks on already registered button as he thinks he is invited`, async () => {
  //       await expect(
  //         registrationWidgetIframe.getRegistrationFormComponent
  //           .alreadyRegisteredButtonContainer,
  //         "expecting already registered button to be visible"
  //       ).toBeVisible();
  //       await registrationWidgetIframe.getRegistrationFormComponent.clickOnAlreadyRegisteredButton();
  //       await registrationWidgetIframe.getLoginOptionsComponent.emailInputBoxForOTP.fill(
  //         attendeeEmail
  //       );
  //       await registrationWidgetIframe.getLoginOptionsComponent.submitButton.click();
  //     });

  //     await test.step(`verify attendee sees the attendee restricted page to tell he has no access`, async () => {
  //       await registrationWidgetIframe.verifyInviteOnlyRestrictedScreenIsVisibleForAttendee();
  //     });
  //   });

  //   test(`TC014: @organiser if already logged in visit Duda published landing page, he should be treated as logged in`, async () => {
  //     /**
  //      * in an invite only event
  //      * verify if an uninvited user tries to login
  //      * he sees restricted entry screen for attendee
  //      */
  //     let dudaAttendeeLandingPage: DudaLandingPage;
  //     let registrationWidgetIframe: RegistrationWidgetIframe;
  //     let orgPage = await orgBrowserContext.newPage();
  //     await test.step(`Updaing landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
  //       await updateEventLandingPageDetails(
  //         organiserApiContext,
  //         dudaPublishedEventId,
  //         {
  //           eventEntryType: EventEntryType.REG_BASED,
  //           isMagicLinkEnabled: true,
  //         }
  //       );
  //     });

  //     await test.step(`orgainser goes to visit the duda published landing page, should see enter now button`, async () => {
  //       dudaAttendeeLandingPage = new DudaLandingPage(orgPage);
  //       await dudaAttendeeLandingPage.load(dudaPublishedAttendeeLandingPage);
  //       await dudaAttendeeLandingPage.clickOnEnterNowButton();
  //     });

  //     await test.step(`organiser if clicks on enter now button should be landed on stage`, async () => {
  //       await dudaAttendeeLandingPage.isStageLoaded();
  //     });
  //   });
});
