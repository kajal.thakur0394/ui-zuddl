import test, {
  APIRequestContext,
  BrowserContext,
  Page,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import SeriesRegistrationSection from "../event-series/seriesRegistrationSection";
import { EventSeriesController } from "../../../controller/EventSeriesController";
import { EventSeriesPage } from "../../../page-objects/new-org-side-pages/EventSeriesPage";

const thumbnailPath = "eventSeries.jpeg";
const seriesName = "AutomationSeries";

test.describe("@event-series @custom-registration", async () => {
  test.slow;
  //   test.describe.configure({ retries: 2 });

  let seriesId: string;
  let organiserPage: Page;
  let organiserApiContext: APIRequestContext;
  let organiserBrowserContext: BrowserContext;

  let seriesSetupPageObject: EventSeriesPage;
  let seriesController: EventSeriesController;

  test.beforeAll(async () => {
    await test.step("Initialize Organiser browser, API context and page.", async () => {
      organiserBrowserContext = await BrowserFactory.getBrowserContext({});
      organiserApiContext = organiserBrowserContext.request;
      organiserPage = await organiserBrowserContext.newPage();
    });

    await test.step("Initialize EventSeries page object and Event controller.", async () => {
      await test.step("Create new series.", async () => {
        seriesId = await new EventSeriesPage(organiserPage).createSeries(
          seriesName,
          thumbnailPath
        );

        seriesSetupPageObject = new EventSeriesPage(organiserPage, seriesId);

        seriesController = new EventSeriesController(
          organiserApiContext,
          seriesId
        );
      });
    });
  });

  test.afterAll(async () => {
    await test.step("Close Organiser browser and page.", async () => {
      await organiserPage?.close();
      await organiserBrowserContext?.close();
    });
  });

  test("TC001: Create a custom field in series registration form and create a Webinar.", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-754",
    });

    let seriesRegistrationSection: SeriesRegistrationSection;

    await test.step("Initialize RegistrationSection object.", async () => {
      seriesRegistrationSection = new SeriesRegistrationSection(
        organiserApiContext,
        seriesId,
        seriesName
      );
    });

    await test.step("Add a custom field in registration form.", async () => {
      await seriesRegistrationSection.addCustomFieldsToRegistrationFormOfEventSeries();
    });

    await test.step("Add new Webinar to series.", async () => {
      await seriesSetupPageObject.addWebinarToTheSeriesByApi(
        organiserApiContext,
        "QAT-754 Webinar"
      );
    });
  });
});
