import test, { APIRequestContext, BrowserContext } from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { EventSeriesController } from "../../../controller/EventSeriesController";
import { DataUtl } from "../../../util/dataUtil";

test.describe("@event-series", async () => {
  let organiserBrowserContext: BrowserContext;
  let organiserApiContext: APIRequestContext;
  let seriesController: EventSeriesController;

  let staticEventSeriesId: string;
  let staticEventSeriesEventId: string;
  let staticEventSeriesWebinarId: string;

  test.beforeEach(async () => {
    await test.step("Initialize Organiser browser, API context and page.", async () => {
      organiserBrowserContext = await BrowserFactory.getBrowserContext({});
      organiserApiContext = organiserBrowserContext.request;
    });

    await test.step("Fetch eventSeries Id.", async () => {
      staticEventSeriesId = await DataUtl.getApplicationTestDataObj()[
        "seriesIdForEventSeriesTesting"
      ];
    });

    await test.step("Initialize Event Series controller.", async () => {
      seriesController = new EventSeriesController(
        organiserApiContext,
        staticEventSeriesId
      );
    });
  });

  test.afterEach(async () => {
    await organiserBrowserContext?.close();
  });

  test("TC001: Verify new attendee added to series is not added to completed Event.", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-708",
    });

    let attendeeEmail;

    await test.step("Fetch eventSeries Event id.", async () => {
      staticEventSeriesEventId = await DataUtl.getApplicationTestDataObj()[
        "completedEventIdForEventSeriesTesting"
      ];
      console.log("Static Event Id ->", staticEventSeriesEventId);
    });

    await test.step("Add attendee to the Event series and verify the user is added.", async () => {
      await seriesController.verifySeriesAttendeesAreAdded(
        staticEventSeriesId,
        staticEventSeriesEventId
      );
    });

    await test.step("Add another attendee to the Series and verify if the attendee is added to the event.", async () => {
      attendeeEmail = await seriesController.getRandomAttendeeEmail();
      await seriesController.addAttendeesToSeries(attendeeEmail);

      await seriesController.verifySeriesAttendeeNotAdded(
        staticEventSeriesEventId,
        attendeeEmail
      );
    });

    // await test.step("Remove attendee from Series.", async () => {
    //   await seriesController.removeAttendeeFromSeries(attendeeEmail);
    // });
  });

  test("TC002: Verify new attendee added to series is not added to completed Webinar.", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-709",
    });

    let attendeeEmail;

    await test.step("Fetch eventSeries Webinar id.", async () => {
      staticEventSeriesWebinarId = await DataUtl.getApplicationTestDataObj()[
        "completedWebinarIdForEventSeriesTesting"
      ];
      console.log("Static Webinar Id ->", staticEventSeriesWebinarId);
    });

    await test.step("Add attendee to the Event series and verify the user is added.", async () => {
      await seriesController.verifySeriesAttendeesAreAdded(
        staticEventSeriesId,
        staticEventSeriesWebinarId
      );
    });

    await test.step("Add another attendee to the Series and verify if the attendee is added to the event.", async () => {
      attendeeEmail = await seriesController.getRandomAttendeeEmail();
      await seriesController.addAttendeesToSeries(attendeeEmail);

      await seriesController.verifySeriesAttendeeNotAdded(
        staticEventSeriesWebinarId,
        attendeeEmail
      );
    });

    // await test.step("Remove attendee from Series.", async () => {
    //   await seriesController.removeAttendeeFromSeries(attendeeEmail);
    // });
  });
});
