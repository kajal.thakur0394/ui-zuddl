import { BrowserContext, APIRequestContext, Page } from "@playwright/test";
import { getEventDetails } from "../../../util/apiUtil";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { EventSeriesController } from "../../../controller/EventSeriesController";
import { RegistrationFormController } from "../../../controller/RegistrationFormController";
import conditionalFieldsTestData from "../../../test-data/conditional-fields-testdata/createConditionalFields.json";

export default class SeriesRegistrationSection {
  protected page: Page;
  protected eventId: string;
  protected eventTitle: string;
  protected landingPageId;

  protected eventInfoDto: EventInfoDTO;
  protected apiContext: APIRequestContext;
  protected browserContext: BrowserContext;
  protected seriesController: EventSeriesController;
  protected registrationFormController: RegistrationFormController;

  constructor(
    apiContext: APIRequestContext,
    eventId: string,
    eventTitle: string
  ) {
    this.apiContext = apiContext;
    this.eventId = eventId;
    this.eventTitle = eventTitle;

    this.eventInfoDto = new EventInfoDTO(this.eventId, eventTitle);
    this.seriesController = new EventSeriesController(apiContext, this.eventId);
  }

  async addCustomFieldsToRegistrationFormOfEventSeries() {
    console.log(
      "#### Adding Custom fields to Event Series Registration form #####"
    );

    this.eventInfoDto.eventEntryType = EventEntryType.REG_BASED;

    const event_details_api_resp = await (
      await getEventDetails(this.apiContext, this.eventId)
    ).json();
    console.log("event_details_api_resp :", event_details_api_resp);

    this.landingPageId = event_details_api_resp.eventLandingPageId;
    this.eventInfoDto.landingPageId = this.landingPageId;
    console.log("Landing Page Id ->", this.eventInfoDto.landingPageId);

    let customConditionalFieldsTestData =
      conditionalFieldsTestData["endToend-custom-conditional-fields-data"];

    console.log("######## adding custom fields to the events ##############");
    await this.seriesController.addCustomFieldsWithConditionToEvent(
      customConditionalFieldsTestData
    );

    let attendee_landing_page = this.eventInfoDto.attendeeLandingPage;
    console.log(attendee_landing_page);
  }
}
