import test, {
  APIRequestContext,
  BrowserContext,
  Page,
} from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { EventSeriesController } from "../../../controller/EventSeriesController";
import EventSeriesCommunicationEnum from "../../../enums/EventSeriesCommunicationEnum";
import { EventSeriesPage } from "../../../page-objects/new-org-side-pages/EventSeriesPage";
import { EventController } from "../../../controller/EventController";

const thumbnailPath = "eventSeries.jpeg";

test.describe("@event-series", async () => {
  // test.slow;
  // test.describe.configure({ retries: 2 });

  let organiserBrowserContext: BrowserContext;
  let organiserApiContext: APIRequestContext;
  let organiserPage: Page;
  let seriesController: EventSeriesController;
  let seriesSetupPageObject: EventSeriesPage;

  let staticSeriesId: string;

  test.beforeAll(async () => {
    await test.step("Initialize Organiser browser, API context and page.", async () => {
      organiserBrowserContext = await BrowserFactory.getBrowserContext({});
      organiserApiContext = organiserBrowserContext.request;
      organiserPage = await organiserBrowserContext.newPage();
    });

    await test.step("Fetch eventSeries Id.", async () => {
      staticSeriesId = await DataUtl.getApplicationTestDataObj()[
        "seriesIdForEventSeriesTesting"
      ];
    });

    await test.step("Initialize Event Series controller.", async () => {
      seriesController = new EventSeriesController(
        organiserApiContext,
        staticSeriesId
      );

      seriesSetupPageObject = new EventSeriesPage(
        organiserPage,
        staticSeriesId
      );
    });

    await test.step("Enable 'Add Event communication', 'Event Invitation' and 'Registration Confirmation' setting.", async () => {
      await seriesController.changeCommunicationSettings(
        EventSeriesCommunicationEnum.EVENT_SERIES_NEW_EVENT_ADDED,
        true
      );

      await seriesController.changeCommunicationSettings(
        EventSeriesCommunicationEnum.EVENT_INVITATION,
        true
      );

      await seriesController.changeCommunicationSettings(
        EventSeriesCommunicationEnum.REGISTRATION_CONFIRMATION,
        true
      );
    });
  });

  test.afterAll(async () => {
    await test.step("Close Organiser browser and page.", async () => {
      await organiserBrowserContext?.close();
    });
  });

  test("TC001: Verify magic link of attendee if checkin time increased at Event.", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-718",
    });

    let attendeeBrowserContext1: BrowserContext;
    let attendeeBrowserContext2: BrowserContext;
    let attendeePage1: Page;
    let attendeePage2: Page;
    let attendeeEmail1: string;
    let attendeeEmail2: string;

    let eventId;
    await test.step("Creating new Event.", async () => {
      eventId = await seriesSetupPageObject.addEventToTheSeries(
        organiserApiContext,
        "EventSeries-TestEvent"
      );
    });

    await test.step("Verify Series attendees are added to Upcoming Event.", async () => {
      await seriesController.verifySeriesAttendeesAreAdded(
        staticSeriesId,
        eventId
      );
    });

    await test.step("Fetch Series attendee list.", async () => {
      let attendees = await seriesController.getAttendeeList(staticSeriesId);
      attendeeEmail1 = attendees[0];
      console.log("Fetched Series email ->", attendeeEmail1);
    });

    await test.step("Update checkin time for user.", async () => {
      await seriesController.updateCheckInTime(eventId, 5);
      await seriesSetupPageObject.verifyUpdatedCheckinTime(eventId);
    });

    await test.step("Add an attendee to the Event and verify if the attendee is added to the Event.", async () => {
      attendeeEmail2 = await seriesController.getRandomAttendeeEmail();
      await seriesController.addAttendeeToEvent(eventId, attendeeEmail2);
      await seriesController.verifySeriesAttendeesAreAdded(
        staticSeriesId,
        eventId
      );
    });

    await test.step("Initialize Attendee browser, API context and page.", async () => {
      attendeeBrowserContext1 = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeeBrowserContext2 = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage1 = await attendeeBrowserContext1.newPage();
      attendeePage2 = await attendeeBrowserContext2.newPage();
    });

    await test.step("Fetch attendee magic link and verify if Live event side is accessible.", async () => {
      let magicLink =
        await seriesController.getMagicLinkFromEventWebinarAddedEmail(
          attendeeEmail1,
          false
        );

      await seriesController.verifyLiveEventSideIsAccessibleForAttendee(
        magicLink,
        attendeePage1
      );
    });

    await test.step("Fetch attendee magic link and verify if Live event side is accessible.", async () => {
      let magicLink =
        await seriesController.getMagicLinkFromEventInvitationEmail(
          attendeeEmail2
        );

      await seriesController.verifyLiveEventSideIsAccessibleForAttendee(
        magicLink,
        attendeePage2
      );
    });
  });

  test("TC002: Verify magic link of attendee if checkin time increased at Webinar.", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-719",
    });

    let attendeeBrowserContext1: BrowserContext;
    let attendeeBrowserContext2: BrowserContext;
    let attendeePage1: Page;
    let attendeePage2: Page;
    let attendeeEmail1: string;
    let attendeeEmail2: string;

    let webinarId: string;
    await test.step("Creating new Event.", async () => {
      webinarId = await seriesSetupPageObject.addWebinarToTheSeriesByApi(
        organiserApiContext,
        "EventSeries-TestWebinar"
      );
    });

    await test.step("Verify Series attendees are added to Upcoming Webinar.", async () => {
      await seriesController.verifySeriesAttendeesAreAdded(
        staticSeriesId,
        webinarId
      );
    });

    await test.step("Fetch Series attendee list.", async () => {
      let attendees = await seriesController.getAttendeeList(staticSeriesId);
      attendeeEmail1 = attendees[0];
      console.log("Fetched Series email ->", attendeeEmail1);
    });

    await test.step("Update checkin time for user.", async () => {
      await seriesController.updateCheckInTime(webinarId, 5);
      await seriesSetupPageObject.verifyUpdatedCheckinTime(webinarId);
    });

    await test.step("Add an attendee to the Webianr and verify if the attendee is added to the Webinar.", async () => {
      attendeeEmail2 = await seriesController.getRandomAttendeeEmail();
      await seriesController.addAttendeeToEvent(webinarId, attendeeEmail2);
      await seriesController.verifySeriesAttendeesAreAdded(
        staticSeriesId,
        webinarId
      );
    });

    await test.step("Initialize Attendee browser, API context and page.", async () => {
      attendeeBrowserContext1 = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeeBrowserContext2 = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage1 = await attendeeBrowserContext1.newPage();
      attendeePage2 = await attendeeBrowserContext2.newPage();
    });

    await test.step("Fetch attendee magic link and verify if Live event side is accessible.", async () => {
      let magicLink =
        await seriesController.getMagicLinkFromEventWebinarAddedEmail(
          attendeeEmail1,
          true
        );

      await seriesController.verifyLiveEventSideIsAccessibleForAttendee(
        magicLink,
        attendeePage1,
        true
      );
    });

    await test.step("Fetch attendee magic link and verify if Live event side is accessible.", async () => {
      let magicLink =
        await seriesController.getMagicLinkFromEventInvitationEmail(
          attendeeEmail2
        );

      await seriesController.verifyLiveEventSideIsAccessibleForAttendee(
        magicLink,
        attendeePage2,
        true
      );
    });
  });

  test.skip("TC003: Verify magic link of attendee outside checkin window for Event.", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-720",
    });

    let attendeeBrowserContext1: BrowserContext;
    let attendeeBrowserContext2: BrowserContext;
    let attendeePage1: Page;
    let attendeePage2: Page;
    let attendeeEmail1: string;
    let attendeeEmail2: string;

    let eventId;
    await test.step("Creating new Event.", async () => {
      eventId = await seriesSetupPageObject.addEventToTheSeries(
        organiserApiContext,
        "EventSeries-TestEvent",
        0,
        5
      );
    });

    let eventController = new EventController(organiserApiContext, eventId);
    await eventController.disableOnboardingChecksForAttendee();
    await eventController.disableOnboardingChecksForSpeaker();

    await test.step("Verify Series attendees are added to Upcoming Event.", async () => {
      await seriesController.verifySeriesAttendeesAreAdded(
        staticSeriesId,
        eventId
      );
    });

    await test.step("Fetch Series attendee list.", async () => {
      let attendees = await seriesController.getAttendeeList(staticSeriesId);
      attendeeEmail1 = attendees[0];
      console.log("Fetched Series email ->", attendeeEmail1);
    });

    await test.step("Update checkin time for user.", async () => {
      await seriesController.updateCheckInTime(eventId, 1);
      await seriesSetupPageObject.verifyUpdatedCheckinTime(eventId);
    });

    await test.step("Add an attendee to the Event and verify if the attendee is added to the Event.", async () => {
      attendeeEmail2 = await seriesController.getRandomAttendeeEmail();
      await seriesController.addAttendeeToEvent(eventId, attendeeEmail2);
      await seriesController.verifySeriesAttendeesAreAdded(
        staticSeriesId,
        eventId
      );
    });

    await test.step("Initialize Attendee browser, API context and page.", async () => {
      attendeeBrowserContext1 = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeeBrowserContext2 = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage1 = await attendeeBrowserContext1.newPage();
      attendeePage2 = await attendeeBrowserContext2.newPage();
    });

    await test.step("Fetch attendee magic link and verify if Live event side is accessible.", async () => {
      let magicLink =
        await seriesController.getMagicLinkFromEventWebinarAddedEmail(
          attendeeEmail1,
          false
        );

      await seriesController.verifyMagicLinkNavigatesToRegistrationPage(
        magicLink,
        attendeePage1
      );
    });

    await test.step("Fetch attendee magic link and verify if Live event side is accessible.", async () => {
      let magicLink =
        await seriesController.getMagicLinkFromEventInvitationEmail(
          attendeeEmail2
        );

      await seriesController.verifyMagicLinkNavigatesToRegistrationPage(
        magicLink,
        attendeePage2
      );
    });
  });

  test.skip("TC004: Verify magic link of attendee outside checkin window for Webinar.", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-721",
    });

    let attendeeBrowserContext1: BrowserContext;
    let attendeeBrowserContext2: BrowserContext;
    let attendeePage1: Page;
    let attendeePage2: Page;
    let attendeeEmail1: string;
    let attendeeEmail2: string;

    let webinarId: string;
    await test.step("Creating new Event.", async () => {
      webinarId = await seriesSetupPageObject.addWebinarToTheSeriesByApi(
        organiserApiContext,
        "EventSeries-TestWebinar"
      );
    });

    await test.step("Verify Series attendees are added to Upcoming Webinar.", async () => {
      await seriesController.verifySeriesAttendeesAreAdded(
        staticSeriesId,
        webinarId
      );
    });

    await test.step("Fetch Series attendee list.", async () => {
      let attendees = await seriesController.getAttendeeList(staticSeriesId);
      attendeeEmail1 = attendees[0];
      console.log("Fetched Series email ->", attendeeEmail1);
    });

    await test.step("Update checkin time for user.", async () => {
      await seriesController.updateCheckInTime(webinarId, 1);
      await seriesSetupPageObject.verifyUpdatedCheckinTime(webinarId);
    });

    await test.step("Add an attendee to the Webianr and verify if the attendee is added to the Webinar.", async () => {
      attendeeEmail2 = await seriesController.getRandomAttendeeEmail();
      await seriesController.addAttendeeToEvent(webinarId, attendeeEmail2);
      await seriesController.verifySeriesAttendeesAreAdded(
        staticSeriesId,
        webinarId
      );
    });

    await test.step("Initialize Attendee browser, API context and page.", async () => {
      attendeeBrowserContext1 = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeeBrowserContext2 = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage1 = await attendeeBrowserContext1.newPage();
      attendeePage2 = await attendeeBrowserContext2.newPage();
    });

    await test.step("Fetch attendee magic link and verify if Live event side is accessible.", async () => {
      let magicLink =
        await seriesController.getMagicLinkFromEventWebinarAddedEmail(
          attendeeEmail1,
          true
        );

      await seriesController.verifyMagicLinkNavigatesToRegistrationPage(
        magicLink,
        attendeePage1
      );
    });

    await test.step("Fetch attendee magic link and verify if Live event side is accessible.", async () => {
      let magicLink =
        await seriesController.getMagicLinkFromEventInvitationEmail(
          attendeeEmail2
        );

      await seriesController.verifyMagicLinkNavigatesToRegistrationPage(
        magicLink,
        attendeePage2
      );
    });
  });
});
