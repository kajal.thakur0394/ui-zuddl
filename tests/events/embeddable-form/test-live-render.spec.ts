import test, {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { LandingPage } from "../../../page-objects/events-pages/embedded-form/LandingPage";
import {
  deleteAttendeeFromEvent,
  getEventDetails,
  updateEventEntryType,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { validateEditProfileConditionalFieldDataFromDB } from "../../../util/validation-util";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe("@registration @embeddable-form", async () => {
  test.describe.configure({ retries: 2 });
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let orgPage: Page;
  let userInfoDTO: UserInfoDTO;
  let event_id: any;
  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgPage = await orgBrowserContext.newPage();
    orgApiContext = await orgBrowserContext.request;
    event_id =
      DataUtl.getApplicationTestDataObj()["eventIdForEmbedFormTesting"];

    let eventController = new EventController(orgApiContext, event_id);
    await eventController.changeEventStartAndEndDateTime({
      deltaByHoursInStartTime: 1,
      deltaByHoursInEndTime: 3,
    });
  });
  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`TC001: opening embedded form with default only fields`, async ({
    page,
  }) => {
    // await page.pause();
    let landingPage = new LandingPage(page);
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    let embeddableFormUrl =
      DataUtl.getApplicationTestDataObj()["embedFormSiteUrl"];
    console.log(`embed form site url to test is ${embeddableFormUrl}`);
    await landingPage.load(embeddableFormUrl);
    // await page.pause();
    await (
      await landingPage.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData._conditionalFieldFormData1,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
        clickOnSubmitButton: true,
        clickOnDesclaimerButton: true,
      }
    );
    await test.step("Fetch Registered user custom fields data from the DB", async () => {
      const fetchedCustomFieldsDataFromDb =
        await QueryUtil.fetchCustomFieldsFilledByRegisteredUser(
          event_id,
          attendeeEmail
        );
      console.log(
        `registered user fetched custom fields data is ${fetchedCustomFieldsDataFromDb}`
      );

      await expect(
        await validateEditProfileConditionalFieldDataFromDB(
          fetchedCustomFieldsDataFromDb,
          userInfoDTO.userRegFormData._conditionalFieldFormData1,
          {
            checkMandatoryFields: true,
            checkOptionalFields: true,
            fieldsToSkip: ["First Name", "Last Name", "Email"],
          }
        ),
        "comparing user custom field test data with DB Data"
      ).toBeTruthy();
    });
  });

  test(`TC002: Same user registering again`, async ({ page }) => {
    // await page.pause();
    let landingPage = new LandingPage(page);
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    let embeddableFormUrl =
      DataUtl.getApplicationTestDataObj()["embedFormSiteUrl"];
    console.log(`embed form site url to test is ${embeddableFormUrl}`);
    // await page.pause();
    await test.step("Filling fields in embedded form", async () => {
      await landingPage.load(embeddableFormUrl);
      await (
        await landingPage.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData._conditionalFieldFormData1,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
          clickOnSubmitButton: true,
          clickOnDesclaimerButton: true,
        }
      );
    });

    await test.step("Fetch and verify Registered user custom fields data from the DB", async () => {
      const fetchedCustomFieldsDataFromDb =
        await QueryUtil.fetchCustomFieldsFilledByRegisteredUser(
          event_id,
          attendeeEmail
        );
      console.log(
        `registered user fetched custom fields data is ${fetchedCustomFieldsDataFromDb}`
      );

      await expect(
        await validateEditProfileConditionalFieldDataFromDB(
          fetchedCustomFieldsDataFromDb,
          userInfoDTO.userRegFormData._conditionalFieldFormData1,
          {
            checkMandatoryFields: true,
            checkOptionalFields: true,
            fieldsToSkip: ["First Name", "Last Name", "Email"],
          }
        ),
        "comparing user custom field test data with DB Data"
      ).toBeTruthy();
    });
    await test.step("Re-Filling fields in embedded form and Yo've already regsitered button visisble", async () => {
      await landingPage.load(embeddableFormUrl);
      await (
        await landingPage.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData._conditionalFieldFormData1,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
          clickOnSubmitButton: true,
          clickOnDesclaimerButton: true,
        }
      );
      await expect(
        page.locator("text=You've already registered")
      ).toBeVisible();
    });
  });

  test(`TC003: Deleted user trying to registering again`, async ({ page }) => {
    // await page.pause();
    let landingPage = new LandingPage(page);
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    let embeddableFormUrl =
      DataUtl.getApplicationTestDataObj()["embedFormSiteUrl"];
    console.log(`embed form site url to test is ${embeddableFormUrl}`);
    // await page.pause();
    await test.step("Filling fields in embedded form", async () => {
      await landingPage.load(embeddableFormUrl);
      await (
        await landingPage.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData._conditionalFieldFormData1,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
          clickOnSubmitButton: true,
          clickOnDesclaimerButton: true,
        }
      );
    });

    await test.step("Fetch and verify Registered user custom fields data from the DB", async () => {
      const fetchedCustomFieldsDataFromDb =
        await QueryUtil.fetchCustomFieldsFilledByRegisteredUser(
          event_id,
          attendeeEmail
        );
      console.log(
        `registered user fetched custom fields data is ${fetchedCustomFieldsDataFromDb}`
      );

      expect(
        await validateEditProfileConditionalFieldDataFromDB(
          fetchedCustomFieldsDataFromDb,
          userInfoDTO.userRegFormData._conditionalFieldFormData1,
          {
            checkMandatoryFields: true,
            checkOptionalFields: true,
            fieldsToSkip: ["First Name", "Last Name", "Email"],
          }
        ),
        "comparing user custom field test data with DB Data"
      ).toBeTruthy();
    });

    await test.step(`Now delete the user from the event`, async () => {
      await test.step(`delete the user via api`, async () => {
        await deleteAttendeeFromEvent(orgApiContext, event_id, attendeeEmail);
      });
    });

    await test.step("Re-Filling fields in embedded form and Yo've been removed from event to be visible", async () => {
      await landingPage.load(embeddableFormUrl);
      await (
        await landingPage.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData._conditionalFieldFormData1,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
          clickOnSubmitButton: true,
          clickOnDesclaimerButton: true,
        }
      );
      await expect(
        page.locator("text=You have been removed from the event.")
      ).toBeVisible();
    });
  });

  // user registration with same email but in upper case,leading and trailing spaces.
  test(`TC004: Same user registering again with same email but in upper case, leading and trailing spaces`, async ({
    page,
  }) => {
    // await page.pause();
    let landingPage = new LandingPage(page);
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    let embeddableFormUrl =
      DataUtl.getApplicationTestDataObj()["embedFormSiteUrl"];
    console.log(`embed form site url to test is ${embeddableFormUrl}`);
    // await page.pause();
    await test.step("Filling fields in embedded form", async () => {
      await landingPage.load(embeddableFormUrl);
      await (
        await landingPage.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData._conditionalFieldFormData1,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
          clickOnSubmitButton: true,
          clickOnDesclaimerButton: true,
        }
      );
    });

    await test.step("Fetch and verify Registered user custom fields data from the DB", async () => {
      const fetchedCustomFieldsDataFromDb =
        await QueryUtil.fetchCustomFieldsFilledByRegisteredUser(
          event_id,
          attendeeEmail
        );
      console.log(
        `registered user fetched custom fields data is ${fetchedCustomFieldsDataFromDb}`
      );

      expect(
        await validateEditProfileConditionalFieldDataFromDB(
          fetchedCustomFieldsDataFromDb,
          userInfoDTO.userRegFormData._conditionalFieldFormData1,
          {
            checkMandatoryFields: true,
            checkOptionalFields: true,
            fieldsToSkip: ["First Name", "Last Name", "Email"],
          }
        ),
        "comparing user custom field test data with DB Data"
      ).toBeTruthy();
    });
    await test.step("Re-Filling fields in embedded form with email in upper case", async () => {
      // Changing email to upper case
      userInfoDTO = new UserInfoDTO(
        attendeeEmail.toUpperCase(),
        "",
        "prateek",
        "parashar",
        true
      );
      await landingPage.load(embeddableFormUrl);
      await (
        await landingPage.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData._conditionalFieldFormData1,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
          clickOnSubmitButton: true,
          clickOnDesclaimerButton: true,
        }
      );
      await expect(
        page.locator("text=You've already registered")
      ).toBeVisible();
    });
    await test.step("Re-Filling fields in embedded form with email having leading space", async () => {
      // Changing email to upper case
      userInfoDTO = new UserInfoDTO(
        " " + attendeeEmail,
        "",
        "prateek",
        "parashar",
        true
      );
      await landingPage.load(embeddableFormUrl);
      await (
        await landingPage.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData._conditionalFieldFormData1,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
          clickOnSubmitButton: true,
          clickOnDesclaimerButton: true,
        }
      );
      await expect(
        page.locator("text=You've already registered")
      ).toBeVisible();
    });
    await test.step("Re-Filling fields in embedded form with email having trailing space", async () => {
      // Changing email to upper case
      userInfoDTO = new UserInfoDTO(
        attendeeEmail + " ",
        "",
        "prateek",
        "parashar",
        true
      );
      await landingPage.load(embeddableFormUrl);
      await (
        await landingPage.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData._conditionalFieldFormData1,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
          clickOnSubmitButton: true,
          clickOnDesclaimerButton: true,
        }
      );
      await expect(
        page.locator("text=You've already registered")
      ).toBeVisible();
    });
  });

  // when event is already over test case
  test(`TC005: When event is already over, user should not be able to register`, async ({
    page,
  }) => {
    /***
     * Test case: If event is over, expectation is embed form should not allow registration
     * hence as soon as page is loaded, it should show msg that registrations are closed
     */
    //Update event date time for static event
    let eventStartTime = new Date();
    console.log(`current data time is ${eventStartTime}`);
    let eventController = new EventController(orgApiContext, event_id);
    await eventController.changeEventStartAndEndDateTime({
      deltaByHoursInStartTime: -8,
      deltaByHoursInEndTime: -4,
    });
    let landingPage = new LandingPage(page);
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    let embeddableFormUrl =
      DataUtl.getApplicationTestDataObj()["embedFormSiteUrl"];
    console.log(`embed form site url to test is ${embeddableFormUrl}`);
    await landingPage.load(embeddableFormUrl);
    await test.step(`verify attendee is shown that event is over , hence registration is closed`, async () => {
      const messageLocator = page.getByText("This event has already ended.");
      await expect(messageLocator).toBeVisible();
    });
  });

  test(`TC006: When event entry type is changed to invite only, user should not be able to register`, async ({
    page,
  }) => {
    const event_details_api_resp = await (
      await getEventDetails(orgApiContext, event_id)
    ).json();
    let landing_page_id = event_details_api_resp.eventLandingPageId;
    await test.step("Changing event entry type to invite only.", async () => {
      await updateEventEntryType(
        orgApiContext,
        event_id,
        landing_page_id,
        EventEntryType.INVITE_ONLY
      );
    });
    let landingPage = new LandingPage(page);
    let embeddableFormUrl =
      DataUtl.getApplicationTestDataObj()["embedFormSiteUrl"];
    console.log(`embed form site url to test is ${embeddableFormUrl}`);
    await landingPage.load(embeddableFormUrl);

    await test.step(`Verify attendee is shown that registration is not available for the event.`, async () => {
      await landingPage.verifyRegistrationNotAvailable();
    });

    await test.step("Changing event entry type back to reg based", async () => {
      await updateEventEntryType(
        orgApiContext,
        event_id,
        landing_page_id,
        EventEntryType.REG_BASED
      );
    });
  });

  test(`TC0007: @attendee if email restriction is applied, same should reflect on embded registration widget `, async ({
    page,
  }) => {
    /**
     * in an invite only event
     * verify if an uninvited user tries to login
     * he sees restricted entry screen for attendee
     */
    let landingPage: LandingPage;
    const attendeeEmail = "prateek@gmail.com";
    let eventController = new EventController(orgApiContext, event_id);
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    await test.step(`Apply email restriction to not allow @gmail.com to this event`, async () => {
      await eventController.applyEmailRestrictionToNotAllowBusinessEmails();
    });
    await test.step(`Attendee open embed reg form embedded  landing page`, async () => {
      landingPage = new LandingPage(page);
      let embeddableFormUrl =
        DataUtl.getApplicationTestDataObj()["embedFormSiteUrl"];
      await landingPage.load(embeddableFormUrl);
    });

    await test.step(`Attendee try to register with email ${attendeeEmail} which is restricted and hence it should be blocked`, async () => {
      await (
        await landingPage.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData._conditionalFieldFormData1,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
          clickOnSubmitButton: true,
          clickOnDesclaimerButton: true,
        }
      );
    });
    await test.step(`Email is domain restricted message should be visible.`, async () => {
      await expect(
        page.locator(
          "text=This email domain is restricted. Try a different email."
        )
      ).toBeVisible();
    });
  });

  test(`@utm-params TC008: verify if website embedding reg form opened with appedned utm param in url, same get recorded in event registration table for successfull registration`, async ({
    page,
  }) => {
    // await page.pause();
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-240/validation-for-embed-form",
    });
    let landingPage = new LandingPage(page);
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    let embeddableFormUrl =
      DataUtl.getApplicationTestDataObj()["embedFormSiteUrl"];
    let embeddableFormUrlWithUtmParams: string;

    const utm_source = "automation";
    const utm_medium = "playwright";

    await test.step(`Appending the embed form url with utm params`, async () => {
      //append this landing page with UTM sources
      embeddableFormUrlWithUtmParams = `${embeddableFormUrl}?utm_source=${utm_source}&utm_medium=${utm_medium}`;
      console.log(
        `embeddableFormUrlWithUtmParams landing page with utm params appended is ${embeddableFormUrlWithUtmParams}`
      );
    });
    console.log(`embed form site url to test is ${embeddableFormUrl}`);
    await landingPage.load(embeddableFormUrlWithUtmParams!);
    // await page.pause();
    await (
      await landingPage.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData._conditionalFieldFormData1,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
        clickOnSubmitButton: true,
        clickOnDesclaimerButton: true,
      }
    );
    await test.step("Fetch Registered user custom fields data from the DB", async () => {
      const fetchedCustomFieldsDataFromDb =
        await QueryUtil.fetchCustomFieldsFilledByRegisteredUser(
          event_id,
          attendeeEmail
        );
      console.log(
        `registered user fetched custom fields data is ${fetchedCustomFieldsDataFromDb}`
      );

      await expect(
        await validateEditProfileConditionalFieldDataFromDB(
          fetchedCustomFieldsDataFromDb,
          userInfoDTO.userRegFormData._conditionalFieldFormData1,
          {
            checkMandatoryFields: true,
            checkOptionalFields: true,
            fieldsToSkip: ["First Name", "Last Name", "Email"],
          }
        ),
        "comparing user custom field test data with DB Data"
      ).toBeTruthy();
    });

    await test.step(`Now fetching the user entry from event_registration table and validate the utm params`, async () => {
      //verify in DB event_registration table, the utm param exists as expected
      const userRegDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          event_id,
          attendeeEmail
        );
      //fetch the utm param from db for registration record
      const utmParamsFromDb = userRegDataFromDb["utm"];
      //validate the utm params
      //const utmParamJson = JSON.parse(utmParamsFromDb);
      //validate the key and value
      await test.step(`validating the utm_medium param from db entry`, async () => {
        expect(
          utmParamsFromDb["utm_medium"],
          `expecting utm_medium to be ${utm_medium}`
        ).toBe(utm_medium);
      });

      await test.step(`validating the utm_source param from db entry`, async () => {
        expect(
          utmParamsFromDb["utm_source"],
          `expecting utm_source to be ${utm_source}`
        ).toBe(utm_source);
      });
    });
  });

  test(`TC009: User registering without checking desclaimer should throw error @desclaimer @embeddable`, async ({
    page,
  }) => {
    // await page.pause();
    let landingPage = new LandingPage(page);
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    let embeddableFormUrl =
      DataUtl.getApplicationTestDataObj()["embedFormSiteUrl"];
    console.log(`embed form site url to test is ${embeddableFormUrl}`);

    await test.step("Load embed form", async () => {
      await landingPage.load(embeddableFormUrl);
    });

    await test.step("Filling fields in embedded form", async () => {
      await (
        await landingPage.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData._conditionalFieldFormData1,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
          clickOnSubmitButton: true,
          clickOnDesclaimerButton: false,
        }
      );
    });

    await test.step("Verify error is shown to fill the required fields", async () => {
      await landingPage.verifyDisclaimer();
    });
  });
});
