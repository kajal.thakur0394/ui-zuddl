import test, {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
} from "@playwright/test";
import { FormPage } from "../../../page-objects/new-org-side-pages/RegistrationFormTabPage";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";

test.describe("@registration @embeddable-form", async () => {
  test.describe.configure({ retries: 2 });
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let orgPage: Page;
  let embeddableFormUrl: string;
  let event_id: any;
  let setupUrl: string;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgPage = await orgBrowserContext.newPage();
    orgApiContext = await orgBrowserContext.request;
    event_id =
      DataUtl.getApplicationTestDataObj()["eventIdForEmbedFormTesting"];
    embeddableFormUrl = DataUtl.getApplicationTestDataObj()["embedFormSiteUrl"];
    setupUrl = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/event/${event_id}`;
  });
  test.afterEach(async () => {
    // Closing or browser context
    await orgBrowserContext?.close();
  });

  test(`TC001:Copy the html code and validate for the presence of important elements`, async ({}) => {
    await orgPage.goto(setupUrl);
    let formTab = new FormPage(orgPage);
    await formTab.openFormTab();
    let embeddableFormPage = await formTab.switchToEnbeddableFormTab();

    await test.step("Verify embed button is visible", async () => {
      await embeddableFormPage.verifyEmbedButtonIsVisible();
    });

    await test.step("Click on embed button and verify html code is visible", async () => {
      await embeddableFormPage.clickEmbedButton();
      await embeddableFormPage.verifyHtmlCodeisVisible();
    });

    await test.step("Verify the contents of HTML code", async () => {
      let dataToverify = await embeddableFormPage.copyHtmlCode();
      let eventIdFetched = dataToverify[0];
      expect(eventIdFetched, "Verify event Id is same as Event id").toBe(
        event_id
      );
      let DataEnvFetched = dataToverify[1];
      expect(DataEnvFetched, "Verify event Id is same as Event id").toBe(
        process.env.run_env
      );
    });
  });
});
