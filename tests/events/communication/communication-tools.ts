import test, { APIRequestContext, expect } from "@playwright/test";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { EventController } from "../../../controller/EventController";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import EventSettingID from "../../../enums/EventSettingEnum";
import EventRole from "../../../enums/eventRoleEnum";
import {
  fetchMailasaurEmailObject,
  recievedNewEmail,
} from "../../../util/emailUtil";
import { emailTemplateValidation } from "../../../util/emailTemplateValidation";
import {
  validateEventStartTime,
  validateIcsData,
  validateTicketPdfContent,
} from "../../../util/validation-util";
import { deleteFile, parseICS } from "../../../util/commonUtil";
import { getIcsAttachment, getTicketAttachment } from "../../../util/emailUtil";
import ical from "node-ical";

import pdfParse from "pdf-parse";
import * as path from "path";
import * as fs from "fs";

export default class CommunicationHelper {
  protected orgApiRequestContext: APIRequestContext;
  protected eventController: EventController;
  protected eventId: any;
  protected userInfoDto: UserInfoDTO;
  protected eventInfoDto: EventInfoDTO;
  protected isFlow: boolean;
  protected isInPersonEvent: boolean;

  constructor(
    orgApiReuqestContext: APIRequestContext,
    eventId: any,
    userInfoDto: UserInfoDTO,
    eventInfoDto: EventInfoDTO,
    isFlow: boolean = false,
    isInPersonEvent: boolean = false
  ) {
    this.orgApiRequestContext = orgApiReuqestContext;
    this.eventId = eventId;
    this.userInfoDto = userInfoDto;
    this.eventInfoDto = eventInfoDto;
    this.isFlow = isFlow;
    this.eventController = new EventController(
      this.orgApiRequestContext,
      this.eventId
    );
    this.isInPersonEvent = isInPersonEvent;
  }

  async toggleEmailInvites(
    role: EventRole,
    enableEmail: boolean,
    calendarBlock: boolean
  ) {
    calendarBlock = calendarBlock && enableEmail;
    await enableDisableEmailTrigger(
      this.orgApiRequestContext,
      this.eventId,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      calendarBlock,
      role,
      enableEmail
    );
    await new Promise((r) => setTimeout(r, 15000));
  }

  async verifyInvitationEmailContents(
    expectICSFile: boolean,
    timezone: string = "(IST)"
  ) {
    let emailInvite: emailTemplateValidation;
    await test.step("fetching email from mailosaur", async () => {
      let fetched_email = await fetchMailasaurEmailObject(
        this.userInfoDto.userEmail,
        this.eventInfoDto.getInviteEmailSubj()
      );

      emailInvite = new emailTemplateValidation(fetched_email);
    });

    await test.step("Expect timezone code to be present in email", async () => {
      expect(emailInvite.htmlBody).toContain(timezone);
    });

    await test.step("Validating name in email", async () => {
      let name = await emailInvite.fetchAttendeeName();
      console.log("The name is:", name);
      console.log(
        "The expected name is:",
        `${this.userInfoDto.userFirstName} ${this.userInfoDto.userFirstName}`
      );
      expect(name).toContain(
        `${this.userInfoDto.userFirstName} ${this.userInfoDto.userLastName}`
      );
    });

    await test.step("Verifying Event start time from email recieved after registering", async () => {
      await validateEventStartTime(emailInvite, this.eventInfoDto);
    });

    await test.step("Verify ICS link and text from email recieved.", async () => {
      let icsContents = await emailInvite.fetchIcsContents();
      let outlookinvite_link = icsContents[0];
      let outlookinvitetext = icsContents[1];

      console.log("The ics contents are:", icsContents);

      expect(outlookinvitetext, "Verifying text of outlook ICS file").toContain(
        "Outlook"
      );

      await test.step("Verifying Outlook ICS files link and text from email recieved after registering", async () => {
        const outlookIcsData = await ical.async.fromURL(outlookinvite_link);
        console.log("Outlook ics Data: ", outlookIcsData);
        await test.step("Validate content", async () => {
          await validateIcsData(
            outlookIcsData,
            this.eventInfoDto,
            this.isInPersonEvent
          );
        });
      });

      let appleinvite_link = icsContents[2];
      let appleinvitetext = icsContents[3];

      expect(appleinvitetext, "Verifying text of apple ICS file").toContain(
        "Apple"
      );

      await test.step("Verifying Apple ICS files link and text from email recieved after registering", async () => {
        const appleIcsData = await ical.async.fromURL(appleinvite_link);
        console.log("Apple ics Data: ", appleIcsData);
        await test.step("Validate content", async () => {
          await validateIcsData(
            appleIcsData,
            this.eventInfoDto,
            this.isInPersonEvent
          );
        });
      });
    });
    if (expectICSFile) {
      await test.step("Validating attachment ICS file from email recieved after registering", async () => {
        let emailAttachmentFilename = await getIcsAttachment(
          this.userInfoDto.userEmail,
          this.eventInfoDto.getInviteEmailSubj()
        );
        console.log("Filename of ICS is:", emailAttachmentFilename);
        const emailAttachmentData = parseICS(emailAttachmentFilename);
        await validateIcsData(
          emailAttachmentData,
          this.eventInfoDto,
          this.isInPersonEvent
        );
      });
    }
  }

  async verifyEventRescheduleEmailContents(expectEmail: boolean) {
    let stepTitle = expectEmail ? "Expecting email" : "Not expecting email";
    await test.step(`${stepTitle}`, async () => {
      let receivedEmail = await recievedNewEmail(
        this.userInfoDto.userEmail,
        this.eventInfoDto.rescheduleEmailSubj,
        40
      );
      expect(receivedEmail).toBe(expectEmail);
    });
    if (expectEmail) {
      await test.step("Fetching Email body from email recieved after registering", async () => {
        let fetched_email = await fetchMailasaurEmailObject(
          this.userInfoDto.userEmail,
          this.eventInfoDto.rescheduleEmailSubj
        );
      });

      await test.step("Verify ICS Details", async () => {
        let emailAttachmentFilename = await getIcsAttachment(
          this.userInfoDto.userEmail,
          this.eventInfoDto.rescheduleEmailSubj
        );
        console.log("Filename of ICS is:", emailAttachmentFilename);
        const emailAttachmentData = parseICS(emailAttachmentFilename);
        await validateIcsData(
          emailAttachmentData,
          this.eventInfoDto,
          this.isInPersonEvent
        );
      });
    }
  }

  async verifyConfirmationEmailContents(
    expectICSFile: boolean,
    timezone: string = "(IST)",
    expectTicketPDF: boolean = false
  ) {
    let emailInvite: emailTemplateValidation;
    await test.step("fetching email from mailosaur", async () => {
      let fetched_email = await fetchMailasaurEmailObject(
        this.userInfoDto.userEmail,
        this.eventInfoDto.registrationConfirmationEmailSubj
      );

      emailInvite = new emailTemplateValidation(fetched_email);
    });

    await test.step("Expect timezone code to be present in email", async () => {
      expect(emailInvite.htmlBody).toContain(timezone);
    });

    await test.step("Validating name in email", async () => {
      let name: string;
      if (this.isFlow) {
        name = await emailInvite.fetchAttendeeNameFlow();
      } else {
        name = await emailInvite.fetchAttendeeName();
      }
      console.log("The name is:", name);
      console.log(
        "The expected name is:",
        `${this.userInfoDto.userFirstName} ${this.userInfoDto.userFirstName}`
      );
      expect(name).toContain(
        `${this.userInfoDto.userFirstName} ${this.userInfoDto.userLastName}`
      );
    });

    await test.step("Verifying Event start time from email recieved after registering", async () => {
      await validateEventStartTime(emailInvite, this.eventInfoDto);
    });

    await test.step("Verify ICS link and text from email recieved.", async () => {
      let icsContents = await emailInvite.fetchIcsContents();
      let outlookinvite_link = icsContents[0];
      let outlookinvitetext = icsContents[1];

      console.log("The ics contents are:", icsContents);

      expect(outlookinvitetext, "Verifying text of outlook ICS file").toContain(
        "Outlook"
      );

      await test.step("Verifying Outlook ICS files link and text from email recieved after registering", async () => {
        const outlookIcsData = await ical.async.fromURL(outlookinvite_link);
        console.log("Outlook ics Data: ", outlookIcsData);
        await test.step("Validate content", async () => {
          await validateIcsData(
            outlookIcsData,
            this.eventInfoDto,
            this.isInPersonEvent
          );
        });
      });

      let appleinvite_link = icsContents[2];
      let appleinvitetext = icsContents[3];

      expect(appleinvitetext, "Verifying text of apple ICS file").toContain(
        "Apple"
      );

      await test.step("Verifying Apple ICS files link and text from email recieved after registering", async () => {
        const appleIcsData = await ical.async.fromURL(appleinvite_link);
        console.log("Apple ics Data: ", appleIcsData);
        await test.step("Validate content", async () => {
          await validateIcsData(
            appleIcsData,
            this.eventInfoDto,
            this.isInPersonEvent
          );
        });
      });
    });
    if (expectICSFile) {
      await test.step("Validating attachment ICS file from email recieved after registering", async () => {
        let emailAttachmentFilename = await getIcsAttachment(
          this.userInfoDto.userEmail,
          this.eventInfoDto.registrationConfirmationEmailSubj
        );
        console.log("Filename of ICS is:", emailAttachmentFilename);
        const emailAttachmentData = parseICS(emailAttachmentFilename);
        await validateIcsData(
          emailAttachmentData,
          this.eventInfoDto,
          this.isInPersonEvent
        );
        // delete emailAttachmentFilename;
        deleteFile(emailAttachmentFilename);
      });
    }
  }
  async verifyConfirmationEmailContentsTicketing(
    expectICSFile: boolean,
    timezone: string = "(IST)",
    ticketCodeExpected: string = "",
    expectTicketPDF: boolean = false,
    ticketPrice: string = "$0.0"
  ) {
    let emailInvite: emailTemplateValidation;
    await test.step("fetching email from mailosaur", async () => {
      let fetched_email = await fetchMailasaurEmailObject(
        this.userInfoDto.userEmail,
        this.eventInfoDto.registrationConfirmationEmailSubj
      );

      emailInvite = new emailTemplateValidation(fetched_email);
    });

    await test.step("Expect timezone code to be present in email", async () => {
      expect(emailInvite.htmlBody).toContain(timezone);
    });

    await test.step("Validating name in email", async () => {
      let name: string;
      if (this.isFlow) {
        name = await emailInvite.fetchAttendeeNameFlow();
      } else {
        name = await emailInvite.fetchAttendeeName();
      }
      console.log("The name is:", name);
      console.log(
        "The expected name is:",
        `${this.userInfoDto.userFirstName} ${this.userInfoDto.userFirstName}`
      );
      expect(name).toContain(
        `${this.userInfoDto.userFirstName} ${this.userInfoDto.userLastName}`
      );
    });

    await test.step("Verifying ticket code in email", async () => {
      const ticketCode = await emailInvite.fetchTicketCode();
      if (ticketCodeExpected !== "") {
        expect(ticketCodeExpected).toContain(ticketCode);
      } else {
        console.log("Expected ticket code is empty. Skipping validation.");
        console.log("Ticket code is:", ticketCode);
      }
    });

    await test.step("Verifying ticket img tag in email and URL", async () => {
      const ticketImgUrl = await emailInvite.verifyTicketCode(
        ticketCodeExpected
      );
    });

    if (expectTicketPDF) {
      await test.step("Validating attachment ticket PDF file from email recieved after registering", async () => {
        let emailAttachmentFilename = await getTicketAttachment(
          this.userInfoDto.userEmail,
          this.eventInfoDto.registrationConfirmationEmailSubj
        );
        console.log("Filename of ticket PDF is:", emailAttachmentFilename);

        validateTicketPdfContent(emailAttachmentFilename, {
          ticketCode: ticketCodeExpected,
          startTime: this.eventInfoDto.startDateTime,
          eventTitle: this.eventInfoDto.eventTitle,
          ticketPrice: ticketPrice,
          timeZone: timezone,
        });
      });
    }

    await test.step("Verifying Event start time from email recieved after registering", async () => {
      await validateEventStartTime(emailInvite, this.eventInfoDto);
    });

    await test.step("Verify ICS link and text from email recieved.", async () => {
      let icsContents = await emailInvite.fetchIcsContents();
      let outlookinvite_link = icsContents[0];
      let outlookinvitetext = icsContents[1];

      console.log("The ics contents are:", icsContents);

      expect(outlookinvitetext, "Verifying text of outlook ICS file").toContain(
        "Outlook"
      );

      await test.step("Verifying Outlook ICS files link and text from email recieved after registering", async () => {
        const outlookIcsData = await ical.async.fromURL(outlookinvite_link);
        console.log("Outlook ics Data: ", outlookIcsData);
        await test.step("Validate content", async () => {
          await validateIcsData(
            outlookIcsData,
            this.eventInfoDto,
            this.isInPersonEvent
          );
        });
      });

      let appleinvite_link = icsContents[2];
      let appleinvitetext = icsContents[3];

      expect(appleinvitetext, "Verifying text of apple ICS file").toContain(
        "Apple"
      );

      await test.step("Verifying Apple ICS files link and text from email recieved after registering", async () => {
        const appleIcsData = await ical.async.fromURL(appleinvite_link);
        console.log("Apple ics Data: ", appleIcsData);
        await test.step("Validate content", async () => {
          await validateIcsData(
            appleIcsData,
            this.eventInfoDto,
            this.isInPersonEvent
          );
        });
      });
    });
    if (expectICSFile) {
      await test.step("Validating attachment ICS file from email recieved after registering", async () => {
        let emailAttachmentFilename = await getIcsAttachment(
          this.userInfoDto.userEmail,
          this.eventInfoDto.registrationConfirmationEmailSubj
        );
        console.log("Filename of ICS is:", emailAttachmentFilename);
        const emailAttachmentData = parseICS(emailAttachmentFilename);
        await validateIcsData(
          emailAttachmentData,
          this.eventInfoDto,
          this.isInPersonEvent
        );
        // delete emailAttachmentFilename;
        deleteFile(emailAttachmentFilename);
      });
    }
  }

  updateEventInfoDto(eventInfoDto: EventInfoDTO) {
    this.eventInfoDto = eventInfoDto;
  }

  async verifyTicketPDFContents() {
    let ticketPdfPath = await getIcsAttachment(
      this.userInfoDto.userEmail,
      this.eventInfoDto.registrationConfirmationEmailSubj
    );
    let dataBuffer = fs.readFileSync(ticketPdfPath);
    let data = await pdfParse(dataBuffer);
    console.log(data.text);
  }
}
