import test, {
  APIRequestContext,
  BrowserContext,
  Page,
} from "@playwright/test";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { EventController } from "../../../controller/EventController";
import { EventSetupPage } from "../../../page-objects/new-org-side-pages/event-setup";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import EventRole from "../../../enums/eventRoleEnum";
import CommunicationHelper from "./communication-tools";

test.describe.parallel(
  "Reschedule event email scenario @communication",
  {
    tag: "@smoke",
  },
  async () => {
    let orgBrowserContext: BrowserContext;
    let eventTitle: string;
    let eventId: any;
    let organiserApiContext: APIRequestContext;
    let eventInfoDto: EventInfoDTO;
    let userInfoDto: UserInfoDTO;
    let eventController: EventController;
    let eventInfo: any;
    let organiserPage: Page;
    let eventSetupPage: EventSetupPage;

    test.describe.configure({ retries: 2 });

    test.beforeEach(async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({});
      organiserApiContext = await orgBrowserContext.request;
      await test.step("Creating new event.", async () => {
        eventId = await EventController.generateNewEvent(organiserApiContext, {
          event_title: DataUtl.getRandomEventTitle(),
          default_settings: false,
        });
      });

      await test.step("Initialising new event controller.", async () => {
        eventController = new EventController(organiserApiContext, eventId);
        eventInfoDto = new EventInfoDTO(eventId, eventTitle, true);
      });

      await test.step(`Get event details`, async () => {
        eventInfo = await (await eventController.getEventInfo()).json();
        console.log("Event Info: ", eventInfo);
        eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
        eventInfoDto.startDateTime = eventInfo.startDateTime;
      });

      await test.step("Initiate organiser event setup page", async () => {
        organiserPage = await orgBrowserContext.newPage();
        let setupPage = `${
          DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
        }/event/${eventId}/event-setup/basic`;
        await organiserPage.goto(setupPage);
        eventSetupPage = new EventSetupPage(organiserPage);
      });
    });

    test.afterEach(async () => {
      console.log("In teardown, closing the browser context");
      await eventController.cancelAnEvent();
      await orgBrowserContext?.close();
    });

    const resendInvite = [true, false];
    let calendarBlock: boolean[];
    for (const resend of resendInvite) {
      // For attendee
      if (resend) {
        calendarBlock = [true, false];
      } else {
        calendarBlock = [false];
      }
      for (const calendar of calendarBlock) {
        test(`@attendee @mail @reschedule @invited Reschedule event email scenario for attendee with calendar block: ${calendar} and resend invite: ${resend}`, async () => {
          test.info().annotations.push({
            type: "TC",
            description: "https://linear.app/zuddl/issue/QAT-354",
          });

          test.info().annotations.push({
            type: "TC",
            description: "https://linear.app/zuddl/issue/QAT-356",
          });
          test.info().annotations.push({
            type: "TC",
            description: "https://linear.app/zuddl/issue/QAT-353",
          });

          const attendeeEmail = DataUtl.getRandomAttendeeEmail();
          userInfoDto = new UserInfoDTO(
            attendeeEmail,
            "",
            "Automation",
            "TestAttendee"
          );
          let communicationHelper = new CommunicationHelper(
            organiserApiContext,
            eventId,
            userInfoDto,
            eventInfoDto
          );

          await test.step("Toggle email invite", async () => {
            await communicationHelper.toggleEmailInvites(
              EventRole.ATTENDEE,
              true,
              calendar
            );
          });

          await test.step(`Invite attendee ${attendeeEmail} to event`, async () => {
            await eventController.inviteAttendeeToTheEvent(attendeeEmail);
          });

          await test.step("Fetching Email body from email recieved after registering", async () => {
            await communicationHelper.verifyInvitationEmailContents(calendar);
          });

          await test.step("Change event start date", async () => {
            await eventSetupPage.changeEventStartTimeFromUI(
              "11",
              "33",
              "AM",
              eventId,
              calendar,
              resend
            );
            await eventSetupPage.page.waitForTimeout(10000);
          });

          await test.step("Get eventinfo from API", async () => {
            eventInfo = await (await eventController.getEventInfo()).json();
            console.log("Event Info: ", eventInfo);
            eventInfoDto.startDateTime = eventInfo.startDateTime;
          });

          await communicationHelper.verifyEventRescheduleEmailContents(
            calendar && resend
          );
        });
      }
    }

    for (const resend of resendInvite) {
      if (resend) {
        calendarBlock = [true, false];
      } else {
        calendarBlock = [false];
      }
      for (const calendar of calendarBlock) {
        test(`@Speaker @mail @reschedule @invited Reschedule event email scenario for speaker with calendar block: ${calendar} and resend invite: ${resend}`, async () => {
          test.info().annotations.push({
            type: "TC",
            description: "https://linear.app/zuddl/issue/QAT-355",
          });
          test.info().annotations.push({
            type: "TC",
            description: "https://linear.app/zuddl/issue/QAT-356",
          });
          test.info().annotations.push({
            type: "TC",
            description: "https://linear.app/zuddl/issue/QAT-353",
          });

          const speakerEmail = DataUtl.getRandomSpeakerEmail();
          userInfoDto = new UserInfoDTO(
            speakerEmail,
            "",
            "Automation",
            "TestAttendee"
          );
          let communicationHelper = new CommunicationHelper(
            organiserApiContext,
            eventId,
            userInfoDto,
            eventInfoDto
          );

          await test.step("Toggle email invite", async () => {
            await communicationHelper.toggleEmailInvites(
              EventRole.SPEAKER,
              resend,
              calendar
            );
          });

          await test.step(`Invite speaker ${speakerEmail} to event`, async () => {
            await eventController.inviteSpeakerToTheEventByApi(
              speakerEmail,
              "Automation",
              "TestAttendee"
            );
          });

          await test.step("Fetching Email body from email recieved after registering", async () => {
            await communicationHelper.verifyInvitationEmailContents(calendar);
          });

          await test.step("Change event start date", async () => {
            await eventSetupPage.changeEventStartTimeFromUI(
              "11",
              "57",
              "AM",
              eventId,
              resend && calendar
            );
            await eventSetupPage.page.waitForTimeout(5000);
          });

          await test.step("Get eventinfo from API", async () => {
            eventInfo = await (await eventController.getEventInfo()).json();
            console.log("Event Info: ", eventInfo);
            eventInfoDto.startDateTime = eventInfo.startDateTime;
            console.log("Event Info DTO: ", eventInfoDto);
            console.log(`Event start date time: ${eventInfoDto.startDateTime}`);
            communicationHelper.updateEventInfoDto(eventInfoDto);
          });

          // toggle email invite for attendee
          await test.step("Toggle email invite for attendee", async () => {
            await communicationHelper.toggleEmailInvites(
              EventRole.ATTENDEE,
              resend,
              calendar
            );
          });

          // add an attendee to the event
          let attendeeEmail = DataUtl.getRandomAttendeeEmail();
          let attendeeInfoDto = new UserInfoDTO(
            attendeeEmail,
            "",
            "Automation",
            "TestAttendee"
          );

          await test.step("Add an attendee to the event", async () => {
            await eventController.inviteAttendeeToTheEvent(attendeeEmail);
          });

          let attendeeCommunicationHelper = new CommunicationHelper(
            organiserApiContext,
            eventId,
            attendeeInfoDto,
            eventInfoDto
          );

          await test.step("Fetching Email body from email recieved after registering", async () => {
            await attendeeCommunicationHelper.verifyInvitationEmailContents(
              calendar
            );
          });

          await test.step("Get eventinfo from API", async () => {
            eventInfo = await (await eventController.getEventInfo()).json();
            console.log("Event Info: ", eventInfo);
            eventInfoDto.startDateTime = eventInfo.startDateTime;
          });

          // update the event start time from UI

          await test.step("Change event start date", async () => {
            await eventSetupPage.page.reload();
            await eventSetupPage.page.waitForTimeout(8000);
            await eventSetupPage.changeEventStartTimeFromUI(
              "11",
              "33",
              "AM",
              eventId,
              calendar,
              resend
            );
          });

          await test.step("Get eventinfo from API", async () => {
            eventInfo = await (await eventController.getEventInfo()).json();
            console.log("Event Info: ", eventInfo);
            eventInfoDto.startDateTime = eventInfo.startDateTime;
            attendeeCommunicationHelper.updateEventInfoDto(eventInfoDto);
            communicationHelper.updateEventInfoDto(eventInfoDto);
          });

          await communicationHelper.verifyEventRescheduleEmailContents(
            calendar && resend
          );

          await test.step("Fetching Email body from email recieved after reschedule", async () => {
            await attendeeCommunicationHelper.verifyEventRescheduleEmailContents(
              calendar && resend
            );
          });

          await test.step("Get eventinfo from API", async () => {
            eventInfo = await (await eventController.getEventInfo()).json();
            console.log("Event Info: ", eventInfo);
            eventInfoDto.startDateTime = eventInfo.startDateTime;
            attendeeCommunicationHelper.updateEventInfoDto(eventInfoDto);
            communicationHelper.updateEventInfoDto(eventInfoDto);
          });

          await test.step("Fetching Email body from email recieved after reschedule", async () => {
            Promise.all([
              attendeeCommunicationHelper.verifyEventRescheduleEmailContents(
                calendar && resend
              ),
              communicationHelper.verifyEventRescheduleEmailContents(
                calendar && resend
              ),
            ]);
          });
        });
      }
    }
  }
);
