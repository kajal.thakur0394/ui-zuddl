import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { EventController } from "../../../controller/EventController";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import EventRole from "../../../enums/eventRoleEnum";
import CommunicationHelper from "./communication-tools";
import { getTimezoneList } from "../../../test-data/timezone-testdata";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { updateLandingPageType } from "../../../util/apiUtil";
import LandingPageType from "../../../enums/landingPageEnum";
import EventEntryType from "../../../enums/eventEntryEnum";
import { LandingPageTwo } from "../../../page-objects/events-pages/landing-page-2/LandingPageTwo";

test.describe(
  "Verify timezone in invitation email.",
  {
    tag: "@smoke",
  },
  () => {
    let orgBrowserContext: BrowserContext;
    let eventId: any;
    let orgApiContext: APIRequestContext;
    let eventController: EventController;
    let eventInfo: any;
    let eventInfoDto: EventInfoDTO;
    let userInfoDto: UserInfoDTO;

    const timezone = getTimezoneList()[0];

    test.describe.configure({ retries: 2 });

    test.beforeEach(async () => {
      console.log("before all");
      orgBrowserContext = await BrowserFactory.getBrowserContext({});
      orgApiContext = orgBrowserContext.request;

      // create new event
      await test.step("Create a new event", async () => {
        eventId = await EventController.generateNewEvent(orgApiContext, {
          event_title: DataUtl.getRandomEventTitle(),
          timezone: timezone,
        });
        eventController = new EventController(orgApiContext, eventId);
      });

      await test.step("Get event info", async () => {
        let eventInfo = await (await eventController.getEventInfo()).json();
        console.log("eventInfo", eventInfo);
      });

      await test.step(`Get event details`, async () => {
        eventInfo = await (await eventController.getEventInfo()).json();
        console.log("Event Info: ", eventInfo);
        eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
        eventInfoDto.startDateTime = eventInfo.startDateTime;
        eventInfoDto.timeZone = eventInfo.tz;
      });
    });

    test.afterEach(async () => {
      await eventController.cancelAnEvent();
      await orgBrowserContext.close();
    });

    test("TC001: Verify timezone in invitation email for attendee.", async () => {
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-722",
      });
      console.log("Verify timezone in invitation email.");
      const attendeeEmail = DataUtl.getRandomAttendeeEmail();
      userInfoDto = new UserInfoDTO(
        attendeeEmail,
        "",
        "Automation",
        "TestAttendee"
      );
      let communicationHelper = new CommunicationHelper(
        orgApiContext,
        eventId,
        userInfoDto,
        eventInfoDto
      );

      await test.step("Toggle email invite", async () => {
        await communicationHelper.toggleEmailInvites(
          EventRole.ATTENDEE,
          true,
          true
        );
      });

      await test.step(`Invite attendee ${attendeeEmail} to event`, async () => {
        await eventController.inviteAttendeeToTheEvent(attendeeEmail);
      });

      await test.step("Fetching Email body from email recieved after registering", async () => {
        await communicationHelper.verifyInvitationEmailContents(true, "(GST)");
      });
    });

    test("TC002: Verify timezone in invitation email for speaker.", async () => {
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-722",
      });

      console.log("Verify timezone in invitation email.");
      const speakerEmail = DataUtl.getRandomSpeakerEmail();
      userInfoDto = new UserInfoDTO(
        speakerEmail,
        "",
        "Automation",
        "TestAttendee"
      );
      let communicationHelper = new CommunicationHelper(
        orgApiContext,
        eventId,
        userInfoDto,
        eventInfoDto
      );

      await test.step("Toggle email invite", async () => {
        await communicationHelper.toggleEmailInvites(
          EventRole.SPEAKER,
          true,
          true
        );
      });

      await test.step(`Invite speaker ${speakerEmail} to event`, async () => {
        await eventController.inviteSpeakerToTheEventByApi(
          speakerEmail,
          "Automation",
          "TestAttendee"
        );
      });

      await test.step("Fetching Email body from email recieved after registering", async () => {
        await communicationHelper.verifyInvitationEmailContents(true, "(GST)");
      });
    });

    test("TC003: Verify timezone in confirmation email for attendee using LP1.", async () => {
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-736",
      });
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-723",
      });

      await enableDisableEmailTrigger(
        orgApiContext,
        eventId,
        EventSettingID.EmailOnRegistration,
        true, // enabling calendar invite
        EventRole.ATTENDEE,
        true
      );

      const landingPage = eventInfoDto.attendeeLandingPage;
      const attendeeEmail = DataUtl.getRandomAttendeeEmail();
      userInfoDto = new UserInfoDTO(attendeeEmail, "", "prateek", "QA");

      let attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      let attendeePage = await attendeeBrowserContext.newPage();
      let landingPageOne: LandingPageOne;

      await test.step("Attendee opens landing page", async () => {
        await attendeePage.goto(landingPage);
        landingPageOne = new LandingPageOne(attendeePage);
      });

      await test.step("Attendee registers for the event", async () => {
        await (
          await landingPageOne.getRegistrationFormComponent()
        ).fillUpTheRegistrationForm(
          userInfoDto.userRegFormData.userRegFormDataForDefaultFields
        );

        // verify that the event invite has been sent screen is shown to user
        await expect(
          landingPageOne.page.locator(
            "div[class^='styles-module__logoHeaderBody']"
          )
        ).toContainText("You've successfully registered!");
      });

      let communicationHelper = new CommunicationHelper(
        orgApiContext,
        eventId,
        userInfoDto,
        eventInfoDto
      );

      await test.step("Fetching Email body from email recieved after registering", async () => {
        await communicationHelper.verifyConfirmationEmailContents(
          true,
          "(GST)"
        );
      });
    });

    test("TC004: Verify timezone in confirmation email for attendee using LP2.", async () => {
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-737",
      });
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-723",
      });

      // Update landing page

      await enableDisableEmailTrigger(
        orgApiContext,
        eventId,
        EventSettingID.EmailOnRegistration,
        true, // enabling calendar invite
        EventRole.ATTENDEE,
        true
      );
      eventInfo = await (await eventController.getEventInfo()).json();
      const landingPageId = await new EventController(
        orgApiContext,
        eventId
      ).getEventLandingPageId();
      await updateLandingPageType(
        orgApiContext,
        eventId,
        landingPageId,
        LandingPageType.STYLE2,
        "true",
        EventEntryType.REG_BASED
      );

      const landingPage = eventInfoDto.attendeeLandingPage;

      const attendeeEmail = DataUtl.getRandomAttendeeEmail();
      userInfoDto = new UserInfoDTO(attendeeEmail, "", "prateek", "QA");

      let attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      let attendeePage = await attendeeBrowserContext.newPage();
      let landingPageTwo: LandingPageTwo;

      await test.step("Attendee opens landing page", async () => {
        await attendeePage.goto(landingPage);
        landingPageTwo = new LandingPageTwo(attendeePage);
      });

      await test.step("Attendee registers for the event", async () => {
        await (
          await landingPageTwo.getRegistrationFormComponent()
        ).fillUpTheRegistrationForm(
          userInfoDto.userRegFormData.userRegFormDataForDefaultFields
        );
        await expect(
          landingPageTwo.page.locator(
            "div[class^='styles-module__registrationMessageC']"
          )
        ).toContainText("You've successfully registered! ");
      });

      let communicationHelper = new CommunicationHelper(
        orgApiContext,
        eventId,
        userInfoDto,
        eventInfoDto
      );

      await test.step("Fetching Email body from email recieved after registering", async () => {
        await communicationHelper.verifyConfirmationEmailContents(
          true,
          "(GST)"
        );
      });
    });
  }
);
