import { test, BrowserContext, APIRequestContext } from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { CommunicationsPage } from "../../../page-objects/new-org-side-pages/CommunicationPage";
import { EventController } from "../../../controller/EventController";
import { DataUtl } from "../../../util/dataUtil";
import testData from "../../../test-data/defaultToggleConfig.json";

// enable comm trigger from Org ui settings and verify calendarBlock status
test.describe("Calendar Block Toggle State", async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventController: EventController;
  let eventId;

  test.beforeAll(async () => {
    console.log("before all");
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = orgBrowserContext.request;

    // create new event
    await test.step("Create a new event", async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
      });
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step("Get event info", async () => {
      let eventInfo = await (await eventController.getEventInfo()).json();
      console.log("eventInfo", eventInfo);
    });

    await test.step("Update event start and end dates", async () => {
      await eventController.changeEventStartAndEndDateTime({
        deltaByDayInStartDate: 10,
        deltaByDayInEndDate: 12,
      });
      const eventInfo = await (await eventController.getEventInfo()).json();
      console.log("eventInfo", eventInfo);
    });
  });

  test.afterAll(async () => {
    await eventController.cancelAnEvent();
    await orgBrowserContext.close();
  });

  test(
    "TC001: Verify Calendar Block Toggle State",
    {
      tag: "@smoke",
    },
    async () => {
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-699",
      });
      console.log("Verify Calendar Block Toggle State");
      let orgPage;
      await test.step("Create a new org page", async () => {
        orgPage = await orgBrowserContext.newPage();
        let commSetupUrl = `${
          DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
        }/event/${eventId}/communication/standard`;
        await orgPage.goto(commSetupUrl);
        await orgPage.waitForTimeout(5000);
      });
      let communicationPage: CommunicationsPage;

      await test.step("Initialise Communication Page", async () => {
        communicationPage = new CommunicationsPage(orgPage);
      });

      const users = ["Attendees", "Speakers"];
      for (let user of users) {
        await test.step(`Toggling to ${user}`, async () => {
          communicationPage.toggleView(user);
        });
        for (let toggle of testData[user]) {
          await test.step(`Verify Calendar Block Icon for ${toggle.toggleTitle}`, async () => {
            await test.step("Enable a toggle", async () => {
              if (toggle.toggleTitle != "Event Canceled") {
                await communicationPage.toggleSetting(toggle.toggleTitle);
              }
            });

            await test.step("Verify Calendar Block Icon", async () => {
              await communicationPage.verifyCalendarInviteIcon(
                toggle.toggleTitle,
                toggle.defaultCalendarBlockStatus
              );
            });
          });
        }
      }
    }
  );
});
