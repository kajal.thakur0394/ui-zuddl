import { test, BrowserContext, APIRequestContext } from "@playwright/test";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  createNewEvent,
  getEventDetails,
  enableMagicLinkInEvent,
  inviteAttendeeByAPI,
  updateEventLandingPageDetails,
  updateEventEntryType,
  cancelEvent,
} from "../../../../../util/apiUtil";
import { fetchAttendeeInviteMagicLink } from "../../../../../util/emailUtil";
import { LandingPageOne } from "../../../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { UserInfoDTO } from "../../../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import EventEntryType from "../../../../../enums/eventEntryEnum";
import { UserEventRoleDto } from "../../../../../dto/userEventRoleDto";
import EventRole from "../../../../../enums/eventRoleEnum";
import { DataUtl } from "../../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../../util/email-validation-api-util";
import EventSettingID from "../../../../../enums/EventSettingEnum";

test.describe
  .parallel("Invite Based | Magic link Enabled | Attendee Ladning Page | @Existing-users @inviteonly", () => {
  test.describe.configure({ retries: 2 });

  let org_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let attendeeEmail: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let landingPageOne: LandingPageOne;

  test.beforeEach(async () => {
    // setup org context
    org_browser_context = await BrowserFactory.getBrowserContext({});
    organiserApiContext = await org_browser_context.request;
    // org_session = await org_browser_context.newPage()
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await updateEventEntryType(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );

    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;

    attendee_landing_page = eventInfoDto.attendeeLandingPage;

    // user info
    attendeeEmail =
      DataUtl.getApplicationTestDataObj()["attendeeEmailWithAccountEntry"]; // existing email as an attendee
    let attendeePwd =
      DataUtl.getApplicationTestDataObj()["attendeePasswordWithAccountEntry"];
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      attendeePwd,
      "prateek",
      "parashar",
      true
    );

    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
  });

  test.afterEach(async () => {
    await cancelEvent(organiserApiContext, event_id);
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
  });

  test("TC001: Uninvited attendee try to login via otp to event @attendee @otp", async ({
    page,
  }) => {
    landingPageOne = new LandingPageOne(page);
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await landingPageOne.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageOne.getRegistrationFormComponent()).isNotVisible();
    // now click on already registered
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    // now enter your email
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForMagicLink(userEventRoleDto);
    await (
      await landingPageOne.getRestrcitedEventScreen()
    ).isPrivateEventScreenVisibleForOTPFlow();
  });

  test("TC002: Un-invited attendee try to login via password to event @attendee @password", async ({
    page,
  }) => {
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      eventEntryType: EventEntryType.INVITE_ONLY,
      attendeeAuthOptions: ["EMAIL"],
    });

    landingPageOne = new LandingPageOne(page);
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await landingPageOne.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageOne.getRegistrationFormComponent()).isNotVisible();
    // now click on already registered
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    // now enter your email
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await (
      await landingPageOne.getRestrcitedEventScreen()
    ).isPrivateEventScreenVisible();
  });

  test("TC003: Invited attendee try to login via otp to event @attendee @otp", async ({
    page,
  }) => {
    landingPageOne = new LandingPageOne(page);
    // invite this user to this event as an attendee
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    await landingPageOne.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageOne.getRegistrationFormComponent()).isNotVisible();
    // now click on already registered
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    // now enter your email
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForMagicLink(userEventRoleDto);
    await landingPageOne.isSuccessToastMessageVisible();
    let magicLink = await fetchAttendeeInviteMagicLink(
      attendeeEmail,
      eventInfoDto.registrationConfirmationEmailSubj
    );
    await landingPageOne.load(magicLink);
    await landingPageOne.isLobbyLoaded();
  });

  test("@lp1 TC004: existing attendee if invited then, he should be able to login via password to event, if toggle is enabled @attendee @password", async ({
    page,
  }) => {
    // enable password login
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      eventEntryType: EventEntryType.INVITE_ONLY,
      attendeeAuthOptions: ["EMAIL"],
    });

    landingPageOne = new LandingPageOne(page);
    // invite this user to this event as an attendee
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    userInfoDto.hasEntryInAccountTable = true;
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    await landingPageOne.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageOne.getRegistrationFormComponent()).isNotVisible();
    // now click on already registered
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC005: Organiser try to login via password to event @attendee @password", async ({
    page,
  }) => {
    // enable password login
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      eventEntryType: EventEntryType.INVITE_ONLY,
      attendeeAuthOptions: ["EMAIL"],
    });

    landingPageOne = new LandingPageOne(page);
    attendeeEmail = DataUtl.getApplicationTestDataObj()["eventsOrganiserEmail"]; // existing email as an attendee
    let attendeePwd = DataUtl.getApplicationTestDataObj()["organiserPassword"];
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      attendeePwd,
      "prateek",
      "parashar",
      true
    );

    // invite this user to this event as an attendee
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ORGANISER,
      true
    );
    await landingPageOne.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageOne.getRegistrationFormComponent()).isNotVisible();
    // now click on already registered
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isStageLoaded();
  });

  //open bug for it : https://linear.app/zuddl/issue/BUG-1425/organiser-of-an-event-if-tries-to-login-from-magic-link-he-is-not-able
  test.fixme(
    "TC006: Organiser try to login via magiclink to event @attendee @magiclink",
    async ({ page }) => {
      landingPageOne = new LandingPageOne(page);
      attendeeEmail =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserEmail"]; // existing email as an attendee
      let attendeePwd =
        DataUtl.getApplicationTestDataObj()["organiserPassword"];
      userInfoDto = new UserInfoDTO(
        attendeeEmail,
        attendeePwd,
        "prateek",
        "parashar",
        true
      );
      // invite this user to this event as an attendee
      await inviteAttendeeByAPI(
        organiserApiContext,
        eventInfoDto.eventId,
        userInfoDto.userEmail
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.ORGANISER,
        true
      );
      await landingPageOne.load(attendee_landing_page);
      // since the event is invite only reg form should not be visible
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).isNotVisible();
      // now click on already registered
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).clickOnAlreadyRegisteredButton();
      await (
        await landingPageOne.getLoginOptionsComponent()
      ).submitEmailForMagicLink(userEventRoleDto);
      let magicLink = await fetchAttendeeInviteMagicLink(
        attendeeEmail,
        eventInfoDto.registrationConfirmationEmailSubj
      );
      await landingPageOne.load(magicLink);
      await landingPageOne.clickOnEnterNowButton();
      await landingPageOne.isLobbyLoaded();
    }
  );

  test("TC007: Organiser with logged in cookies visit event landing page @organiser", async () => {
    let org_session = await org_browser_context.newPage();
    landingPageOne = new LandingPageOne(org_session);
    await landingPageOne.load(attendee_landing_page);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isStageLoaded();
  });
});
