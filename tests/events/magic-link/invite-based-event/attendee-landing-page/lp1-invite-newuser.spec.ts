import {
  test,
  Page,
  BrowserContext,
  APIRequestContext,
  expect,
} from "@playwright/test";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  cancelEvent,
  createNewEvent,
  getEventDetails,
  inviteAttendeeByAPI,
  updateEventEntryType,
  updateEventLandingPageDetails,
} from "../../../../../util/apiUtil";
import { fetchAttendeeInviteMagicLink } from "../../../../../util/emailUtil";
import { LandingPageOne } from "../../../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { UserInfoDTO } from "../../../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import EventEntryType from "../../../../../enums/eventEntryEnum";
import { UserEventRoleDto } from "../../../../../dto/userEventRoleDto";
import EventRole from "../../../../../enums/eventRoleEnum";
import { DataUtl } from "../../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../../util/email-validation-api-util";
import EventSettingID from "../../../../../enums/EventSettingEnum";

test.describe
  .parallel("Invite Based | Magic link Enabled | Attendee Landing Page | @New-users @inviteonly", () => {
  let org_browser_context: BrowserContext;
  let attendee_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let userDefaultRegFieldData: object;
  let attendeeEmail: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let landingPageOne: LandingPageOne;
  let page: Page;

  test.beforeEach(async ({ context }) => {
    // setup org context
    org_browser_context = await BrowserFactory.getBrowserContext({});
    organiserApiContext = org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await updateEventEntryType(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;

    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    page = await context.newPage();
  });

  test.afterEach(async () => {
    await cancelEvent(organiserApiContext, event_id);
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
  });

  test("TC001: Uninvited attendee try to login via otp to event @attendee @otp", async () => {
    landingPageOne = new LandingPageOne(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await landingPageOne.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageOne.getRegistrationFormComponent()).isNotVisible();
    // now click on already registered
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    // now enter your email
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForMagicLink(userEventRoleDto);
    await (
      await landingPageOne.getRestrcitedEventScreen()
    ).isPrivateEventScreenVisibleForOTPFlow();
  });

  test("TC002: Un-invited attendee try to login via password to event when password option is enabled. @attendee @password", async () => {
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      eventEntryType: EventEntryType.INVITE_ONLY,
      attendeeAuthOptions: ["EMAIL"],
    });

    landingPageOne = new LandingPageOne(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await landingPageOne.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageOne.getRegistrationFormComponent()).isNotVisible();
    // now click on already registered
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    // now enter your email
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await (
      await landingPageOne.getRestrcitedEventScreen()
    ).isPrivateEventScreenVisible();
  });

  test("TC003: Invited attendee try to login via otp to event @attendee @otp", async () => {
    landingPageOne = new LandingPageOne(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );

    // invite this user to this event as an attendee
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    userEventRoleDto.hasUserRegisteredToThisEvent = true;

    await landingPageOne.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageOne.getRegistrationFormComponent()).isNotVisible();
    // now click on already registered
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    // now enter your email
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForMagicLink(userEventRoleDto);
    await landingPageOne.isSuccessToastMessageVisible();
    let magicLink = await fetchAttendeeInviteMagicLink(
      attendeeEmail,
      eventInfoDto.registrationConfirmationEmailSubj
    );
    await landingPageOne.load(magicLink);
    await landingPageOne.isLobbyLoaded();
  });

  test("@lp1 TC004: new attendee if invited, attempts to login via password should trigger otp flow if enabled @attendee @password", async () => {
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      eventEntryType: EventEntryType.INVITE_ONLY,
      attendeeAuthOptions: ["EMAIL"],
    });

    landingPageOne = new LandingPageOne(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );

    // invite this user to this event as an attendee
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    userEventRoleDto.hasUserRegisteredToThisEvent = true;

    await landingPageOne.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageOne.getRegistrationFormComponent()).isNotVisible();
    // now click on already registered
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });
});
