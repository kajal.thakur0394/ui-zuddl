import {
  test,
  Page,
  BrowserContext,
  APIRequestContext,
} from "@playwright/test";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  cancelEvent,
  createNewEvent,
  getEventDetails,
  inviteAttendeeByAPI,
  updateEventEntryType,
  updateEventLandingPageDetails,
  updateLandingPageType,
} from "../../../../../util/apiUtil";
import { fetchAttendeeInviteMagicLink } from "../../../../../util/emailUtil";
import { LandingPageTwo } from "../../../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
import { UserInfoDTO } from "../../../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import EventEntryType from "../../../../../enums/eventEntryEnum";
import { UserEventRoleDto } from "../../../../../dto/userEventRoleDto";
import EventRole from "../../../../../enums/eventRoleEnum";
import { DataUtl } from "../../../../../util/dataUtil";
import LandingPageType from "../../../../../enums/landingPageEnum";

test.describe
  .parallel("Invite Based | Magic link Enabled | Attendee Ladning Page|@Landing-page 2 @Existing-users @inviteonly", () => {
  let org_session: Page;
  let org_browser_context: BrowserContext;
  let attendee_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let userDefaultRegFieldData: object;
  let attendeeEmail: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let landingPageTwo: LandingPageTwo;
  let page: Page;

  test.beforeEach(async ({ context }) => {
    // setup org context
    org_browser_context = await BrowserFactory.getBrowserContext({});
    organiserApiContext = await org_browser_context.request;
    // org_session = await org_browser_context.newPage()
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;

    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "true",
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;

    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;

    // user info
    attendeeEmail =
      DataUtl.getApplicationTestDataObj()["attendeeEmailWithAccountEntry"]; // existing email as an attendee
    let attendeePwd =
      DataUtl.getApplicationTestDataObj()["attendeePasswordWithAccountEntry"];
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      attendeePwd,
      "prateek",
      "parashar",
      true
    );
    page = await context.newPage();
  });

  test.afterEach(async () => {
    await cancelEvent(organiserApiContext, event_id);
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
  });

  test("TC001: Uninvited attendee try to login via otp to event @attendee @Landing-page 2 @otp", async () => {
    landingPageTwo = new LandingPageTwo(page);
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await landingPageTwo.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageTwo.getRegistrationFormComponent()).isNotVisible();
    // now click on already registered
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    // now enter your email
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForMagicLink(userEventRoleDto);
    await (
      await landingPageTwo.getRestrcitedEventScreen()
    ).isPrivateEventScreenVisibleForOTPFlow;
  });

  test("TC002: Un-invited attendee try to login via password to event @attendee @Landing-page 2 @password", async () => {
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      eventEntryType: EventEntryType.INVITE_ONLY,
      attendeeAuthOptions: ["EMAIL"],
    });

    landingPageTwo = new LandingPageTwo(page);
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await landingPageTwo.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageTwo.getRegistrationFormComponent()).isNotVisible();
    // now click on already registered
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    // now enter your email
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await (
      await landingPageTwo.getRestrcitedEventScreen()
    ).isPrivateEventScreenVisible();
  });
  /*
    Expected failure on staging, should work fine once the bug is fixed
    */
  test("TC003: Invited attendee try to login via otp to event @attendee @Landing-page 2 @otp", async () => {
    landingPageTwo = new LandingPageTwo(page);
    // invite this user to this event as an attendee
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    await landingPageTwo.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageTwo.getRegistrationFormComponent()).isNotVisible();
    // now click on already registered
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    // now enter your email
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForMagicLink(userEventRoleDto);
    await landingPageTwo.isSuccessToastMessageVisible();
    let magicLink = await fetchAttendeeInviteMagicLink(
      attendeeEmail,
      eventInfoDto.registrationConfirmationEmailSubj
    );
    await landingPageTwo.load(magicLink);
    await landingPageTwo.isLobbyLoaded();
  });
  /*
    Expected failure on staging, should work fine once the bug is fixed
    */
  test("TC004: Invited attendee try to login via password to event @attendee @Landing-page 2 @password", async () => {
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      eventEntryType: EventEntryType.INVITE_ONLY,
      attendeeAuthOptions: ["EMAIL"],
    });

    landingPageTwo = new LandingPageTwo(page);
    // invite this user to this event as an attendee
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    await landingPageTwo.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageTwo.getRegistrationFormComponent()).isNotVisible();
    // now click on already registered
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isLobbyLoaded();
  });

  // test('TC005: Organiser try to login via password to event @attendee @Landing-page 2 @password',async({page})=>{
  //     landingPageTwo = new LandingPageTwo(page)
  //     attendeeEmail = DataUtl.getApplicationTestDataObj()['eventsOrganiserEmail'] // existing email as an attendee
  //     let attendeePwd = DataUtl.getApplicationTestDataObj()['organiserPassword']
  //     userInfoDto = new UserInfoDTO(attendeeEmail,attendeePwd,"prateek","parashar",true)

  //     // invite this user to this event as an attendee
  //     await inviteAttendeeByAPI(organiserApiContext,eventInfoDto.eventId,userInfoDto.userEmail)
  //     userEventRoleDto = new UserEventRoleDto(userInfoDto,eventInfoDto,EventRole.ORGANISER,true)
  //     await landingPageTwo.load(attendee_landing_page)
  //     // since the event is invite only reg form should not be visible
  //     await (await landingPageTwo.getRegistrationFormComponent()).isNotVisible()
  //     // now click on already registered
  //     await (await landingPageTwo.getRegistrationFormComponent()).clickOnAlreadyRegisteredButton()
  //     await (await landingPageTwo.getLoginOptionsComponent()).submitEmailForPasswordLogin(userEventRoleDto)
  //     await landingPageTwo.clickOnEnterNowButton()
  //     await page.waitForURL(/stage/)
  // })

  //right now organiser when try to login from alp, we treat him as uninvited - to fix
  test.fixme(
    "TC006: Organiser try to login via password to event @attendee @Landing-page 2 @otp",
    async ({ page }) => {
      landingPageTwo = new LandingPageTwo(page);
      attendeeEmail =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserEmail"]; // existing email as an attendee
      let attendeePwd =
        DataUtl.getApplicationTestDataObj()["organiserPassword"];
      userInfoDto = new UserInfoDTO(
        attendeeEmail,
        attendeePwd,
        "prateek",
        "parashar",
        true
      );
      // invite this user to this event as an attendee
      await inviteAttendeeByAPI(
        organiserApiContext,
        eventInfoDto.eventId,
        userInfoDto.userEmail
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.ORGANISER,
        true
      );
      await landingPageTwo.load(attendee_landing_page);
      // since the event is invite only reg form should not be visible
      await (
        await landingPageTwo.getRegistrationFormComponent()
      ).isNotVisible();
      // now click on already registered
      await (
        await landingPageTwo.getRegistrationFormComponent()
      ).clickOnAlreadyRegisteredButton();
      await (
        await landingPageTwo.getLoginOptionsComponent()
      ).submitEmailForMagicLink(userEventRoleDto);
      await landingPageTwo.clickOnEnterNowButton();
      await landingPageTwo.isStageLoaded();
    }
  );

  test("TC007: Organiser with logged in cookies visit event landing page @organiser @landing-page-2", async () => {
    let org_session = await org_browser_context.newPage();
    landingPageTwo = new LandingPageTwo(org_session);
    await landingPageTwo.load(attendee_landing_page);
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
    await org_session.close();
  });
});
