import {
  test,
  expect,
  Page,
  BrowserContext,
  APIRequestContext,
} from "@playwright/test";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  createNewEvent,
  getEventDetails,
  enableMagicLinkInEvent,
  enableSpeakerMagicLink,
  inviteSpeakerByApi,
  deleteSpeakerFromEvent,
  inviteAttendeeByAPI,
  enableAttendeeNSpeakerMagickLinks,
  updateEventLandingPageDetails,
  cancelEvent,
  updateEventEntryType,
} from "../../../../../util/apiUtil";
import {
  fetchAttendeeInviteMagicLink,
  getInviteLinkFromSpeakerInviteEmail,
} from "../../../../../util/emailUtil";
import { LandingPageOne } from "../../../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { UserInfoDTO } from "../../../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import EventEntryType from "../../../../../enums/eventEntryEnum";
import { UserRegFormData } from "../../../../../test-data/userRegForm";
import { UserEventRoleDto } from "../../../../../dto/userEventRoleDto";
import EventRole from "../../../../../enums/eventRoleEnum";
import { DataUtl } from "../../../../../util/dataUtil";
import { SpeakerData } from "../../../../../test-data/speakerData";
import { enableDisableEmailTrigger } from "../../../../../util/email-validation-api-util";
import EventSettingID from "../../../../../enums/EventSettingEnum";
import { fetchMagicLinkFromSendGridurl } from "../../../../../util/validation-util";

test.describe
  .parallel("Invite Based | Magic link Enabled | Speaker Landing Page | Completely new Users", () => {
  let org_browser_context: BrowserContext;
  let attendee_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let userDefaultRegFieldData: object;
  let attendee_email_add: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let page: Page;

  test.beforeEach(async ({ context }) => {
    // attendee_email_add = DataUtl.getRandomAttendeeEmail()
    userDefaultRegFieldData = new UserRegFormData(attendee_email_add)
      .userRegFormDataForDefaultFields;
    org_browser_context = await BrowserFactory.getBrowserContext({});
    organiserApiContext = await org_browser_context.request;
    // org_session = await org_browser_context.newPage()
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    // enable magic link to event and change its entry type
    // await enableMagicLinkInEvent(organiserApiContext,event_id,landing_page_id,EventEntryType.INVITE_ONLY)
    await updateEventEntryType(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    page = await context.newPage();
  });
  test.afterEach(async () => {
    await cancelEvent(organiserApiContext, event_id);
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
  });

  test("TC001: User who is not speaker try to enter via otp from speaker landing page @speaker @otp", async () => {
    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "automation",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );
    await landingPageOne.load(speaker_landing_page);
    await (
      await landingPageOne.getSpeakerLandingComponent()
    ).clickOnLoginButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await (await landingPageOne.getEventRestrictionSpeakersPage()).isVisible();
  });

  test("TC002: User who is not speaker try to enter via password from speaker landing page @speaker @password", async () => {
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      eventEntryType: EventEntryType.INVITE_ONLY,
      speakerAuthOptions: ["EMAIL"],
    });

    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "automation",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );
    await landingPageOne.load(speaker_landing_page);
    await (
      await landingPageOne.getSpeakerLandingComponent()
    ).clickOnLoginButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto, "speakerLandingPage");
    await (
      await landingPageOne.getRestrcitedEventScreen()
    ).isPrivateEventScreenVisible();
  });

  test("TC003: Invited speaker, logins to event via magic link recieved in email @speaker @invitemail", async () => {
    let landingPageOne = new LandingPageOne(page);
    let speakerUser = new SpeakerData(
      DataUtl.getRandomAttendeeEmail(),
      "Prateek"
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      true
    ); // true denote to send email
    let speakerInviteLink: string = await getInviteLinkFromSpeakerInviteEmail(
      speakerUser["email"],
      eventInfoDto.getInviteEmailSubj()
    );
    //checking if invite link contained magic link
    console.log(`speaker invite link is ${speakerInviteLink}`);
    await landingPageOne.load(speakerInviteLink); // should load the speaker landing page
    await landingPageOne.isStageLoaded();
  });

  test("TC004: Invited user as speaker, login via otp on speaker landing page @speaker @invitemail", async () => {
    let landingPageOne = new LandingPageOne(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    ); // true denote to send email
    await landingPageOne.load(speaker_landing_page);
    await landingPageOne.clickOnLoginButton();
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER
    );
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isStageLoaded();
  });

  /*
          The flow is not consistent,
          If a new user added as speaker, who do not have password,try login via password
          - otp flow should be triggered, 
          But if event is reg based, then its happening , if not then its not happening
      */
  test("TC005: Invited user as speaker, login via password on speaker landing page @speaker @invitemail", async () => {
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      eventEntryType: EventEntryType.INVITE_ONLY,
      speakerAuthOptions: ["EMAIL"],
    });

    let landingPageOne = new LandingPageOne(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      false
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    ); // true denote to send email
    await landingPageOne.load(speaker_landing_page);
    await landingPageOne.clickOnLoginButton();
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER,
      true
    );
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto, "speakerLandingPage");
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isStageLoaded();
  });

  test("TC006: Deleted speaker trying to login from speaker landing page via password @speaker @organiser", async ({
    context,
  }) => {
    // add a new user as speaker
    // true denote to send email
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    );

    // create new browser instance
    let landingPageOne = new LandingPageOne(page);

    // now added  speaker will login to the event
    await landingPageOne.load(speaker_landing_page);
    await landingPageOne.clickOnLoginButton();
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER
    );
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isStageLoaded();

    // now delete this speaker via API
    await deleteSpeakerFromEvent(
      organiserApiContext,
      eventInfoDto.eventId,
      speakerEmail
    ); // delete this speaker from the event
    userEventRoleDto.setUserRole(EventRole.NOTEXISTS);

    // now in order to create new fresh instance, will clear the cookies of existing context
    await context.clearCookies();
    // now the same speaker will reload the page
    await landingPageOne.load(speaker_landing_page);
    await landingPageOne.clickOnLoginButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await (await landingPageOne.getEventRestrictionSpeakersPage()).isVisible();
  });

  test("TC007: Invited speaker if changes magic link url to direct to attendee, user should not allow to enter @invitemail", async () => {
    let landingPageOne = new LandingPageOne(page);
    let speakerUser = new SpeakerData(
      DataUtl.getRandomAttendeeEmail(),
      "Prateek"
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      true
    ); // true denote to send email
    let speakerInviteLink: string = await getInviteLinkFromSpeakerInviteEmail(
      speakerUser["email"],
      eventInfoDto.getInviteEmailSubj()
    );

    speakerInviteLink = speakerInviteLink.includes("click")
      ? await fetchMagicLinkFromSendGridurl(speakerInviteLink)
      : speakerInviteLink;
    // checking if invite link contained magic link
    let isMagicLinkRecieved = false;
    if (speakerInviteLink.includes(speaker_landing_page)) {
      isMagicLinkRecieved = true;
    }
    console.log(`speaker invite link is ${speakerInviteLink}`);
    expect(
      isMagicLinkRecieved,
      "expecting magic link to be recieved in speaker invitation email"
    ).toBeTruthy();
    // extract magic link and change url to attendee
    let magicLinkToAttendePage = speakerInviteLink.replace("/p/m/", "/p/m/a/"); // make it attendee magic link
    await landingPageOne.load(magicLinkToAttendePage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).isAlreadyRegisteredButtonVisible();
  });

  test("TC008: Invited speaker if changes magic link to attendee page and even if attendee magic link is enabled,it should not work @invitemail", async () => {
    let landingPageOne = new LandingPageOne(page);
    let speakerUser = new SpeakerData(
      DataUtl.getRandomAttendeeEmail(),
      "Prateek"
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      true
    ); // true denote to send email
    let speakerInviteLink: string = await getInviteLinkFromSpeakerInviteEmail(
      speakerUser["email"],
      eventInfoDto.getInviteEmailSubj()
    );
    speakerInviteLink = speakerInviteLink.includes("click")
      ? await fetchMagicLinkFromSendGridurl(speakerInviteLink)
      : speakerInviteLink;
    // checking if invite link contained magic link
    let isMagicLinkRecieved = false;
    if (speakerInviteLink.includes(speaker_landing_page)) {
      isMagicLinkRecieved = true;
    }
    console.log(`speaker invite link is ${speakerInviteLink}`);
    expect(
      isMagicLinkRecieved,
      "expecting magic link to be recieved in speaker invitation email"
    ).toBeTruthy();
    // extract magic link and change url to attendee
    let magicLinkToAttendePage = speakerInviteLink.replace("/p/m/", "/p/m/a/"); // make it attendee magic link
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.REG_BASED
    );
    await landingPageOne.load(magicLinkToAttendePage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).isAlreadyRegisteredButtonVisible();
  });

  /*
      For now both the links are working fine attendee magic link as well as speaker magic link
      - need to check with dev if its expected or failed case
      */
  test("TC009: For attendee converted to speaker,only speaker magic link should work and not attendee magic link @invitemail", async () => {
    let landingPageOne = new LandingPageOne(page);
    let speakerEmail = DataUtl.getRandomAttendeeEmail();
    console.log(`speaker email for this test case is ${speakerEmail}`);
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    //enable attendee magic link
    await enableAttendeeNSpeakerMagickLinks(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    // await enableSpeakerMagicLink(organiserApiContext,event_id,landing_page_id,EventEntryType.INVITE_ONLY)
    //add attendee via api
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      speakerEmail
    ); // true denote to send email
    let attendeeMagicLink: string = await fetchAttendeeInviteMagicLink(
      speakerEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    console.log(`recieved attendee invite link is ${attendeeMagicLink}`);
    // convert same user to speaker now
    console.log(`inviting speaker by email throguh api`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      true
    );
    // fetch speaker magic link
    console.log(`fetching speaker magic link`);
    let speakerMagicLink: string = await getInviteLinkFromSpeakerInviteEmail(
      speakerEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    console.log(`speaker magic link is ${speakerMagicLink}`);
    // now if user loads attendee magic link, he should not be allowed to enter
    console.log(`Loading attendee magic link`);
    await landingPageOne.load(attendeeMagicLink);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).isAlreadyRegisteredButtonVisible();
    // now if user loads speaker magic linnk he should be allowed to enter
    console.log(`Loading speaker magic link`);
    await landingPageOne.load(speakerMagicLink);
    await landingPageOne.isStageLoaded();
  });

  /*
      
      This test case is failing because API to delete speakers is giving 500 error
  
      */
  test("TC010: Deleted speaker trying to login from his speaker magic link @speaker @organiser", async () => {
    // add a new user as speaker
    let landingPageOne = new LandingPageOne(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    // true denote to send email
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      true
    );
    // now this user will login to the event
    let speakerMagicLink: string = await getInviteLinkFromSpeakerInviteEmail(
      speakerEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    await deleteSpeakerFromEvent(
      organiserApiContext,
      eventInfoDto.eventId,
      speakerEmail
    );
    await landingPageOne.load(speakerMagicLink);
    await expect(landingPageOne.enterNowButton).toBeHidden();
  });
});
