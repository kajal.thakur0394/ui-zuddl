import {
  test,
  expect,
  Page,
  BrowserContext,
  APIRequestContext,
} from "@playwright/test";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  createNewEvent,
  getEventDetails,
  enableMagicLinkInEvent,
  inviteAttendeeByAPI,
  addCustomFieldsToEvent,
  updateEventLandingPageDetails,
  cancelEvent,
} from "../../../../../util/apiUtil";
import { fetchAttendeeInviteMagicLink } from "../../../../../util/emailUtil";
import { LandingPageOne } from "../../../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { UserInfoDTO } from "../../../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import EventEntryType from "../../../../../enums/eventEntryEnum";
import {
  EventCustomFieldFormObj,
  UserRegFormData,
} from "../../../../../test-data/userRegForm";
import { UserEventRoleDto } from "../../../../../dto/userEventRoleDto";
import EventRole from "../../../../../enums/eventRoleEnum";
import { DataUtl } from "../../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../../util/email-validation-api-util";
import EventSettingID from "../../../../../enums/EventSettingEnum";

test.describe
  .parallel("Reg Based | Magic link Enabled | Attendee Landing Page | Completely new Users @attendee @magiclink", () => {
  let org_browser_context: BrowserContext;
  let attendee_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let userDefaultRegFieldData: object;
  let attendee_email_add: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let page: Page;

  test.beforeEach(async ({ context }) => {
    // attendee_email_add = DataUtl.getRandomAttendeeEmail()
    userDefaultRegFieldData = new UserRegFormData(attendee_email_add)
      .userRegFormDataForDefaultFields;
    org_browser_context = await BrowserFactory.getBrowserContext({});
    organiserApiContext = await org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    // await updateEventEntryType(organiserApiContext,event_id,landing_page_id,EventEntryType.REG_BASED)
    eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.REG_BASED
    );
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );

    page = await context.newPage();
  });

  test.afterEach(async () => {
    await cancelEvent(organiserApiContext, event_id);
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
  });

  test("TC001: New Attendee registers and then login through magic link @attendee @emailcheck", async () => {
    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "automation",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );
    await landingPageOne.load(attendee_landing_page);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    let event_magic_link: string = await fetchAttendeeInviteMagicLink(
      userInfoDto.userEmail,
      eventInfoDto.registrationConfirmationEmailSubj
    );
    await expect(
      page.locator("div[class^='styles-module__logoHeaderBody']")
    ).toContainText("Check your email for the event invite");
    await landingPageOne.load(event_magic_link);
    await landingPageOne.isLobbyLoaded();
  });

  test("TC002: New attendee skips registration and try to login via OTP @attendee @emailcheck", async () => {
    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );
    await landingPageOne.load(attendee_landing_page);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForMagicLink(userEventRoleDto);

    await (await landingPageOne.getRegistrationFormComponent()).isVisible();
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).emailFieldIsPrefilledWith(userInfoDto.userEmail);
    // now fill the custom fields info which are not prefilled already
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    // then submit the details should trigger magic link again
    let event_magic_link: string = await fetchAttendeeInviteMagicLink(
      userInfoDto.userEmail,
      eventInfoDto.registrationConfirmationEmailSubj
    );
    await landingPageOne.load(event_magic_link);
    await landingPageOne.isLobbyLoaded();
  });

  test("TC003: New attendee skips registration and try to login via password @attendee @emailcheck", async () => {
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      attendeeAuthOptions: ["EMAIL"],
    });

    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );

    await landingPageOne.load(attendee_landing_page);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    // since the user is new and not registered, it will navigate him to registration form again
    await (await landingPageOne.getRegistrationFormComponent()).isVisible();
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).emailFieldIsPrefilledWith(userInfoDto.userEmail);
    // now fill the custom fields info which are not prefilled already
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    // then submit the details should trigger magic link again
    let event_magic_link: string = await fetchAttendeeInviteMagicLink(
      userInfoDto.userEmail,
      eventInfoDto.registrationConfirmationEmailSubj
    );
    await landingPageOne.load(event_magic_link);
    await landingPageOne.isLobbyLoaded();
  });

  test("TC004: New Attendee who is already registered tries to register again @attendee @re-registration", async () => {
    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await landingPageOne.load(attendee_landing_page);
    // first let user register to this event
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    // verify that the event invite has been sent screen is shown to user
    await expect(
      page.locator("div[class^='styles-module__logoHeaderBody']")
    ).toContainText("Check your email for the event invite");
    // now lets reload the attendee landing page and try to fill the form again
    await page.goto(attendee_landing_page);
    await page.reload();
    // try to fill the same form again
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    // verify screen is shown which tells that you are already registerd
    await expect(page.locator("text=You've already registered")).toBeVisible();
  });

  test("TC005: New Attendee after registering, trying to login via OTP @attendee @otp @emailcheck", async () => {
    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeMailosaurEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await landingPageOne.load(attendee_landing_page);
    // first let user register to this event
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    // verify that the event invite has been sent screen is shown to user
    await expect(
      page.locator("div[class^='styles-module__logoHeaderBody']")
    ).toContainText("Check your email for the event invite");
    // now since user is registered, we will update eventRoleDto
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    // now lets reload the attendee landing page and try to fill the form again
    await landingPageOne.load(attendee_landing_page);
    // try to fill the same form again
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForMagicLink(userEventRoleDto);
    await expect(
      page.locator("div[class^='styles-module__notificationContainer']")
    ).toContainText("success");
    let event_magic_link: string = await fetchAttendeeInviteMagicLink(
      userInfoDto.userEmail,
      eventInfoDto.registrationConfirmationEmailSubj
    );
    await page.goto(event_magic_link);
    await landingPageOne.isLobbyLoaded();
  });

  test("TC006: New Attendee after registering, trying to login via password @attendee @password", async () => {
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      attendeeAuthOptions: ["EMAIL"],
    });

    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await landingPageOne.load(attendee_landing_page);
    // first let user register to this event
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    // verify that the event invite has been sent screen is shown to user
    await expect(
      page.locator("div[class^='styles-module__logoHeaderBody']")
    ).toContainText("Check your email for the event invite");
    // now since user is registered, we will update eventRoleDto
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    // now lets reload the attendee landing page and try to fill the form again
    await landingPageOne.load(attendee_landing_page);
    // try to fill the same form again
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC007: New attenede gets invited, then login via magic link from email @attendee-invite @emailcheck", async () => {
    let landingPageOne = new LandingPageOne(page);
    let attendeeEmailToInvite = DataUtl.getRandomAttendeeEmail();
    userInfoDto = new UserInfoDTO(
      attendeeEmailToInvite,
      "",
      "prateek",
      "parashar",
      false
    );
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // after adding an attendee by email, account role will get created, event role will get created and reg record also
    userInfoDto.hasEntryInAccountTable = true;
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    // this user now fetch magic link which he should have recieved in his email
    let event_magic_link: string = await fetchAttendeeInviteMagicLink(
      attendeeEmailToInvite,
      eventInfoDto.getInviteEmailSubj()
    );
    await landingPageOne.load(event_magic_link);
    await landingPageOne.isLobbyLoaded();
  });

  test("TC008: New attenede gets invited, but try to register again to the event @emailcheck @attendee-invite", async () => {
    let landingPageOne = new LandingPageOne(page);
    let attendeeEmailToInvite = DataUtl.getRandomAttendeeEmail();
    userInfoDto = new UserInfoDTO(
      attendeeEmailToInvite,
      "",
      "prateek",
      "parashar",
      false
    );
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // after adding an attendee by email, account role will get created, event role will get created and reg record also
    userInfoDto.hasEntryInAccountTable = true;
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    // now user goes to attendee landing page and try to re register
    await landingPageOne.load(attendee_landing_page);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    // it should show that you are already registered
    await expect(page.locator("text=You've already registered")).toBeVisible();
    // email with magic link should have triggered
    let event_magic_link: string = await fetchAttendeeInviteMagicLink(
      attendeeEmailToInvite,
      eventInfoDto.getInviteEmailSubj()
    );
    await landingPageOne.load(event_magic_link);
    await landingPageOne.isLobbyLoaded();
  });

  test("TC009: New attenede gets invited, try to login via otp @emailcheck @attendee-invite @otp", async () => {
    let landingPageOne = new LandingPageOne(page);
    let attendeeEmailToInvite = DataUtl.getRandomAttendeeEmail();
    userInfoDto = new UserInfoDTO(
      attendeeEmailToInvite,
      "",
      "prateek",
      "parashar",
      false
    );
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // after adding an attendee by email, account role will get created, event role will get created and reg record also
    userInfoDto.hasEntryInAccountTable = true;
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    // now user goes to attendee landing page and try to re register
    await landingPageOne.load(attendee_landing_page);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForMagicLink(userEventRoleDto);
    await expect(
      page.locator("div[class^='styles-module__notificationContainer']")
    ).toContainText("success");
    let event_magic_link: string = await fetchAttendeeInviteMagicLink(
      userInfoDto.userEmail,
      eventInfoDto.registrationConfirmationEmailSubj
    );
    await page.goto(event_magic_link);
    await landingPageOne.isLobbyLoaded();
  });

  test("TC010: New attenede gets invited, try to login via password @attendee @attendee-invite @password", async () => {
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      attendeeAuthOptions: ["EMAIL"],
    });

    let landingPageOne = new LandingPageOne(page);
    let attendeeEmailToInvite = DataUtl.getRandomAttendeeEmail();
    userInfoDto = new UserInfoDTO(
      attendeeEmailToInvite,
      "",
      "prateek",
      "parashar",
      false
    );
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // after adding an attendee by email, account role will get created, event role will get created and reg record also
    userInfoDto.hasEntryInAccountTable = false;
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    // now user goes to attendee landing page and try to re register
    await landingPageOne.load(attendee_landing_page);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test(`TC011: New Attendee filling up the registration form with custom fields @emailcheck @customfield`, async () => {
    let landingPageOne = new LandingPageOne(page);
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await addCustomFieldsToEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      new EventCustomFieldFormObj(event_id, landing_page_id).customFieldObj
    );
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    await landingPageOne.load(attendee_landing_page);
    // update the default reg fields to custom feilds
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userCustomFieldFormData
    );
    let event_magic_link: string = await fetchAttendeeInviteMagicLink(
      userInfoDto.userEmail,
      eventInfoDto.registrationConfirmationEmailSubj
    );
    await page.goto(event_magic_link);
    await landingPageOne.isLobbyLoaded();
  });
});
