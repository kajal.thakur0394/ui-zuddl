import {
  test,
  expect,
  Page,
  BrowserContext,
  APIRequestContext,
} from "@playwright/test";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  createNewEvent,
  getEventDetails,
  enableMagicLinkInEvent,
  inviteAttendeeByAPI,
  updateEventLandingPageDetails,
  cancelEvent,
} from "../../../../../util/apiUtil";
import { fetchAttendeeInviteMagicLink } from "../../../../../util/emailUtil";
import { LandingPageOne } from "../../../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { UserInfoDTO } from "../../../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import EventEntryType from "../../../../../enums/eventEntryEnum";
import { UserRegFormData } from "../../../../../test-data/userRegForm";
import { UserEventRoleDto } from "../../../../../dto/userEventRoleDto";
import EventRole from "../../../../../enums/eventRoleEnum";
import { DataUtl } from "../../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../../util/email-validation-api-util";
import EventSettingID from "../../../../../enums/EventSettingEnum";

test.describe
  .parallel("Reg Based | Magic link Enabled | Attendee Landing Page | Existing Users", () => {
  let org_browser_context: BrowserContext;
  let attendee_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let userDefaultRegFieldData: object;
  let attendee_email_add: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let page: Page;

  test.beforeEach(async ({ context }) => {
    // attendee_email_add = DataUtl.getRandomAttendeeEmail()
    userDefaultRegFieldData = new UserRegFormData(attendee_email_add)
      .userRegFormDataForDefaultFields;
    org_browser_context = await BrowserFactory.getBrowserContext({});
    organiserApiContext = await org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.REG_BASED
    );
    eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );

    page = await context.newPage();
  });

  test.afterEach(async () => {
    await cancelEvent(organiserApiContext, event_id);
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
  });

  test("TC001: New user who has an account, registers to event and try login via Password in already registered flow @attendee", async () => {
    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getApplicationTestDataObj()["attendeeEmailWithAccountEntry"],
      DataUtl.getApplicationTestDataObj()["attendeePasswordWithAccountEntry"],
      "prateek",
      "parashar",
      true
    );
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      attendeeAuthOptions: ["EMAIL"],
    });

    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await landingPageOne.load(attendee_landing_page);
    // first let user register to this event
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    // verify that the event invite has been sent screen is shown to user
    await expect(
      page.locator("div[class^='styles-module__logoHeaderBody']")
    ).toContainText("Check your email for the event invite");
    // now we will update the eventrole dto
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    // now lets reload the attendee landing page and try to fill the form again
    await landingPageOne.load(attendee_landing_page);
    // try to fill the same form again
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC002: Invited an attendee who has an account, he logins through magic link @attendee", async () => {
    let landingPageOne = new LandingPageOne(page);
    let attendeeEmailToInvite =
      DataUtl.getApplicationTestDataObj()["attendeeEmailWithAccountEntry"];
    let invitedAttendeePassword =
      DataUtl.getApplicationTestDataObj()["attendeePasswordWithAccountEntry"];
    userInfoDto = new UserInfoDTO(
      attendeeEmailToInvite,
      invitedAttendeePassword,
      "prateek",
      "parashar",
      true
    );
    //enable invite emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.ATTENDEE,
      true
    );
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    let event_magic_link: string = await fetchAttendeeInviteMagicLink(
      attendeeEmailToInvite,
      eventInfoDto.getInviteEmailSubj()
    );
    await landingPageOne.load(event_magic_link);
    await landingPageOne.isLobbyLoaded();
  });

  test("TC003: Invited an attendee who has an account, he logins via OTP through already reg flow @attendee @otp", async () => {
    let landingPageOne = new LandingPageOne(page);
    let attendeeEmailToInvite =
      DataUtl.getApplicationTestDataObj()["attendeeEmailWithAccountEntry"];
    let invitedAttendeePassword =
      DataUtl.getApplicationTestDataObj()["attendeePasswordWithAccountEntry"];
    userInfoDto = new UserInfoDTO(
      attendeeEmailToInvite,
      invitedAttendeePassword,
      "prateek",
      "parashar",
      true
    );
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );

    await landingPageOne.load(attendee_landing_page);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForMagicLink(userEventRoleDto);
    await expect(
      page.locator("div[class^='styles-module__notificationContainer']")
    ).toContainText("success");
    let event_magic_link: string = await fetchAttendeeInviteMagicLink(
      userInfoDto.userEmail,
      eventInfoDto.registrationConfirmationEmailSubj
    );
    await landingPageOne.load(event_magic_link);
    await landingPageOne.isLobbyLoaded();
  });

  test("TC004: Invited an attendee who has an account, he logins via password through already reg flow @attendee @password", async () => {
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      attendeeAuthOptions: ["EMAIL"],
    });

    let landingPageOne = new LandingPageOne(page);
    let attendeeEmailToInvite =
      DataUtl.getApplicationTestDataObj()["attendeeEmailWithAccountEntry"];
    let invitedAttendeePassword =
      DataUtl.getApplicationTestDataObj()["attendeePasswordWithAccountEntry"];
    userInfoDto = new UserInfoDTO(
      attendeeEmailToInvite,
      invitedAttendeePassword,
      "prateek",
      "parashar",
      true
    );
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    await landingPageOne.load(attendee_landing_page);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC005: Organiser in already authenticated state,  opens the event landing page @organiser", async () => {
    let org_session = await org_browser_context.newPage();
    let landingPageOne = new LandingPageOne(org_session);
    await landingPageOne.load(attendee_landing_page);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isStageLoaded();
    await org_session.close();
  });

  // this needs to be fixed and organiser should directly be able to enter into the event with otp
  test.fixme(
    "TC006: Organiser of this event trying to login via otp @organiser",
    async ({ page }) => {
      let landingPageOne = new LandingPageOne(page);
      userInfoDto = new UserInfoDTO(
        DataUtl.getApplicationTestDataObj()["eventsOrganiserEmail"],
        "",
        "prateek",
        "organiser",
        true
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.ORGANISER,
        true
      );
      await landingPageOne.load(attendee_landing_page);
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).clickOnAlreadyRegisteredButton();
      await (
        await landingPageOne.getLoginOptionsComponent()
      ).submitEmailForMagicLink(userEventRoleDto);
      await expect(
        page.locator("div[class^='styles-module__logoHeaderBody']")
      ).toHaveText("Check your email for the event invite");
      let event_magic_link: string = await fetchAttendeeInviteMagicLink(
        userInfoDto.userEmail,
        eventInfoDto.registrationConfirmationEmailSubj
      );
      await landingPageOne.load(event_magic_link);
      await landingPageOne.isStageLoaded();
    }
  );

  test("TC007: Organiser of this event trying to login via password @organiser", async () => {
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      attendeeAuthOptions: ["EMAIL"],
    });

    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getApplicationTestDataObj()["eventsOrganiserEmail"],
      DataUtl.getApplicationTestDataObj()["organiserPassword"],
      "prateek",
      "organiser",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ORGANISER,
      true
    );
    await landingPageOne.load(attendee_landing_page);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isStageLoaded();
  });

  // we are not able to identify organiser and letting him register again
  test.fixme(
    "TC008: Organiser of this event trying to register himself as attendee @organiser",
    async ({ page }) => {
      let landingPageOne = new LandingPageOne(page);
      userInfoDto = new UserInfoDTO(
        DataUtl.getApplicationTestDataObj()["eventsOrganiserEmail"],
        DataUtl.getApplicationTestDataObj()["organiserPassword"],
        "prateek",
        "organiser",
        true
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.ORGANISER,
        true
      );
      await landingPageOne.load(attendee_landing_page);
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDto.userRegFormData.userRegFormDataForDefaultFields
      );
      await expect(
        page.locator("div[class^='styles-module__logoHeaderBody']")
      ).toHaveText("Check your email for the event invite");
    }
  );
});
