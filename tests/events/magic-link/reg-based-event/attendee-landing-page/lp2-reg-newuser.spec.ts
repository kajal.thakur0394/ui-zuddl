import {
  test,
  expect,
  BrowserContext,
  APIRequestContext,
} from "@playwright/test";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  cancelEvent,
  createNewEvent,
  getEventDetails,
  inviteAttendeeByAPI,
  updateEventLandingPageDetails,
  updateLandingPageType,
} from "../../../../../util/apiUtil";
import { fetchAttendeeInviteMagicLink } from "../../../../../util/emailUtil";
import { LandingPageTwo } from "../../../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
import { UserInfoDTO } from "../../../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import EventEntryType from "../../../../../enums/eventEntryEnum";
import { UserRegFormData } from "../../../../../test-data/userRegForm";
import { UserEventRoleDto } from "../../../../../dto/userEventRoleDto";
import EventRole from "../../../../../enums/eventRoleEnum";
import { DataUtl } from "../../../../../util/dataUtil";
import LandingPageType from "../../../../../enums/landingPageEnum";
import { enableDisableEmailTrigger } from "../../../../../util/email-validation-api-util";
import EventSettingID from "../../../../../enums/EventSettingEnum";

test.describe
  .parallel("Reg Based | Magic link Enabled | Attendee Landing Page | Completely new Users| @Landing-page 2", () => {
  let org_browser_context: BrowserContext;
  let attendee_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let userDefaultRegFieldData: object;
  let attendee_email_add: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;

  test.beforeEach(async () => {
    // attendee_email_add = DataUtl.getRandomAttendeeEmail()
    userDefaultRegFieldData = new UserRegFormData(attendee_email_add)
      .userRegFormDataForDefaultFields;
    org_browser_context = await BrowserFactory.getBrowserContext({});
    organiserApiContext = await org_browser_context.request;
    console.log(organiserApiContext);
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    // Enabling magic link, event entry type and Landing page type
    eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "true",
      EventEntryType.REG_BASED
    );
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
  });

  test.afterEach(async () => {
    await cancelEvent(organiserApiContext, event_id);
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
  });

  test("TC001: New attenede registers, then login via magic link from email @attendee @Landing-page 2 @invite", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "automation",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      false,
      EventRole.ATTENDEE,
      true
    );
    await landingPageTwo.load(attendee_landing_page);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    let event_magic_link: string = await fetchAttendeeInviteMagicLink(
      userInfoDto.userEmail,
      eventInfoDto.registrationConfirmationEmailSubj
    );
    await page.goto(event_magic_link);
    await landingPageTwo.isLobbyLoaded();
  });

  test("TC002: New Attendee after registering, trying to login via password @attendee @Landing-page 2 @password", async ({
    page,
  }) => {
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      attendeeAuthOptions: ["EMAIL"],
    });

    let landingPageTwo = new LandingPageTwo(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await landingPageTwo.load(attendee_landing_page);
    // first let user register to this event
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    await expect(
      page.locator("div[class^='styles-module__registrationMessageC']")
    ).toContainText("You've successfully registered! ");
    // now since user is registered, we will update eventRoleDto
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    // now lets reload the attendee landing page and try to fill the form again
    await landingPageTwo.load(attendee_landing_page);
    // try to fill the same form again
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isLobbyLoaded();
  });

  // This is failing with email is not pre-filled.
  test("TC003: New attendee skips registration and try to login via OTP @attendee @Landing-page 2", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );

    await landingPageTwo.load(attendee_landing_page);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await (await landingPageTwo.getRegistrationFormComponent()).isVisible();
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).emailFieldIsPrefilledWith(userInfoDto.userEmail);
    // now fill the custom fields info which are not prefilled already
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    // then submit the details should trigger magic link again
    let event_magic_link: string = await fetchAttendeeInviteMagicLink(
      userInfoDto.userEmail,
      eventInfoDto.registrationConfirmationEmailSubj
    );
    await landingPageTwo.load(event_magic_link);
    await landingPageTwo.isLobbyLoaded();
  });

  test("TC004: New Attendee who is already registered tries to register again @attendee @Landing-page 2 @re-registration", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await landingPageTwo.load(attendee_landing_page);
    // first let user register to this event
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    // verify that the event invite has been sent screen is shown to user
    await expect(
      page.locator("div[class^='styles-module__registrationMessageC']")
    ).toContainText("You've successfully registered! ");
    // now lets reload the attendee landing page and try to fill the form again
    await page.goto(attendee_landing_page);
    await page.reload();
    // try to fill the same form again
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    // verify screen is shown which tells that you are already registerd
    await expect(page.locator("text=You've already registered")).toBeVisible();
  });

  test("TC005: New Attendee after registering, trying to login via OTP @attendee @Landing-page 2 @otp", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await landingPageTwo.load(attendee_landing_page);
    // first let user register to this event
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    // verify that the event invite has been sent screen is shown to user
    await expect(
      page.locator("div[class^='styles-module__registrationMessageC']")
    ).toContainText("You've successfully registered! ");
    // now since user is registered, we will update eventRoleDto
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    // now lets reload the attendee landing page and try to fill the form again
    await landingPageTwo.load(attendee_landing_page);
    // try to fill the same form again
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForMagicLink(userEventRoleDto);
    await expect(
      page.locator("div[class^='styles-module__magicLinkSentText']")
    ).toContainText("Success");
    let event_magic_link: string = await fetchAttendeeInviteMagicLink(
      userInfoDto.userEmail,
      eventInfoDto.registrationConfirmationEmailSubj
    );
    await page.goto(event_magic_link);
    await landingPageTwo.isLobbyLoaded();
  });

  test("TC006: New attenede gets invited, then login via magic link from email @attendee @Landing-page 2 @invite", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    let attendeeEmailToInvite = DataUtl.getRandomAttendeeEmail();
    userInfoDto = new UserInfoDTO(
      attendeeEmailToInvite,
      "",
      "prateek",
      "parashar",
      false
    );
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // after adding an attendee by email, account role will get created, event role will get created and reg record also
    userInfoDto.hasEntryInAccountTable = true;
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    // this user now fetch magic link which he should have recieved in his email
    let event_magic_link: string = await fetchAttendeeInviteMagicLink(
      attendeeEmailToInvite,
      eventInfoDto.getInviteEmailSubj()
    );
    await landingPageTwo.load(event_magic_link);
    await landingPageTwo.isLobbyLoaded();
  });

  test("TC007:New attenede gets invited, but try to register again to the event @Landing-page 2 @attendee @invite", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    let attendeeEmailToInvite = DataUtl.getRandomAttendeeEmail();
    userInfoDto = new UserInfoDTO(
      attendeeEmailToInvite,
      "",
      "prateek",
      "parashar",
      false
    );
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // after adding an attendee by email, account role will get created, event role will get created and reg record also
    userInfoDto.hasEntryInAccountTable = true;
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    // now user goes to attendee landing page and try to re register
    await landingPageTwo.load(attendee_landing_page);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    // it should show that you are already registered
    await expect(page.locator("text=You've already registered")).toBeVisible();
    // email with magic link should have triggered
    let event_magic_link: string = await fetchAttendeeInviteMagicLink(
      attendeeEmailToInvite,
      eventInfoDto.getInviteEmailSubj()
    );
    await landingPageTwo.load(event_magic_link);
    await landingPageTwo.isLobbyLoaded();
  });
});
