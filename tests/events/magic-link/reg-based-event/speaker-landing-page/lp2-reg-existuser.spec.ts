import {
  test,
  Page,
  BrowserContext,
  APIRequestContext,
} from "@playwright/test";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  createNewEvent,
  getEventDetails,
  inviteSpeakerByApi,
  updateLandingPageType,
  inviteAttendeeByAPI,
  updateSpeakerBio,
  updateEventLandingPageDetails,
  cancelEvent,
} from "../../../../../util/apiUtil";
import { LandingPageTwo } from "../../../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
import { UserInfoDTO } from "../../../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import EventEntryType from "../../../../../enums/eventEntryEnum";
import { UserRegFormData } from "../../../../../test-data/userRegForm";
import { UserEventRoleDto } from "../../../../../dto/userEventRoleDto";
import EventRole from "../../../../../enums/eventRoleEnum";
import { DataUtl } from "../../../../../util/dataUtil";
import { SpeakerData } from "../../../../../test-data/speakerData";
import LandingPageType from "../../../../../enums/landingPageEnum";

test.describe
  .parallel("Invite Based | Magic link Enabled | Speaker Landing Page | Existing Users|@Landing-page 2 ", () => {
  let org_session: Page;
  let org_browser_context: BrowserContext;
  let attendee_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let existUserEmail: string;
  let existUserPwd: string;
  let speakerData: SpeakerData;

  test.beforeEach(async () => {
    org_browser_context = await BrowserFactory.getBrowserContext({});
    organiserApiContext = await org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    // Enabling magic link, event entry type and Landing page type
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "true",
      EventEntryType.REG_BASED
    );
    eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    // existing user data
    existUserEmail =
      DataUtl.getApplicationTestDataObj()["attendeeEmailWithAccountEntry"];
    existUserPwd =
      DataUtl.getApplicationTestDataObj()["attendeePasswordWithAccountEntry"];
    userInfoDto = new UserInfoDTO(
      existUserEmail,
      existUserPwd,
      "Prateek",
      "Automation",
      true
    );
    userInfoDto.hasSetPassword = true;
    speakerData = new SpeakerData(existUserEmail, "Prateek");
  });

  test.afterEach(async () => {
    await cancelEvent(organiserApiContext, event_id);
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
  });

  test("TC001: Invited existing user as speaker, login via otp on speaker landing page @speaker @Landing-page 2 @Landing-page 2 @invitemail", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    console.log(`Speaker data to add is ${existUserEmail}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerData.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    ); // true denote to send email
    await landingPageTwo.load(speaker_landing_page);
    // await landingPageTwo.clickOnLoginButton()
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER
    );
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
  });

  test("TC002: Invited existing user as speaker, login via password on speaker landing page @speaker @Landing-page 2 @Landing-page 2 @invitemail", async ({
    page,
  }) => {
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      speakerAuthOptions: ["EMAIL"],
    });

    let landingPageTwo = new LandingPageTwo(page);
    console.log(`Speaker data to add is ${existUserEmail}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerData.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    ); // true denote to send email
    await landingPageTwo.load(speaker_landing_page);
    await landingPageTwo.clickOnSigninButton();
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER
    );
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto, "speakerLandingPage");
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
  });

  test("TC003: verify modification of speaker data does not impact speaker login @speaker @Landing-page 2 @Landing-page 2 @invitemail", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    console.log(`Speaker data to add is ${existUserEmail}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerData.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    ); // true denote to send email
    // update speaker bio
    speakerData.bio = "New bio of speaker by automation";
    await updateSpeakerBio(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail,
      "New bio by automation"
    );
    await landingPageTwo.load(speaker_landing_page);
    await landingPageTwo.clickOnLoginButton();
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER,
      true
    );
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
  });

  test("TC004: Organiser, login via otp on speaker landing page @organiser @invitemail @Landing-page 2", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    console.log(`Speaker data to add is ${existUserEmail}`);
    await landingPageTwo.load(speaker_landing_page);
    // await landingPageTwo.clickOnLoginButton()
    let orgEmail: string =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserEmail"];
    let orgPassword: string =
      DataUtl.getApplicationTestDataObj()["organiserPassword"];
    userInfoDto = new UserInfoDTO(
      orgEmail,
      orgPassword,
      "prateek",
      "automation",
      true
    );
    userInfoDto.hasSetPassword = true;
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ORGANISER
    );
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
  });

  test("TC005: Organiser, login via password on speaker landing page @organiser @invitemail @Landing-page 2", async ({
    page,
  }) => {
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      speakerAuthOptions: ["EMAIL"],
    });

    let landingPageTwo = new LandingPageTwo(page);
    console.log(`Speaker data to add is ${existUserEmail}`);
    await landingPageTwo.load(speaker_landing_page);
    // await landingPageTwo.clickOnLoginButton()
    let orgEmail: string =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserEmail"];
    let orgPassword: string =
      DataUtl.getApplicationTestDataObj()["organiserPassword"];
    userInfoDto = new UserInfoDTO(
      orgEmail,
      orgPassword,
      "prateek",
      "automation",
      true
    );
    userInfoDto.hasSetPassword = true;
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ORGANISER
    );
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto, "speakerLandingPage");
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
  });

  test("TC006: Existing attendee gets added to speaker try to login via speaker landing page @speaker @Landing-page 2 @Landing-page 2", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    console.log(`Speaker data to add is ${existUserEmail}`);
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerData.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    ); // true denote to send email
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER
    );
    await landingPageTwo.load(speaker_landing_page);
    await landingPageTwo.clickOnLoginButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
  });
});
