import {
  test,
  expect,
  Page,
  BrowserContext,
  APIRequestContext,
} from "@playwright/test";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  createNewEvent,
  getEventDetails,
  inviteSpeakerByApi,
  inviteAttendeeByAPI,
  deleteSpeakerFromEvent,
  enableMagicLinkInEvent,
  enableSpeakerMagicLink,
  enableAttendeeNSpeakerMagickLinks,
  updateEventLandingPageDetails,
  cancelEvent,
  updateLandingPageType,
  updateEventEntryType,
} from "../../../../../util/apiUtil";
import {
  getInviteLinkFromSpeakerInviteEmail,
  fetchAttendeeInviteMagicLink,
} from "../../../../../util/emailUtil";
import { LandingPageOne } from "../../../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { UserInfoDTO } from "../../../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import { UserRegFormData } from "../../../../../test-data/userRegForm";
import { UserEventRoleDto } from "../../../../../dto/userEventRoleDto";
import EventRole from "../../../../../enums/eventRoleEnum";
import { DataUtl } from "../../../../../util/dataUtil";
import { SpeakerData } from "../../../../../test-data/speakerData";
import EventEntryType from "../../../../../enums/eventEntryEnum";
import { enableDisableEmailTrigger } from "../../../../../util/email-validation-api-util";
import EventSettingID from "../../../../../enums/EventSettingEnum";
import { fetchMagicLinkFromSendGridurl } from "../../../../../util/validation-util";

test.describe.parallel("Reg Based | Magic link Enabled | @speaker-landing-page | Completely new Users", () => {
  test.describe.configure({ retries: 2 });

  let org_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let page: Page;

  test.beforeEach(async ({ context }) => {
    org_browser_context = await BrowserFactory.getBrowserContext({});
    organiserApiContext = await org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link enabled
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await updateEventEntryType(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.REG_BASED
    );
    eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;

    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    page = await context.newPage();
  });
  test.afterEach(async () => {
    await cancelEvent(organiserApiContext, event_id);
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
  });

  test("TC001: User who is not speaker try to enter via otp from speaker landing page @speaker @otp", async () => {
    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "automation",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );
    await landingPageOne.load(speaker_landing_page);
    await (
      await landingPageOne.getSpeakerLandingComponent()
    ).clickOnLoginButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await (await landingPageOne.getEventRestrictionSpeakersPage()).isVisible();
    //verify user gets an option to go to attendee page from restricted entry page
    await (
      await landingPageOne.getEventRestrictionSpeakersPage()
    ).chooseToGoToAttendeePage();
    await expect(
      page,
      `expecting ${attendee_landing_page} url to be loaded`
    ).toHaveURL(attendee_landing_page, { timeout: 10000 });
  });

  test("TC002: User who is not speaker try to enter via password from speaker landing page @speaker @password", async () => {
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      speakerAuthOptions: ["EMAIL"],
    });

    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "automation",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );
    await landingPageOne.load(speaker_landing_page);
    await (
      await landingPageOne.getSpeakerLandingComponent()
    ).clickOnLoginButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto, "speakerLandingPage");
    await (await landingPageOne.getEventRestrictionSpeakersPage()).isVisible();
    //verify user gets an option to go to attendee page from restricted entry page
    await (
      await landingPageOne.getEventRestrictionSpeakersPage()
    ).chooseToGoToAttendeePage();
    await expect(
      page,
      `expecting ${attendee_landing_page} url to be loaded`
    ).toHaveURL(attendee_landing_page, { timeout: 10000 });
  });

  test("TC003: Invited speaker, logins to event via magic link from email  @speaker @invitemail", async () => {
    eventInfoDto.isMagicLinkEnabled = true;
    let landingPageOne = new LandingPageOne(page);
    let speakerUser = new SpeakerData(
      DataUtl.getRandomAttendeeEmail(),
      "Prateek"
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      true
    ); // true denote to send email
    let speakerInviteLink = await getInviteLinkFromSpeakerInviteEmail(
      speakerUser["email"],
      eventInfoDto.getInviteEmailSubj()
    );
    speakerInviteLink = speakerInviteLink.includes("click")
      ? await fetchMagicLinkFromSendGridurl(speakerInviteLink)
      : speakerInviteLink;
    let isMagicLinkRecieved = false;
    if (speakerInviteLink.includes(eventInfoDto.speakerLandingPage)) {
      isMagicLinkRecieved = true;
    }
    console.log(`speaker invite link is ${speakerInviteLink}`);
    expect(
      isMagicLinkRecieved,
      "expecting magic link to be recieved in speaker invitation email"
    ).toBeTruthy();
    await landingPageOne.load(speakerInviteLink);
    await landingPageOne.isStageLoaded();
  });

  test("TC004: Invited user as speaker, login via otp on speaker landing page @speaker @invitemail", async () => {
    let landingPageOne = new LandingPageOne(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    ); // true denote to send email
    await landingPageOne.load(speaker_landing_page);
    await landingPageOne.clickOnLoginButton();
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER
    );
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isStageLoaded();
  });

  /*
     this is not working as it should, as speaker who do not have passowrd should 
     trigger otp flow if he chooses to
     login via password but for now, I have automated it by setting login password by forget
     password flow
    */
  test("TC005: Invited user as speaker, login via password on speaker landing page @speaker @invitemail", async () => {
    await updateEventLandingPageDetails(organiserApiContext, event_id, {
      speakerAuthOptions: ["EMAIL"],
    });
    let landingPageOne = new LandingPageOne(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      false
    );
    userInfoDto.hasSetPassword = false;
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    ); // true denote to send email
    userInfoDto.hasEntryInAccountTable = true;
    await landingPageOne.load(speaker_landing_page);
    await landingPageOne.clickOnLoginButton();
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER,
      true
    );
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto, "speakerLandingPage", true);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isStageLoaded();
  });

  test("TC006: Deleted speaker trying to login from speaker landing page via password @speaker @organiser", async ({
    context,
  }) => {
    // add a new user as speaker
    // let page = await context.newPage();
    let landingPageOne = new LandingPageOne(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    // true denote to send email
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    );
    // now this user will login to the event
    await landingPageOne.load(speaker_landing_page);
    await landingPageOne.clickOnLoginButton();
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER
    );
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isStageLoaded();
    await context.clearCookies();
    // now delete this speaker via API
    await deleteSpeakerFromEvent(
      organiserApiContext,
      eventInfoDto.eventId,
      speakerEmail
    ); // delete this speaker from the event
    // now this user logs in again
    userEventRoleDto.setUserRole(EventRole.NOTEXISTS);
    await landingPageOne.load(speaker_landing_page);
    await landingPageOne.clickOnLoginButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await (await landingPageOne.getEventRestrictionSpeakersPage()).isVisible();
  });

  test("TC007: Invited speaker if changes magic link url to direct to attendee, user should not allow to enter @invitemail", async () => {
    eventInfoDto.isMagicLinkEnabled = true;
    let landingPageOne = new LandingPageOne(page);
    let speakerUser = new SpeakerData(
      DataUtl.getRandomAttendeeEmail(),
      "Prateek"
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      true
    ); // true denote to send email
    let speakerInviteLink: string = await getInviteLinkFromSpeakerInviteEmail(
      speakerUser["email"],
      eventInfoDto.getInviteEmailSubj()
    );
    speakerInviteLink = speakerInviteLink.includes("click")
      ? await fetchMagicLinkFromSendGridurl(speakerInviteLink)
      : speakerInviteLink;
    // checking if invite link contained magic link
    let isMagicLinkRecieved = false;
    if (speakerInviteLink.includes(eventInfoDto.speakerLandingPage)) {
      isMagicLinkRecieved = true;
    }
    console.log(`speaker invite link is ${speakerInviteLink}`);
    expect(
      isMagicLinkRecieved,
      "expecting magic link to be recieved in speaker invitation email"
    ).toBeTruthy();
    // extract magic link and change url to attendee
    let magicLinkToAttendePage = speakerInviteLink.replace("/p/m/", "/p/m/a/"); // make it attendee magic link
    await landingPageOne.load(magicLinkToAttendePage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).isAlreadyRegisteredButtonVisible();
  });

  test("TC008: Invited speaker if changes magic link to attendee page and even if attendee magic link is enabled,it should not work @invitemail", async () => {
    eventInfoDto.isMagicLinkEnabled = true;
    let landingPageOne = new LandingPageOne(page);
    let speakerUser = new SpeakerData(
      DataUtl.getRandomAttendeeEmail(),
      "Prateek"
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      true
    ); // true denote to send email
    let speakerInviteLink: string = await getInviteLinkFromSpeakerInviteEmail(
      speakerUser["email"],
      eventInfoDto.getInviteEmailSubj()
    );
    speakerInviteLink = speakerInviteLink.includes("click")
      ? await fetchMagicLinkFromSendGridurl(speakerInviteLink)
      : speakerInviteLink;
    // checking if invite link contained magic link
    let isMagicLinkRecieved = false;
    if (speakerInviteLink.includes(eventInfoDto.speakerLandingPage)) {
      isMagicLinkRecieved = true;
    }
    console.log(`speaker invite link is ${speakerInviteLink}`);
    expect(
      isMagicLinkRecieved,
      "expecting magic link to be recieved in speaker invitation email"
    ).toBeTruthy();
    // extract magic link and change url to attendee
    let magicLinkToAttendePage = speakerInviteLink.replace("/p/m/", "/p/m/a/"); // make it attendee magic link
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    await landingPageOne.load(magicLinkToAttendePage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).isAlreadyRegisteredButtonVisible();
  });

  /*
    For now both the links are working fine attendee magic link as well as speaker magic link
    - need to check with dev if its expected or failed case
    */
  test("TC009: For attendee converted to speaker,only speaker magic link should work and not attendee magic link @invitemail", async () => {
    eventInfoDto.isMagicLinkEnabled = true;
    let landingPageOne = new LandingPageOne(page);
    let speakerEmail = DataUtl.getRandomAttendeeEmail();
    console.log(`speaker email for this test case is ${speakerEmail}`);
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    //enable attendee magic link
    await enableAttendeeNSpeakerMagickLinks(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    //add attendee via api
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      speakerEmail
    ); // true denote to send email
    let attendeeMagicLink: string = await fetchAttendeeInviteMagicLink(
      speakerEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    console.log(`recieved attendee invite link is ${attendeeMagicLink}`);
    // convert same user to speaker now
    console.log(`inviting speaker by email throguh api`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      true
    );
    // fetch speaker magic link
    console.log(`fetching speaker magic link`);
    let speakerMagicLink: string = await getInviteLinkFromSpeakerInviteEmail(
      speakerEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    console.log(`speaker magic link is ${speakerMagicLink}`);
    // now if user loads attendee magic link, he should not be allowed to enter
    console.log(`Loading attendee magic link`);
    await landingPageOne.load(attendeeMagicLink);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).isAlreadyRegisteredButtonVisible();
    // now if user loads speaker magic linnk he should be allowed to enter
    console.log(`Loading speaker magic link`);
    await landingPageOne.load(speakerMagicLink);
    await landingPageOne.isStageLoaded();
  });

  /*
    
    This test case is failing because API to delete speakers is giving 500 error

    */
  test("TC010: Deleted speaker trying to login from his speaker magic link @speaker @organiser", async () => {
    // add a new user as speaker
    eventInfoDto.isMagicLinkEnabled = true;
    let landingPageOne = new LandingPageOne(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    // true denote to send email
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      true
    );
    // now this user will login to the event
    let speakerMagicLink: string = await getInviteLinkFromSpeakerInviteEmail(
      speakerEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    await deleteSpeakerFromEvent(
      organiserApiContext,
      eventInfoDto.eventId,
      speakerEmail
    );
    await landingPageOne.load(speakerMagicLink);
    await expect(landingPageOne.enterNowButton).toBeHidden();
  });
});
