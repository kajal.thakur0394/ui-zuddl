import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../../../controller/EventController";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  createNewEvent,
  updateEventLandingPageDetails,
  updateEventType,
} from "../../../../../util/apiUtil";
import EventType from "../../../../../enums/eventTypeEnum";
import { FlowBuilderController } from "../../../../../controller/FlowBuilderController";
import { FlowStatus, FlowType } from "../../../../../enums/FlowTypeEnum";
import { publishDataPayload } from "../../../../../interfaces/FlowBuilerControllerInterface";
import { FlowBuilderPage } from "../../../../../page-objects/flow-builder/flow-ticketing";
import { DataUtl } from "../../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../../util/email-validation-api-util";
import EventSettingID from "../../../../../enums/EventSettingEnum";
import EventRole from "../../../../../enums/eventRoleEnum";
import { UserInfoDTO } from "../../../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import CommunicationHelper from "../../../../../tests/events/communication/communication-tools";
import { CSVController } from "../../../../../controller/CSVController";
import { readCsvStringAsJson } from "../../../../../util/commonUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@flow-builder @registraion-without-ticket`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId: any;
  let attendeeBrowserContext: BrowserContext;
  let eventController: EventController;
  let flowBuilderPage: FlowBuilderPage;
  let flowBuilderController: FlowBuilderController;
  let publishdataPayLoad: publishDataPayload;
  let attendeePage: Page;
  let attendeeEmail: string;
  let registeredEmail: string;
  const userFirstName = "Jxtin";
  const userLastName = "zuddl";
  let eventInfoDto: EventInfoDTO;

  test.beforeAll(async () => {
    await test.step(`setup browser contexts`, async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: true,
      });
      orgApiContext = orgBrowserContext.request;
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage = await attendeeBrowserContext.newPage();
    });

    await test.step(`create an event with FLEX as resgistration type`, async () => {
      eventId = await createNewEvent({
        api_request_context: orgApiContext,
        event_title: "flow-without-ticket automation",
        isFlex: true,
      });
      console.log(`event id ${eventId}`);
    });

    await test.step(`update an event with HYBRID as event type`, async () => {
      await updateEventType(orgApiContext, eventId, EventType.HYBRID);
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        isMagicLinkEnabled: true,
      });
    });

    await test.step(`Initialise event controller`, async () => {
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step(`Get event details`, async () => {
      let eventInfo = await (await eventController.getEventInfo()).json();
      console.log("Event Info: ", eventInfo);
      eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
      eventInfoDto.startDateTime = eventInfo.startDateTime;
    });

    await enableDisableEmailTrigger(
      orgApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );

    await test.step(`Initialise flowbuilder controller`, async () => {
      flowBuilderController = new FlowBuilderController(orgApiContext, eventId);
    });

    await test.step(`make publish flow test data`, async () => {
      let flowDraftId = await flowBuilderController.getFlowId(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
      publishdataPayLoad = {
        flowId: "",
        flowDraftId: flowDraftId,
        clientFlowDraftVersion: 2,
      };
    });

    await test.step(`Initialise flowbuilder controller`, async () => {
      flowBuilderController = new FlowBuilderController(orgApiContext, eventId);
    });

    await test.step(`update default flow steps with existing expected steps`, async () => {
      await flowBuilderController.updateFlowData(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
    });

    await test.step(`now, publish the flow`, async () => {
      await flowBuilderController.publishFlowData(publishdataPayLoad);
      await attendeePage.waitForTimeout(5000);
    });

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
  });

  test.beforeEach(async () => {
    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
  });

  test.afterEach(async () => {
    await attendeePage.reload();
    // clear the cookies
    await attendeePage.context().clearCookies();
    await attendeePage.evaluate(() => {
      localStorage.clear();
    });
  });

  test.afterAll(async () => {
    await orgBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test(`TC001: @without-ticket-and-branch Verify that the new user successfully registered to the event`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-743/verify-that-the-new-user-successfully-registered-to-the-event",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-741/registration-flow-without-ticket",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-814/",
    });

    const customDataValues = {
      holiday1: "Yes",
      richText: "",
      weekdays: [
        {
          label: "Saturday",
          value: "Saturday",
        },
        {
          label: "Sunday",
          value: "Sunday",
        },
      ],
      disclaimers: [
        {
          text: "<p>Mandatory disclaimer</p>\n",
          consentGiven: true,
        },
      ],
      yourLinkedIn: "https://www.linkedin.com/in/testuser/",
      titleOptional: "Engineer",
    };

    await test.step(`Fill the registration form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form - Mandatory",
        userEmail: attendeeEmail,
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Non Mandatory Form - 1",
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Selection Form",
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Logic Rich Text",
      });
    });

    await test.step(`Verify that the user is successfully registered`, async () => {
      await flowBuilderPage.verifySuccessFullRegistration({
        userFirstName: userFirstName,
        userLastName: userLastName,
        email: attendeeEmail,
      });
    });

    await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
      await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
        attendeeEmail,
        eventId
      );
    });

    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      userFirstName,
      userLastName
    );

    let communicationHelper = new CommunicationHelper(
      orgApiContext,
      eventId,
      userInfoDto,
      eventInfoDto,
      true
    );

    await test.step("Fetching Email body from email recieved after registering", async () => {
      await communicationHelper.verifyConfirmationEmailContents(true, "(IST)");
    });

    await test.step(`Now fetching the user entry from event_registration table and validate the custom fields`, async () => {
      const userRegDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          eventId,
          attendeeEmail
        );
      console.log(userRegDataFromDb);
      const customFields = userRegDataFromDb["custom_field"];
     // const customFieldsJson = JSON.parse(customFields);
      for (const [key, value] of Object.entries(customDataValues)) {
        await test.step(`validating the ${key} param from db entry`, async () => {
          expect(
            customFields[key],
            `expecting ${key} to be ${value}`
          ).toStrictEqual(value);
        });
      }
    });

    registeredEmail = attendeeEmail;
  });

  test(`TC002: @without-ticket-and-branch Verify attendee data in download csv or database`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-746/verify-attendee-data-in-download-csv-or-database",
    });

    await test.step(`Verify data in downloaded CSV`, async () => {
      let csvController = new CSVController(orgApiContext, eventId);
      let csvResponse = await csvController.getDownloadCsv();
      let jsonData = await readCsvStringAsJson(csvResponse);
      await test.step(`verify the download csv contains the attendee record`, async () => {
        console.log(jsonData);
        expect(jsonData, "expecting csv to contain 1 record").toHaveLength(1);
        expect(
          jsonData[0].attendee_email.toLowerCase(),
          `expecting ${registeredEmail} to be present in the csv`
        ).toBe(registeredEmail.toLowerCase());
      });
    });
  });

  test(`TC003: @without-ticket-and-branch Verify already register user cannot register again to the event`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-744/verify-already-register-user-cannot-register-again-to-the-event",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-741/registration-flow-without-ticket",
    });

    await attendeePage.waitForTimeout(5000);

    await test.step(`Try registering with same details again and expect error.`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form - Mandatory",
        expectErrorPostContinue: true,
        userEmail: registeredEmail,
      });
    });
  });

  test(`TC004: @without-ticket-and-branch Verify form does not continue without mandatory field`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-771/verify-form-does-not-continue-without-mandatory-field",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-741/registration-flow-without-ticket",
    });

    await test.step(`Fill the registration form without disclaimer`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        userEmail: attendeeEmail,
        expectedFormHeading: "Form - Mandatory",
        skipMandatoryDisclaimer: true,
      });
    });

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });

    await test.step(`Fill the registration form without firstname`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: "",
        userLastName: userLastName,
        expectedFormHeading: "Form - Mandatory",
        expectErrorPostContinue: true,
        userEmail: attendeeEmail,
      });
    });

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });

    await test.step(`Fill the registration form without lastname`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: "",
        expectedFormHeading: "Form - Mandatory",
        expectErrorPostContinue: true,
        userEmail: attendeeEmail,
      });
    });

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });

    await test.step(`Fill the registration form with unformatted email`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        userEmail: "testattendee",
        expectedFormHeading: "Form - Mandatory",
        expectErrorPreContinue: true,
      });
    });
  });

  test(`TC005: @without-ticket-and-branch Verify Phone number does not accept alphabets`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-741/registration-flow-without-ticket",
    });

    await test.step(`Fill the registration form Mandatory page`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form - Mandatory",
        userEmail: attendeeEmail,
      });
    });

    await test.step(`Fill the registration form Non Mandatory page, but type non digit characters into the phone number field`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Non Mandatory Form - 1",
        phoneNumber: "000abcghij1111",
      });
    });
  });

  test(`TC006: @without-ticket-and-branch Verify utm parameter added in url is getting stored in an event`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-747/verify-custom-field-data-utm-parameter-standard-field-data-is-getting",
    });
    const utm_medium = "playwright";
    const utm_source = "automation";
    const utmparameters = `utm_source=${utm_source}&utm_medium=${utm_medium}`;

    await test.step(`set content to publish site having utm parameters`, async () => {
      await attendeePage.goto("https://example.com?" + utmparameters);
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
      await attendeePage.waitForTimeout(5000);
    });

    await test.step(`Fill the registration form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form - Mandatory",
        userEmail: attendeeEmail,
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Non Mandatory Form - 1",
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Selection Form",
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Logic Rich Text",
      });
    });

    await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
      await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
        attendeeEmail,
        eventId
      );
    });

    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      userFirstName,
      userLastName
    );

    let communicationHelper = new CommunicationHelper(
      orgApiContext,
      eventId,
      userInfoDto,
      eventInfoDto,
      true
    );

    await test.step("Fetching Email body from email recieved after registering", async () => {
      await communicationHelper.verifyConfirmationEmailContents(true, "(IST)");
    });

    await test.step(`Now fetching the user entry from event_registration table and validate the utm params`, async () => {
      //verify in DB event_registration table, the utm param exists as expected
      const userRegDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          eventId,
          attendeeEmail
        );
      //fetch the utm param from db for registration record
      const utmParamsFromDb = userRegDataFromDb["utm"];
      //validate the utm params
     // const utmParamJson = JSON.parse(utmParamsFromDb);
      //validate the key and value
      await test.step(`validating the utm_medium param from db entry`, async () => {
        expect(
          utmParamsFromDb["utm_medium"],
          `expecting utm_medium to be ${utm_medium}`
        ).toBe(utm_medium);
      });

      await test.step(`validating the utm_source param from db entry`, async () => {
        expect(
          utmParamsFromDb["utm_source"],
          `expecting utm_source to be ${utm_source}`
        ).toBe(utm_source);
      });
    });
  });

  test(`TC007: @without-ticket-and-branch Update the form and verify the change is reflected`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-741/registration-flow-without-ticket",
    });

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-749/add-multiple-forms-between-first-form-and-thank-you-form-verify-new",
    });

    await test.step(`Insert a new form at the end and verify the change is reflected`, async () => {
      await flowBuilderController.insertFormToExistingFlow(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT,
        "Logic Rich Text"
      );
    });

    const customDataValuesWithNewForm = {
      holiday1: "",
      weekdays: [
        {
          label: "Saturday",
          value: "Saturday",
        },
        {
          label: "Sunday",
          value: "Sunday",
        },
      ],
      newlyAdded: "New field data",
      disclaimers: [
        {
          text: "<p>Mandatory disclaimer</p>\n",
          consentGiven: true,
        },
      ],
      yourLinkedIn: "",
      titleOptional: "",
    };

    const customDataValuesWithoutNewForm = {
      holiday1: "",
      weekdays: [
        {
          label: "Saturday",
          value: "Saturday",
        },
        {
          label: "Sunday",
          value: "Sunday",
        },
      ],
      disclaimers: [
        {
          text: "<p>Mandatory disclaimer</p>\n",
          consentGiven: true,
        },
      ],
      yourLinkedIn: "",
      titleOptional: "",
    };

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
      await attendeePage.waitForTimeout(5000);
    });

    attendeeEmail = DataUtl.getRandomAttendeeEmail();

    await test.step(`Fill the registration form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form - Mandatory",
        userEmail: attendeeEmail,
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Non Mandatory Form - 1",
        fillNonMandatoryForm: false,
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Selection Form",
        fillHoliday: false,
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Logic Rich Text",
        logicRichText: false,
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "New Form",
      });
    });

    await test.step(`Verify that the user is successfully registered`, async () => {
      await flowBuilderPage.verifySuccessFullRegistration({
        userFirstName: userFirstName,
        userLastName: userLastName,
        email: attendeeEmail,
      });
    });

    await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
      await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
        attendeeEmail,
        eventId
      );
    });

    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      userFirstName,
      userLastName
    );

    let communicationHelper = new CommunicationHelper(
      orgApiContext,
      eventId,
      userInfoDto,
      eventInfoDto,
      true
    );

    await test.step("Fetching Email body from email recieved after registering", async () => {
      await communicationHelper.verifyConfirmationEmailContents(true, "(IST)");
    });

    await test.step(`Now fetching the user entry from event_registration table and validate the custom fields`, async () => {
      const userRegDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          eventId,
          attendeeEmail
        );
      console.log(userRegDataFromDb);
      const customFields = userRegDataFromDb["custom_field"];
     // const customFieldsJson = JSON.parse(customFields);
      for (const [key, value] of Object.entries(customDataValuesWithNewForm)) {
        await test.step(`validating the ${key} param from db entry`, async () => {
          expect(
            customFields[key],
            `expecting ${key} to be ${value}`
          ).toStrictEqual(value);
        });
      }
    });

    attendeeEmail = DataUtl.getRandomAttendeeEmail();

    await test.step(`Delete the new form inserted at the end and verify the change is reflected`, async () => {
      await flowBuilderController.deleteNewStepFromFlow(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
    });

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
      await attendeePage.waitForTimeout(5000);
    });

    await test.step(`Fill the registration form with new user`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form - Mandatory",
        userEmail: attendeeEmail,
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Non Mandatory Form - 1",
        fillNonMandatoryForm: false,
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Selection Form",
        fillHoliday: false,
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Logic Rich Text",
        logicRichText: false,
      });
    });

    await test.step(`Verify that the user is successfully registered`, async () => {
      await flowBuilderPage.verifySuccessFullRegistration({
        userFirstName: userFirstName,
        userLastName: userLastName,
        email: attendeeEmail,
      });
    });

    await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
      await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
        attendeeEmail,
        eventId
      );
    });

    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      userFirstName,
      userLastName
    );

    communicationHelper = new CommunicationHelper(
      orgApiContext,
      eventId,
      userInfoDto,
      eventInfoDto,
      true
    );

    await test.step("Fetching Email body from email recieved after registering", async () => {
      await communicationHelper.verifyConfirmationEmailContents(true, "(IST)");
    });

    await test.step(`Now fetching the user entry from event_registration table and validate the custom fields`, async () => {
      const userRegDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          eventId,
          attendeeEmail
        );
      console.log(userRegDataFromDb);
      const customFields = userRegDataFromDb["custom_field"];
      //const customFieldsJson = JSON.parse(customFields);
      for (const [key, value] of Object.entries(
        customDataValuesWithoutNewForm
      )) {
        await test.step(`validating the ${key} param from db entry`, async () => {
          expect(
            customFields[key],
            `expecting ${key} to be ${value}`
          ).toStrictEqual(value);
        });
      }
    });
  });
});
