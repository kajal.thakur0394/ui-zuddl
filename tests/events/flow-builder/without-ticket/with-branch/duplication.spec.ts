import {
  APIRequestContext,
  BrowserContext,
  Page,
  test,
} from "@playwright/test";
import { EventController } from "../../../../../controller/EventController";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  createNewEvent,
  updateEventLandingPageDetails,
  updateEventType,
} from "../../../../../util/apiUtil";
import EventType from "../../../../../enums/eventTypeEnum";
import { FlowBuilderController } from "../../../../../controller/FlowBuilderController";
import { FlowStatus, FlowType } from "../../../../../enums/FlowTypeEnum";
import { publishDataPayload } from "../../../../../interfaces/FlowBuilerControllerInterface";
import { FlowBuilderPage } from "../../../../../page-objects/flow-builder/flow-ticketing";
import { DataUtl } from "../../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../../util/email-validation-api-util";
import EventSettingID from "../../../../../enums/EventSettingEnum";
import EventRole from "../../../../../enums/eventRoleEnum";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";

test.describe(
  `@flow-builder @registraion-without-ticket`,
  {
    tag: "@smoke",
  },
  async () => {
    let orgBrowserContext: BrowserContext;
    let orgApiContext: APIRequestContext;
    let eventId: any;
    let attendeeBrowserContext: BrowserContext;
    let eventController: EventController;
    let flowBuilderPage: FlowBuilderPage;
    let flowBuilderController: FlowBuilderController;
    let publishdataPayLoad: publishDataPayload;
    let attendeePage: Page;
    let attendeeEmail: string;
    let attendeeEmail2: string;
    let registeredEmail: string;
    const userFirstName = "Jxtin";
    const userLastName = "zuddl";
    let eventInfoDto: EventInfoDTO;
    let originalEventId;

    test.beforeAll(async () => {
      await test.step(`setup browser contexts`, async () => {
        orgBrowserContext = await BrowserFactory.getBrowserContext({
          laodOrganiserCookies: true,
        });
        orgApiContext = orgBrowserContext.request;
        attendeeBrowserContext = await BrowserFactory.getBrowserContext({
          laodOrganiserCookies: false,
        });
        attendeePage = await attendeeBrowserContext.newPage();
      });

      await test.step(`create an event with FLEX as resgistration type`, async () => {
        eventId = await createNewEvent({
          api_request_context: orgApiContext,
          event_title: "flow-without-ticket automation",
          isFlex: true,
        });
        console.log(`event id ${eventId}`);
      });

      await test.step(`update an event with HYBRID as event type`, async () => {
        await updateEventType(orgApiContext, eventId, EventType.HYBRID);
        await updateEventLandingPageDetails(orgApiContext, eventId, {
          isMagicLinkEnabled: true,
        });
      });

      await test.step(`Initialise event controller`, async () => {
        eventController = new EventController(orgApiContext, eventId);
      });

      await test.step(`Get event details`, async () => {
        let eventInfo = await (await eventController.getEventInfo()).json();
        console.log("Event Info: ", eventInfo);
        eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
        eventInfoDto.startDateTime = eventInfo.startDateTime;
      });

      await enableDisableEmailTrigger(
        orgApiContext,
        eventId,
        EventSettingID.EmailOnRegistration,
        true,
        EventRole.ATTENDEE,
        true
      );

      await test.step(`Initialise flowbuilder controller`, async () => {
        flowBuilderController = new FlowBuilderController(
          orgApiContext,
          eventId
        );
      });

      await test.step(`make publish flow test data`, async () => {
        let flowDraftId = await flowBuilderController.getFlowId(
          FlowType.REGISTRATION,
          FlowStatus.DRAFT
        );
        publishdataPayLoad = {
          flowId: "",
          flowDraftId: flowDraftId,
          clientFlowDraftVersion: 2,
        };
      });

      await test.step(`update default flow steps with existing expected steps`, async () => {
        await flowBuilderController.generateFlowWithBranching(
          FlowType.REGISTRATION,
          FlowStatus.DRAFT
        );
      });

      await test.step(`now, publish the flow`, async () => {
        await flowBuilderController.publishFlowData(publishdataPayLoad);
        await attendeePage.waitForTimeout(5000);
      });

      // duplicate the event
      let duplicateEventId;
      await test.step(`duplicate the event`, async () => {
        duplicateEventId = await eventController.duplicateEvent({
          title: "flow-wo-ticket-w-branch - duplicate",
        });
        console.log(`duplicate event id ${duplicateEventId}`);
      });

      originalEventId = eventId;

      eventId = duplicateEventId;

      await test.step(`Initialise event controller for duplicated event`, async () => {
        eventController = new EventController(orgApiContext, duplicateEventId);
      });

      await test.step(`Get event details`, async () => {
        let eventInfo = await (await eventController.getEventInfo()).json();
        console.log("Event Info: ", eventInfo);
        eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
        eventInfoDto.startDateTime = eventInfo.startDateTime;
      });

      await test.step(`Get flowbuilder controller for duplicated event`, async () => {
        flowBuilderController = new FlowBuilderController(
          orgApiContext,
          duplicateEventId
        );

        let flowDraftId = await flowBuilderController.getFlowId(
          FlowType.REGISTRATION,
          FlowStatus.DRAFT
        );
      });

      await test.step(`set content to publish site`, async () => {
        await attendeePage.goto("https://example.com");
        await attendeePage.setContent(
          await flowBuilderController.generateEmbedHtml()
        );
        flowBuilderPage = new FlowBuilderPage(attendeePage);
      });
    });

    test.beforeEach(async () => {
      await test.step(`set content to publish site`, async () => {
        await attendeePage.goto("https://example.com");
        await attendeePage.setContent(
          await flowBuilderController.generateEmbedHtml()
        );
        flowBuilderPage = new FlowBuilderPage(attendeePage);
      });
      attendeeEmail = DataUtl.getRandomAttendeeEmail();
      attendeeEmail2 = DataUtl.getRandomAttendeeEmail("zuddl.com");
    });

    test.afterEach(async () => {
      await attendeePage.reload();
      // clear the cookies
      await attendeePage.context().clearCookies();
      await attendeePage.evaluate(() => {
        localStorage.clear();
      });
    });

    test.afterAll(async () => {
      await orgBrowserContext?.close();
      await attendeeBrowserContext?.close();
    });

    test(`ETE default flow where primary email contains "zuddl"`, async () => {
      await test.step(`Fill the registration form`, async () => {
        await flowBuilderPage.fillRegistrationForm({
          userFirstName: userFirstName,
          userLastName: userLastName,
          expectedFormHeading: "Form - Mandatory",
          userEmail: attendeeEmail2,
        });
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Non Mandatory Form - 1",
        phoneNumber: "9999999999",
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Selection Form",
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Logic Rich Text",
      });
      await flowBuilderPage.fillSingleFieldForm({
        expectedFormHeading: "Form - logic in number field type",
        expectedFieldName: "this is number field type",
        fieldValue: "8",
      });
      await flowBuilderPage.verifySuccessFullRegistration({
        userFirstName: userFirstName,
        userLastName: userLastName,
        email: attendeeEmail2,
      });
    });

    test(`ETE flow primary email does not contain "zuddl",secondary email is not @gmail.com or @yahoo.com`, async () => {
      await test.step(`Fill the registration form`, async () => {
        await flowBuilderPage.fillRegistrationForm({
          userFirstName: userFirstName,
          userLastName: userLastName,
          expectedFormHeading: "Form - Mandatory",
          userEmail: attendeeEmail,
        });
        await test.step(`Verifying email format check`, async () => {
          await flowBuilderPage.fillSingleFieldForm({
            expectedFormHeading: "Form - custom email form",
            expectedFieldName: "what is your secondary email",
            fieldValue: "attendeeEmail",
            expectError: true,
            clickContinue: true,
          });
        });
        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - custom email form",
          expectedFieldName: "what is your secondary email",
          fieldValue: attendeeEmail,
        });
        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form",
          expectedFieldName: "Field - this is text field type",
          fieldValue: "attendeeEmail",
        });
        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - logic in number field type",
          expectedFieldName: "this is number field type",
          fieldValue: "8",
        });
        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - number is 8",
          expectedFieldName: "Field - number is 8 form",
          fieldValue: "8",
        });
        await flowBuilderPage.verifySuccessFullRegistration({
          userFirstName: userFirstName,
          userLastName: userLastName,
          email: attendeeEmail,
        });
      });
    });

    test(`ETE flow primary email does not contain "zuddl",secondary email is @gmail.com`, async () => {
      await test.step(`Fill the registration form`, async () => {
        await flowBuilderPage.fillRegistrationForm({
          userFirstName: userFirstName,
          userLastName: userLastName,
          expectedFormHeading: "Form - Mandatory",
          userEmail: attendeeEmail,
        });

        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - custom email form",
          expectedFieldName: "what is your secondary email",
          fieldValue: "jxtin@gmail.com",
        });

        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - email contains yahoo and gmail",
          expectedFieldName: "Field - this is text field type",
          fieldValue: "attendeeEmail",
        });

        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - logic in number field type",
          expectedFieldName: "this is number field type",
          fieldValue: "8",
        });
        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - number is 8",
          expectedFieldName: "Field - number is 8 form",
          fieldValue: "8",
        });
      });

      await test.step(`Verify the email validation`, async () => {
        await flowBuilderPage.verifySuccessFullRegistration({
          userFirstName: userFirstName,
          userLastName: userLastName,
          email: attendeeEmail,
        });
      });
    });

    test(`ETE flow primary email does not contain "zuddl",secondary email is @yahoo.com`, async () => {
      await test.step(`Fill the registration form`, async () => {
        await flowBuilderPage.fillRegistrationForm({
          userFirstName: userFirstName,
          userLastName: userLastName,
          expectedFormHeading: "Form - Mandatory",
          userEmail: attendeeEmail,
        });

        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - custom email form",
          expectedFieldName: "what is your secondary email",
          fieldValue: "jxtin@yahoo.com",
        });

        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - email contains yahoo and gmail",
          expectedFieldName: "Field - this is text field type",
          fieldValue: "attendeeEmail",
        });

        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - logic in number field type",
          expectedFieldName: "this is number field type",
          fieldValue: "8",
        });

        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - number is 8",
          expectedFieldName: "Field - number is 8 form",
          fieldValue: "8",
        });

        await flowBuilderPage.verifySuccessFullRegistration({
          userFirstName: userFirstName,
          userLastName: userLastName,
          email: attendeeEmail,
        });
      });
    });

    test(`ETE flow primary email is abc@abc.com ,secondary email is @yahoo.com`, async () => {
      attendeeEmail = "abc@abc.com";
      await test.step(`Fill the registration form`, async () => {
        await flowBuilderPage.fillRegistrationForm({
          userFirstName: userFirstName,
          userLastName: userLastName,
          expectedFormHeading: "Form - Mandatory",
          userEmail: attendeeEmail,
        });

        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - custom email form",
          expectedFieldName: "what is your secondary email",
          fieldValue: "jxtin@yahoo.com",
        });

        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - email contains yahoo and gmail",
          expectedFieldName: "Field - this is text field type",
          fieldValue: "attendeeEmail",
        });

        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - logic in number field type",
          expectedFieldName: "this is number field type",
          fieldValue: "8",
        });

        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - number is 8",
          expectedFieldName: "Field - number is 8 form",
          fieldValue: "8",
        });

        await flowBuilderPage.verifySuccessFullRegistration({
          userFirstName: userFirstName,
          userLastName: userLastName,
          email: attendeeEmail,
        });
      });
    });

    test(`ETE flow with logic in number field case number more than 8`, async () => {
      await test.step(`Fill the registration form`, async () => {
        await flowBuilderPage.fillRegistrationForm({
          userFirstName: userFirstName,
          userLastName: userLastName,
          expectedFormHeading: "Form - Mandatory",
          userEmail: attendeeEmail,
        });
        await test.step(`Verifying email format check`, async () => {
          await flowBuilderPage.fillSingleFieldForm({
            expectedFormHeading: "Form - custom email form",
            expectedFieldName: "what is your secondary email",
            fieldValue: "attendeeEmail",
            expectError: true,
            clickContinue: true,
          });
        });
        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - custom email form",
          expectedFieldName: "what is your secondary email",
          fieldValue: attendeeEmail,
        });
        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form",
          expectedFieldName: "Field - this is text field type",
          fieldValue: "attendeeEmail",
        });
        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - logic in number field type",
          expectedFieldName: "this is number field type",
          fieldValue: "9",
        });
        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - more than 8 form",
          expectedFieldName: "this is more than 8 form",
          fieldValue: "9",
        });
        await flowBuilderPage.verifySuccessFullRegistration({
          userFirstName: userFirstName,
          userLastName: userLastName,
          email: attendeeEmail,
        });
      });
    });

    test(`ETE flow with logic in number field case number less than 8`, async () => {
      await test.step(`Fill the registration form`, async () => {
        await flowBuilderPage.fillRegistrationForm({
          userFirstName: userFirstName,
          userLastName: userLastName,
          expectedFormHeading: "Form - Mandatory",
          userEmail: attendeeEmail,
        });
        await test.step(`Verifying email format check`, async () => {
          await flowBuilderPage.fillSingleFieldForm({
            expectedFormHeading: "Form - custom email form",
            expectedFieldName: "what is your secondary email",
            fieldValue: "attendeeEmail",
            expectError: true,
            clickContinue: true,
          });
        });
        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - custom email form",
          expectedFieldName: "what is your secondary email",
          fieldValue: attendeeEmail,
        });
        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form",
          expectedFieldName: "Field - this is text field type",
          fieldValue: "attendeeEmail",
        });
        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - logic in number field type",
          expectedFieldName: "this is number field type",
          fieldValue: "7",
        });
        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - less than 8 form",
          expectedFieldName: "number is les than 8",
          fieldValue: "7",
        });
        await flowBuilderPage.verifySuccessFullRegistration({
          userFirstName: userFirstName,
          userLastName: userLastName,
          email: attendeeEmail,
        });
      });
    });

    test(`ETE flow primary email contains "zuddl", phone number contains 8, country contains India`, async () => {
      // if country contains india then form heading will be Form - test 1
      // else it will be Form - test 2
      attendeeEmail = DataUtl.getRandomAttendeeEmail();
      attendeeEmail = attendeeEmail.replace("@", "zuddl@");
      const countryName = "India";
      await test.step(`Fill the registration form`, async () => {
        await flowBuilderPage.fillRegistrationForm({
          userFirstName: userFirstName,
          userLastName: userLastName,
          expectedFormHeading: "Form - Mandatory",
          userEmail: attendeeEmail,
        });

        await flowBuilderPage.handleNonMandatoryForm(false, "80000000");
        await flowBuilderPage.clickOnContinueButton();

        await flowBuilderPage.handlePhoneNumber8Form({
          luckyNumber: "8",
          countryName: countryName,
        });

        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - test 1",
          expectedFieldName: "Field",
          fieldValue: "Hello",
        });

        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - logic in number field type",
          expectedFieldName: "this is number field type",
          fieldValue: "8",
        });
        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - number is 8",
          expectedFieldName: "Field - number is 8 form",
          fieldValue: "8",
        });
        await flowBuilderPage.verifySuccessFullRegistration({
          userFirstName: userFirstName,
          userLastName: userLastName,
          email: attendeeEmail,
        });
      });
    });

    test(`ETE default flow where primary email contains "zuddl", weekdays is Saturday and holiday is NO`, async () => {
      await test.step(`Fill the registration form`, async () => {
        await flowBuilderPage.fillRegistrationForm({
          userFirstName: userFirstName,
          userLastName: userLastName,
          expectedFormHeading: "Form - Mandatory",
          userEmail: attendeeEmail2,
        });
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Non Mandatory Form - 1",
        phoneNumber: "9999999999",
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Selection Form",
        isholiday: false,
      });
      await flowBuilderPage.fillSingleFieldForm({
        expectedFormHeading: "Form - branch on drop down",
        expectedFieldName: "this is text field type",
      });
      await flowBuilderPage.fillSingleFieldForm({
        expectedFormHeading: "Form - logic in number field type",
        expectedFieldName: "this is number field type",
        fieldValue: "8",
      });
      await flowBuilderPage.verifySuccessFullRegistration({
        userFirstName: userFirstName,
        userLastName: userLastName,
        email: attendeeEmail2,
      });
    });

    test(`ETE default flow where primary email contains "zuddl", weekdays is Monday and holiday is YES`, async () => {
      await test.step(`Fill the registration form`, async () => {
        await flowBuilderPage.fillRegistrationForm({
          userFirstName: userFirstName,
          userLastName: userLastName,
          expectedFormHeading: "Form - Mandatory",
          userEmail: attendeeEmail2,
        });
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Non Mandatory Form - 1",
        phoneNumber: "9999999999",
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Selection Form",
        weekend: false,
      });
      await flowBuilderPage.fillSingleFieldForm({
        expectedFormHeading: "Form - branch on multiselect field type",
        expectedFieldName: "this is text field type",
      });
      await flowBuilderPage.fillSingleFieldForm({
        expectedFormHeading: "Form - logic in number field type",
        expectedFieldName: "this is number field type",
        fieldValue: "8",
      });
      await flowBuilderPage.fillSingleFieldForm({
        expectedFormHeading: "Form - number is 8",
        expectedFieldName: "Field - number is 8 form",
        fieldValue: "8",
      });
      await flowBuilderPage.verifySuccessFullRegistration({
        userFirstName: userFirstName,
        userLastName: userLastName,
        email: attendeeEmail2,
      });
    });

    test(`ETE flow primary email contains "zuddl", phone number contains 8, country does not contain India`, async () => {
      // if country contains india then form heading will be Form - test 1
      // else it will be Form - test 2
      attendeeEmail = DataUtl.getRandomAttendeeEmail();
      attendeeEmail = attendeeEmail.replace("@", "zuddl@");
      const countryName = "Russia";
      await test.step(`Fill the registration form`, async () => {
        await flowBuilderPage.fillRegistrationForm({
          userFirstName: userFirstName,
          userLastName: userLastName,
          expectedFormHeading: "Form - Mandatory",
          userEmail: attendeeEmail,
        });

        await flowBuilderPage.handleNonMandatoryForm(false, "80000000");
        await flowBuilderPage.clickOnContinueButton();

        await flowBuilderPage.handlePhoneNumber8Form({
          luckyNumber: "8",
          countryName: countryName,
        });

        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - test 2",
          expectedFieldName: "Field",
          fieldValue: "Hello",
        });

        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - logic in number field type",
          expectedFieldName: "this is number field type",
          fieldValue: "8",
        });
        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - number is 8",
          expectedFieldName: "Field - number is 8 form",
          fieldValue: "8",
        });
        await flowBuilderPage.verifySuccessFullRegistration({
          userFirstName: userFirstName,
          userLastName: userLastName,
          email: attendeeEmail,
        });
      });
    });
  }
);
