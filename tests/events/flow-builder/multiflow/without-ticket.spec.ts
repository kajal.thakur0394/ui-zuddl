import {
  APIRequestContext,
  BrowserContext,
  Page,
  test,
  expect,
} from "@playwright/test";
import { EventController } from "../../../../controller/EventController";
import BrowserFactory from "../../../../util/BrowserFactory";
import {
  createNewEvent,
  updateEventLandingPageDetails,
  updateEventType,
} from "../../../../util/apiUtil";
import EventType from "../../../../enums/eventTypeEnum";
import { FlowBuilderController } from "../../../../controller/FlowBuilderController";
import { FlowStatus, FlowType } from "../../../../enums/FlowTypeEnum";
import { publishDataPayload } from "../../../../interfaces/FlowBuilerControllerInterface";
import { FlowBuilderPage } from "../../../../page-objects/flow-builder/flow-ticketing";
import { DataUtl } from "../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../util/email-validation-api-util";
import EventSettingID from "../../../../enums/EventSettingEnum";
import EventRole from "../../../../enums/eventRoleEnum";
import { EventInfoDTO } from "../../../../dto/eventInfoDto";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";
import { UserInfoDTO } from "playwright-qa/dto/userInfoDto";
import CommunicationHelper from "../../communication/communication-tools";
import { CSVController } from "../../../../controller/CSVController";
import { readCsvStringAsJson } from "../../../../util/commonUtil";

test.describe(`@flow-builder @registraion-without-ticket`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId: any;
  let attendeeBrowserContext: BrowserContext;
  let eventController: EventController;
  let flowBuilderPage: FlowBuilderPage;
  let flowBuilderController: FlowBuilderController;
  let publishdataPayLoad: publishDataPayload;
  let attendeePage: Page;
  let attendeeEmail: string;
  let attendeeEmail2: string;
  let registeredEmail: string;
  const userFirstName = "Jxtin";
  const userLastName = "zuddl";
  let eventInfoDto: EventInfoDTO;
  let flowOneEmbed: string;
  let flowTwoEmbed: string;

  test.beforeAll(async () => {
    await test.step(`setup browser contexts`, async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: true,
      });
      orgApiContext = orgBrowserContext.request;
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage = await attendeeBrowserContext.newPage();
    });

    await test.step(`create an event with FLEX as resgistration type`, async () => {
      eventId = await createNewEvent({
        api_request_context: orgApiContext,
        event_title: "flow-without-ticket automation",
        isFlex: true,
      });
      console.log(`event id ${eventId}`);
    });

    await test.step(`update an event with HYBRID as event type`, async () => {
      await updateEventType(orgApiContext, eventId, EventType.HYBRID);
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        isMagicLinkEnabled: true,
      });
    });

    await test.step(`Initialise event controller`, async () => {
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step(`Get event details`, async () => {
      let eventInfo = await (await eventController.getEventInfo()).json();
      console.log("Event Info: ", eventInfo);
      eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
      eventInfoDto.startDateTime = eventInfo.startDateTime;
    });

    await enableDisableEmailTrigger(
      orgApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );

    await test.step(`Initialise flowbuilder controller`, async () => {
      flowBuilderController = new FlowBuilderController(orgApiContext, eventId);
    });

    await test.step(`update default flow steps with existing expected steps`, async () => {
      await flowBuilderController.generateMultiflowWithoutTicketing();
    });
    const flowIds = await flowBuilderController.getPublishedFlowIds();

    flowOneEmbed = await flowBuilderController.getHtmlForFlow(flowIds[0]);
    flowTwoEmbed = await flowBuilderController.getHtmlForFlow(flowIds[1]);
    console.log(`flowOneEmbed: ${flowOneEmbed}`); // branched flow
    console.log(`flowTwoEmbed: ${flowTwoEmbed}`); // linear flow
  });

  async function setupBrowserContext(branch: boolean = false) {
    let flowEmbed = branch ? flowOneEmbed : flowTwoEmbed;
    await attendeePage.goto("https://example.com");
    await attendeePage.setContent(flowEmbed);
    flowBuilderPage = new FlowBuilderPage(attendeePage);
    return flowBuilderPage;
  }

  test.beforeEach(async () => {
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
    attendeeEmail2 = DataUtl.getRandomAttendeeEmail("zuddl.com");
  });

  test.afterEach(async () => {
    await attendeePage.reload();
    // clear the cookies
    await attendeePage.context().clearCookies();
    await attendeePage.evaluate(() => {
      localStorage.clear();
    });
  });

  test.afterAll(async () => {
    await orgBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test(`ETE default flow where primary email contains "zuddl" @branching`, async () => {
    flowBuilderPage = await setupBrowserContext(true);
    await test.step(`Fill the registration form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form - Mandatory",
        userEmail: attendeeEmail2,
      });
    });
    await flowBuilderPage.fillRegistrationForm({
      expectedFormHeading: "Non Mandatory Form - 1",
      phoneNumber: "9999999999",
    });
    await flowBuilderPage.fillRegistrationForm({
      expectedFormHeading: "Selection Form",
    });
    await flowBuilderPage.fillRegistrationForm({
      expectedFormHeading: "Logic Rich Text",
    });
    await flowBuilderPage.fillSingleFieldForm({
      expectedFormHeading: "Form - logic in number field type",
      expectedFieldName: "this is number field type",
      fieldValue: "8",
    });
    await flowBuilderPage.verifySuccessFullRegistration({
      userFirstName: userFirstName,
      userLastName: userLastName,
      email: attendeeEmail2,
    });
  });

  test(`ETE flow primary email does not contain "zuddl",secondary email is not @gmail.com or @yahoo.com @branching`, async () => {
    flowBuilderPage = await setupBrowserContext(true);
    await test.step(`Fill the registration form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form - Mandatory",
        userEmail: attendeeEmail,
      });
      await test.step(`Verifying email format check`, async () => {
        await flowBuilderPage.fillSingleFieldForm({
          expectedFormHeading: "Form - custom email form",
          expectedFieldName: "what is your secondary email",
          fieldValue: "attendeeEmail",
          expectError: true,
          clickContinue: true,
        });
      });
      await flowBuilderPage.fillSingleFieldForm({
        expectedFormHeading: "Form - custom email form",
        expectedFieldName: "what is your secondary email",
        fieldValue: attendeeEmail,
      });
      await flowBuilderPage.fillSingleFieldForm({
        expectedFormHeading: "Form",
        expectedFieldName: "Field - this is text field type",
        fieldValue: "attendeeEmail",
      });
      await flowBuilderPage.fillSingleFieldForm({
        expectedFormHeading: "Form - logic in number field type",
        expectedFieldName: "this is number field type",
        fieldValue: "8",
      });
      await flowBuilderPage.fillSingleFieldForm({
        expectedFormHeading: "Form - number is 8",
        expectedFieldName: "Field - number is 8 form",
        fieldValue: "8",
      });
      await flowBuilderPage.verifySuccessFullRegistration({
        userFirstName: userFirstName,
        userLastName: userLastName,
        email: attendeeEmail,
      });
    });
  });

  test(`ETE flow primary email does not contain "zuddl",secondary email is @gmail.com @branching`, async () => {
    flowBuilderPage = await setupBrowserContext(true);

    await test.step(`Fill the registration form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form - Mandatory",
        userEmail: attendeeEmail,
      });

      await flowBuilderPage.fillSingleFieldForm({
        expectedFormHeading: "Form - custom email form",
        expectedFieldName: "what is your secondary email",
        fieldValue: "jxtin@gmail.com",
      });

      await flowBuilderPage.fillSingleFieldForm({
        expectedFormHeading: "Form - email contains yahoo and gmail",
        expectedFieldName: "Field - this is text field type",
        fieldValue: "attendeeEmail",
      });

      await flowBuilderPage.fillSingleFieldForm({
        expectedFormHeading: "Form - logic in number field type",
        expectedFieldName: "this is number field type",
        fieldValue: "8",
      });
      await flowBuilderPage.fillSingleFieldForm({
        expectedFormHeading: "Form - number is 8",
        expectedFieldName: "Field - number is 8 form",
        fieldValue: "8",
      });
    });

    await test.step(`Verify the email validation`, async () => {
      await flowBuilderPage.verifySuccessFullRegistration({
        userFirstName: userFirstName,
        userLastName: userLastName,
        email: attendeeEmail,
      });
    });
  });

  test(`ETE flow primary email does not contain "zuddl",secondary email is @yahoo.com  @branching`, async () => {
    flowBuilderPage = await setupBrowserContext(true);

    await test.step(`Fill the registration form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form - Mandatory",
        userEmail: attendeeEmail,
      });

      await flowBuilderPage.fillSingleFieldForm({
        expectedFormHeading: "Form - custom email form",
        expectedFieldName: "what is your secondary email",
        fieldValue: "jxtin@yahoo.com",
      });

      await flowBuilderPage.fillSingleFieldForm({
        expectedFormHeading: "Form - email contains yahoo and gmail",
        expectedFieldName: "Field - this is text field type",
        fieldValue: "attendeeEmail",
      });

      await flowBuilderPage.fillSingleFieldForm({
        expectedFormHeading: "Form - logic in number field type",
        expectedFieldName: "this is number field type",
        fieldValue: "8",
      });

      await flowBuilderPage.fillSingleFieldForm({
        expectedFormHeading: "Form - number is 8",
        expectedFieldName: "Field - number is 8 form",
        fieldValue: "8",
      });

      await flowBuilderPage.verifySuccessFullRegistration({
        userFirstName: userFirstName,
        userLastName: userLastName,
        email: attendeeEmail,
      });
    });
  });

  test(`@without-ticket-and-branch Verify that the new user successfully registered to the event`, async () => {
    flowBuilderPage = await setupBrowserContext(false);

    const customDataValues = {
      "Holiday?1": "Yes",
      richText: "",
      Weekdays1: [
        {
          label: "Saturday",
          value: "Saturday",
        },
        {
          label: "Sunday",
          value: "Sunday",
        },
      ],
      disclaimers: [
        {
          text: "<p>Mandatory disclaimer</p>\n",
          consentGiven: true,
        },
      ],
      YourLinkedIn1: "https://www.linkedin.com/in/testuser/",
      "Title-Optional1": "Engineer",
    };

    await test.step(`Fill the registration form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form - Mandatory",
        userEmail: attendeeEmail,
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Non Mandatory Form - 1",
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Selection Form",
      });
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Logic Rich Text",
      });
    });

    await test.step(`Verify that the user is successfully registered`, async () => {
      await flowBuilderPage.verifySuccessFullRegistration({
        userFirstName: userFirstName,
        userLastName: userLastName,
        email: attendeeEmail,
      });
    });

    await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
      await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
        attendeeEmail,
        eventId
      );
    });

    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      userFirstName,
      userLastName
    );

    let communicationHelper = new CommunicationHelper(
      orgApiContext,
      eventId,
      userInfoDto,
      eventInfoDto,
      true
    );

    await test.step("Fetching Email body from email recieved after registering", async () => {
      await communicationHelper.verifyConfirmationEmailContents(true, "(IST)");
    });

    await test.step(`Now fetching the user entry from event_registration table and validate the custom fields`, async () => {
      const userRegDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          eventId,
          attendeeEmail
        );
      console.log(userRegDataFromDb);
      const customFields = userRegDataFromDb["custom_field"];
      // const customFieldsJson = JSON.parse(customFields);
      console.log(customFields);
      for (const [key, value] of Object.entries(customDataValues)) {
        console.log(key, value);
        await test.step(`validating the ${key} param from db entry`, async () => {
          expect(
            customFields[key],
            `expecting ${key} to be ${value}`
          ).toStrictEqual(value);
        });
      }
    });

    registeredEmail = attendeeEmail;
  });

  test(`@without-ticket-and-branch Verify attendee data in download csv or database`, async () => {
    flowBuilderPage = await setupBrowserContext(false);

    await test.step(`Verify data in downloaded CSV`, async () => {
      let csvController = new CSVController(orgApiContext, eventId);
      let csvResponse = await csvController.getDownloadCsv();
      let jsonData = await readCsvStringAsJson(csvResponse);
      await test.step(`verify the download csv contains the attendee record`, async () => {
        console.log(jsonData);
        expect(
          jsonData[3].attendee_email.toLowerCase(),
          `expecting ${registeredEmail} to be present in the csv`
        ).toBe(registeredEmail.toLowerCase());
      });
    });
  });

  test(`@without-ticket-and-branch Verify already register user cannot register again to the event`, async () => {
    flowBuilderPage = await setupBrowserContext(false);
    await attendeePage.waitForTimeout(5000);

    await test.step(`Try registering with same details again and expect error.`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form - Mandatory",
        expectErrorPostContinue: true,
        userEmail: registeredEmail,
      });
    });
  });

  test(`@without-ticket-and-branch Verify form does not continue without mandatory field`, async () => {
    flowBuilderPage = await setupBrowserContext(false);
    await test.step(`Fill the registration form without disclaimer`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        userEmail: attendeeEmail,
        expectedFormHeading: "Form - Mandatory",
        skipMandatoryDisclaimer: true,
      });
    });

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });

    await test.step(`Fill the registration form without firstname`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: "",
        userLastName: userLastName,
        expectedFormHeading: "Form - Mandatory",
        expectErrorPostContinue: true,
        userEmail: attendeeEmail,
      });
    });

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });

    await test.step(`Fill the registration form without lastname`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: "",
        expectedFormHeading: "Form - Mandatory",
        expectErrorPostContinue: true,
        userEmail: attendeeEmail,
      });
    });

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });

    await test.step(`Fill the registration form with unformatted email`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        userEmail: "testattendee",
        expectedFormHeading: "Form - Mandatory",
        expectErrorPreContinue: true,
      });
    });
  });
});
