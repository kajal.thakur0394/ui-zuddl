import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../../../../controller/EventController";
import { FlowBuilderController } from "../../../../../../controller/FlowBuilderController";
import { TicketingController } from "../../../../../../controller/TicketingController";
import { EventInfoDTO } from "../../../../../../dto/eventInfoDto";
import { UserInfoDTO } from "../../../../../../dto/userInfoDto";
import EventSettingID from "../../../../../../enums/EventSettingEnum";
import { FlowStatus, FlowType } from "../../../../../../enums/FlowTypeEnum";
import EventRole from "../../../../../../enums/eventRoleEnum";
import EventType from "../../../../../../enums/eventTypeEnum";
import {
  ExpectedTicketDetailsData,
  publishDataPayload,
} from "../../../../../../interfaces/FlowBuilerControllerInterface";
import { FlowBuilderPage } from "../../../../../../page-objects/flow-builder/flow-ticketing";
import CommunicationHelper from "../../../../../../tests/events/communication/communication-tools";
import BrowserFactory from "../../../../../../util/BrowserFactory";
import {
  createNewEvent,
  getSignedS3Url,
  updateEventLandingPageDetails,
  updateEventType,
} from "../../../../../../util/apiUtil";
import { DataUtl } from "../../../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../../../util/email-validation-api-util";
import { convertStringToNumber } from "../../../../../../util/commonUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";
test.describe(
  `@flow-builder @registration-with-ticket @paid-ticket @bulk-ticketing`,
  {
    tag: "@smoke",
  },
  async () => {
    let orgBrowserContext: BrowserContext;
    let orgApiContext: APIRequestContext;
    let eventId: any;
    let attendeeBrowserContext: BrowserContext;
    let eventController: EventController;
    let flowBuilderPage: FlowBuilderPage;
    let flowBuilderController: FlowBuilderController;
    let publishdataPayLoad: publishDataPayload;
    let attendeePage: Page;
    let couponCode: string;
    let attendeeEmail: string;
    const userFirstName = "Jxtin";
    const userLastName = "zuddl";
    let eventInfoDto: EventInfoDTO;
    let ticketingController: TicketingController;
    let ticketInfo1 = {
      name: "ticket1",
      numberOfTickets: 10,
      pricePerTicket: 100,
      isPaidTicket: true,
      isBulkPurchaseEnabled: true,
      minTicketsPerOrder: 2,
      maxTicketsPerOrder: 5,
      soldTickets: 0,
    };
    let ticketInfo2 = {
      name: "ticket2",
      numberOfTickets: 10,
      pricePerTicket: 50,
      isPaidTicket: true,
      isBulkPurchaseEnabled: true,
      minTicketsPerOrder: 2,
      maxTicketsPerOrder: 4,
    };
    let ticketInfo3 = {
      name: "ticket3",
      numberOfTickets: 10,
      pricePerTicket: 25,
      isPaidTicket: true,
      isAvailableForAll: false,
      isDomainRestriction: true,
      domains: "gmail.com",
      isBulkPurchaseEnabled: true,
      minTicketsPerOrder: 3,
      maxTicketsPerOrder: 4,
    };
    let gmailCouponCode = DataUtl.getRandomCouponName();
    let invitelist1CouponCode = DataUtl.getRandomCouponName();

    test.beforeAll(async () => {
      await test.step(`setup browser contexts`, async () => {
        orgBrowserContext = await BrowserFactory.getBrowserContext({
          laodOrganiserCookies: true,
        });
        orgApiContext = orgBrowserContext.request;
        attendeeBrowserContext = await BrowserFactory.getBrowserContext({
          laodOrganiserCookies: false,
        });
        attendeePage = await attendeeBrowserContext.newPage();
      });

      await test.step(`create an event with FLEX as resgistration type`, async () => {
        eventId = await createNewEvent({
          api_request_context: orgApiContext,
          event_title: "flow-with-bulk-ticket automation",
          isFlex: true,
        });
        console.log(`event id ${eventId}`);
      });

      await test.step(`update an event with IN-PERSON as event type`, async () => {
        await updateEventType(orgApiContext, eventId, EventType.IN_PERSON);
        await updateEventLandingPageDetails(orgApiContext, eventId, {
          isMagicLinkEnabled: true,
        });
      });

      await test.step(`Initialise event controller`, async () => {
        eventController = new EventController(orgApiContext, eventId);
      });

      await test.step(`Get event details`, async () => {
        let eventInfo = await (await eventController.getEventInfo()).json();
        console.log("Event Info: ", eventInfo);
        eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
        eventInfoDto.startDateTime = eventInfo.startDateTime;
      });
      let s3Url_l1 = await getSignedS3Url(
        orgApiContext,
        "test-data/invitelist-1.csv"
      );
      const groupName1 = "audience1";
      const groupName2 = "audience2";
      let s3Url_l2 = await getSignedS3Url(
        orgApiContext,
        "test-data/invitelist-2.csv"
      );
      await new Promise((r) => setTimeout(r, 15000));
      await eventController.addInviteListToEvent(s3Url_l1, groupName1, "audi1");
      await new Promise((r) => setTimeout(r, 15000));
      await eventController.addInviteListToEvent(s3Url_l2, groupName2, "audi2");
      await new Promise((r) => setTimeout(r, 20000));

      let invitelistGroupId1 =
        await eventController.getInviteListAudienceGroupId(groupName1);
      let invitelistGroupId2 =
        await eventController.getInviteListAudienceGroupId(groupName2);
      console.log("invitelistGroupId1", invitelistGroupId1);
      console.log("invitelistGroupId2", invitelistGroupId2);

      await enableDisableEmailTrigger(
        orgApiContext,
        eventId,
        EventSettingID.EmailOnRegistration,
        true,
        EventRole.ATTENDEE,
        true
      );

      ticketingController = new TicketingController(orgApiContext, eventId);

      await ticketingController.selectGateway();

      await ticketingController.createTicket(ticketInfo1);

      await ticketingController.createTicket(ticketInfo2);

      await ticketingController.createTicket(ticketInfo3);

      await test.step(`Create coupon for paid tickets`, async () => {
        const ticketTypeId1 = await ticketingController.getTicketTypeId(
          ticketInfo1.name
        );
        const ticketTypeId2 = await ticketingController.getTicketTypeId(
          ticketInfo2.name
        );

        const gmailCouponInfo = {
          couponCode: gmailCouponCode,
          description: `Coupon for ticket1, ${gmailCouponCode} only for gmail.com and gmail2.com domains`,
          active: true,
          singleUsage: false,
          discountValue: 20,
          ticketTypeIds: [ticketTypeId1],
          isAvailableForEveryone: false,
          isDomainRestriction: true,
          domains: "gmail.com,gmail2.com",
        };
        await ticketingController.addCoupons(gmailCouponInfo);

        const invitelist1CouponInfo = {
          couponCode: invitelist1CouponCode,
          description: `Coupon for ticket1, ${invitelist1CouponCode} only for invitelist1 audience`,
          active: true,
          singleUsage: false,
          discountValue: 20,
          ticketTypeIds: [ticketTypeId2],
          isAvailableForEveryone: false,
          isInvitedList: true,
          audienceGroupIds: [invitelistGroupId1],
        };
        await ticketingController.addCoupons(invitelist1CouponInfo);
      });

      await test.step(`Initialise flowbuilder controller`, async () => {
        flowBuilderController = new FlowBuilderController(
          orgApiContext,
          eventId
        );
      });
      await test.step(`update default flow steps with existing expected steps`, async () => {
        await flowBuilderController.generateSimpleTicketingFlow(
          FlowType.REGISTRATION,
          FlowStatus.DRAFT
        );
      });

      await test.step(`make publish flow test data`, async () => {
        let flowDraftId = await flowBuilderController.getFlowId(
          FlowType.REGISTRATION,
          FlowStatus.DRAFT
        );
        publishdataPayLoad = {
          flowId: "",
          flowDraftId: flowDraftId,
          clientFlowDraftVersion: 2,
        };
      });

      await test.step(`now, publish the flow`, async () => {
        await flowBuilderController.publishFlowData(publishdataPayLoad);
        await attendeePage.waitForTimeout(5000);
      });

      await test.step(`set content to publish site`, async () => {
        await attendeePage.goto("https://example.com");
        await attendeePage.setContent(
          await flowBuilderController.generateEmbedHtml()
        );
        flowBuilderPage = new FlowBuilderPage(attendeePage);
      });
    });

    test.beforeEach(async () => {
      await test.step(`set content to publish site`, async () => {
        await attendeePage.goto("https://example.com");
        await attendeePage.setContent(
          await flowBuilderController.generateEmbedHtml()
        );
        flowBuilderPage = new FlowBuilderPage(attendeePage);
      });
      attendeeEmail = DataUtl.getRandomAttendeeEmail();
    });

    test.afterEach(async () => {
      await attendeePage.reload();
      // clear the cookies
      await attendeePage.context().clearCookies();
      await attendeePage.evaluate(() => {
        localStorage.clear();
      });
    });

    test.afterAll(async () => {
      await orgBrowserContext?.close();
      await attendeeBrowserContext?.close();
    });

    test(`TC001 : @flow-builder @registration-with-ticket @paid-ticket @bulk-ticketing @domain-access @happy-flow Two users with gmail.com and gmail2.com buy ticket1 with coupon gmailCouponCode`, async () => {
      const ticketName = ticketInfo1.name;
      const ticketQty = 2;
      let attendeeDetails = [];
      couponCode = gmailCouponCode;
      const ticketDetailsData: ExpectedTicketDetailsData = {
        ticketName: ticketName,
        ticketPrice: "$200",
        dicountValue: "$20",
        totalPriceAfterDiscount: "$180",
      };

      attendeeEmail = DataUtl.getRandomAttendeeEmail("gmail.com");

      attendeeDetails.push({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });

      for (let i = 0; i < ticketQty - 1; i++) {
        attendeeDetails.push({
          firstName: DataUtl.getRandomName(),
          lastName: DataUtl.getRandomName(),
          email: DataUtl.getRandomAttendeeEmail("gmail2.com"),
        });
      }

      await test.step(`User gets a paid ticket`, async () => {
        await flowBuilderPage.fillRegistrationForm({
          userFirstName: userFirstName,
          userLastName: userLastName,
          expectedFormHeading: "Form",
          userEmail: attendeeEmail,
        });

        await flowBuilderPage.fillRegistrationForm({
          ticketInfo: {
            ticketName: ticketName,
            quantity: ticketQty,
            price: ticketInfo1.pricePerTicket,
            coupon: couponCode,
            expectSoldOut: false,
          },
          expectedFormHeading: "Ticket",
        });

        await flowBuilderPage.page.waitForTimeout(5000);

        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: attendeeDetails,
          ticketInfo: {
            ticketName: ticketName,
            quantity: ticketQty,
            price: ticketInfo1.pricePerTicket,
            coupon: couponCode,
            expectSoldOut: false,
          },
        });
        await flowBuilderPage.page.waitForTimeout(5000);
        await flowBuilderPage.verifyPaidTicketDetails(true, ticketDetailsData);
        await flowBuilderPage.verifyOrderSummaryBulk({
          dataToVerify: {
            attendeeDetails: attendeeDetails,
            ticketName: ticketName,
          },
        });
        await flowBuilderPage.page.waitForTimeout(5000);
        await flowBuilderPage.verifyPaidTicketDetails(true, {
          ticketName: ticketName,
          ticketPrice: (ticketQty * ticketInfo1.pricePerTicket).toString(),
          dicountValue: "20",
          totalPriceAfterDiscount: (
            ticketQty * ticketInfo1.pricePerTicket
          ).toString(),
        });

        await flowBuilderPage.fillPaymentDetailsPaidTicket({});
        await flowBuilderPage.page.waitForTimeout(5000);
        await flowBuilderPage.verifyBulkTicketingSuccess({
          attendeeDetails: attendeeDetails,
          ticketDetails: {
            ticketName: ticketName,
          },
        });
      });

      await flowBuilderPage.page.waitForTimeout(5000);

      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        for (let i = 0; i < attendeeDetails.length; i++) {
          await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
            attendeeDetails[i].email,
            eventId
          );
        }
      });

      await test.step(`Verify the transaction in vivenu db table`, async () => {
        await ticketingController.verifyVivenuTransactionDetails(
          attendeeDetails[0].email,
          eventId,
          attendeePage,
          {
            total_tickets: ticketQty,
            city: "Bhopal",
            company: null,
            currency: "USD",
            email: attendeeDetails[0].email,
            postal: "111",
            regular_price: ticketInfo1.pricePerTicket * ticketQty,
            last_name: attendeeDetails[0].lastName,
            first_name: attendeeDetails[0].firstName,
            street: "Random Home",
            applied_coupons: couponCode,
            price:
              ticketInfo1.pricePerTicket * ticketQty -
              (await convertStringToNumber(ticketDetailsData.dicountValue)),
          }
        );
      });

      ticketInfo1.soldTickets = ticketQty;
    });

    test(`TC002 : @flow-builder @registration-with-ticket @paid-ticket @bulk-ticketing @domain-access Two users with gmail.com and notgmail.com buy ticket1 with coupon gmailCouponCode`, async () => {
      const ticketName = ticketInfo1.name;
      const ticketQty = 2;
      let attendeeDetails = [];
      couponCode = gmailCouponCode;
      const ticketDetailsData: ExpectedTicketDetailsData = {
        ticketName: ticketName,
        ticketPrice: "$200",
        dicountValue: "$20",
        totalPriceAfterDiscount: "$180",
      };

      attendeeEmail = DataUtl.getRandomAttendeeEmail("gmail.com");

      attendeeDetails.push({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });

      for (let i = 0; i < ticketQty - 1; i++) {
        attendeeDetails.push({
          firstName: DataUtl.getRandomName(),
          lastName: DataUtl.getRandomName(),
          email: DataUtl.getRandomAttendeeEmail("notgmail.com"),
        });
      }

      await test.step(`User gets a paid ticket`, async () => {
        await flowBuilderPage.fillRegistrationForm({
          userFirstName: userFirstName,
          userLastName: userLastName,
          expectedFormHeading: "Form",
          userEmail: attendeeEmail,
        });

        await flowBuilderPage.fillRegistrationForm({
          ticketInfo: {
            ticketName: ticketName,
            quantity: ticketQty,
            price: ticketInfo1.pricePerTicket,
            coupon: couponCode,
            expectSoldOut: false,
          },
          expectedFormHeading: "Ticket",
        });

        await flowBuilderPage.page.waitForTimeout(5000);

        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: attendeeDetails,
          ticketInfo: {
            ticketName: ticketName,
            quantity: ticketQty,
            price: ticketInfo1.pricePerTicket,
            coupon: couponCode,
            expectSoldOut: false,
          },
          expectErrorPostContinue: true,
        });
        await expect(flowBuilderPage.errorMessage).toBeVisible();
      });
    });
    test(`TC003 : @flow-builder @registration-with-ticket @paid-ticket @bulk-ticketing @domain-access Two users with notgmail.com and gmail.com buy ticket1 with coupon gmailCouponCode`, async () => {
      const ticketName = ticketInfo1.name;
      const ticketQty = 2;
      let attendeeDetails = [];
      couponCode = gmailCouponCode;
      const ticketDetailsData: ExpectedTicketDetailsData = {
        ticketName: ticketName,
        ticketPrice: "$200",
        dicountValue: "$20",
        totalPriceAfterDiscount: "$180",
      };

      attendeeEmail = DataUtl.getRandomAttendeeEmail("notgmail.com");

      attendeeDetails.push({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });

      for (let i = 0; i < ticketQty - 1; i++) {
        attendeeDetails.push({
          firstName: DataUtl.getRandomName(),
          lastName: DataUtl.getRandomName(),
          email: DataUtl.getRandomAttendeeEmail("notgmail.com"),
        });
      }

      await test.step(`User gets a paid ticket`, async () => {
        await flowBuilderPage.fillRegistrationForm({
          userFirstName: userFirstName,
          userLastName: userLastName,
          expectedFormHeading: "Form",
          userEmail: attendeeEmail,
        });

        await flowBuilderPage.fillRegistrationForm({
          ticketInfo: {
            ticketName: ticketName,
            quantity: ticketQty,
            price: ticketInfo1.pricePerTicket,
            coupon: couponCode,
            expectSoldOut: false,
          },
          expectedFormHeading: "Ticket",
          expectErrorPreContinue: true,
        });

        await flowBuilderPage.applyCouponCode(couponCode);
        await flowBuilderPage.verifyCouponInvalidity();
      });
    });
    test(`TC004 : @flow-builder @registration-with-ticket @paid-ticket @bulk-ticketing @invite-list Two users with "invite-audience1" and "invite-audience2" buy ticket1 with coupon invitelist1CouponCode`, async () => {
      const ticketName = ticketInfo2.name;
      const ticketQty = 2;
      let attendeeDetails = [];
      couponCode = invitelist1CouponCode;
      const ticketDetailsData: ExpectedTicketDetailsData = {
        ticketName: ticketName,
        ticketPrice: "$50",
        dicountValue: "$20",
        totalPriceAfterDiscount: "$80",
      };

      attendeeEmail = "invite-audience1@justjoyn.com";

      attendeeDetails.push({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });

      for (let i = 0; i < ticketQty - 1; i++) {
        attendeeDetails.push({
          firstName: DataUtl.getRandomName(),
          lastName: DataUtl.getRandomName(),
          email: "invite-audience2@justjoyn.com",
        });
      }

      await test.step(`User gets a paid ticket`, async () => {
        await flowBuilderPage.fillRegistrationForm({
          userFirstName: userFirstName,
          userLastName: userLastName,
          expectedFormHeading: "Form",
          userEmail: attendeeEmail,
        });

        await flowBuilderPage.fillRegistrationForm({
          ticketInfo: {
            ticketName: ticketName,
            quantity: ticketQty,
            price: ticketInfo2.pricePerTicket,
            coupon: couponCode,
            expectSoldOut: false,
          },
          expectedFormHeading: "Ticket",
        });
        await flowBuilderPage.page.waitForTimeout(5000);
        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: attendeeDetails,
          ticketInfo: {
            ticketName: ticketName,
            quantity: ticketQty,
            price: ticketInfo1.pricePerTicket,
            coupon: couponCode,
            expectSoldOut: false,
          },
        });
        await flowBuilderPage.page.waitForTimeout(5000);

        await flowBuilderPage.verifyOrderSummaryBulk({
          dataToVerify: {
            attendeeDetails: attendeeDetails,
            ticketName: ticketName,
          },
        });
        await flowBuilderPage.page.waitForTimeout(5000);
        await flowBuilderPage.verifyPaidTicketDetails(true, {
          ticketName: ticketName,
          ticketPrice: (ticketQty * ticketInfo2.pricePerTicket).toString(),
          dicountValue: "20",
          totalPriceAfterDiscount: (
            ticketQty * ticketInfo1.pricePerTicket
          ).toString(),
        });

        await flowBuilderPage.fillPaymentDetailsPaidTicket({});
        await flowBuilderPage.page.waitForTimeout(5000);
        await flowBuilderPage.verifyBulkTicketingSuccess({
          attendeeDetails: attendeeDetails,
          ticketDetails: {
            ticketName: ticketName,
          },
        });
      });

      await flowBuilderPage.page.waitForTimeout(5000);

      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        for (let i = 0; i < attendeeDetails.length; i++) {
          await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
            attendeeDetails[i].email,
            eventId
          );
        }
      });

      await test.step(`Verify the transaction in vivenu db table`, async () => {
        await ticketingController.verifyVivenuTransactionDetails(
          attendeeDetails[0].email,
          eventId,
          attendeePage,
          {
            total_tickets: ticketQty,
            city: "Bhopal",
            company: null,
            currency: "USD",
            email: attendeeDetails[0].email,
            postal: "111",
            regular_price: ticketInfo2.pricePerTicket * ticketQty,
            last_name: attendeeDetails[0].lastName,
            first_name: attendeeDetails[0].firstName,
            street: "Random Home",
            applied_coupons: couponCode,
            price:
              ticketInfo2.pricePerTicket * ticketQty -
              (await convertStringToNumber(ticketDetailsData.dicountValue)),
          }
        );
      });
    });
    test(`TC005 : @flow-builder @registration-with-ticket @paid-ticket @bulk-ticketing @invite-list Two users with invite-audience3 and not-invited buy ticket1 with coupon invitelist1CouponCode`, async () => {
      const ticketName = ticketInfo2.name;
      const ticketQty = 2;
      let attendeeDetails = [];
      couponCode = gmailCouponCode;
      const ticketDetailsData: ExpectedTicketDetailsData = {
        ticketName: ticketName,
        ticketPrice: "$50",
        dicountValue: "$20",
        totalPriceAfterDiscount: "$80",
      };

      attendeeEmail = "invite-audience3@justjoyn.com";

      attendeeDetails.push({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });

      for (let i = 0; i < ticketQty - 1; i++) {
        attendeeDetails.push({
          firstName: DataUtl.getRandomName(),
          lastName: DataUtl.getRandomName(),
          email: "not-invite-audience3@justjoyn.com",
        });
      }

      await test.step(`User gets a paid ticket`, async () => {
        await flowBuilderPage.fillRegistrationForm({
          userFirstName: userFirstName,
          userLastName: userLastName,
          expectedFormHeading: "Form",
          userEmail: attendeeEmail,
        });

        await flowBuilderPage.fillRegistrationForm({
          ticketInfo: {
            ticketName: ticketName,
            quantity: ticketQty,
            price: ticketInfo2.pricePerTicket,
            coupon: couponCode,
            expectSoldOut: false,
          },
          expectedFormHeading: "Ticket",
        });

        await flowBuilderPage.page.waitForTimeout(5000);

        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: attendeeDetails,
          ticketInfo: {
            ticketName: ticketName,
            quantity: ticketQty,
            price: ticketInfo2.pricePerTicket,
            coupon: couponCode,
            expectSoldOut: false,
          },
          expectErrorPostContinue: true,
        });
        await flowBuilderPage.applyCouponCode(couponCode);
        await expect(flowBuilderPage.errorMessage).toBeVisible();
      });
    });
    test(`TC006 : @flow-builder @registration-with-ticket @paid-ticket @bulk-ticketing @invite-list Two users with not-invited and invite-audience4 buy ticket1 with coupon invitelist1CouponCode`, async () => {
      const ticketName = ticketInfo2.name;
      const ticketQty = 2;
      let attendeeDetails = [];
      couponCode = invitelist1CouponCode;
      const ticketDetailsData: ExpectedTicketDetailsData = {
        ticketName: ticketName,
        ticketPrice: "$50",
        dicountValue: "$20",
        totalPriceAfterDiscount: "$80",
      };

      attendeeEmail = "not-invite-audience3@justjoyn.com";

      attendeeDetails.push({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });

      for (let i = 0; i < ticketQty - 1; i++) {
        attendeeDetails.push({
          firstName: DataUtl.getRandomName(),
          lastName: DataUtl.getRandomName(),
          email: "invite-audience4@justjoyn.com",
        });
      }

      await test.step(`User gets a paid ticket`, async () => {
        await flowBuilderPage.fillRegistrationForm({
          userFirstName: userFirstName,
          userLastName: userLastName,
          expectedFormHeading: "Form",
          userEmail: attendeeEmail,
        });

        await flowBuilderPage.fillRegistrationForm({
          ticketInfo: {
            ticketName: ticketName,
            quantity: ticketQty,
            price: ticketInfo2.pricePerTicket,
            coupon: couponCode,
            expectSoldOut: false,
          },
          expectedFormHeading: "Ticket",
          expectErrorPreContinue: true,
        });

        await flowBuilderPage.applyCouponCode(couponCode);
        await flowBuilderPage.verifyCouponInvalidity();
      });
    });
  }
);
