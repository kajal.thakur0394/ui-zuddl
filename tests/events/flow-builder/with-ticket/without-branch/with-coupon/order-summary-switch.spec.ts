import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../../../../controller/EventController";
import { FlowBuilderController } from "../../../../../../controller/FlowBuilderController";
import { TicketingController } from "../../../../../../controller/TicketingController";
import { EventInfoDTO } from "../../../../../../dto/eventInfoDto";
import { UserInfoDTO } from "../../../../../../dto/userInfoDto";
import EventSettingID from "../../../../../../enums/EventSettingEnum";
import { FlowStatus, FlowType } from "../../../../../../enums/FlowTypeEnum";
import EventRole from "../../../../../../enums/eventRoleEnum";
import EventType from "../../../../../../enums/eventTypeEnum";
import {
  ExpectedTicketDetailsData,
  publishDataPayload,
} from "../../../../../../interfaces/FlowBuilerControllerInterface";
import { FlowBuilderPage } from "../../../../../../page-objects/flow-builder/flow-ticketing";
import CommunicationHelper from "../../../../../../tests/events/communication/communication-tools";
import BrowserFactory from "../../../../../../util/BrowserFactory";
import {
  createNewEvent,
  updateEventLandingPageDetails,
  updateEventType,
} from "../../../../../../util/apiUtil";
import { DataUtl } from "../../../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../../../util/email-validation-api-util";
import { convertStringToNumber } from "../../../../../../util/commonUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@flow-builder @registration-with-ticket @paid-ticket @bulk-ticketing`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId: any;
  let attendeeBrowserContext: BrowserContext;
  let eventController: EventController;
  let flowBuilderPage: FlowBuilderPage;
  let flowBuilderController: FlowBuilderController;
  let publishdataPayLoad: publishDataPayload;
  let attendeePage: Page;
  let couponCode: string;
  let attendeeEmail: string;
  let registeredEmail: string;
  const userFirstName = "Jxtin";
  const userLastName = "zuddl";
  let eventInfoDto: EventInfoDTO;
  let ticketingController: TicketingController;
  let ticketInfo1 = {
    name: "ticket1",
    numberOfTickets: 5,
    pricePerTicket: 100,
    isPaidTicket: true,
    isBulkPurchaseEnabled: true,
    minTicketsPerOrder: 2,
    maxTicketsPerOrder: 3,
    soldTickets: 0,
  };
  let ticketInfo2 = {
    name: "ticket2",
    numberOfTickets: 10,
    pricePerTicket: 50,
    isPaidTicket: true,
    isBulkPurchaseEnabled: true,
    minTicketsPerOrder: 3,
    maxTicketsPerOrder: 4,
  };
  let ticketInfo3 = {
    name: "ticket3",
    numberOfTickets: 10,
    pricePerTicket: 25,
    isPaidTicket: true,
    isAvailableForAll: false,
    isDomainRestriction: true,
    domains: "gmail.com",
    isBulkPurchaseEnabled: true,
    minTicketsPerOrder: 3,
    maxTicketsPerOrder: 4,
  };

  test.beforeAll(async () => {
    await test.step(`setup browser contexts`, async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: true,
      });
      orgApiContext = orgBrowserContext.request;
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage = await attendeeBrowserContext.newPage();
    });

    await test.step(`create an event with FLEX as resgistration type`, async () => {
      eventId = await createNewEvent({
        api_request_context: orgApiContext,
        event_title: "flow-with-bulk-ticket automation",
        isFlex: true,
      });
      console.log(`event id ${eventId}`);
    });

    await test.step(`update an event with IN-PERSON as event type`, async () => {
      await updateEventType(orgApiContext, eventId, EventType.IN_PERSON);
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        isMagicLinkEnabled: true,
      });
    });

    await test.step(`Initialise event controller`, async () => {
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step(`Get event details`, async () => {
      let eventInfo = await (await eventController.getEventInfo()).json();
      console.log("Event Info: ", eventInfo);
      eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
      eventInfoDto.startDateTime = eventInfo.startDateTime;
    });

    await enableDisableEmailTrigger(
      orgApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );

    ticketingController = new TicketingController(orgApiContext, eventId);

    await ticketingController.selectGateway();

    await ticketingController.createTicket(ticketInfo1);

    await ticketingController.createTicket(ticketInfo2);

    await ticketingController.createTicket(ticketInfo3);

    await test.step(`Create coupon for paid tickets`, async () => {
      const ticketTypeId1 = await ticketingController.getTicketTypeId(
        ticketInfo1.name
      );

      couponCode = DataUtl.getRandomCouponName();
      await ticketingController.addCoupons({
        couponCode: couponCode,
        active: true,
        singleUsage: false,
        discountValue: 20,
        isAvailableForAllTickets: true,
        isAvailableForEveryone: true,
      });
    });

    await test.step(`Initialise flowbuilder controller`, async () => {
      flowBuilderController = new FlowBuilderController(orgApiContext, eventId);
    });
    await test.step(`update default flow steps with existing expected steps`, async () => {
      await flowBuilderController.generateSimpleTicketingFlow(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
    });

    await test.step(`make publish flow test data`, async () => {
      let flowDraftId = await flowBuilderController.getFlowId(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
      publishdataPayLoad = {
        flowId: "",
        flowDraftId: flowDraftId,
        clientFlowDraftVersion: 2,
      };
    });

    await test.step(`now, publish the flow`, async () => {
      await flowBuilderController.publishFlowData(publishdataPayLoad);
      await attendeePage.waitForTimeout(5000);
    });

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
  });

  test.beforeEach(async () => {
    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
  });

  test.afterEach(async () => {
    await attendeePage.reload();
    // clear the cookies
    await attendeePage.context().clearCookies();
    await attendeePage.evaluate(() => {
      localStorage.clear();
    });
  });

  test.afterAll(async () => {
    await orgBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test(`TC001 : @flow-builder @registration-with-ticket @paid-ticket @bulk-ticketing Verify that the user should be able to change ticket and register successfully in order summary page`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-790/verify-that-the-user-should-be-able-to-change-ticket-and-register",
    });

    const ticketName = ticketInfo1.name;
    const ticketQty = 3;
    let attendeeDetails = [];
    let ticketDetailsData: ExpectedTicketDetailsData = {
      ticketName: ticketName,
      ticketPrice: "$300",
      dicountValue: "$20",
      totalPriceAfterDiscount: "$280",
    };

    attendeeDetails.push({
      firstName: userFirstName,
      lastName: userLastName,
      email: attendeeEmail,
    });

    for (let i = 0; i < ticketQty - 1; i++) {
      attendeeDetails.push({
        firstName: DataUtl.getRandomName(),
        lastName: DataUtl.getRandomName(),
        email: DataUtl.getRandomAttendeeEmail(),
      });
    }

    await test.step(`User gets a paid ticket`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form",
        userEmail: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName,
          quantity: ticketQty,
          price: ticketInfo1.pricePerTicket,
          coupon: couponCode,
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.page.waitForTimeout(5000);

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Ticket details",
        attendeeDetails: attendeeDetails,
        ticketInfo: {
          ticketName: ticketName,
          quantity: ticketQty,
          price: ticketInfo1.pricePerTicket,
          coupon: couponCode,
          expectSoldOut: false,
        },
      });
      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.verifyPaidTicketDetails(true, ticketDetailsData);

      await flowBuilderPage.editDetailsSummary.click();
      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.chooseTicketQuantity({
        ticketName: ticketInfo2.name,
        quantity: 3,
        price: ticketInfo2.pricePerTicket,
        expectSoldOut: false,
      });
    });
    ticketDetailsData = {
      ticketName: ticketInfo2.name,
      ticketPrice: "$150",
      dicountValue: "$20",
      totalPriceAfterDiscount: "$130",
    };
    await flowBuilderPage.verifyTicketChangeConfirmation();
    await flowBuilderPage.clickOnContinueButton();
    await flowBuilderPage.clickOnContinueButton();
    await flowBuilderPage.clickOnContinueButton();

    await flowBuilderPage.verifyOrderSummaryBulk({
      dataToVerify: {
        attendeeDetails: attendeeDetails,
        ticketName: ticketInfo2.name,
      },
    });
    await flowBuilderPage.page.waitForTimeout(5000);
    await flowBuilderPage.verifyPaidTicketDetails(true, {
      ticketName: ticketInfo2.name,
      ticketPrice: (ticketQty * ticketInfo2.pricePerTicket).toString(),
      dicountValue: "20",
      totalPriceAfterDiscount: (
        ticketQty * ticketInfo2.pricePerTicket
      ).toString(),
    });

    await flowBuilderPage.fillPaymentDetailsPaidTicket({});
    await flowBuilderPage.page.waitForTimeout(5000);
    await flowBuilderPage.verifyBulkTicketingSuccess({
      attendeeDetails: attendeeDetails,
      ticketDetails: {
        ticketName: ticketInfo2.name,
      },
    });
    await flowBuilderPage.page.waitForTimeout(5000);
    await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
      for (let i = 0; i < attendeeDetails.length; i++) {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeDetails[i].email,
          eventId
        );
      }
    });

    await test.step(`Verify email of One random attendee`, async () => {
      let randomAttendee =
        attendeeDetails[Math.floor(Math.random() * attendeeDetails.length)];
      let userInfoDto = new UserInfoDTO(
        randomAttendee.email,
        "",
        randomAttendee.firstName,
        randomAttendee.lastName
      );

      let communicationHelper = new CommunicationHelper(
        orgApiContext,
        eventId,
        userInfoDto,
        eventInfoDto,
        true,
        true // for in-person event
      );
      let ticketCode: string;
      await test.step(`Now fetching the user entry from event_registration table and get the ticket`, async () => {
        const userRegDataFromDb =
          await QueryUtil.fetchUserEventRegistrationDataFromDb(
            eventId,
            randomAttendee.email
          );
        ticketCode = userRegDataFromDb["qr_code"];
      });

      await test.step("Fetching Email body from email recieved after registering", async () => {
        await communicationHelper.verifyConfirmationEmailContentsTicketing(
          true,
          "(IST)",
          ticketCode
        );
      });
    });
    await test.step(`Verify the transaction in vivenu db table`, async () => {
      await ticketingController.verifyVivenuTransactionDetails(
        attendeeDetails[0].email,
        eventId,
        attendeePage,
        {
          total_tickets: ticketQty,
          city: "Bhopal",
          company: null,
          currency: "USD",
          email: attendeeDetails[0].email,
          postal: "111",
          regular_price: ticketInfo2.pricePerTicket * ticketQty,
          last_name: attendeeDetails[0].lastName,
          first_name: attendeeDetails[0].firstName,
          street: "Random Home",
          applied_coupons: couponCode,
          price:
            ticketInfo2.pricePerTicket * ticketQty -
            (await convertStringToNumber(ticketDetailsData.dicountValue)),
        }
      );
    });
  });
});
