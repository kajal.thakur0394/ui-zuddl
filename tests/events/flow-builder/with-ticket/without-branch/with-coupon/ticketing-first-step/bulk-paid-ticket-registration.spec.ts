import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../../../../../controller/EventController";
import { FlowBuilderController } from "../../../../../../../controller/FlowBuilderController";
import { TicketingController } from "../../../../../../../controller/TicketingController";
import { EventInfoDTO } from "../../../../../../../dto/eventInfoDto";
import { UserInfoDTO } from "../../../../../../../dto/userInfoDto";
import EventSettingID from "../../../../../../../enums/EventSettingEnum";
import {
  DiscountType,
  FlowStatus,
  FlowType,
} from "../../../../../../../enums/FlowTypeEnum";
import EventRole from "../../../../../../../enums/eventRoleEnum";
import EventType from "../../../../../../../enums/eventTypeEnum";
import {
  ExpectedTicketDetailsData,
  publishDataPayload,
} from "../../../../../../../interfaces/FlowBuilerControllerInterface";
import { FlowBuilderPage } from "../../../../../../../page-objects/flow-builder/flow-ticketing";
import CommunicationHelper from "../../../../../../../tests/events/communication/communication-tools";
import BrowserFactory from "../../../../../../../util/BrowserFactory";
import {
  createNewEvent,
  updateEventLandingPageDetails,
  updateEventType,
} from "../../../../../../../util/apiUtil";
import { DataUtl } from "../../../../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../../../../util/email-validation-api-util";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@flow-builder @registraion-with-ticket @paid-ticket @bulk-ticketing`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId: any;
  let attendeeBrowserContext: BrowserContext;
  let eventController: EventController;
  let flowBuilderPage: FlowBuilderPage;
  let flowBuilderController: FlowBuilderController;
  let publishdataPayLoad: publishDataPayload;
  let attendeePage: Page;
  let attendeeEmail: string;
  const userFirstName = "Jxtin";
  const userLastName = "zuddl";
  let eventInfoDto: EventInfoDTO;
  let ticketingController: TicketingController;
  let couponCode: string;
  let lockCoupon: string;
  let hideCoupon: string;
  let couponCode2: string;

  let ticketInfo1 = {
    name: "ticket1",
    numberOfTickets: 5,
    pricePerTicket: 100,
    isPaidTicket: true,
    isBulkPurchaseEnabled: true,
    minTicketsPerOrder: 2,
    maxTicketsPerOrder: 3,
    soldTickets: 0,
  };
  let ticketInfo2 = {
    name: "ticket2",
    numberOfTickets: 10,
    pricePerTicket: 50,
    isPaidTicket: true,
    isBulkPurchaseEnabled: true,
    minTicketsPerOrder: 3,
    maxTicketsPerOrder: 4,
  };
  let ticketInfo3 = {
    name: "ticket3",
    numberOfTickets: 10,
    pricePerTicket: 25,
    isPaidTicket: true,
    isAvailableForAll: false,
    isDomainRestriction: true,
    domains: "gmail.com",
    isBulkPurchaseEnabled: true,
    minTicketsPerOrder: 3,
    maxTicketsPerOrder: 4,
  };
  let ticketInfo4 = {
    name: "ticket4",
    numberOfTickets: 7,
    pricePerTicket: 100,
    isPaidTicket: true,
    isBulkPurchaseEnabled: true,
    minTicketsPerOrder: 3,
    maxTicketsPerOrder: 4,
  };

  let lockedTicketInfo = {
    name: "LockTicket",
    numberOfTickets: 2,
    pricePerTicket: 100,
    isPaidTicket: true,
    isLocked: true,
    isBulkPurchaseEnabled: true,
    minTicketsPerOrder: 3,
    maxTicketsPerOrder: 4,
  };

  let hiddenTicketInfo = {
    name: "HiddenTicket",
    numberOfTickets: 4,
    pricePerTicket: 100,
    isPaidTicket: true,
    isHide: true,
    isBulkPurchaseEnabled: true,
    minTicketsPerOrder: 3,
    maxTicketsPerOrder: 4,
  };

  test.beforeAll(async () => {
    await test.step(`setup browser contexts`, async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: true,
      });
      orgApiContext = orgBrowserContext.request;
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage = await attendeeBrowserContext.newPage();
    });

    await test.step(`create an event with FLEX as resgistration type`, async () => {
      eventId = await createNewEvent({
        api_request_context: orgApiContext,
        event_title: "flow-with-bulk-ticket automation",
        isFlex: true,
      });
      console.log(`event id ${eventId}`);
    });

    await test.step(`update an event with IN-PERSON as event type`, async () => {
      await updateEventType(orgApiContext, eventId, EventType.IN_PERSON);
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        isMagicLinkEnabled: true,
      });
    });

    await test.step(`Initialise event controller`, async () => {
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step(`Get event details`, async () => {
      let eventInfo = await (await eventController.getEventInfo()).json();
      console.log("Event Info: ", eventInfo);
      eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
      eventInfoDto.startDateTime = eventInfo.startDateTime;
    });

    await enableDisableEmailTrigger(
      orgApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );

    ticketingController = new TicketingController(orgApiContext, eventId);

    await ticketingController.selectGateway();

    await ticketingController.createTicket(ticketInfo1);

    await ticketingController.createTicket(ticketInfo2);

    await ticketingController.createTicket(ticketInfo3);

    await ticketingController.createTicket(ticketInfo4);

    await ticketingController.createTicket(lockedTicketInfo);

    await ticketingController.createTicket(hiddenTicketInfo);

    await test.step(`Create coupon for paid tickets`, async () => {
      const ticketTypeId1 = await ticketingController.getTicketTypeId(
        "ticket1"
      );
      const ticketTypeId3 = await ticketingController.getTicketTypeId(
        "ticket4"
      );
      const ticketTypeId2 = await ticketingController.getTicketTypeId(
        "LockTicket"
      );
      const ticketTypeId4 = await ticketingController.getTicketTypeId(
        "HiddenTicket"
      );
      couponCode = DataUtl.getRandomCouponName();
      couponCode2 = DataUtl.getRandomCouponName();
      lockCoupon = DataUtl.getRandomCouponName();
      hideCoupon = DataUtl.getRandomCouponName();

      await ticketingController.addCoupons({
        couponCode: couponCode,
        active: true,
        singleUsage: false,
        ticketTypeIds: [ticketTypeId1],
        isAvailableForAllTickets: true,
        isAvailableForEveryone: true,
      });

      await ticketingController.addCoupons({
        couponCode: couponCode2,
        active: true,
        singleUsage: false,
        discountType: DiscountType.PERCENTAGE,
        ticketTypeIds: [ticketTypeId3],
        isAvailableForAllTickets: false,
        isAvailableForEveryone: true,
        maxUsageCount: 1,
      });

      await ticketingController.addCoupons({
        couponCode: hideCoupon,
        active: true,
        singleUsage: true,
        ticketTypeIds: [ticketTypeId4],
        isAvailableForAllTickets: false,
        isAvailableForEveryone: true,
        maxUsageCount: 1,
        maxTickets: 5,
      });
      await ticketingController.addCoupons({
        couponCode: lockCoupon,
        active: true,
        singleUsage: false,
        ticketTypeIds: [ticketTypeId2],
        isAvailableForAllTickets: false,
        isAvailableForEveryone: true,
        maxUsageCount: 1,
      });
    });

    await test.step(`Initialise flowbuilder controller`, async () => {
      flowBuilderController = new FlowBuilderController(orgApiContext, eventId);
    });
    await test.step(`update default flow steps with existing expected steps`, async () => {
      await flowBuilderController.generateTicketingFirstStep(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
    });

    await test.step(`make publish flow test data`, async () => {
      let flowDraftId = await flowBuilderController.getFlowId(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
      publishdataPayLoad = {
        flowId: "",
        flowDraftId: flowDraftId,
        clientFlowDraftVersion: 2,
      };
    });

    await test.step(`now, publish the flow`, async () => {
      await flowBuilderController.publishFlowData(publishdataPayLoad);
      await attendeePage.waitForTimeout(10000);
    });

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
  });

  test.beforeEach(async () => {
    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
  });

  test.afterEach(async () => {
    await attendeePage.reload();
    // clear the cookies
    await attendeePage.context().clearCookies();
    await attendeePage.evaluate(() => {
      localStorage.clear();
    });
  });

  test.afterAll(async () => {
    await orgBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test(`TC001 : @flow-builder @registraion-with-ticket @paid-ticket @bulk-ticketing Verify max ticket count`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-789/",
    });

    const ticketName = ticketInfo1.name;
    let ticketInfo = await ticketingController.getTicketInfo(ticketName);
    const totalTickets = ticketInfo["numberOfTickets"];
    const soldTickets = ticketInfo["numberOfSoldTickets"];
    expect(totalTickets).toEqual(ticketInfo1.numberOfTickets);
    expect(soldTickets).toEqual(0);

    await test.step(`User fills Basic details`, async () => {
      let availableRange = await flowBuilderPage.getAvailableTicketCountRange({
        ticketName: ticketName,
        expectSoldOut: false,
        price: ticketInfo1.pricePerTicket,
      });
      console.log(`availableRange ${availableRange}`);
      let minTicketsAvailable = availableRange[0];
      let maxTicketsAvailable = availableRange[1];
      console.log(`minTicketsAvailable ${minTicketsAvailable}`);
      console.log(`maxTicketsAvailable ${maxTicketsAvailable}`);
      expect(maxTicketsAvailable).toEqual(ticketInfo1.maxTicketsPerOrder);
      expect(minTicketsAvailable).toEqual(ticketInfo1.minTicketsPerOrder);
    });
  });

  test(`TC002 : @flow-builder @registraion-with-ticket @paid-ticket @bulk-ticketing Verify user can register with bulk ticketing with coupon`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-742",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-821/",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-786/",
    });

    const ticketName = ticketInfo1.name;
    const ticketQty = 3;
    let attendeeDetails = [];

    attendeeDetails.push({
      firstName: userFirstName,
      lastName: userLastName,
      email: attendeeEmail,
    });

    for (let i = 0; i < ticketQty - 1; i++) {
      attendeeDetails.push({
        firstName: DataUtl.getRandomName(),
        lastName: DataUtl.getRandomName(),
        email: DataUtl.getRandomAttendeeEmail(),
      });
    }

    await test.step(`User gets a paid ticket`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName,
          quantity: ticketQty,
          price: ticketInfo1.pricePerTicket,
          coupon: couponCode,
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.page.waitForTimeout(5000);

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Ticket details",
        attendeeDetails: attendeeDetails,
        ticketInfo: {
          ticketName: ticketName,
          quantity: ticketQty,
          price: ticketInfo1.pricePerTicket,
          coupon: "",
          expectSoldOut: false,
        },
        ticketAsFirstStep: true,
      });
      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.verifyOrderSummaryBulk({
        dataToVerify: {
          attendeeDetails: attendeeDetails,
          ticketName: ticketName,
        },
      });
      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.verifyPaidTicketDetails(true, {
        ticketName: ticketName,
        ticketPrice: (ticketQty * ticketInfo1.pricePerTicket).toString(),
        dicountValue: "10",
        totalPriceAfterDiscount: (
          ticketQty * ticketInfo1.pricePerTicket -
          10
        ).toString(),
      });

      await flowBuilderPage.fillPaymentDetailsPaidTicket({});
      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.verifyBulkTicketingSuccess({
        attendeeDetails: attendeeDetails,
        ticketDetails: {
          ticketName: ticketName,
        },
      });
    });

    await flowBuilderPage.waitForPaymentConfirmation();

    await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
      for (let i = 0; i < attendeeDetails.length; i++) {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeDetails[i].email,
          eventId
        );
      }
    });

    await test.step(`Verify email of One random attendee`, async () => {
      let randomAttendee =
        attendeeDetails[Math.floor(Math.random() * attendeeDetails.length)];
      let userInfoDto = new UserInfoDTO(
        randomAttendee.email,
        "",
        randomAttendee.firstName,
        randomAttendee.lastName
      );

      let communicationHelper = new CommunicationHelper(
        orgApiContext,
        eventId,
        userInfoDto,
        eventInfoDto,
        true,
        true // for in-person event
      );
      let ticketCode: string;
      await test.step(`Now fetching the user entry from event_registration table and get the ticket`, async () => {
        const userRegDataFromDb =
          await QueryUtil.fetchUserEventRegistrationDataFromDb(
            eventId,
            randomAttendee.email
          );
        ticketCode = userRegDataFromDb["qr_code"];
      });

      let discountedPrice = `$${(
        (ticketInfo1.pricePerTicket * ticketQty - 10) /
        ticketQty
      ).toFixed(2)}`;
      if (discountedPrice.endsWith("0")) {
        discountedPrice = discountedPrice.slice(0, -1);
      }

      await test.step("Fetching Email body from email recieved after registering", async () => {
        await communicationHelper.verifyConfirmationEmailContentsTicketing(
          true,
          "(IST)",
          ticketCode,
          true,
          discountedPrice
        );
      });
    });
    await test.step(`Verify the transaction in vivenu db table`, async () => {
      await ticketingController.verifyVivenuTransactionDetails(
        attendeeDetails[0].email,
        eventId,
        attendeePage,
        {
          total_tickets: ticketQty,
          city: "Bhopal",
          company: null,
          currency: "USD",
          email: attendeeDetails[0].email,
          postal: "111",
          regular_price: ticketInfo1.pricePerTicket * ticketQty,
          last_name: attendeeDetails[0].lastName,
          first_name: attendeeDetails[0].firstName,
          street: "Random Home",
          price: ticketInfo1.pricePerTicket * ticketQty - 10,
          applied_coupons: couponCode,
        }
      );
    });

    ticketInfo1.soldTickets = ticketQty;
  });

  test(`TC003 : @flow-builder @registraion-with-ticket @paid-ticket @bulk-ticketing Verify ticket count is reduced`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-822/",
    });

    const ticketName = ticketInfo1.name;
    let ticketInfo = await ticketingController.getTicketInfo(ticketName);
    const totalTickets = ticketInfo["numberOfTickets"];
    const soldTickets = ticketInfo["numberOfSoldTickets"];
    let minTicketsAvailable: number;
    let maxTicketsAvailable: number;

    await test.step(`Verify Remaining ticket count`, async () => {
      let availableRange = await flowBuilderPage.getAvailableTicketCountRange({
        ticketName: ticketName,
        expectSoldOut: false,
        price: ticketInfo1.pricePerTicket,
      });
      minTicketsAvailable = availableRange[0];
      maxTicketsAvailable = availableRange[1];
      console.log(`minTicketsAvailable ${minTicketsAvailable}`);
      console.log(`maxTicketsAvailable ${maxTicketsAvailable}`);
      console.log(`soldTickets ${soldTickets}`);
      console.log(`totalTickets ${totalTickets}`);
      expect(minTicketsAvailable).toEqual(ticketInfo1.minTicketsPerOrder);
      if (totalTickets - soldTickets >= ticketInfo1.maxTicketsPerOrder) {
        expect(maxTicketsAvailable).toEqual(ticketInfo1.maxTicketsPerOrder);
      } else {
        expect(maxTicketsAvailable).toEqual(totalTickets - soldTickets);
      }
    });
  });

  test(`TC004: @with-ticket-and-branch Verify coupon code percentage correctly when user register`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-820",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-850/",
    });
    const ticketName = ticketInfo4.name;
    const coupon = couponCode2;
    const ticketQty = 3;
    const ticketDetailsData: ExpectedTicketDetailsData = {
      ticketName: ticketName,
      ticketPrice: "$300",
      dicountValue: "$30",
      totalPriceAfterDiscount: "$90",
    };
    let attendeeDetails = [];

    attendeeDetails.push({
      firstName: userFirstName,
      lastName: userLastName,
      email: attendeeEmail,
    });
    for (let i = 1; i < ticketQty; i++) {
      attendeeDetails.push({
        firstName: DataUtl.getRandomName(),
        lastName: DataUtl.getRandomName(),
        email: DataUtl.getRandomAttendeeEmail(),
      });
    }

    await test.step(`User gets a Paid ticket`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName,
          quantity: ticketQty,
          price: ticketInfo4.pricePerTicket,
          coupon: coupon,
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });
      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Ticket details",
        attendeeDetails: attendeeDetails,
        ticketInfo: {
          ticketName: ticketName,
          quantity: ticketQty,
          price: 100,
          coupon: coupon,
          expectSoldOut: false,
        },
        ticketAsFirstStep: true,
      });
      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.verifyPaidTicketDetails(true, ticketDetailsData);
      await flowBuilderPage.verifyOrderSummary({
        dataToVerify: {
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
          ticketName: ticketName,
        },
      });
      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.fillPaymentDetailsPaidTicket({});
      await flowBuilderPage.page.waitForTimeout(10000);
      await flowBuilderPage.verifySingleTicketingSuccess({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
        ticketDetails: {
          ticketName: ticketName,
        },
      });
      await flowBuilderPage.waitForPaymentConfirmation();

      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeEmail,
          eventId
        );
      });
    });

    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      userFirstName,
      userLastName
    );

    let communicationHelper = new CommunicationHelper(
      orgApiContext,
      eventId,
      userInfoDto,
      eventInfoDto,
      true,
      true // for in-person event
    );
    let ticketCode: string;
    await test.step(`Now fetching the user entry from event_registration table and get the ticket`, async () => {
      const userRegDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          eventId,
          attendeeEmail
        );
      ticketCode = userRegDataFromDb["qr_code"];
    });

    await test.step("Fetching Email body from email recieved after registering", async () => {
      await communicationHelper.verifyConfirmationEmailContentsTicketing(
        true,
        "(IST)",
        ticketCode
      );
    });
  });

  test(`TC005 : @flow-builder @registraion-with-ticket @paid-ticket @bulk-ticketing: Verify email access domain.`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-799/",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-817/",
    });

    const ticketName1 = ticketInfo1.name;
    const ticketName2 = ticketInfo2.name;
    const ticketName3 = ticketInfo3.name;

    await flowBuilderPage.page.waitForTimeout(15000);

    await test.step(`All tickets should be visible`, async () => {
      expect(
        await flowBuilderPage.isTicketDivPresent(ticketName1)
      ).toBeTruthy();
      expect(
        await flowBuilderPage.isTicketDivPresent(ticketName2)
      ).toBeTruthy();
      expect(
        await flowBuilderPage.isTicketDivPresent(ticketName3)
      ).toBeTruthy();
    });

    let attendeeDetails = [];

    await test.step(`User with allowed email checks tickets`, async () => {
      const ticketQty = 3;
      attendeeEmail = DataUtl.getRandomAttendeeEmail("gmail.com");
      attendeeDetails.push({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });

      for (let i = 1; i < ticketQty; i++) {
        attendeeDetails.push({
          firstName: DataUtl.getRandomName(),
          lastName: DataUtl.getRandomName(),
          email: DataUtl.getRandomAttendeeEmail("gmail.com"),
        });
      }

      expect(
        await flowBuilderPage.isTicketDivPresent(ticketName3)
      ).toBeTruthy();
      expect(
        await flowBuilderPage.isTicketDivPresent(ticketName1)
      ).toBeTruthy();
      expect(
        await flowBuilderPage.isTicketDivPresent(ticketName2)
      ).toBeTruthy();

      const purchaseTicketInfo = {
        ticketName: ticketName3,
        quantity: ticketQty,
        price: ticketInfo3.pricePerTicket,
        coupon: "",
        expectSoldOut: false,
      };
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: purchaseTicketInfo,
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Ticket details",
        attendeeDetails: attendeeDetails,
        ticketInfo: {
          ticketName: ticketName3,
          quantity: ticketQty,
          price: ticketInfo1.pricePerTicket,
          coupon: "",
          expectSoldOut: false,
        },
        ticketAsFirstStep: true,
      });
      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.verifyOrderSummaryBulk({
        dataToVerify: {
          attendeeDetails: attendeeDetails,
          ticketName: ticketName3,
        },
      });
      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.verifyPaidTicketDetails(false, {
        ticketName: ticketName3,
        ticketPrice: (ticketQty * ticketInfo3.pricePerTicket).toString(),
        dicountValue: "0",
        totalPriceAfterDiscount: (
          ticketQty * ticketInfo1.pricePerTicket
        ).toString(),
      });

      await flowBuilderPage.fillPaymentDetailsPaidTicket({});
      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.verifyBulkTicketingSuccess({
        attendeeDetails: attendeeDetails,
        ticketDetails: {
          ticketName: ticketName3,
        },
      });
    });

    await flowBuilderPage.waitForPaymentConfirmation();

    await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
      for (let i = 0; i < attendeeDetails.length; i++) {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeDetails[i].email,
          eventId
        );
      }
    });
    await test.step(`Clean-up`, async () => {
      await test.step(`Clean up the cookies`, async () => {
        await attendeePage.reload();
        // clear the cookies
        await attendeePage.context().clearCookies();
        await attendeePage.evaluate(() => {
          localStorage.clear();
        });
      });

      await test.step(`Set the content to publish site`, async () => {
        await attendeePage.goto("https://example.com");
        await attendeePage.setContent(
          await flowBuilderController.generateEmbedHtml()
        );
        flowBuilderPage = new FlowBuilderPage(attendeePage);
      });
    });

    await test.step(`User with not allowed email checks tickets`, async () => {
      const ticketQty = 3;
      const ticketName3 = ticketInfo3.name;

      attendeeEmail = DataUtl.getRandomAttendeeEmail();

      const purchaseTicketInfo = {
        ticketName: ticketName3,
        quantity: ticketQty,
        price: ticketInfo3.pricePerTicket,
        coupon: "",
        expectSoldOut: false,
      };
      let attendeeDetails = [];
      attendeeDetails.push({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });

      for (let i = 1; i < ticketQty; i++) {
        attendeeDetails.push({
          firstName: DataUtl.getRandomName(),
          lastName: DataUtl.getRandomName(),
          email: DataUtl.getRandomAttendeeEmail(),
        });
      }
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: purchaseTicketInfo,
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Ticket details",
        attendeeDetails: attendeeDetails,
        ticketInfo: {
          ticketName: ticketName3,
          quantity: ticketQty,
          price: ticketInfo1.pricePerTicket,
          coupon: "",
          expectSoldOut: false,
        },
        ticketAsFirstStep: true,
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Ticket details",
        attendeeDetails: attendeeDetails,
        ticketInfo: purchaseTicketInfo,
        expectErrorPreContinue: true,
      });
    });
  });

  test(`TC006: @with-ticket-and-branch Verify coupon applied for max transaction limit`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-820",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-809/",
    });
    const coupon = couponCode2;
    const ticketQty = 2;
    let attendeeDetails = [];

    attendeeDetails.push({
      firstName: userFirstName,
      lastName: userLastName,
      email: attendeeEmail,
    });
    for (let i = 1; i < ticketQty; i++) {
      attendeeDetails.push({
        firstName: DataUtl.getRandomName(),
        lastName: DataUtl.getRandomName(),
        email: DataUtl.getRandomAttendeeEmail(),
      });
    }
    await test.step(`Apply coupon code->${{
      coupon,
    }} and verify it is not active or invalid`, async () => {
      await flowBuilderPage.applyCouponCode(coupon);
      await flowBuilderPage.verifyCouponInvalidity("expired");
    });
  });

  test(`TC007: @with-ticket-and-branch Verify Lock and Hidden functionality`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-810",
    });
    const ticketName = "LockTicket";
    const coupon = lockCoupon;
    const hiddenTicketName = "HiddenTicket";
    const ticketDetailsData: ExpectedTicketDetailsData = {
      ticketName: hiddenTicketName,
      ticketPrice: "$300",
      dicountValue: "$10",
      totalPriceAfterDiscount: "$290",
    };
    const ticketQty = 3;
    let attendeeDetails = [];

    attendeeDetails.push({
      firstName: userFirstName,
      lastName: userLastName,
      email: attendeeEmail,
    });
    for (let i = 1; i < ticketQty; i++) {
      attendeeDetails.push({
        firstName: DataUtl.getRandomName(),
        lastName: DataUtl.getRandomName(),
        email: DataUtl.getRandomAttendeeEmail(),
      });
    }

    await test.step(`User fills the reg form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName,
          quantity: ticketQty,
          price: 100,
          coupon: coupon,
          expectSoldOut: false,
        },
        expectLockTicket: true,
        expectedFormHeading: "Ticket",
      });
    });

    await test.step(`Now apply locked coupon-code and check the lock ticket status to be invisble`, async () => {
      await flowBuilderPage.verifyLockedTicketStatusInvisible(ticketName);
      await flowBuilderPage.checkTicketNotPresent(hiddenTicketName);
      await flowBuilderPage.removeCouponCode();
    });

    await test.step(`Then apply hidden coupon-code and verify hidden ticket which was previously inviisble is nos visible`, async () => {
      await flowBuilderPage.applyCouponCode(hideCoupon);
      await flowBuilderPage.checkTicketPresent(hiddenTicketName);
    });

    await test.step(`User now selects 1 hidden ticket and buy the hidden ticket`, async () => {
      await flowBuilderPage.selectQuantityFromDropdownAndVerifyCouponDetails(
        hiddenTicketName,
        ticketQty,
        true,
        true,
        ticketDetailsData
      );

      await flowBuilderPage.verifyPaidTicketDetails(true, ticketDetailsData);
      await flowBuilderPage.clickOnContinueButton();
      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Ticket details",
        attendeeDetails: attendeeDetails,
        ticketInfo: {
          ticketName: hiddenTicketName,
          quantity: ticketQty,
          price: 100,
          coupon: "",
          expectSoldOut: false,
        },
        ticketAsFirstStep: true,
      });

      await flowBuilderPage.verifyOrderSummary({
        dataToVerify: {
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
          ticketName: hiddenTicketName,
        },
      });
      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.fillPaymentDetailsPaidTicket({});
      await flowBuilderPage.page.waitForTimeout(10000);
      await flowBuilderPage.verifySingleTicketingSuccess({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
        ticketDetails: {
          ticketName: hiddenTicketName,
        },
      });
    });
  });
});
