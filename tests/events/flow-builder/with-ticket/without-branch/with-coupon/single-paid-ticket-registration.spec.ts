import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../../../../controller/EventController";
import { FlowBuilderController } from "../../../../../../controller/FlowBuilderController";
import { TicketingController } from "../../../../../../controller/TicketingController";
import { EventInfoDTO } from "../../../../../../dto/eventInfoDto";
import { UserInfoDTO } from "../../../../../../dto/userInfoDto";
import EventSettingID from "../../../../../../enums/EventSettingEnum";
import {
  DiscountType,
  FlowStatus,
  FlowType,
} from "../../../../../../enums/FlowTypeEnum";
import EventRole from "../../../../../../enums/eventRoleEnum";
import EventType from "../../../../../../enums/eventTypeEnum";
import {
  ExpectedTicketDetailsData,
  publishDataPayload,
} from "../../../../../../interfaces/FlowBuilerControllerInterface";
import { FlowBuilderPage } from "../../../../../../page-objects/flow-builder/flow-ticketing";
import CommunicationHelper from "../../../../../../tests/events/communication/communication-tools";
import BrowserFactory from "../../../../../../util/BrowserFactory";
import {
  createNewEvent,
  updateEventLandingPageDetails,
  updateEventType,
} from "../../../../../../util/apiUtil";
import { DataUtl } from "../../../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../../../util/email-validation-api-util";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@flow-builder @registraion-without-ticket`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId: any;
  let attendeeBrowserContext: BrowserContext;
  let eventController: EventController;
  let flowBuilderPage: FlowBuilderPage;
  let flowBuilderController: FlowBuilderController;
  let publishdataPayLoad: publishDataPayload;
  let attendeePage: Page;
  let attendeeEmail: string;
  let registeredEmail: string;
  const userFirstName = "Jxtin";
  const userLastName = "zuddl";
  let eventInfoDto: EventInfoDTO;
  let ticketingController: TicketingController;
  let couponCode: string;
  let lockCoupon: string;
  let hideCoupon: string;
  let couponCode2: string;

  test.beforeAll(async () => {
    await test.step(`setup browser contexts`, async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: true,
      });
      orgApiContext = orgBrowserContext.request;
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage = await attendeeBrowserContext.newPage();
    });

    await test.step(`create an event with FLEX as resgistration type`, async () => {
      eventId = await createNewEvent({
        api_request_context: orgApiContext,
        event_title: "flow-with-ticket automation",
        isFlex: true,
      });
      console.log(`event id ${eventId}`);
    });

    await test.step(`update an event with IN-PERSON as event type`, async () => {
      await updateEventType(orgApiContext, eventId, EventType.IN_PERSON);
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        isMagicLinkEnabled: true,
      });
    });

    await test.step(`Initialise event controller`, async () => {
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step(`Get event details`, async () => {
      let eventInfo = await (await eventController.getEventInfo()).json();
      console.log("Event Info: ", eventInfo);
      eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
      eventInfoDto.startDateTime = eventInfo.startDateTime;
    });

    await enableDisableEmailTrigger(
      orgApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );

    ticketingController = new TicketingController(orgApiContext, eventId);

    await ticketingController.selectGateway();

    await ticketingController.createTicket({
      name: "ticket1",
      numberOfTickets: 1,
      pricePerTicket: 100,
      isPaidTicket: true,
    });

    await ticketingController.createTicket({
      name: "ticket2",
      numberOfTickets: 2,
      pricePerTicket: 100,
      isPaidTicket: true,
    });

    await ticketingController.createTicket({
      name: "ticket3",
      numberOfTickets: 1,
      pricePerTicket: 100,
      isPaidTicket: true,
      isAvailableForAll: false,
      isDomainRestriction: true,
      domains: "gmail.com",
    });

    await ticketingController.createTicket({
      name: "ticket4",
      numberOfTickets: 2,
      pricePerTicket: 100,
      isPaidTicket: true,
    });

    await ticketingController.createTicket({
      name: "LockTicket",
      numberOfTickets: 2,
      pricePerTicket: 100,
      isPaidTicket: true,
      isLocked: true,
    });
    await ticketingController.createTicket({
      name: "HiddenTicket",
      numberOfTickets: 2,
      pricePerTicket: 100,
      isPaidTicket: true,
      isHide: true,
    });
    await test.step(`Create coupon for paid tickets`, async () => {
      const ticketTypeId1 = await ticketingController.getTicketTypeId(
        "ticket1"
      );
      const ticketTypeId3 = await ticketingController.getTicketTypeId(
        "ticket4"
      );
      const ticketTypeId2 = await ticketingController.getTicketTypeId(
        "LockTicket"
      );
      const ticketTypeId4 = await ticketingController.getTicketTypeId(
        "HiddenTicket"
      );
      couponCode = DataUtl.getRandomCouponName();
      couponCode2 = DataUtl.getRandomCouponName();
      lockCoupon = DataUtl.getRandomCouponName();
      hideCoupon = DataUtl.getRandomCouponName();

      await ticketingController.addCoupons({
        couponCode: couponCode,
        active: true,
        singleUsage: false,
        ticketTypeIds: [ticketTypeId1],
        isAvailableForAllTickets: true,
        isAvailableForEveryone: true,
      });

      await ticketingController.addCoupons({
        couponCode: couponCode2,
        active: true,
        singleUsage: false,
        discountType: DiscountType.PERCENTAGE,
        ticketTypeIds: [ticketTypeId3],
        isAvailableForAllTickets: false,
        isAvailableForEveryone: true,
        maxUsageCount: 1,
      });

      await ticketingController.addCoupons({
        couponCode: hideCoupon,
        active: true,
        singleUsage: true,
        ticketTypeIds: [ticketTypeId4],
        isAvailableForAllTickets: false,
        isAvailableForEveryone: true,
        maxUsageCount: 1,
        maxTickets: 1,
      });
      await ticketingController.addCoupons({
        couponCode: lockCoupon,
        active: true,
        singleUsage: false,
        ticketTypeIds: [ticketTypeId2],
        isAvailableForAllTickets: false,
        isAvailableForEveryone: true,
        maxUsageCount: 1,
      });
    });

    await test.step(`Initialise flowbuilder controller`, async () => {
      flowBuilderController = new FlowBuilderController(orgApiContext, eventId);
    });
    await test.step(`update default flow steps with existing expected steps`, async () => {
      await flowBuilderController.generateSimpleTicketingFlow(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
    });

    await test.step(`make publish flow test data`, async () => {
      let flowDraftId = await flowBuilderController.getFlowId(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
      publishdataPayLoad = {
        flowId: "",
        flowDraftId: flowDraftId,
        clientFlowDraftVersion: 2,
      };
    });

    await test.step(`now, publish the flow`, async () => {
      await flowBuilderController.publishFlowData(publishdataPayLoad);
      await attendeePage.waitForTimeout(5000);
    });

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
  });

  test.beforeEach(async () => {
    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
  });

  test.afterEach(async () => {
    await attendeePage.reload();
    // clear the cookies
    await attendeePage.context().clearCookies();
    await attendeePage.evaluate(() => {
      localStorage.clear();
    });
  });

  test(`TC001: @with-ticket-and-branch Verify flow after entering coupon code where ticket is present after the registration form`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-791/",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-820/",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-805/",
    });
    const ticketName = "ticket1";
    const coupon = couponCode;
    const ticketDetailsData: ExpectedTicketDetailsData = {
      ticketName: ticketName,
      ticketPrice: "$100",
      dicountValue: "$10",
      totalPriceAfterDiscount: "$90",
    };

    await test.step(`User gets a Paid ticket`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form",
        userEmail: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName,
          quantity: 1,
          price: 100,
          coupon: coupon,
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.verifyPaidTicketDetails(true, ticketDetailsData);
      await flowBuilderPage.verifyOrderSummary({
        dataToVerify: {
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
          ticketName: "ticket1",
        },
      });

      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.fillPaymentDetailsPaidTicket({});
      await flowBuilderPage.page.waitForTimeout(10000);
      await flowBuilderPage.verifySingleTicketingSuccess({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
        ticketDetails: {
          ticketName: ticketName,
        },
      });

      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeEmail,
          eventId
        );
      });

      await test.step(`Verify the transaction in vivenu db table`, async () => {
        await ticketingController.verifyVivenuTransactionDetails(
          attendeeEmail,
          eventId,
          attendeePage,
          {
            total_tickets: 1,
            city: "Bhopal",
            company: null,
            currency: "USD",
            email: attendeeEmail,
            postal: "111",
            regular_price: 100,
            last_name: userLastName,
            first_name: userFirstName,
            street: "Random Home",
            price: 90,
            applied_coupons: coupon,
          }
        );
      });
    });

    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      userFirstName,
      userLastName
    );

    let communicationHelper = new CommunicationHelper(
      orgApiContext,
      eventId,
      userInfoDto,
      eventInfoDto,
      true,
      true // for in-person event
    );
    let ticketCode: string;
    await test.step(`Now fetching the user entry from event_registration table and get the ticket`, async () => {
      const userRegDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          eventId,
          attendeeEmail
        );
      ticketCode = userRegDataFromDb["qr_code"];
    });
    let discountedPrice = `${ticketDetailsData.totalPriceAfterDiscount.replace(
      "US",
      ""
    )}.0`;

    await test.step("Fetching Email body from email recieved after registering", async () => {
      await communicationHelper.verifyConfirmationEmailContentsTicketing(
        true,
        "(IST)",
        ticketCode,
        true,
        discountedPrice
      );
    });
  });

  test(`TC002: @with-ticket-and-branch Verify ticket count is reduced`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-820",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-808/",
    });

    const ticketName = "ticket1";
    let ticketInfo = await ticketingController.getTicketInfo(ticketName);
    const totalTickets = ticketInfo["numberOfTickets"];
    const soldTickets = ticketInfo["numberOfSoldTickets"];
    // for non-bulk we cannot check the remaining ticket counts, only the soldout status

    await test.step(`User fills Basic details`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form",
        userEmail: attendeeEmail,
      });
    });

    await test.step(`User should be able to see sold out message`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName,
          quantity: 1,
          price: 100,
          coupon: "",
          expectSoldOut: true,
        },
        expectedFormHeading: "Ticket",
      });
    });
    const additionalTickets = 3;
    await test.step(`Increase the ticket count`, async () => {
      await ticketingController.addMoreTickets(ticketName, additionalTickets);
    });

    const newTicketInfo = await ticketingController.getTicketInfo(ticketName);
    expect(
      newTicketInfo["numberOfTickets"],
      "Expecting the total ticket count to be increased."
    ).toBe(totalTickets + additionalTickets);
    expect(
      newTicketInfo["numberOfSoldTickets"],
      "Expecting sold ticket count to be unaffected."
    ).toBe(soldTickets);
  });

  test(`TC003: @with-ticket-and-branch Verify that the user is able to register after ticket count increase`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-820",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-847/",
    });
    await test.step(`User gets a Paid ticket`, async () => {
      const ticketName = "ticket1";

      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form",
        userEmail: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName,
          quantity: 1,
          price: 100,
          coupon: "",
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.page.waitForTimeout(5000);

      await flowBuilderPage.verifyOrderSummary({
        dataToVerify: {
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
          ticketName: "ticket1",
        },
      });

      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.fillPaymentDetailsPaidTicket({});
      await flowBuilderPage.page.waitForTimeout(10000);
      await flowBuilderPage.verifySingleTicketingSuccess({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
        ticketDetails: {
          ticketName: ticketName,
        },
      });

      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeEmail,
          eventId
        );
      });

      await test.step(`Verify the transaction in vivenu db table`, async () => {
        await ticketingController.verifyVivenuTransactionDetails(
          attendeeEmail,
          eventId,
          attendeePage,
          {
            total_tickets: 1,
            city: "Bhopal",
            company: null,
            currency: "USD",
            email: attendeeEmail,
            postal: "111",
            regular_price: 100,
            last_name: userLastName,
            first_name: userFirstName,
            street: "Random Home",
            price: 100,
          }
        );
      });
    });

    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      userFirstName,
      userLastName
    );

    let communicationHelper = new CommunicationHelper(
      orgApiContext,
      eventId,
      userInfoDto,
      eventInfoDto,
      true,
      true // for in-person event
    );
    let ticketCode: string;
    await test.step(`Now fetching the user entry from event_registration table and get the ticket`, async () => {
      const userRegDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          eventId,
          attendeeEmail
        );
      ticketCode = userRegDataFromDb["qr_code"];
    });
    let discountedPrice = "$100.0";

    await test.step("Fetching Email body from email recieved after registering", async () => {
      await communicationHelper.verifyConfirmationEmailContentsTicketing(
        true,
        "(IST)",
        ticketCode,
        true,
        discountedPrice
      );
    });
  });

  test(`TC004: @with-ticket-and-branch Edit and update ticket.`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-820",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-848/",
    });

    const ticketName = "ticket1";
    const altTicketName = "ticket2";

    await test.step(`User gets a Paid ticket`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form",
        userEmail: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName,
          quantity: 1,
          price: 100,
          coupon: "",
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.page.waitForTimeout(5000);

      await flowBuilderPage.verifyOrderSummary({
        dataToVerify: {
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
          ticketName: "ticket1",
        },
        editTicket: true,
        newTicket: {
          ticketName: altTicketName,
          quantity: 1,
          expectSoldOut: false,
        },
      });

      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.fillPaymentDetailsPaidTicket({});
      await flowBuilderPage.page.waitForTimeout(10000);
      await flowBuilderPage.verifySingleTicketingSuccess({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
        ticketDetails: {
          ticketName: ticketName,
        },
      });

      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeEmail,
          eventId
        );
      });
    });

    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      userFirstName,
      userLastName
    );

    let communicationHelper = new CommunicationHelper(
      orgApiContext,
      eventId,
      userInfoDto,
      eventInfoDto,
      true,
      true // for in-person event
    );
    let ticketCode: string;
    await test.step(`Now fetching the user entry from event_registration table and get the ticket`, async () => {
      const userRegDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          eventId,
          attendeeEmail
        );
      ticketCode = userRegDataFromDb["qr_code"];
    });

    await test.step("Fetching Email body from email recieved after registering", async () => {
      await communicationHelper.verifyConfirmationEmailContentsTicketing(
        true,
        "(IST)",
        ticketCode
      );
    });
  });

  test(`TC005: @with-ticket-and-branch Verify email access domain`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-820",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-849/",
    });

    await test.step(`User gets a Free ticket`, async () => {
      const ticketName = "ticket3";
      const ticketName2 = "ticket2";
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form",
        userEmail: attendeeEmail,
        ticketInfo: {
          ticketName,
          quantity: 1,
          price: 100,
          coupon: "",
          expectSoldOut: false,
        },
      });
      expect(await flowBuilderPage.isTicketDivPresent(ticketName)).toBeFalsy();
      expect(
        await flowBuilderPage.isTicketDivPresent(ticketName2)
      ).toBeTruthy();
    });
  });

  test(`TC006: @with-ticket-and-branch Verify coupon code percentage correctly when user register`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-820",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-850/",
    });
    const ticketName = "ticket4";
    const coupon = couponCode2;
    const ticketDetailsData: ExpectedTicketDetailsData = {
      ticketName: ticketName,
      ticketPrice: "$100",
      dicountValue: "$10",
      totalPriceAfterDiscount: "$90",
    };
    await test.step(`User gets a Paid ticket`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form",
        userEmail: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName,
          quantity: 1,
          price: 100,
          coupon: coupon,
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });
      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.verifyPaidTicketDetails(true, ticketDetailsData);
      await flowBuilderPage.verifyOrderSummary({
        dataToVerify: {
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
          ticketName: ticketName,
        },
      });
      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.fillPaymentDetailsPaidTicket({});
      await flowBuilderPage.page.waitForTimeout(10000);
      await flowBuilderPage.verifySingleTicketingSuccess({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
        ticketDetails: {
          ticketName: ticketName,
        },
      });

      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeEmail,
          eventId
        );
      });

      await test.step(`Verify the transaction in vivenu db table`, async () => {
        await ticketingController.verifyVivenuTransactionDetails(
          attendeeEmail,
          eventId,
          attendeePage,
          {
            total_tickets: 1,
            city: "Bhopal",
            company: null,
            currency: "USD",
            email: attendeeEmail,
            postal: "111",
            regular_price: 100,
            last_name: userLastName,
            first_name: userFirstName,
            street: "Random Home",
            price: 90,
            applied_coupons: coupon,
          }
        );
      });
    });

    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      userFirstName,
      userLastName
    );

    let communicationHelper = new CommunicationHelper(
      orgApiContext,
      eventId,
      userInfoDto,
      eventInfoDto,
      true,
      true // for in-person event
    );
    let ticketCode: string;
    await test.step(`Now fetching the user entry from event_registration table and get the ticket`, async () => {
      const userRegDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          eventId,
          attendeeEmail
        );
      ticketCode = userRegDataFromDb["qr_code"];
    });

    await test.step("Fetching Email body from email recieved after registering", async () => {
      await communicationHelper.verifyConfirmationEmailContentsTicketing(
        true,
        "(IST)",
        ticketCode
      );
    });
  });

  test(`TC007: @with-ticket-and-branch Verify coupon applied for max transaction limit`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-820",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-809/",
    });
    const coupon = couponCode2;
    await test.step(`User gets a Paid ticket`, async () => {
      const ticketName = "ticket4";
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form",
        userEmail: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName,
          quantity: 1,
          price: 100,
          coupon: coupon,
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });
    });
    await test.step(`Apply coupon code->${{
      coupon,
    }} and verify it is not active or invalid`, async () => {
      await flowBuilderPage.applyCouponCode(coupon);
      await flowBuilderPage.verifyCouponInvalidity("expired");
    });
  });

  test(`TC008: @with-ticket-and-branch Verify Lock and Hidden functionality`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-810",
    });
    const ticketName = "LockTicket";
    const coupon = lockCoupon;
    const hiddenTicketName = "HiddenTicket";
    const ticketDetailsData: ExpectedTicketDetailsData = {
      ticketName: hiddenTicketName,
      ticketPrice: "$100",
      dicountValue: "$10",
      totalPriceAfterDiscount: "$90",
    };
    await test.step(`User fills the reg form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form",
        userEmail: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName,
          quantity: 1,
          price: 100,
          coupon: coupon,
          expectSoldOut: false,
        },
        expectLockTicket: true,
        expectedFormHeading: "Ticket",
      });
    });

    await test.step(`Now apply locked coupon-code and check the lock ticket status to be invisble`, async () => {
      await flowBuilderPage.verifyLockedTicketStatusInvisible(ticketName);
      await flowBuilderPage.checkTicketNotPresent(hiddenTicketName);
      await flowBuilderPage.removeCouponCode();
    });

    await test.step(`Then apply hidden coupon-code and verify hidden ticket which was previously inviisble is nos visible`, async () => {
      await flowBuilderPage.applyCouponCode(hideCoupon);
      await flowBuilderPage.checkTicketPresent(hiddenTicketName);
    });

    await test.step(`User now selects 1 hidden ticket and buy the hidden ticket`, async () => {
      await flowBuilderPage.selectQuantityFromDropdownAndVerifyCouponDetails(
        hiddenTicketName,
        1,
        true,
        true,
        ticketDetailsData
      );
      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.verifyPaidTicketDetails(true, ticketDetailsData);
      await flowBuilderPage.clickOnContinueButton();
      await flowBuilderPage.verifyOrderSummary({
        dataToVerify: {
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
          ticketName: hiddenTicketName,
        },
      });
      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.fillPaymentDetailsPaidTicket({});
      await flowBuilderPage.page.waitForTimeout(10000);
      await flowBuilderPage.verifySingleTicketingSuccess({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
        ticketDetails: {
          ticketName: hiddenTicketName,
        },
      });
    });
  });

  test(`TC009: @with-ticket-and-branch Verify Single use of coupon functionality`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-807",
    });

    await test.step(`User fills the registration form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form",
        userEmail: attendeeEmail,
      });
    });

    await test.step(`Apply single use coupon code ${{
      hideCoupon,
    }} and verify it is not active or invalid`, async () => {
      await flowBuilderPage.applyCouponCode(hideCoupon);
      await flowBuilderPage.verifyCouponInvalidity("expired");
    });
  });
});
