import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../../../../controller/EventController";
import { FlowBuilderController } from "../../../../../../controller/FlowBuilderController";
import { TicketingController } from "../../../../../../controller/TicketingController";
import { EventInfoDTO } from "../../../../../../dto/eventInfoDto";
import { UserInfoDTO } from "../../../../../../dto/userInfoDto";
import EventSettingID from "../../../../../../enums/EventSettingEnum";
import { FlowStatus, FlowType } from "../../../../../../enums/FlowTypeEnum";
import EventRole from "../../../../../../enums/eventRoleEnum";
import EventType from "../../../../../../enums/eventTypeEnum";
import { publishDataPayload } from "../../../../../../interfaces/FlowBuilerControllerInterface";
import { FlowBuilderPage } from "../../../../../../page-objects/flow-builder/flow-ticketing";
import CommunicationHelper from "../../../../../../tests/events/communication/communication-tools";
import BrowserFactory from "../../../../../../util/BrowserFactory";
import {
  createNewEvent,
  updateEventLandingPageDetails,
  updateEventType,
} from "../../../../../../util/apiUtil";
import { DataUtl } from "../../../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../../../util/email-validation-api-util";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@flow-builder @registraion-without-ticket`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId: any;
  let attendeeBrowserContext: BrowserContext;
  let eventController: EventController;
  let flowBuilderPage: FlowBuilderPage;
  let flowBuilderController: FlowBuilderController;
  let publishdataPayLoad: publishDataPayload;
  let attendeePage: Page;
  let attendeeEmail: string;
  let registeredEmail: string;
  const userFirstName = "Jxtin";
  const userLastName = "zuddl";
  let eventInfoDto: EventInfoDTO;
  let ticketingController: TicketingController;

  test.beforeAll(async () => {
    await test.step(`setup browser contexts`, async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: true,
      });
      orgApiContext = orgBrowserContext.request;
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage = await attendeeBrowserContext.newPage();
    });

    await test.step(`create an event with FLEX as resgistration type`, async () => {
      eventId = await createNewEvent({
        api_request_context: orgApiContext,
        event_title: "flow-with-ticket automation",
        isFlex: true,
      });
      console.log(`event id ${eventId}`);
    });

    await test.step(`update an event with IN-PERSON as event type`, async () => {
      await updateEventType(orgApiContext, eventId, EventType.IN_PERSON);
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        isMagicLinkEnabled: true,
      });
    });

    await test.step(`Initialise event controller`, async () => {
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step(`Get event details`, async () => {
      let eventInfo = await (await eventController.getEventInfo()).json();
      console.log("Event Info: ", eventInfo);
      eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
      eventInfoDto.startDateTime = eventInfo.startDateTime;
    });

    await enableDisableEmailTrigger(
      orgApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );

    ticketingController = new TicketingController(orgApiContext, eventId);

    await ticketingController.selectGateway();

    await ticketingController.createTicket({
      name: "ticket1",
      numberOfTickets: 1,
      pricePerTicket: 0,
    });

    await ticketingController.createTicket({
      name: "ticket2",
      numberOfTickets: 2,
      pricePerTicket: 0,
    });
    await ticketingController.createTicket({
      name: "ticket3",
      numberOfTickets: 1,
      pricePerTicket: 0,
      isAvailableForAll: false,
      isDomainRestriction: true,
      domains: "gmail.com",
    });

    await test.step(`Initialise flowbuilder controller`, async () => {
      flowBuilderController = new FlowBuilderController(orgApiContext, eventId);
    });
    await test.step(`update default flow steps with existing expected steps`, async () => {
      await flowBuilderController.generateSimpleTicketingFlow(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
    });

    await test.step(`make publish flow test data`, async () => {
      let flowDraftId = await flowBuilderController.getFlowId(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
      publishdataPayLoad = {
        flowId: "",
        flowDraftId: flowDraftId,
        clientFlowDraftVersion: 2,
      };
    });

    await test.step(`now, publish the flow`, async () => {
      await flowBuilderController.publishFlowData(publishdataPayLoad);
      await attendeePage.waitForTimeout(5000);
    });

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
  });

  test.beforeEach(async () => {
    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
  });

  test.afterEach(async () => {
    await attendeePage.reload();
    // clear the cookies
    await attendeePage.context().clearCookies();
    await attendeePage.evaluate(() => {
      localStorage.clear();
    });
  });

  test.afterAll(async () => {
    await orgBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test(`TC001: @with-ticket-without-branch Verify that the new user successfully registered to the event`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-742",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-783/",
    });

    await test.step(`User gets a Free ticket`, async () => {
      const ticketName = "ticket1";

      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form",
        userEmail: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName,
          quantity: 1,
          price: 0,
          coupon: "",
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.page.waitForTimeout(5000);

      await flowBuilderPage.verifyOrderSummary({
        dataToVerify: {
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
          ticketName: "ticket1",
        },
      });

      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.fillPaymentDetailsFreeTicket({});
      await flowBuilderPage.page.waitForTimeout(10000);
      await flowBuilderPage.verifySingleTicketingSuccess({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
        ticketDetails: {
          ticketName: ticketName,
        },
      });

      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeEmail,
          eventId
        );
      });

      await test.step(`Verify the transaction in vivenu db table`, async () => {
        await ticketingController.verifyVivenuTransactionDetails(
          attendeeEmail,
          eventId,
          attendeePage,
          {
            total_tickets: 1,
            city: "",
            company: null,
            currency: "USD",
            email: attendeeEmail,
            postal: "111",
            regular_price: 0,
            last_name: userLastName,
            first_name: userFirstName,
            street: "Random Home",
            price: 0,
          }
        );
      });
    });

    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      userFirstName,
      userLastName
    );

    let communicationHelper = new CommunicationHelper(
      orgApiContext,
      eventId,
      userInfoDto,
      eventInfoDto,
      true,
      true // for in-person event
    );
    let ticketCode: string;
    await test.step(`Now fetching the user entry from event_registration table and get the ticket`, async () => {
      const userRegDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          eventId,
          attendeeEmail
        );
      ticketCode = userRegDataFromDb["qr_code"];
    });

    await test.step("Fetching Email body from email recieved after registering", async () => {
      await communicationHelper.verifyConfirmationEmailContentsTicketing(
        true,
        "(IST)",
        ticketCode
      );
    });
  });

  test(`TC002: @with-ticket-without-branch Verify ticket count is reduced`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-742",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-802/",
    });

    const ticketName = "ticket1";
    let ticketInfo = await ticketingController.getTicketInfo(ticketName);
    const totalTickets = ticketInfo["numberOfTickets"];
    const soldTickets = ticketInfo["numberOfSoldTickets"];
    // for non-bulk we cannot check the remaining ticket counts, only the soldout status

    await test.step(`User fills Basic details`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form",
        userEmail: attendeeEmail,
      });
    });

    await test.step(`User should be able to see sold out message`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName,
          quantity: 1,
          price: 0,
          coupon: "",
          expectSoldOut: true,
        },
        expectedFormHeading: "Ticket",
      });
    });
    const additionalTickets = 3;
    await test.step(`Increase the ticket count`, async () => {
      await ticketingController.addMoreTickets(ticketName, additionalTickets);
    });

    const newTicketInfo = await ticketingController.getTicketInfo(ticketName);
    expect(
      newTicketInfo["numberOfTickets"],
      "Expecting the total ticket count to be increased."
    ).toBe(totalTickets + additionalTickets);
    expect(
      newTicketInfo["numberOfSoldTickets"],
      "Expecting sold ticket count to be unaffected."
    ).toBe(soldTickets);
  });

  test(`TC003: @with-ticket-without-branch Verify that the user is able to register after ticket count increase`, async () => {
    await test.step(`User gets a Free ticket`, async () => {
      const ticketName = "ticket1";

      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form",
        userEmail: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName,
          quantity: 1,
          price: 0,
          coupon: "",
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.page.waitForTimeout(5000);

      await flowBuilderPage.verifyOrderSummary({
        dataToVerify: {
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
          ticketName: "ticket1",
        },
      });

      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.fillPaymentDetailsFreeTicket({});
      await flowBuilderPage.page.waitForTimeout(10000);
      await flowBuilderPage.verifySingleTicketingSuccess({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
        ticketDetails: {
          ticketName: ticketName,
        },
      });

      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeEmail,
          eventId
        );
      });
    });
    await test.step(`Verify the transaction in vivenu db table`, async () => {
      await ticketingController.verifyVivenuTransactionDetails(
        attendeeEmail,
        eventId,
        attendeePage,
        {
          total_tickets: 1,
          city: "Bhopal",
          company: null,
          currency: "USD",
          email: attendeeEmail,
          postal: "111",
          regular_price: 0,
          last_name: userLastName,
          first_name: userFirstName,
          street: "Random Home",
          price: 0,
        }
      );
    });

    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      userFirstName,
      userLastName
    );

    let communicationHelper = new CommunicationHelper(
      orgApiContext,
      eventId,
      userInfoDto,
      eventInfoDto,
      true,
      true // for in-person event
    );
    let ticketCode: string;
    await test.step(`Now fetching the user entry from event_registration table and get the ticket`, async () => {
      const userRegDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          eventId,
          attendeeEmail
        );
      ticketCode = userRegDataFromDb["qr_code"];
    });

    await test.step("Fetching Email body from email recieved after registering", async () => {
      await communicationHelper.verifyConfirmationEmailContentsTicketing(
        true,
        "(IST)",
        ticketCode
      );
    });
  });

  test(`TC004: @with-ticket-without-branch Edit and update ticket.`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-742",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-818/",
    });

    const ticketName = "ticket1";
    const altTicketName = "ticket2";

    await test.step(`User gets a Free ticket`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form",
        userEmail: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName,
          quantity: 1,
          price: 0,
          coupon: "",
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.page.waitForTimeout(5000);

      await flowBuilderPage.verifyOrderSummary({
        dataToVerify: {
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
          ticketName: "ticket1",
        },
        editTicket: true,
        newTicket: {
          ticketName: altTicketName,
          quantity: 1,
          expectSoldOut: false,
        },
      });

      await flowBuilderPage.page.waitForTimeout(5000);
      await flowBuilderPage.fillPaymentDetailsFreeTicket({});
      await flowBuilderPage.page.waitForTimeout(10000);
      await flowBuilderPage.verifySingleTicketingSuccess({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
        ticketDetails: {
          ticketName: ticketName,
        },
      });

      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeEmail,
          eventId
        );
      });
      await test.step(`Verify the transaction in vivenu db table`, async () => {
        await ticketingController.verifyVivenuTransactionDetails(
          attendeeEmail,
          eventId,
          attendeePage,
          {
            total_tickets: 1,
            city: "Bhopal",
            company: null,
            currency: "USD",
            email: attendeeEmail,
            postal: "111",
            regular_price: 0,
            last_name: userLastName,
            first_name: userFirstName,
            street: "Random Home",
            price: 0,
          }
        );
      });
    });

    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      userFirstName,
      userLastName
    );

    let communicationHelper = new CommunicationHelper(
      orgApiContext,
      eventId,
      userInfoDto,
      eventInfoDto,
      true,
      true // for in-person event
    );
    let ticketCode: string;
    await test.step(`Now fetching the user entry from event_registration table and get the ticket`, async () => {
      const userRegDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          eventId,
          attendeeEmail
        );
      ticketCode = userRegDataFromDb["qr_code"];
    });

    await test.step("Fetching Email body from email recieved after registering", async () => {
      await communicationHelper.verifyConfirmationEmailContentsTicketing(
        true,
        "(IST)",
        ticketCode
      );
    });
  });

  test(`TC005: @with-ticket-without-branch Verify email access domain`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-742",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-817/",
    });

    await test.step(`User gets a Free ticket`, async () => {
      const ticketName = "ticket3";
      const ticketName2 = "ticket2";
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: userFirstName,
        userLastName: userLastName,
        expectedFormHeading: "Form",
        userEmail: attendeeEmail,
        ticketInfo: {
          ticketName,
          quantity: 1,
          price: 0,
          coupon: "",
          expectSoldOut: false,
        },
      });
      expect(await flowBuilderPage.isTicketDivPresent(ticketName)).toBeFalsy();
      expect(
        await flowBuilderPage.isTicketDivPresent(ticketName2)
      ).toBeTruthy();
    });
  });
});
