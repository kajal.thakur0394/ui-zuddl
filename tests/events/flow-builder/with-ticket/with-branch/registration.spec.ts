import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../../../controller/EventController";
import { FlowBuilderController } from "../../../../../controller/FlowBuilderController";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import EventSettingID from "../../../../../enums/EventSettingEnum";
import { FlowStatus, FlowType } from "../../../../../enums/FlowTypeEnum";
import EventRole from "../../../../../enums/eventRoleEnum";
import EventType from "../../../../../enums/eventTypeEnum";
import {
  ExpectedTicketDetailsData,
  publishDataPayload,
} from "../../../../../interfaces/FlowBuilerControllerInterface";
import { FlowBuilderPage } from "../../../../../page-objects/flow-builder/flow-ticketing";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  createNewEvent,
  updateEventLandingPageDetails,
  updateEventType,
} from "../../../../../util/apiUtil";
import { DataUtl } from "../../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../../util/email-validation-api-util";
import { TicketingController } from "../../../../../controller/TicketingController";
import { QueryUtil } from "../../../../../DB/QueryUtil";

test.describe(
  `@flow-builder @registraion-with-ticket @branching @bulk-ticketing`,
  {
    tag: "@smoke",
  },
  async () => {
    let orgBrowserContext: BrowserContext;
    let orgApiContext: APIRequestContext;
    let eventId: any;
    let attendeeBrowserContext: BrowserContext;
    let eventController: EventController;
    let flowBuilderPage: FlowBuilderPage;
    let flowBuilderController: FlowBuilderController;
    let publishdataPayLoad: publishDataPayload;
    let attendeePage: Page;
    let attendeeEmail: string;
    let registeredEmail: string;
    const userFirstName = "Jxtin";
    const userLastName = "zuddl";
    let eventInfoDto: EventInfoDTO;
    const hideCoupon = DataUtl.getRandomCouponName();

    test.beforeAll(async () => {
      await test.step(`setup browser contexts`, async () => {
        orgBrowserContext = await BrowserFactory.getBrowserContext({
          laodOrganiserCookies: true,
        });
        orgApiContext = orgBrowserContext.request;
        attendeeBrowserContext = await BrowserFactory.getBrowserContext({
          laodOrganiserCookies: false,
        });
        attendeePage = await attendeeBrowserContext.newPage();
      });

      await test.step(`create an event with FLEX as resgistration type`, async () => {
        eventId = await createNewEvent({
          api_request_context: orgApiContext,
          event_title: "flow-with-bulk-ticket automation",
          isFlex: true,
        });
        console.log(`event id ${eventId}`);
      });

      await test.step(`update an event with IN-PERSON as event type`, async () => {
        await updateEventType(orgApiContext, eventId, EventType.IN_PERSON);
        await updateEventLandingPageDetails(orgApiContext, eventId, {
          isMagicLinkEnabled: true,
        });
      });

      await test.step(`Initialise event controller`, async () => {
        eventController = new EventController(orgApiContext, eventId);
      });

      await test.step(`Get event details`, async () => {
        let eventInfo = await (await eventController.getEventInfo()).json();
        console.log("Event Info: ", eventInfo);
        eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
        eventInfoDto.startDateTime = eventInfo.startDateTime;
      });

      await enableDisableEmailTrigger(
        orgApiContext,
        eventId,
        EventSettingID.EmailOnRegistration,
        true,
        EventRole.ATTENDEE,
        true
      );

      let ticketId: string;

      await test.step(`create tickets`, async () => {
        let ticketInfo1 = {
          name: "TKT_1",
          description: "desc",
          numberOfTickets: 10,
          pricePerTicket: 10,
          isBulkPurchaseEnabled: true,
          minTicketsPerOrder: 2,
          maxTicketsPerOrder: 10,
          soldTickets: 0,
        };
        let ticketInfo2 = {
          name: "TKT_Virtual",
          description: "desc virtual",
          numberOfTickets: 10,
          pricePerTicket: 10,
          isBulkPurchaseEnabled: false,
          isHide: true,
        };

        let ticketingController = new TicketingController(
          orgApiContext,
          eventId
        );

        await ticketingController.selectGateway();

        await ticketingController.createTicket(ticketInfo1);
        ticketId = (await ticketingController.createTicket(ticketInfo2))[
          "ticketTypeId"
        ];

        await ticketingController.addCoupons({
          couponCode: hideCoupon,
          active: true,
          singleUsage: false,
          ticketTypeIds: [ticketId],
          isAvailableForAllTickets: false,
          isAvailableForEveryone: true,
          maxUsageCount: 5,
          discountValue: 7,
        });
      });

      await test.step(`Initialise flowbuilder controller`, async () => {
        flowBuilderController = new FlowBuilderController(
          orgApiContext,
          eventId
        );
      });
      await test.step(`make publish flow test data`, async () => {
        let flowDraftId = await flowBuilderController.getFlowId(
          FlowType.REGISTRATION,
          FlowStatus.DRAFT
        );
        publishdataPayLoad = {
          flowId: "",
          flowDraftId: flowDraftId,
          clientFlowDraftVersion: 2,
        };
      });

      await test.step(`update default flow steps with existing expected steps`, async () => {
        await flowBuilderController.generateFlowWithTicketBranching(
          FlowType.REGISTRATION,
          FlowStatus.DRAFT,
          ticketId
        );
      });

      await test.step(`now, publish the flow`, async () => {
        await flowBuilderController.publishFlowData(publishdataPayLoad);
        await attendeePage.waitForTimeout(5000);
      });

      await test.step(`set content to publish site`, async () => {
        await attendeePage.goto("https://example.com");
        await attendeePage.setContent(
          await flowBuilderController.generateEmbedHtml()
        );
        flowBuilderPage = new FlowBuilderPage(attendeePage);
      });
    });

    test.beforeEach(async () => {
      await test.step(`set content to publish site`, async () => {
        await attendeePage.goto("https://example.com");
        await attendeePage.setContent(
          await flowBuilderController.generateEmbedHtml()
        );
        flowBuilderPage = new FlowBuilderPage(attendeePage);
      });
      attendeeEmail = DataUtl.getRandomAttendeeEmail();
    });

    test.afterEach(async () => {
      await attendeePage.reload();
      // clear the cookies
      await attendeePage.context().clearCookies();
      await attendeePage.evaluate(() => {
        localStorage.clear();
      });
    });

    test.afterAll(async () => {
      await orgBrowserContext?.close();
      await attendeeBrowserContext?.close();
    });

    test(`Email is not gmail.com, user chooses TKT_1`, async () => {
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-855",
      }); // common across all the tests

      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-813/with-branching",
      }); // common across all the tests

      let attendeeDetails = [];

      attendeeEmail = DataUtl.getRandomAttendeeEmail();
      const ticketInfo = {
        ticketName: "TKT_1",
        quantity: 2,
        price: 10,
        coupon: "",
        expectSoldOut: false,
      };

      const ticketQty = ticketInfo.quantity;
      attendeeDetails.push({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });

      for (let i = 0; i < ticketQty - 1; i++) {
        attendeeDetails.push({
          firstName: DataUtl.getRandomName(),
          lastName: DataUtl.getRandomName(),
          email: DataUtl.getRandomAttendeeEmail(),
        });
      }

      await test.step(`User fills up the form`, async () => {
        await flowBuilderPage.handleTicketForm({
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
        });

        await flowBuilderPage.fillRegistrationForm({
          ticketInfo: ticketInfo,
          expectedFormHeading: "Ticket",
        });

        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: attendeeDetails,
          ticketInfo: ticketInfo,
        });
        await flowBuilderPage.verifyOrderSummaryBulk({
          dataToVerify: {
            attendeeDetails: attendeeDetails,
            ticketName: ticketInfo.ticketName,
          },
        });
        await flowBuilderPage.verifyPaidTicketDetails(false, {
          ticketName: ticketInfo.ticketName,
          ticketPrice: (ticketQty * ticketInfo.price).toString(),
          dicountValue: "0",
          totalPriceAfterDiscount: (ticketQty * ticketInfo.price).toString(),
        });
        await flowBuilderPage.fillPaymentDetailsPaidTicket({});
        await flowBuilderPage.verifyBulkTicketingSuccess({
          attendeeDetails: attendeeDetails,
          ticketDetails: {
            ticketName: ticketInfo.ticketName,
          },
        });
      });

      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        for (let i = 0; i < attendeeDetails.length; i++) {
          await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
            attendeeDetails[i].email,
            eventId
          );
        }
      });
    });

    test(`Email is gmail.com, user chooses TKT_1`, async () => {
      let attendeeDetails = [];

      attendeeEmail = DataUtl.getRandomAttendeeEmail("gmail.com");
      const ticketInfo = {
        ticketName: "TKT_1",
        quantity: 2,
        price: 10,
        coupon: "",
        expectSoldOut: false,
      };

      const ticketQty = ticketInfo.quantity;
      attendeeDetails.push({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });

      for (let i = 0; i < ticketQty - 1; i++) {
        attendeeDetails.push({
          firstName: DataUtl.getRandomName(),
          lastName: DataUtl.getRandomName(),
          email: DataUtl.getRandomAttendeeEmail(),
        });
      }

      await test.step(`User fills up the form`, async () => {
        await flowBuilderPage.handleTicketForm({
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
        });

        await flowBuilderPage.fillRegistrationForm({
          ticketInfo: ticketInfo,
          expectedFormHeading: "Ticket",
        });

        await flowBuilderPage.fillSingleFieldFormTicket({
          expectedFormHeading: "Form - Email address",
          expectedFieldName: "Field",
          fieldValue: "hello",
        });

        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: attendeeDetails,
          ticketInfo: ticketInfo,
        });
        await flowBuilderPage.verifyOrderSummaryBulk({
          dataToVerify: {
            attendeeDetails: attendeeDetails,
            ticketName: ticketInfo.ticketName,
          },
        });
        await flowBuilderPage.page.waitForTimeout(5000);
        await flowBuilderPage.verifyPaidTicketDetails(false, {
          ticketName: ticketInfo.ticketName,
          ticketPrice: (ticketQty * ticketInfo.price).toString(),
          dicountValue: "0",
          totalPriceAfterDiscount: (ticketQty * ticketInfo.price).toString(),
        });

        await flowBuilderPage.fillPaymentDetailsPaidTicket({});
        await flowBuilderPage.page.waitForTimeout(5000);
        await flowBuilderPage.verifyBulkTicketingSuccess({
          attendeeDetails: attendeeDetails,
          ticketDetails: {
            ticketName: ticketInfo.ticketName,
          },
        });
      });

      await flowBuilderPage.waitForPaymentConfirmation();
      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        for (let i = 0; i < attendeeDetails.length; i++) {
          await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
            attendeeDetails[i].email,
            eventId
          );
        }
      });
    });

    test(`Email is not gmail.com, user chooses TKT_VIRTUAL`, async () => {
      let attendeeDetails = [];

      attendeeEmail = DataUtl.getRandomAttendeeEmail();
      const ticketInfo = {
        ticketName: "TKT_Virtual",
        quantity: 1,
        price: 10,
        coupon: "",
        expectSoldOut: false,
      };

      const ticketQty = ticketInfo.quantity;
      attendeeDetails.push({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });

      await test.step(`User fills up the form`, async () => {
        await flowBuilderPage.handleTicketForm({
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
        });

        await flowBuilderPage.page.waitForTimeout(5000);
        await flowBuilderPage.applyCouponCode(hideCoupon);
        let ticketDetailsData: ExpectedTicketDetailsData = {
          ticketName: ticketInfo.ticketName,
          ticketPrice: (ticketQty * ticketInfo.price).toString(),
          dicountValue: "7",
          totalPriceAfterDiscount: `$${(
            ticketQty * ticketInfo.price -
            7
          ).toString()}`,
        };

        await flowBuilderPage.selectQuantityFromDropdownAndVerifyCouponDetails(
          hideCoupon,
          1,
          true,
          true,
          ticketDetailsData
        );

        await flowBuilderPage.clickOnContinueButton();
        await flowBuilderPage.page.waitForTimeout(5000);
        await flowBuilderPage.fillSingleFieldFormTicket({
          expectedFormHeading: "Form - ticket type",
          expectedFieldName: "Field - ticket",
          fieldValue: "hello",
        });

        await flowBuilderPage.page.waitForTimeout(5000);

        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: attendeeDetails,
          ticketInfo: ticketInfo,
        });
        await flowBuilderPage.page.waitForTimeout(5000);
        await flowBuilderPage.verifyOrderSummaryBulk({
          dataToVerify: {
            attendeeDetails: attendeeDetails,
            ticketName: ticketInfo.ticketName,
          },
        });
        await flowBuilderPage.page.waitForTimeout(5000);
        await flowBuilderPage.verifyPaidTicketDetails(true, {
          ticketName: ticketInfo.ticketName,
          ticketPrice: (ticketQty * ticketInfo.price).toString(),
          dicountValue: "7",
          totalPriceAfterDiscount: (
            ticketQty * ticketInfo.price -
            7
          ).toString(),
        });

        await flowBuilderPage.fillPaymentDetailsPaidTicket({});
        await flowBuilderPage.page.waitForTimeout(5000);
        await flowBuilderPage.verifyBulkTicketingSuccess({
          attendeeDetails: attendeeDetails,
          ticketDetails: {
            ticketName: ticketInfo.ticketName,
          },
        });
      });

      await flowBuilderPage.waitForPaymentConfirmation();
      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        for (let i = 0; i < attendeeDetails.length; i++) {
          await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
            attendeeDetails[i].email,
            eventId
          );
        }
      });
    });

    test(`Email is gmail.com, user chooses TKT_VIRTUAL`, async () => {
      let attendeeDetails = [];

      attendeeEmail = DataUtl.getRandomAttendeeEmail("gmail.com");
      const ticketInfo = {
        ticketName: "TKT_Virtual",
        quantity: 1,
        price: 10,
        coupon: "",
        expectSoldOut: false,
      };

      const ticketQty = ticketInfo.quantity;
      attendeeDetails.push({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });

      await test.step(`User fills up the form`, async () => {
        await flowBuilderPage.handleTicketForm({
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
        });

        await flowBuilderPage.page.waitForTimeout(5000);
        await flowBuilderPage.applyCouponCode(hideCoupon);
        let ticketDetailsData: ExpectedTicketDetailsData = {
          ticketName: ticketInfo.ticketName,
          ticketPrice: (ticketQty * ticketInfo.price).toString(),
          dicountValue: "7",
          totalPriceAfterDiscount: `$${(
            ticketQty * ticketInfo.price -
            7
          ).toString()}`,
        };

        await flowBuilderPage.selectQuantityFromDropdownAndVerifyCouponDetails(
          hideCoupon,
          1,
          true,
          true,
          ticketDetailsData
        );

        await flowBuilderPage.clickOnContinueButton();
        await flowBuilderPage.page.waitForTimeout(5000);
        await flowBuilderPage.fillSingleFieldFormTicket({
          expectedFormHeading: "Form - Email address",
          expectedFieldName: "Field",
          fieldValue: "hello",
        });

        await flowBuilderPage.page.waitForTimeout(5000);

        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: attendeeDetails,
          ticketInfo: ticketInfo,
        });
        await flowBuilderPage.page.waitForTimeout(5000);
        await flowBuilderPage.verifyOrderSummaryBulk({
          dataToVerify: {
            attendeeDetails: attendeeDetails,
            ticketName: ticketInfo.ticketName,
          },
        });
        await flowBuilderPage.page.waitForTimeout(5000);
        await flowBuilderPage.verifyPaidTicketDetails(true, {
          ticketName: ticketInfo.ticketName,
          ticketPrice: (ticketQty * ticketInfo.price).toString(),
          dicountValue: "7",
          totalPriceAfterDiscount: (
            ticketQty * ticketInfo.price -
            7
          ).toString(),
        });

        await flowBuilderPage.fillPaymentDetailsPaidTicket({});
        await flowBuilderPage.page.waitForTimeout(5000);
        await flowBuilderPage.verifyBulkTicketingSuccess({
          attendeeDetails: attendeeDetails,
          ticketDetails: {
            ticketName: ticketInfo.ticketName,
          },
        });
      });

      await flowBuilderPage.waitForPaymentConfirmation();
      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        for (let i = 0; i < attendeeDetails.length; i++) {
          await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
            attendeeDetails[i].email,
            eventId
          );
        }
      });
    });
  }
);
