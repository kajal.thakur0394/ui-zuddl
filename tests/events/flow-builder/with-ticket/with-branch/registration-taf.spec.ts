import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../../../controller/EventController";
import { FlowBuilderController } from "../../../../../controller/FlowBuilderController";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import EventSettingID from "../../../../../enums/EventSettingEnum";
import { FlowStatus, FlowType } from "../../../../../enums/FlowTypeEnum";
import EventRole from "../../../../../enums/eventRoleEnum";
import EventType from "../../../../../enums/eventTypeEnum";
import {
  ExpectedTicketDetailsData,
  publishDataPayload,
} from "../../../../../interfaces/FlowBuilerControllerInterface";
import { FlowBuilderPage } from "../../../../../page-objects/flow-builder/flow-ticketing";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  createNewEvent,
  updateEventLandingPageDetails,
  updateEventType,
} from "../../../../../util/apiUtil";
import { DataUtl } from "../../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../../util/email-validation-api-util";
import { TicketingController } from "../../../../../controller/TicketingController";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(
  `@flow-builder @registraion-with-ticket @branching @bulk-ticketing @ticketing-first-step`,
  {
    tag: "@smoke",
  },
  async () => {
    let orgBrowserContext: BrowserContext;
    let orgApiContext: APIRequestContext;
    let eventId: any;
    let attendeeBrowserContext: BrowserContext;
    let eventController: EventController;
    let flowBuilderPage: FlowBuilderPage;
    let flowBuilderController: FlowBuilderController;
    let publishdataPayLoad: publishDataPayload;
    let attendeePage: Page;
    let attendeeEmail: string;
    let registeredEmail: string;
    const userFirstName = "Jxtin";
    const userLastName = "zuddl";
    let eventInfoDto: EventInfoDTO;
    const hideCoupon = DataUtl.getRandomCouponName();

    test.beforeAll(async () => {
      await test.step(`setup browser contexts`, async () => {
        orgBrowserContext = await BrowserFactory.getBrowserContext({
          laodOrganiserCookies: true,
        });
        orgApiContext = orgBrowserContext.request;
        attendeeBrowserContext = await BrowserFactory.getBrowserContext({
          laodOrganiserCookies: false,
        });
        attendeePage = await attendeeBrowserContext.newPage();
      });

      await test.step(`create an event with FLEX as resgistration type`, async () => {
        eventId = await createNewEvent({
          api_request_context: orgApiContext,
          event_title: "flow-with-bulk-ticket automation",
          isFlex: true,
        });
        console.log(`event id ${eventId}`);
      });

      await test.step(`update an event with IN-PERSON as event type`, async () => {
        await updateEventType(orgApiContext, eventId, EventType.IN_PERSON);
        await updateEventLandingPageDetails(orgApiContext, eventId, {
          isMagicLinkEnabled: true,
        });
      });

      await test.step(`Initialise event controller`, async () => {
        eventController = new EventController(orgApiContext, eventId);
      });

      await test.step(`Get event details`, async () => {
        let eventInfo = await (await eventController.getEventInfo()).json();
        console.log("Event Info: ", eventInfo);
        eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
        eventInfoDto.startDateTime = eventInfo.startDateTime;
      });

      await enableDisableEmailTrigger(
        orgApiContext,
        eventId,
        EventSettingID.EmailOnRegistration,
        true,
        EventRole.ATTENDEE,
        true
      );

      let virtualTicketId: string;
      let inPersonTicketId: string;

      await test.step(`create tickets`, async () => {
        let inPersonTicket = {
          name: "tkt_inp",
          description: "desc",
          ticketTagType: EventType.IN_PERSON,
          numberOfTickets: 10,
          pricePerTicket: 0,
          isBulkPurchaseEnabled: true,
          minTicketsPerOrder: 2,
          maxTicketsPerOrder: 10,
          soldTickets: 0,
        };
        let virtualTicket = {
          name: "tkt_virtual",
          description: "desc virtual",
          ticketTagType: EventType.VIRTUAL,
          numberOfTickets: 10,
          pricePerTicket: 0,
          isBulkPurchaseEnabled: true,
          minTicketsPerOrder: 2,
          maxTicketsPerOrder: 10,
          soldTickets: 0,
        };

        let ticketingController = new TicketingController(
          orgApiContext,
          eventId
        );

        await ticketingController.selectGateway();

        virtualTicketId = (
          await ticketingController.createTicket(virtualTicket)
        )["ticketTypeId"];

        inPersonTicketId = (
          await ticketingController.createTicket(inPersonTicket)
        )["ticketTypeId"];

        await test.step(`Initialise flowbuilder controller`, async () => {
          flowBuilderController = new FlowBuilderController(
            orgApiContext,
            eventId
          );
        });
        await test.step(`make publish flow test data`, async () => {
          let flowDraftId = await flowBuilderController.getFlowId(
            FlowType.REGISTRATION,
            FlowStatus.DRAFT
          );
          publishdataPayLoad = {
            flowId: "",
            flowDraftId: flowDraftId,
            clientFlowDraftVersion: 2,
          };
        });

        await test.step(`update default flow steps with existing expected steps`, async () => {
          await flowBuilderController.generateFlowWithTAFBranching(
            FlowType.REGISTRATION,
            FlowStatus.DRAFT,
            inPersonTicketId,
            virtualTicketId
          );
        });

        await test.step(`now, publish the flow`, async () => {
          await flowBuilderController.publishFlowData(publishdataPayLoad);
          await attendeePage.waitForTimeout(5000);
        });

        await test.step(`set content to publish site`, async () => {
          await attendeePage.goto("https://example.com");
          await attendeePage.setContent(
            await flowBuilderController.generateEmbedHtml()
          );
          flowBuilderPage = new FlowBuilderPage(attendeePage);
        });
      });
    });
    test.beforeEach(async () => {
      await test.step(`set content to publish site`, async () => {
        await attendeePage.goto("https://example.com");
        await attendeePage.setContent(
          await flowBuilderController.generateEmbedHtml()
        );
        flowBuilderPage = new FlowBuilderPage(attendeePage);
      });
      attendeeEmail = DataUtl.getRandomAttendeeEmail();
    });

    test.afterEach(async () => {
      await attendeePage.reload();
      // clear the cookies
      await attendeePage.context().clearCookies();
      await attendeePage.evaluate(() => {
        localStorage.clear();
      });
    });

    test.afterAll(async () => {
      await orgBrowserContext?.close();
      await attendeeBrowserContext?.close();
    });

    test(`TC001 : User gets tkt_inp, no ticket holder has gmail.com`, async () => {
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-855",
      }); // common across all the tests

      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-813/with-branching",
      }); // common across all the tests

      await test.step(`User gets a Paid ticket`, async () => {
        const ticketName = "tkt_inp";
        const ticketQty = 2;
        const ticketDetailsData: ExpectedTicketDetailsData = {
          ticketName: ticketName,
          ticketPrice: "Free",
          dicountValue: "0",
          totalPriceAfterDiscount: "Free",
        };

        await flowBuilderPage.fillRegistrationForm({
          ticketInfo: {
            ticketName: ticketName,
            quantity: ticketQty,
            price: 0,
            coupon: "",
            expectSoldOut: false,
          },
          expectedFormHeading: "Ticket",
        });

        let attendeeDetails = [];

        attendeeDetails.push({
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
        });
        for (let i = 1; i < ticketQty; i++) {
          attendeeDetails.push({
            firstName: DataUtl.getRandomName(),
            lastName: DataUtl.getRandomName(),
            email: DataUtl.getRandomAttendeeEmail(),
          });
        }
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: [attendeeDetails[0]],
          ticketInfo: {
            ticketName: ticketName,
            quantity: 1,
            price: 0,
            coupon: "",
            expectSoldOut: false,
          },
          ticketAsFirstStep: true,
        });
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.fillSingleFieldFormTicket({
          expectedFormHeading: "Form - tkt_free",
          expectedFieldName: "Field",
          fieldValue: "test",
        });
        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: [attendeeDetails[1]],
          ticketInfo: {
            ticketName: ticketName,
            quantity: 1,
            price: 0,
            coupon: "",
            expectSoldOut: false,
          },
          ticketAsFirstStep: true,
        });
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.fillSingleFieldFormTicket({
          expectedFormHeading: "Form - tkt_free",
          expectedFieldName: "Field",
          fieldValue: "test",
        });

        await flowBuilderPage.page.waitForTimeout(2000);

        await flowBuilderPage.verifyBulkTicketingSuccess({
          attendeeDetails: attendeeDetails,
          ticketDetails: {
            ticketName: ticketName,
          },
        });

        await flowBuilderPage.clickOnContinueButton();
        await flowBuilderPage.page.waitForTimeout(2500);

        await flowBuilderPage.fillPaymentDetailsFreeTicket({});
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.verifyBulkTicketingSuccess({
          attendeeDetails: attendeeDetails,
          ticketDetails: {
            ticketName: ticketName,
          },
        });
      });
      await flowBuilderPage.waitForPaymentConfirmation();

      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeEmail,
          eventId
        );
      });
    });

    test(`TC002 : User gets tkt_inp, both have gmail.com`, async () => {
      await test.step(`User gets a Paid ticket`, async () => {
        const ticketName = "tkt_inp";
        const ticketQty = 2;
        const ticketDetailsData: ExpectedTicketDetailsData = {
          ticketName: ticketName,
          ticketPrice: "Free",
          dicountValue: "0",
          totalPriceAfterDiscount: "Free",
        };

        await flowBuilderPage.fillRegistrationForm({
          ticketInfo: {
            ticketName: ticketName,
            quantity: ticketQty,
            price: 0,
            coupon: "",
            expectSoldOut: false,
          },
          expectedFormHeading: "Ticket",
        });
        attendeeEmail = DataUtl.getRandomAttendeeEmail("gmail.com");
        let attendeeDetails = [];

        attendeeDetails.push({
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
        });
        for (let i = 1; i < ticketQty; i++) {
          attendeeDetails.push({
            firstName: DataUtl.getRandomName(),
            lastName: DataUtl.getRandomName(),
            email: DataUtl.getRandomAttendeeEmail("gmail.com"),
          });
        }
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: [attendeeDetails[0]],
          ticketInfo: {
            ticketName: ticketName,
            quantity: 1,
            price: 0,
            coupon: "",
            expectSoldOut: false,
          },
          ticketAsFirstStep: true,
        });
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.fillSingleFieldFormTicket({
          expectedFormHeading: "Form - email contains gmail",
          expectedFieldName: "Field",
          fieldValue: "test",
        });
        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: [attendeeDetails[1]],
          ticketInfo: {
            ticketName: ticketName,
            quantity: 1,
            price: 0,
            coupon: "",
            expectSoldOut: false,
          },
          ticketAsFirstStep: true,
        });
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.fillSingleFieldFormTicket({
          expectedFormHeading: "Form - email contains gmail",
          expectedFieldName: "Field",
          fieldValue: "test",
        });

        await flowBuilderPage.page.waitForTimeout(2000);

        await flowBuilderPage.verifyBulkTicketingSuccess({
          attendeeDetails: attendeeDetails,
          ticketDetails: {
            ticketName: ticketName,
          },
        });

        await flowBuilderPage.clickOnContinueButton();
        await flowBuilderPage.page.waitForTimeout(2500);

        await flowBuilderPage.fillPaymentDetailsFreeTicket({});
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.verifyBulkTicketingSuccess({
          attendeeDetails: attendeeDetails,
          ticketDetails: {
            ticketName: ticketName,
          },
        });
      });
      await flowBuilderPage.waitForPaymentConfirmation();

      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeEmail,
          eventId
        );
      });
    });

    test(`TC003 : User gets tkt_inp, one of them is gmail.com`, async () => {
      await test.step(`User gets a Paid ticket`, async () => {
        const ticketName = "tkt_inp";
        const ticketQty = 2;
        const ticketDetailsData: ExpectedTicketDetailsData = {
          ticketName: ticketName,
          ticketPrice: "Free",
          dicountValue: "0",
          totalPriceAfterDiscount: "Free",
        };

        await flowBuilderPage.fillRegistrationForm({
          ticketInfo: {
            ticketName: ticketName,
            quantity: ticketQty,
            price: 0,
            coupon: "",
            expectSoldOut: false,
          },
          expectedFormHeading: "Ticket",
        });
        attendeeEmail = DataUtl.getRandomAttendeeEmail();
        let attendeeDetails = [];

        attendeeDetails.push({
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
        });
        for (let i = 1; i < ticketQty; i++) {
          attendeeDetails.push({
            firstName: DataUtl.getRandomName(),
            lastName: DataUtl.getRandomName(),
            email: DataUtl.getRandomAttendeeEmail("gmail.com"),
          });
        }
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: [attendeeDetails[0]],
          ticketInfo: {
            ticketName: ticketName,
            quantity: 1,
            price: 0,
            coupon: "",
            expectSoldOut: false,
          },
          ticketAsFirstStep: true,
        });
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.fillSingleFieldFormTicket({
          expectedFormHeading: "Form - tkt_free",
          expectedFieldName: "Field",
          fieldValue: "test",
        });
        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: [attendeeDetails[1]],
          ticketInfo: {
            ticketName: ticketName,
            quantity: 1,
            price: 0,
            coupon: "",
            expectSoldOut: false,
          },
          ticketAsFirstStep: true,
        });
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.fillSingleFieldFormTicket({
          expectedFormHeading: "Form - email contains gmail",
          expectedFieldName: "Field",
          fieldValue: "test",
        });

        await flowBuilderPage.page.waitForTimeout(2000);

        await flowBuilderPage.verifyBulkTicketingSuccess({
          attendeeDetails: attendeeDetails,
          ticketDetails: {
            ticketName: ticketName,
          },
        });

        await flowBuilderPage.clickOnContinueButton();
        await flowBuilderPage.page.waitForTimeout(2500);

        await flowBuilderPage.fillPaymentDetailsFreeTicket({});
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.verifyBulkTicketingSuccess({
          attendeeDetails: attendeeDetails,
          ticketDetails: {
            ticketName: ticketName,
          },
        });
      });
      await flowBuilderPage.waitForPaymentConfirmation();

      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeEmail,
          eventId
        );
      });
    });

    test(`TC004 : User gets tkt_virtual, no ticket holder has gmail.com`, async () => {
      await test.step(`User gets a Paid ticket`, async () => {
        const ticketName = "tkt_virtual";
        const ticketQty = 2;
        const ticketDetailsData: ExpectedTicketDetailsData = {
          ticketName: ticketName,
          ticketPrice: "Free",
          dicountValue: "0",
          totalPriceAfterDiscount: "Free",
        };

        await flowBuilderPage.fillRegistrationForm({
          ticketInfo: {
            ticketName: ticketName,
            quantity: ticketQty,
            price: 0,
            coupon: "",
            expectSoldOut: false,
          },
          expectedFormHeading: "Ticket",
        });

        let attendeeDetails = [];

        attendeeDetails.push({
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
        });
        for (let i = 1; i < ticketQty; i++) {
          attendeeDetails.push({
            firstName: DataUtl.getRandomName(),
            lastName: DataUtl.getRandomName(),
            email: DataUtl.getRandomAttendeeEmail(),
          });
        }
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: [attendeeDetails[0]],
          ticketInfo: {
            ticketName: ticketName,
            quantity: 1,
            price: 0,
            coupon: "",
            expectSoldOut: false,
          },
          ticketAsFirstStep: true,
        });
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.fillSingleFieldFormTicket({
          expectedFormHeading: "Form - tkt_free_virtual",
          expectedFieldName: "Field",
          fieldValue: "test",
        });
        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: [attendeeDetails[1]],
          ticketInfo: {
            ticketName: ticketName,
            quantity: 1,
            price: 0,
            coupon: "",
            expectSoldOut: false,
          },
          ticketAsFirstStep: true,
        });
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.fillSingleFieldFormTicket({
          expectedFormHeading: "Form - tkt_free_virtual",
          expectedFieldName: "Field",
          fieldValue: "test",
        });

        await flowBuilderPage.page.waitForTimeout(2000);

        await flowBuilderPage.verifyBulkTicketingSuccess({
          attendeeDetails: attendeeDetails,
          ticketDetails: {
            ticketName: ticketName,
          },
        });

        await flowBuilderPage.clickOnContinueButton();
        await flowBuilderPage.page.waitForTimeout(2500);

        await flowBuilderPage.fillPaymentDetailsFreeTicket({});
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.verifyBulkTicketingSuccess({
          attendeeDetails: attendeeDetails,
          ticketDetails: {
            ticketName: ticketName,
          },
        });
      });
      await flowBuilderPage.waitForPaymentConfirmation();

      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeEmail,
          eventId
        );
      });
    });

    test(`TC005 : User gets tkt_virtual, one of them is gmail.com`, async () => {
      await test.step(`User gets a Paid ticket`, async () => {
        const ticketName = "tkt_virtual";
        const ticketQty = 2;
        attendeeEmail = DataUtl.getRandomAttendeeEmail("gmail.com");
        const ticketDetailsData: ExpectedTicketDetailsData = {
          ticketName: ticketName,
          ticketPrice: "Free",
          dicountValue: "0",
          totalPriceAfterDiscount: "Free",
        };

        await flowBuilderPage.fillRegistrationForm({
          ticketInfo: {
            ticketName: ticketName,
            quantity: ticketQty,
            price: 0,
            coupon: "",
            expectSoldOut: false,
          },
          expectedFormHeading: "Ticket",
        });

        let attendeeDetails = [];

        attendeeDetails.push({
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
        });
        for (let i = 1; i < ticketQty; i++) {
          attendeeDetails.push({
            firstName: DataUtl.getRandomName(),
            lastName: DataUtl.getRandomName(),
            email: DataUtl.getRandomAttendeeEmail(),
          });
        }
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: [attendeeDetails[0]],
          ticketInfo: {
            ticketName: ticketName,
            quantity: 1,
            price: 0,
            coupon: "",
            expectSoldOut: false,
          },
          ticketAsFirstStep: true,
        });
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.fillSingleFieldFormTicket({
          expectedFormHeading: "Form - email contains gmail",
          expectedFieldName: "Field",
          fieldValue: "test",
        });
        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: [attendeeDetails[1]],
          ticketInfo: {
            ticketName: ticketName,
            quantity: 1,
            price: 0,
            coupon: "",
            expectSoldOut: false,
          },
          ticketAsFirstStep: true,
        });
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.fillSingleFieldFormTicket({
          expectedFormHeading: "Form - tkt_free_virtual",
          expectedFieldName: "Field",
          fieldValue: "test",
        });

        await flowBuilderPage.page.waitForTimeout(2000);

        await flowBuilderPage.verifyBulkTicketingSuccess({
          attendeeDetails: attendeeDetails,
          ticketDetails: {
            ticketName: ticketName,
          },
        });

        await flowBuilderPage.clickOnContinueButton();
        await flowBuilderPage.page.waitForTimeout(2500);

        await flowBuilderPage.fillPaymentDetailsFreeTicket({});
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.verifyBulkTicketingSuccess({
          attendeeDetails: attendeeDetails,
          ticketDetails: {
            ticketName: ticketName,
          },
        });
      });
      await flowBuilderPage.waitForPaymentConfirmation();

      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeEmail,
          eventId
        );
      });
    });

    test(`TC006 : User gets tkt_virtual, both have gmail.com`, async () => {
      await test.step(`User gets a Paid ticket`, async () => {
        const ticketName = "tkt_virtual";
        const ticketQty = 2;
        attendeeEmail = DataUtl.getRandomAttendeeEmail("gmail.com");
        const ticketDetailsData: ExpectedTicketDetailsData = {
          ticketName: ticketName,
          ticketPrice: "Free",
          dicountValue: "0",
          totalPriceAfterDiscount: "Free",
        };

        await flowBuilderPage.fillRegistrationForm({
          ticketInfo: {
            ticketName: ticketName,
            quantity: ticketQty,
            price: 0,
            coupon: "",
            expectSoldOut: false,
          },
          expectedFormHeading: "Ticket",
        });

        let attendeeDetails = [];

        attendeeDetails.push({
          firstName: userFirstName,
          lastName: userLastName,
          email: attendeeEmail,
        });
        for (let i = 1; i < ticketQty; i++) {
          attendeeDetails.push({
            firstName: DataUtl.getRandomName(),
            lastName: DataUtl.getRandomName(),
            email: DataUtl.getRandomAttendeeEmail("gmail.com"),
          });
        }
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: [attendeeDetails[0]],
          ticketInfo: {
            ticketName: ticketName,
            quantity: 1,
            price: 0,
            coupon: "",
            expectSoldOut: false,
          },
          ticketAsFirstStep: true,
        });
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.fillSingleFieldFormTicket({
          expectedFormHeading: "Form - email contains gmail",
          expectedFieldName: "Field",
          fieldValue: "test",
        });
        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: [attendeeDetails[1]],
          ticketInfo: {
            ticketName: ticketName,
            quantity: 1,
            price: 0,
            coupon: "",
            expectSoldOut: false,
          },
          ticketAsFirstStep: true,
        });
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.fillSingleFieldFormTicket({
          expectedFormHeading: "Form - email contains gmail",
          expectedFieldName: "Field",
          fieldValue: "test",
        });

        await flowBuilderPage.page.waitForTimeout(2000);

        await flowBuilderPage.verifyBulkTicketingSuccess({
          attendeeDetails: attendeeDetails,
          ticketDetails: {
            ticketName: ticketName,
          },
        });

        await flowBuilderPage.clickOnContinueButton();
        await flowBuilderPage.page.waitForTimeout(2500);

        await flowBuilderPage.fillPaymentDetailsFreeTicket({});
        await flowBuilderPage.page.waitForTimeout(2000);
        await flowBuilderPage.verifyBulkTicketingSuccess({
          attendeeDetails: attendeeDetails,
          ticketDetails: {
            ticketName: ticketName,
          },
        });
      });
      await flowBuilderPage.waitForPaymentConfirmation();

      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeEmail,
          eventId
        );
      });
    });
  }
);
