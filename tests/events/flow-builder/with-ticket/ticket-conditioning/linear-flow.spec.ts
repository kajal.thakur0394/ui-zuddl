import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../../../controller/EventController";
import { FlowBuilderController } from "../../../../../controller/FlowBuilderController";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import EventSettingID from "../../../../../enums/EventSettingEnum";
import { FlowStatus, FlowType } from "../../../../../enums/FlowTypeEnum";
import EventRole from "../../../../../enums/eventRoleEnum";
import EventType from "../../../../../enums/eventTypeEnum";
import {
  ExpectedTicketDetailsData,
  publishDataPayload,
} from "../../../../../interfaces/FlowBuilerControllerInterface";
import { FlowBuilderPage } from "../../../../../page-objects/flow-builder/flow-ticketing";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  createNewEvent,
  updateEventLandingPageDetails,
  updateEventType,
} from "../../../../../util/apiUtil";
import { DataUtl } from "../../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../../util/email-validation-api-util";
import { TicketingController } from "../../../../../controller/TicketingController";
import { QueryUtil } from "../../../../../DB/QueryUtil";

test.describe(`@flow-builder @registraion-with-ticket @branching @bulk-ticketing`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId: any;
  let attendeeBrowserContext: BrowserContext;
  let eventController: EventController;
  let flowBuilderPage: FlowBuilderPage;
  let flowBuilderController: FlowBuilderController;
  let publishdataPayLoad: publishDataPayload;
  let attendeePage: Page;
  let attendeeEmail: string;
  let eventInfoDto: EventInfoDTO;
  const hideCoupon = DataUtl.getRandomCouponName();
  const lockedCoupon = DataUtl.getRandomCouponName();
  let namedTicketInfo: {
    name: string;
    description: string;
    numberOfTickets: number;
    pricePerTicket: number;
    isBulkPurchaseEnabled: boolean;
    minTicketsPerOrder: number;
    soldTickets: number;
  };
  let luckyTicketInfo: {
    name: string;
    description: string;
    numberOfTickets: number;
    pricePerTicket: number;
    isBulkPurchaseEnabled: boolean;
    minTicketsPerOrder: number;
    soldTickets: number;
  };
  let emailTicketInfo: {
    name: string;
    description: string;
    numberOfTickets: number;
    pricePerTicket: number;
    isBulkPurchaseEnabled: boolean;
    maxTicketsPerOrder: number;
    minTicketsPerOrder: number;
    soldTickets: number;
  };

  let countryTicketInfo: {
    name: string;
    description: string;
    numberOfTickets: number;
    pricePerTicket: number;
    isBulkPurchaseEnabled: boolean;
    maxTicketsPerOrder: number;
    minTicketsPerOrder: number;
    soldTickets: number;
  };

  let hiddenTicketInfo: {
    name: string;
    description: string;
    numberOfTickets: number;
    pricePerTicket: number;
    isBulkPurchaseEnabled: boolean;
    soldTickets: number;
    isHide: boolean;
  };

  let lockedTicketInfo: {
    name: string;
    description: string;
    numberOfTickets: number;
    pricePerTicket: number;
    isBulkPurchaseEnabled: boolean;
    soldTickets: number;
    isLocked: boolean;
  };

  let namedTicketId: string;
  let luckyTicketId: string;
  let emailTicketId: string;
  let countryTicketId: string;
  let hiddenTicketId: string;
  let lockedTicketId: string;

  test.beforeAll(async () => {
    await test.step(`setup browser contexts`, async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: true,
      });
      orgApiContext = orgBrowserContext.request;
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage = await attendeeBrowserContext.newPage();
    });

    await test.step(`create an event with FLEX as resgistration type`, async () => {
      eventId = await createNewEvent({
        api_request_context: orgApiContext,
        event_title: "flow-with-bulk-ticket automation",
        isFlex: true,
      });
      console.log(`event id ${eventId}`);
    });

    await test.step(`update an event with IN-PERSON as event type`, async () => {
      await updateEventType(orgApiContext, eventId, EventType.IN_PERSON);
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        isMagicLinkEnabled: true,
      });
    });

    await test.step(`Initialise event controller`, async () => {
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step(`Get event details`, async () => {
      let eventInfo = await (await eventController.getEventInfo()).json();
      console.log("Event Info: ", eventInfo);
      eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
      eventInfoDto.startDateTime = eventInfo.startDateTime;
    });

    await enableDisableEmailTrigger(
      orgApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );

    let ticketId: string;

    await test.step(`Initialise flowbuilder controller`, async () => {
      flowBuilderController = new FlowBuilderController(orgApiContext, eventId);
    });
    await test.step(`make publish flow test data`, async () => {
      let flowDraftId = await flowBuilderController.getFlowId(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
      publishdataPayLoad = {
        flowId: "",
        flowDraftId: flowDraftId,
        clientFlowDraftVersion: 2,
      };
    });

    await test.step(`update default flow steps with existing expected steps`, async () => {
      await flowBuilderController.generateLinearTktConditoningFlow(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
    });

    await test.step(`now, publish the flow`, async () => {
      await flowBuilderController.publishFlowData(publishdataPayLoad);
      await attendeePage.waitForTimeout(5000);
    });
    await test.step(`create tickets`, async () => {
      // tickets to be created :
      // Ticket on Name (and) :
      //    FirstName = validFirstName
      //    &&
      //    LastName = validLastName
      // Ticket on email :
      //    zuddl.com || zudddl.com || lucky_number = 2
      // Lucky ticket :
      //    if lucky_number = 4
      // Ticket nationality :
      //    if country contains india

      namedTicketInfo = {
        name: "TKT by name",
        description: "desc",
        numberOfTickets: 10,
        pricePerTicket: 0,
        isBulkPurchaseEnabled: false,
        minTicketsPerOrder: 1,
        soldTickets: 0,
      };

      emailTicketInfo = {
        name: "TKT by email",
        description: "desc",
        numberOfTickets: 10,
        pricePerTicket: 0,
        isBulkPurchaseEnabled: true,
        maxTicketsPerOrder: 3,
        minTicketsPerOrder: 1,
        soldTickets: 0,
      };

      luckyTicketInfo = {
        name: "Lucky TKT",
        description: "desc",
        numberOfTickets: 10,
        pricePerTicket: 0,
        isBulkPurchaseEnabled: false,
        minTicketsPerOrder: 1,
        soldTickets: 0,
      };

      countryTicketInfo = {
        name: "Country TKT",
        description: "desc",
        numberOfTickets: 10,
        pricePerTicket: 0,
        isBulkPurchaseEnabled: true,
        maxTicketsPerOrder: 3,
        minTicketsPerOrder: 1,
        soldTickets: 0,
      };

      hiddenTicketInfo = {
        name: "HiddenTicket",
        description: "desc",
        numberOfTickets: 4,
        pricePerTicket: 0,
        isHide: true,
        isBulkPurchaseEnabled: false,
        soldTickets: 0,
      };

      lockedTicketInfo = {
        name: "LockTicket",
        description: "desc",
        numberOfTickets: 2,
        pricePerTicket: 0,
        isLocked: true,
        isBulkPurchaseEnabled: false,
        soldTickets: 0,
      };

      let ticketingController = new TicketingController(orgApiContext, eventId);
      await ticketingController.selectGateway();

      namedTicketId = await ticketingController.createTicket(namedTicketInfo);
      emailTicketId = await ticketingController.createTicket(emailTicketInfo);
      luckyTicketId = await ticketingController.createTicket(luckyTicketInfo);
      countryTicketId = await ticketingController.createTicket(
        countryTicketInfo
      );
      hiddenTicketId = (
        await ticketingController.createTicket(hiddenTicketInfo)
      ).ticketTypeId;
      lockedTicketId = (
        await ticketingController.createTicket(lockedTicketInfo)
      ).ticketTypeId;

      await ticketingController.addCoupons({
        couponCode: hideCoupon,
        active: true,
        singleUsage: true,
        ticketTypeIds: [hiddenTicketId],
        isAvailableForAllTickets: false,
        isAvailableForEveryone: true,
        maxUsageCount: 1,
        maxTickets: 5,
      });
      await ticketingController.addCoupons({
        couponCode: lockedCoupon,
        active: true,
        singleUsage: false,
        ticketTypeIds: [lockedTicketId],
        isAvailableForAllTickets: false,
        isAvailableForEveryone: true,
        maxUsageCount: 1,
      });

      ticketingController.setupLinearTicketConditioning(
        namedTicketInfo.name,
        emailTicketInfo.name,
        luckyTicketInfo.name,
        countryTicketInfo.name,
        hiddenTicketInfo.name,
        lockedTicketInfo.name
      );
    });
    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
  });

  test.beforeEach(async () => {
    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
  });

  test.afterEach(async () => {
    await attendeePage.reload();
    // clear the cookies
    await attendeePage.context().clearCookies();
    await attendeePage.evaluate(() => {
      localStorage.clear();
    });
  });

  test.afterAll(async () => {
    await orgBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test(`TC001: All tickets visible ( Name ticket : condition firstName and lastName )`, async () => {
    let attendeeDetails = [];
    attendeeEmail = DataUtl.getRandomAttendeeEmail("zuddl.com");
    attendeeDetails.push({
      firstName: "jxtin",
      lastName: "me",
      email: attendeeEmail,
    });
    const ticketInfo = {
      ticketName: namedTicketInfo.name,
      quantity: 1,
      price: namedTicketInfo.pricePerTicket,
      coupon: "",
      expectSoldOut: false,
    };

    /*
    form data : // case : all good, expect all tickets to be visible
    firstName: "jxtin"
    lastName: "me"
    email: "random@zuddl.com"
    phone: "1234567890"
    lucky_number: 4 
    country: "India"
    */

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.handleTicketForm({
        firstName: attendeeDetails[0].firstName,
        lastName: attendeeDetails[0].lastName,
        email: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "phone number form",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Are you lucky?",
        luckyNumber: "4",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Nationality Check",
      });
    });

    await test.step(`Verifying ticket visibility`, async () => {
      await flowBuilderPage.expectTicket({
        ticketName: namedTicketInfo.name,
        shouldBeVisible: true,
        expectSoldOut: false,
        ticketPrice: namedTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.expectTicket({
        ticketName: emailTicketInfo.name,
        shouldBeVisible: true,
        expectSoldOut: false,
        ticketPrice: emailTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.expectTicket({
        ticketName: luckyTicketInfo.name,
        shouldBeVisible: true,
        expectSoldOut: false,
        ticketPrice: luckyTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.expectTicket({
        ticketName: countryTicketInfo.name,
        shouldBeVisible: true,
        expectSoldOut: false,
        ticketPrice: countryTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: ticketInfo,
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.clickOnContinueButton(); // to deal with "After ticket is chosen"

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Ticket details",
        attendeeDetails: attendeeDetails,
        ticketInfo: ticketInfo,
      });

      await flowBuilderPage.verifyOrderSummaryBulk({
        dataToVerify: {
          attendeeDetails: attendeeDetails,
          ticketName: ticketInfo.ticketName,
        },
      });

      await flowBuilderPage.fillPaymentDetailsFreeTicket({});

      await flowBuilderPage.waitForPaymentConfirmation();

      await flowBuilderPage.verifySuccessFullRegistration({
        userFirstName: attendeeDetails[0].firstName,
        userLastName: attendeeDetails[0].lastName,
        email: attendeeEmail,
      });
    });
  });

  test(`TC002: Ticket with and condition, with one condition false`, async () => {
    let attendeeDetails = [];
    attendeeEmail = DataUtl.getRandomAttendeeEmail("zuddl.com");
    attendeeDetails.push({
      firstName: "jxtin",
      lastName: "NOT ME", // false condition, (lastName != me)
      email: attendeeEmail,
    });
    const ticketInfo = {
      ticketName: namedTicketInfo.name,
      quantity: 1,
      price: namedTicketInfo.pricePerTicket,
      coupon: "",
      expectSoldOut: false,
    };

    /*
    form data : // case : first name correct, last name incorrect, country different, lucky number correct expect only email ticket and lucky ticket to be visible
    firstName: "jxtin"
    lastName: "NOT ME"
    email: "random@zuddl.com"
    phone: "1234567890"
    lucky_number: 4 
    country: "Japan"
    */

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.handleTicketForm({
        firstName: attendeeDetails[0].firstName,
        lastName: attendeeDetails[0].lastName,
        email: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "phone number form",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Are you lucky?",
        luckyNumber: "4",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Nationality Check",
        country: "Japan",
      });
    });

    await test.step(`Verifying ticket visibility`, async () => {
      await flowBuilderPage.expectTicket({
        ticketName: namedTicketInfo.name,
        shouldBeVisible: false,
        expectSoldOut: false,
        ticketPrice: namedTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.expectTicket({
        ticketName: emailTicketInfo.name,
        shouldBeVisible: true,
        expectSoldOut: false,
        ticketPrice: emailTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.expectTicket({
        ticketName: luckyTicketInfo.name,
        shouldBeVisible: true,
        expectSoldOut: false,
        ticketPrice: luckyTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.expectTicket({
        ticketName: countryTicketInfo.name,
        shouldBeVisible: false,
        expectSoldOut: false,
        ticketPrice: countryTicketInfo.pricePerTicket,
      });
    });
  });

  test(`TC003: No ticket applicable for user`, async () => {
    let attendeeDetails = [];
    attendeeEmail = DataUtl.getRandomAttendeeEmail("zuddlnot.com");
    attendeeDetails.push({
      firstName: "who", // false condition, (firstName != jxtin)
      lastName: "NOT ME", // false condition, (lastName != me)
      email: attendeeEmail,
    });
    const ticketInfo = {
      ticketName: namedTicketInfo.name,
      quantity: 1,
      price: namedTicketInfo.pricePerTicket,
      coupon: "",
      expectSoldOut: false,
    };

    /*
    form data : // case : first name incorrect, last name incorrect, country different, lucky number incorrect expect no tickets to be visible
    firstName: "who"
    lastName: "NOT ME"
    email: "random@zuddlnot.com"
    phone: "1234567890"
    lucky_number: 3 
    country: "Japan"
    */

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.handleTicketForm({
        firstName: attendeeDetails[0].firstName,
        lastName: attendeeDetails[0].lastName,
        email: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "phone number form",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Are you lucky?",
        luckyNumber: "3",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Nationality Check",
        country: "Japan",
      });
    });

    await test.step(`Verifying ticket visibility ( No tickets should be visible)`, async () => {
      await flowBuilderPage.expectTicket({
        ticketName: namedTicketInfo.name,
        shouldBeVisible: false,
        expectSoldOut: false,
        ticketPrice: namedTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.expectTicket({
        ticketName: emailTicketInfo.name,
        shouldBeVisible: false,
        expectSoldOut: false,
        ticketPrice: emailTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.expectTicket({
        ticketName: luckyTicketInfo.name,
        shouldBeVisible: false,
        expectSoldOut: false,
        ticketPrice: luckyTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.expectTicket({
        ticketName: countryTicketInfo.name,
        shouldBeVisible: false,
        expectSoldOut: false,
        ticketPrice: countryTicketInfo.pricePerTicket,
      });
    });
  });

  test(`TC004: Ticket with OR condition on different fields, email not valid, lucky number condition valid `, async () => {
    let attendeeDetails = [];
    attendeeEmail = DataUtl.getRandomAttendeeEmail("zuddlnot.com");
    attendeeDetails.push({
      firstName: "who", // false condition, (firstName != jxtin)
      lastName: "NOT ME", // false condition, (lastName != me)
      email: attendeeEmail,
    });
    const ticketInfo = {
      ticketName: emailTicketInfo.name,
      quantity: 1,
      price: emailTicketInfo.pricePerTicket,
      coupon: "",
      expectSoldOut: false,
    };

    /*
    form data : // case : first name incorrect, last name incorrect, country different, lucky number incorrect expect no tickets to be visible
    firstName: "who"
    lastName: "NOT ME"
    email: "random@zuddlnot.com"
    phone: "1234567890"
    lucky_number: 3 
    country: "Japan"
    */

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.handleTicketForm({
        firstName: attendeeDetails[0].firstName,
        lastName: attendeeDetails[0].lastName,
        email: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "phone number form",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Are you lucky?",
        luckyNumber: "2",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Nationality Check",
        country: "Japan",
      });
    });

    await test.step(`Verifying ticket visibility ( No tickets should be visible)`, async () => {
      await flowBuilderPage.expectTicket({
        ticketName: namedTicketInfo.name,
        shouldBeVisible: false,
        expectSoldOut: false,
        ticketPrice: namedTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.expectTicket({
        ticketName: emailTicketInfo.name,
        shouldBeVisible: true,
        expectSoldOut: false,
        ticketPrice: emailTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.expectTicket({
        ticketName: luckyTicketInfo.name,
        shouldBeVisible: false,
        expectSoldOut: false,
        ticketPrice: luckyTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.expectTicket({
        ticketName: countryTicketInfo.name,
        shouldBeVisible: false,
        expectSoldOut: false,
        ticketPrice: countryTicketInfo.pricePerTicket,
      });
    });

    await test.step(`User selects email ticket`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: ticketInfo,
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.clickOnContinueButton(); // to deal with "After ticket is chosen"

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Ticket details",
        attendeeDetails: attendeeDetails,
        ticketInfo: ticketInfo,
      });

      await flowBuilderPage.verifyOrderSummaryBulk({
        dataToVerify: {
          attendeeDetails: attendeeDetails,
          ticketName: ticketInfo.ticketName,
        },
      });

      await flowBuilderPage.fillPaymentDetailsFreeTicket({});

      await flowBuilderPage.waitForPaymentConfirmation();

      await flowBuilderPage.verifySuccessFullRegistration({
        userFirstName: attendeeDetails[0].firstName,
        userLastName: attendeeDetails[0].lastName,
        email: attendeeEmail,
      });
    });
  });

  test(`TC005: Bulk ticketing, first user correct, second user correct`, async () => {
    let attendeeDetails = [];
    attendeeEmail = DataUtl.getRandomAttendeeEmail("zuddlnot.com");
    attendeeDetails.push({
      firstName: "who", // false condition, (firstName != jxtin)
      lastName: "NOT ME", // false condition, (lastName != me)
      email: attendeeEmail,
    });

    attendeeDetails.push({
      firstName: "jxtin",
      lastName: "me",
      email: DataUtl.getRandomAttendeeEmail("zuddlnot.com"),
    });

    const ticketInfo = {
      ticketName: emailTicketInfo.name,
      quantity: 2,
      price: emailTicketInfo.pricePerTicket,
      coupon: "",
      expectSoldOut: false,
    };

    /*
    form data : // case : first name incorrect, last name incorrect, country different, lucky number incorrect expect no tickets to be visible
    firstName: "who"
    lastName: "NOT ME"
    email: "random@zuddlnot.com"
    phone: "1234567890"
    lucky_number: 3 
    country: "Japan"
    */

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.handleTicketForm({
        firstName: attendeeDetails[0].firstName,
        lastName: attendeeDetails[0].lastName,
        email: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "phone number form",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Are you lucky?",
        luckyNumber: "2",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Nationality Check",
        country: "Japan",
      });
    });

    await test.step(`Verifying ticket visibility ( No tickets should be visible)`, async () => {
      await flowBuilderPage.expectTicket({
        ticketName: emailTicketInfo.name,
        shouldBeVisible: true,
        expectSoldOut: false,
        ticketPrice: emailTicketInfo.pricePerTicket,
      });
    });

    await test.step(`User selects email ticket`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: ticketInfo,
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.clickOnContinueButton(); // to deal with "After ticket is chosen"

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Ticket details",
        attendeeDetails: attendeeDetails,
        ticketInfo: ticketInfo,
      });

      await flowBuilderPage.fillPhoneNumber("1234567890");
      await flowBuilderPage.clickOnContinueButton();
      await flowBuilderPage.fillAreYouLuckyForm("2");
      await flowBuilderPage.clickOnContinueButton();
      await flowBuilderPage.fillNationalityCheck("India");
      await flowBuilderPage.clickOnContinueButton();
      await flowBuilderPage.clickOnContinueButton();

      await flowBuilderPage.verifyOrderSummaryBulk({
        dataToVerify: {
          attendeeDetails: attendeeDetails,
          ticketName: ticketInfo.ticketName,
        },
      });

      await flowBuilderPage.fillPaymentDetailsFreeTicket({});

      await flowBuilderPage.waitForPaymentConfirmation();

      await flowBuilderPage.verifyBulkTicketingSuccess({
        attendeeDetails: attendeeDetails,
        ticketDetails: ticketInfo,
      });
    });
  });

  test(`TC006: Single ticket, user updates details, ticket should be visible`, async () => {
    let attendeeDetails = [];
    attendeeEmail = DataUtl.getRandomAttendeeEmail("zuddl.com");
    attendeeDetails.push({
      firstName: "jxtin",
      lastName: "me",
      email: attendeeEmail,
    });
    const ticketInfo = {
      ticketName: namedTicketInfo.name,
      quantity: 1,
      price: namedTicketInfo.pricePerTicket,
      coupon: "",
      expectSoldOut: false,
    };

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.handleTicketForm({
        firstName: attendeeDetails[0].firstName,
        lastName: attendeeDetails[0].lastName,
        email: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "phone number form",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Are you lucky?",
        luckyNumber: "4",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Nationality Check",
      });
    });

    await test.step(`Verifying ticket visibility`, async () => {
      await flowBuilderPage.expectTicket({
        ticketName: namedTicketInfo.name,
        shouldBeVisible: true,
        expectSoldOut: false,
        ticketPrice: namedTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.expectTicket({
        ticketName: emailTicketInfo.name,
        shouldBeVisible: true,
        expectSoldOut: false,
        ticketPrice: emailTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.expectTicket({
        ticketName: luckyTicketInfo.name,
        shouldBeVisible: true,
        expectSoldOut: false,
        ticketPrice: luckyTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.expectTicket({
        ticketName: countryTicketInfo.name,
        shouldBeVisible: true,
        expectSoldOut: false,
        ticketPrice: countryTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: ticketInfo,
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.clickOnContinueButton(); // to deal with "After ticket is chosen"

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Ticket details",
        attendeeDetails: attendeeDetails,
        ticketInfo: ticketInfo,
      });

      await flowBuilderPage.editDetailsFlowForm({
        firstName: "newFirstName",
        lastName: "newLastName",
        email: "newEmail@zuddl.com",
      });

      await expect(flowBuilderPage.errorMessage).toBeVisible({
        timeout: 5000,
      });

      await flowBuilderPage.handleTicketForm({
        firstName: attendeeDetails[0].firstName,
        lastName: attendeeDetails[0].lastName,
        email: attendeeEmail,
      });

      let formsLeft = 4;
      for (let i = 0; i < formsLeft; i++) {
        await flowBuilderPage.clickOnContinueButton();
        await flowBuilderPage.page.waitForTimeout(1000);
      }
    });
    await flowBuilderPage.verifyOrderSummaryBulk({
      dataToVerify: {
        attendeeDetails: attendeeDetails,
        ticketName: ticketInfo.ticketName,
      },
    });

    await flowBuilderPage.fillPaymentDetailsFreeTicket({});

    await flowBuilderPage.waitForPaymentConfirmation();

    await flowBuilderPage.verifySuccessFullRegistration({
      userFirstName: attendeeDetails[0].firstName,
      userLastName: attendeeDetails[0].lastName,
      email: attendeeEmail,
    });
  });

  test(`TC007: Single ticket, user selects a ticket, then updates details, chooses another ticket`, async () => {
    let attendeeDetails = [];
    attendeeEmail = DataUtl.getRandomAttendeeEmail("zuddlnot.com");
    attendeeDetails.push({
      firstName: "who", // false condition, (firstName != jxtin)
      lastName: "NOT ME", // false condition, (lastName != me)
      email: attendeeEmail,
    });
    const ticketInfo = {
      ticketName: emailTicketInfo.name,
      quantity: 1,
      price: emailTicketInfo.pricePerTicket,
      coupon: "",
      expectSoldOut: false,
    };

    /*
    form data : // case : first name incorrect, last name incorrect, country different, lucky number incorrect expect no tickets to be visible
    firstName: "who"
    lastName: "NOT ME"
    email: "random@zuddlnot.com"
    phone: "1234567890"
    lucky_number: 3 
    country: "Japan"
    */

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.handleTicketForm({
        firstName: attendeeDetails[0].firstName,
        lastName: attendeeDetails[0].lastName,
        email: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "phone number form",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Are you lucky?",
        luckyNumber: "2",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Nationality Check",
        country: "india",
      });
    });

    await test.step(`Verifying ticket visibility ( No tickets should be visible)`, async () => {
      await flowBuilderPage.expectTicket({
        ticketName: namedTicketInfo.name,
        shouldBeVisible: false,
        expectSoldOut: false,
        ticketPrice: namedTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.expectTicket({
        ticketName: emailTicketInfo.name,
        shouldBeVisible: true,
        expectSoldOut: false,
        ticketPrice: emailTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.expectTicket({
        ticketName: luckyTicketInfo.name,
        shouldBeVisible: false,
        expectSoldOut: false,
        ticketPrice: luckyTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.expectTicket({
        ticketName: countryTicketInfo.name,
        shouldBeVisible: true,
        expectSoldOut: false,
        ticketPrice: countryTicketInfo.pricePerTicket,
      });
    });

    await test.step(`User selects email ticket`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: ticketInfo,
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.clickOnContinueButton(); // to deal with "After ticket is chosen"

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Ticket details",
        attendeeDetails: attendeeDetails,
        ticketInfo: ticketInfo,
      });
      await flowBuilderPage.editDetailsFlowForm({
        firstName: "jxtin",
        lastName: "me",
        email: attendeeEmail,
      });

      // await flowBuilderPage.handleTicketForm({
      //   firstName: "jxtin",
      //   lastName: "me",
      //   email: attendeeEmail,
      // });

      let formsLeft = 4;
      for (let i = 0; i < formsLeft; i++) {
        await flowBuilderPage.clickOnContinueButton();
        await flowBuilderPage.page.waitForTimeout(1000);
      }

      await flowBuilderPage.verifyOrderSummary({
        dataToVerify: {
          firstName: "jxtin",
          lastName: "me",
          email: attendeeEmail,
          ticketName: ticketInfo.ticketName,
        },
        editTicket: true,
        newTicket: {
          ticketName: namedTicketInfo.name,
          quantity: 1,
          expectSoldOut: false,
        },
      });
    });

    attendeeDetails = [];
    attendeeDetails.push({
      firstName: "jxtin",
      lastName: "me",
      email: attendeeEmail,
    });

    await flowBuilderPage.fillPaymentDetailsFreeTicket({});

    await flowBuilderPage.waitForPaymentConfirmation();

    await flowBuilderPage.verifyBulkTicketingSuccess({
      attendeeDetails: attendeeDetails,
      ticketDetails: ticketInfo,
    });
  });

  test(`TC008: Bulk ticketing, first user correct, second user incorrect`, async () => {
    let attendeeDetails = [];
    attendeeEmail = DataUtl.getRandomAttendeeEmail("zuddlnot.com");
    attendeeDetails.push({
      firstName: "who", // false condition, (firstName != jxtin)
      lastName: "NOT ME", // false condition, (lastName != me)
      email: attendeeEmail,
    });

    attendeeDetails.push({
      firstName: "jxtin",
      lastName: "lmao",
      email: DataUtl.getRandomAttendeeEmail("zuddddlnot.com"),
    });

    const ticketInfo = {
      ticketName: emailTicketInfo.name,
      quantity: 2,
      price: emailTicketInfo.pricePerTicket,
      coupon: "",
      expectSoldOut: false,
    };

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.handleTicketForm({
        firstName: attendeeDetails[0].firstName,
        lastName: attendeeDetails[0].lastName,
        email: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "phone number form",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Are you lucky?",
        luckyNumber: "2",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Nationality Check",
        country: "india",
      });
    });

    await test.step(`Name ticket should be visible`, async () => {
      await flowBuilderPage.expectTicket({
        ticketName: emailTicketInfo.name,
        shouldBeVisible: true,
        expectSoldOut: false,
        ticketPrice: emailTicketInfo.pricePerTicket,
      });
    });

    await test.step(`User selects email ticket`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: ticketInfo,
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.clickOnContinueButton(); // to deal with "After ticket is chosen"

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Ticket details",
        attendeeDetails: attendeeDetails,
        ticketInfo: ticketInfo,
      });

      await flowBuilderPage.fillPhoneNumber("1234567890");
      await flowBuilderPage.clickOnContinueButton();
      await flowBuilderPage.fillAreYouLuckyForm("6");
      await flowBuilderPage.clickOnContinueButton();
      await flowBuilderPage.fillNationalityCheck("india");
      await flowBuilderPage.clickOnContinueButton();
      await expect(flowBuilderPage.errorBanner).toBeVisible({
        timeout: 5000,
      });

      await flowBuilderPage.switchTicket({
        ticketName: countryTicketInfo.name,
        quantity: 2,
        price: countryTicketInfo.pricePerTicket,
      });

      await flowBuilderPage.clickOnContinueButton();

      await flowBuilderPage.clickOnContinueButton();

      for (let i = 0; i < 5; i++) {
        await flowBuilderPage.clickOnContinueButton();
        console.log(i);
        await flowBuilderPage.page.waitForTimeout(1000);
      }

      await flowBuilderPage.verifyOrderSummaryBulk({
        dataToVerify: {
          attendeeDetails: attendeeDetails,
          ticketName: countryTicketInfo.name,
        },
      });
      await flowBuilderPage.fillPaymentDetailsFreeTicket({});

      await flowBuilderPage.waitForPaymentConfirmation();

      await flowBuilderPage.verifyBulkTicketingSuccess({
        attendeeDetails: attendeeDetails,
        ticketDetails: {
          ticketName: countryTicketInfo.name,
        },
      });
    });
  });

  test(`TC009: Single ticket, hidden ticket, necessary condition : country = russian`, async () => {
    let attendeeDetails = [];
    attendeeEmail = DataUtl.getRandomAttendeeEmail("zuddlnot.com");
    attendeeDetails.push({
      firstName: "who", // false condition, (firstName != jxtin)
      lastName: "NOT ME", // false condition, (lastName != me)
      email: attendeeEmail,
    });
    const ticketInfo = {
      ticketName: hiddenTicketInfo.name,
      quantity: 1,
      price: hiddenTicketInfo.pricePerTicket,
      expectSoldOut: false,
      coupon: "",
    };

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.handleTicketForm({
        firstName: attendeeDetails[0].firstName,
        lastName: attendeeDetails[0].lastName,
        email: attendeeEmail,
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "phone number form",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Are you lucky?",
        luckyNumber: "2",
      });

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Nationality Check",
        country: "russia",
      });

      await flowBuilderPage.checkTicketNotPresent(hiddenTicketInfo.name);

      await flowBuilderPage.applyCouponCode(hideCoupon);

      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: ticketInfo,
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.clickOnContinueButton();

      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Ticket details",
        attendeeDetails: attendeeDetails,
        ticketInfo: ticketInfo,
      });

      await flowBuilderPage.verifyOrderSummaryBulk({
        dataToVerify: {
          attendeeDetails: attendeeDetails,
          ticketName: ticketInfo.ticketName,
        },
      });

      await flowBuilderPage.fillPaymentDetailsFreeTicket({});
      await flowBuilderPage.waitForPaymentConfirmation();
      await flowBuilderPage.verifySuccessFullRegistration({
        userFirstName: attendeeDetails[0].firstName,
        userLastName: attendeeDetails[0].lastName,
        email: attendeeEmail,
      });
    });
  });
});
