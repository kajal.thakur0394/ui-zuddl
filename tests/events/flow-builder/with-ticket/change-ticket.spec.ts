import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../../controller/EventController";
import { FlowBuilderController } from "../../../../controller/FlowBuilderController";
import { EventInfoDTO } from "../../../../dto/eventInfoDto";
import EventSettingID from "../../../../enums/EventSettingEnum";
import { FlowStatus, FlowType } from "../../../../enums/FlowTypeEnum";
import EventRole from "../../../../enums/eventRoleEnum";
import EventType from "../../../../enums/eventTypeEnum";
import { publishDataPayload } from "../../../../interfaces/FlowBuilerControllerInterface";
import { FlowBuilderPage } from "../../../../page-objects/flow-builder/flow-ticketing";
import BrowserFactory from "../../../../util/BrowserFactory";
import {
  createNewEvent,
  updateEventLandingPageDetails,
  updateEventType,
} from "../../../../util/apiUtil";
import { DataUtl } from "../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../util/email-validation-api-util";
import { TicketingController } from "../../../../controller/TicketingController";
import { fetchMailasaurEmailObject } from "../../../../util/emailUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@flow-builder @registraion-with-ticket @invite-list`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId: any;
  let attendeeBrowserContext: BrowserContext;
  let eventController: EventController;
  let flowBuilderPage: FlowBuilderPage;
  let flowBuilderController: FlowBuilderController;
  let publishdataPayLoad: publishDataPayload;
  let attendeePage: Page;
  let attendeeEmail: string;
  const userFirstName = "Jxtin";
  const userLastName = "zuddl";
  let eventInfoDto: EventInfoDTO;
  const hideCoupon = DataUtl.getRandomCouponName();
  let ticketId1: string;
  let ticketId2: string;
  let ticketId3: string;
  let ticketName1: string;
  let ticketName2: string;
  let ticketName3: string;
  const eventTitle = "flow-with-taf-invitelist automation";

  test.beforeAll(async () => {
    await test.step(`setup browser contexts`, async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: true,
      });
      orgApiContext = orgBrowserContext.request;
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage = await attendeeBrowserContext.newPage();
    });

    await test.step(`create an event with FLEX as resgistration type`, async () => {
      eventId = await createNewEvent({
        api_request_context: orgApiContext,
        event_title: eventTitle,
        isFlex: true,
      });
      console.log(`event id ${eventId}`);
    });

    await test.step(`update an event with IN-PERSON as event type`, async () => {
      await updateEventType(orgApiContext, eventId, EventType.IN_PERSON);
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        isMagicLinkEnabled: true,
      });
    });

    await test.step(`Initialise event controller`, async () => {
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step(`Get event details`, async () => {
      let eventInfo = await (await eventController.getEventInfo()).json();
      console.log("Event Info: ", eventInfo);
      eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
      eventInfoDto.startDateTime = eventInfo.startDateTime;
    });

    await enableDisableEmailTrigger(
      orgApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );

    await test.step(`create tickets`, async () => {
      let ticketInfo1 = {
        name: "TKT_1",
        description: "desc",
        numberOfTickets: 10,
        pricePerTicket: 0,
        isBulkPurchaseEnabled: false,
        soldTickets: 0,
        isAvailableForAll: true,
      };

      let ticketInfo2 = {
        name: "TKT_2",
        description: "desc 2",
        numberOfTickets: 10,
        pricePerTicket: 0,
        minTicketsPerOrder: 2,
        maxTicketsPerOrder: 3,
        isBulkPurchaseEnabled: true,
        isHide: false,
        isAvailableForAll: true,
      };

      let ticketInfo3 = {
        name: "TKT_3",
        description: "desc 3",
        numberOfTickets: 10,
        pricePerTicket: 0,
        isBulkPurchaseEnabled: false,
        isHide: false,
        isAvailableForAll: true,
      };

      ticketName1 = ticketInfo1.name;
      ticketName2 = ticketInfo2.name;
      ticketName3 = ticketInfo3.name;
      let ticketingController = new TicketingController(orgApiContext, eventId);

      await ticketingController.selectGateway();

      ticketId1 = (await ticketingController.createTicket(ticketInfo1))[
        "ticketTypeId"
      ];
      ticketId2 = (await ticketingController.createTicket(ticketInfo2))[
        "ticketTypeId"
      ];
      ticketId3 = (await ticketingController.createTicket(ticketInfo3))[
        "ticketTypeId"
      ];
    });

    await test.step(`Initialise flowbuilder controller`, async () => {
      flowBuilderController = new FlowBuilderController(orgApiContext, eventId);
    });
    await test.step(`make publish flow test data`, async () => {
      let flowDraftId = await flowBuilderController.getFlowId(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
      publishdataPayLoad = {
        flowId: "",
        flowDraftId: flowDraftId,
        clientFlowDraftVersion: 2,
      };
    });

    await test.step(`update default flow steps with existing expected steps`, async () => {
      await flowBuilderController.generateTicketingFirstStep(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
    });

    await test.step(`now, publish the flow`, async () => {
      await flowBuilderController.publishFlowData(publishdataPayLoad);
      await attendeePage.waitForTimeout(5000);
    });

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
  });

  test.beforeEach(async () => {
    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
  });

  test.afterEach(async () => {
    await attendeePage.reload();
    // clear the cookies
    await attendeePage.context().clearCookies();
    await attendeePage.evaluate(() => {
      localStorage.clear();
    });
  });

  test.afterAll(async () => {
    await orgBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test(`TC001 : @flow-builder @registraion-with-ticket  Purchaser changes own details including email`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-795/verify-ticket-purchaser-can-reassigned-the-ticket",
    });

    let attendeeDetails = [];
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
    const ticketInfo = {
      ticketName: "TKT_1",
      quantity: 1,
      price: 0,
      coupon: "",
      expectSoldOut: false,
    };

    const ticketQty = ticketInfo.quantity;
    attendeeDetails.push({
      firstName: userFirstName,
      lastName: userLastName,
      email: attendeeEmail,
    });

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName1,
          quantity: ticketQty,
          price: 0,
          coupon: "",
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.handleTicketForm({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });
    });
    await test.step(`Complete the registration`, async () => {
      for (let i = 1; i < ticketQty; i++) {
        attendeeDetails.push({
          firstName: DataUtl.getRandomName(),
          lastName: DataUtl.getRandomName(),
          email: DataUtl.getRandomAttendeeEmail(),
        });
      }
      await flowBuilderPage.clickOnContinueButton();
      await flowBuilderPage.fillPaymentDetailsFreeTicket({});
      await flowBuilderPage.page.waitForTimeout(2000);
      await flowBuilderPage.verifyBulkTicketingSuccess({
        attendeeDetails: attendeeDetails,
        ticketDetails: {
          ticketName: ticketName1,
        },
      });
      await flowBuilderPage.waitForPaymentConfirmation();
    });

    await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
      await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
        attendeeEmail,
        eventId
      );
    });

    await test.step(`Change ticket details`, async () => {
      await flowBuilderPage.verifySuccessFullRegistration({
        userLastName: userLastName,
        userFirstName: userFirstName,
        email: attendeeEmail,
      });
      await flowBuilderPage.page.waitForTimeout(2000);

      let editDetailsURL = await flowBuilderPage.getEditDetailsURL(eventId);
      flowBuilderPage.page.goto(editDetailsURL);
      await flowBuilderPage.page.waitForTimeout(2000);

      // get otp from metabase

      const otpFetched = await QueryUtil.fetchLoginOTPForEvent(
        attendeeEmail,
        eventId
      );

      await flowBuilderPage.fillOtpAndLogin(otpFetched);

      await flowBuilderPage.page.waitForTimeout(5000);

      await flowBuilderPage.verifyAttendeeDetails({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
        ticketName: ticketName1,
        tktPrice: `$${0}`,
      });

      let updatedUserFirstName = DataUtl.getRandomName();
      let updatedUserLastName = DataUtl.getRandomName();
      let updatedAttendeeEmail = DataUtl.getRandomAttendeeEmail();

      await flowBuilderPage.editDetails({
        firstName: updatedUserFirstName,
        lastName: updatedUserLastName,
        email: updatedAttendeeEmail,
      });

      await flowBuilderPage.page.waitForTimeout(2000);

      // verify the updated details
      await flowBuilderPage.verifyAttendeeDetails({
        firstName: updatedUserFirstName,
        lastName: updatedUserLastName,
        email: updatedAttendeeEmail,
        ticketName: ticketName1,
        tktPrice: `$${0}`,
      });

      // verify the updated details in DB
      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          updatedAttendeeEmail,
          eventId
        );
      });
      await test.step(`Now fetching the user entry from event_registration table and get the ticket`, async () => {
        const userRegDataFromDb =
          await QueryUtil.fetchUserEventRegistrationDataFromDb(
            eventId,
            updatedAttendeeEmail
          );
        console.log(userRegDataFromDb);

        expect(userRegDataFromDb["first_name"]).toBe(updatedUserFirstName);
        expect(userRegDataFromDb["last_name"]).toBe(updatedUserLastName);
      });
    });
  });

  test(`TC002 : @flow-builder @registraion-with-ticket @bulk-ticketing Purchasers tries changing details from link in email`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-795/verify-ticket-purchaser-can-reassigned-the-ticket",
    });

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-793/verify-user-should-be-able-to-modify-the-details-filled-by-ticket",
    });

    let attendeeDetails = [];
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
    const ticketInfo = {
      ticketName: "TKT_2",
      quantity: 2,
      price: 0,
      coupon: "",
      expectSoldOut: false,
    };

    const ticketQty = ticketInfo.quantity;
    attendeeDetails.push({
      firstName: userFirstName,
      lastName: userLastName,
      email: attendeeEmail,
    });
    for (let i = 1; i < ticketQty; i++) {
      attendeeDetails.push({
        firstName: DataUtl.getRandomName(),
        lastName: DataUtl.getRandomName(),
        email: DataUtl.getRandomAttendeeEmail(),
      });
    }

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName2,
          quantity: ticketQty,
          price: 0,
          coupon: "",
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.handleTicketForm({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });
    });
    await test.step(`Complete the registration`, async () => {
      for (let i = 1; i < ticketQty; i++) {
        attendeeDetails.push({
          firstName: DataUtl.getRandomName(),
          lastName: DataUtl.getRandomName(),
          email: DataUtl.getRandomAttendeeEmail(),
        });
      }
      await flowBuilderPage.handleTicketForm({
        firstName: attendeeDetails[1].firstName,
        lastName: attendeeDetails[1].lastName,
        email: attendeeDetails[1].email,
      });
      await flowBuilderPage.clickOnContinueButton();
      await flowBuilderPage.fillPaymentDetailsFreeTicket({});
      await flowBuilderPage.page.waitForTimeout(2000);
      await flowBuilderPage.verifyBulkTicketingSuccess({
        attendeeDetails: attendeeDetails,
        ticketDetails: {
          ticketName: ticketName1,
        },
      });
      await flowBuilderPage.waitForPaymentConfirmation();
    });

    await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
      await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
        attendeeEmail,
        eventId
      );
    });

    await test.step(`Change ticket details`, async () => {
      await flowBuilderPage.verifySuccessFullRegistration({
        userLastName: userLastName,
        userFirstName: userFirstName,
        email: attendeeEmail,
      });
      eventInfoDto = new EventInfoDTO(eventId, eventTitle);
      await flowBuilderPage.page.waitForTimeout(2000);
      let fetched_email = await fetchMailasaurEmailObject(
        attendeeDetails[0].email,
        eventInfoDto.registrationConfirmationEmailSubj
      );
      console.log(fetched_email);
      console.log(fetched_email.html);

      let editRegistrationLink = fetched_email.html.links[4]["href"];

      flowBuilderPage.page.goto(editRegistrationLink);

      const otpFetched = await QueryUtil.fetchLoginOTPForEvent(
        attendeeDetails[0].email,
        eventId
      );

      await flowBuilderPage.fillOtpAndLogin(otpFetched);

      await flowBuilderPage.page.waitForTimeout(5000);

      await flowBuilderPage.verifyDetailsOnModifyLink({
        firstName: attendeeDetails[0].firstName,
        lastName: attendeeDetails[0].lastName,
        email: attendeeDetails[0].email,
      });

      let newFirstName = DataUtl.getRandomName();
      let newLastName = DataUtl.getRandomName();

      await flowBuilderPage.modifyDetails({
        firstName: newFirstName,
        lastName: newLastName,
      });

      await flowBuilderPage.page.waitForTimeout(2000);

      // verify updated details in db

      await test.step(`Now fetching the user entry from event_registration table and get the ticket`, async () => {
        const userRegDataFromDb =
          await QueryUtil.fetchUserEventRegistrationDataFromDb(
            eventId,
            attendeeDetails[0].email
          );
        console.log(userRegDataFromDb);

        expect(userRegDataFromDb["first_name"]).toBe(newFirstName);
        expect(userRegDataFromDb["last_name"]).toBe(newLastName);
      });
    });
  });

  test(`TC003 : @flow-builder @registraion-with-ticket @bulk-ticketing Secondary user tries changing details from link in email`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-793/verify-user-should-be-able-to-modify-the-details-filled-by-ticket",
    });

    let attendeeDetails = [];
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
    const ticketInfo = {
      ticketName: "TKT_2",
      quantity: 2,
      price: 0,
      coupon: "",
      expectSoldOut: false,
    };

    const ticketQty = ticketInfo.quantity;
    attendeeDetails.push({
      firstName: userFirstName,
      lastName: userLastName,
      email: attendeeEmail,
    });
    for (let i = 1; i < ticketQty; i++) {
      attendeeDetails.push({
        firstName: DataUtl.getRandomName(),
        lastName: DataUtl.getRandomName(),
        email: DataUtl.getRandomAttendeeEmail(),
      });
    }

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName2,
          quantity: ticketQty,
          price: 0,
          coupon: "",
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.handleTicketForm({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });
    });
    await test.step(`Complete the registration`, async () => {
      for (let i = 1; i < ticketQty; i++) {
        attendeeDetails.push({
          firstName: DataUtl.getRandomName(),
          lastName: DataUtl.getRandomName(),
          email: DataUtl.getRandomAttendeeEmail(),
        });
      }
      await flowBuilderPage.handleTicketForm({
        firstName: attendeeDetails[1].firstName,
        lastName: attendeeDetails[1].lastName,
        email: attendeeDetails[1].email,
      });
      await flowBuilderPage.clickOnContinueButton();
      await flowBuilderPage.fillPaymentDetailsFreeTicket({});
      await flowBuilderPage.page.waitForTimeout(2000);
      await flowBuilderPage.verifyBulkTicketingSuccess({
        attendeeDetails: attendeeDetails,
        ticketDetails: {
          ticketName: ticketName1,
        },
      });
      await flowBuilderPage.waitForPaymentConfirmation();
    });

    await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
      await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
        attendeeEmail,
        eventId
      );
    });

    await test.step(`Change ticket details`, async () => {
      await flowBuilderPage.verifySuccessFullRegistration({
        userLastName: userLastName,
        userFirstName: userFirstName,
        email: attendeeEmail,
      });
      eventInfoDto = new EventInfoDTO(eventId, eventTitle);
      await flowBuilderPage.page.waitForTimeout(2000);
      let fetched_email = await fetchMailasaurEmailObject(
        attendeeDetails[1].email,
        eventInfoDto.registrationConfirmationEmailSubj
      );
      console.log(fetched_email);
      console.log(fetched_email.html);

      let editRegistrationLink = fetched_email.html.links[4]["href"];

      flowBuilderPage.page.goto(editRegistrationLink);

      const otpFetched = await QueryUtil.fetchLoginOTPForEvent(
        attendeeDetails[1].email,
        eventId
      );

      await flowBuilderPage.fillOtpAndLogin(otpFetched);

      await flowBuilderPage.page.waitForTimeout(5000);

      await flowBuilderPage.verifyDetailsOnModifyLink({
        firstName: attendeeDetails[1].firstName,
        lastName: attendeeDetails[1].lastName,
        email: attendeeDetails[1].email,
      });

      let newFirstName = DataUtl.getRandomName();
      let newLastName = DataUtl.getRandomName();

      await flowBuilderPage.modifyDetails({
        firstName: newFirstName,
        lastName: newLastName,
      });

      await flowBuilderPage.page.waitForTimeout(2000);

      // verify updated details in db

      await test.step(`Now fetching the user entry from event_registration table and get the ticket`, async () => {
        const userRegDataFromDb =
          await QueryUtil.fetchUserEventRegistrationDataFromDb(
            eventId,
            attendeeDetails[1].email
          );
        console.log(userRegDataFromDb);

        expect(userRegDataFromDb["first_name"]).toBe(newFirstName);
        expect(userRegDataFromDb["last_name"]).toBe(newLastName);
      });
    });
  });
});
