import {
  APIRequestContext,
  BrowserContext,
  Page,
  test,
} from "@playwright/test";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";
import { EventController } from "../../../../controller/EventController";
import { FlowBuilderController } from "../../../../controller/FlowBuilderController";
import { TicketingController } from "../../../../controller/TicketingController";
import { EventInfoDTO } from "../../../../dto/eventInfoDto";
import EventSettingID from "../../../../enums/EventSettingEnum";
import { FlowStatus, FlowType } from "../../../../enums/FlowTypeEnum";
import EventRole from "../../../../enums/eventRoleEnum";
import EventType from "../../../../enums/eventTypeEnum";
import { publishDataPayload } from "../../../../interfaces/FlowBuilerControllerInterface";
import { FlowBuilderPage } from "../../../../page-objects/flow-builder/flow-ticketing";
import BrowserFactory from "../../../../util/BrowserFactory";
import {
  createNewEvent,
  updateEventLandingPageDetails,
  updateEventType,
} from "../../../../util/apiUtil";
import { DataUtl } from "../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../util/email-validation-api-util";

test.describe(`@flow-builder @registraion-with-ticket @invite-list`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId: any;
  let attendeeBrowserContext: BrowserContext;
  let eventController: EventController;
  let flowBuilderPage: FlowBuilderPage;
  let flowBuilderController: FlowBuilderController;
  let publishdataPayLoad: publishDataPayload;
  let attendeePage: Page;
  let attendeeEmail: string;
  const userFirstName = "Jxtin";
  const userLastName = "zuddl";
  let eventInfoDto: EventInfoDTO;
  const hideCoupon = DataUtl.getRandomCouponName();
  let ticketId1: string;
  let ticketId2: string;
  let ticketId3: string;
  let ticketName1: string;
  let ticketName2: string;
  let ticketName3: string;
  let lockedTicketId: string;
  let lockedTicketName: string;
  let hiddenTicketId: string;
  let hiddenTicketName: string;
  const lockCoupon = "UNLOCK";

  const eventTitle = `${Date.now()}-${process.env.run_env}`;

  test.beforeAll(async () => {
    await test.step(`setup browser contexts`, async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: true,
      });
      orgApiContext = orgBrowserContext.request;
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage = await attendeeBrowserContext.newPage();
    });

    await test.step(`create an event with FLEX as resgistration type`, async () => {
      eventId = await createNewEvent({
        api_request_context: orgApiContext,
        event_title: eventTitle,
        isFlex: true,
      });
      console.log(`event id ${eventId}`);
    });

    await test.step(`update an event with IN-PERSON as event type`, async () => {
      await updateEventType(orgApiContext, eventId, EventType.IN_PERSON);
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        isMagicLinkEnabled: true,
      });
    });

    await test.step(`Initialise event controller`, async () => {
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step(`Get event details`, async () => {
      let eventInfo = await (await eventController.getEventInfo()).json();
      console.log("Event Info: ", eventInfo);
      eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
      eventInfoDto.startDateTime = eventInfo.startDateTime;
    });

    await enableDisableEmailTrigger(
      orgApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );

    await test.step(`create tickets`, async () => {
      let ticketInfo1 = {
        name: "TKT_1",
        description: "desc",
        numberOfTickets: 10,
        pricePerTicket: 5,
        isBulkPurchaseEnabled: true,
        minTicketsPerOrder: 2,
        maxTicketsPerOrder: 3,
        soldTickets: 0,
        isAvailableForAll: true,
      };

      let ticketInfo2 = {
        name: "TKT_2",
        description: "desc 2",
        numberOfTickets: 10,
        pricePerTicket: 0,
        minTicketsPerOrder: 2,
        maxTicketsPerOrder: 3,
        isBulkPurchaseEnabled: true,
        isHide: false,
        isAvailableForAll: true,
      };

      let ticketInfo3 = {
        name: "TKT_3",
        description: "desc 3",
        numberOfTickets: 10,
        pricePerTicket: 10,
        minTicketsPerOrder: 2,
        maxTicketsPerOrder: 3,
        isBulkPurchaseEnabled: true,
        isHide: false,
        isAvailableForAll: true,
      };

      let lockedTicketInfo = {
        name: "LOCKED_TKT",
        description: "LOCKED",
        numberOfTickets: 5,
        pricePerTicket: 15,
        isBulkPurchaseEnabled: false,
        isHide: false,
        isAvailableForAll: true,
        isLocked: true,
      };

      let hiddenTicketInfo = {
        name: "HIDDEN_TKT",
        description: "HIDDEN",
        numberOfTickets: 5,
        pricePerTicket: 15,
        isBulkPurchaseEnabled: false,
        isHide: true,
        isAvailableForAll: true,
      };

      ticketName1 = ticketInfo1.name;
      ticketName2 = ticketInfo2.name;
      ticketName3 = ticketInfo3.name;
      lockedTicketName = lockedTicketInfo.name;
      hiddenTicketName = hiddenTicketInfo.name;

      let ticketingController = new TicketingController(orgApiContext, eventId);

      await ticketingController.selectGateway();

      ticketId1 = (await ticketingController.createTicket(ticketInfo1))[
        "ticketTypeId"
      ];
      ticketId2 = (await ticketingController.createTicket(ticketInfo2))[
        "ticketTypeId"
      ];
      ticketId3 = (await ticketingController.createTicket(ticketInfo3))[
        "ticketTypeId"
      ];
      lockedTicketId = (
        await ticketingController.createTicket(lockedTicketInfo)
      )["ticketTypeId"];
      hiddenTicketId = (
        await ticketingController.createTicket(hiddenTicketInfo)
      )["ticketTypeId"];
    });

    await test.step(`Create Add-on`, async () => {
      let freeAddOnInfo = {
        name: "ADD_ON_1",
        description: "desc",
        numberOfTickets: 10,
        pricePerTicket: 0,
        isBulkPurchaseEnabled: false,
        soldTickets: 0,
        isAvailableForAll: true,
        ticketTagType: "ADD_ON",
        linkedTicketTypeIds: [ticketId1, ticketId2],
      };

      let paidAddOnInfo = {
        name: "ADD_ON_2",
        description: "desc",
        numberOfTickets: 10,
        pricePerTicket: 10,
        isBulkPurchaseEnabled: false,
        soldTickets: 0,
        isAvailableForAll: true,
        ticketTagType: "ADD_ON",
        linkedTicketTypeIds: [ticketId1, ticketId3, hiddenTicketId],
      };

      let lockedAddOnInfo = {
        name: "LOCKED_ADD_ON",
        description: "desc",
        numberOfTickets: 10,
        pricePerTicket: 10,
        isBulkPurchaseEnabled: false,
        soldTickets: 0,
        isAvailableForAll: true,
        ticketTagType: "ADD_ON",
        linkedTicketTypeIds: [lockedTicketId],
      };
      let ticketingController = new TicketingController(orgApiContext, eventId);
      await ticketingController.createTicket(freeAddOnInfo);
      await ticketingController.createTicket(paidAddOnInfo);
      await ticketingController.createTicket(lockedAddOnInfo);

      await ticketingController.addCoupons({
        couponCode: lockCoupon,
        active: true,
        singleUsage: false,
        ticketTypeIds: [lockedTicketId, hiddenTicketId],
        isAvailableForAllTickets: false,
        isAvailableForEveryone: true,
        maxUsageCount: 10,
      });
    });

    await test.step(`Initialise flowbuilder controller`, async () => {
      flowBuilderController = new FlowBuilderController(orgApiContext, eventId);
    });
    await test.step(`make publish flow test data`, async () => {
      let flowDraftId = await flowBuilderController.getFlowId(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
      publishdataPayLoad = {
        flowId: "",
        flowDraftId: flowDraftId,
        clientFlowDraftVersion: 2,
      };
    });

    await test.step(`update default flow steps with existing expected steps`, async () => {
      await flowBuilderController.generateTicketingFirstStep(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
    });

    await test.step(`now, publish the flow`, async () => {
      await flowBuilderController.publishFlowData(publishdataPayLoad);
      await attendeePage.waitForTimeout(5000);
    });

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
  });

  test.beforeEach(async () => {
    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
  });

  test.afterEach(async () => {
    await attendeePage.reload();
    // clear the cookies
    await attendeePage.context().clearCookies();
    await attendeePage.evaluate(() => {
      localStorage.clear();
    });
  });

  test.afterAll(async () => {
    await orgBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test(`TC001 : @flow-builder @registraion-with-ticket @addon @bulk 3 ticket + free addon`, async () => {
    let attendeeDetails = [];
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
    const ticketInfo = {
      ticketName: "TKT_2",
      quantity: 3,
      price: 0,
      coupon: "",
      expectSoldOut: false,
    };

    const ticketQty = ticketInfo.quantity;
    attendeeDetails.push({
      firstName: userFirstName,
      lastName: userLastName,
      email: attendeeEmail,
    });

    for (let i = 1; i < ticketQty; i++) {
      attendeeDetails.push({
        firstName: DataUtl.getRandomName(),
        lastName: DataUtl.getRandomName(),
        email: DataUtl.getRandomAttendeeEmail(),
      });
    }

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketInfo.ticketName,
          quantity: ticketQty,
          price: ticketInfo.price,
          coupon: "",
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });
      for (let i = 0; i < ticketQty; i++) {
        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: [attendeeDetails[i]],
          ticketAsFirstStep: true,
        });
      }
    });

    await test.step(`Complete the registration`, async () => {
      await flowBuilderPage.getAddonDiv("ADD_ON_2", false);
      await flowBuilderPage.getAddonDiv("LOCKED_ADD_ON", false);

      await flowBuilderPage.selectAddOns([
        {
          addOnName: "ADD_ON_1",
          quantity: 3,
          price: 0,
        },
      ]);
      await flowBuilderPage.clickOnContinueButton();
      await flowBuilderPage.page.waitForTimeout(2000);
      await flowBuilderPage.fillPaymentDetailsFreeTicket({});
      await flowBuilderPage.waitForPaymentConfirmation();
      await flowBuilderPage.verifyBulkTicketingSuccess({
        attendeeDetails: attendeeDetails,
        ticketDetails: {
          ticketName: ticketInfo.ticketName,
        },
        hasAddOns: true,
        addOnDetails: [
          {
            addOnName: "ADD_ON_1",
            quantity: 3,
            price: 0,
          },
        ],
      });
      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeEmail,
          eventId
        );
      });
    });
  });

  test(`TC002 : @flow-builder @registraion-with-ticket @addon @bulk 3 ticket + paid addon`, async () => {
    let attendeeDetails = [];
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
    const ticketInfo = {
      ticketName: "TKT_3",
      quantity: 3,
      price: 10,
      coupon: "",
      expectSoldOut: false,
    };

    const ticketQty = ticketInfo.quantity;
    attendeeDetails.push({
      firstName: userFirstName,
      lastName: userLastName,
      email: attendeeEmail,
    });

    for (let i = 1; i < ticketQty; i++) {
      attendeeDetails.push({
        firstName: DataUtl.getRandomName(),
        lastName: DataUtl.getRandomName(),
        email: DataUtl.getRandomAttendeeEmail(),
      });
    }

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketInfo.ticketName,
          quantity: ticketQty,
          price: ticketInfo.price,
          coupon: "",
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });
      for (let i = 0; i < ticketQty; i++) {
        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: [attendeeDetails[i]],
          ticketAsFirstStep: true,
        });
      }
    });

    await test.step(`Complete the registration`, async () => {
      await flowBuilderPage.selectAddOns([
        {
          addOnName: "ADD_ON_2",
          quantity: 3,
          price: 10,
        },
      ]);
      await flowBuilderPage.clickOnContinueButton();
      await flowBuilderPage.page.waitForTimeout(2000);
      await flowBuilderPage.fillPaymentDetailsPaidTicket({});
      await flowBuilderPage.waitForPaymentConfirmation();
      await flowBuilderPage.verifyBulkTicketingSuccess({
        attendeeDetails: attendeeDetails,
        ticketDetails: {
          ticketName: ticketInfo.ticketName,
        },
        hasAddOns: true,
        addOnDetails: [
          {
            addOnName: "ADD_ON_2",
            quantity: 3,
            price: 10,
          },
        ],
      });
      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeEmail,
          eventId
        );
      });
    });
  });

  test(`TC003 : @flow-builder @registraion-with-ticket @addon @bulk 3 ticket but different addons`, async () => {
    let attendeeDetails = [];
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
    const ticketInfo = {
      ticketName: ticketName1,
      quantity: 3,
      price: 5,
      coupon: "",
      expectSoldOut: false,
    };

    const ticketQty = ticketInfo.quantity;
    attendeeDetails.push({
      firstName: userFirstName,
      lastName: userLastName,
      email: attendeeEmail,
    });

    for (let i = 1; i < ticketQty; i++) {
      attendeeDetails.push({
        firstName: DataUtl.getRandomName(),
        lastName: DataUtl.getRandomName(),
        email: DataUtl.getRandomAttendeeEmail(),
      });
    }

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketInfo.ticketName,
          quantity: ticketQty,
          price: ticketInfo.price,
          coupon: "",
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });
      for (let i = 0; i < ticketQty; i++) {
        await flowBuilderPage.fillRegistrationForm({
          expectedFormHeading: "Ticket details",
          attendeeDetails: [attendeeDetails[i]],
          ticketAsFirstStep: true,
        });
      }
    });

    await test.step(`Complete the registration`, async () => {
      await flowBuilderPage.selectAddOns([
        {
          addOnName: "ADD_ON_2",
          quantity: 2,
          price: 10,
        },
        {
          addOnName: "ADD_ON_1",
          quantity: 1,
          price: 0,
        },
      ]);
      await flowBuilderPage.clickOnContinueButton();
      await flowBuilderPage.page.waitForTimeout(2000);
      await flowBuilderPage.fillPaymentDetailsPaidTicket({});
      await flowBuilderPage.waitForPaymentConfirmation();
      await flowBuilderPage.verifyBulkTicketingSuccess({
        attendeeDetails: attendeeDetails,
        ticketDetails: {
          ticketName: ticketInfo.ticketName,
        },
        hasAddOns: true,
        addOnDetails: [
          {
            addOnName: "ADD_ON_2",
            quantity: 2,
            price: 10,
          },
          {
            addOnName: "ADD_ON_1",
            quantity: 1,
            price: 0,
          },
        ],
      });
      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeEmail,
          eventId
        );
      });
    });
  });
});
