import {
  APIRequestContext,
  BrowserContext,
  Page,
  test,
  expect,
} from "@playwright/test";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";
import { EventController } from "../../../../controller/EventController";
import { FlowBuilderController } from "../../../../controller/FlowBuilderController";
import { TicketingController } from "../../../../controller/TicketingController";
import { EventInfoDTO } from "../../../../dto/eventInfoDto";
import EventSettingID from "../../../../enums/EventSettingEnum";
import { FlowStatus, FlowType } from "../../../../enums/FlowTypeEnum";
import EventRole from "../../../../enums/eventRoleEnum";
import EventType from "../../../../enums/eventTypeEnum";
import { publishDataPayload } from "../../../../interfaces/FlowBuilerControllerInterface";
import { FlowBuilderPage } from "../../../../page-objects/flow-builder/flow-ticketing";
import BrowserFactory from "../../../../util/BrowserFactory";
import {
  createNewEvent,
  updateEventLandingPageDetails,
  updateEventType,
} from "../../../../util/apiUtil";
import { DataUtl } from "../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../util/email-validation-api-util";

import { TransactionListPage } from "../../../../page-objects/new-org-side-pages/TransactionListPage";
import { TransactionDetailPage } from "../../../../page-objects/new-org-side-pages/TransactionDetailPage";

test.describe(`@flow-builder @registraion-with-ticket @invite-list`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId: any;
  let attendeeBrowserContext: BrowserContext;
  let eventController: EventController;
  let flowBuilderPage: FlowBuilderPage;
  let flowBuilderController: FlowBuilderController;
  let publishdataPayLoad: publishDataPayload;
  let attendeePage: Page;
  let attendeeEmail: string;
  const userFirstName = "Jxtin";
  const userLastName = "zuddl";
  let eventInfoDto: EventInfoDTO;
  const hideCoupon = DataUtl.getRandomCouponName();
  let ticketId1: string;
  let ticketId2: string;
  let ticketId3: string;
  let ticketName1: string;
  let ticketName2: string;
  let ticketName3: string;
  const eventTitle = "flow-with-taf-invitelist automation";
  let organiserPage: Page;

  let transactionListPage: TransactionListPage;
  let transactionDetailPage: TransactionDetailPage;

  test.beforeAll(async () => {
    await test.step(`setup browser contexts`, async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: true,
      });
      orgApiContext = orgBrowserContext.request;
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage = await attendeeBrowserContext.newPage();
    });

    await test.step(`create an event with FLEX as resgistration type`, async () => {
      eventId = await createNewEvent({
        api_request_context: orgApiContext,
        event_title: eventTitle,
        isFlex: true,
      });
      console.log(`event id ${eventId}`);
    });

    await test.step(`update an event with IN-PERSON as event type`, async () => {
      await updateEventType(orgApiContext, eventId, EventType.IN_PERSON);
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        isMagicLinkEnabled: true,
      });
    });

    await test.step(`Initialise event controller`, async () => {
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step(`Get event details`, async () => {
      let eventInfo = await (await eventController.getEventInfo()).json();
      console.log("Event Info: ", eventInfo);
      eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
      eventInfoDto.startDateTime = eventInfo.startDateTime;
    });

    await enableDisableEmailTrigger(
      orgApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );

    await test.step(`create tickets`, async () => {
      let ticketInfo1 = {
        name: "TKT_1",
        description: "desc",
        numberOfTickets: 10,
        pricePerTicket: 0,
        isBulkPurchaseEnabled: false,
        soldTickets: 0,
        isAvailableForAll: true,
      };

      let ticketInfo2 = {
        name: "TKT_2",
        description: "desc 2",
        numberOfTickets: 10,
        pricePerTicket: 0,
        minTicketsPerOrder: 2,
        maxTicketsPerOrder: 3,
        isBulkPurchaseEnabled: true,
        isHide: false,
        isAvailableForAll: true,
      };

      let ticketInfo3 = {
        name: "TKT_3",
        description: "desc 3",
        numberOfTickets: 10,
        pricePerTicket: 0,
        isBulkPurchaseEnabled: false,
        isHide: false,
        isAvailableForAll: true,
      };

      ticketName1 = ticketInfo1.name;
      ticketName2 = ticketInfo2.name;
      ticketName3 = ticketInfo3.name;
      let ticketingController = new TicketingController(orgApiContext, eventId);

      await ticketingController.selectGateway();

      ticketId1 = (await ticketingController.createTicket(ticketInfo1))[
        "ticketTypeId"
      ];
      ticketId2 = (await ticketingController.createTicket(ticketInfo2))[
        "ticketTypeId"
      ];
      ticketId3 = (await ticketingController.createTicket(ticketInfo3))[
        "ticketTypeId"
      ];
    });

    await test.step(`Create Add-on`, async () => {
      let freeAddOnInfo = {
        name: "ADD_ON_1",
        description: "desc",
        numberOfTickets: 10,
        pricePerTicket: 0,
        isBulkPurchaseEnabled: false,
        soldTickets: 0,
        isAvailableForAll: true,
        ticketTagType: "ADD_ON",
        linkedTicketTypeIds: [ticketId1, ticketId2],
      };

      let paidAddOnInfo = {
        name: "ADD_ON_2",
        description: "desc",
        numberOfTickets: 10,
        pricePerTicket: 10,
        isBulkPurchaseEnabled: false,
        soldTickets: 0,
        isAvailableForAll: true,
        ticketTagType: "ADD_ON",
        linkedTicketTypeIds: [ticketId1, ticketId3],
      };
      let ticketingController = new TicketingController(orgApiContext, eventId);
      await ticketingController.createTicket(freeAddOnInfo);
      await ticketingController.createTicket(paidAddOnInfo);
    });

    await test.step(`Initialise flowbuilder controller`, async () => {
      flowBuilderController = new FlowBuilderController(orgApiContext, eventId);
    });
    await test.step(`make publish flow test data`, async () => {
      let flowDraftId = await flowBuilderController.getFlowId(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
      publishdataPayLoad = {
        flowId: "",
        flowDraftId: flowDraftId,
        clientFlowDraftVersion: 2,
      };
    });

    await test.step(`update default flow steps with existing expected steps`, async () => {
      await flowBuilderController.generateTicketingFirstStep(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
    });

    await test.step(`now, publish the flow`, async () => {
      await flowBuilderController.publishFlowData(publishdataPayLoad);
      await attendeePage.waitForTimeout(5000);
    });

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
  });

  test.beforeEach(async () => {
    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
  });

  test.afterAll(async () => {
    await orgBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test("TransactionNoCopyIcon verification", async () => {
    let attendeeDetails = [];
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
    const ticketInfo = {
      ticketName: "TKT_1",
      quantity: 1,
      price: 0,
      coupon: "",
      expectSoldOut: false,
    };

    const ticketQty = ticketInfo.quantity;
    attendeeDetails.push({
      firstName: userFirstName,
      lastName: userLastName,
      email: attendeeEmail,
    });

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName1,
          quantity: ticketQty,
          price: 0,
          coupon: "",
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.handleTicketForm({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });
    });

    await test.step(`Complete the registration`, async () => {
      for (let i = 1; i < ticketQty; i++) {
        attendeeDetails.push({
          firstName: DataUtl.getRandomName(),
          lastName: DataUtl.getRandomName(),
          email: DataUtl.getRandomAttendeeEmail(),
        });
      }
      await flowBuilderPage.selectAddOns([
        {
          addOnName: "ADD_ON_1",
          quantity: 1,
          price: 0,
        },
      ]);
      await flowBuilderPage.clickOnContinueButton();
      await flowBuilderPage.page.waitForTimeout(2000);
      await flowBuilderPage.fillPaymentDetailsFreeTicket({});
      await flowBuilderPage.page.waitForTimeout(2000);
      await flowBuilderPage.verifyBulkTicketingSuccess({
        attendeeDetails: attendeeDetails,
        ticketDetails: {
          ticketName: ticketName1,
        },
        hasAddOns: true,
        addOnDetails: [
          {
            addOnName: "ADD_ON_1",
            quantity: 1,
            price: 0,
          },
        ],
      });
      await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeEmail,
          eventId
        );
      });

      attendeeBrowserContext.close();
    });

    await test.step("verify transactioncopyIcon", async () => {
      let transactionPageUrl = `${DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
        }/event/${eventId}/registration/transactions`;

      orgBrowserContext = await BrowserFactory.getBrowserContext({});
      organiserPage = await orgBrowserContext.newPage();
      await organiserPage.goto(transactionPageUrl);

      transactionListPage = new TransactionListPage(organiserPage);

      // this will navigate to transactionDetailPage
      await transactionListPage.clickOnTransactionRow(0);

      transactionDetailPage = new TransactionDetailPage(organiserPage);
      await transactionDetailPage.copyTransactionNo();
      await transactionDetailPage.expectCopiedMessageVisible();

      // todo : verify clipboard content
      const expectedClipboardContent = await transactionDetailPage.getTextelement();
      const clipboardContent = await transactionDetailPage.clipboardContent();
      expect("#" + clipboardContent).toBe(expectedClipboardContent);

    });
  });
});
