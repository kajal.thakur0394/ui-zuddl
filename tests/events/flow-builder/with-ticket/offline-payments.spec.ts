import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../../controller/EventController";
import { FlowBuilderController } from "../../../../controller/FlowBuilderController";
import { EventInfoDTO } from "../../../../dto/eventInfoDto";
import EventSettingID from "../../../../enums/EventSettingEnum";
import { FlowStatus, FlowType } from "../../../../enums/FlowTypeEnum";
import EventRole from "../../../../enums/eventRoleEnum";
import EventType from "../../../../enums/eventTypeEnum";
import {
  ExpectedTicketDetailsData,
  publishDataPayload,
} from "../../../../interfaces/FlowBuilerControllerInterface";
import { FlowBuilderPage } from "../../../../page-objects/flow-builder/flow-ticketing";
import BrowserFactory from "../../../../util/BrowserFactory";
import {
  createNewEvent,
  updateEventLandingPageDetails,
  updateEventType,
} from "../../../../util/apiUtil";
import { DataUtl } from "../../../../util/dataUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";
import { enableDisableEmailTrigger } from "../../../../util/email-validation-api-util";
import { TicketingController } from "../../../../controller/TicketingController";
import { AccessGroupController } from "../../../../controller/AccessGroupController";

test.describe(`@flow-builder @registraion-with-ticket Offline Payments`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId: any;
  let attendeeBrowserContext: BrowserContext;
  let eventController: EventController;
  let flowBuilderPage: FlowBuilderPage;
  let flowBuilderController: FlowBuilderController;
  let ticketingController: TicketingController;
  let publishdataPayLoad: publishDataPayload;
  let attendeePage: Page;
  let attendeeEmail: string;
  const userFirstName = "Jxtin";
  const userLastName = "zuddl";
  let eventInfoDto: EventInfoDTO;
  const hideCoupon = DataUtl.getRandomCouponName();
  let ticketId1: string;
  let ticketId2: string;
  let ticketId3: string;

  test.beforeAll(async () => {
    await test.step(`setup browser contexts`, async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: true,
      });
      orgApiContext = orgBrowserContext.request;
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage = await attendeeBrowserContext.newPage();
    });

    await test.step(`create an event with FLEX as resgistration type`, async () => {
      eventId = await createNewEvent({
        api_request_context: orgApiContext,
        event_title: "flow-with-bulk-ticket automation",
        isFlex: true,
      });
      console.log(`event id ${eventId}`);
    });

    await test.step(`update an event with IN-PERSON as event type`, async () => {
      await updateEventType(orgApiContext, eventId, EventType.IN_PERSON);
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        isMagicLinkEnabled: true,
      });
    });

    await test.step(`Initialise event controller`, async () => {
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step(`Get event details`, async () => {
      let eventInfo = await (await eventController.getEventInfo()).json();
      console.log("Event Info: ", eventInfo);
      eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
      eventInfoDto.startDateTime = eventInfo.startDateTime;
    });

    await enableDisableEmailTrigger(
      orgApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );

    await test.step(`create tickets`, async () => {
      let ticketInfo1 = {
        name: "TKT_1",
        description: "desc",
        numberOfTickets: 10,
        pricePerTicket: 10,
        isBulkPurchaseEnabled: true,
        minTicketsPerOrder: 2,
        maxTicketsPerOrder: 10,
        soldTickets: 0,
      };
      let ticketInfo2 = {
        name: "TKT_Virtual",
        description: "desc virtual",
        numberOfTickets: 10,
        pricePerTicket: 10,
        isBulkPurchaseEnabled: false,
        isHide: true,
      };

      let ticketInfo3 = {
        name: "TKT_3",
        description: "desc",
        numberOfTickets: 12,
        pricePerTicket: 10,
        isBulkPurchaseEnabled: true,
        minTicketsPerOrder: 3,
        maxTicketsPerOrder: 4,
        soldTickets: 0,
      };

      ticketingController = new TicketingController(orgApiContext, eventId);

      await ticketingController.selectGateway();

      ticketId1 = (await ticketingController.createTicket(ticketInfo1))[
        "ticketTypeId"
      ];
      ticketId2 = (await ticketingController.createTicket(ticketInfo2))[
        "ticketTypeId"
      ];
      ticketId3 = (await ticketingController.createTicket(ticketInfo3))[
        "ticketTypeId"
      ];

      await ticketingController.addCoupons({
        couponCode: hideCoupon,
        active: true,
        singleUsage: false,
        ticketTypeIds: [ticketId2],
        isAvailableForAllTickets: false,
        isAvailableForEveryone: true,
        maxUsageCount: 5,
        discountValue: 7,
      });
    });

    await test.step(`Initialise flowbuilder controller`, async () => {
      flowBuilderController = new FlowBuilderController(orgApiContext, eventId);
    });
    await test.step(`make publish flow test data`, async () => {
      let flowDraftId = await flowBuilderController.getFlowId(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
      publishdataPayLoad = {
        flowId: "",
        flowDraftId: flowDraftId,
        clientFlowDraftVersion: 2,
      };
    });

    await test.step(`update default flow steps with existing expected steps`, async () => {
      await flowBuilderController.generateFlowWithTicketBranching(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT,
        ticketId2
      );
    });

    await test.step(`now, publish the flow`, async () => {
      await flowBuilderController.publishFlowData(publishdataPayLoad);
      await attendeePage.waitForTimeout(1000);
    });

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
  });

  test.beforeEach(async () => {
    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
  });

  test.afterEach(async () => {
    await attendeePage.reload();
    // clear the cookies
    await attendeePage.context().clearCookies();
    await attendeePage.evaluate(() => {
      localStorage.clear();
    });
  });

  test.afterAll(async () => {
    await orgBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test(`TC001: User Tries to buy a ticket using offline payment (CASH) and then cancels the transaction`, async () => {
    let attendeeDetails = [];
    const params = `?ticket_id=${ticketId3}`;
    await test.step(`set content to publish site having utm parameters`, async () => {
      await attendeePage.goto("https://example.com/" + params);
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
      await attendeePage.waitForTimeout(1000);
    });

    attendeeEmail = DataUtl.getRandomAttendeeEmail();
    const ticketInfo = {
      ticketName: "TKT_3",
      quantity: 3,
      price: 10,
      coupon: "",
      expectSoldOut: false,
    };

    const ticketQty = ticketInfo.quantity;
    attendeeDetails.push({
      firstName: userFirstName,
      lastName: userLastName,
      email: attendeeEmail,
    });

    for (let i = 0; i < ticketQty - 1; i++) {
      attendeeDetails.push({
        firstName: DataUtl.getRandomName(),
        lastName: DataUtl.getRandomName(),
        email: DataUtl.getRandomAttendeeEmail(),
      });
    }

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.handleTicketForm({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });

      await flowBuilderPage.page.waitForTimeout(1000);

      await test.step(`Verifying that the ticket is already selected`, async () => {
        await flowBuilderPage.verifySelectedTicketCount({
          ticketName: ticketInfo.ticketName,
          expectedTicketCount: ticketQty,
        });
      });

      await flowBuilderPage.clickOnContinueButton();
      await flowBuilderPage.page.waitForTimeout(1000);
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Ticket details",
        attendeeDetails: attendeeDetails,
        ticketInfo: ticketInfo,
      });
      await flowBuilderPage.page.waitForTimeout(1000);
      await flowBuilderPage.verifyOrderSummaryBulk({
        dataToVerify: {
          attendeeDetails: attendeeDetails,
          ticketName: ticketInfo.ticketName,
        },
      });
      await flowBuilderPage.page.waitForTimeout(1000);
      await flowBuilderPage.verifyPaidTicketDetails(false, {
        ticketName: ticketInfo.ticketName,
        ticketPrice: (ticketQty * ticketInfo.price).toString(),
        dicountValue: "0",
        totalPriceAfterDiscount: (ticketQty * ticketInfo.price).toString(),
      });

      await flowBuilderPage.useOfflinePaymentMethod({});
      await flowBuilderPage.page.waitForTimeout(1000);
      await flowBuilderPage.verifyBulkTicketingSuccess({
        attendeeDetails: attendeeDetails,
        ticketDetails: {
          ticketName: ticketInfo.ticketName,
        },
      });
    });

    await flowBuilderPage.page.waitForTimeout(15000);
    await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
      for (let i = 0; i < attendeeDetails.length; i++) {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeDetails[i].email,
          eventId
        );
      }
    });

    let transactions = await ticketingController.getAllTransactions();
    const transactionId = await flowBuilderPage.getTransactionCode();
    let transactionInfo = transactions.find(
      (transaction) => transaction["vivenuTransactionId"] === transactionId
    );

    await test.step(`Verify the transaction details`, async () => {
      console.log(JSON.stringify(transactionInfo));
      expect(transactionInfo["paymentStatus"]).toBe("LOCAL_PENDING");
      expect(transactionInfo["paymentMode"]).toBe("local");
      let tickets = transactionInfo["tickets"];
      for (let i = 0; i < tickets.length; i++) {
        expect(tickets[i]["ticketTypeId"]).toBe(ticketId3);
      }
      expect(transactionInfo["price"]).toBe(ticketQty * ticketInfo.price);
      expect(transactionInfo["paymentMethod"]).toBe("local");
      console.log(JSON.stringify(transactionInfo));
      console.log(transactionId);

      transactionInfo = await ticketingController.getTransactionDetails(
        transactionId
      );

      console.log(JSON.stringify(transactionInfo));

      expect(transactionInfo["paymentStatus"]).toBe("LOCAL_PENDING");
      expect(transactionInfo["paymentMode"]).toBe("local");
      expect(transactionInfo["paymentMethod"]).toBe("local");
    });

    const paymentMethod = "CASH";

    await test.step(`Organiser verifies the transaction`, async () => {
      await ticketingController.confirmTransaction(transactionId, {
        paymentGatewayType: "CASH",
      });
    });

    transactions = await ticketingController.getAllTransactions();
    transactionInfo = transactions.find(
      (transaction) => transaction["vivenuTransactionId"] === transactionId
    );

    await test.step(`Verify the transaction details`, async () => {
      console.log(JSON.stringify(transactionInfo));
      expect(transactionInfo["paymentStatus"]).toBe("RECEIVED");
      expect(transactionInfo["paymentMode"]).toBe("local");
      expect(transactionInfo["paymentMethod"]).toBe(paymentMethod);
      let tickets = transactionInfo["tickets"];
      for (let i = 0; i < tickets.length; i++) {
        expect(tickets[i]["ticketTypeId"]).toBe(ticketId3);
      }
      expect(transactionInfo["price"]).toBe(ticketQty * ticketInfo.price);
      expect(transactionInfo["paymentMethod"]).toBe("CASH");

      transactionInfo = await ticketingController.getTransactionDetails(
        transactionInfo["vivenuTransactionId"]
      );

      expect(transactionInfo["paymentStatus"]).toBe("RECEIVED");
      expect(transactionInfo["paymentMode"]).toBe("local");
      expect(transactionInfo["paymentMethod"]).toBe(`${paymentMethod}`);
    });

    await test.step(`User cancels the transaction`, async () => {
      await ticketingController.cancelTransaction(transactionId);
    });

    transactions = await ticketingController.getAllTransactions();
    transactionInfo = transactions.find(
      (transaction) => transaction["vivenuTransactionId"] === transactionId
    );

    await test.step(`Verify the transaction details`, async () => {
      expect(transactionInfo["paymentStatus"]).toBe("LOCAL_RECEIVED");
      expect(transactionInfo["paymentMode"]).toBe("local");
      expect(transactionInfo["paymentMethod"]).toBe(paymentMethod);
      let tickets = transactionInfo["tickets"];
      for (let i = 0; i < tickets.length; i++) {
        expect(tickets[i]["ticketTypeId"]).toBe(ticketId3);
      }
      expect(transactionInfo["price"]).toBe(ticketQty * ticketInfo.price);
      expect(transactionInfo["paymentMethod"]).toBe(paymentMethod);

      transactionInfo = await ticketingController.getTransactionDetails(
        transactionInfo["vivenuTransactionId"]
      );

      expect(transactionInfo["paymentStatus"]).toBe("LOCAL_RECEIVED");
      expect(transactionInfo["paymentMode"]).toBe("local");
      expect(transactionInfo["paymentMethod"]).toBe(`${paymentMethod}`);
    });

    transactionInfo = await ticketingController.getTransactionDetails(
      transactionId
    );
    await test.step(`Verify the transaction details`, async () => {
      expect(transactionInfo["paymentStatus"]).toBe("LOCAL_RECEIVED");
      expect(transactionInfo["paymentMode"]).toBe("local");
      expect(transactionInfo["paymentMethod"]).toBe(paymentMethod);
      expect(transactionInfo["price"]).toBe(ticketQty * ticketInfo.price);
      expect(transactionInfo["paymentMethod"]).toBe(paymentMethod);

      transactionInfo = await ticketingController.getTransactionDetails(
        transactionInfo["vivenuTransactionId"]
      );

      expect(transactionInfo["paymentStatus"]).toBe("LOCAL_RECEIVED");
      expect(transactionInfo["paymentMode"]).toBe("local");
      expect(transactionInfo["paymentMethod"]).toBe(`${paymentMethod}`);
    });
  });

  test(`TC002: User Tries to buy a ticket using offline payment (BANK_TRANSFER)`, async () => {
    let attendeeDetails = [];
    const params = `?ticket_id=${ticketId3}`;
    await test.step(`set content to publish site having utm parameters`, async () => {
      await attendeePage.goto("https://example.com/" + params);
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
      await attendeePage.waitForTimeout(1000);
    });

    attendeeEmail = DataUtl.getRandomAttendeeEmail();
    const ticketInfo = {
      ticketName: "TKT_3",
      quantity: 3,
      price: 10,
      coupon: "",
      expectSoldOut: false,
    };

    const ticketQty = ticketInfo.quantity;
    attendeeDetails.push({
      firstName: userFirstName,
      lastName: userLastName,
      email: attendeeEmail,
    });

    for (let i = 0; i < ticketQty - 1; i++) {
      attendeeDetails.push({
        firstName: DataUtl.getRandomName(),
        lastName: DataUtl.getRandomName(),
        email: DataUtl.getRandomAttendeeEmail(),
      });
    }

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.handleTicketForm({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });

      await flowBuilderPage.page.waitForTimeout(1000);

      await test.step(`Verifying that the ticket is already selected`, async () => {
        await flowBuilderPage.verifySelectedTicketCount({
          ticketName: ticketInfo.ticketName,
          expectedTicketCount: ticketQty,
        });
      });

      await flowBuilderPage.clickOnContinueButton();
      await flowBuilderPage.page.waitForTimeout(1000);
      await flowBuilderPage.fillRegistrationForm({
        expectedFormHeading: "Ticket details",
        attendeeDetails: attendeeDetails,
        ticketInfo: ticketInfo,
      });
      await flowBuilderPage.page.waitForTimeout(1000);
      await flowBuilderPage.verifyOrderSummaryBulk({
        dataToVerify: {
          attendeeDetails: attendeeDetails,
          ticketName: ticketInfo.ticketName,
        },
      });
      await flowBuilderPage.page.waitForTimeout(1000);
      await flowBuilderPage.verifyPaidTicketDetails(false, {
        ticketName: ticketInfo.ticketName,
        ticketPrice: (ticketQty * ticketInfo.price).toString(),
        dicountValue: "0",
        totalPriceAfterDiscount: (ticketQty * ticketInfo.price).toString(),
      });

      await flowBuilderPage.useOfflinePaymentMethod({});
      await flowBuilderPage.page.waitForTimeout(1000);
      await flowBuilderPage.verifyBulkTicketingSuccess({
        attendeeDetails: attendeeDetails,
        ticketDetails: {
          ticketName: ticketInfo.ticketName,
        },
      });
    });

    await flowBuilderPage.page.waitForTimeout(15000);
    await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
      for (let i = 0; i < attendeeDetails.length; i++) {
        await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
          attendeeDetails[i].email,
          eventId
        );
      }
    });

    let transactions = await ticketingController.getAllTransactions();
    const transactionId = await flowBuilderPage.getTransactionCode();
    let transactionInfo = transactions.find(
      (transaction) => transaction["vivenuTransactionId"] === transactionId
    );
    await test.step(`Verify the transaction details`, async () => {
      console.log(JSON.stringify(transactionInfo));
      expect(transactionInfo["paymentStatus"]).toBe("LOCAL_PENDING");
      expect(transactionInfo["paymentMode"]).toBe("local");
      let tickets = transactionInfo["tickets"];
      for (let i = 0; i < tickets.length; i++) {
        expect(tickets[i]["ticketTypeId"]).toBe(ticketId3);
      }
      expect(transactionInfo["price"]).toBe(ticketQty * ticketInfo.price);
      expect(transactionInfo["paymentMethod"]).toBe("local");
      console.log(JSON.stringify(transactionInfo));
      console.log(transactionId);

      transactionInfo = await ticketingController.getTransactionDetails(
        transactionId
      );

      console.log(JSON.stringify(transactionInfo));

      expect(transactionInfo["paymentStatus"]).toBe("LOCAL_PENDING");
      expect(transactionInfo["paymentMode"]).toBe("local");
      expect(transactionInfo["paymentMethod"]).toBe("local");
    });

    const paymentMethod = "BANK_TRANSFER";

    await test.step(`Organiser verifies the transaction`, async () => {
      await ticketingController.confirmTransaction(transactionId, {
        paymentGatewayType: paymentMethod,
      });
    });
    transactions = await ticketingController.getAllTransactions();
    transactionInfo = transactions.find(
      (transaction) => transaction["vivenuTransactionId"] === transactionId
    );

    await test.step(`Verify the transaction details`, async () => {
      console.log(JSON.stringify(transactionInfo));
      expect(transactionInfo["paymentStatus"]).toBe("RECEIVED");
      expect(transactionInfo["paymentMode"]).toBe("local");
      expect(transactionInfo["paymentMethod"]).toBe(paymentMethod);
      let tickets = transactionInfo["tickets"];
      for (let i = 0; i < tickets.length; i++) {
        expect(tickets[i]["ticketTypeId"]).toBe(ticketId3);
      }
      expect(transactionInfo["price"]).toBe(ticketQty * ticketInfo.price);
      expect(transactionInfo["paymentMethod"]).toBe(paymentMethod);

      transactionInfo = await ticketingController.getTransactionDetails(
        transactionInfo["vivenuTransactionId"]
      );

      expect(transactionInfo["paymentStatus"]).toBe("RECEIVED");
      expect(transactionInfo["paymentMode"]).toBe("local");
      expect(transactionInfo["paymentMethod"]).toBe(`${paymentMethod}`);
    });
  });
});
