import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../../controller/EventController";
import { FlowBuilderController } from "../../../../controller/FlowBuilderController";
import { EventInfoDTO } from "../../../../dto/eventInfoDto";
import EventSettingID from "../../../../enums/EventSettingEnum";
import { FlowStatus, FlowType } from "../../../../enums/FlowTypeEnum";
import EventRole from "../../../../enums/eventRoleEnum";
import EventType from "../../../../enums/eventTypeEnum";
import { publishDataPayload } from "../../../../interfaces/FlowBuilerControllerInterface";
import { FlowBuilderPage } from "../../../../page-objects/flow-builder/flow-ticketing";
import BrowserFactory from "../../../../util/BrowserFactory";
import {
  createNewEvent,
  getSignedS3Url,
  updateEventLandingPageDetails,
  updateEventType,
} from "../../../../util/apiUtil";
import { DataUtl } from "../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../util/email-validation-api-util";
import { TicketingController } from "../../../../controller/TicketingController";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@flow-builder @registraion-with-ticket @invite-list`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId: any;
  let attendeeBrowserContext: BrowserContext;
  let eventController: EventController;
  let flowBuilderPage: FlowBuilderPage;
  let flowBuilderController: FlowBuilderController;
  let publishdataPayLoad: publishDataPayload;
  let attendeePage: Page;
  let attendeeEmail: string;
  const userFirstName = "Jxtin";
  const userLastName = "zuddl";
  let eventInfoDto: EventInfoDTO;
  const hideCoupon = DataUtl.getRandomCouponName();
  let ticketId1: string;
  let ticketId2: string;
  let ticketId3: string;
  let ticketName1: string;
  let ticketName2: string;
  let ticketName3: string;

  const email = {
    group1: "invite-audience1@justjoyn.com",
    group2: "jxtin@justjoyn.com",
  };

  test.beforeAll(async () => {
    await test.step(`setup browser contexts`, async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: true,
      });
      orgApiContext = orgBrowserContext.request;
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage = await attendeeBrowserContext.newPage();
    });

    await test.step(`create an event with FLEX as resgistration type`, async () => {
      eventId = await createNewEvent({
        api_request_context: orgApiContext,
        event_title: "flow-with-taf-invitelist automation",
        isFlex: true,
      });
      console.log(`event id ${eventId}`);
    });

    await test.step(`update an event with IN-PERSON as event type`, async () => {
      await updateEventType(orgApiContext, eventId, EventType.IN_PERSON);
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        isMagicLinkEnabled: true,
      });
    });

    await test.step(`Initialise event controller`, async () => {
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step(`Get event details`, async () => {
      let eventInfo = await (await eventController.getEventInfo()).json();
      console.log("Event Info: ", eventInfo);
      eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
      eventInfoDto.startDateTime = eventInfo.startDateTime;
    });

    await enableDisableEmailTrigger(
      orgApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );

    let s3Url_l1 = await getSignedS3Url(
      orgApiContext,
      "test-data/invitelist-1.csv"
    );
    const groupName1 = "audience1";
    const groupName2 = "audience2";
    let s3Url_l2 = await getSignedS3Url(
      orgApiContext,
      "test-data/invitelist-2.csv"
    );
    await new Promise((r) => setTimeout(r, 15000));
    await eventController.addInviteListToEvent(s3Url_l1, groupName1, "audi1");
    await new Promise((r) => setTimeout(r, 15000));
    await eventController.addInviteListToEvent(s3Url_l2, groupName2, "audi2");
    await new Promise((r) => setTimeout(r, 15000));

    let invitelistGroupId1 = await eventController.getInviteListAudienceGroupId(
      groupName1
    );
    let invitelistGroupId2 = await eventController.getInviteListAudienceGroupId(
      groupName2
    );
    console.log("invitelistGroupId1", invitelistGroupId1);
    console.log("invitelistGroupId2", invitelistGroupId2);

    await test.step(`create tickets`, async () => {
      let ticketInfo1 = {
        name: "TKT_1",
        description: "desc",
        numberOfTickets: 10,
        pricePerTicket: 0,
        isBulkPurchaseEnabled: false,
        soldTickets: 0,
        isInvitedList: true,
        audienceGroupIds: [invitelistGroupId1],
        isAvailableForAll: false,
      };

      let ticketInfo2 = {
        name: "TKT_2",
        description: "desc 2",
        numberOfTickets: 10,
        pricePerTicket: 0,
        isBulkPurchaseEnabled: false,
        isHide: false,
        isAvailableForAll: false,
        audienceGroupIds: [invitelistGroupId2],
        isInvitedList: true,
      };

      let ticketInfo3 = {
        name: "TKT_3",
        description: "desc 3",
        numberOfTickets: 10,
        pricePerTicket: 0,
        isBulkPurchaseEnabled: false,
        isHide: false,
        isAvailableForAll: false,
        audienceGroupIds: [invitelistGroupId1, invitelistGroupId2],
        isInvitedList: true,
      };

      ticketName1 = ticketInfo1.name;
      ticketName2 = ticketInfo2.name;
      ticketName3 = ticketInfo3.name;
      let ticketingController = new TicketingController(orgApiContext, eventId);

      await ticketingController.selectGateway();

      ticketId1 = (await ticketingController.createTicket(ticketInfo1))[
        "ticketTypeId"
      ];
      ticketId2 = (await ticketingController.createTicket(ticketInfo2))[
        "ticketTypeId"
      ];
      ticketId3 = (await ticketingController.createTicket(ticketInfo3))[
        "ticketTypeId"
      ];
    });

    await test.step(`Initialise flowbuilder controller`, async () => {
      flowBuilderController = new FlowBuilderController(orgApiContext, eventId);
    });
    await test.step(`make publish flow test data`, async () => {
      let flowDraftId = await flowBuilderController.getFlowId(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
      publishdataPayLoad = {
        flowId: "",
        flowDraftId: flowDraftId,
        clientFlowDraftVersion: 2,
      };
    });

    await test.step(`update default flow steps with existing expected steps`, async () => {
      await flowBuilderController.generateTicketingFirstStep(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
    });

    await test.step(`now, publish the flow`, async () => {
      await flowBuilderController.publishFlowData(publishdataPayLoad);
      await attendeePage.waitForTimeout(5000);
    });

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
  });

  test.beforeEach(async () => {
    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
  });

  test.afterEach(async () => {
    await attendeePage.reload();
    // clear the cookies
    await attendeePage.context().clearCookies();
    await attendeePage.evaluate(() => {
      localStorage.clear();
    });
  });

  test.afterAll(async () => {
    await orgBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test(`TC001 : @flow-builder @registraion-with-ticket @invite-list @invite-list-1 Using email from group 1`, async () => {
    let attendeeDetails = [];
    attendeeEmail = email.group1;
    const ticketInfo = {
      ticketName: "TKT_1",
      quantity: 1,
      price: 0,
      coupon: "",
      expectSoldOut: false,
    };

    const ticketQty = ticketInfo.quantity;
    attendeeDetails.push({
      firstName: userFirstName,
      lastName: userLastName,
      email: attendeeEmail,
    });

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: {
          ticketName: ticketName1,
          quantity: ticketQty,
          price: 0,
          coupon: "",
          expectSoldOut: false,
        },
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.handleTicketForm({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });
    });
    await test.step(`Complete the registration`, async () => {
      await flowBuilderPage.page.waitForTimeout(2000);
      for (let i = 1; i < ticketQty; i++) {
        attendeeDetails.push({
          firstName: DataUtl.getRandomName(),
          lastName: DataUtl.getRandomName(),
          email: DataUtl.getRandomAttendeeEmail(),
        });
      }
      await flowBuilderPage.clickOnContinueButton();
      await flowBuilderPage.fillPaymentDetailsFreeTicket({});
      await flowBuilderPage.page.waitForTimeout(2000);
      await flowBuilderPage.verifyBulkTicketingSuccess({
        attendeeDetails: attendeeDetails,
        ticketDetails: {
          ticketName: ticketName1,
        },
      });
    });

    await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
      await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
        attendeeEmail,
        eventId
      );
    });
  });

  test(`TC002 : @flow-builder @registraion-with-ticket @invite-list @invite-list-2 Using email from group 2, gets ticket 3`, async () => {
    let attendeeDetails = [];
    attendeeEmail = email.group2;

    const ticketInfo = {
      ticketName: "TKT_3",
      quantity: 1,
      price: 0,
      coupon: "",
      expectSoldOut: false,
    };

    const ticketQty = ticketInfo.quantity;

    attendeeDetails.push({
      firstName: userFirstName,
      lastName: userLastName,
      email: attendeeEmail,
    });

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: ticketInfo,
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.handleTicketForm({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });
    });

    await test.step(`Complete the registration`, async () => {
      for (let i = 1; i < ticketQty; i++) {
        attendeeDetails.push({
          firstName: DataUtl.getRandomName(),
          lastName: DataUtl.getRandomName(),
          email: DataUtl.getRandomAttendeeEmail(),
        });
      }
      await flowBuilderPage.clickOnContinueButton();
      await flowBuilderPage.fillPaymentDetailsFreeTicket({});
      await flowBuilderPage.page.waitForTimeout(2000);
      await flowBuilderPage.verifyBulkTicketingSuccess({
        attendeeDetails: attendeeDetails,
        ticketDetails: {
          ticketName: ticketName3,
        },
      });
    });

    await test.step(`Verify in DB user is registered in an event ${eventId}`, async () => {
      await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
        attendeeEmail,
        eventId
      );
    });
  });

  test(`TC003 : @flow-builder @registraion-with-ticket @invite-list @invite-list-3 Using email from group 1, gets ticket 2`, async () => {
    let attendeeDetails = [];
    attendeeEmail = email.group1;

    const ticketInfo = {
      ticketName: "TKT_2",
      quantity: 1,
      price: 0,
      coupon: "",
      expectSoldOut: false,
    };

    const ticketQty = ticketInfo.quantity;

    attendeeDetails.push({
      firstName: userFirstName,
      lastName: userLastName,
      email: attendeeEmail,
    });

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: ticketInfo,
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.handleTicketForm({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });

      await expect(flowBuilderPage.errorMessage).toBeVisible({ timeout: 5000 });
    });
  });

  test(`TC004 : @flow-builder @registraion-with-ticket @invite-list @invite-list-4 Using email from neither group`, async () => {
    let attendeeDetails = [];
    attendeeEmail = DataUtl.getRandomAttendeeEmail();

    const ticketInfo = {
      ticketName: "TKT_3",
      quantity: 1,
      price: 0,
      coupon: "",
      expectSoldOut: false,
    };

    const ticketQty = ticketInfo.quantity;

    attendeeDetails.push({
      firstName: userFirstName,
      lastName: userLastName,
      email: attendeeEmail,
    });

    await test.step(`User fills up the form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        ticketInfo: ticketInfo,
        expectedFormHeading: "Ticket",
      });

      await flowBuilderPage.handleTicketForm({
        firstName: userFirstName,
        lastName: userLastName,
        email: attendeeEmail,
      });

      await expect(flowBuilderPage.errorMessage).toBeVisible({
        timeout: 5000,
      });
    });
  });
});
