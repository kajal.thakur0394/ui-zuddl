import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import {
  userVerifyOtpByApi,
  userVerifyTeamInvitationUsingOtp,
} from "../../../util/apiUtil";
import { TeamController } from "../../../controller/TeamsController";
import { EventController } from "../../../controller/EventController";
import { OrganisationController } from "../../../controller/OrganisationController";
import { MemberRolesEnum } from "../../../enums/MemberRolesEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@duplicate-events @bugs`, async () => {
  test.describe.configure({ retries: 2 });
  // team admin
  let teamAdminBrowserContext: BrowserContext;
  let teamAdminPage: Page;
  let teamAdminApiRequest: APIRequestContext;

  // team moderator
  let teamModeratorContext: BrowserContext;
  let teamModeratorPage: Page;
  let teamModeratorRequest: APIRequestContext;

  // team organizer
  let teamOrganiserContext: BrowserContext;
  let teamOrganiserPage: Page;
  let teamOrganiserRequest: APIRequestContext;

  let teamModeratorEmail: string;
  let teamOrganiserEmail: string;
  let generalTeamId: string;

  let organisationIdForTeamAutomation: string;
  let adminEmail: string;
  let teamController: TeamController;
  let organisationController: OrganisationController;

  test.beforeAll(async ({ browser }) => {
    await test.step(`Organiser context created for team automation organisation`, async () => {
      // team admin context
      teamAdminBrowserContext = await browser.newContext();
      teamAdminPage = await teamAdminBrowserContext.newPage();
      teamAdminApiRequest = teamAdminPage.request;

      // moderator context
      teamModeratorContext = await browser.newContext();
      teamModeratorPage = await teamModeratorContext.newPage();
      teamModeratorRequest = teamModeratorPage.request;

      // organizer context
      teamOrganiserContext = await browser.newContext();
      teamOrganiserPage = await teamOrganiserContext.newPage();
      teamOrganiserRequest = teamOrganiserPage.request;

      //getting organisation id which is needed for teams automation
      organisationIdForTeamAutomation =
        await DataUtl.getApplicationTestDataObj()[
          "organisationIdForTeamsAutomation"
        ];
      teamController = new TeamController(
        teamAdminApiRequest,
        organisationIdForTeamAutomation
      );
      organisationController = new OrganisationController(teamAdminApiRequest);

      //getting the admin email so that he can login and we can set the cookies for browser context
      adminEmail = await DataUtl.getApplicationTestDataObj()[
        "adminEmailForTeamAutomationOrganisation"
      ];
      await test.step(`Organiser login to setup side using otp`, async () => {
        const otpVerificationIdForAdmin =
          await userVerifyTeamInvitationUsingOtp(
            teamAdminApiRequest,
            adminEmail
          );
        console.log(
          `admin otp verification id is ${otpVerificationIdForAdmin}`
        );
        //fetch otp for admin
        const otpCodeForAdmin = await QueryUtil.fetchLoginOTPByVerificationId(
          adminEmail,
          otpVerificationIdForAdmin
        );
        //verify otp for admin
        console.log(`fetched otp for admin  is ${otpCodeForAdmin}`);
        await userVerifyOtpByApi(
          teamAdminPage.request,
          otpVerificationIdForAdmin,
          otpCodeForAdmin
        );
      });
    });
    await test.step(`Fetch Default Team Id`, async () => {
      generalTeamId = await teamController.fetchGeneralTeamId();
    });

    await test.step(`Add new member as moderator in teams`, async () => {
      teamModeratorEmail = DataUtl.getRandomModeratorEmail();
      console.log(`Adding ${teamModeratorEmail} as moderator to team`);
      const moderatorDetailsToAdd = {
        firstName: "PrateekModerator",
        lastName: "Automation",
        email: teamModeratorEmail,
        productRole: MemberRolesEnum.MODERATOR,
        myTeamList: [{ teamId: generalTeamId }],
      };
      await organisationController.addNewMemberToOrganisation(
        moderatorDetailsToAdd
      );
    });

    await test.step(`Moderator verifies his email by completing otp verification`, async () => {
      const otpVerificationIdForOrganiser =
        await userVerifyTeamInvitationUsingOtp(
          teamModeratorRequest,
          teamModeratorEmail
        );
      //fetch otp by verification id
      const otpCodeForOrganiser = await QueryUtil.fetchLoginOTPByVerificationId(
        teamModeratorEmail,
        otpVerificationIdForOrganiser
      );
      console.log(`fetched otp for organiser is ${otpCodeForOrganiser}`);
      //now organiser verify his otp
      await userVerifyOtpByApi(
        teamModeratorRequest,
        otpVerificationIdForOrganiser,
        otpCodeForOrganiser
      );
    });

    await test.step(`Add new member as organizer in teams`, async () => {
      teamOrganiserEmail = DataUtl.getRandomOrganiserEmail();
      console.log(`Adding ${teamOrganiserEmail} as organizer to team`);
      const organiserDetailsToAdd = {
        firstName: "PrateekOrganizer",
        lastName: "Automation",
        email: teamOrganiserEmail,
        productRole: MemberRolesEnum.ORGANIZER,
        myTeamList: [{ teamId: generalTeamId }],
      };
      await organisationController.addNewMemberToOrganisation(
        organiserDetailsToAdd
      );
    });

    await test.step(`Organizer verifies his email by completing otp verification`, async () => {
      const otpVerificationIdForOrganiser =
        await userVerifyTeamInvitationUsingOtp(
          teamOrganiserRequest,
          teamOrganiserEmail
        );
      //fetch otp by verification id
      const otpCodeForOrganiser = await QueryUtil.fetchLoginOTPByVerificationId(
        teamOrganiserEmail,
        otpVerificationIdForOrganiser
      );
      console.log(`fetched otp for organiser is ${otpCodeForOrganiser}`);
      //now organiser verify his otp
      await userVerifyOtpByApi(
        teamOrganiserRequest,
        otpVerificationIdForOrganiser,
        otpCodeForOrganiser
      );
    });
  });

  test.afterAll(async () => {
    await teamAdminBrowserContext?.close();
    await teamModeratorContext?.close();
    await teamOrganiserContext?.close();
  });

  test(`TC001 : Event Role for organization Member role should be added as Organizer on duplication of event`, async () => {
    // linear ticket info
    const originalEventTitle = DataUtl.getRandomEventTitle();
    let originalEventID;
    let duplicateEventId;
    let eventController: EventController;

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-135/event-role-for-organization-member-role-should-be-added-as-organizer",
    });

    await test.step(`Team admin as organiser creates a new event`, async () => {
      originalEventID = await EventController.generateNewEvent(
        teamAdminApiRequest,
        {
          event_title: originalEventTitle,
        }
      );
    });

    await test.step(`verify organizer email has correct event role as ORGANIZER for the newly created event`, async () => {
      const organiserEventRoleData = await QueryUtil.fetchUserEventRoleData(
        originalEventID,
        teamOrganiserEmail
      );
      expect(
        organiserEventRoleData["role"],
        "expecting event role for this user to be ORGANIZER"
      ).toBe("ORGANIZER");
    });

    await test.step(`verify moderator email has correct event role as MODERATOR for the newly created event`, async () => {
      const organiserEventRoleData = await QueryUtil.fetchUserEventRoleData(
        originalEventID,
        teamModeratorEmail
      );
      expect(
        organiserEventRoleData["role"],
        "expecting event role for this user to be MODERATOR"
      ).toBe("MODERATOR");
    });

    await test.step(`Team admin as organiser duplicates the event created`, async () => {
      eventController = new EventController(
        teamAdminApiRequest,
        originalEventID
      );
      duplicateEventId =
        await eventController.triggerDuplicateEventApiForThisEvent();
    });

    await test.step(`verify organizer email has correct event role as ORGANIZER for the newly created event`, async () => {
      const organiserEventRoleData = await QueryUtil.fetchUserEventRoleData(
        duplicateEventId,
        teamOrganiserEmail
      );
      expect(
        organiserEventRoleData["role"],
        "expecting event role for this user to be ORGANIZER"
      ).toBe("ORGANIZER");
    });

    await test.step(`verify moderator email has correct event role as MODERATOR for the newly created event`, async () => {
      const moderatorEventRoleData = await QueryUtil.fetchUserEventRoleData(
        duplicateEventId,
        teamModeratorEmail
      );
      expect(
        moderatorEventRoleData["role"],
        "expecting event role for this user to be MODERATOR"
      ).toBe("MODERATOR");
    });
  });
});
