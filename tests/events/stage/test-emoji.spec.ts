import test, {
  APIRequestContext,
  BrowserContext,
  Page,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import {
  inviteAttendeeByAPI,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import { GreenRoom } from "../../../page-objects/events-pages/site-modules/GreenRoom";
import { VenueSetup } from "../../../page-objects/new-org-side-pages/VenueSetup";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe("@legacy-backstage @emojis", async () => {
  let eventId: any;
  let stageId: any;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let organiserPage: Page;
  let organiserEventSidePage: Page;
  let eventController: EventController;
  let attendeeLandingPage: string;
  let attendeeBrowserContext: BrowserContext;
  let attendeePage: Page;
  let stageUrl: string;
  let magicLink: string;
  let attendeeEmail: string;
  let organiserStagePage;
  let organiserGreenRoom: GreenRoom;
  let attendeeStagePage: StagePage;

  const organiserFirstName =
    DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

  test.beforeEach(async () => {
    await test.step("Initialize organiser browser and API context and page.", async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({});
      orgApiContext = orgBrowserContext.request;
      organiserPage = await orgBrowserContext.newPage();
      organiserEventSidePage = await orgBrowserContext.newPage();
    });

    await test.step("Creating new event.", async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
      });
    });

    await test.step("Initializing event controller.", async () => {
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step("Fetching deafult stage id.", async () => {
      stageId = await eventController.getDefaultStageId();
      eventController.disableStudioAsBackstage(stageId);
    });

    await test.step("Enable magic link.", async () => {
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
      });
    });

    await test.step("Get attendee landing page and stage page URL.", async () => {
      let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
      attendeeLandingPage = eventInfoDto.attendeeLandingPage;

      stageUrl = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/l/event/${eventId}/stages/${stageId}`;
    });

    await test.step("Initialize Attendee browser context and page.", async () => {
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage = await attendeeBrowserContext.newPage();

      attendeeStagePage = new StagePage(attendeePage);
    });

    await test.step("Update event landing page details.", async () => {
      await eventController.disableOnboardingChecksForAttendee();
    });

    await test.step("Genreate random attnedee email.", async () => {
      attendeeEmail = DataUtl.getRandomAttendeeEmail();
    });

    await test.step("Invite Attendee by API.", async () => {
      await inviteAttendeeByAPI(orgApiContext, eventId, attendeeEmail, true);
    });

    await test.step("Fetch Magic-Link from database.", async () => {
      magicLink = await QueryUtil.fetchMagicLinkFromDB(eventId, attendeeEmail);
    });

    await test.step(`Organizer joins the event and join backstage via default stage.`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(stageUrl);

      organiserStagePage = new StagePage(organiserPage);
      const orgAvModal =
        await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
      await orgAvModal.isVideoStreamLoadedOnAVModal();
      await orgAvModal.clickOnJoinButtonOnAvModal();

      await test.step(`Verify Organiser's presence in backstage.`, async () => {
        organiserGreenRoom = new GreenRoom(organiserPage);
        await organiserGreenRoom.isGreenRoomVisible();
        await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(1);
      });
    });
  });

  test.afterEach(async () => {
    await organiserPage?.close();
    await attendeePage?.close();
    await orgBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test("TC001: Organiser enables the emojis for stage and attendee is able to view the emojis in Dry-Run. | @oldStage @dry-run", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-190",
    });

    await test.step(`Organizer initiate dry-run and joins the stage.`, async () => {
      await organiserGreenRoom.startTheDryRun();
    });

    await test.step("Attendee login to event via Magic-Link.", async () => {
      await attendeePage.goto(magicLink);
      await attendeePage.click("text=Enter Now");
      await attendeePage.goto(stageUrl);
    });

    await test.step(`Verify Attendee's view.`, async () => {
      await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
        0
      );
    });

    await test.step("Verify if emojis are not Visible", async () => {
      await attendeeStagePage.verifyEmojisAreNotVisible();
    });

    await test.step(`Organizer joins the stage.`, async () => {
      await organiserGreenRoom.sendUserToOnstageFromBackStageUsingRaiseHand(
        organiserFirstName
      );
    });

    await test.step(`Verify Attendee's view.`, async () => {
      await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
        1
      );
    });

    await test.step("Verify if emojis are Visible", async () => {
      await attendeeStagePage.verifyEmojisAreVisible();
    });

    await test.step(`Organiser ends the dry run`, async () => {
      await organiserGreenRoom.endTheDryRun();
    });

    await test.step("Verify if emojis are not Visible", async () => {
      await attendeeStagePage.verifyEmojisAreNotVisible();
    });
  });

  test("TC002: Organiser enables the emojis for stage and attendee is able to view the emojis in Session-Run | @oldStage @session", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-191",
    });

    await test.step(`Organizer initiate session.`, async () => {
      await organiserGreenRoom.disableDryRun();
      await organiserGreenRoom.startTheSession();
    });

    await test.step("Attendee login to event via Magic-Link.", async () => {
      await attendeePage.goto(magicLink);
      await attendeePage.click("text=Enter Now");
      await attendeePage.goto(stageUrl);
    });

    await test.step(`Verify Attendee's view.`, async () => {
      await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
        0
      );
    });

    await test.step("Verify if emojis are Visible", async () => {
      await attendeeStagePage.verifyEmojisAreNotVisible();
    });

    await test.step(`Organizer joins the stage.`, async () => {
      await organiserGreenRoom.sendUserToOnstageFromBackStageUsingRaiseHand(
        organiserFirstName
      );
    });

    await test.step(`Verify Attendee's view.`, async () => {
      await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
        1
      );
    });

    await test.step("Verify if emojis are Visible", async () => {
      await attendeeStagePage.verifyEmojisAreVisible();
    });

    await test.step(`Organiser ends the session`, async () => {
      await organiserGreenRoom.endTheSession();
    });

    await test.step("Verify if emojis are not Visible", async () => {
      await attendeeStagePage.verifyEmojisAreNotVisible();
    });
  });

  test("TC003: Organiser enables the emojis of choice and attendee is able to view the same emojis in Session-Run | @oldStage @session", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-193",
    });

    let venueSetupPage = new VenueSetup(organiserEventSidePage);

    await test.step(`Organizer deselects emojis.`, async () => {
      await venueSetupPage.selectEmojisOfChoice(stageId, eventId);
    });

    await test.step(`Organizer initiate session.`, async () => {
      await organiserGreenRoom.disableDryRun();
      await organiserGreenRoom.startTheSession();
    });

    await test.step("Attendee login to event via Magic-Link.", async () => {
      await attendeePage.goto(magicLink);
      await attendeePage.click("text=Enter Now");
      await attendeePage.goto(stageUrl);
    });

    await test.step(`Verify Attendee's view.`, async () => {
      await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
        0
      );
    });

    await test.step("Verify if emojis are Visible", async () => {
      await attendeeStagePage.verifyEmojisAreNotVisible();
    });

    await test.step(`Organizer joins the stage.`, async () => {
      await organiserGreenRoom.sendUserToOnstageFromBackStageUsingRaiseHand(
        organiserFirstName
      );
    });

    await test.step(`Verify Attendee's view.`, async () => {
      await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
        1
      );
    });

    await test.step("Get emojis on stage", async () => {
      await attendeeStagePage.verifyEmojisAreVisible();
    });

    await test.step(`Organiser ends the session`, async () => {
      await organiserGreenRoom.endTheSession();
    });
  });
});
