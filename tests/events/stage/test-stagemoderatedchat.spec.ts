import { APIRequestContext, BrowserContext, test } from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import {
  createNewEvent,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { StageController } from "../../../controller/StageController";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@stage @interaction-panel`, async () => {
  test.slow();
  // test.describe.configure({ retries: 1 });
  let eventId;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let attendeeOneBrowserContext: BrowserContext;
  let attendeeTwoBrowserContext: BrowserContext;
  let eventController: EventController;
  let attendeeLandingPage: string;
  let stageController: StageController;
  let stageId: string;
  let stageUrl: string;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiContext = orgBrowserContext.request;
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: "moderated-stage automation",
    });
    console.log(`event id ${eventId}`);
    attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    attendeeTwoBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });

    eventController = new EventController(orgApiContext, eventId);

    await test.step("Fetching deafult stage id", async () => {
      stageId = await eventController.getDefaultStageId();
    });
    eventController.disableStudioAsBackstage(stageId);
    //add the stage controller
    stageController = new StageController(orgApiContext, eventId, stageId);

    await test.step(`Enable the moderated chat for this stage`, async () => {
      await stageController.updateStageEngagementSettingsForThisStage({
        hasChat: true,
        hasChatModerate: true,
      });
    });
    let eventInfoDto = new EventInfoDTO(eventId, "stage automation", true);
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
    stageUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/stages/${stageId}`;
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
    await attendeeOneBrowserContext?.close();
    await attendeeTwoBrowserContext?.close();
  });

  test("TC001: @moderated-chat verify e2e flow of attendee posting a chat message and organiser moderate it to approve it ", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-494/qat-488-verify-flow-of-chat-message-approval-by-organiser",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-495/qat-487-verify-organiser-only-sees-moderated-chat-button-and-attendees",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-496/qat-486-verify-organiser-is-able-to-enable-disable-moderated-chat",
    });

    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let attendeeTwoPage = await attendeeTwoBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let attendeeOneStage: StagePage;
    let attendeeTwoStage: StagePage;
    let organiserStage: StagePage;

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const attendeeTwoFirstName = "prateekAttendeeTwo";
    const attendeeTwoLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    const organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let attendeeEmailTwo = DataUtl.getRandomAttendeeEmail();

    //messages
    const orgMessageOne = "Hi , I am organiser 1";
    const attendeeMessageOne = "Hi , I am attendee 1";

    await test.step(`organiser invites attendee  1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await test.step(`organiser invites attendee  2 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeTwoFirstName,
        lastName: attendeeTwoLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailTwo,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the stage`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      await attendeeOnePage.goto(stageUrl);
    });
    await test.step(`attendee 2 logs into the stage`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailTwo
      );
      await attendeeTwoPage.goto(magicLink);
      await attendeeTwoPage.click(`text=Enter Now`);
      await attendeeTwoPage.waitForURL(/lobby/);
      await attendeeTwoPage.goto(stageUrl);
    });

    await test.step(`organiser logs into the stage`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(stageUrl);
    });
    //now attendee clicks on interaction panel
    await test.step(`Verify organiser messages does not go under moderation and are visible to all user present inside stage`, async () => {
      organiserStage = new StagePage(organiserPage);
      await organiserStage.closeWelcomeStagePopUp();
      await organiserStage.getChatComponent.sendChatMessage(orgMessageOne);

      await test.step(`Verify organiser is able to see his message on chat panel`, async () => {
        await organiserStage.getChatComponent.verifyMessageWithGivenContentIsVisible(
          orgMessageOne
        );
      });
      await test.step(`Verify attendee 1 is able to see his message on chat panel`, async () => {
        attendeeOneStage = new StagePage(attendeeOnePage);
        await attendeeOneStage.getChatComponent.verifyMessageWithGivenContentIsVisible(
          orgMessageOne
        );
      });
      await test.step(`Verify attendee 2 is able to see his message on chat panel`, async () => {
        attendeeTwoStage = new StagePage(attendeeTwoPage);
        await attendeeTwoStage.getChatComponent.verifyMessageWithGivenContentIsVisible(
          orgMessageOne
        );
      });

      await test.step(`Verify attendee 1 if posts message is not visible to anyone inside chat container and is visible to org under moderation container`, async () => {
        await attendeeOneStage.getChatComponent.sendChatMessage(
          attendeeMessageOne
        );
        await test.step(`Verify organiser is not able to see his message on chat panel`, async () => {
          await organiserStage.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
            attendeeMessageOne
          );
        });
        await test.step(`Verify attendee 1 is not  able to see his message on chat panel`, async () => {
          await attendeeOneStage.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
            attendeeMessageOne
          );
        });
        await test.step(`Verify attendee 2 is not able to see his message on chat panel`, async () => {
          await attendeeTwoStage.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
            attendeeMessageOne
          );
        });

        await test.step(`verify organiser sees chat moderator button inside chat container`, async () => {
          await organiserStage.getChatComponent.verifyModeratedChatButtonIsVisibleOnChatContainer();
        });

        await test.step(`verify attendees does not sees chat moderator button inside chat container`, async () => {
          await attendeeOneStage.getChatComponent.verifyModeratedChatButtonIsNotVisibleOnChatContainer();
        });
      });

      await test.step(`Organiser clicks on moderated chat button, then approves the message, then verify its visible to all on chat container `, async () => {
        await test.step("organiser clicks on chat moderator button and verify moderated chat container is visible", async () => {
          await organiserStage.getChatComponent.updateStatusOfChatWaitingForModeration(
            attendeeMessageOne,
            true
          );
        });
      });

      await test.step(`Now verify post approval by organiser, chat message is visible in the chat container to all user`, async () => {
        await test.step(`Verify organiser is now able to see attendee 1 message on chat panel`, async () => {
          await organiserStage.getChatComponent.verifyMessageWithGivenContentIsVisible(
            attendeeMessageOne
          );
        });
        await test.step(`Verify attendee 1 is now  able to see his message on chat panel`, async () => {
          await attendeeOneStage.getChatComponent.verifyMessageWithGivenContentIsVisible(
            attendeeMessageOne
          );
        });
        await test.step(`Verify attendee 2 is now  able to see attendee 1 message on chat panel`, async () => {
          await attendeeTwoStage.getChatComponent.verifyMessageWithGivenContentIsVisible(
            attendeeMessageOne
          );
        });
      });
    });
  });

  test("TC002: @moderated-chat verify e2e flow of attendee posting a chat message and organiser moderate it to reject it ", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-495/qat-487-verify-organiser-only-sees-moderated-chat-button-and-attendees",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-496/qat-486-verify-organiser-is-able-to-enable-disable-moderated-chat",
    });

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-493/qat-489-verify-flow-of-chat-rejection-under-moderation-by-organiser",
    });
    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let attendeeTwoPage = await attendeeTwoBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let attendeeOneStage: StagePage;
    let attendeeTwoStage: StagePage;
    let organiserStage: StagePage;

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const attendeeTwoFirstName = "prateekAttendeeTwo";
    const attendeeTwoLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    const organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let attendeeEmailTwo = DataUtl.getRandomAttendeeEmail();

    //messages
    const orgMessageOne = "Hi , I am organiser 1";
    const attendeeMessageOne = "Hi , I am attendee 1";

    await test.step(`organiser invites attendee  1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await test.step(`organiser invites attendee  2 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeTwoFirstName,
        lastName: attendeeTwoLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailTwo,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the stage`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      await attendeeOnePage.goto(stageUrl);
    });
    await test.step(`attendee 2 logs into the stage`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailTwo
      );
      await attendeeTwoPage.goto(magicLink);
      await attendeeTwoPage.click(`text=Enter Now`);
      await attendeeTwoPage.waitForURL(/lobby/);
      await attendeeTwoPage.goto(stageUrl);
    });

    await test.step(`organiser logs into the stage`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(stageUrl);
    });

    //now attendee clicks on interaction panel
    await test.step(`Verify organiser messages does not go under moderation and are visible to all user present inside stage`, async () => {
      organiserStage = new StagePage(organiserPage);
      await organiserStage.closeWelcomeStagePopUp();
      await organiserStage.getChatComponent.sendChatMessage(orgMessageOne);

      await test.step(`Verify organiser is able to see his message on chat panel`, async () => {
        await organiserStage.getChatComponent.verifyMessageWithGivenContentIsVisible(
          orgMessageOne
        );
      });
      await test.step(`Verify attendee 1 is able to see his message on chat panel`, async () => {
        attendeeOneStage = new StagePage(attendeeOnePage);
        await attendeeOneStage.getChatComponent.verifyMessageWithGivenContentIsVisible(
          orgMessageOne
        );
      });
      await test.step(`Verify attendee 2 is able to see his message on chat panel`, async () => {
        attendeeTwoStage = new StagePage(attendeeTwoPage);
        await attendeeTwoStage.getChatComponent.verifyMessageWithGivenContentIsVisible(
          orgMessageOne
        );
      });
    });

    await test.step(`Verify attendee 1 if posts message is not visible to anyone inside chat container and is visible to org under moderation container`, async () => {
      await attendeeOneStage.getChatComponent.sendChatMessage(
        attendeeMessageOne
      );
      await test.step(`Verify organiser is not able to see his message on chat panel`, async () => {
        await organiserStage.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });
      await test.step(`Verify attendee 1 is not  able to see his message on chat panel`, async () => {
        await attendeeOneStage.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });
      await test.step(`Verify attendee 2 is not able to see his message on chat panel`, async () => {
        await attendeeTwoStage.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });

      await test.step(`verify organiser sees chat moderator button inside chat contianer`, async () => {
        await organiserStage.getChatComponent.verifyModeratedChatButtonIsVisibleOnChatContainer();
      });

      await test.step(`verify attendees does not sees chat moderator button inside chat contianer`, async () => {
        await attendeeOneStage.getChatComponent.verifyModeratedChatButtonIsNotVisibleOnChatContainer();
      });
    });

    await test.step(`Organiser clicks on moderated chat button, then rejects the message, then verify its not  visible to all on chat container `, async () => {
      await test.step("organiser clicks on chat moderator button and verify moderated chat container is visible", async () => {
        await organiserStage.getChatComponent.updateStatusOfChatWaitingForModeration(
          attendeeMessageOne,
          false
        );
      });
    });

    await test.step(`Now verify post rejection by organiser, chat message is not visible in the chat container to all user`, async () => {
      await test.step(`Verify organiser is not able to see attendee 1 message on chat panel`, async () => {
        await organiserStage.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });
      await test.step(`Verify attendee 1 is not  able to see his message on chat panel`, async () => {
        await attendeeOneStage.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });
      await test.step(`Verify attendee 2 is not  able to see attendee 1 message on chat panel`, async () => {
        await attendeeTwoStage.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });
    });
  });
});
