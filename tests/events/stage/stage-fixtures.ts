import {
  test as base,
  BrowserContext,
  Page,
  APIRequestContext,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { EventController } from "../../../controller/EventController";
import EventEntryType from "../../../enums/eventEntryEnum";

interface ContextDetails {
  apiRequestContext: APIRequestContext;
  browserContext: BrowserContext;
  page: Page;
}

class BrowserContextInfo implements ContextDetails {
  public readonly apiRequestContext: APIRequestContext;
  constructor(
    public readonly browserContext: BrowserContext,
    public readonly page: Page
  ) {
    this.browserContext = browserContext;
    this.page = page;
    this.apiRequestContext = this.browserContext.request;
  }
}

class EventInfoModal {
  constructor(
    public readonly eventId: number,
    public readonly eventTitle: string,
    public eventTimeZone: string,
    public isAttendeeMagicLinkEnabled: boolean,
    public isSpeakerMagicLinkEnabled: boolean
  ) {
    this.eventId = eventId;
    this.eventTitle = eventTitle;
    this.eventTimeZone = eventTimeZone;
    this.isAttendeeMagicLinkEnabled = isAttendeeMagicLinkEnabled;
    this.isSpeakerMagicLinkEnabled = isSpeakerMagicLinkEnabled;
  }
  public getSpeakerLandingPageLink() {
    return `${DataUtl.getApplicationTestDataObj()["baseUrl"]}/p/event/${
      this.eventId
    }`;
  }

  public getAttendeeLandingPageLink() {
    return `${DataUtl.getApplicationTestDataObj()["baseUrl"]}/p/a/event/${
      this.eventId
    }`;
  }
}

type eventInfoFixture = {
  eventInfoModal: EventInfoModal;
};

// interface MultipleBrowsersContext {
//   attendeeBrowserContextInfo: ContextDetails;
//   speakerBrowserContextInfo: ContextDetails;
// }

type UserBrowserContextFixtures = {
  attendeeBrowserContextInfo: ContextDetails;
  speakerBrowserContextInfo: ContextDetails;
  organiserBrowserContextInfo: ContextDetails;
};

export const stageTests = base.extend<
  UserBrowserContextFixtures & eventInfoFixture
>({
  organiserBrowserContextInfo: async ({}, use) => {
    const organiserBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    const organsierPage = await organiserBrowserContext.newPage();
    const browserInfo = new BrowserContextInfo(
      organiserBrowserContext,
      organsierPage
    );
    await use(browserInfo);
    await organiserBrowserContext.close();
  },
  eventInfoModal: async ({ organiserBrowserContextInfo }, use) => {
    const orgApiRequestContext = organiserBrowserContextInfo.apiRequestContext;
    const eventTitle = DataUtl.getRandomEventTitle();
    const eventId = await EventController.generateNewEvent(
      orgApiRequestContext,
      { event_title: eventTitle }
    );
    //enable attendee magic link and speaker magic link for reg based event
    const eventController = new EventController(orgApiRequestContext, eventId);
    await eventController.updateEventLandingPageSettings({
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
      isSpeakermagicLinkEnabled: true,
      isGuestAuthEnabled: true,
    });
    const eventInfoModal = new EventInfoModal(
      eventId,
      eventTitle,
      "Asia/Kolkata",
      true,
      true
    );
    await organiserBrowserContextInfo.page.goto(
      eventInfoModal.getAttendeeLandingPageLink()
    );
    await use(eventInfoModal);
  },
  attendeeBrowserContextInfo: async ({ eventInfoModal }, use) => {
    const attendeeBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    const attendeePage = await attendeeBrowserContext.newPage();
    await attendeePage.goto(eventInfoModal.getAttendeeLandingPageLink());
    const browserInfo = new BrowserContextInfo(
      attendeeBrowserContext,
      attendeePage
    );
    await use(browserInfo);
    await attendeeBrowserContext.close();
  },
  speakerBrowserContextInfo: async ({ eventInfoModal }, use) => {
    const speakerBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    const speakerPage = await speakerBrowserContext.newPage();
    await speakerPage.goto(eventInfoModal.getSpeakerLandingPageLink());
    const browserInfo = new BrowserContextInfo(
      speakerBrowserContext,
      speakerPage
    );
    await use(browserInfo);
    await speakerBrowserContext.close();
  },
});
