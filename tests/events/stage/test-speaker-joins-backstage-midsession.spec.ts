import test, { APIRequestContext, BrowserContext } from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { GreenRoom } from "../../../page-objects/events-pages/site-modules/GreenRoom";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import {
  createNewEvent,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import BrowserName from "../../../enums/BrowserEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@stage @show-flow @speaker  @AV`, async () => {
  test.slow();
  let eventId;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let attendeeOneBrowserContext: BrowserContext;
  let speakerOneBrowserContext: BrowserContext;
  let eventController: EventController;
  let defaultStageUrl: string;
  let attendeeLandingPage: string;
  let stageId;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      browserName: BrowserName.CHROME,
    });

    attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
      browserName: BrowserName.CHROME,
      laodOrganiserCookies: false,
    });

    speakerOneBrowserContext = await BrowserFactory.getBrowserContext({
      browserName: BrowserName.CHROME,
      laodOrganiserCookies: false,
    });
    orgApiContext = orgBrowserContext.request;

    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: "room automation",
    });

    eventController = new EventController(orgApiContext, eventId);

    stageId = await eventController.getDefaultStageId();
    eventController.disableStudioAsBackstage(stageId);

    await test.step("Disabling onboarding checks for speaker and attendee access group", async () => {
      await eventController.getAccessGroupController.handleOnboardingChecksForSpeaker(
        false
      );
      await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
        false
      );
    });

    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
      isSpeakermagicLinkEnabled: true,
    });

    let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;

    defaultStageUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/stages/${stageId}`;
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
    await attendeeOneBrowserContext?.close();
    await speakerOneBrowserContext?.close();
  });

  test("TC001: Speaker joins backstage mid-session. || Old stage.", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-104/e2e-flow-of-organiser-speaker-and-attendee-joining-stage-in-different",
    });

    let attendeeOnePage;
    let organiserPage;
    let speakerOnePage;
    let organiserStagePage: StagePage;
    let attendeeStagePage: StagePage;
    let speakerStagePage: StagePage;
    let organiserGreenRoom: GreenRoom;
    let speakerGreenRoom: GreenRoom;

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const speakerOneFirstName = "prateekSpeakerOne";
    const speakerOneLastName = "QA";

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let speakerOneEmail = DataUtl.getRandomSpeakerEmail();

    await test.step("Attendee page.", async () => {
      attendeeOnePage = await attendeeOneBrowserContext.newPage();
    });

    await test.step("Organiser page.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step("Speaker page.", async () => {
      speakerOnePage = await speakerOneBrowserContext.newPage();
    });

    await test.step("Get random attendee email.", async () => {
      attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    });

    await test.step("Get random speaker email.", async () => {
      speakerOneEmail = DataUtl.getRandomSpeakerEmail();
    });

    await test.step(`Organiser invites attendee and speaker to the event.`, async () => {
      //Attendee joins via invite and enters the default stage
      await test.step(`Organiser invites Attendee.`, async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeOneFirstName,
          lastName: attendeeOneLastName,
          phoneNumber: "1234567899",
          email: attendeeEmailOne,
        });
      });

      //Speaker joins via invite and enters the default stage
      await test.step(`Organizer invites Speaker.`, async () => {
        await eventController.inviteSpeakerToTheEventByApi(
          speakerOneEmail,
          speakerOneFirstName,
          speakerOneLastName
        );
      });
    });

    await test.step(`Organizer and Attendee join the event and enter default stage.`, async () => {
      await test.step(`Attendee joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeEmailOne
        );

        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(defaultStageUrl);
        attendeeStagePage = new StagePage(attendeeOnePage);
      });

      await test.step(`Organizer joins the event and join backstage via default stage.`, async () => {
        await organiserPage.goto(attendeeLandingPage);
        await organiserPage.click(`text=Enter Now`);
        await organiserPage.goto(defaultStageUrl);
        organiserStagePage = new StagePage(organiserPage);
        const orgAvModal =
          await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
        await orgAvModal.isVideoStreamLoadedOnAVModal();
        await orgAvModal.clickOnJoinButtonOnAvModal();

        await test.step(`Verify Organiser's and Speaker's presence in backstage.`, async () => {
          organiserGreenRoom = new GreenRoom(organiserPage);
          await organiserGreenRoom.isGreenRoomVisible();
          await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(1);
        });
      });
    });

    await test.step(`Green room validation.`, async () => {
      await test.step(`Verify Attendee view of stage || Empty stage`, async () => {
        await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
          0
        );
      });

      await test.step(`Verify Organizer view of green room || 2 streams in green roon`, async () => {
        await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(1);
      });
    });

    await test.step("Verify Organizer green room.", async () => {
      await organiserStagePage.getGreenRoomContainer.verifyCountOfStreamInsideGreenRoomMatches(
        1
      );
    });

    await test.step(`Organizer initiate session and joins the stage.`, async () => {
      await organiserGreenRoom.disableDryRun();
      await organiserGreenRoom.startTheSession();
      await organiserPage.waitForTimeout(2000);
    });

    await test.step(`Main stage validation.`, async () => {
      await test.step(`Verify Organiser's view || Organizer in green room.`, async () => {
        await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(1);
      });

      await test.step(`Verify Attendee's view || No stream.`, async () => {
        await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
          0
        );
      });
    });

    await test.step(`Speaker joins the event and enter default stage.`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerOneEmail
      );

      await speakerOnePage.goto(magicLink);
      await speakerOnePage.click(`text=Enter Now`);
      await speakerOnePage.goto(defaultStageUrl);
      speakerStagePage = new StagePage(speakerOnePage);

      const speakerAvModal =
        await speakerStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
      await speakerAvModal.isVideoStreamLoadedOnAVModal();
      await speakerAvModal.clickOnJoinButtonOnAvModal();

      await test.step(`Verify Speaker's presence in backstage.`, async () => {
        speakerGreenRoom = new GreenRoom(speakerOnePage);
        await speakerGreenRoom.isGreenRoomVisible();
        await speakerGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });
    });

    await test.step(`Organizer sends Speaker on stage.`, async () => {
      await organiserStagePage.getGreenRoomContainer.sendUserToOnstageFromBackStageUsingRaiseHand(
        speakerOneFirstName
      );
    });

    await test.step(`Verify Speaker view of main stage || Speaker stream in main stage.`, async () => {
      await speakerGreenRoom.verifyCountOfStreamInsideMainStageContainer(1);
    });

    await test.step(`Organiser ends the session.`, async () => {
      await organiserGreenRoom.endTheSession();
    });
  });
});
