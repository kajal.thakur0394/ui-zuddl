import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { JoinRequestTab } from "../../../page-objects/events-pages/live-side-components/JoinRequestTab";
import { GreenRoom } from "../../../page-objects/events-pages/site-modules/GreenRoom";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import {
  createNewEvent,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { StageController } from "../../../controller/StageController";
import { AVModal } from "../../../page-objects/events-pages/live-side-components/AvModal";
import { RoomModule } from "../../../page-objects/events-pages/site-modules/Rooms";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@stage @breakout @AV`, async () => {
  test.describe.configure({ retries: 2 });

  let eventId;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let attendeeOneBrowserContext: BrowserContext;
  let attendeeTwoBrowserContext: BrowserContext;
  let speakerOneBrowserContext: BrowserContext;
  let eventController: EventController;
  let defaultStageUrl: string;
  let attendeeLandingPage: string;
  let stageId;

  test.beforeEach(async () => {
    test.slow();

    //organiser
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = orgBrowserContext.request;
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: "breakout automation",
    });
    //attendee 1
    attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    //attendee 2
    attendeeTwoBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    //speaker 1
    speakerOneBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
      isSpeakermagicLinkEnabled: true,
    });

    eventController = new EventController(orgApiContext, eventId);
    stageId = await eventController.getDefaultStageId();
    let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
    defaultStageUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/stages/${stageId}`;
    eventController.disableStudioAsBackstage(stageId);

    //enable breakout room for this event default stage
    let stageController = new StageController(orgApiContext, eventId, stageId);
    await stageController.enableBreakoutRoomsForThisStage(1);
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
    await attendeeOneBrowserContext?.close();
    await speakerOneBrowserContext?.close();
    await attendeeTwoBrowserContext?.close();
  });

  test("TC001: verify organiser is able to switch between breakout rooms seamlessly during Dry run", async () => {
    // linear ticket info
    test.info().annotations.push({
      type: "P1",
      description:
        "https://linear.app/zuddl/issue/QAT-243/automation-scenario-verify-organiser-is-able-to-switch-between",
    });
    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let attendeeTwoPage = await attendeeTwoBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let speakerOnePage = await speakerOneBrowserContext.newPage();

    //stage pages
    let organiserStagePage: StagePage;
    let attendeeOneStagePage: StagePage;
    let attendeeTwoStagePage: StagePage;

    let speakerStagePage: StagePage;
    // green room pages
    let organiserGreenRoom: GreenRoom;
    let speakerGreenRoom: GreenRoom;
    // join tab component
    let attendeeOneJoinTab: JoinRequestTab;
    let organiserJoinTab: JoinRequestTab;

    //breakout room page
    let organiserRoom: RoomModule;

    //attendee 1
    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    //attendee 2
    let attendeeEmailTwo = DataUtl.getRandomAttendeeEmail();
    const attendeeTwoFirstName = "prateekAttendeeTwo";
    const attendeeTwoLastName = "QA";

    //speaker 1
    let speakerOneEmail = DataUtl.getRandomSpeakerEmail();
    const speakerOneFirstName = "prateekSpeakerOne";
    const speakerOneLastName = "QA";

    //organiser
    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    const organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    //disable onboarding checks
    await eventController.disableOnboardingChecksForAttendee();

    await test.step(`organiser invites attendee 1, attendee 2 and speaker 1 to the event`, async () => {
      // invite attendee 1 and he logs in and then go to default stage
      await test.step(`organiser invites attendee  1 to the event`, async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeOneFirstName,
          lastName: attendeeOneLastName,
          phoneNumber: "1234567899",
          email: attendeeEmailOne,
        });
      });

      // invite attendee 2 and he logs in and then go to default stage
      await test.step(`organiser invites attendee  2 to the event`, async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeTwoFirstName,
          lastName: attendeeTwoLastName,
          phoneNumber: "1234567899",
          email: attendeeEmailTwo,
        });
      });
      // invite speaker 1 and he logs in and then he enters to the default stage
      await test.step(`organiser invites speaker 1 to the event`, async () => {
        await eventController.inviteSpeakerToTheEventByApi(
          speakerOneEmail,
          speakerOneFirstName,
          speakerOneLastName
        );
      });
    });

    await test.step(`speaker, organiser and attendee all logs into the event and goes to stage or inside stage`, async () => {
      await test.step(`attendee 1 logs into the event and go to default stage`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeEmailOne
        );
        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(defaultStageUrl);
        attendeeOneStagePage = new StagePage(attendeeOnePage);
      });

      await test.step(`attendee 2 logs into the event and go to default stage`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeEmailTwo
        );
        await attendeeTwoPage.goto(magicLink);
        await attendeeTwoPage.click(`text=Enter Now`);
        await attendeeTwoPage.goto(defaultStageUrl);
        attendeeTwoStagePage = new StagePage(attendeeTwoPage);
      });

      await test.step(`speaker 1 logs into the event and go to default stage`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          eventId,
          speakerOneEmail
        );
        await speakerOnePage.goto(magicLink);
        await speakerOnePage.click(`text=Enter Now`);
        await speakerOnePage.goto(defaultStageUrl);
        speakerStagePage = new StagePage(speakerOnePage);

        const speakerAvModal =
          await speakerStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
        await speakerAvModal.isVideoStreamLoadedOnAVModal();
        await speakerAvModal.clickOnJoinButtonOnAvModal();

        await test.step(`verify speaker has joined backstage and he is able to see himself`, async () => {
          speakerGreenRoom = new GreenRoom(speakerOnePage);
          await speakerGreenRoom.isGreenRoomVisible();
          await speakerGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(1);
        });
      });
      await test.step(`organiser logs into the event and go to default stage and go to backstage`, async () => {
        await organiserPage.goto(attendeeLandingPage);
        await organiserPage.click(`text=Enter Now`);
        await organiserPage.goto(defaultStageUrl);
        organiserStagePage = new StagePage(organiserPage);
        const orgAvModal =
          await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
        await orgAvModal.isVideoStreamLoadedOnAVModal();
        await orgAvModal.clickOnJoinButtonOnAvModal();

        await test.step(`verify organiser has joined backstage and he is able to see himself and speaker stream on green room`, async () => {
          organiserGreenRoom = new GreenRoom(organiserPage);
          await organiserGreenRoom.isGreenRoomVisible();
          await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
        });
      });
    });

    await test.step(`green room validation for org, speaker and attendee when dry run is not started`, async () => {
      await test.step(`verify speaker sees 2 streams on green roon`, async () => {
        await speakerGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });
      await test.step(`verify org sees 2 streams on green roon`, async () => {
        await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });
    });

    await test.step(`organiser starts the dry run`, async () => {
      await organiserGreenRoom.startTheDryRun();
      await organiserPage.waitForTimeout(2000);
    });

    await test.step(`Organiser starts the breakout room and click on exit stage prompt to move out of stage`, async () => {
      await organiserGreenRoom.openExtraSection();
      await organiserGreenRoom.openBreakoutRoomsSection();
      await organiserGreenRoom.startBreakoutRooms();
      await organiserGreenRoom.isExitStagePromptVisibleToOrganiser();
      await organiserGreenRoom.clickOnExitStagePromptDuringBreakout();
      let orgavModal = new AVModal(organiserPage);
      await orgavModal.isVideoStreamLoadedOnAVModal();
      await orgavModal.clickOnJoinButtonOnAvModal();
    });
    await test.step(`verify speaker sees exit stage prompt and click on it to leave stage and go to breakout room`, async () => {
      //speaker part
      await speakerGreenRoom.isExitStagePromptVisibleToOrganiser();
      await speakerGreenRoom.clickOnExitStagePromptDuringBreakout();
      let speakeravModal = new AVModal(speakerOnePage);
      await speakeravModal.isVideoStreamLoadedOnAVModal();
      await speakeravModal.clickOnJoinButtonOnAvModal();
    });

    await test.step(`Attendee 1 enters the breakout room by click on join button on his av Modal`, async () => {
      let attendeeOneAvModal = new AVModal(attendeeOnePage);
      await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
    });

    await test.step(`Attendee 2 enters the breakout room by click on join button on his av Modal`, async () => {
      let attendeeTwoAvModal = new AVModal(attendeeTwoPage);
      await attendeeTwoAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeTwoAvModal.clickOnJoinButtonOnAvModal();
    });

    await test.step(`Verify organiser has joined the room`, async () => {
      organiserRoom = new RoomModule(organiserPage);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    await test.step(`Verify attendee 1 has joined the room`, async () => {
      let attendeeOneRoom = new RoomModule(attendeeOnePage);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
    });
    await test.step(`Verify attendee 2 has joined the room`, async () => {
      let attendeeTwoRoom = new RoomModule(attendeeTwoPage);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
    });
    await test.step(`Verify speaker has joined the room`, async () => {
      let speakerRoom = new RoomModule(speakerOnePage);
      await speakerRoom.verifyUserStreamIsVisible(speakerOneFirstName);
    });

    await test.step(`Organiser clicks on join button on the other room to leave the current room and join another one`, async () => {
      const eachbreakOutRoomsListForOrganiser = organiserPage.locator(
        "div[class^='styles-module__brokenStageSettings'] div[class^='styles-module__room_']"
      );
      const joinButtonOnRoom = eachbreakOutRoomsListForOrganiser
        .filter({
          hasText: "Join Room",
        })
        .locator("div[class^='styles-module__joinButton']");

      const joinedRoomTitleLocator = eachbreakOutRoomsListForOrganiser
        .filter({
          hasText: "Leave Room",
        })
        .locator("div[class^='styles-module__roomName']");

      let joinedRoomTitle;

      await test.step(`Organiser fetches the title of his current joined room`, async () => {
        joinedRoomTitle = await joinedRoomTitleLocator.textContent();
        console.log(`Joined room title is ${joinedRoomTitle}`);
      });
      await test.step(`Verify 4 rooms are visible to organiser on left hand side in list`, async () => {
        await expect(eachbreakOutRoomsListForOrganiser).toHaveCount(4);
      });
      await test.step(`Organiser clicks on join room on the another room appearing in list`, async () => {
        await joinButtonOnRoom.nth(1).click();
      });
      await test.step(`Verify now organiser sees total of 2 stream in the room`, async () => {
        await organiserRoom.verifyCountOfStreamInsideRoomMatches(2);
      });

      await test.step(`Verify organiser is able switch back to his original room`, async () => {
        const orgPreviousRoomJoinButton = eachbreakOutRoomsListForOrganiser
          .filter({
            hasText: joinedRoomTitle,
          })
          .locator("div[class^='styles-module__joinButton']");
        await orgPreviousRoomJoinButton.click();
      });
      await test.step(`Verify now organiser sees total of 1 stream in the room`, async () => {
        await organiserRoom.verifyCountOfStreamInsideRoomMatches(1);
      });
    });
  });
});
