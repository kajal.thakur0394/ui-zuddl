import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { ChatComponent } from "../../../page-objects/events-pages/live-side-components/ChatComponent";
import { JoinRequestTab } from "../../../page-objects/events-pages/live-side-components/JoinRequestTab";
import { GreenRoom } from "../../../page-objects/events-pages/site-modules/GreenRoom";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import {
  createNewEvent,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import BrowserName from "../../../enums/BrowserEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@stage @show-flow @chat  @AV`, async () => {
  test.describe.configure({ retries: 2 });
  let eventId;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let attendeeOneBrowserContext: BrowserContext;
  let speakerOneBrowserContext: BrowserContext;
  let eventController: EventController;
  let defaultStageUrl: string;
  let attendeeLandingPage: string;
  let stageId;

  test.beforeEach(async () => {
    test.slow();
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      browserName: BrowserName.CHROME,
      laodOrganiserCookies: true,
    });
    orgApiContext = orgBrowserContext.request;
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: "room automation",
    });
    attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
      browserName: BrowserName.CHROME,
      laodOrganiserCookies: false,
    });

    speakerOneBrowserContext = await BrowserFactory.getBrowserContext({
      browserName: BrowserName.CHROME,
      laodOrganiserCookies: false,
    });

    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
      isSpeakermagicLinkEnabled: true,
    });

    eventController = new EventController(orgApiContext, eventId);
    stageId = await eventController.getDefaultStageId();
    let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
    defaultStageUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/stages/${stageId}`;
    eventController.disableStudioAsBackstage(stageId);
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
    await attendeeOneBrowserContext?.close();
    await speakerOneBrowserContext?.close();
  });

  test("TC001: @backstage-chat @event-chat Organiser during show flow verifies that chat functionality is working fine on event and backstage chat ", async () => {
    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let speakerOnePage = await speakerOneBrowserContext.newPage();

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const speakerOneFirstName = "prateekSpeakerOne";
    const speakerOneLastName = "QA";

    //organiser
    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    const organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let speakerOneEmail = DataUtl.getRandomSpeakerEmail();

    let organiserStagePage: StagePage;
    let attendeeStagePage: StagePage;
    let speakerStagePage: StagePage;
    let organiserGreenRoom: GreenRoom;
    let speakerGreenRoom: GreenRoom;
    let attendeeOneJoinTab: JoinRequestTab;
    let organiserJoinTab: JoinRequestTab;

    // event chat message
    const attendeeMessageEventChat: string =
      "I am attendee, messaging in event chat 1";
    const speakerMessageEventChat: string =
      "I am speaker, messaging in event chat 2";
    const organisereMessageEventChat: string =
      "I am organiser, messaging in event chat 3";
    const speakerMessageEventChat2: string =
      "I am speaker, again messaging in event chat 4";

    // back stage chat message
    const speakerMessageBackstageChat: string =
      "I am speaker, messaging in backstage chat 1";
    const speakerMessageBackstageChat2: string =
      "I am speaker, again messaging in backstage chat 2";
    const organisereMessageBackstageChat: string =
      "I am organiser, messaging in backstage chat 2";

    // chat components
    let attendeeChatComponent: ChatComponent;
    let speakerChatComponent: ChatComponent;
    let organiserChatComponent: ChatComponent;

    //disable onboarding checks
    await eventController.disableOnboardingChecksForAttendee();

    await test.step(`organiser invites attendee and speaker to the event`, async () => {
      // invite attendee 1 and he logs in and then go to default stage
      await test.step(`organiser invites attendee  1 to the event`, async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeOneFirstName,
          lastName: attendeeOneLastName,
          phoneNumber: "1234567899",
          email: attendeeEmailOne,
        });
      });
      // invite speaker 1 and he logs in and then he enters to the default stage
      await test.step(`organiser invites speaker 1 to the event`, async () => {
        await eventController.inviteSpeakerToTheEventByApi(
          speakerOneEmail,
          speakerOneFirstName,
          speakerOneLastName
        );
      });
    });

    await test.step(`speaker, organiser and attendee all logs into the event and goes to stage or inside stage`, async () => {
      await test.step(`attendee 1 logs into the event and go to default stage`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeEmailOne
        );
        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(defaultStageUrl);
        attendeeStagePage = new StagePage(attendeeOnePage);
      });

      await test.step(`speaker 1 logs into the event and go to default stage`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          eventId,
          speakerOneEmail
        );
        await speakerOnePage.goto(magicLink);
        await speakerOnePage.click(`text=Enter Now`);
        await speakerOnePage.goto(defaultStageUrl);
        speakerStagePage = new StagePage(speakerOnePage);

        const speakerAvModal =
          await speakerStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
        await speakerAvModal.isVideoStreamLoadedOnAVModal();
        await speakerAvModal.clickOnJoinButtonOnAvModal();

        await test.step(`verify speaker has joined backstage and he is able to see himself`, async () => {
          speakerGreenRoom = new GreenRoom(speakerOnePage);
          await speakerGreenRoom.isGreenRoomVisible();
          await speakerGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(1);
        });
      });
      await test.step(`organiser logs into the event and go to default stage and go to backstage`, async () => {
        await organiserPage.goto(attendeeLandingPage);
        await organiserPage.click(`text=Enter Now`);
        await organiserPage.goto(defaultStageUrl);
        organiserStagePage = new StagePage(organiserPage);
        const orgAvModal =
          await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
        await orgAvModal.isVideoStreamLoadedOnAVModal();
        await orgAvModal.clickOnJoinButtonOnAvModal();

        await test.step(`verify organiser has joined backstage and he is able to see himself and speaker stream on green room`, async () => {
          organiserGreenRoom = new GreenRoom(organiserPage);
          await organiserGreenRoom.isGreenRoomVisible();
          await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
        });
      });
    });

    await test.step(`green room validation for org, speaker and attendee when dry run is not started`, async () => {
      await test.step(`verify attendee sees empty state on stage`, async () => {
        await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
          0
        );
      });
      await test.step(`verify speaker sees 2 streams on green roon`, async () => {
        await speakerGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });
      await test.step(`verify org sees 2 streams on green roon`, async () => {
        await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });
    });

    //event chat trigger
    await test.step(`Attendee, organiser and speaker all posts some message in event chat`, async () => {
      await test.step(`attendee post a message : ${attendeeMessageEventChat} on event chat`, async () => {
        attendeeChatComponent =
          await attendeeStagePage.clickOnChatTabOnInteractionPanel();
        await attendeeChatComponent.sendChatMessage(attendeeMessageEventChat);
      });
      await test.step(`speaker post a message : ${speakerMessageEventChat} on event chat`, async () => {
        speakerChatComponent =
          await speakerStagePage.clickOnChatTabOnInteractionPanel();
        await speakerChatComponent.sendChatMessage(speakerMessageEventChat);
      });
      await test.step(`organiser post a message : ${organisereMessageEventChat} on event chat`, async () => {
        organiserChatComponent =
          await organiserStagePage.clickOnChatTabOnInteractionPanel();
        await organiserChatComponent.sendChatMessage(
          organisereMessageEventChat
        );
      });
    });

    //event chat validation when dry run is not started
    await test.step(`Dry run not started: validation of event chat messages by org, speaker from greenrom and attendee in attendee view`, async () => {
      // attendee validation
      await test.step(`attendee side validation of event chat messages`, async () => {
        await attendeeChatComponent.verifyCountOfMessageMatches(3);
        await attendeeChatComponent.verifyMessageWithGivenContentIsVisible(
          attendeeMessageEventChat
        );
        await attendeeChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageEventChat
        );
        await attendeeChatComponent.verifyMessageWithGivenContentIsVisible(
          organisereMessageEventChat
        );
      });

      // speaker validation
      await test.step(`speaker side validation of event chat messages`, async () => {
        await speakerChatComponent.verifyCountOfMessageMatches(3);
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          attendeeMessageEventChat
        );
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageEventChat
        );
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          organisereMessageEventChat
        );
      });

      // organiser validation
      await test.step(`organiser side validation of event chat messages`, async () => {
        await organiserChatComponent.verifyCountOfMessageMatches(3);
        await organiserChatComponent.verifyMessageWithGivenContentIsVisible(
          attendeeMessageEventChat
        );
        await organiserChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageEventChat
        );
        await organiserChatComponent.verifyMessageWithGivenContentIsVisible(
          organisereMessageEventChat
        );
      });
    });

    //backstage chat trigger
    await test.step(`organiser and speaker interact on backstage chat`, async () => {
      await test.step(`organiser open backstage chat tab`, async () => {
        await organiserChatComponent.openBackStageChatTab();
      });
      await test.step(`speaker open backstage chat tab`, async () => {
        await speakerChatComponent.openBackStageChatTab();
      });

      await test.step(`organiser sends chat message in backstage chat`, async () => {
        await organiserChatComponent.sendChatMessage(
          organisereMessageBackstageChat
        );
      });
      await test.step(`speakers sends chat message in backstage chat`, async () => {
        await speakerChatComponent.sendChatMessage(speakerMessageBackstageChat);
      });
      await test.step(`verify attendee does not see those chat message appearing on event chat`, async () => {
        await expect(
          attendeeChatComponent.backstageChatTab,
          "expecting backstage tab to not appear for attendee chat"
        ).not.toBeVisible();
      });
      await test.step(`verify attendee does not see backstage chat section on interaction panel`, async () => {
        await attendeeChatComponent.verifyCountOfMessageMatches(3);
      });
    });

    //backstage chat validation from speaker and organiser point of view when dry run is not started
    await test.step(`validation of event chat messages by org, speaker from greenrom and attendee in attendee view`, async () => {
      // speaker validation
      await test.step(`speaker side validation of backstage chat messages`, async () => {
        await speakerChatComponent.verifyCountOfMessageMatches(2);
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageBackstageChat
        );
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          organisereMessageBackstageChat
        );
      });

      // organiser validation
      await test.step(`organiser side validation of backstage chat messages`, async () => {
        await organiserChatComponent.verifyCountOfMessageMatches(2);
        await organiserChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageBackstageChat
        );
        await organiserChatComponent.verifyMessageWithGivenContentIsVisible(
          organisereMessageBackstageChat
        );
      });
    });

    await test.step(`organiser sends speaker on to the stage from green room and veirfy speaker still has access to same backstage chats`, async () => {
      await organiserStagePage.getGreenRoomContainer.sendUserToOnstageFromBackStageUsingRaiseHand(
        speakerOneFirstName
      );
      // speaker validation
      await test.step(`speaker side validation of backstage chat messages`, async () => {
        await speakerChatComponent.verifyCountOfMessageMatches(2);
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageBackstageChat
        );
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          organisereMessageBackstageChat
        );
      });

      //verify speaker can post a backstage chat message from main stage too
      await test.step(`speaker posts a second message on backstage chat when he is on main stage`, async () => {
        await speakerChatComponent.sendChatMessage(
          speakerMessageBackstageChat2
        );
        await speakerChatComponent.verifyCountOfMessageMatches(3);
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageBackstageChat
        );
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageBackstageChat2
        );
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          organisereMessageBackstageChat
        );
      });

      //verify speaker can post a event chat message from main stage too
      await test.step(`speaker posts a second message on backstage chat when he is on main stage`, async () => {
        await speakerChatComponent.openStageChatTab();
        await speakerChatComponent.sendChatMessage(speakerMessageEventChat2);
        await speakerChatComponent.verifyCountOfMessageMatches(4);
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageEventChat2
        );
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageEventChat
        );
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          organisereMessageEventChat
        );
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          attendeeMessageEventChat
        );
      });
    });

    await test.step(`verify org sees speaker on main stage and speaker also sees himself on main stage`, async () => {
      await organiserStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
        1
      );
      await speakerStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
        1
      );
      await speakerStagePage.getGreenRoomContainer.isGreenRoomNotVisible();
      await organiserStagePage.getGreenRoomContainer.verifyCountOfStreamInsideGreenRoomMatches(
        1
      );
    });

    await test.step(`organiser starts the dry run and sends him self up to the stage`, async () => {
      await organiserGreenRoom.startTheDryRun();
      await organiserPage.waitForTimeout(2000);
      await organiserGreenRoom.sendUserToOnstageFromBackStageUsingRaiseHand(
        organiserFirstName
      );
    });

    //event chat validation when dry run is started
    await test.step(`Dry run started : validation of event chat messages by org, speaker from greenrom and attendee in attendee view`, async () => {
      //opening event chat tabs
      await organiserChatComponent.openStageChatTab();
      await speakerChatComponent.openStageChatTab();

      // attendee validation
      await test.step(`attendee side validation of event chat messages`, async () => {
        await attendeeChatComponent.verifyCountOfMessageMatches(4);
        await attendeeChatComponent.verifyMessageWithGivenContentIsVisible(
          attendeeMessageEventChat
        );
        await attendeeChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageEventChat
        );
        await attendeeChatComponent.verifyMessageWithGivenContentIsVisible(
          organisereMessageEventChat
        );
        await attendeeChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageEventChat2
        );
      });

      // speaker validation
      await test.step(`speaker side validation of event chat messages`, async () => {
        await speakerChatComponent.verifyCountOfMessageMatches(4);
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          attendeeMessageEventChat
        );
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageEventChat
        );
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          organisereMessageEventChat
        );
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageEventChat2
        );
      });

      // organiser validation
      await test.step(`organiser side validation of event chat messages`, async () => {
        await organiserChatComponent.verifyCountOfMessageMatches(4);
        await organiserChatComponent.verifyMessageWithGivenContentIsVisible(
          attendeeMessageEventChat
        );
        await organiserChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageEventChat
        );
        await organiserChatComponent.verifyMessageWithGivenContentIsVisible(
          organisereMessageEventChat
        );
        await organiserChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageEventChat2
        );
      });
    });

    await test.step(`main stage validation from org, speaker and attendee perspective`, async () => {
      await test.step(`verify organiser is able to see him self on main stage`, async () => {
        await organiserGreenRoom.verifyCountOfStreamInsideMainStageContainer(2);
      });

      await test.step(`verify speaker is able to see organiser on main stage`, async () => {
        await speakerGreenRoom.verifyCountOfStreamInsideMainStageContainer(2);
      });
      await test.step(`verify attendee sees organiser stream on main stage from attendee view`, async () => {
        await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
          2
        );
      });
      await test.step(`verify attendee is abl eto see an emoji panel`, async () => {
        await attendeeStagePage.verifyEmojiContainerIsVisible();
      });
    });

    //attendee raise hand to join stage and organiser approves it
    await test.step(`attendee joins the stage by raising hand and organiser approving it`, async () => {
      await test.step(`attendee opening interaction panel and raise hand to send request to join stage`, async () => {
        //by default interaction panel will be open
        await attendeeStagePage.isInteractionPanelOpen();
        attendeeOneJoinTab =
          await attendeeStagePage.openJoinTabOnInteraactionPanel();
        await attendeeOneJoinTab.isVideoStreamLoaded();
        await attendeeOneJoinTab.clickOnSendStageJoinRequest();
        await attendeeOneJoinTab.isWaitingForApprovalButtonIsVisible();
      });
      await test.step(`organiser opening interaction panel and accepting the request`, async () => {
        await organiserStagePage.isInteractionPanelOpen();
        organiserJoinTab =
          await organiserStagePage.openJoinTabOnInteraactionPanel();
        await organiserJoinTab.approveAttendeeJoinRequest(attendeeOneFirstName);
      });
      await test.step(`now attendee joins the stage`, async () => {
        await attendeeOneJoinTab.clickOnJoinStageButton();
      });
    });

    //event chat validation when attendee has joined the start
    await test.step(`Attendee joined stage : validation of event chat messages by org, speaker from greenrom and attendee in attendee view`, async () => {
      await test.step(`organiser opens chat tab as he will be currently on join tab`, async () => {
        await organiserStagePage.clickOnChatTabOnInteractionPanel();
      });

      await test.step(`Attendee also opens the chat tab, since he will be currently on join tab`, async () => {
        await attendeeStagePage.clickOnChatTabOnInteractionPanel();
      });

      // now open the stage chat tab
      await organiserChatComponent.openStageChatTab();
      await speakerChatComponent.openStageChatTab();

      // attendee validation
      await test.step(`attendee side validation of event chat messages`, async () => {
        await attendeeChatComponent.verifyCountOfMessageMatches(4);
        await attendeeChatComponent.verifyMessageWithGivenContentIsVisible(
          attendeeMessageEventChat
        );
        await attendeeChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageEventChat
        );
        await attendeeChatComponent.verifyMessageWithGivenContentIsVisible(
          organisereMessageEventChat
        );
        await attendeeChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageEventChat2
        );
      });

      // speaker validation
      await test.step(`speaker side validation of event chat messages`, async () => {
        await speakerChatComponent.verifyCountOfMessageMatches(4);
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          attendeeMessageEventChat
        );
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageEventChat
        );
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageEventChat2
        );
        await speakerChatComponent.verifyMessageWithGivenContentIsVisible(
          organisereMessageEventChat
        );
      });

      // organiser validation
      await test.step(`organiser side validation of event chat messages`, async () => {
        await organiserChatComponent.verifyCountOfMessageMatches(4);
        await organiserChatComponent.verifyMessageWithGivenContentIsVisible(
          attendeeMessageEventChat
        );
        await organiserChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageEventChat
        );
        await organiserChatComponent.verifyMessageWithGivenContentIsVisible(
          organisereMessageEventChat
        );
        await organiserChatComponent.verifyMessageWithGivenContentIsVisible(
          speakerMessageEventChat2
        );
      });
    });

    // main stage validation when attendee has joined the stage
    await test.step(`main stage validation when attendee has joined stage`, async () => {
      await test.step(`verify organiser sees 3 stream on main stage`, async () => {
        await organiserGreenRoom.verifyCountOfStreamInsideMainStageContainer(3);
      });

      await test.step(`verify speaker sees 3 streeam on main stage`, async () => {
        await speakerGreenRoom.verifyCountOfStreamInsideMainStageContainer(3);
      });
      await test.step(`verify attendee sees 3 stream on main stage`, async () => {
        await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
          3
        );
      });
    });

    await test.step(`organiser sends himself back to backstage using lower hand`, async () => {
      await organiserGreenRoom.sendUserToGreenRoomFromBackStageUsingLowerHand(
        organiserFirstName
      );
    });

    await test.step(`organiser sends speaker back to backstage using lower hand`, async () => {
      await organiserGreenRoom.sendUserToGreenRoomFromBackStageUsingLowerHand(
        speakerOneFirstName
      );
    });

    // green room validation when only attendee is left on main stage and org and speaker has come down
    await test.step(`green room validation from org, speaker and attendee perspective when organiser has moved himself down`, async () => {
      await test.step(`verify organiser is seeing 2 streams on green room stage`, async () => {
        await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });
      await test.step(`verify speaker is seeing 1 (Attendee) streams on main stage`, async () => {
        await speakerGreenRoom.verifyCountOfStreamInsideMainStageContainer(1);
      });

      await test.step(`verify speaker is seeing 2 streams on green room stage`, async () => {
        await speakerGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });
      await test.step(`verify attendee sees 1 stream (own stream) on main stage`, async () => {
        await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
          1
        );
      });
    });

    // now organiser removes the attendee from the main stage
    await test.step(`organiser removing the attendee from main stage using remove button on join tab`, async () => {
      await organiserStagePage.openJoinTabOnInteraactionPanel();
      await organiserJoinTab.removeAttendeeFromStage(attendeeOneFirstName);
    });

    // main stage validation when attendee has left the stage and org, speaker are on green room
    await test.step(`main stage validation when organiser has removed the attendee from stage`, async () => {
      await test.step(`verify organiser sees 0 stream on main stage`, async () => {
        await organiserGreenRoom.verifyCountOfStreamInsideMainStageContainer(0);
      });

      await test.step(`verify speaker sees 0 streeam on main stage`, async () => {
        await speakerGreenRoom.verifyCountOfStreamInsideMainStageContainer(0);
      });
      await test.step(`verify attendee sees 0 stream on main stage`, async () => {
        await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
          0
        );
      });
    });

    await test.step(`organiser ends the dry run`, async () => {
      await organiserGreenRoom.endTheDryRun();
      await organiserPage.waitForTimeout(2000);
    });

    await test.step(`green room  validation from org, speaker and attendee perspective after ending dry run`, async () => {
      await test.step(`after ending dry run verify, orgganiser sees 2 user streams on green room `, async () => {
        await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });
      await test.step(`after ending dry run verify, speaker sees 2 user streams on green room `, async () => {
        await speakerGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });
      await test.step(`verify attendee sees empty stage as dry run has ended`, async () => {
        await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
          0
        );
      });
    });
  });
});
