import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { JoinRequestTab } from "../../../page-objects/events-pages/live-side-components/JoinRequestTab";
import { GreenRoom } from "../../../page-objects/events-pages/site-modules/GreenRoom";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import {
  createNewEvent,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@stage @show-flow @roll-call  @AV`, async () => {
  test.describe.configure({ retries: 2 });

  let eventId;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let attendeeOneBrowserContext: BrowserContext;
  let speakerOneBrowserContext: BrowserContext;
  let eventController: EventController;
  let defaultStageUrl: string;
  let attendeeLandingPage: string;
  let stageId;

  test.beforeEach(async () => {
    test.slow();

    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiContext = orgBrowserContext.request;
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: "room automation",
    });
    attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    speakerOneBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
      isSpeakermagicLinkEnabled: true,
    });

    eventController = new EventController(orgApiContext, eventId);
    stageId = await eventController.getDefaultStageId();
    await eventController.disableStudioAsBackstage(stageId);
    let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
    defaultStageUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/stages/${stageId}`;
    eventController.disableStudioAsBackstage(stageId);
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
    await attendeeOneBrowserContext?.close();
    await speakerOneBrowserContext?.close();
  });

  test("TC001: Organiser runs show flow in dry run mode, adds speaker using roll call and removes him using speaker on stage section ", async () => {
    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let speakerOnePage = await speakerOneBrowserContext.newPage();

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const speakerOneFirstName = "prateekSpeakerOne";
    const speakerOneLastName = "QA";

    //organiser
    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    const organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let speakerOneEmail = DataUtl.getRandomSpeakerEmail();

    let organiserStagePage: StagePage;
    let attendeeStagePage: StagePage;
    let speakerStagePage: StagePage;
    let organiserGreenRoom: GreenRoom;
    let speakerGreenRoom: GreenRoom;
    let attendeeOneJoinTab: JoinRequestTab;
    let organiserJoinTab: JoinRequestTab;

    //disable onboarding checks
    await eventController.disableOnboardingChecksForAttendee();

    await test.step(`organiser invites attendee and speaker to the event`, async () => {
      // invite attendee 1 and he logs in and then go to default stage
      await test.step(`organiser invites attendee  1 to the event`, async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeOneFirstName,
          lastName: attendeeOneLastName,
          phoneNumber: "1234567899",
          email: attendeeEmailOne,
        });
      });
      // invite speaker 1 and he logs in and then he enters to the default stage
      await test.step(`organiser invites speaker 1 to the event`, async () => {
        await eventController.inviteSpeakerToTheEventByApi(
          speakerOneEmail,
          speakerOneFirstName,
          speakerOneLastName
        );
      });
    });

    await test.step(`speaker, organiser and attendee all logs into the event and goes to stage or inside stage`, async () => {
      await test.step(`attendee 1 logs into the event and go to default stage`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeEmailOne
        );
        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(defaultStageUrl);
        attendeeStagePage = new StagePage(attendeeOnePage);
      });

      await test.step(`speaker 1 logs into the event and go to default stage`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          eventId,
          speakerOneEmail
        );
        await speakerOnePage.goto(magicLink);
        await speakerOnePage.click(`text=Enter Now`);
        await speakerOnePage.goto(defaultStageUrl);
        speakerStagePage = new StagePage(speakerOnePage);

        const speakerAvModal =
          await speakerStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
        await speakerAvModal.isVideoStreamLoadedOnAVModal();
        await speakerAvModal.clickOnJoinButtonOnAvModal();

        await test.step(`verify speaker has joined backstage and he is able to see himself`, async () => {
          speakerGreenRoom = new GreenRoom(speakerOnePage);
          await speakerGreenRoom.isGreenRoomVisible();
          await speakerGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(1);
        });
      });
      await test.step(`organiser logs into the event and go to default stage and go to backstage`, async () => {
        await organiserPage.goto(attendeeLandingPage);
        await organiserPage.click(`text=Enter Now`);
        await organiserPage.goto(defaultStageUrl);
        organiserStagePage = new StagePage(organiserPage);
        const orgAvModal =
          await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
        await orgAvModal.isVideoStreamLoadedOnAVModal();
        await orgAvModal.clickOnJoinButtonOnAvModal();

        await test.step(`verify organiser has joined backstage and he is able to see himself and speaker stream on green room`, async () => {
          organiserGreenRoom = new GreenRoom(organiserPage);
          await organiserGreenRoom.isGreenRoomVisible();
          await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
        });
      });
    });

    await test.step(`green room validation for org, speaker and attendee when dry run is not started`, async () => {
      await test.step(`verify attendee sees empty state on stage`, async () => {
        await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
          0
        );
      });
      await test.step(`verify speaker sees 2 streams on green roon`, async () => {
        await speakerGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });
      await test.step(`verify org sees 2 streams on green roon`, async () => {
        await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });
    });

    await test.step(`organiser opens roll call section and move speaker to On stage from the list`, async () => {
      await organiserStagePage.getGreenRoomContainer.openRollCallSection();
      await organiserStagePage.getGreenRoomContainer.sendSpeakerToOnStageFromBackStageUsingRollCall(
        speakerOneFirstName
      );
    });

    await test.step(`verify org sees speaker on main stage and speaker also sees himself on main stage`, async () => {
      await organiserStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
        1
      );
      await speakerStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
        1
      );
      await speakerStagePage.getGreenRoomContainer.isGreenRoomNotVisible();
      await organiserStagePage.getGreenRoomContainer.verifyCountOfStreamInsideGreenRoomMatches(
        1
      );
    });

    await test.step(`organiser starts the dry run and sends him self up to the stage`, async () => {
      await organiserGreenRoom.startTheDryRun();
      await organiserPage.waitForTimeout(2000);
      await organiserGreenRoom.sendUserToOnstageFromBackStageUsingRaiseHand(
        organiserFirstName
      );
    });

    await test.step(`main stage validation from org, speaker and attendee perspective`, async () => {
      await test.step(`verify organiser is able to see him self on main stage`, async () => {
        await organiserGreenRoom.verifyCountOfStreamInsideMainStageContainer(2);
      });

      await test.step(`verify speaker is able to see organiser on main stage`, async () => {
        await speakerGreenRoom.verifyCountOfStreamInsideMainStageContainer(2);
      });
      await test.step(`verify attendee sees organiser stream on main stage from attendee view`, async () => {
        await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
          2
        );
      });
      await test.step(`verify attendee is abl eto see an emoji panel`, async () => {
        await attendeeStagePage.verifyEmojiContainerIsVisible();
      });
    });

    await test.step(`organiser sends himself back to backstage using lower hand`, async () => {
      await organiserGreenRoom.sendUserToGreenRoomFromBackStageUsingLowerHand(
        organiserFirstName
      );
    });

    await test.step(`organiser sends speaker to green room from mainstage using off stage button on speaker on stage section`, async () => {
      await organiserGreenRoom.openSpeakersOnStageSection();
      await organiserGreenRoom.sendSpeakerToGreenRoomFromMainStageUsingSpeakersOnStageContainer(
        speakerOneFirstName
      );
    });

    // green room validation when only attendee is left on main stage and org and speaker has come down
    await test.step(`green room validation from org, speaker and attendee perspective when organiser has moved himself down`, async () => {
      await test.step(`verify organiser is seeing 2 streams on green room stage`, async () => {
        await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });
      await test.step(`verify speaker is seeing 2 streams on green room stage`, async () => {
        await speakerGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });
    });

    await test.step(`organiser ends the dry run`, async () => {
      await organiserGreenRoom.endTheDryRun();
      await organiserPage.waitForTimeout(2000);
    });

    await test.step(`green room  validation from org, speaker and attendee perspective after ending dry run`, async () => {
      await test.step(`after ending dry run verify, orgganiser sees 2 user streams on green room `, async () => {
        await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });
      await test.step(`after ending dry run verify, speaker sees 2 user streams on green room `, async () => {
        await speakerGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });
      await test.step(`verify attendee sees empty stage as dry run has ended`, async () => {
        await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
          0
        );
      });
    });
  });
});
