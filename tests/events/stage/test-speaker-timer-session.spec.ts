import test, { APIRequestContext, BrowserContext } from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { JoinRequestTab } from "../../../page-objects/events-pages/live-side-components/JoinRequestTab";
import { GreenRoom } from "../../../page-objects/events-pages/site-modules/GreenRoom";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import {
  createNewEvent,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import BrowserName from "../../../enums/BrowserEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@stage @show-flow @speaker-timer @ssession-run  @AV`, async () => {
  test.slow();
  test.describe.configure({ retries: 2 });
  let eventId;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let attendeeOneBrowserContext: BrowserContext;
  let speakerOneBrowserContext: BrowserContext;
  let eventController: EventController;
  let defaultStageUrl: string;
  let attendeeLandingPage: string;
  let stageId;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      browserName: BrowserName.CHROME,
    });

    orgApiContext = orgBrowserContext.request;

    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: "room automation",
    });

    attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
      browserName: BrowserName.CHROME,
      laodOrganiserCookies: false,
    });

    speakerOneBrowserContext = await BrowserFactory.getBrowserContext({
      browserName: BrowserName.CHROME,
      laodOrganiserCookies: false,
    });
    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
      isSpeakermagicLinkEnabled: true,
    });

    eventController = new EventController(orgApiContext, eventId);

    stageId = await eventController.getDefaultStageId();

    let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;

    defaultStageUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/stages/${stageId}`;
    eventController.disableStudioAsBackstage(stageId);
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
    await attendeeOneBrowserContext?.close();
    await speakerOneBrowserContext?.close();
  });

  test("TC001: Verifies Speaker-Timer functionality in event and backstage || Session.", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-349/validating-speaker-timer-for-old-stage-in-real-session-run",
    });

    let attendeeOnePage;
    let organiserPage;
    let speakerOnePage;

    await test.step("Attendee page.", async () => {
      attendeeOnePage = await attendeeOneBrowserContext.newPage();
    });

    await test.step("Organiser page.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step("Speaker page.", async () => {
      speakerOnePage = await speakerOneBrowserContext.newPage();
    });

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const speakerOneFirstName = "prateekSpeakerOne";
    const speakerOneLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let speakerOneEmail = DataUtl.getRandomSpeakerEmail();

    await test.step("Get random attendee email.", async () => {
      attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    });

    await test.step("Get random speaker email.", async () => {
      speakerOneEmail = DataUtl.getRandomSpeakerEmail();
    });

    let organiserStagePage: StagePage;
    let attendeeStagePage: StagePage;
    let speakerStagePage: StagePage;
    let organiserGreenRoom: GreenRoom;
    let speakerGreenRoom: GreenRoom;
    let attendeeOneJoinTab: JoinRequestTab;
    let organiserJoinTab: JoinRequestTab;

    await test.step("Diable onboarding checks for attendee.", async () => {
      //disable onboarding checks
      await eventController.disableOnboardingChecksForAttendee();
    });

    await test.step(`Organiser invites attendee and speaker to the event.`, async () => {
      //Attendee joins via invite and enters the default stage
      await test.step(`Organiser invites Attendee.`, async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeOneFirstName,
          lastName: attendeeOneLastName,
          phoneNumber: "1234567899",
          email: attendeeEmailOne,
        });
      });

      //Speaker joins via invite and enters the default stage
      await test.step(`Organizer invites Speaker.`, async () => {
        await eventController.inviteSpeakerToTheEventByApi(
          speakerOneEmail,
          speakerOneFirstName,
          speakerOneLastName
        );
      });
    });

    await test.step(`Organizer, Speaker and Attendee join the event and enter default stage.`, async () => {
      await test.step(`Attendee joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeEmailOne
        );

        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(defaultStageUrl);
        attendeeStagePage = new StagePage(attendeeOnePage);
      });

      await test.step(`Speaker joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          eventId,
          speakerOneEmail
        );

        await speakerOnePage.goto(magicLink);
        await speakerOnePage.click(`text=Enter Now`);
        await speakerOnePage.goto(defaultStageUrl);
        speakerStagePage = new StagePage(speakerOnePage);

        const speakerAvModal =
          await speakerStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
        await speakerAvModal.isVideoStreamLoadedOnAVModal();
        await speakerAvModal.clickOnJoinButtonOnAvModal();

        await test.step(`Verify Speaker's presence in backstage.`, async () => {
          speakerGreenRoom = new GreenRoom(speakerOnePage);
          await speakerGreenRoom.isGreenRoomVisible();
          await speakerGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(1);
        });
      });

      await test.step(`Organizer joins the event and join backstage via default stage.`, async () => {
        await organiserPage.goto(attendeeLandingPage);
        await organiserPage.click(`text=Enter Now`);
        await organiserPage.goto(defaultStageUrl);
        organiserStagePage = new StagePage(organiserPage);
        const orgAvModal =
          await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
        await orgAvModal.isVideoStreamLoadedOnAVModal();
        await orgAvModal.clickOnJoinButtonOnAvModal();

        await test.step(`Verify Organiser's and Speaker's presence in backstage.`, async () => {
          organiserGreenRoom = new GreenRoom(organiserPage);
          await organiserGreenRoom.isGreenRoomVisible();
          await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
        });
      });
    });

    await test.step(`Green room validation.`, async () => {
      await test.step(`Verify Attendee view of stage || Empty stage`, async () => {
        await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
          0
        );
      });

      await test.step(`Verify Speaker view of green room || 2 streams in green roon`, async () => {
        await speakerGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });

      await test.step(`Verify Organizer view of green room || 2 streams in green roon`, async () => {
        await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });
    });

    await test.step(`Organizer sends Speaker on stage.`, async () => {
      await organiserStagePage.getGreenRoomContainer.sendUserToOnstageFromBackStageUsingRaiseHand(
        speakerOneFirstName
      );
    });

    await test.step("Verify Organizer and Speaker streams in green room and stage respectively.", async () => {
      await organiserStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
        1
      );
      await speakerStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
        1
      );
      await speakerStagePage.getGreenRoomContainer.isGreenRoomNotVisible();
      await organiserStagePage.getGreenRoomContainer.verifyCountOfStreamInsideGreenRoomMatches(
        1
      );
    });

    await test.step(`Organizer initiate session and joins the stage.`, async () => {
      await organiserGreenRoom.disableDryRun();
      await organiserGreenRoom.startTheSession();
      await organiserPage.waitForTimeout(2000);
      await organiserGreenRoom.sendUserToOnstageFromBackStageUsingRaiseHand(
        organiserFirstName
      );
    });

    await test.step("Organiser start the speaker-timer.", async () => {
      await organiserGreenRoom.startSpeakerTimer();
    });

    await test.step(`Main stage validation.`, async () => {
      await test.step(`Verify Organiser's view || Speaker and Organizer on stage.`, async () => {
        await organiserGreenRoom.verifyCountOfStreamInsideMainStageContainer(2);
      });

      await test.step(`Verify Speaker's view || Speaker and Organizer on stage.`, async () => {
        await speakerGreenRoom.verifyCountOfStreamInsideMainStageContainer(2);
      });

      await test.step(`Verify Attendee's view || Speaker and Organizer on stage.`, async () => {
        await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
          2
        );
      });

      await test.step(`Verify emoji panel is visible to Attendee.`, async () => {
        await attendeeStagePage.verifyEmojiContainerIsVisible();
      });

      await test.step("Timer is visibile to Organiser.", async () => {
        await organiserGreenRoom.verifyTimerVisibility(true);
      });

      await test.step("Timer is visibile to Speaker.", async () => {
        await speakerGreenRoom.verifyTimerVisibility(true);
      });

      await test.step("Timer is visibile to Attendee.", async () => {
        await attendeeStagePage.verifyTimerVisibility(true);
      });
    });

    //attendee raise hand to join stage and organiser approves it
    await test.step(`Attendee joins stage by join-request.`, async () => {
      await test.step(`Attendee send join request from panel to join the stage.`, async () => {
        //by default interaction panel will be open
        await attendeeStagePage.isInteractionPanelOpen();
        attendeeOneJoinTab =
          await attendeeStagePage.openJoinTabOnInteraactionPanel();
        await attendeeOneJoinTab.isVideoStreamLoaded();
        await attendeeOneJoinTab.clickOnSendStageJoinRequest();
        await attendeeOneJoinTab.isWaitingForApprovalButtonIsVisible();
      });

      await test.step(`Organizer accepts the request.`, async () => {
        await organiserStagePage.isInteractionPanelOpen();
        organiserJoinTab =
          await organiserStagePage.openJoinTabOnInteraactionPanel();
        await organiserJoinTab.approveAttendeeJoinRequest(attendeeOneFirstName);
      });

      await test.step(`Attendee joins the stage.`, async () => {
        await attendeeOneJoinTab.clickOnJoinStageButton();
      });

      await test.step("Timer is visibile to Organiser.", async () => {
        await organiserGreenRoom.verifyTimerVisibility(true);
      });

      await test.step("Timer is visibile to Speaker.", async () => {
        await speakerGreenRoom.verifyTimerVisibility(true);
      });

      await test.step("Timer is visibile to Attendee.", async () => {
        await attendeeStagePage.verifyTimerVisibility(true);
      });
    });

    await test.step(`Organizer joins backstage.`, async () => {
      await organiserGreenRoom.sendUserToGreenRoomFromBackStageUsingLowerHand(
        organiserFirstName
      );
    });

    await test.step(`Organizer send Speaker to backstage.`, async () => {
      await organiserGreenRoom.sendUserToGreenRoomFromBackStageUsingLowerHand(
        speakerOneFirstName
      );
    });

    await test.step("Verify timer visibilty.", async () => {
      await test.step("Timer is visibile to Organiser.", async () => {
        await organiserGreenRoom.verifyTimerVisibility(true);
      });

      await test.step("Timer is visibile to Speaker.", async () => {
        await speakerGreenRoom.verifyTimerVisibility(true);
      });

      await test.step("Timer is visibile to Attendee.", async () => {
        await attendeeStagePage.verifyTimerVisibility(true);
      });
    });

    // green room validation when only attendee is left on main stage and org and speaker has come down
    await test.step(`Green room validation.`, async () => {
      await test.step(`Verify Organiser's view || Organizer and Speaker stream in green room.`, async () => {
        await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });

      await test.step(`Verify Speaker's view || Attendee stream on main stage.`, async () => {
        await speakerGreenRoom.verifyCountOfStreamInsideMainStageContainer(1);
      });

      await test.step(`Verify Speaker's View || Organizer and Speaker streams in green room.`, async () => {
        await speakerGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });

      await test.step(`Verify Attendee's view || Attendee stream on main stage`, async () => {
        await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
          1
        );
      });
    });

    // now organiser removes the attendee from the main stage
    await test.step(`Organizer removes Attendee from main stage.`, async () => {
      await organiserStagePage.openJoinTabOnInteraactionPanel();
      await organiserJoinTab.removeAttendeeFromStage(attendeeOneFirstName);
    });

    // main stage validation when attendee has left the stage and org, speaker are on green room
    await test.step(`Main stage validation.`, async () => {
      await test.step(`Verify Organiser's view || No stream on main stage.`, async () => {
        await organiserGreenRoom.verifyCountOfStreamInsideMainStageContainer(0);
      });

      await test.step(`Verify Speaker's view || No stream on main stage.`, async () => {
        await speakerGreenRoom.verifyCountOfStreamInsideMainStageContainer(0);
      });

      await test.step(`Verify Attendee's view || No stream on main stage`, async () => {
        await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
          0
        );
      });
    });

    await test.step("Verify timer is not visibile on main stage.", async () => {
      await test.step("Timer is visibile to Organiser.", async () => {
        await organiserGreenRoom.verifyTimerVisibility(false);
      });

      await test.step("Timer is visibile to Speaker.", async () => {
        await speakerGreenRoom.verifyTimerVisibility(false);
      });

      await test.step("Timer is visibile to Attendee.", async () => {
        await attendeeStagePage.verifyTimerVisibility(false);
      });
    });

    await test.step(`Organiser ends the session`, async () => {
      await organiserGreenRoom.endTheSession();
      await organiserPage.waitForTimeout(2000);
    });

    await test.step("Organiser stops the timer.", async () => {
      await organiserGreenRoom.endSpeakerTimer();
    });

    await test.step(`Green room validation.`, async () => {
      await test.step(`Verify Organiser's view || Organiser and Speaker stream in green room.`, async () => {
        await organiserGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });

      await test.step(`Verify Speaker's view || Organiser and Speaker stream in green room.`, async () => {
        await speakerGreenRoom.verifyCountOfStreamInsideGreenRoomMatches(2);
      });

      await test.step(`Verify Attendee's view || Empty stage.`, async () => {
        await attendeeStagePage.getMainStageContainer.verifyCountOfStreamInsideMainStageContainer(
          0
        );
      });
    });
  });
});
