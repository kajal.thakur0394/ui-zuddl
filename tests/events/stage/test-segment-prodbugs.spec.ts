import {
  APIRequestContext,
  expect,
  test,
  BrowserContext,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { EventController } from "../../../controller/EventController";
import { StageController } from "../../../controller/StageController";

test.describe(`@stage @prod-bugs`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventTitle: string;
  let eventId;
  let eventController: EventController;
  let defaultStageController: StageController;
  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = orgBrowserContext.request;

    //event details
    eventTitle = DataUtl.getRandomEventTitle();

    await test.step(`Creating a new event with title ${eventTitle}`, async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: eventTitle,
      });
      console.log(`Newly generated Event id is ${eventId}`);
      eventController = new EventController(orgApiContext, eventId);
    });
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`@segment TC001: verify we are able to delete segment using API`, async () => {
    // linear ticket info
    let defaultStageId;
    let segmentId;
    let listOfSegmentsForThisStage;
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-138/delete-segment-error-copy-p0",
    });

    await test.step(`Get the default stage ID for the event ${eventId}`, async () => {
      defaultStageId = await eventController.getDefaultStageId();
    });
    await test.step(`Create a segment on default stage`, async () => {
      defaultStageController = new StageController(
        orgApiContext,
        eventId,
        defaultStageId
      );
      eventController.disableStudioAsBackstage(defaultStageId);
      const addSegmentApiResp =
        await defaultStageController.createNewSegmentOnStage({});
      const addSegmentApiRespJson = await addSegmentApiResp.json();
      segmentId = addSegmentApiRespJson["segmentId"];
      console.log(`Newly created segment on stage is ${segmentId}`);
    });

    await test.step(`Verify segment is created succesfully for the default stage`, async () => {
      listOfSegmentsForThisStage =
        await defaultStageController.getAllSegmentsForThisStage();
      expect(
        listOfSegmentsForThisStage,
        "expecting list of segment list to be 1"
      ).toHaveLength(1);
    });
    await test.step(`Trigger delete segment API to remove the segment`, async () => {
      await defaultStageController.deleteSegmentFromStage(segmentId);
    });

    await test.step(`Verify segment is deleted succesfully`, async () => {
      listOfSegmentsForThisStage =
        await defaultStageController.getAllSegmentsForThisStage();
      expect(
        listOfSegmentsForThisStage,
        "expecting list of segment list to be 0"
      ).toHaveLength(0);
    });
  });
});
