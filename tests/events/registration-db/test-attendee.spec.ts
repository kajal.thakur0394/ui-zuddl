import test, {
  APIRequestContext,
  BrowserContext,
  expect,
  Page,
} from "@playwright/test";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import {
  addCustomFieldsToEvent,
  createNewEvent,
  getEventDetails,
  updateLandingPageType,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import conditionalFieldsTestData from "../../../test-data/conditional-fields-testdata/createConditionalFields.json";
import { DataUtl } from "../../../util/dataUtil";
import LandingPageType from "../../../enums/landingPageEnum";
import { EventController } from "../../../controller/EventController";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import EventRole from "../../../enums/eventRoleEnum";
import {
  validateEditProfileConditionalFieldDataFromDB,
  validatePreDefinedfieldsFromDB,
} from "../../../util/validation-util";
import { LandingPageTwo } from "../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
import testDataForCustomFieldsUploadCsv from "../../../test-data/uploadcsv-customfields-testdata/customFieldCsvTestData.json";
import { CsvApiController } from "../../../util/csv-api-controller";
import { validateRegisteredUserCustomFieldDataWithDBData } from "../../../util/validationHelper";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";
import { EventCustomFieldFormObj } from "../../../test-data/userRegForm";
import { LandingPage } from "../../../page-objects/events-pages/embedded-form/LandingPage";
import { DudaLandingPage } from "../../../page-objects/events-pages/duda-website/dudaLandingPage";
import { RegistrationWidgetIframe } from "../../../page-objects/events-pages/duda-website/registrationIframe";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe("@registration @predefined", async () => {
  let eventTitle: string;
  let eventId;
  let attendeeLandingPage: string;
  let speakerLandingpPage: string;
  let attendeePage: Page;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let landingPageId;
  let userInfoDTO: UserInfoDTO;
  let eventInfoDto: EventInfoDTO;
  let userEventRoleDto: UserEventRoleDto;
  let destinationCsvPath: string;

  test.beforeEach(async ({ context }, testInfo) => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = await orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    console.log(`Running ${testInfo.title}`);
    if (testInfo.title.includes("embeddable")) {
      eventId = await DataUtl.getApplicationTestDataObj()[
        "eventIdForPreDefinedFieldEmbedeForm"
      ];
      console.log("eventId:", eventId);
      if (eventId == undefined) {
        throw new Error("The event id is not able to be fetched");
      }
      let eventController = new EventController(orgApiContext, eventId);
      await eventController.changeEventStartAndEndDateTime({
        deltaByHoursInStartTime: 1,
        deltaByHoursInEndTime: 3,
      });
    } else if (testInfo.title.includes("duda")) {
      eventId =
        DataUtl.getApplicationTestDataObj()[
          "dudaPublishedEventIdForPreDefinedFieldsTesting"
        ];
      let eventController = new EventController(orgApiContext, eventId);

      // update the event start and end time
      await eventController.changeEventStartAndEndDateTime({
        deltaByHoursInStartTime: 1,
        deltaByHoursInEndTime: 3,
      });
    } else {
      eventId = await createNewEvent({
        api_request_context: orgApiContext,
        event_title: eventTitle,
      });
      const event_details_api_resp = await (
        await getEventDetails(orgApiContext, eventId)
      ).json();
      landingPageId = await event_details_api_resp.eventLandingPageId;
      // eventInfoDto.landingPageId = landingPageId;

      eventInfoDto = new EventInfoDTO(eventId, eventTitle, true); // true to denote magic link enabled
      attendeeLandingPage = eventInfoDto.attendeeLandingPage;
      speakerLandingpPage = eventInfoDto.speakerLandingPage;
      // get driver for attendee
      attendeePage = await context.newPage();
    }
  });

  test.afterEach(async () => {
    console.log("In teardown, closing the browser context");
    await orgBrowserContext?.close();
  });

  test(
    `TC001: Verify registration object is being saved for attendee registration from landing page 1`,
    {
      tag: "@smoke",
    },
    async () => {
      //make the event entry type as landing page 1 and enable magic link and make it reg based
      await updateLandingPageType(
        orgApiContext,
        eventId,
        landingPageId,
        LandingPageType.STYLE1,
        "true",
        EventEntryType.REG_BASED
      );
      //add the registration from with all default fields + predefined fields + custom fields
      let eventController: EventController = new EventController(
        orgApiContext,
        eventId
      );
      let ConditionalFieldsTestData =
        conditionalFieldsTestData["predefined-fields-regform"];
      await eventController.addCustomFieldsWithConditionToEvent(
        ConditionalFieldsTestData
      );
      let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
        .attendeeLandingPage;
      let attendeeEmail = DataUtl.getRandomAttendeeEmail();
      userInfoDTO = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        true
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDTO,
        eventInfoDto,
        EventRole.NOTEXISTS,
        false
      );
      let landingPageOne = new LandingPageOne(attendeePage);
      await landingPageOne.load(attendeeLandingPage);
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData.predefinedFieldsFormData,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
        }
      );

      await test.step("verify succesfully registered message visible.", async () => {
        await (
          await landingPageOne.getRegistrationConfirmationScreen()
        ).isRegistrationConfirmationMessageVisible(true);
      });

      await test.step("Fetch Registered user custom fields data from the DB", async () => {
        let fetchedCustomFieldsDataFromDb =
          await QueryUtil.fetchUserEventRegistrationDataFromDb(
            eventId,
            attendeeEmail
          );
        console.log(fetchedCustomFieldsDataFromDb);
        // console.log(
        //   `registered user fetched custom fields data is ${fetchedCustomFieldsDataFromDb}`
        // );
        expect(
          await validatePreDefinedfieldsFromDB(
            JSON.stringify(fetchedCustomFieldsDataFromDb),
            userInfoDTO.userRegFormData.predefinedFieldsFormData,
            {
              checkOptionalFields: true,
              checkMandatoryFields: true,
              fieldsToSkip: [
                "Do you watch sport?",
                "Write any cntry you have visited?",
                "Please rate Delhi on Scale of 0-10?",
                "Please select covid vaccine doses you've got?",
              ],
            }
          )
        ).toBe(true);
        console.log(
          "Type of fetched custom field data:",
          fetchedCustomFieldsDataFromDb
        );
        fetchedCustomFieldsDataFromDb =
          await QueryUtil.fetchCustomFieldsFilledByRegisteredUser(
            eventId,
            attendeeEmail
          );
        console.log(
          `registered user fetched custom fields data is ${fetchedCustomFieldsDataFromDb}`
        );
        expect(
          await validateEditProfileConditionalFieldDataFromDB(
            fetchedCustomFieldsDataFromDb,
            userInfoDTO.userRegFormData.predefinedFieldsFormData,
            {
              checkOptionalFields: true,
              checkMandatoryFields: true,
              fieldsToSkip: [
                "First Name",
                "Last Name",
                "Email",
                "Phone Number",
                "Bio",
                "Title",
                "Company",
                "Country",
              ],
            }
          )
        ).toBe(true);
      });
    }
  );

  test(
    `TC002: Verify registration object is being saved for attendee registration from landing page 2`,
    {
      tag: "@smoke",
    },
    async () => {
      //make the event entry type as landing page 1 and enable magic link and make it reg based
      await updateLandingPageType(
        orgApiContext,
        eventId,
        landingPageId,
        LandingPageType.STYLE2,
        "true",
        EventEntryType.REG_BASED
      );
      //add the registration from with all default fields + predefined fields + custom fields
      let eventController: EventController = new EventController(
        orgApiContext,
        eventId
      );
      let ConditionalFieldsTestData =
        conditionalFieldsTestData["predefined-fields-regform"];
      await eventController.addCustomFieldsWithConditionToEvent(
        ConditionalFieldsTestData
      );
      let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
        .attendeeLandingPage;
      let attendeeEmail = DataUtl.getRandomAttendeeEmail();
      userInfoDTO = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        true
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDTO,
        eventInfoDto,
        EventRole.NOTEXISTS,
        false
      );
      let landingPageTwo = new LandingPageTwo(attendeePage);
      await landingPageTwo.load(attendeeLandingPage);
      await (
        await landingPageTwo.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData.predefinedFieldsFormData,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
        }
      );

      await test.step("verify succesfully registered message visible.", async () => {
        await (
          await landingPageTwo.getRegistrationConfirmationScreen()
        ).isRegistrationConfirmationMessageVisible(true);
      });

      await test.step("Fetch Registered user custom fields data from the DB", async () => {
        let fetchedCustomFieldsDataFromDb =
          await QueryUtil.fetchUserEventRegistrationDataFromDb(
            eventId,
            attendeeEmail
          );
        console.log(fetchedCustomFieldsDataFromDb);
        // console.log(
        //   `registered user fetched custom fields data is ${fetchedCustomFieldsDataFromDb}`
        // );
        expect(
          await validatePreDefinedfieldsFromDB(
            JSON.stringify(fetchedCustomFieldsDataFromDb),
            userInfoDTO.userRegFormData.predefinedFieldsFormData,
            {
              checkOptionalFields: true,
              checkMandatoryFields: true,
              fieldsToSkip: [
                "Do you watch sport?",
                "Write any cntry you have visited?",
                "Please rate Delhi on Scale of 0-10?",
                "Please select covid vaccine doses you've got?",
                "Industry",
              ],
            }
          )
        ).toBe(true);
        fetchedCustomFieldsDataFromDb =
          await QueryUtil.fetchCustomFieldsFilledByRegisteredUser(
            eventId,
            attendeeEmail
          );
        console.log(
          `registered user fetched custom fields data is ${fetchedCustomFieldsDataFromDb["PleaserateDelhionScaleof010"]}`
        );
        expect(
          await validateEditProfileConditionalFieldDataFromDB(
            fetchedCustomFieldsDataFromDb,
            userInfoDTO.userRegFormData.predefinedFieldsFormData,
            {
              checkOptionalFields: true,
              checkMandatoryFields: true,
              fieldsToSkip: [
                "First Name",
                "Last Name",
                "Email",
                "Phone Number",
                "Bio",
                "Title",
                "Industry",
                "Company",
                "Country",
              ],
            }
          )
        ).toBe(true);
      });
    }
  );

  test(
    `TC003: Verify registration object is being saved for attendee imported with csv`,
    {
      tag: "@smoke",
    },
    async () => {
      //make the event entry type as landing page 1 and enable magic link and make it reg based
      await updateLandingPageType(
        orgApiContext,
        eventId,
        landingPageId,
        LandingPageType.STYLE1,
        "true",
        EventEntryType.REG_BASED
      );
      //add the registration from with all default fields + predefined fields + custom fields
      let eventController: EventController = new EventController(
        orgApiContext,
        eventId
      );
      let ConditionalFieldsTestData =
        conditionalFieldsTestData["predefined-fields-regform"];
      await eventController.addCustomFieldsWithConditionToEvent(
        ConditionalFieldsTestData
      );
      let csvApiController = new CsvApiController(orgApiContext);
      let csvProcessingResponse;
      const attendeeDataRecordToAddInCsv =
        testDataForCustomFieldsUploadCsv["predefined-conditional-fields-data"][
          "testDataValuesForCsv"
        ]["tc_all_fields_filled"];
      await test.step(`Invite attendee to event by csv`, async () => {
        //path where the destination csv will be generated
        destinationCsvPath =
          "test-data/uploadcsv-customfields-testdata/preDefinedDestinationCsv.csv";
        await test.step(`Generating csv at ${destinationCsvPath} with headers contain only default fields`, async () => {
          await csvApiController.createCsvWithCustomFields(
            destinationCsvPath,
            attendeeDataRecordToAddInCsv
          );
        });

        await test.step(`Uploading the csv and waiting for its processing to get completed`, async () => {
          let uploadedFileNameOnS3 = await csvApiController.getSignedS3Url(
            destinationCsvPath
          );

          //mapping object
          const csvMappingObjectForMappingEndpoint =
            testDataForCustomFieldsUploadCsv[
              "predefined-conditional-fields-data"
            ]["csvFieldsMappingObjectForMappingReq"];

          //list of csv field mapping for upload csv v2 endpoint
          const listOfCsvFieldMappingForUploadEndpoint =
            testDataForCustomFieldsUploadCsv[
              "predefined-conditional-fields-data"
            ]["listOfCsvFieldMapping"];

          await csvApiController.processAttendeeCsvOnEndPoint(
            eventId.toString(),
            uploadedFileNameOnS3,
            csvMappingObjectForMappingEndpoint,
            listOfCsvFieldMappingForUploadEndpoint
          );
          csvProcessingResponse =
            await csvApiController.waitForCsvFileProcessingToComplete(
              eventId.toString(),
              uploadedFileNameOnS3
            );
        });

        await test.step(`Expecting success record count to be 1 and failure record count to be 0`, async () => {
          const csvProcessingRespJson = await csvProcessingResponse.json();
          expect(
            csvProcessingRespJson.succeedRecordsCount,
            "expecting success record count to be 1"
          ).toBe(1);
        });
      });
      await test.step("Fetch Registered user custom fields data from the DB", async () => {
        const fetchedCustomFieldsDataFromDb =
          await QueryUtil.fetchCustomFieldsFilledByRegisteredUser(
            eventId,
            "anyrandomemail@testjoyn.com"
          );
        console.log(
          `registered user fetched custom fields data is ${fetchedCustomFieldsDataFromDb}`
        );
        expect(
          validateRegisteredUserCustomFieldDataWithDBData(
            attendeeDataRecordToAddInCsv,
            fetchedCustomFieldsDataFromDb
          ),
          "comparing user custom field test data with DB Data"
        ).toBeTruthy();
      });
    }
  );

  // Country is not saved after registering from the event.
  test(
    `TC004: Verify registration object is being saved where new mandatory fields are added after attendee has registered `,
    {
      tag: "@smoke",
    },
    async () => {
      //make the event entry type as landing page 1 and enable magic link and make it reg based
      await updateLandingPageType(
        orgApiContext,
        eventId,
        landingPageId,
        LandingPageType.STYLE1,
        "true",
        EventEntryType.REG_BASED
      );
      //add the registration from with all default fields + predefined fields + custom fields
      let eventController: EventController = new EventController(
        orgApiContext,
        eventId
      );
      let ConditionalFieldsTestData =
        conditionalFieldsTestData["predefined-fields-regform"];
      await eventController.addCustomFieldsWithConditionToEvent(
        ConditionalFieldsTestData
      );
      let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
        .attendeeLandingPage;
      let attendeeEmail = DataUtl.getRandomAttendeeEmail();
      userInfoDTO = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        true
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDTO,
        eventInfoDto,
        EventRole.NOTEXISTS,
        false
      );
      let landingPageOne = new LandingPageOne(attendeePage);
      await landingPageOne.load(attendeeLandingPage);
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData.predefinedFieldsFormData,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
        }
      );

      await test.step("verify succesfully registered message visible.", async () => {
        await (
          await landingPageOne.getRegistrationConfirmationScreen()
        ).isRegistrationConfirmationMessageVisible(true);
      });

      await enableDisableEmailTrigger(
        orgApiContext,
        eventId,
        EventSettingID.EmailOnRegistration,
        false,
        EventRole.ATTENDEE,
        true
      );

      await test.step(`adding custom fields to the event`, async () => {
        await addCustomFieldsToEvent(
          orgApiContext,
          eventId,
          landingPageId,
          new EventCustomFieldFormObj(
            eventId,
            landingPageId
          ).getAnExtraMandatoryCustomFieldObjForPreDefinedFields()
        );
      });

      let attendeeMagicLink: string;
      await test.step("Attendee checking his email to get the magic link", async () => {
        attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeEmail
        );
        console.log("attendeeMagicLink: ", attendeeMagicLink);
      });

      await test.step("Now user loads magic link , he should be prompted to fill the registration field", async () => {
        // let landingPageOne = new LandingPageOne(page);
        const eventRoleApiPromise = attendeePage.waitForResponse(
          (response) =>
            response.url().includes(`/event/${eventId}/role`) &&
            response.status() === 200
        );
        await landingPageOne.load(attendeeMagicLink);

        const getEventRoleResponse = await eventRoleApiPromise;
        expect(
          getEventRoleResponse.status(),
          "expecting event role api respoinse to be 200"
        ).toBe(200);
        await (
          await landingPageOne.getRegistrationFormComponent()
        ).fillUpTheRegistrationForm(
          userInfoDTO.userRegFormData.predefinedReRegisterFieldsFormData,
          {
            fillMandatoryFields: true,
            fillOptionalFields: true,
            clickOnContinueButton: true,
            fieldsToSkip: ["Email"],
          }
        );
        await (
          await landingPageOne.getRegistrationFormComponent()
        ).clickOnContinueButtonOnRegisterationForm();
        await landingPageOne.clickOnEnterNowButton();
        await landingPageOne.isLobbyLoaded();
      });

      await test.step("Fetch Registered user custom fields data from the DB", async () => {
        let fetchedCustomFieldsDataFromDb =
          await QueryUtil.fetchUserEventRegistrationDataFromDb(
            eventId,
            attendeeEmail
          );
        console.log(fetchedCustomFieldsDataFromDb);
        // console.log(
        //   `registered user fetched custom fields data is ${fetchedCustomFieldsDataFromDb}`
        // );
        expect(
          await validatePreDefinedfieldsFromDB(
            JSON.stringify(fetchedCustomFieldsDataFromDb),
            userInfoDTO.userRegFormData.predefinedFieldsFormData,
            {
              checkOptionalFields: true,
              checkMandatoryFields: true,
              fieldsToSkip: [
                "Do you watch sport?",
                "Write any cntry you have visited?",
                "Please rate Delhi on Scale of 0-10?",
                "Please select covid vaccine doses you've got?",
                "extraMandatoryField",
              ],
            }
          )
        ).toBe(true);
        console.log(
          "Type of fetched custom field data:",
          fetchedCustomFieldsDataFromDb
        );
        fetchedCustomFieldsDataFromDb =
          await QueryUtil.fetchCustomFieldsFilledByRegisteredUser(
            eventId,
            attendeeEmail
          );
        console.log(
          `registered user fetched custom fields data is ${fetchedCustomFieldsDataFromDb}`
        );
        expect(
          await validateEditProfileConditionalFieldDataFromDB(
            fetchedCustomFieldsDataFromDb,
            userInfoDTO.userRegFormData
              .predefinedReRegisterFieldsVerificationFormData,
            {
              checkOptionalFields: true,
              checkMandatoryFields: true,
              fieldsToSkip: [
                "First Name",
                "Last Name",
                "Email",
                "Phone Number",
                "Bio",
                "Title",
                "Company",
                "Country",
              ],
            }
          )
        ).toBe(true);
      });
    }
  );

  test.fixme(
    `TC005: Verify registration object is being saved for attendee registring from duda landing page `,
    async ({ page }) => {
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-186/validating-pre-defined-fields-on-duda-landing-page",
      });
      let dudaPublishedAttendeeLandingPage =
        DataUtl.getApplicationTestDataObj()[
          "dudaPublishedEventIdForAttendeePageTestingPreDefinedFields"
        ];
      let dudaAttendeeLandingPage: DudaLandingPage;
      let registrationWidgetIframe: RegistrationWidgetIframe;

      const attendeeEmail = DataUtl.getRandomAttendeeEmail();
      const attendeeInfoDto = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "automation",
        false
      );
      const eventInfoDto = new EventInfoDTO(eventId, "", false);
      const eventRoleDto = new UserEventRoleDto(
        attendeeInfoDto,
        eventInfoDto,
        EventRole.ATTENDEE,
        false
      );
      await test.step(`Loading the duda attendee landing page and click on register button to get registration iframe`, async () => {
        await page.goto(dudaPublishedAttendeeLandingPage);
        dudaAttendeeLandingPage = new DudaLandingPage(page);
        registrationWidgetIframe =
          await dudaAttendeeLandingPage.clickOnRegisterButton();
        await page.waitForLoadState("networkidle");
        await page.waitForLoadState("domcontentloaded");
        await page.waitForLoadState("load");
      });

      await test.step(`fill up the registration form`, async () => {
        await registrationWidgetIframe.getRegistrationFormComponent.fillUpTheRegistrationFormForPredefinedFields(
          attendeeInfoDto.userRegFormData.predefinedFieldsFormData
        );
      });

      await test.step(`verify that successfull registration message is displayed`, async () => {
        await registrationWidgetIframe.getRegistrationFormComponent.verifySuccessFullRegistrationMessageIsVisible();
      });
      await test.step("Fetch Registered user custom fields data from the DB", async () => {
        let fetchedCustomFieldsDataFromDb =
          await QueryUtil.fetchUserEventRegistrationDataFromDb(
            eventId,
            attendeeEmail
          );
        console.log(fetchedCustomFieldsDataFromDb);
        // console.log(
        //   `registered user fetched custom fields data is ${fetchedCustomFieldsDataFromDb}`
        // );
        expect(
          await validatePreDefinedfieldsFromDB(
            JSON.stringify(fetchedCustomFieldsDataFromDb),
            attendeeInfoDto.userRegFormData.predefinedFieldsFormData,
            {
              checkOptionalFields: true,
              checkMandatoryFields: true,
              fieldsToSkip: [
                "Do you watch sport?",
                "Write any cntry you have visited?",
                "Please rate Delhi on Scale of 0-10?",
                "Please select covid vaccine doses you've got?",
              ],
            }
          )
        ).toBe(true);
        console.log(
          "Type of fetched custom field data:",
          fetchedCustomFieldsDataFromDb
        );
        fetchedCustomFieldsDataFromDb =
          await QueryUtil.fetchCustomFieldsFilledByRegisteredUser(
            eventId,
            attendeeEmail
          );
        console.log(
          `registered user fetched custom fields data is ${fetchedCustomFieldsDataFromDb}`
        );
        expect(
          await validateEditProfileConditionalFieldDataFromDB(
            fetchedCustomFieldsDataFromDb,
            attendeeInfoDto.userRegFormData.predefinedFieldsFormData,
            {
              checkOptionalFields: true,
              checkMandatoryFields: true,
              fieldsToSkip: [
                "First Name",
                "Last Name",
                "Email",
                "Phone Number",
                "Bio",
                "Title",
                "Company",
                "Country",
              ],
            }
          )
        ).toBe(true);
      });
    }
  );

  test(
    `TC006: Verify registration object is being saved for attendee registring from embeddable registration form `,
    {
      tag: "@smoke",
    },
    async ({ page }) => {
      test.skip(
        process.env.run_env !== "pre-prod",
        "Skipping recording as it can only run on preprod"
      );
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-187/validating-pre-defined-fields-on-embeddable-form",
      });
      let landingPage = new LandingPage(page);
      let attendeeEmail = DataUtl.getRandomAttendeeEmail();
      userInfoDTO = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        true
      );
      let embeddableFormUrl =
        DataUtl.getApplicationTestDataObj()["preDefinedFieldembedFormSiteUrl"];
      console.log(`embed form site url to test is ${embeddableFormUrl}`);
      await landingPage.load(embeddableFormUrl);
      // await page.pause();
      await (
        await landingPage.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData.predefinedFieldsFormDataForEmbed,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
          clickOnSubmitButton: true,
          clickOnDesclaimerButton: false,
        }
      );
      await test.step("Fetch Registered user custom fields data from the DB", async () => {
        let fetchedCustomFieldsDataFromDb =
          await QueryUtil.fetchUserEventRegistrationDataFromDb(
            eventId,
            attendeeEmail
          );
        console.log(fetchedCustomFieldsDataFromDb);
        // console.log(
        //   `registered user fetched custom fields data is ${fetchedCustomFieldsDataFromDb}`
        // );
        expect(
          await validatePreDefinedfieldsFromDB(
            JSON.stringify(fetchedCustomFieldsDataFromDb),
            userInfoDTO.userRegFormData.predefinedFieldsFormData,
            {
              checkOptionalFields: true,
              checkMandatoryFields: true,
              fieldsToSkip: [
                "Do you watch sport?",
                "Write any cntry you have visited?",
                "Please rate Delhi on Scale of 0-10?",
                "Please select covid vaccine doses you've got?",
              ],
            }
          )
        ).toBe(true);
        console.log(
          "Type of fetched custom field data:",
          fetchedCustomFieldsDataFromDb
        );
        fetchedCustomFieldsDataFromDb =
          await QueryUtil.fetchCustomFieldsFilledByRegisteredUser(
            eventId,
            attendeeEmail
          );
        console.log(
          `registered user fetched custom fields data is ${fetchedCustomFieldsDataFromDb}`
        );
        expect(
          await validateEditProfileConditionalFieldDataFromDB(
            fetchedCustomFieldsDataFromDb,
            userInfoDTO.userRegFormData.predefinedFieldsFormData,
            {
              checkOptionalFields: true,
              checkMandatoryFields: true,
              fieldsToSkip: [
                "First Name",
                "Last Name",
                "Email",
                "Phone Number",
                "Bio",
                "Title",
                "Company",
                "Country",
              ],
            }
          )
        ).toBe(true);
      });
    }
  );
});
