import test, { APIRequestContext, BrowserContext } from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import BrowserName from "../../../enums/BrowserEnum";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import BrowserFactory from "../../../util/BrowserFactory";
import EventEntryType from "../../../enums/eventEntryEnum";
import { updateEventLandingPageDetails } from "../../../util/apiUtil";
import { EventController } from "../../../controller/EventController";
import { StageController } from "../../../controller/StageController";
import { StudioController } from "../../../controller/StudioController";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import { WebinarStage } from "../../../page-objects/webinar-pages/webinarStage";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(
  `@stage @screen`,
  {
    tag: "@smoke",
  },
  async () => {
    let eventId;
    let stageId;
    let studioId;
    let studioController: StudioController;
    let orgBrowserContext: BrowserContext;
    let orgApiContext: APIRequestContext;
    let attendeeOneBrowserContext: BrowserContext;
    let speakerOneBrowserContext: BrowserContext;
    let eventController: EventController;
    let defaultStageUrl: string;
    let attendeeLandingPage: string;

    test.beforeEach(async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: true,
      });

      orgApiContext = orgBrowserContext.request;

      await test.step("Creating new event.", async () => {
        eventId = await EventController.generateNewEvent(orgApiContext, {
          event_title: DataUtl.getRandomEventTitle(),
        });
      });

      eventController = new EventController(orgApiContext, eventId);

      await test.step("Fetching deafult stage id.", async () => {
        stageId = await eventController.getDefaultStageId();
      });

      await test.step("Enabling Studio as backstage for default stage.", async () => {
        studioId = await eventController.enableStudioAsBackstage(stageId);
      });

      studioController = new StudioController(orgApiContext, studioId, stageId);

      attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });

      speakerOneBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });

      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
        isSpeakermagicLinkEnabled: true,
      });

      let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
      attendeeLandingPage = eventInfoDto.attendeeLandingPage;

      defaultStageUrl = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/l/event/${eventId}/stages/${stageId}`;

      await test.step("Disabling onboarding checks for speaker and attendees access group.", async () => {
        await eventController.getAccessGroupController.handleOnboardingChecksForSpeaker(
          false
        );
        await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
          false
        );
      });

      await test.step(`Disabling recording for this stage.`, async () => {
        let stageController = new StageController(
          orgApiContext,
          eventId,
          stageId
        );
        await stageController.disableRecordingForStudioAsBackstage();
      });
    });

    test.afterEach(async () => {
      await orgBrowserContext?.close();
      await attendeeOneBrowserContext?.close();
      await speakerOneBrowserContext?.close();
    });

    test("TC001: Testing full screen view of main stage container.", async () => {
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-148/testing-full-screen-view-of-main-stage-container",
      });

      let attendeeOnePage;
      let organiserPage;
      let speakerOnePage;
      let attendeeOneEmail;
      let speakerOneEmail;
      let organiserStagePage: StagePage;
      let attendeeOneStagePage: WebinarStage;
      let speakerStagePage: StagePage;
      let organiserBackstage: StudioPage;
      let speakerBackstage: StudioPage;

      const attendeeOneFirstName = "PrateekAttendeeOne";
      const attendeeOneLastName = "QA";

      const speakerOneFirstName = "PrateekSpeakerOne";
      const speakerOneLastName = "QA";

      const organiserFirstName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

      await test.step("Attendees browser context.", async () => {
        attendeeOnePage = await attendeeOneBrowserContext.newPage();
      });

      await test.step("Organiser browser context.", async () => {
        organiserPage = await orgBrowserContext.newPage();
      });

      await test.step("Speaker browser context.", async () => {
        speakerOnePage = await speakerOneBrowserContext.newPage();
      });

      await test.step("Get random attendee emails.", async () => {
        attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
        console.log(`Attendee One Email: ${attendeeOneEmail}`);
      });

      await test.step("Get random speaker email.", async () => {
        speakerOneEmail = DataUtl.getRandomSpeakerEmail();
      });

      await test.step(`Organiser invites attendee and speaker to the event.`, async () => {
        await test.step(`Invites Attendees.`, async () => {
          await eventController.registerUserToEventWithRegistrationApi({
            firstName: attendeeOneFirstName,
            lastName: attendeeOneLastName,
            phoneNumber: "1234567899",
            email: attendeeOneEmail,
          });
        });

        await test.step(`Invites Speaker.`, async () => {
          await eventController.inviteSpeakerToTheEventByApi(
            speakerOneEmail,
            speakerOneFirstName,
            speakerOneLastName
          );
        });
      });

      await test.step(`Speaker and Organizer join the event and enter backstage concurrently.`, async () => {
        // Fetch the magic link for the speaker
        let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          eventId,
          speakerOneEmail
        );

        const speakerSteps = async () => {
          await speakerOnePage.goto(magicLink);
          await speakerOnePage.click(`text=Enter Now`);
          await speakerOnePage.goto(defaultStageUrl);
          speakerStagePage = new StagePage(speakerOnePage);

          const speakerAvModal =
            await speakerStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await speakerAvModal.isVideoStreamLoadedOnAVModalSAB();
          await speakerAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Speaker's presence in backstage.`, async () => {
            speakerBackstage = new StudioPage(speakerOnePage, studioController);
            // await speakerBackstage.closeStudioWalkthrough();
            await speakerBackstage.isBackstageVisible();
            await speakerBackstage.verifyCountOfStreamInBackstage(2);
          });
        };

        // Define the organizer's steps
        const organizerSteps = async () => {
          await organiserPage.goto(attendeeLandingPage);
          await organiserPage.click(`text=Enter Now`);
          await organiserPage.goto(defaultStageUrl);
          organiserStagePage = new StagePage(organiserPage);

          const orgAvModal =
            await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await orgAvModal.isVideoStreamLoadedOnAVModalSAB();
          await orgAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Organiser's and Speaker's stream in backstage.`, async () => {
            organiserBackstage = new StudioPage(
              organiserPage,
              studioController
            );
            await organiserBackstage.isBackstageVisible();
            await organiserBackstage.verifyCountOfStreamInBackstage(2);
          });
        };

        // Run speaker and organizer steps concurrently
        await Promise.all([speakerSteps(), organizerSteps()]);
      });

      await test.step(`Disable dry-run.`, async () => {
        await studioController.disableDryRun();
      });

      await test.step(`Organizer initiate the session, send Speaker to stage and joins the stage.`, async () => {
        await organiserBackstage.addUserToStageFromBackStage(
          organiserFirstName,
          true
        );
        await organiserBackstage.verifyCountOfStreamInStage(1);
        await organiserBackstage.addUserToStageFromBackStage(
          speakerOneFirstName
        );
        await organiserBackstage.verifyCountOfStreamInStage(2);
        await organiserBackstage.startTheSession();
      });

      await test.step(`Attendee joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeOneEmail
        );

        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(defaultStageUrl);
        await attendeeOnePage.waitForURL(defaultStageUrl);
        attendeeOneStagePage = new WebinarStage(attendeeOnePage);
        await attendeeOneStagePage.verifyAttendeeScreenIsLoaded();
      });

      await test.step(`Verify Attendee view || 2 streams.`, async () => {
        await attendeeOneStagePage.verifyNumberOfVisibileStreams(2);
      });

      await test.step(`Organsier change to fullscreen view.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeToFullscreenView();
      });

      await test.step(`Verify the fullscreen view.`, async () => {
        await organiserBackstage.getStudioSceneComponent.verifyFullscreenView();
      });

      await test.step(`End the session.`, async () => {
        let studioController = new StudioController(orgApiContext, studioId);
        await studioController.stopSessionOnStudio();
      });
    });
  }
);
