import test, { APIRequestContext, BrowserContext } from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import BrowserFactory from "../../../util/BrowserFactory";
import EventEntryType from "../../../enums/eventEntryEnum";
import { StageController } from "../../../controller/StageController";
import { EventController } from "../../../controller/EventController";
import { updateEventLandingPageDetails } from "../../../util/apiUtil";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import { WebinarStage } from "../../../page-objects/webinar-pages/webinarStage";
import { JoinRequestTab } from "../../../page-objects/events-pages/live-side-components/JoinRequestTab";
import BrowserName from "../../../enums/BrowserEnum";
import { StudioController } from "../../../controller/StudioController";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(
  `@stage @speaker-timer  @AV`,
  {
    tag: "@smoke",
  },
  async () => {
    let eventId;
    let stageId;
    let studioId;
    let studioController: StudioController;
    let orgBrowserContext: BrowserContext;
    let orgApiContext: APIRequestContext;
    let attendeeOneBrowserContext: BrowserContext;
    let speakerOneBrowserContext: BrowserContext;
    let eventController: EventController;
    let defaultStageUrl: string;
    let attendeeLandingPage: string;

    test.beforeEach(async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: true,
      });

      orgApiContext = orgBrowserContext.request;

      await test.step("Creating new event.", async () => {
        eventId = await EventController.generateNewEvent(orgApiContext, {
          event_title: DataUtl.getRandomEventTitle(),
        });
      });

      eventController = new EventController(orgApiContext, eventId);

      await test.step("Fetching deafult stage id.", async () => {
        stageId = await eventController.getDefaultStageId();
      });

      await test.step("Enabling Studio as backstage for default stage.", async () => {
        studioId = await eventController.enableStudioAsBackstage(stageId);
      });

      studioController = new StudioController(orgApiContext, studioId);

      attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });

      speakerOneBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });

      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
        isSpeakermagicLinkEnabled: true,
      });

      let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
      attendeeLandingPage = eventInfoDto.attendeeLandingPage;

      defaultStageUrl = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/l/event/${eventId}/stages/${stageId}`;

      await test.step("Disabling onboarding checks for speaker and attendee access group", async () => {
        await eventController.getAccessGroupController.handleOnboardingChecksForSpeaker(
          false
        );
        await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
          false
        );
      });

      await test.step(`Disabling recording for this stage`, async () => {
        let stageController = new StageController(
          orgApiContext,
          eventId,
          stageId
        );
        await stageController.disableRecordingForStudioAsBackstage();
      });
    });

    test.afterEach(async () => {
      await orgBrowserContext?.close();
      await attendeeOneBrowserContext?.close();
      await speakerOneBrowserContext?.close();
    });

    test("TC001: Verifies Speaker-Timer functionality in event and studio-as-backstage || Session.", async () => {
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-111/validating-stage-speaker-timer",
      });

      let attendeeOnePage;
      let organiserPage;
      let speakerOnePage;
      let organiserStagePage: StagePage;
      let attendeeStagePage: WebinarStage;
      let speakerStagePage: StagePage;
      let organiserBackstage: StudioPage;
      let speakerBackstage: StudioPage;
      let attendeeOneJoinTab: JoinRequestTab;
      let organiserJoinTab: JoinRequestTab;

      const attendeeOneFirstName = "PrateekAttendeeOne";
      const attendeeOneLastName = "QA";
      const attendeeFullName = `${attendeeOneFirstName} ${attendeeOneLastName}`;

      const speakerOneFirstName = "PrateekSpeakerOne";
      const speakerOneLastName = "QA";

      const organiserFirstName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

      const organiserLastName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];
      ("Automation");

      let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
      let speakerOneEmail = DataUtl.getRandomSpeakerEmail();

      await test.step("Attendee browser context.", async () => {
        attendeeOnePage = await attendeeOneBrowserContext.newPage();
      });

      await test.step("Organiser browser context.", async () => {
        organiserPage = await orgBrowserContext.newPage();
      });

      await test.step("Speaker browser context.", async () => {
        speakerOnePage = await speakerOneBrowserContext.newPage();
      });

      await test.step("Get random attendee email.", async () => {
        attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
      });

      await test.step("Get random speaker email.", async () => {
        speakerOneEmail = DataUtl.getRandomSpeakerEmail();
      });

      await test.step("Diable onboarding checks for attendee.", async () => {
        await eventController.disableOnboardingChecksForAttendee();
      });

      await test.step(`Organiser invites attendee and speaker to the event.`, async () => {
        await test.step(`Invites Attendee.`, async () => {
          await eventController.registerUserToEventWithRegistrationApi({
            firstName: attendeeOneFirstName,
            lastName: attendeeOneLastName,
            phoneNumber: "1234567899",
            email: attendeeEmailOne,
          });
        });

        await test.step(`Invites Speaker.`, async () => {
          await eventController.inviteSpeakerToTheEventByApi(
            speakerOneEmail,
            speakerOneFirstName,
            speakerOneLastName
          );
        });
      });

      await test.step(`Organizer, Speaker and Attendee join the event and enter default stage.`, async () => {
        await test.step(`Speaker joins the event and enter default stage.`, async () => {
          let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
            eventId,
            speakerOneEmail
          );

          await speakerOnePage.goto(magicLink);
          await speakerOnePage.click(`text=Enter Now`);
          await speakerOnePage.goto(defaultStageUrl);
          speakerStagePage = new StagePage(speakerOnePage);

          const speakerAvModal =
            await speakerStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await speakerAvModal.isVideoStreamLoadedOnAVModalSAB();
          await speakerAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Speaker's presence in backstage.`, async () => {
            speakerBackstage = new StudioPage(speakerOnePage, studioController);
            // await speakerBackstage.closeStudioWalkthrough();
            await speakerBackstage.isBackstageVisible();
            await speakerBackstage.verifyCountOfStreamInBackstage(1);
          });
        });

        await test.step(`Organizer joins the event and join backstage via default stage.`, async () => {
          await organiserPage.goto(attendeeLandingPage);
          await organiserPage.click(`text=Enter Now`);
          await organiserPage.goto(defaultStageUrl);
          organiserStagePage = new StagePage(organiserPage);
          const orgAvModal =
            await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await orgAvModal.isVideoStreamLoadedOnAVModalSAB();
          await orgAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Organiser's and Speaker's presence in backstage.`, async () => {
            organiserBackstage = new StudioPage(
              organiserPage,
              studioController
            );
            await organiserBackstage.isBackstageVisible();
            await organiserBackstage.verifyCountOfStreamInBackstage(2);
          });
        });
      });

      await test.step(`Organizer sends Speaker on stage.`, async () => {
        await organiserBackstage.addUserToStageFromBackStage(
          speakerOneFirstName
        );
      });

      await test.step("Verify Organizer and Speaker streams in backstage and stage respectively.", async () => {
        await organiserBackstage.verifyCountOfStreamInBackstage(1);
        await organiserBackstage.verifyCountOfStreamInStage(1);
      });

      await test.step(`Organizer initiate session and joins the stage.`, async () => {
        await organiserBackstage.addUserToStageFromBackStage(
          organiserFirstName,
          true
        );
        await organiserBackstage.startTheSession();
      });

      await test.step("Organiser start the speaker-timer.", async () => {
        await organiserBackstage.clickOnExtraTab();
        await organiserBackstage.startTheTimer("50");
      });

      await test.step(`Attendee joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeEmailOne
        );

        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(defaultStageUrl);
        await attendeeOnePage.waitForURL(defaultStageUrl);
        attendeeStagePage = new WebinarStage(attendeeOnePage);
      });

      await test.step(`Verify Attendee's view || Empty stage.`, async () => {
        await attendeeStagePage.verifyNumberOfVisibileStreams(0);
        await attendeeStagePage.getStagePage.verifyEmojiContainerIsVisible();
      });

      await test.step("Time Validation.", async () => {
        await test.step("Timer is visibile to Organiser.", async () => {
          await organiserBackstage.verifyTimerVisibility(true);
        });

        await test.step("Timer is visibile to Speaker.", async () => {
          await speakerBackstage.verifyTimerVisibility(true);
        });

        await test.step("Timer is visibile to Attendee.", async () => {
          await attendeeStagePage.verifyTimerVisibility(true);
        });
      });

      await test.step(`Attendee joins stage by join-request.`, async () => {
        await test.step(`Attendee send join request from panel to join the stage.`, async () => {
          //by default interaction panel will be open
          await attendeeStagePage.getStagePage.isInteractionPanelOpen();
          attendeeOneJoinTab =
            await attendeeStagePage.getStagePage.openJoinTabOnInteraactionPanel();
          await attendeeOneJoinTab.isVideoStreamLoaded();
          await attendeeOneJoinTab.page.waitForTimeout(10000);
          await attendeeOneJoinTab.clickOnSendStageJoinRequest();
          await attendeeOneJoinTab.isWaitingForApprovalButtonIsVisible();
        });

        await test.step(`Organizer accepts the request.`, async () => {
          await organiserBackstage.clickEngageTab();
          organiserJoinTab =
            await organiserStagePage.openJoinTabOnInteraactionPanelStudio();
          await organiserJoinTab.approveAttendeeJoinRequestStudio(
            attendeeFullName
          );
        });

        await test.step(`Attendee joins the stage.`, async () => {
          await attendeeOneJoinTab.clickOnJoinStageButton();
          await organiserBackstage.verifyAttendeeVideoContainer(
            attendeeOneFirstName
          );
        });

        await test.step("Timer is visibile to Organiser.", async () => {
          await organiserBackstage.verifyTimerVisibility(true);
        });

        await test.step("Timer is visibile to Speaker.", async () => {
          await speakerBackstage.verifyTimerVisibility(true);
        });

        await test.step("Timer is visibile to Attendee.", async () => {
          await attendeeStagePage.verifyTimerVisibility(true);
        });
      });

      await test.step(`Organizer joins backstage.`, async () => {
        await organiserBackstage.removeUserFromStage("You");
      });

      await test.step(`Organizer send Speaker to backstage.`, async () => {
        await organiserBackstage.removeUserFromStage(speakerOneFirstName);
      });

      await test.step("Verify timer visibilty.", async () => {
        await test.step("Timer is visibile to Organiser.", async () => {
          await organiserBackstage.verifyTimerVisibility(true);
        });

        await test.step("Timer is visibile to Speaker.", async () => {
          await speakerBackstage.verifyTimerVisibility(true);
        });

        await test.step("Timer is visibile to Attendee.", async () => {
          await attendeeStagePage.verifyTimerVisibility(true);
        });
      });

      await test.step(`Organizer removes Attendee from main stage.`, async () => {
        await organiserBackstage.removeUserFromStage(attendeeOneFirstName);
      });

      await test.step("Organiser stops the timer.", async () => {
        await organiserBackstage.clickOnExtraTab();
        await organiserBackstage.stopTheTimer();
      });

      await test.step("Verify timer is not visibile on main stage.", async () => {
        await test.step("Timer is visibile to Organiser.", async () => {
          await organiserBackstage.verifyTimerVisibility(false);
        });

        await test.step("Timer is visibile to Speaker.", async () => {
          await speakerBackstage.verifyTimerVisibility(false);
        });

        await test.step("Timer is visibile to Attendee.", async () => {
          await attendeeStagePage.verifyTimerVisibility(false);
        });
      });

      await test.step(`Backstage validation.`, async () => {
        await test.step(`Verify Organiser's view || Organiser and Speaker stream in green room.`, async () => {
          await organiserBackstage.verifyCountOfStreamInBackstage(2);
        });

        await test.step(`Verify Speaker's view || Organiser and Speaker stream in green room.`, async () => {
          await speakerBackstage.verifyCountOfStreamInBackstage(2);
        });

        await test.step(`Verify Speaker's view || Organiser and Speaker stream in green room.`, async () => {
          await attendeeStagePage.verifyNumberOfVisibileStreams(0);
        });
      });
    });
  }
);
