import test, { APIRequestContext, BrowserContext } from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import {
  inviteAttendeeByAPI,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import { QuestionsSortingEnum } from "playwright-qa/enums/qNaSortingEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(
  "@stage @show-flow @emoji",
  {
    tag: "@smoke",
  },
  async () => {
    let eventId;
    let stageId;

    let orgBrowserContext: BrowserContext;
    let orgApiContext: APIRequestContext;
    let eventController: EventController;
    let attendeeLandingPage: string;
    let stageListingPage: string;
    let magicLink: string;

    test.beforeAll(async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({});

      orgApiContext = orgBrowserContext.request;

      await test.step("Creating new event.", async () => {
        eventId = await EventController.generateNewEvent(orgApiContext, {
          event_title: DataUtl.getRandomEventTitle(),
        });
      });

      eventController = new EventController(orgApiContext, eventId);

      await test.step("Fetching deafult stage id.", async () => {
        stageId = await eventController.getDefaultStageId();
      });

      await test.step("Enabling Studio as backstage for default stage.", async () => {
        await eventController.enableStudioAsBackstage(stageId);
      });

      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
      });

      let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
      attendeeLandingPage = eventInfoDto.attendeeLandingPage;
      stageListingPage = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/l/event/${eventId}/stages/${stageId}`;

      await test.step("Update event landing page details.", async () => {
        await eventController.disableOnboardingChecksForAttendee();
      });
    });

    test.afterEach(async () => {
      await orgBrowserContext?.close();
    });

    test("TC001: Organiser enables the emojis for stage and attendee is able to view the emojis.", async ({
      page,
    }) => {
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-139/validation-of-emojis-on-stage",
      });

      await test.step("Update event landing page details - Enable Magic Link and Enable Guest login.", async () => {
        await updateEventLandingPageDetails(orgApiContext, eventId, {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
          isGuestAuthEnabled: true,
        });
      });

      let attendeeEmail = DataUtl.getRandomAttendeeEmail();

      await test.step("Invite Attendee by API.", async () => {
        await inviteAttendeeByAPI(orgApiContext, eventId, attendeeEmail, true);
      });

      await test.step("Fetch Magic-Link from database.", async () => {
        magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeEmail
        );
      });

      await test.step("Attendee login to event via Magic-Link.", async () => {
        await page.goto(magicLink);
        await page.click("text=Enter Now");
        await page.goto(stageListingPage);
      });

      await test.step("Verify if emojis are Visible", async () => {
        let stagePage = new StagePage(page);
        await stagePage.verifyEmojisAreVisible();
      });
    });
  }
);
