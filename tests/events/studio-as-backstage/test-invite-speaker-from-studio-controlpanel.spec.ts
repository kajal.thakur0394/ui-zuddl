import test, { APIRequestContext, BrowserContext } from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import BrowserName from "../../../enums/BrowserEnum";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import BrowserFactory from "../../../util/BrowserFactory";
import EventEntryType from "../../../enums/eventEntryEnum";
import { updateEventLandingPageDetails } from "../../../util/apiUtil";
import { EventController } from "../../../controller/EventController";
import { StageController } from "../../../controller/StageController";
import { StudioController } from "../../../controller/StudioController";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";

test.describe(
  `@stage @studio`,
  {
    tag: "@smoke",
  },
  async () => {
    let eventId;
    let stageId;
    let studioId;
    let attendeeLandingPage;
    let defaultStageUrl;
    let expectedPeoplePageUrl;
    let studioController: StudioController;
    let orgBrowserContext: BrowserContext;
    let orgApiContext: APIRequestContext;
    let eventController: EventController;

    test.beforeEach(async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: true,
      });

      orgApiContext = orgBrowserContext.request;

      await test.step("Creating new event.", async () => {
        eventId = await EventController.generateNewEvent(orgApiContext, {
          event_title: DataUtl.getRandomEventTitle(),
        });
      });

      eventController = new EventController(orgApiContext, eventId);

      await test.step("Fetching deafult stage id.", async () => {
        stageId = await eventController.getDefaultStageId();
      });

      await test.step("Enabling Studio as backstage for default stage.", async () => {
        studioId = await eventController.enableStudioAsBackstage(stageId);
      });

      studioController = new StudioController(orgApiContext, studioId);

      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
        isSpeakermagicLinkEnabled: true,
      });

      let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
      attendeeLandingPage = eventInfoDto.attendeeLandingPage;

      defaultStageUrl = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/l/event/${eventId}/stages/${stageId}`;

      expectedPeoplePageUrl = `${
        DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
      }/event/${eventId}/people/speakers`;

      await test.step(`Disabling recording for default stage.`, async () => {
        let stageController = new StageController(
          orgApiContext,
          eventId,
          stageId
        );
        await stageController.disableRecordingForStudioAsBackstage();
      });
    });

    test.afterEach(async () => {
      await orgBrowserContext?.close();
    });

    test("TC001: Invite speaker flow from inside of studio control panel, should navigate to events people section.", async () => {
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-145/invite-speaker-flow-from-inside-of-studio-control-panel-should",
      });

      let organiserPage;
      let organiserStagePage: StagePage;
      let organiserBackstage: StudioPage;

      await test.step("Organiser browser context.", async () => {
        organiserPage = await orgBrowserContext.newPage();
      });

      await test.step(`Organizer joins the event and enter default stage.`, async () => {
        await test.step(`Organizer joins the event and join backstage via default stage.`, async () => {
          await organiserPage.goto(attendeeLandingPage);
          await organiserPage.click(`text=Enter Now`);
          await organiserPage.goto(defaultStageUrl);
          organiserStagePage = new StagePage(organiserPage);
          const orgAvModal =
            await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await orgAvModal.isVideoStreamLoadedOnAVModalSAB();
          await orgAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Organiser's stream in backstage.`, async () => {
            organiserBackstage = new StudioPage(
              organiserPage,
              studioController
            );
            await organiserBackstage.isBackstageVisible();
            await organiserBackstage.verifyCountOfStreamInBackstage(1);
          });
        });
      });

      await test.step(`Organiser clicks on "Invite a speaker" in Studio control panel.`, async () => {
        await organiserBackstage.clickAddSpeakerButton();
      });

      await test.step(`Click on invite speaker button.`, async () => {
        await organiserBackstage.clickInviteSpeakerButtonAndVerifyPage();
      });

      await test.step(`Verify the button redirects to "Event people section" page.`, async () => {
        await organiserBackstage.verifyPageUrl(
          orgBrowserContext,
          expectedPeoplePageUrl
        );
      });
    });
  }
);
