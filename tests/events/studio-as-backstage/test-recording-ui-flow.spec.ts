import test, { APIRequestContext, BrowserContext } from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import BrowserName from "../../../enums/BrowserEnum";
import BrowserFactory from "../../../util/BrowserFactory";
import EventEntryType from "../../../enums/eventEntryEnum";
import { updateEventLandingPageDetails } from "../../../util/apiUtil";
import { EventController } from "../../../controller/EventController";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { StudioController } from "../../../controller/StudioController";
import { setTimeOut } from "../../../util/commonUtil";
import { RecordingController } from "../../../controller/RecordingController";

test.describe(`@studio @recording`, { tag: "@smoke" }, async () => {
  let eventId;
  let stageId;
  let studioId;
  let studioController: StudioController;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventController: EventController;
  let organiserBackstage: StudioPage;
  let defaultStageUrl: string;
  let organiserPage;

  test.beforeAll(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      browserName: BrowserName.CHROME,
      laodOrganiserCookies: true,
    });

    orgApiContext = orgBrowserContext.request;

    await test.step("Creating new event.", async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
      });
    });

    eventController = new EventController(orgApiContext, eventId);

    await test.step("Fetching deafult stage id.", async () => {
      stageId = await eventController.getDefaultStageId();
    });

    await test.step("Enabling Studio as backstage for default stage.", async () => {
      studioId = await eventController.enableStudioAsBackstage(stageId);
    });

    studioController = new StudioController(orgApiContext, studioId, stageId);

    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
      isSpeakermagicLinkEnabled: true,
    });

    defaultStageUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/stages/${stageId}`;
  });

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      browserName: BrowserName.CHROME,
      laodOrganiserCookies: true,
    });

    orgApiContext = orgBrowserContext.request;
    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });
  });
  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test("TC001: Disable recording and then verify it, then Enable it again and verify it.", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-149/enabledisable-save-recording-flow-through-ui",
    });

    let organiserStagePage: StagePage;

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

    await test.step(`Organizer joins the event and enter default stage.`, async () => {
      await organiserPage.goto(defaultStageUrl);
      organiserStagePage = new StagePage(organiserPage);
      const orgAvModal =
        await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
      await orgAvModal.isVideoStreamLoadedOnAVModalSAB();
      await orgAvModal.clickOnEnterStudioOnAvModal();

      await test.step(`Verify Organiser presence in backstage.`, async () => {
        organiserBackstage = new StudioPage(organiserPage, studioController);
        await organiserBackstage.isBackstageVisible();
        await organiserBackstage.verifyCountOfStreamInBackstage(1);
      });
    });

    await test.step("Disable dry-run.", async () => {
      await studioController.disableDryRun();
    });

    await test.step("Disable the recording.", async () => {
      await organiserBackstage.changeRecordingStatus(false);
    });

    await test.step(`Organizer initiates the session and joins the stage.`, async () => {
      await organiserBackstage.addUserToStageFromBackStage(
        organiserFirstName,
        true
      );
      await organiserBackstage.startTheSession();
    });

    await test.step(`Verify recording is diabled during session.`, async () => {
      await organiserBackstage.verifyRecordingStatus(false);
    });

    await test.step(`End the session.`, async () => {
      await organiserBackstage.endTheSession();
    });

    await test.step(`Enable the recording.`, async () => {
      await organiserBackstage.changeRecordingStatus(true);
    });

    await test.step(`Organizer initiates the session.`, async () => {
      await organiserBackstage.startTheSession();
    });

    await test.step(`Verify recording is enabled during session.`, async () => {
      await organiserBackstage.verifyRecordingStatus(true);
    });
    await test.step(`End the session.`, async () => {
      await organiserBackstage.endTheSession();
    });
  });

  test("TC002: Verify recording video is getting listed on organizer side.", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-923/verify-recording-video-is-coming-on-organizer-side",
    });
    test.skip(
      process.env.run_env !== "pre-prod",
      "Skipping recording as it can only run on preprod"
    );

    let organiserStagePage: StagePage;
    let recordingController: RecordingController;

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    recordingController = new RecordingController(orgApiContext, eventId);

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step(`Organizer joins the event and enter default stage.`, async () => {
      await organiserPage.goto(defaultStageUrl);
      organiserStagePage = new StagePage(organiserPage);
      const orgAvModal =
        await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
      await orgAvModal.isVideoStreamLoadedOnAVModalSAB();
      await orgAvModal.clickOnEnterStudioOnAvModal();

      await test.step(`Verify Organiser presence in backstage.`, async () => {
        organiserBackstage = new StudioPage(organiserPage, studioController);
        await organiserBackstage.isBackstageVisible();
        await organiserBackstage.verifyCountOfStreamInBackstage(1);
      });
    });

    await test.step("Disable dry-run.", async () => {
      await studioController.disableDryRun();
    });

    await test.step(`Organizer joins the stage.`, async () => {
      await organiserBackstage.addUserToStageFromBackStage(
        organiserFirstName,
        true
      );
    });

    await test.step(`Enable the recording.`, async () => {
      await organiserBackstage.changeRecordingStatus(true);
    });

    await test.step(`Organizer initiates the session.`, async () => {
      await organiserBackstage.startTheSession();
    });

    await test.step(`Verify recording is enabled during session.`, async () => {
      await organiserBackstage.verifyRecordingStatus(true);
    });

    await test.step(`End the session.`, async () => {
      await setTimeOut(6000);
      await organiserBackstage.endTheSession();
    });
    await test.step(`Verify Recording duration to be Non-Zero`, async () => {
      await recordingController.verifyIfDurationIsNonZero();
    });

    await test.step(`Verify Recording Size to be Non-Zero`, async () => {
      await recordingController.verifyIfSizeByteIsNonZero();
    });
  });
});
