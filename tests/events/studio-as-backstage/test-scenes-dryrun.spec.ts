import test, { APIRequestContext, BrowserContext } from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import BrowserName from "../../../enums/BrowserEnum";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import BrowserFactory from "../../../util/BrowserFactory";
import EventEntryType from "../../../enums/eventEntryEnum";
import { updateEventLandingPageDetails } from "../../../util/apiUtil";
import { EventController } from "../../../controller/EventController";
import { StageController } from "../../../controller/StageController";
import { StudioController } from "../../../controller/StudioController";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import { WebinarStage } from "../../../page-objects/webinar-pages/webinarStage";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(
  `@stage @scenes @dryrun`,
  {
    tag: "@smoke",
  },
  async () => {
    let eventId;
    let stageId;
    let studioId;
    let studioController: StudioController;
    let orgBrowserContext: BrowserContext;
    let orgApiContext: APIRequestContext;
    let attendeeOneBrowserContext: BrowserContext;
    let speakerOneBrowserContext: BrowserContext;
    let eventController: EventController;
    let defaultStageUrl: string;
    let attendeeLandingPage: string;

    test.beforeEach(async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: true,
      });

      orgApiContext = orgBrowserContext.request;

      await test.step("Creating new event.", async () => {
        eventId = await EventController.generateNewEvent(orgApiContext, {
          event_title: DataUtl.getRandomEventTitle(),
        });
      });

      eventController = new EventController(orgApiContext, eventId);

      await test.step("Fetching deafult stage id.", async () => {
        stageId = await eventController.getDefaultStageId();
      });

      await test.step("Enabling Studio as backstage for default stage.", async () => {
        studioId = await eventController.enableStudioAsBackstage(stageId);
      });

      studioController = new StudioController(orgApiContext, studioId, stageId);

      attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });

      speakerOneBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });

      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
        isSpeakermagicLinkEnabled: true,
      });

      let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
      attendeeLandingPage = eventInfoDto.attendeeLandingPage;

      defaultStageUrl = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/l/event/${eventId}/stages/${stageId}`;

      await test.step("Disabling onboarding checks for speaker and attendees access group.", async () => {
        await eventController.getAccessGroupController.handleOnboardingChecksForSpeaker(
          false
        );
        await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
          false
        );
      });

      await test.step(`Disabling recording for this stage.`, async () => {
        let stageController = new StageController(
          orgApiContext,
          eventId,
          stageId
        );
        await stageController.disableRecordingForStudioAsBackstage();
      });
    });

    test.afterEach(async () => {
      await orgBrowserContext?.close();
      await attendeeOneBrowserContext?.close();
      await speakerOneBrowserContext?.close();
    });

    test("TC001: Verify save scene functionality in dry run.", async () => {
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-434/verify-save-scene-functionality-in-dry-run",
      });

      let attendeeOnePage;
      let organiserPage;
      let speakerOnePage;
      let attendeeOneEmail;
      let speakerOneEmail;
      let organiserStagePage: StagePage;
      let attendeeOneStagePage: WebinarStage;
      let speakerStagePage: StagePage;
      let organiserBackstage: StudioPage;
      let speakerBackstage: StudioPage;

      const attendeeOneFirstName = "PrateekAttendeeOne";
      const attendeeOneLastName = "QA";

      const speakerOneFirstName = "PrateekSpeakerOne";
      const speakerOneLastName = "QA";

      const organiserFirstName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

      await test.step("Attendees browser context.", async () => {
        attendeeOnePage = await attendeeOneBrowserContext.newPage();
      });

      await test.step("Organiser browser context.", async () => {
        organiserPage = await orgBrowserContext.newPage();
      });

      await test.step("Speaker browser context.", async () => {
        speakerOnePage = await speakerOneBrowserContext.newPage();
      });

      await test.step("Get random attendee emails.", async () => {
        attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
        console.log(`Attendee One Email: ${attendeeOneEmail}`);
      });

      await test.step("Get random speaker email.", async () => {
        speakerOneEmail = DataUtl.getRandomSpeakerEmail();
      });

      await test.step(`Organiser invites attendee and speaker to the event.`, async () => {
        await test.step(`Invites Attendees.`, async () => {
          await eventController.registerUserToEventWithRegistrationApi({
            firstName: attendeeOneFirstName,
            lastName: attendeeOneLastName,
            phoneNumber: "1234567899",
            email: attendeeOneEmail,
          });
        });

        await test.step(`Invites Speaker.`, async () => {
          await eventController.inviteSpeakerToTheEventByApi(
            speakerOneEmail,
            speakerOneFirstName,
            speakerOneLastName
          );
        });
      });

      await test.step(`Organizer and Speaker join the event and enter default stage.`, async () => {
        await test.step(`Speaker joins the event and enter default stage.`, async () => {
          let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
            eventId,
            speakerOneEmail
          );

          await speakerOnePage.goto(magicLink);
          await speakerOnePage.click(`text=Enter Now`);
          await speakerOnePage.goto(defaultStageUrl);
          speakerStagePage = new StagePage(speakerOnePage);

          const speakerAvModal =
            await speakerStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await speakerAvModal.isVideoStreamLoadedOnAVModalSAB();
          await speakerAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Speaker's presence in backstage.`, async () => {
            speakerBackstage = new StudioPage(speakerOnePage, studioController);
            // await speakerBackstage.closeStudioWalkthrough();
            await speakerBackstage.isBackstageVisible();
            await speakerBackstage.verifyCountOfStreamInBackstage(1);
          });
        });

        await test.step(`Organizer joins the event and join backstage via default stage.`, async () => {
          await organiserPage.goto(attendeeLandingPage);
          await organiserPage.click(`text=Enter Now`);
          await organiserPage.goto(defaultStageUrl);
          organiserStagePage = new StagePage(organiserPage);
          const orgAvModal =
            await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await orgAvModal.isVideoStreamLoadedOnAVModalSAB();
          await orgAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Organiser's and Speaker's stream in backstage.`, async () => {
            organiserBackstage = new StudioPage(
              organiserPage,
              studioController
            );
            await organiserBackstage.isBackstageVisible();
            await organiserBackstage.verifyCountOfStreamInBackstage(2);
          });
        });
      });

      await test.step(`Enable dry-run.`, async () => {
        await studioController.enableDryRun();
      });

      await test.step(`Change the brand color, theme, logo, background and overlay.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          1
        );
      });

      await test.step(`Organizer initiate dry-run, send Speaker to stage and joins the stage.`, async () => {
        await organiserBackstage.addUserToStageFromBackStage(
          organiserFirstName,
          true
        );
        await organiserBackstage.verifyCountOfStreamInStage(1);
        await organiserBackstage.addUserToStageFromBackStage(
          speakerOneFirstName
        );
        await organiserBackstage.verifyCountOfStreamInStage(2);
        await organiserBackstage.startTheSession();
      });

      await test.step(`Attendee joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeOneEmail
        );

        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(defaultStageUrl);
        await attendeeOnePage.waitForURL(defaultStageUrl);
        attendeeOneStagePage = new WebinarStage(attendeeOnePage);
        await attendeeOneStagePage.verifyAttendeeScreenIsLoaded();
      });

      await test.step(`Verify Attendee view || 2 streams.`, async () => {
        await attendeeOneStagePage.verifyNumberOfVisibileStreams(2);
      });

      await test.step(`Save a scene and capture the details.`, async () => {
        await organiserBackstage.getStudioSceneComponent.saveCurrentScene(
          studioId
        );
        await organiserBackstage.getStudioSceneComponent.captureExpectedData();
      });

      await test.step(`Change theme, logo, background and overlay.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          2
        );
        await organiserBackstage.verifyCountOfStreamInBackstage(2);
      });

      await test.step(`Play the scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.playScene();
        await organiserBackstage.verifyCountOfStreamInStage(2);
      });

      await test.step(`Validate the applied scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.validateTheScene();
      });

      await test.step(`End the dry-run.`, async () => {
        await organiserBackstage.endTheSession();
      });
    });
  }
);
