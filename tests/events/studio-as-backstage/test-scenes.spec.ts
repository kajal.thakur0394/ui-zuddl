import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import BrowserName from "../../../enums/BrowserEnum";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import BrowserFactory from "../../../util/BrowserFactory";
import EventEntryType from "../../../enums/eventEntryEnum";
import { updateEventLandingPageDetails } from "../../../util/apiUtil";
import { EventController } from "../../../controller/EventController";
import { StageController } from "../../../controller/StageController";
import { StudioController } from "../../../controller/StudioController";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import { WebinarStage } from "../../../page-objects/webinar-pages/webinarStage";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(
  `@stage @show-flow @scenes`,
  {
    tag: "@smoke",
  },
  async () => {
    test.setTimeout(4 * 60 * 1000);

    let eventId;
    let stageId;
    let studioId;
    let studioController: StudioController;
    let orgBrowserContext: BrowserContext;
    let orgApiContext: APIRequestContext;
    let attendeeOneBrowserContext: BrowserContext;
    let speakerOneBrowserContext: BrowserContext;
    let speakerTwoBrowserContext: BrowserContext;
    let eventController: EventController;
    let defaultStageUrl: string;
    let attendeeLandingPage: string;

    test.beforeEach(async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: true,
      });

      orgApiContext = orgBrowserContext.request;

      await test.step("Creating new event.", async () => {
        eventId = await EventController.generateNewEvent(orgApiContext, {
          event_title: DataUtl.getRandomEventTitle(),
        });
      });

      eventController = new EventController(orgApiContext, eventId);

      await test.step("Fetching deafult stage id.", async () => {
        stageId = await eventController.getDefaultStageId();
      });

      await test.step("Enabling Studio as backstage for default stage.", async () => {
        studioId = await eventController.enableStudioAsBackstage(stageId);
      });

      studioController = new StudioController(orgApiContext, studioId);

      attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });

      speakerOneBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });

      speakerTwoBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });

      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
        isSpeakermagicLinkEnabled: true,
      });

      let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
      attendeeLandingPage = eventInfoDto.attendeeLandingPage;

      defaultStageUrl = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/l/event/${eventId}/stages/${stageId}`;

      await test.step("Disabling onboarding checks for speaker and attendees access group.", async () => {
        await eventController.getAccessGroupController.handleOnboardingChecksForSpeaker(
          false
        );
        await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
          false
        );
      });

      await test.step(`Disabling recording for this stage.`, async () => {
        let stageController = new StageController(
          orgApiContext,
          eventId,
          stageId
        );
        await stageController.disableRecordingForStudioAsBackstage();
      });
    });

    test.afterEach(async () => {
      await orgBrowserContext?.close();
      await attendeeOneBrowserContext?.close();
      await speakerOneBrowserContext?.close();
    });

    test("TC001: Verify flow of saving a scene with multiple user stream and then playing it on stage.", async () => {
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-418/verify-flow-of-saving-a-scene-with-multiple-user-stream-and-then",
      });

      let attendeeOnePage;
      let organiserPage;
      let speakerOnePage;
      let attendeeOneEmail;
      let speakerOneEmail;
      let organiserStagePage: StagePage;
      let attendeeOneStagePage: WebinarStage;
      let speakerStagePage: StagePage;
      let organiserBackstage: StudioPage;
      let speakerBackstage: StudioPage;

      const attendeeOneFirstName = "PrateekAttendeeOne";
      const attendeeOneLastName = "QA";

      const speakerOneFirstName = "PrateekSpeakerOne";
      const speakerOneLastName = "QA";

      const organiserFirstName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

      await test.step("Attendees browser context.", async () => {
        attendeeOnePage = await attendeeOneBrowserContext.newPage();
      });

      await test.step("Organiser browser context.", async () => {
        organiserPage = await orgBrowserContext.newPage();
      });

      await test.step("Speaker browser context.", async () => {
        speakerOnePage = await speakerOneBrowserContext.newPage();
      });

      await test.step("Get random attendee emails.", async () => {
        attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
        console.log(`Attendee One Email: ${attendeeOneEmail}`);
      });

      await test.step("Get random speaker email.", async () => {
        speakerOneEmail = DataUtl.getRandomSpeakerEmail();
      });

      await test.step(`Organiser invites attendee and speaker to the event.`, async () => {
        await test.step(`Invites Attendees.`, async () => {
          await eventController.registerUserToEventWithRegistrationApi({
            firstName: attendeeOneFirstName,
            lastName: attendeeOneLastName,
            phoneNumber: "1234567899",
            email: attendeeOneEmail,
          });
        });

        await test.step(`Invites Speaker.`, async () => {
          await eventController.inviteSpeakerToTheEventByApi(
            speakerOneEmail,
            speakerOneFirstName,
            speakerOneLastName
          );
        });
      });

      await test.step(`Organizer, Speaker and Attendee join the event and enter default stage.`, async () => {
        // Fetch the magic link for the speaker
        let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          eventId,
          speakerOneEmail
        );

        // Define the speaker's steps
        const speakerSteps = async () => {
          await speakerOnePage.goto(magicLink);
          await speakerOnePage.click(`text=Enter Now`);
          await speakerOnePage.goto(defaultStageUrl);
          speakerStagePage = new StagePage(speakerOnePage);

          const speakerAvModal =
            await speakerStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await speakerAvModal.isVideoStreamLoadedOnAVModalSAB();
          await speakerAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Speaker's presence in backstage.`, async () => {
            speakerBackstage = new StudioPage(speakerOnePage, studioController);
            // await speakerBackstage.closeStudioWalkthrough();
            await speakerBackstage.isBackstageVisible();
            await speakerBackstage.verifyCountOfStreamInBackstage(2);
          });
        };

        // Define the organizer's steps
        const organizerSteps = async () => {
          await organiserPage.goto(attendeeLandingPage);
          await organiserPage.click(`text=Enter Now`);
          await organiserPage.goto(defaultStageUrl);
          organiserStagePage = new StagePage(organiserPage);

          const orgAvModal =
            await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await orgAvModal.isVideoStreamLoadedOnAVModalSAB();
          await orgAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Organiser's and Speaker's stream in backstage.`, async () => {
            organiserBackstage = new StudioPage(
              organiserPage,
              studioController
            );
            await organiserBackstage.isBackstageVisible();
            await organiserBackstage.verifyCountOfStreamInBackstage(2);
          });
        };

        // Run speaker, organizer, and attendee steps concurrently
        await Promise.all([speakerSteps(), organizerSteps()]);
      });

      await test.step(`Change the brand color, theme, logo, background and overlay.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          1
        );
      });

      await test.step(`Organizer initiate session, send Speaker to stage and joins the stage.`, async () => {
        await organiserBackstage.addUserToStageFromBackStage(
          organiserFirstName,
          true
        );
        await organiserBackstage.verifyCountOfStreamInStage(1);
        await organiserBackstage.addUserToStageFromBackStage(
          speakerOneFirstName
        );
        await organiserBackstage.verifyCountOfStreamInStage(2);
        await organiserBackstage.startTheSession();
      });

      await test.step(`Attendee joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeOneEmail
        );

        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(defaultStageUrl);
        await attendeeOnePage.waitForURL(defaultStageUrl);
        attendeeOneStagePage = new WebinarStage(attendeeOnePage);
        attendeeOneStagePage.verifyAttendeeScreenIsLoaded();
      });

      await test.step(`Verify Attendee view || 2 streams.`, async () => {
        await attendeeOneStagePage.verifyNumberOfVisibileStreams(2);
      });

      await test.step(`Save a scene and capture the details.`, async () => {
        await organiserBackstage.getStudioSceneComponent.saveCurrentScene(
          studioId
        );
        await organiserBackstage.getStudioSceneComponent.captureExpectedData();
      });

      await test.step(`Change theme, logo, background and overlay.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          2
        );
        await organiserBackstage.verifyCountOfStreamInBackstage(2);
      });

      await test.step(`Play the scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.playScene();
        await organiserBackstage.verifyCountOfStreamInStage(2);
      });

      await test.step(`Validate the applied scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.validateTheScene();
      });

      await test.step(`End the session.`, async () => {
        await organiserBackstage.endTheSession();
      });
    });

    test("TC002: Verify flow of updating existing scene with new one and then verify if played updated content are visible on stage.", async () => {
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-420/verify-flow-of-updating-existing-scene-with-new-one-and-then-verify-if",
      });
      let attendeeOnePage;
      let organiserPage;
      let speakerOnePage;
      let attendeeOneEmail;
      let speakerOneEmail;
      let organiserStagePage: StagePage;
      let attendeeOneStagePage: WebinarStage;
      let speakerStagePage: StagePage;
      let organiserBackstage: StudioPage;
      let speakerBackstage: StudioPage;

      const attendeeOneFirstName = "PrateekAttendeeOne";
      const attendeeOneLastName = "QA";

      const speakerOneFirstName = "PrateekSpeakerOne";
      const speakerOneLastName = "QA";

      const organiserFirstName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

      await test.step("Attendees browser context.", async () => {
        attendeeOnePage = await attendeeOneBrowserContext.newPage();
      });

      await test.step("Organiser browser context.", async () => {
        organiserPage = await orgBrowserContext.newPage();
      });

      await test.step("Speaker browser context.", async () => {
        speakerOnePage = await speakerOneBrowserContext.newPage();
      });

      await test.step("Get random attendee emails.", async () => {
        attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
        console.log(`Attendee One Email: ${attendeeOneEmail}`);
      });

      await test.step("Get random speaker email.", async () => {
        speakerOneEmail = DataUtl.getRandomSpeakerEmail();
      });

      await test.step(`Organiser invites attendee and speaker to the event.`, async () => {
        await test.step(`Invites Attendees.`, async () => {
          await eventController.registerUserToEventWithRegistrationApi({
            firstName: attendeeOneFirstName,
            lastName: attendeeOneLastName,
            phoneNumber: "1234567899",
            email: attendeeOneEmail,
          });
        });

        await test.step(`Invites Speaker.`, async () => {
          await eventController.inviteSpeakerToTheEventByApi(
            speakerOneEmail,
            speakerOneFirstName,
            speakerOneLastName
          );
        });
      });

      await test.step(`Organizer, Speaker and Attendee join the event and enter default stage.`, async () => {
        // Fetch the magic link for the speaker
        let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          eventId,
          speakerOneEmail
        );

        // Define the speaker's steps
        const speakerSteps = async () => {
          await speakerOnePage.goto(magicLink);
          await speakerOnePage.click(`text=Enter Now`);
          await speakerOnePage.goto(defaultStageUrl);
          speakerStagePage = new StagePage(speakerOnePage);

          const speakerAvModal =
            await speakerStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await speakerAvModal.isVideoStreamLoadedOnAVModalSAB();
          await speakerAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Speaker's presence in backstage.`, async () => {
            speakerBackstage = new StudioPage(speakerOnePage, studioController);
            // await speakerBackstage.closeStudioWalkthrough();
            await speakerBackstage.isBackstageVisible();
            await speakerBackstage.verifyCountOfStreamInBackstage(2);
          });
        };

        // Define the organizer's steps
        const organizerSteps = async () => {
          await organiserPage.goto(attendeeLandingPage);
          await organiserPage.click(`text=Enter Now`);
          await organiserPage.goto(defaultStageUrl);
          organiserStagePage = new StagePage(organiserPage);

          const orgAvModal =
            await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await orgAvModal.isVideoStreamLoadedOnAVModalSAB();
          await orgAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Organiser's and Speaker's stream in backstage.`, async () => {
            organiserBackstage = new StudioPage(
              organiserPage,
              studioController
            );
            await organiserBackstage.isBackstageVisible();
            await organiserBackstage.verifyCountOfStreamInBackstage(2);
          });
        };

        // Run speaker, organizer, and attendee steps concurrently
        await Promise.all([speakerSteps(), organizerSteps()]);
      });

      await test.step(`Change the brand color, theme, logo, background and overlay.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          1
        );
      });

      await test.step(`Organizer initiate session, send Speaker to stage and joins the stage.`, async () => {
        await organiserBackstage.addUserToStageFromBackStage(
          organiserFirstName,
          true
        );
        await organiserBackstage.verifyCountOfStreamInStage(1);
        await organiserBackstage.addUserToStageFromBackStage(
          speakerOneFirstName
        );
        await organiserBackstage.verifyCountOfStreamInStage(2);
        await organiserBackstage.startTheSession();
      });

      await test.step(`Attendee joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeOneEmail
        );

        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(defaultStageUrl);
        await attendeeOnePage.waitForURL(defaultStageUrl);
        attendeeOneStagePage = new WebinarStage(attendeeOnePage);
        attendeeOneStagePage.verifyAttendeeScreenIsLoaded();
      });

      await test.step(`Verify Attendee view || 2 streams.`, async () => {
        await attendeeOneStagePage.verifyNumberOfVisibileStreams(2);
      });

      await test.step(`Save currrent scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.saveCurrentScene(
          studioId
        );
      });

      await test.step(`Change the configuration.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          2
        );
        await organiserBackstage.verifyCountOfStreamInBackstage(2);
      });

      await test.step(`Organizer send Speaker to stage.`, async () => {
        await organiserBackstage.addUserToStageFromBackStage(
          speakerOneFirstName
        );
        await organiserBackstage.verifyCountOfStreamInStage(1);
      });

      await test.step(`Update the scene and capture the details.`, async () => {
        await organiserBackstage.getStudioSceneComponent.updateScene();
        await organiserBackstage.getStudioSceneComponent.captureExpectedData();
      });

      await test.step(`Change the configuration.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          3
        );
        await organiserBackstage.verifyCountOfStreamInBackstage(2);
      });

      await test.step(`Play the scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.playScene();
        await organiserBackstage.verifyCountOfStreamInStage(1);
      });

      await test.step(`Validate the applied scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.validateTheScene();
      });

      await test.step(`End the session.`, async () => {
        await organiserBackstage.endTheSession();
      });
    });

    test("TC003: Verify organiser is able to delete the scenes.", async () => {
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-418/verify-flow-of-saving-a-scene-with-multiple-user-stream-and-then",
      });

      let attendeeOnePage;
      let organiserPage;
      let speakerOnePage;
      let attendeeOneEmail;
      let speakerOneEmail;
      let organiserStagePage: StagePage;
      let attendeeOneStagePage: WebinarStage;
      let speakerStagePage: StagePage;
      let organiserBackstage: StudioPage;
      let speakerBackstage: StudioPage;

      const attendeeOneFirstName = "PrateekAttendeeOne";
      const attendeeOneLastName = "QA";

      const speakerOneFirstName = "PrateekSpeakerOne";
      const speakerOneLastName = "QA";

      const organiserFirstName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

      await test.step("Attendees browser context.", async () => {
        attendeeOnePage = await attendeeOneBrowserContext.newPage();
      });

      await test.step("Organiser browser context.", async () => {
        organiserPage = await orgBrowserContext.newPage();
      });

      await test.step("Speaker browser context.", async () => {
        speakerOnePage = await speakerOneBrowserContext.newPage();
      });

      await test.step("Get random attendee emails.", async () => {
        attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
        console.log(`Attendee One Email: ${attendeeOneEmail}`);
      });

      await test.step("Get random speaker email.", async () => {
        speakerOneEmail = DataUtl.getRandomSpeakerEmail();
      });

      await test.step(`Organiser invites attendee and speaker to the event.`, async () => {
        await test.step(`Invites Attendees.`, async () => {
          await eventController.registerUserToEventWithRegistrationApi({
            firstName: attendeeOneFirstName,
            lastName: attendeeOneLastName,
            phoneNumber: "1234567899",
            email: attendeeOneEmail,
          });
        });

        await test.step(`Invites Speaker.`, async () => {
          await eventController.inviteSpeakerToTheEventByApi(
            speakerOneEmail,
            speakerOneFirstName,
            speakerOneLastName
          );
        });
      });

      await test.step(`Organizer and Speaker join the event and enter default stage.`, async () => {
        await test.step(`Speaker joins the event and enter default stage.`, async () => {
          let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
            eventId,
            speakerOneEmail
          );

          await speakerOnePage.goto(magicLink);
          await speakerOnePage.click(`text=Enter Now`);
          await speakerOnePage.goto(defaultStageUrl);
          speakerStagePage = new StagePage(speakerOnePage);

          const speakerAvModal =
            await speakerStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await speakerAvModal.isVideoStreamLoadedOnAVModalSAB();
          await speakerAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Speaker's presence in backstage.`, async () => {
            speakerBackstage = new StudioPage(speakerOnePage, studioController);
            // await speakerBackstage.closeStudioWalkthrough();
            await speakerBackstage.isBackstageVisible();
            await speakerBackstage.verifyCountOfStreamInBackstage(1);
          });
        });

        await test.step(`Organizer joins the event and join backstage via default stage.`, async () => {
          await organiserPage.goto(attendeeLandingPage);
          await organiserPage.click(`text=Enter Now`);
          await organiserPage.goto(defaultStageUrl);
          organiserStagePage = new StagePage(organiserPage);
          const orgAvModal =
            await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await orgAvModal.isVideoStreamLoadedOnAVModalSAB();
          await orgAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Organiser's and Speaker's presence in backstage.`, async () => {
            organiserBackstage = new StudioPage(
              organiserPage,
              studioController
            );
            await organiserBackstage.isBackstageVisible();
            await organiserBackstage.verifyCountOfStreamInBackstage(2);
          });
        });
      });

      await test.step(`Change the brand color, theme, logo, background and overlay.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          1
        );
      });

      await test.step(`Organizer initiate session, send Speaker to stage and joins the stage.`, async () => {
        await organiserBackstage.addUserToStageFromBackStage(
          organiserFirstName,
          true
        );
        await organiserBackstage.verifyCountOfStreamInStage(1);
        await organiserBackstage.addUserToStageFromBackStage(
          speakerOneFirstName
        );
        await organiserBackstage.verifyCountOfStreamInStage(2);
        await organiserBackstage.startTheSession();
      });

      await test.step(`Attendee joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeOneEmail
        );

        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(defaultStageUrl);
        await attendeeOnePage.waitForURL(defaultStageUrl);
        attendeeOneStagePage = new WebinarStage(attendeeOnePage);
        attendeeOneStagePage.verifyAttendeeScreenIsLoaded();
      });

      await test.step(`Verify Attendee view || 2 streams.`, async () => {
        await attendeeOneStagePage.verifyNumberOfVisibileStreams(2);
      });

      await test.step(`Save a scene and capture the details.`, async () => {
        await organiserBackstage.getStudioSceneComponent.saveCurrentScene(
          studioId
        );
        await organiserBackstage.getStudioSceneComponent.captureExpectedData();
      });

      await test.step(`Change theme, logo, background and overlay.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          2
        );
        await organiserBackstage.verifyCountOfStreamInBackstage(2);
      });

      await test.step(`Play the scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.playScene();
        await organiserBackstage.verifyCountOfStreamInStage(2);
      });

      await test.step(`Validate the applied scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.validateTheScene();
      });

      await test.step(`Delete the scene and verify the scene is deleted.`, async () => {
        await organiserBackstage.getStudioSceneComponent.deleteScene();
        await organiserBackstage.getStudioSceneComponent.verifySceneIsAbsent();
      });

      await test.step(`End the session.`, async () => {
        await organiserBackstage.endTheSession();
      });
    });

    test("TC004: Verify saving scene with placeholder of speakers.", async () => {
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-418/verify-flow-of-saving-a-scene-with-multiple-user-stream-and-then",
      });

      let attendeeOnePage;
      let organiserPage;
      let speakerOnePage;
      let speakerTwoPage;
      let attendeeOneEmail;
      let speakerOneEmail;
      let speakerTwoEmail;
      let organiserStagePage: StagePage;
      let attendeeOneStagePage: WebinarStage;
      let speakerOneStagePage: StagePage;
      let speakerTwoStagePage: StagePage;
      let organiserBackstage: StudioPage;
      let speakerOneBackstage: StudioPage;
      let speakerTwoBackstage: StudioPage;

      const attendeeOneFirstName = "PrateekAttendeeOne";
      const attendeeOneLastName = "QA";

      const speakerOneFirstName = "PrateekSpeakerOne";
      const speakerOneLastName = "QA";
      const spekaerOneFullName = `${speakerOneFirstName} ${speakerOneLastName}`;

      const speakerTwoFirstName = "PrateekSpeakerTwo";
      const speakerTwoLastName = "QA";
      const spekaerTwoFullName = `${speakerTwoFirstName} ${speakerTwoLastName}`;

      const organiserFirstName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

      await test.step("Attendees browser context.", async () => {
        attendeeOnePage = await attendeeOneBrowserContext.newPage();
      });

      await test.step("Organiser browser context.", async () => {
        organiserPage = await orgBrowserContext.newPage();
      });

      await test.step("Speaker one browser context.", async () => {
        speakerOnePage = await speakerOneBrowserContext.newPage();
      });

      await test.step("Speaker two browser context.", async () => {
        speakerTwoPage = await speakerTwoBrowserContext.newPage();
      });

      await test.step("Get random attendee emails.", async () => {
        attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
        console.log(`Attendee One Email: ${attendeeOneEmail}`);
      });

      await test.step("Get random speaker emails.", async () => {
        speakerOneEmail = DataUtl.getRandomSpeakerEmail();
        console.log(`Speaker one email - ${speakerOneEmail}.`);
        speakerTwoEmail = DataUtl.getRandomSpeakerEmail();
        console.log(`Speaker two email - ${speakerTwoEmail}.`);
      });

      await test.step(`Organiser invites attendee and speaker to the event.`, async () => {
        await test.step(`Invites Attendees.`, async () => {
          await eventController.registerUserToEventWithRegistrationApi({
            firstName: attendeeOneFirstName,
            lastName: attendeeOneLastName,
            phoneNumber: "1234567899",
            email: attendeeOneEmail,
          });
        });

        await test.step(`Invites Speakers.`, async () => {
          await eventController.inviteSpeakerToTheEventByApi(
            speakerOneEmail,
            speakerOneFirstName,
            speakerOneLastName
          );

          await eventController.inviteSpeakerToTheEventByApi(
            speakerTwoEmail,
            speakerTwoFirstName,
            speakerTwoLastName
          );
        });
      });

      await test.step(`Organizer joins the event and enter default stage.`, async () => {
        await test.step(`Organizer joins the event and join backstage via default stage.`, async () => {
          await organiserPage.goto(attendeeLandingPage);
          await organiserPage.click(`text=Enter Now`);
          await organiserPage.goto(defaultStageUrl);
          organiserStagePage = new StagePage(organiserPage);
          const orgAvModal =
            await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await orgAvModal.isVideoStreamLoadedOnAVModalSAB();
          await orgAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Organiser's stream in backstage.`, async () => {
            organiserBackstage = new StudioPage(
              organiserPage,
              studioController
            );
            await organiserBackstage.isBackstageVisible();
            await organiserBackstage.verifyCountOfStreamInBackstage(1);
          });
        });
      });

      await test.step(`Change the brand color, theme, logo, background and overlay.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          1
        );
      });

      await test.step(`Add the Speaker Placeholders.`, async () => {
        await organiserBackstage.getStudioSceneComponent.addSpeakerPlaceholders(
          [spekaerOneFullName, spekaerTwoFullName]
        );
      });

      await test.step(`Verify Placeholder count.`, async () => {
        await organiserBackstage.getStudioSceneComponent.verifyCountOfPlaceholderInStage(
          2
        );
      });

      await test.step(`Organizer joins the stage.`, async () => {
        await organiserBackstage.addUserToStageFromBackStage(
          organiserFirstName,
          true
        );
        await organiserBackstage.verifyCountOfStreamInStage(3);
      });

      await test.step(`Save a scene and capture the details.`, async () => {
        await organiserBackstage.getStudioSceneComponent.saveCurrentScene(
          studioId
        );
        await organiserBackstage.getStudioSceneComponent.captureExpectedData();
      });

      await test.step(`Speaker one joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          eventId,
          speakerOneEmail
        );

        await speakerOnePage.goto(magicLink);
        await speakerOnePage.click(`text=Enter Now`);
        await speakerOnePage.goto(defaultStageUrl);
        speakerOneStagePage = new StagePage(speakerOnePage);

        const speakerAvModal =
          await speakerOneStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
        await speakerAvModal.isVideoStreamLoadedOnAVModalSAB();
        await speakerAvModal.clickOnEnterStudioOnAvModal();

        await test.step(`Verify Speaker's presence in backstage.`, async () => {
          speakerOneBackstage = new StudioPage(
            speakerOnePage,
            studioController
          );
          // await speakerOneBackstage.closeStudioWalkthrough();
          await speakerOneBackstage.isBackstageVisible();
          await speakerOneBackstage.verifyCountOfStreamInBackstage(1);
        });
      });

      await test.step(`Speaker two joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          eventId,
          speakerTwoEmail
        );

        await speakerTwoPage.goto(magicLink);
        await speakerTwoPage.click(`text=Enter Now`);
        await speakerTwoPage.goto(defaultStageUrl);
        speakerTwoStagePage = new StagePage(speakerTwoPage);

        const speakerAvModal =
          await speakerTwoStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
        await speakerAvModal.isVideoStreamLoadedOnAVModalSAB();
        await speakerAvModal.clickOnEnterStudioOnAvModal();

        await test.step(`Verify Speaker's presence in backstage.`, async () => {
          speakerTwoBackstage = new StudioPage(
            speakerTwoPage,
            studioController
          );
          // await speakerTwoBackstage.closeStudioWalkthrough();
          await speakerTwoBackstage.isBackstageVisible();
          await speakerTwoBackstage.verifyCountOfStreamInBackstage(2);
        });
      });

      await test.step(`Organizer initiate the session.`, async () => {
        await organiserBackstage.startTheSession();
      });

      await test.step(`Attendee joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeOneEmail
        );
        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(defaultStageUrl);
        await attendeeOnePage.waitForURL(defaultStageUrl);
        attendeeOneStagePage = new WebinarStage(attendeeOnePage);
        attendeeOneStagePage.verifyAttendeeScreenIsLoaded();
      });

      await test.step(`Verify Attendee view || 1 stream.`, async () => {
        await attendeeOneStagePage.verifyNumberOfVisibileStreams(1);
      });

      await test.step(`Change theme, logo, background and overlay.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          2
        );
        await organiserBackstage.verifyCountOfStreamInBackstage(3);
      });

      await test.step(`Play the scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.playScene();
        await organiserBackstage.verifyCountOfStreamInStage(3);
      });

      await test.step(`Verify Attendee view || 3 streams.`, async () => {
        await attendeeOneStagePage.verifyNumberOfVisibileStreams(3);
      });

      await test.step(`Validate the applied scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.validateTheScene();
      });

      await test.step(`End the session.`, async () => {
        await organiserBackstage.endTheSession();
      });
    });

    test("TC005: Verify impact on scene if user stream is no more present on the backstage.", async () => {
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-424/verify-impact-on-scene-if-user-stream-is-no-more-present-on-the",
      });

      let attendeeOnePage;
      let organiserPage;
      let speakerOnePage;
      let speakerTwoPage;
      let attendeeOneEmail;
      let speakerOneEmail;
      let speakerTwoEmail;
      let organiserStagePage: StagePage;
      let attendeeOneStagePage: WebinarStage;
      let speakerOneStagePage: StagePage;
      let organiserBackstage: StudioPage;
      let speakerOneBackstage: StudioPage;

      const attendeeOneFirstName = "PrateekAttendeeOne";
      const attendeeOneLastName = "QA";

      const speakerOneFirstName = "PrateekSpeakerOne";
      const speakerOneLastName = "QA";
      const spekaerOneFullName = `${speakerOneFirstName} ${speakerOneLastName}`;

      const speakerTwoFirstName = "PrateekSpeakerTwo";
      const speakerTwoLastName = "QA";
      const spekaerTwoFullName = `${speakerTwoFirstName} ${speakerTwoLastName}`;

      const organiserFirstName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

      await test.step("Attendees browser context.", async () => {
        attendeeOnePage = await attendeeOneBrowserContext.newPage();
      });

      await test.step("Organiser browser context.", async () => {
        organiserPage = await orgBrowserContext.newPage();
      });

      await test.step("Speaker one browser context.", async () => {
        speakerOnePage = await speakerOneBrowserContext.newPage();
      });

      await test.step("Speaker two browser context.", async () => {
        speakerTwoPage = await speakerTwoBrowserContext.newPage();
      });

      await test.step("Get random attendee emails.", async () => {
        attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
        console.log(`Attendee One Email: ${attendeeOneEmail}`);
      });

      await test.step("Get random speaker emails.", async () => {
        speakerOneEmail = DataUtl.getRandomSpeakerEmail();
        console.log(`Speaker one email - ${speakerOneEmail}.`);
        speakerTwoEmail = DataUtl.getRandomSpeakerEmail();
        console.log(`Speaker two email - ${speakerTwoEmail}.`);
      });

      await test.step(`Organiser invites attendee and speaker to the event.`, async () => {
        await test.step(`Invites Attendees.`, async () => {
          await eventController.registerUserToEventWithRegistrationApi({
            firstName: attendeeOneFirstName,
            lastName: attendeeOneLastName,
            phoneNumber: "1234567899",
            email: attendeeOneEmail,
          });
        });

        await test.step(`Invites Speakers.`, async () => {
          await eventController.inviteSpeakerToTheEventByApi(
            speakerOneEmail,
            speakerOneFirstName,
            speakerOneLastName
          );

          await eventController.inviteSpeakerToTheEventByApi(
            speakerTwoEmail,
            speakerTwoFirstName,
            speakerTwoLastName
          );
        });
      });

      await test.step(`Organizer joins the event and enter default stage.`, async () => {
        await test.step(`Organizer joins the event and join backstage via default stage.`, async () => {
          await organiserPage.goto(attendeeLandingPage);
          await organiserPage.click(`text=Enter Now`);
          await organiserPage.goto(defaultStageUrl);
          organiserStagePage = new StagePage(organiserPage);
          const orgAvModal =
            await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await orgAvModal.isVideoStreamLoadedOnAVModalSAB();
          await orgAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Organiser's stream in backstage.`, async () => {
            organiserBackstage = new StudioPage(
              organiserPage,
              studioController
            );
            await organiserBackstage.isBackstageVisible();
            await organiserBackstage.verifyCountOfStreamInBackstage(1);
          });
        });
      });

      await test.step(`Change the brand color, theme, logo, background and overlay.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          1
        );
      });

      await test.step(`Add the Speaker Placeholders.`, async () => {
        await organiserBackstage.getStudioSceneComponent.addSpeakerPlaceholders(
          [spekaerOneFullName, spekaerTwoFullName]
        );
      });

      await test.step(`Verify Placeholder count.`, async () => {
        await organiserBackstage.getStudioSceneComponent.verifyCountOfPlaceholderInStage(
          2
        );
      });

      await test.step(`Organizer joins the stage.`, async () => {
        await organiserBackstage.addUserToStageFromBackStage(
          organiserFirstName,
          true
        );
        await organiserBackstage.verifyCountOfStreamInStage(3);
      });

      await test.step(`Save a scene and capture the details.`, async () => {
        await organiserBackstage.getStudioSceneComponent.saveCurrentScene(
          studioId
        );
        await organiserBackstage.getStudioSceneComponent.captureExpectedData();
      });

      await test.step(`Speaker one joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          eventId,
          speakerOneEmail
        );

        await speakerOnePage.goto(magicLink);
        await speakerOnePage.click(`text=Enter Now`);
        await speakerOnePage.goto(defaultStageUrl);
        speakerOneStagePage = new StagePage(speakerOnePage);

        const speakerAvModal =
          await speakerOneStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
        await speakerAvModal.isVideoStreamLoadedOnAVModalSAB();
        await speakerAvModal.clickOnEnterStudioOnAvModal();

        await test.step(`Verify Speaker's presence in backstage.`, async () => {
          speakerOneBackstage = new StudioPage(
            speakerOnePage,
            studioController
          );
          // await speakerOneBackstage.closeStudioWalkthrough();
          await speakerOneBackstage.isBackstageVisible();
          await speakerOneBackstage.verifyCountOfStreamInBackstage(1);
        });
      });

      await test.step(`Organizer initiate the session.`, async () => {
        await organiserBackstage.startTheSession();
      });

      await test.step(`Attendee joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeOneEmail
        );
        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(defaultStageUrl);
        await attendeeOnePage.waitForURL(defaultStageUrl);
        attendeeOneStagePage = new WebinarStage(attendeeOnePage);
        attendeeOneStagePage.verifyAttendeeScreenIsLoaded();
      });

      await test.step(`Verify Attendee view || 1 stream.`, async () => {
        await attendeeOneStagePage.verifyNumberOfVisibileStreams(1);
      });

      await test.step(`Change theme, logo, background and overlay.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          2
        );
        await organiserBackstage.verifyCountOfStreamInBackstage(2);
      });

      await test.step(`Play the scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.playScene();
        await organiserBackstage.verifyCountOfStreamInStage(2);
      });

      await test.step(`Verify Attendee view || 2 streams.`, async () => {
        await attendeeOneStagePage.verifyNumberOfVisibileStreams(2);
      });

      await test.step(`Validate the applied scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.validateTheScene();
      });

      await test.step(`End the session.`, async () => {
        await organiserBackstage.endTheSession();
      });
    });

    test("TC006: Verify AV stream state had no impact on saving/playing scene. || AV On when scene is saved, AV Off when scene is played.", async () => {
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-435/av-stream-on-when-saving-a-scene-and-av-stream-off-when-playing-the",
      });

      let attendeeOnePage;
      let organiserPage;
      let speakerOnePage;
      let attendeeOneEmail;
      let speakerOneEmail;
      let organiserStagePage: StagePage;
      let attendeeOneStagePage: WebinarStage;
      let speakerStagePage: StagePage;
      let organiserBackstage: StudioPage;
      let speakerBackstage: StudioPage;

      const attendeeOneFirstName = "PrateekAttendeeOne";
      const attendeeOneLastName = "QA";

      const speakerOneFirstName = "PrateekSpeakerOne";
      const speakerOneLastName = "QA";

      const organiserFirstName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

      await test.step("Attendees browser context.", async () => {
        attendeeOnePage = await attendeeOneBrowserContext.newPage();
      });

      await test.step("Organiser browser context.", async () => {
        organiserPage = await orgBrowserContext.newPage();
      });

      await test.step("Speaker browser context.", async () => {
        speakerOnePage = await speakerOneBrowserContext.newPage();
      });

      await test.step("Get random attendee emails.", async () => {
        attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
        console.log(`Attendee One Email: ${attendeeOneEmail}`);
      });

      await test.step("Get random speaker email.", async () => {
        speakerOneEmail = DataUtl.getRandomSpeakerEmail();
      });

      await test.step(`Organiser invites attendee and speaker to the event.`, async () => {
        await test.step(`Invites Attendees.`, async () => {
          await eventController.registerUserToEventWithRegistrationApi({
            firstName: attendeeOneFirstName,
            lastName: attendeeOneLastName,
            phoneNumber: "1234567899",
            email: attendeeOneEmail,
          });
        });

        await test.step(`Invites Speaker.`, async () => {
          await eventController.inviteSpeakerToTheEventByApi(
            speakerOneEmail,
            speakerOneFirstName,
            speakerOneLastName
          );
        });
      });

      await test.step(`Organizer, Speaker and Attendee join the event and enter default stage.`, async () => {
        // Fetch the magic link for the speaker
        let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          eventId,
          speakerOneEmail
        );

        // Define the speaker's steps
        const speakerSteps = async () => {
          await speakerOnePage.goto(magicLink);
          await speakerOnePage.click(`text=Enter Now`);
          await speakerOnePage.goto(defaultStageUrl);
          speakerStagePage = new StagePage(speakerOnePage);

          const speakerAvModal =
            await speakerStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await speakerAvModal.isVideoStreamLoadedOnAVModalSAB();
          await speakerAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Speaker's presence in backstage.`, async () => {
            speakerBackstage = new StudioPage(speakerOnePage, studioController);
            // await speakerBackstage.closeStudioWalkthrough();
            await speakerBackstage.isBackstageVisible();
            await speakerBackstage.verifyCountOfStreamInBackstage(2);
          });
        };

        // Define the organizer's steps
        const organizerSteps = async () => {
          await organiserPage.goto(attendeeLandingPage);
          await organiserPage.click(`text=Enter Now`);
          await organiserPage.goto(defaultStageUrl);
          organiserStagePage = new StagePage(organiserPage);

          const orgAvModal =
            await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await orgAvModal.isVideoStreamLoadedOnAVModalSAB();
          await orgAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Organiser's and Speaker's stream in backstage.`, async () => {
            organiserBackstage = new StudioPage(
              organiserPage,
              studioController
            );
            await organiserBackstage.isBackstageVisible();
            await organiserBackstage.verifyCountOfStreamInBackstage(2);
          });
        };

        // Run speaker, organizer, and attendee steps concurrently
        await Promise.all([speakerSteps(), organizerSteps()]);
      });

      await test.step(`Change the brand color, theme, logo, background and overlay.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          1
        );
      });

      await test.step(`Organizer initiate session, send Speaker to stage and joins the stage.`, async () => {
        await organiserBackstage.addUserToStageFromBackStage(
          organiserFirstName,
          true
        );
        await organiserBackstage.verifyCountOfStreamInStage(1);
        await organiserBackstage.addUserToStageFromBackStage(
          speakerOneFirstName
        );
        await organiserBackstage.verifyCountOfStreamInStage(2);
        await organiserBackstage.startTheSession();
      });

      await test.step(`Attendee joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeOneEmail
        );

        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(defaultStageUrl);
        await attendeeOnePage.waitForURL(defaultStageUrl);
        attendeeOneStagePage = new WebinarStage(attendeeOnePage);
        attendeeOneStagePage.verifyAttendeeScreenIsLoaded();
      });

      await test.step(`Verify Attendee view || 2 streams.`, async () => {
        await attendeeOneStagePage.verifyNumberOfVisibileStreams(2);
      });

      await test.step(`Save a scene and capture the details.`, async () => {
        await organiserBackstage.getStudioSceneComponent.saveCurrentScene(
          studioId
        );
        await organiserBackstage.getStudioSceneComponent.captureExpectedData();
      });

      await test.step(`Change theme, logo, background and overlay.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          2
        );
        await organiserBackstage.verifyCountOfStreamInBackstage(2);
      });

      await test.step(`Turn off Audio/Video for both Organiser and Speaker.`, async () => {
        await organiserBackstage.getStudioAvModal.turnAudioOff();
        await organiserBackstage.getStudioAvModal.turnVideoOff();
        await speakerBackstage.getStudioAvModal.turnAudioOff();
        await speakerBackstage.getStudioAvModal.turnVideoOff();
      });

      await test.step(`Play the scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.playScene();
        await organiserBackstage.verifyCountOfStreamInStage(2);
      });

      await test.step(`Validate the applied scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.validateTheScene();
      });

      await test.step(`Verify Audio/Video state.`, async () => {
        await organiserBackstage.getStudioAvModal.verifyAudioState(false);
        await organiserBackstage.getStudioAvModal.verifyVideoState(false);
        await speakerBackstage.getStudioAvModal.verifyAudioState(false);
        await speakerBackstage.getStudioAvModal.verifyVideoState(false);
      });

      await test.step(`End the session.`, async () => {
        await organiserBackstage.endTheSession();
      });
    });

    test("TC007: Verify Audio/Video stream state had no impact on saving/playing scene. || AV Off when scene is saved, AV On when scene is played.", async () => {
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-436/av-stream-off-when-saving-a-scene-and-av-stream-on-when-playing-a",
      });

      let attendeeOnePage;
      let organiserPage;
      let speakerOnePage;
      let attendeeOneEmail;
      let speakerOneEmail;
      let organiserStagePage: StagePage;
      let attendeeOneStagePage: WebinarStage;
      let speakerStagePage: StagePage;
      let organiserBackstage: StudioPage;
      let speakerBackstage: StudioPage;

      const attendeeOneFirstName = "PrateekAttendeeOne";
      const attendeeOneLastName = "QA";

      const speakerOneFirstName = "PrateekSpeakerOne";
      const speakerOneLastName = "QA";

      const organiserFirstName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

      await test.step("Attendees browser context.", async () => {
        attendeeOnePage = await attendeeOneBrowserContext.newPage();
      });

      await test.step("Organiser browser context.", async () => {
        organiserPage = await orgBrowserContext.newPage();
      });

      await test.step("Speaker browser context.", async () => {
        speakerOnePage = await speakerOneBrowserContext.newPage();
      });

      await test.step("Get random attendee emails.", async () => {
        attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
        console.log(`Attendee One Email: ${attendeeOneEmail}`);
      });

      await test.step("Get random speaker email.", async () => {
        speakerOneEmail = DataUtl.getRandomSpeakerEmail();
      });

      await test.step(`Organiser invites attendee and speaker to the event.`, async () => {
        await test.step(`Invites Attendees.`, async () => {
          await eventController.registerUserToEventWithRegistrationApi({
            firstName: attendeeOneFirstName,
            lastName: attendeeOneLastName,
            phoneNumber: "1234567899",
            email: attendeeOneEmail,
          });
        });

        await test.step(`Invites Speaker.`, async () => {
          await eventController.inviteSpeakerToTheEventByApi(
            speakerOneEmail,
            speakerOneFirstName,
            speakerOneLastName
          );
        });
      });

      await test.step(`Organizer, Speaker and Attendee join the event and enter default stage.`, async () => {
        // Fetch the magic link for the speaker
        let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          eventId,
          speakerOneEmail
        );

        // Define the speaker's steps
        const speakerSteps = async () => {
          await speakerOnePage.goto(magicLink);
          await speakerOnePage.click(`text=Enter Now`);
          await speakerOnePage.goto(defaultStageUrl);
          speakerStagePage = new StagePage(speakerOnePage);

          const speakerAvModal =
            await speakerStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await speakerAvModal.isVideoStreamLoadedOnAVModalSAB();
          await speakerAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Speaker's presence in backstage.`, async () => {
            speakerBackstage = new StudioPage(speakerOnePage, studioController);
            // await speakerBackstage.closeStudioWalkthrough();
            await speakerBackstage.isBackstageVisible();
            await speakerBackstage.verifyCountOfStreamInBackstage(2);
          });
        };

        // Define the organizer's steps
        const organizerSteps = async () => {
          await organiserPage.goto(attendeeLandingPage);
          await organiserPage.click(`text=Enter Now`);
          await organiserPage.goto(defaultStageUrl);
          organiserStagePage = new StagePage(organiserPage);

          const orgAvModal =
            await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await orgAvModal.isVideoStreamLoadedOnAVModalSAB();
          await orgAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Organiser's and Speaker's stream in backstage.`, async () => {
            organiserBackstage = new StudioPage(
              organiserPage,
              studioController
            );
            await organiserBackstage.isBackstageVisible();
            await organiserBackstage.verifyCountOfStreamInBackstage(2);
          });
        };

        // Run speaker, organizer, and attendee steps concurrently
        await Promise.all([speakerSteps(), organizerSteps()]);
      });

      await test.step(`Change the brand color, theme, logo, background and overlay.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          1
        );
      });

      await test.step(`Organizer initiate session, send Speaker to stage and joins the stage.`, async () => {
        await organiserBackstage.addUserToStageFromBackStage(
          organiserFirstName,
          true
        );
        await organiserBackstage.verifyCountOfStreamInStage(1);
        await organiserBackstage.addUserToStageFromBackStage(
          speakerOneFirstName
        );
        await organiserBackstage.verifyCountOfStreamInStage(2);
        await organiserBackstage.startTheSession();
      });

      await test.step(`Attendee joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeOneEmail
        );

        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(defaultStageUrl);
        await attendeeOnePage.waitForURL(defaultStageUrl);
        attendeeOneStagePage = new WebinarStage(attendeeOnePage);
        attendeeOneStagePage.verifyAttendeeScreenIsLoaded();
      });

      await test.step(`Turn off Audio/Video for both Organiser and Speaker.`, async () => {
        await organiserBackstage.getStudioAvModal.turnAudioOff();
        await organiserBackstage.getStudioAvModal.turnVideoOff();
        await speakerBackstage.getStudioAvModal.turnAudioOff();
        await speakerBackstage.getStudioAvModal.turnVideoOff();
      });

      await test.step(`Verify Attendee view || 2 streams.`, async () => {
        await attendeeOneStagePage.verifyNumberOfVisibileStreams(2);
      });

      await test.step(`Save a scene and capture the details.`, async () => {
        await organiserBackstage.getStudioSceneComponent.saveCurrentScene(
          studioId
        );
        await organiserBackstage.getStudioSceneComponent.captureExpectedData();
      });

      await test.step(`Change theme, logo, background and overlay.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          2
        );
        await organiserBackstage.verifyCountOfStreamInBackstage(2);
      });

      await test.step(`Turn on Audio/Video for both Organiser and Speaker.`, async () => {
        await organiserBackstage.getStudioAvModal.turnAudioOn();
        await organiserBackstage.getStudioAvModal.turnVideoOn();
        await speakerBackstage.getStudioAvModal.turnAudioOn();
        await speakerBackstage.getStudioAvModal.turnVideoOn();
      });

      await test.step(`Play the scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.playScene();
        await organiserBackstage.verifyCountOfStreamInStage(2);
      });

      await test.step(`Validate the applied scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.validateTheScene();
      });

      await test.step(`Verify Audio/Video state.`, async () => {
        await organiserBackstage.getStudioAvModal.verifyAudioState(true);
        await organiserBackstage.getStudioAvModal.verifyVideoState(true);
        await speakerBackstage.getStudioAvModal.verifyAudioState(true);
        await speakerBackstage.getStudioAvModal.verifyVideoState(true);
      });

      await test.step(`End the session.`, async () => {
        await organiserBackstage.endTheSession();
      });
    });

    test("TC008: Verify scene can be saved with pdf presentation along with slide number and when played, it starts broadcasting pdf from that slide.", async () => {
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-419/verify-scene-can-be-saved-with-pdf-presentation-along-with-slide",
      });

      let attendeeOnePage;
      let organiserPage;
      let speakerOnePage;
      let attendeeOneEmail: string;
      let speakerOneEmail: string;
      let organiserStagePage: StagePage;
      let attendeeOneStagePage: WebinarStage;
      let speakerStagePage: StagePage;
      let organiserBackstage: StudioPage;
      let speakerBackstage: StudioPage;
      let beforeSlideNumber: string;
      let afterSlideNumber: string;

      const attendeeOneFirstName = "PrateekAttendeeOne";
      const attendeeOneLastName = "QA";

      const speakerOneFirstName = "PrateekSpeakerOne";
      const speakerOneLastName = "QA";

      const organiserFirstName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

      await test.step("Attendees browser context.", async () => {
        attendeeOnePage = await attendeeOneBrowserContext.newPage();
      });

      await test.step("Organiser browser context.", async () => {
        organiserPage = await orgBrowserContext.newPage();
      });

      await test.step("Speaker browser context.", async () => {
        speakerOnePage = await speakerOneBrowserContext.newPage();
      });

      await test.step("Get random attendee emails.", async () => {
        attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
        console.log(`Attendee One Email: ${attendeeOneEmail}`);
      });

      await test.step("Get random speaker email.", async () => {
        speakerOneEmail = DataUtl.getRandomSpeakerEmail();
      });

      await test.step(`Organiser invites attendee and speaker to the event.`, async () => {
        await test.step(`Invites Attendee.`, async () => {
          await eventController.registerUserToEventWithRegistrationApi({
            firstName: attendeeOneFirstName,
            lastName: attendeeOneLastName,
            phoneNumber: "1234567899",
            email: attendeeOneEmail,
          });
        });

        await test.step(`Invites Speaker.`, async () => {
          await eventController.inviteSpeakerToTheEventByApi(
            speakerOneEmail,
            speakerOneFirstName,
            speakerOneLastName
          );
        });
      });

      await test.step(`Organizer and Speaker join the event and enter default stage.`, async () => {
        await test.step(`Speaker joins the event and enter default stage.`, async () => {
          let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
            eventId,
            speakerOneEmail
          );

          await speakerOnePage.goto(magicLink);
          await speakerOnePage.click(`text=Enter Now`);
          await speakerOnePage.goto(defaultStageUrl);
          speakerStagePage = new StagePage(speakerOnePage);

          const speakerAvModal =
            await speakerStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await speakerAvModal.isVideoStreamLoadedOnAVModalSAB();
          await speakerAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Speaker's presence in backstage.`, async () => {
            speakerBackstage = new StudioPage(speakerOnePage, studioController);
            // await speakerBackstage.closeStudioWalkthrough();
            await speakerBackstage.isBackstageVisible();
            await speakerBackstage.verifyCountOfStreamInBackstage(1);
          });
        });

        await test.step(`Organizer joins the event and join backstage via default stage.`, async () => {
          await organiserPage.goto(attendeeLandingPage);
          await organiserPage.click(`text=Enter Now`);
          await organiserPage.goto(defaultStageUrl);
          organiserStagePage = new StagePage(organiserPage);
          const orgAvModal =
            await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
          await orgAvModal.isVideoStreamLoadedOnAVModalSAB();
          await orgAvModal.clickOnEnterStudioOnAvModal();

          await test.step(`Verify Organiser's and Speaker's stream in backstage.`, async () => {
            organiserBackstage = new StudioPage(
              organiserPage,
              studioController
            );
            await organiserBackstage.isBackstageVisible();
            await organiserBackstage.verifyCountOfStreamInBackstage(2);
          });
        });
      });

      await test.step(`Change the brand color, theme, logo, background and overlay.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          1
        );
      });

      await test.step(`Organizer initiate session, send Speaker to stage and joins the stage.`, async () => {
        await organiserBackstage.addUserToStageFromBackStage(
          organiserFirstName,
          true
        );
        await organiserBackstage.verifyCountOfStreamInStage(1);
        await organiserBackstage.addUserToStageFromBackStage(
          speakerOneFirstName
        );
        await organiserBackstage.verifyCountOfStreamInStage(2);
        await organiserBackstage.startTheSession();
      });

      await test.step(`Attendee joins the event and enter default stage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeOneEmail
        );

        await attendeeOnePage.goto(magicLink);
        await attendeeOnePage.click(`text=Enter Now`);
        await attendeeOnePage.goto(defaultStageUrl);
        await attendeeOnePage.waitForURL(defaultStageUrl);
        attendeeOneStagePage = new WebinarStage(attendeeOnePage);
        await attendeeOneStagePage.verifyAttendeeScreenIsLoaded();
      });

      await test.step(`Organiser uploades a PDF and present it on stage.`, async () => {
        await organiserBackstage.getContentPanel.uploadPdfOnStage("sample");
        await organiserBackstage.getContentPanel.playPdfOnStage("sample");
      });

      await test.step(`Turn few pages of PDF.`, async () => {
        beforeSlideNumber =
          await organiserBackstage.getContentPanel.turnNextPage(5);
      });

      await test.step(`Verify Attendee view || 2 streams and PDF.`, async () => {
        await attendeeOneStagePage.verifyNumberOfVisibileStreams(2);
        await attendeeOneStagePage.verifyPresentationIsVisible(true);
      });

      await test.step(`Save a scene and capture the details.`, async () => {
        await organiserBackstage.getStudioSceneComponent.saveCurrentScene(
          studioId
        );
        await organiserBackstage.getStudioSceneComponent.captureExpectedData();
      });

      await test.step(`Change theme, logo, background and overlay.`, async () => {
        await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
          2
        );
        await organiserBackstage.verifyCountOfStreamInBackstage(2);
      });

      await test.step(`Play the scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.playScene();
        await organiserBackstage.verifyCountOfStreamInStage(3);
        afterSlideNumber =
          await organiserBackstage.getContentPanel.getCurrentSlideNumber();
      });

      await test.step(`Validate the applied scene.`, async () => {
        await organiserBackstage.getStudioSceneComponent.validateTheScene();

        await test.step(`Validate the Slide number.`, async () => {
          expect(beforeSlideNumber).toBe(afterSlideNumber);
        });
      });

      await test.step(`End the session.`, async () => {
        await organiserBackstage.endTheSession();
      });
    });
  }
);
