import test, { APIRequestContext, BrowserContext } from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import BrowserName from "../../../enums/BrowserEnum";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import BrowserFactory from "../../../util/BrowserFactory";
import EventEntryType from "../../../enums/eventEntryEnum";
import { EventController } from "../../../controller/EventController";
import { updateEventLandingPageDetails } from "../../../util/apiUtil";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(
  `@speakerAsAttendee @organiserAsAttendee`,
  {
    tag: "@smoke",
  },
  async () => {
    let eventId;
    let stageId;
    let studioId;
    let orgBrowserContext: BrowserContext;
    let orgApiContext: APIRequestContext;
    let speakerBrowserContext: BrowserContext;
    let eventController: EventController;
    let landingPage: string;

    test.beforeEach(async () => {
      await test.step("Initializing Organiser Browser and API context.", async () => {
        orgBrowserContext = await BrowserFactory.getBrowserContext({
          browserName: BrowserName.CHROME,
          laodOrganiserCookies: true,
        });
        orgApiContext = orgBrowserContext.request;
      });

      await test.step("Creating new event and Initialize eventController and enable studio-backstage.", async () => {
        eventId = await EventController.generateNewEvent(orgApiContext, {
          event_title: DataUtl.getRandomEventTitle(),
        });
        eventController = new EventController(orgApiContext, eventId);

        stageId = await eventController.getDefaultStageId();
        studioId = await eventController.enableStudioAsBackstage(stageId);
      });

      await test.step("Initialize Speaker browser contexts.", async () => {
        speakerBrowserContext = await BrowserFactory.getBrowserContext({
          browserName: BrowserName.CHROME,
          laodOrganiserCookies: false,
        });
      });

      await test.step("Enable magic link for Speaker and Get attendee landing page.", async () => {
        await updateEventLandingPageDetails(orgApiContext, eventId, {
          eventEntryType: EventEntryType.REG_BASED,
          isSpeakermagicLinkEnabled: true,
        });

        let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
        landingPage = eventInfoDto.attendeeLandingPage;
      });

      await test.step("Disabling onboarding checks for Speaker access group", async () => {
        await eventController.getAccessGroupController.handleOnboardingChecksForSpeaker(
          false
        );
      });
    });

    test.afterEach(async () => {
      await test.step("Closing browser contexts.", async () => {
        await orgBrowserContext?.close();
        await speakerBrowserContext?.close();
      });
    });

    test("TC001: Verify Organiser and Speaker flow of viewing stage as attendee.", async () => {
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-147",
      });

      let organiserPage;
      let speakerPage;
      let organiserStagePage: StagePage;
      let speakerStagePage: StagePage;

      const speakerFirstName = "AutomationSpeaker";
      const speakerLastName = "QA";

      let speakerEmail = DataUtl.getRandomSpeakerEmail();

      await test.step("Initialize Organiser and Speaker newPage.", async () => {
        organiserPage = await orgBrowserContext.newPage();
        speakerPage = await speakerBrowserContext.newPage();
      });

      await test.step("Get random Speaker email.", async () => {
        speakerEmail = DataUtl.getRandomSpeakerEmail();
      });

      await test.step(`Organiser invites Speaker.`, async () => {
        await eventController.inviteSpeakerToTheEventByApi(
          speakerEmail,
          speakerFirstName,
          speakerLastName
        );
      });

      await test.step(`Organizer joins the event and join backstage via default stage.`, async () => {
        await organiserPage.goto(landingPage);
        await organiserPage.click(`text=Enter Now`);
        organiserStagePage = new StagePage(organiserPage);
        await organiserStagePage.clickOnViewAsAttendeeOnJoinStageModal();
        organiserStagePage.verifyJoinBackstageButtonVisibility();
      });

      await test.step(`Speaker joins the event mid-session and enter backstage.`, async () => {
        let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          eventId,
          speakerEmail
        );

        await speakerPage.goto(magicLink);
        await speakerPage.click(`text=Enter Now`);
        speakerStagePage = new StagePage(speakerPage);

        await speakerStagePage.clickOnViewAsAttendeeOnJoinStageModal();
        speakerStagePage.verifyJoinBackstageButtonVisibility();
      });
    });
  }
);
