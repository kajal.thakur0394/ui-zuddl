import test, { APIRequestContext, BrowserContext } from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import BrowserName from "../../../enums/BrowserEnum";
import BrowserFactory from "../../../util/BrowserFactory";
import EventEntryType from "../../../enums/eventEntryEnum";
import { updateEventLandingPageDetails } from "../../../util/apiUtil";
import { EventController } from "../../../controller/EventController";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { StudioController } from "../../../controller/StudioController";
import { setTimeOut } from "../../../util/commonUtil";
import { RecordingController } from "../../../controller/RecordingController";

test.describe(`@studio @recording`, { tag: "@smoke" }, async () => {
  let eventId;
  let stageId;
  let studioId;
  let studioController: StudioController;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventController: EventController;
  let organiserBackstage: StudioPage;
  let defaultStageUrl: string;
  let organiserPage;

  test.beforeAll(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      browserName: BrowserName.CHROME,
      laodOrganiserCookies: true,
    });

    orgApiContext = orgBrowserContext.request;

    await test.step("Creating new event.", async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
      });
    });

    eventController = new EventController(orgApiContext, eventId);

    await test.step("Fetching deafult stage id.", async () => {
      stageId = await eventController.getDefaultStageId();
    });

    await test.step("Enabling Studio as backstage for default stage.", async () => {
      studioId = await eventController.enableStudioAsBackstage(stageId);
    });

    studioController = new StudioController(orgApiContext, studioId, stageId);

    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
      isSpeakermagicLinkEnabled: true,
    });

    defaultStageUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/stages/${stageId}`;
  });

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      browserName: BrowserName.CHROME,
      laodOrganiserCookies: true,
    });

    orgApiContext = orgBrowserContext.request;
    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });
  });

  test.afterEach(async () => {
    await test.step(`End the session.`, async () => {
      await organiserBackstage.endTheSession();
    });
    await orgBrowserContext?.close();
  });

  test("TC001: Verify that the studio recording page is rendering correctly", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-926/verify-that-the-studio-recording-page-is-rendering-correctly",
    });
    test.skip(
      process.env.run_env !== "pre-prod",
      "Skipping recording as it can only run on preprod"
    );

    let studioUrlPage;
    let organiserStagePage: StagePage;
    let recordingController: RecordingController;
    let studioBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    let studioPageForStudio: StudioPage;

    let studioPage =
      "https://studio.pre-prod.zuddl.io/r/studio/f038d145-df10-41cc-8303-3e603f746adf?s=eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJmM2EzMDM0NS03YTM3LTQyYzktYmM4NC1mNTM5ZWNlZDZkN2MiLCJzdWIiOiJhZThkZjM1Yy1iODdlLTQ0YmItYWM1Mi02ZjI1ZDU0N2U1YjUiLCJpYXQiOjE3MTE2MTQzODcsImV4cCI6MjAyNzE0NzE4N30.q3xIc8d1-VKPhJZWrLPhSdeFGrop6xJEPeut8LKO7WlBCikyZJczdwP14DlbLpNTW9LrrEs8YaYlvPLwpUm8pA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    recordingController = new RecordingController(orgApiContext, eventId);

    await test.step(`Updating eventId to static event ID for studio recording`, async () => {
      eventId =
        DataUtl.getApplicationTestDataObj()["eventIdForStudioRecordings"];
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step(`Updating event end time to end after 2 days from now so that it is outside of checkin window`, async () => {
      let eventStartTime = new Date();
      eventStartTime.setDate(eventStartTime.getDate());
      let eventEndTime = new Date();
      eventEndTime.setDate(eventEndTime.getDate() + 2);
      eventStartTime.setSeconds(0, 0);
      eventEndTime.setSeconds(0, 0);
      await eventController.updateEventEntryTime(
        eventStartTime.toISOString(),
        eventEndTime.toISOString()
      );
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step(`Organizer joins the event and enter default stage.`, async () => {
      defaultStageUrl = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/l/event/${eventId}/stages/${stageId}`;

      await organiserPage.goto(defaultStageUrl);
      organiserStagePage = new StagePage(organiserPage);
      const orgAvModal =
        await organiserStagePage.clickOnJoinBackStageButtonOnJoinStageModal();
      await orgAvModal.isVideoStreamLoadedOnAVModalSAB();
      await orgAvModal.clickOnEnterStudioOnAvModal();

      await test.step(`Verify Organiser presence in backstage.`, async () => {
        organiserBackstage = new StudioPage(organiserPage, studioController);
        await organiserBackstage.isBackstageVisible();
        await organiserBackstage.verifyCountOfStreamInBackstage(1);
      });
    });

    await test.step("Disable dry-run.", async () => {
      await studioController.disableDryRun();
    });

    await test.step(`Organizer joins the stage.`, async () => {
      await organiserBackstage.addUserToStageFromBackStage(
        organiserFirstName,
        true
      );
    });

    await test.step(`Enable the recording.`, async () => {
      await organiserBackstage.changeRecordingStatus(true);
    });

    await test.step(`Open the 'r/studio' page `, async () => {
      studioUrlPage = await studioBrowserContext.newPage();
      await studioUrlPage.goto(studioPage);
      studioPageForStudio = new StudioPage(studioUrlPage, studioController);
    });
    await test.step(`Organizer initiates the session.`, async () => {
      await organiserBackstage.startTheSession();
    });
    await test.step(`Verify recording is enabled during session.`, async () => {
      await organiserBackstage.verifyRecordingStatus(true);
    });

    await test.step(`Verify Preview container is loaded in r/studio`, async () => {
      await studioPageForStudio.verifyPresenceOfStageContainer();
    });
    // await test.step(`Verify logo to be absent in r/studio`, async () => {
    //   await studioPageForStudio.verifyAbsenceOfLogoImage();
    // });
    await test.step(`Verify Background-Image to be absent in r/studio`, async () => {
      await studioPageForStudio.verifyAbsenceOfBackgroundImage();
    });
    await test.step(`Now, organiser adds Background-Image in backstage`, async () => {
      await organiserBackstage.enableOrDisableBackgroundImage(true);
    });
    await test.step(`Verify Background-Image to be present in r/studio`, async () => {
      await studioPageForStudio.verifyPresenceOfBackgroundImage();
      await organiserBackstage.enableOrDisableBackgroundImage(false);
    });
    //await test.step(`Now, organiser adds logo in backstage`, async () => {});
    await test.step(`Verify logo to be present in r/studio`, async () => {
      await studioPageForStudio.verifyPresenceOfLogoImage();
    });
  });
});
