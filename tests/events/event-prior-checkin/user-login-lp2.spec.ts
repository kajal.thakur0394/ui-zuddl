import {
  test,
  expect,
  Page,
  BrowserContext,
  APIRequestContext,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import {
  createNewEvent,
  getEventDetails,
  updateEventEntryType,
  enableMagicLinkInEvent,
  inviteSpeakerByApi,
  inviteAttendeeByAPI,
  enableSpeakerMagicLink,
  updateLandingPageType,
} from "../../../util/apiUtil";
import { LandingPageTwo } from "../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import EventRole from "../../../enums/eventRoleEnum";
import { DataUtl } from "../../../util/dataUtil";
import { SpeakerData } from "../../../test-data/speakerData";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";
import LandingPageType from "../../../enums/landingPageEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe
  .parallel("Checking before and after event scenarios|Invite Based | Magic link Disabled | Attendee Landing Page | @New-users @inviteonly", () => {
  test.describe.configure({ retries: 2 });
  let org_session: Page;
  let org_browser_context: BrowserContext;
  let attendee_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let userDefaultRegFieldData: object;
  let attendeeEmail: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let landingPageTwo: LandingPageTwo;
  let page: Page;

  test.beforeEach(async () => {
    // setup org context
    org_browser_context = await BrowserFactory.getBrowserContext({});

    organiserApiContext = org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
  });

  test.afterEach(async () => {
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
  });

  // With/without magic link - reg/Invite for attendee/Speaker

  // With magic link reg attendee before
  test("TC001: Invited attendee try to checkin 2 Hours before event start to Reg based via magic link", async ({
    page,
  }) => {
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: 3,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    //update event entry to Reg based
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );
    eventInfoDto.landingPageId = landing_page_id;
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.REG_BASED
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;

    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    landingPageTwo = new LandingPageTwo(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    // invite this user to this event as an attendee
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // when inviting a new attendee, now registration entry will get created but no event role
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    let event_magic_link: string = await QueryUtil.fetchMagicLinkFromDB(
      event_id,
      attendeeEmail
    );
    await landingPageTwo.load(event_magic_link);
    await expect(page.locator("text=You’re slightly early")).toBeVisible();
    await expect(
      page.locator("text=The event has not started yet")
    ).toBeVisible();
  });

  // With magic link invite attendee before
  test("TC002: Invited attendee try to checkin 2 Hours before event start to invite only event via magic link", async ({
    page,
  }) => {
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: 3,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );

    eventInfoDto.landingPageId = landing_page_id;
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;

    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    landingPageTwo = new LandingPageTwo(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    // invite this user to this event as an attendee
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // when inviting a new attendee, now registration entry will get created but no event role
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    let event_magic_link: string = await QueryUtil.fetchMagicLinkFromDB(
      event_id,
      attendeeEmail
    );
    await landingPageTwo.load(event_magic_link);
    await expect(page.locator("text=You’re slightly early")).toBeVisible();
    await expect(
      page.locator("text=The event has not started yet")
    ).toBeVisible();
  });

  // Without magic link reg attendee before
  test("TC003: Attendee try to register and checkin 2 Hours before event start to registration based event", async ({
    page,
  }) => {
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: 3,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    //update event entry to reg based
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );

    eventInfoDto.landingPageId = landing_page_id;
    await updateEventEntryType(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.REG_BASED
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    landingPageTwo = new LandingPageTwo(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );

    await landingPageTwo.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageTwo.getRegistrationFormComponent()).isVisible();
    // Attendee registering for the event
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    await expect(
      page.getByText("You've successfully registered!")
    ).toBeVisible();
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    // The Already register button should be hidden.
    // await expect(
    //   (
    //     await landingPageTwo.getRegistrationFormComponent()
    //   ).alreadyRegisteredButton
    // ).toBeHidden();
    // Login optin component should not be visible
    await expect(
      landingPageTwo.loginOptionsComponent.emailInputBoxForOTP
    ).toBeHidden();
    await expect(
      landingPageTwo.loginOptionsComponent.submitButton
    ).toBeHidden();
  });

  // Without magic link invite attendee before
  test("TC004: Invited attendee try to checkin 2 Hours before event start to invite only event without magic link", async ({
    page,
  }) => {
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: 3,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );

    eventInfoDto.landingPageId = landing_page_id;
    await updateEventEntryType(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    landingPageTwo = new LandingPageTwo(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    // invite this user to this event as an attendee
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // when inviting a new attendee, now registration entry will get created but no event role
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    userEventRoleDto.hasUserRegisteredToThisEvent = true;

    await landingPageTwo.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageTwo.getRegistrationFormComponent()).isNotVisible();
    // Login optin component should not be visible
    await expect(
      landingPageTwo.loginOptionsComponent.emailInputBoxForOTP
    ).toBeHidden();
    await expect(
      landingPageTwo.loginOptionsComponent.submitButton
    ).toBeHidden();
  });

  // With magic link reg attendee after
  test("TC005: Invited attendee try to checkin 2 Hours after event is finished to reg based event via magic link", async ({
    page,
  }) => {
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: -5,
      add_end_date_from_today: 0,
      add_hours_to_end_date: -4,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    //update event entry to reg based
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );

    eventInfoDto.landingPageId = landing_page_id;
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.REG_BASED
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;

    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    landingPageTwo = new LandingPageTwo(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    // invite this user to this event as an attendee
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // when inviting a new attendee, now registration entry will get created but no event role
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    userEventRoleDto.hasUserRegisteredToThisEvent = true;

    let event_magic_link: string = await QueryUtil.fetchMagicLinkFromDB(
      event_id,
      attendeeEmail
    );
    await landingPageTwo.load(event_magic_link);
    // since the event is invite only reg form should not be visible
    await (await landingPageTwo.getRegistrationFormComponent()).isNotVisible();
    await (await landingPageTwo.getRegistrationFormComponent()).isNotVisible();
    // Login optin component should not be visible as user should be signed in
    await expect(
      landingPageTwo.loginOptionsComponent.emailInputBoxForOTP
    ).toBeHidden();
    await expect(
      landingPageTwo.loginOptionsComponent.submitButton
    ).toBeHidden();
    await landingPageTwo.restrictedEventScreenComponent.isEventEndedScreenisVisible();
  });

  // With magic link invite attendee after
  test("TC006: Invited attendee try to checkin 2 Hours after event is finished to invite only event via magic link", async ({
    page,
  }) => {
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: -5,
      add_end_date_from_today: 0,
      add_hours_to_end_date: -4,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );

    eventInfoDto.landingPageId = landing_page_id;
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;

    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    landingPageTwo = new LandingPageTwo(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    // invite this user to this event as an attendee
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // when inviting a new attendee, now registration entry will get created but no event role
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    userEventRoleDto.hasUserRegisteredToThisEvent = true;

    let event_magic_link: string = await QueryUtil.fetchMagicLinkFromDB(
      event_id,
      attendeeEmail
    );
    await landingPageTwo.load(event_magic_link);
    // since the event is invite only reg form should not be visible
    await (await landingPageTwo.getRegistrationFormComponent()).isNotVisible();
    await (await landingPageTwo.getRegistrationFormComponent()).isNotVisible();
    // Login optin component should not be visible as user should be signed in
    await expect(
      landingPageTwo.loginOptionsComponent.emailInputBoxForOTP
    ).toBeHidden();
    await expect(
      landingPageTwo.loginOptionsComponent.submitButton
    ).toBeHidden();
    await landingPageTwo.restrictedEventScreenComponent.isEventEndedScreenisVisible();
  });

  // Without magic link reg attendee after
  test("TC007: Attendee try to register and checkin 2 Hours after event is finished to registration based event", async ({
    page,
  }) => {
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: -5,
      add_end_date_from_today: 0,
      add_hours_to_end_date: -4,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    //update event entry to reg based
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );

    eventInfoDto.landingPageId = landing_page_id;
    await updateEventEntryType(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.REG_BASED
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    landingPageTwo = new LandingPageTwo(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    userEventRoleDto.hasUserRegisteredToThisEvent = true;

    await landingPageTwo.load(attendee_landing_page);
    // since the event is finished reg form should not be visible
    await (await landingPageTwo.getRegistrationFormComponent()).isNotVisible();
    // Login optin component should not be visible
    await expect(
      landingPageTwo.loginOptionsComponent.emailInputBoxForOTP
    ).toBeHidden();
    await expect(
      landingPageTwo.loginOptionsComponent.submitButton
    ).toBeHidden();
    await landingPageTwo.restrictedEventScreenComponent.isEventEndedScreenisVisible();
  });

  // Without magic link invite attendee after
  test("TC008: Invited attendee try to checkin 2 Hours after event is finished to invite only event", async ({
    page,
  }) => {
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: -5,
      add_end_date_from_today: 0,
      add_hours_to_end_date: -4,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );

    eventInfoDto.landingPageId = landing_page_id;
    await updateEventEntryType(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    landingPageTwo = new LandingPageTwo(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    // invite this user to this event as an attendee
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // when inviting a new attendee, now registration entry will get created but no event role
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    userEventRoleDto.hasUserRegisteredToThisEvent = true;

    await landingPageTwo.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageTwo.getRegistrationFormComponent()).isNotVisible();
    // Login optin component should not be visible
    await expect(
      landingPageTwo.loginOptionsComponent.emailInputBoxForOTP
    ).toBeHidden();
    await expect(
      landingPageTwo.loginOptionsComponent.submitButton
    ).toBeHidden();
    await landingPageTwo.restrictedEventScreenComponent.isEventEndedScreenisVisible();
  });

  // With magic link reg speaker before
  test("TC009: Invited Speaker try to checkin 2 Hours before event start to reg based event via magic link", async ({
    page,
  }) => {
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: 3,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );

    eventInfoDto.landingPageId = landing_page_id;
    await enableSpeakerMagicLink(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.REG_BASED
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    let landingPageTwo = new LandingPageTwo(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      true
    ); // true denote to send email

    let event_magic_link: string =
      await QueryUtil.fetchMagicLinkForSpeakerFromDB(event_id, speakerEmail);
    await landingPageTwo.load(event_magic_link);
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
  });

  // With magic link invite speaker before
  test("TC010: Invited Speaker try to checkin 2 Hours before event start to Invite only event via magic link", async ({
    page,
  }) => {
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: 3,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );

    eventInfoDto.landingPageId = landing_page_id;
    await enableSpeakerMagicLink(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    let landingPageTwo = new LandingPageTwo(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      true
    ); // true denote to send email
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER
    );
    let event_magic_link: string =
      await QueryUtil.fetchMagicLinkForSpeakerFromDB(event_id, speakerEmail);
    await landingPageTwo.load(event_magic_link);
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
  });

  // Without magic link reg speaker before
  test("TC011: Invited Speaker try to checkin 2 Hours before event start to Reg based event", async ({
    page,
  }) => {
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: 3,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );

    eventInfoDto.landingPageId = landing_page_id;
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.REG_BASED
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    let landingPageTwo = new LandingPageTwo(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    ); // true denote to send email
    await landingPageTwo.load(speaker_landing_page);
    await landingPageTwo.clickOnLoginButton();
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER
    );
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
  });

  // Without magic link invite speaker before
  test("TC012: Invited Speaker try to checkin 2 Hours before event start to invite only event", async ({
    page,
  }) => {
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: 3,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );

    eventInfoDto.landingPageId = landing_page_id;
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    let landingPageTwo = new LandingPageTwo(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    ); // true denote to send email
    await landingPageTwo.load(speaker_landing_page);
    await landingPageTwo.clickOnLoginButton();
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER
    );
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
  });

  // With magic link reg speaker after
  test("TC013: Invited Speaker try to checkin 3 Hours after event start to Reg based event with magic link", async ({
    page,
  }) => {
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: -6,
      add_end_date_from_today: 0,
      add_hours_to_end_date: -3,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );

    eventInfoDto.landingPageId = landing_page_id;
    await enableSpeakerMagicLink(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.REG_BASED
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    let landingPageTwo = new LandingPageTwo(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      true
    ); // true denote to send email

    let event_magic_link: string =
      await QueryUtil.fetchMagicLinkForSpeakerFromDB(event_id, speakerEmail);
    await landingPageTwo.load(event_magic_link);
    await (
      await landingPageTwo.getRestrcitedEventScreen()
    ).isEventEndedScreenisVisible();
  });

  // With magic link invite speaker after
  test("TC014: Invited Speaker try to checkin 3 Hours after event start to Invite based event with magic", async ({
    page,
  }) => {
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: -6,
      add_end_date_from_today: 0,
      add_hours_to_end_date: -3,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );

    eventInfoDto.landingPageId = landing_page_id;
    await enableSpeakerMagicLink(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    let landingPageTwo = new LandingPageTwo(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      true
    ); // true denote to send email

    let event_magic_link: string =
      await QueryUtil.fetchMagicLinkForSpeakerFromDB(event_id, speakerEmail);
    await landingPageTwo.load(event_magic_link);
    await (
      await landingPageTwo.getRestrcitedEventScreen()
    ).isEventEndedScreenisVisible();
  });

  // Without magic link reg speaker after
  test("TC015: Invited Speaker try to checkin 3 Hours after event start to Reg based event without magic link", async ({
    page,
  }) => {
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: -6,
      add_end_date_from_today: 0,
      add_hours_to_end_date: -3,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );

    eventInfoDto.landingPageId = landing_page_id;
    await updateEventEntryType(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.REG_BASED
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    let landingPageTwo = new LandingPageTwo(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    ); // true denote to send email
    await landingPageTwo.load(speaker_landing_page);
    await (
      await landingPageTwo.getRestrcitedEventScreen()
    ).isEventEndedScreenisVisible();
  });

  // Without magic link invite speaker after
  test("TC016: Invited Speaker try to checkin 3 Hours after event start to invite only event without magic link", async ({
    page,
  }) => {
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: -6,
      add_end_date_from_today: 0,
      add_hours_to_end_date: -3,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );

    eventInfoDto.landingPageId = landing_page_id;
    await updateEventEntryType(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    let landingPageTwo = new LandingPageTwo(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    ); // true denote to send email
    await landingPageTwo.load(speaker_landing_page);
    await (
      await landingPageTwo.getRestrcitedEventScreen()
    ).isEventEndedScreenisVisible();
  });

  test("TC017: Organiser try to checkin 2 Hours before event start to invite only event on speaker landing page", async ({
    page,
  }) => {
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: 3,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );

    eventInfoDto.landingPageId = landing_page_id;
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    let landingPageTwo = new LandingPageTwo(page);
    await landingPageTwo.load(speaker_landing_page);
    await landingPageTwo.clickOnLoginButton();
    let orgEmail: string =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserEmail"];
    let orgPassword: string =
      DataUtl.getApplicationTestDataObj()["organiserPassword"];
    userInfoDto = new UserInfoDTO(
      orgEmail,
      orgPassword,
      "prateek",
      "automation",
      true
    );
    userInfoDto.hasSetPassword = true;
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ORGANISER
    );
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
  });

  // With/without magic link - reg/Invite for attendee/Speaker
});
