import {
  test,
  Page,
  BrowserContext,
  APIRequestContext,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { fetchAttendeeInviteMagicLink } from "../../../util/emailUtil";
import {
  createNewEvent,
  getEventDetails,
  enableMagicLinkInEvent,
  inviteAttendeeByAPI,
} from "../../../util/apiUtil";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import createEventPayloadData from "../../../util/create_event_payload.json";
import EventRole from "../../../enums/eventRoleEnum";
import { DataUtl } from "../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";
import {
  validateAddToCalenderLink,
  validateFutureIcsData,
} from "../../../util/validation-util";
import { deleteFile, parseICS } from "../../../util/commonUtil";

test.describe.parallel("Validating Add to Calendar in future events", () => {
  test.describe.configure({ retries: 2 });
  let org_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let attendeeEmail: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let landingPageOne: LandingPageOne;
  let page: Page;
  let appleInviteFileName;
  let outlookInviteFileName;
  let googleCalendarLink;
  let yahooCalendarLink;

  test.beforeEach(async () => {
    // setup org context
    org_browser_context = await BrowserFactory.getBrowserContext({});
    organiserApiContext = org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
  });

  test.afterEach(async () => {
    deleteFile(appleInviteFileName);
    deleteFile(outlookInviteFileName);
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
  });

  test("TC001: LP1 Validating add to calendar links in invited attendee try to checkin 2 Hours before event start to invite only event via magic link.", async ({
    page,
  }) => {
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: 3,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;

    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    landingPageOne = new LandingPageOne(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    // invite this user to this event as an attendee
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // when inviting a new attendee, now registration entry will get created but no event role
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    let event_magic_link: string = await fetchAttendeeInviteMagicLink(
      attendeeEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    await landingPageOne.load(event_magic_link);
    let calendarLinks = await (
      await landingPageOne.getAddToCalendarComponenet()
    ).fetchCalendarLinksFromAddToCalendarButton();
    appleInviteFileName = calendarLinks[0];
    outlookInviteFileName = calendarLinks[1];
    googleCalendarLink = calendarLinks[2];
    yahooCalendarLink = calendarLinks[3];
    eventInfoDto.startDateTime = createEventPayloadData.startDateTime;
    await test.step("Verifying Outlook ICS files link and text from email recieved after registering", async () => {
      await validateFutureIcsData(
        parseICS(outlookInviteFileName),
        eventInfoDto
      );
    });
    await test.step("Verifying Apple ICS files link and text from email recieved after registering", async () => {
      await validateFutureIcsData(parseICS(appleInviteFileName), eventInfoDto);
    });
    await test.step("Verifying Google invite link", async () => {
      await validateAddToCalenderLink(googleCalendarLink, event_id);
    });
    await test.step("Verifying Yahoo invite link", async () => {
      await validateAddToCalenderLink(yahooCalendarLink, event_id);
    });
  });
});
