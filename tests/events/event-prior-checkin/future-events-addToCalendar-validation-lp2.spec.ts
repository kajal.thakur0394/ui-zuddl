import {
  test,
  Page,
  BrowserContext,
  APIRequestContext,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { fetchAttendeeInviteMagicLink } from "../../../util/emailUtil";
import {
  createNewEvent,
  getEventDetails,
  inviteAttendeeByAPI,
  updateLandingPageType,
} from "../../../util/apiUtil";
import { LandingPageTwo } from "../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import createEventPayloadData from "../../../util/create_event_payload.json";
import EventRole from "../../../enums/eventRoleEnum";
import { DataUtl } from "../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";
import {
  validateAddToCalenderLink,
  validateFutureIcsData,
} from "../../../util/validation-util";
import { deleteFile, parseICS } from "../../../util/commonUtil";
import LandingPageType from "../../../enums/landingPageEnum";

test.describe
  .parallel("Validating Add to Calendar in future events @landing-page-2", () => {
  test.describe.configure({ retries: 2 });
  let org_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let attendeeEmail: string;
  let userEventRoleDto: UserEventRoleDto;
  let landingPageTwo: LandingPageTwo;
  let page: Page;
  let appleInviteFileName;
  let outlookInviteFileName;
  let googleCalendarLink;
  let yahooCalendarLink;
  let landingPageId;

  test.beforeEach(async () => {
    // setup org context
    org_browser_context = await BrowserFactory.getBrowserContext({});
    organiserApiContext = org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";

    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: false,
      add_start_date_from_today: 0,
      add_hours_to_start_date: 3,
    });
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    console.log("event_details_api_resp :", event_details_api_resp);
    landingPageId = event_details_api_resp.eventLandingPageId;
    // Updating event landing page type and entry type to invite only
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landingPageId,
      LandingPageType.STYLE2,
      "true",
      EventEntryType.INVITE_ONLY
    );
  });

  test.afterEach(async () => {
    deleteFile(appleInviteFileName);
    deleteFile(outlookInviteFileName);
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
  });

  test("TC001: Validating add to calendar links in invited attendee try to checkin 2 Hours before event start to invite only event via magic link. @landing-page-2", async ({
    page,
  }) => {
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link disabled
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;

    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    landingPageTwo = new LandingPageTwo(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    // invite this user to this event as an attendee
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // when inviting a new attendee, now registration entry will get created but no event role
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    let event_magic_link: string = await fetchAttendeeInviteMagicLink(
      attendeeEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    await landingPageTwo.load(event_magic_link);
    await page.waitForTimeout(5000); // waiting for 5 sec to component to load
    let calendarLinks = await (
      await landingPageTwo.getAddToCalendarComponenet()
    ).fetchCalendarLinksFromAddToCalendarButton();

    appleInviteFileName = calendarLinks[0];
    outlookInviteFileName = calendarLinks[1];
    googleCalendarLink = calendarLinks[2];
    yahooCalendarLink = calendarLinks[3];
    eventInfoDto.startDateTime = createEventPayloadData.startDateTime;
    await test.step("Verifying Outlook ICS files link and text from email recieved after registering", async () => {
      await validateFutureIcsData(
        parseICS(outlookInviteFileName),
        eventInfoDto
      );
    });
    await test.step("Verifying Apple ICS files link and text from email recieved after registering", async () => {
      await validateFutureIcsData(parseICS(appleInviteFileName), eventInfoDto);
    });
    await test.step("Verifying Google invite link", async () => {
      await validateAddToCalenderLink(googleCalendarLink, event_id);
    });
    await test.step("Verifying Yahoo invite link", async () => {
      await validateAddToCalenderLink(yahooCalendarLink, event_id);
    });
  });
});
