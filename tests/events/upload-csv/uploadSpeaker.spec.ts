import {
  test,
  BrowserContext,
  APIRequestContext,
  Page,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import {
  getEventDetails,
  createNewEvent,
  updateEventEntryType,
  addSpeakerRecordInCsv,
  processSpeakerCsvOnEndPoint,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import { DataUtl } from "../../../util/dataUtil";
import { checkIfFileExists } from "../../../util/commonUtil";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventRole from "../../../enums/eventRoleEnum";
import EventEntryType from "../../../enums/eventEntryEnum";
import { getInviteLinkFromSpeakerInviteEmail } from "../../../util/emailUtil";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";

test.describe.parallel("Speaker Added by CSV", () => {
  let org_browser_context: BrowserContext;
  let organiserApiContext: APIRequestContext;
  let eventTitle: string;
  let eventId;
  let csvFileToAddDataIn: string; //filename
  let eventInfoDto: EventInfoDTO;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let landingPageId;
  let page: Page;

  test.beforeEach(async ({ context }) => {
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    organiserApiContext = await org_browser_context.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title: eventTitle,
    });
    csvFileToAddDataIn = DataUtl.getRandomCsvName("speaker");
    // adding event dto
    eventInfoDto = new EventInfoDTO(eventId, eventTitle, false); // true to denote magic link enabled
    eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, eventId)
    ).json();
    landingPageId = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landingPageId;
    // Updating event dto
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;

    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      eventId,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    page = await context.newPage();
  });

  test.afterEach(async () => {
    const fs = require("fs");
    if (checkIfFileExists(csvFileToAddDataIn)) {
      console.log(`file exists`);
      fs.unlink(csvFileToAddDataIn, (err) => {
        if (err) {
          console.error(`error ${err} occurred when deleting the file`);
        }
      });
    } else {
      console.log("file does not exists");
    }
    await org_browser_context?.close();
  });

  test("TC001 Added Speaker by csv is able to enter Registration Based event using otp", async () => {
    await updateEventLandingPageDetails(organiserApiContext, eventId, {
      isSpeakermagicLinkEnabled: false,
      speakerAuthOptions: ["EMAIL_OTP"],
    });

    let speakerEmail = DataUtl.getRandomSpeakerEmail();
    await addSpeakerRecordInCsv(csvFileToAddDataIn, speakerEmail);
    await processSpeakerCsvOnEndPoint(
      organiserApiContext,
      eventId.toString(),
      csvFileToAddDataIn
    );
    let userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    let userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER,
      true
    );
    let landingPageOne = new LandingPageOne(page);
    let inviteLink = await getInviteLinkFromSpeakerInviteEmail(
      speakerEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    await landingPageOne.load(inviteLink);
    await (
      await landingPageOne.getSpeakerLandingComponent()
    ).clickOnLoginButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isStageLoaded();
  });

  test("TC002 Added Speaker by csv is able to enter Registration Based event using password", async () => {
    await updateEventLandingPageDetails(organiserApiContext, eventId, {
      speakerAuthOptions: ["EMAIL"],
      isSpeakermagicLinkEnabled: false,
    });

    let speakerEmail = DataUtl.getRandomSpeakerEmail();
    await addSpeakerRecordInCsv(csvFileToAddDataIn, speakerEmail);
    await processSpeakerCsvOnEndPoint(
      organiserApiContext,
      eventId.toString(),
      csvFileToAddDataIn
    );
    let userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    let userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER,
      true
    );
    let landingPageOne = new LandingPageOne(page);
    let inviteLink = await getInviteLinkFromSpeakerInviteEmail(
      speakerEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    await landingPageOne.load(inviteLink);
    await (
      await landingPageOne.getSpeakerLandingComponent()
    ).clickOnLoginButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto, "speakerLandingPage", true);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isStageLoaded();
  });

  test("TC003 Added speaker by csv is able to enter Invite Based event using otp", async () => {
    await updateEventLandingPageDetails(organiserApiContext, eventId, {
      eventEntryType: EventEntryType.INVITE_ONLY,
      speakerAuthOptions: ["EMAIL_OTP"],
      isSpeakermagicLinkEnabled: false,
    });
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    let speakerEmail = DataUtl.getRandomSpeakerEmail();
    await addSpeakerRecordInCsv(csvFileToAddDataIn, speakerEmail);
    await processSpeakerCsvOnEndPoint(
      organiserApiContext,
      eventId.toString(),
      csvFileToAddDataIn
    );
    let userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    let userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER,
      true
    );
    let landingPageOne = new LandingPageOne(page);
    let inviteLink = await getInviteLinkFromSpeakerInviteEmail(
      speakerEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    await landingPageOne.load(inviteLink);
    await (
      await landingPageOne.getSpeakerLandingComponent()
    ).clickOnLoginButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isStageLoaded();
  });

  test("TC004 Added Speaker by csv is able to enter Registration Based event using otp", async () => {
    let speakerEmail = DataUtl.getRandomSpeakerEmail();
    await addSpeakerRecordInCsv(csvFileToAddDataIn, speakerEmail);
    await processSpeakerCsvOnEndPoint(
      organiserApiContext,
      eventId.toString(),
      csvFileToAddDataIn
    );
    let userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    let userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER,
      true
    );
    let landingPageOne = new LandingPageOne(page);
    let inviteLink = await getInviteLinkFromSpeakerInviteEmail(
      speakerEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    await landingPageOne.load(inviteLink);
    await landingPageOne.isStageLoaded();
  });
});
