import test, {
  APIRequestContext,
  BrowserContext,
  expect,
  Page,
} from "@playwright/test";
import testData from "../../../test-data/conditional-fields-testdata/createConditionalFields.json";
import { CsvApiController } from "../../../util/csv-api-controller";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import BrowserFactory from "../../../util/BrowserFactory";
import {
  createNewEvent,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import { EventController } from "../../../controller/EventController";
import EventEntryType from "../../../enums/eventEntryEnum";
import testDataForCustomFieldsUploadCsv from "../../../test-data/uploadcsv-customfields-testdata/customFieldCsvTestData.json";
import { validateRegisteredUserCustomFieldDataWithDBData } from "../../../util/validationHelper";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe("@conditionalFields @attendee-csv", async () => {
  let org_browser_context: BrowserContext;
  let organiserApiContext: APIRequestContext;
  let event_title: string;
  let eventId;
  let destinationCsvPath: string;

  test.beforeEach(async () => {
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    organiserApiContext = await org_browser_context.request;
    event_title = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
    });

    await updateEventLandingPageDetails(organiserApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
    });
    //path where the destination csv will be generated
    destinationCsvPath =
      "test-data/uploadcsv-customfields-testdata/destinationCsv.csv";
  });

  test.afterEach(async () => {
    await org_browser_context?.close();
  });

  test("Sceanrio : Event with only default fields: Adding attendee csv with all defaults field filled", async () => {
    let csvApiController = new CsvApiController(organiserApiContext);
    //test data to add in csv
    const attendeeDataRecordToAddInCsv =
      testDataForCustomFieldsUploadCsv["event-with-default-fields"]
        .testDataValuesForCsv.tc_all_fields_filled;
    await csvApiController.createCsvWithCustomFields(
      destinationCsvPath,
      attendeeDataRecordToAddInCsv
    );
    let uploadedFileNameOnS3 = await csvApiController.getSignedS3Url(
      destinationCsvPath
    );
    //mapping object
    const csvMappingObjectForMappingEndpoint =
      testDataForCustomFieldsUploadCsv["event-with-default-fields"]
        .csvFieldsMappingObjectForMappingReq;

    await csvApiController.processAttendeeCsvOnEndPoint(
      eventId.toString(),
      uploadedFileNameOnS3,
      csvMappingObjectForMappingEndpoint,
      testDataForCustomFieldsUploadCsv["event-with-default-fields"][
        "listOfCsvFieldMapping"
      ]
    );

    const csvProcessingResp =
      await csvApiController.waitForCsvFileProcessingToComplete(
        eventId.toString(),
        uploadedFileNameOnS3
      );
    const csvProcessingRespJson = await csvProcessingResp.json();
    expect(
      csvProcessingRespJson.succeedRecordsCount,
      "expecting success record count to be 1"
    ).toBe(1);
  });

  const list_of_test_case_scenarios = [
    "text-conditional-fields-data",
    "numeric-conditional-fields-data",
    "dropdown-conditional-fields-data",
    "multi-select-conditional-fields-data",
  ];

  list_of_test_case_scenarios.forEach((scenarioKey) => {
    test(`Scenario : ${scenarioKey}: Uploading attendee csv with all default fields + mandatory fields filled`, async () => {
      let eventController = new EventController(organiserApiContext, eventId);
      let csvApiController = new CsvApiController(organiserApiContext);
      let csvProcessingResponse;
      const testDataCustomFields = testData[scenarioKey];

      await test.step(`Adding custom fields to event ${eventId} by API `, async () => {
        await eventController.addCustomFieldsWithConditionToEvent(
          testDataCustomFields
        );
        let attendeeLandingPage = new EventInfoDTO(eventId, event_title)
          .attendeeLandingPage;
        console.log(
          `attendee landing page for this event is ${attendeeLandingPage}`
        );
      });

      //test data to add in csv
      const attendeeDataRecordToAddInCsv =
        testDataForCustomFieldsUploadCsv[scenarioKey]["testDataValuesForCsv"][
          "tc_all_fields_filled"
        ];
      await test.step(`Generating csv at ${destinationCsvPath} with headers contain only default fields`, async () => {
        await csvApiController.createCsvWithCustomFields(
          destinationCsvPath,
          attendeeDataRecordToAddInCsv
        );
      });

      await test.step(`Uploading the csv and waiting for its processing to get completed`, async () => {
        let uploadedFileNameOnS3 = await csvApiController.getSignedS3Url(
          destinationCsvPath
        );

        //mapping object
        const csvMappingObjectForMappingEndpoint =
          testDataForCustomFieldsUploadCsv[scenarioKey][
            "csvFieldsMappingObjectForMappingReq"
          ];

        //list of csv field mapping for upload csv v2 endpoint
        const listOfCsvFieldMappingForUploadEndpoint =
          testDataForCustomFieldsUploadCsv[scenarioKey][
            "listOfCsvFieldMapping"
          ];

        await csvApiController.processAttendeeCsvOnEndPoint(
          eventId.toString(),
          uploadedFileNameOnS3,
          csvMappingObjectForMappingEndpoint,
          listOfCsvFieldMappingForUploadEndpoint
        );
        csvProcessingResponse =
          await csvApiController.waitForCsvFileProcessingToComplete(
            eventId.toString(),
            uploadedFileNameOnS3
          );
      });

      await test.step(`Expecting success record count to be 1 and failure record count to be 0`, async () => {
        const csvProcessingRespJson = await csvProcessingResponse.json();
        expect(
          csvProcessingRespJson.succeedRecordsCount,
          "expecting success record count to be 1"
        ).toBe(1);
      });

      await test.step("Fetch Registered user custom fields data from the DB", async () => {
        const fetchedCustomFieldsDataFromDb =
          await QueryUtil.fetchCustomFieldsFilledByRegisteredUser(
            eventId,
            "anyrandomemail@testjoyn.com"
          );
        console.log(
          `registered user fetched custom fields data is ${fetchedCustomFieldsDataFromDb}`
        );
        expect(
          validateRegisteredUserCustomFieldDataWithDBData(
            attendeeDataRecordToAddInCsv,
            fetchedCustomFieldsDataFromDb
          ),
          "comparing user custom field test data with DB Data"
        ).toBeTruthy();
      });
    });

    test(`Scenario : ${scenarioKey}: Uploading attendee csv when filling only all default fields and empty conditional fields`, async () => {
      let eventController = new EventController(organiserApiContext, eventId);
      let csvApiController = new CsvApiController(organiserApiContext);
      let destinationCsvPath =
        "test-data/uploadcsv-customfields-testdata/destinationCsv.csv";
      let csvProcessingResponse;
      const testDataCustomFields = testData[scenarioKey];

      await test.step(`Adding custom fields to event ${eventId} by API `, async () => {
        await eventController.addCustomFieldsWithConditionToEvent(
          testDataCustomFields
        );
        let attendeeLandingPage = new EventInfoDTO(eventId, event_title)
          .attendeeLandingPage;
        console.log(
          `attendee landing page for this event is ${attendeeLandingPage}`
        );
      });

      //test data to add in csv
      const attendeeDataRecordToAddInCsv =
        testDataForCustomFieldsUploadCsv[scenarioKey]["testDataValuesForCsv"][
          "tc_default_fields_filled"
        ];

      await test.step(`Generating csv at ${destinationCsvPath} with headers contain only default fields`, async () => {
        await csvApiController.createCsvWithCustomFields(
          destinationCsvPath,
          attendeeDataRecordToAddInCsv
        );
      });

      await test.step(`Uploading the csv and waiting for its processing to get completed`, async () => {
        let uploadedFileNameOnS3 = await csvApiController.getSignedS3Url(
          destinationCsvPath
        );

        //mapping object
        const csvMappingObjectForMappingEndpoint =
          testDataForCustomFieldsUploadCsv[scenarioKey][
            "csvFieldsMappingObjectForMappingReq"
          ];

        await csvApiController.processAttendeeCsvOnEndPoint(
          eventId.toString(),
          uploadedFileNameOnS3,
          csvMappingObjectForMappingEndpoint,
          testDataForCustomFieldsUploadCsv[scenarioKey]["listOfCsvFieldMapping"]
        );
        csvProcessingResponse =
          await csvApiController.waitForCsvFileProcessingToComplete(
            eventId.toString(),
            uploadedFileNameOnS3
          );
      });

      await test.step(`Expecting success record count to be 1 and failure record count to be 0`, async () => {
        const csvProcessingRespJson = await csvProcessingResponse.json();
        expect(
          csvProcessingRespJson.succeedRecordsCount,
          "expecting success record count to be 1"
        ).toBe(1);
      });
    });

    test(`Scenario : ${scenarioKey}: Uploading attendee, skipping filling 1 mandatory default field`, async () => {
      /**
       * In this case,
       * we will add 1 csv record and keep 1 column empty which is mandatory and a default field
       * Hence, when csv is being processed, there should be an error
       * hence failure record count to be 1
       * Logurl should tell the error to be [Line:2, Col:firstName] null - null value encountered
       */
      let eventController = new EventController(organiserApiContext, eventId);
      let csvApiController = new CsvApiController(organiserApiContext);
      let destinationCsvPath =
        "test-data/uploadcsv-customfields-testdata/destinationCsv.csv";
      let csvProcessingResponse;
      const testDataCustomFields = testData[scenarioKey];

      await test.step(`Adding custom fields to event ${eventId} by API `, async () => {
        await eventController.addCustomFieldsWithConditionToEvent(
          testDataCustomFields
        );
        let attendeeLandingPage = new EventInfoDTO(eventId, event_title)
          .attendeeLandingPage;
        console.log(
          `attendee landing page for this event is ${attendeeLandingPage}`
        );
      });

      //test data to add in csv
      const attendeeDataRecordToAddInCsv =
        testDataForCustomFieldsUploadCsv[scenarioKey]["testDataValuesForCsv"][
          "tc_default_mandatory_field_unfilled"
        ];
      await test.step(`Generating csv at ${destinationCsvPath} with headers contain only default fields`, async () => {
        await csvApiController.createCsvWithCustomFields(
          destinationCsvPath,
          attendeeDataRecordToAddInCsv
        );
      });

      await test.step(`Uploading the csv and waiting for its processing to get completed`, async () => {
        let uploadedFileNameOnS3 = await csvApiController.getSignedS3Url(
          destinationCsvPath
        );

        //mapping object
        const csvMappingObjectForMappingEndpoint =
          testDataForCustomFieldsUploadCsv[scenarioKey][
            "csvFieldsMappingObjectForMappingReq"
          ];

        await csvApiController.processAttendeeCsvOnEndPoint(
          eventId.toString(),
          uploadedFileNameOnS3,
          csvMappingObjectForMappingEndpoint,
          testDataForCustomFieldsUploadCsv[scenarioKey]["listOfCsvFieldMapping"]
        );
        csvProcessingResponse =
          await csvApiController.waitForCsvFileProcessingToComplete(
            eventId.toString(),
            uploadedFileNameOnS3
          );
      });

      await test.step(`Expecting failure record count to be 1 and error log to contain expected message`, async () => {
        const csvProcessingRespJson = await csvProcessingResponse.json();
        expect(
          csvProcessingRespJson.failedRecordsCount,
          "expecting success record count to be 1"
        ).toBe(1);
        let logUrl = csvProcessingRespJson.logUrl;
        expect(logUrl, `expecting logurl to not be null`).not.toBeNull();
        console.log(`log url is ${logUrl}`);
        const errorMessage = await (
          await organiserApiContext.get(logUrl)
        ).text();
        expect(
          errorMessage,
          `expecting logurl to contain error message  ${errorMessage}`
        ).toContain(`[Line:2, Col:firstName] null - null value encountered `);
      });
    });

    test(`Scenario : ${scenarioKey}: Uploading attendee, skipping filling 1 mandatory conditional field`, async () => {
      /**
       * In this case,
       * we will add 1 csv record and keep 1 column empty which is mandatory but a conditional custom field
       * Hence, when csv is being processed, there should not be any error if other fields are filled
       * hence success record count to be 1
       */
      let eventController = new EventController(organiserApiContext, eventId);
      let csvApiController = new CsvApiController(organiserApiContext);
      let destinationCsvPath =
        "test-data/uploadcsv-customfields-testdata/destinationCsv.csv";
      let csvProcessingResponse;
      const testDataCustomFields = testData[scenarioKey];

      await test.step(`Adding custom fields to event ${eventId} by API `, async () => {
        await eventController.addCustomFieldsWithConditionToEvent(
          testDataCustomFields
        );
        let attendeeLandingPage = new EventInfoDTO(eventId, event_title)
          .attendeeLandingPage;
        console.log(
          `attendee landing page for this event is ${attendeeLandingPage}`
        );
      });

      //test data to add in csv
      const attendeeDataRecordToAddInCsv =
        testDataForCustomFieldsUploadCsv[scenarioKey]["testDataValuesForCsv"][
          "tc_conditional_mandatory_field_unfilled"
        ];
      await test.step(`Generating csv at ${destinationCsvPath} with headers contain only default fields`, async () => {
        await csvApiController.createCsvWithCustomFields(
          destinationCsvPath,
          attendeeDataRecordToAddInCsv
        );
      });

      await test.step(`Uploading the csv and waiting for its processing to get completed`, async () => {
        let uploadedFileNameOnS3 = await csvApiController.getSignedS3Url(
          destinationCsvPath
        );

        //mapping object
        const csvMappingObjectForMappingEndpoint =
          testDataForCustomFieldsUploadCsv[scenarioKey][
            "csvFieldsMappingObjectForMappingReq"
          ];

        await csvApiController.processAttendeeCsvOnEndPoint(
          eventId.toString(),
          uploadedFileNameOnS3,
          csvMappingObjectForMappingEndpoint,
          testDataForCustomFieldsUploadCsv[scenarioKey]["listOfCsvFieldMapping"]
        );
        csvProcessingResponse =
          await csvApiController.waitForCsvFileProcessingToComplete(
            eventId.toString(),
            uploadedFileNameOnS3
          );
      });

      await test.step(`Expecting success record count to be 1 and failure record count to be 0`, async () => {
        const csvProcessingRespJson = await csvProcessingResponse.json();
        expect(
          csvProcessingRespJson.succeedRecordsCount,
          "expecting success record count to be 1"
        ).toBe(1);
      });
    });
  });
});
