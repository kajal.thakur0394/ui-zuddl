import {
  test,
  BrowserContext,
  APIRequestContext,
  Page,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import {
  addAttendeeRecordInCsv,
  getEventDetails,
  createNewEvent,
  updateEventEntryType,
  updateEventLandingPageDetails,
  cancelEvent,
} from "../../../util/apiUtil";
import { DataUtl } from "../../../util/dataUtil";
import { checkIfFileExists } from "../../../util/commonUtil";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventRole from "../../../enums/eventRoleEnum";
import EventEntryType from "../../../enums/eventEntryEnum";
import { fetchAttendeeInviteMagicLink } from "../../../util/emailUtil";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";
import { CsvApiController } from "../../../util/csv-api-controller";

test.describe.parallel("Attendee Added by CSV", () => {
  test.describe.configure({ retries: 2 });
  let org_browser_context: BrowserContext;
  let organiserApiContext: APIRequestContext;
  let event_title: string;
  let eventId;
  let csvFileToAddDataIn: string; //filename
  let eventInfoDto: EventInfoDTO;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let landingPageId;
  let page: Page;
  let csvApiController: CsvApiController;

  test.beforeEach(async () => {
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    organiserApiContext = await org_browser_context.request;
    event_title = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
    });
    csvFileToAddDataIn = DataUtl.getRandomCsvName();
    // adding event dto
    eventInfoDto = new EventInfoDTO(eventId, event_title, false); // true to denote magic link enabled
    eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, eventId)
    ).json();
    landingPageId = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landingPageId;
    // Updating event dto
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;

    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      eventId,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    //csv api controller
    csvApiController = new CsvApiController(organiserApiContext);
  });

  test.afterEach(async () => {
    const fs = require("fs");
    if (checkIfFileExists(csvFileToAddDataIn)) {
      console.log(`file exists`);
      fs.unlink(csvFileToAddDataIn, (err) => {
        if (err) {
          console.error(`error ${err} occurred when deleting the file`);
        }
      });
    } else {
      console.log("file does not exists");
    }
    await cancelEvent(organiserApiContext, eventId);
    await org_browser_context?.close();
  });

  test("TC001 Attendee is able to enter Registration Based event using otp", async ({
    page,
  }) => {
    await updateEventLandingPageDetails(organiserApiContext, eventId, {
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL_OTP"],
    });
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await addAttendeeRecordInCsv(csvFileToAddDataIn, attendeeEmail);
    let uploadedFileNameOnS3 = await csvApiController.getSignedS3Url(
      csvFileToAddDataIn
    );
    console.log(`fileName is ${uploadedFileNameOnS3}`);
    await csvApiController.processAttendeeCsvOnEndPoint(
      eventId,
      uploadedFileNameOnS3
    );
    await csvApiController.waitForCsvFileProcessingToComplete(
      eventId,
      uploadedFileNameOnS3
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendee_landing_page);
    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    let userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC002 Attendee is able to enter Registration Based event using password flow", async ({
    page,
  }) => {
    await updateEventLandingPageDetails(organiserApiContext, eventId, {
      attendeeAuthOptions: ["EMAIL"],
    });

    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await addAttendeeRecordInCsv(csvFileToAddDataIn, attendeeEmail);
    let uploadedFileNameOnS3 = await csvApiController.getSignedS3Url(
      csvFileToAddDataIn
    );
    console.log(`fileName is ${uploadedFileNameOnS3}`);
    await csvApiController.processAttendeeCsvOnEndPoint(
      eventId,
      uploadedFileNameOnS3
    );
    await csvApiController.waitForCsvFileProcessingToComplete(
      eventId,
      uploadedFileNameOnS3
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendee_landing_page);
    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    let userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC003 Attendee is able to enter Invite based event using otp", async ({
    page,
  }) => {
    await updateEventLandingPageDetails(organiserApiContext, eventId, {
      eventEntryType: EventEntryType.INVITE_ONLY,
      attendeeAuthOptions: ["EMAIL_OTP"],
      isMagicLinkEnabled: false,
    });

    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await addAttendeeRecordInCsv(csvFileToAddDataIn, attendeeEmail);
    let uploadedFileNameOnS3 = await csvApiController.getSignedS3Url(
      csvFileToAddDataIn
    );
    console.log(`fileName is ${uploadedFileNameOnS3}`);
    await csvApiController.processAttendeeCsvOnEndPoint(
      eventId,
      uploadedFileNameOnS3
    );
    await csvApiController.waitForCsvFileProcessingToComplete(
      eventId,
      uploadedFileNameOnS3
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendee_landing_page);
    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    let userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC004 Inviting new attendee via csv, if he logs in via password should trigger otp flow", async ({
    page,
  }) => {
    await updateEventLandingPageDetails(organiserApiContext, eventId, {
      eventEntryType: EventEntryType.INVITE_ONLY,
      attendeeAuthOptions: ["EMAIL_OTP", "EMAIL"],
      isMagicLinkEnabled: false,
    });

    await updateEventEntryType(
      organiserApiContext,
      eventId,
      landingPageId,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await addAttendeeRecordInCsv(csvFileToAddDataIn, attendeeEmail);
    let uploadedFileNameOnS3 = await csvApiController.getSignedS3Url(
      csvFileToAddDataIn
    );
    console.log(`fileName is ${uploadedFileNameOnS3}`);
    await csvApiController.processAttendeeCsvOnEndPoint(
      eventId,
      uploadedFileNameOnS3
    );
    await csvApiController.waitForCsvFileProcessingToComplete(
      eventId,
      uploadedFileNameOnS3
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendee_landing_page);
    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    let userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC005 Added attendee able to enter Invite only event using magic link from email invite", async ({
    page,
  }) => {
    await updateEventEntryType(
      organiserApiContext,
      eventId,
      landingPageId,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;

    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await addAttendeeRecordInCsv(csvFileToAddDataIn, attendeeEmail);
    let uploadedFileNameOnS3 = await csvApiController.getSignedS3Url(
      csvFileToAddDataIn
    );
    console.log(`fileName is ${uploadedFileNameOnS3}`);

    await csvApiController.processAttendeeCsvOnEndPoint(
      eventId,
      uploadedFileNameOnS3
    );

    await csvApiController.waitForCsvFileProcessingToComplete(
      eventId,
      uploadedFileNameOnS3
    );

    let magicLink = await fetchAttendeeInviteMagicLink(
      attendeeEmail,
      eventInfoDto.getInviteEmailSubj()
    );

    console.log(`Magic link -> ${magicLink}`);

    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(magicLink);
    await landingPageOne.isLobbyLoaded();
  });
});
