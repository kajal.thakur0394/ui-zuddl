import {
    test,
    BrowserContext,
    APIRequestContext,
    Page,
  } from "@playwright/test";
  import BrowserFactory from "../../../util/BrowserFactory";
  import {
    getEventDetails,
    createNewEvent,
    updateLandingPageType,
  } from "../../../util/apiUtil";
  import { DataUtl } from "../../../util/dataUtil";
  import { LandingPageTwo } from "../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
  import { UserInfoDTO } from "../../../dto/userInfoDto";
  import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
  import { EventInfoDTO } from "../../../dto/eventInfoDto";
  import EventRole from "../../../enums/eventRoleEnum";
  import EventEntryType from "../../../enums/eventEntryEnum";
  import conditionalFieldsTestData from "../../../test-data/conditional-fields-testdata/createConditionalFields.json";

  import { EventController } from "../../../controller/EventController";

  import LandingPageType from "../../../enums/landingPageEnum";
  
  test.describe
    .parallel("Events with Disclaimer fields | Landing Page 2", () => {
    let org_browser_context: BrowserContext;
    let organiserApiContext: APIRequestContext;
    let eventTitle: string;
    let eventId;
    let eventInfoDto: EventInfoDTO;
    let attendee_landing_page: string;
    let landingPageId;
    let userInfoDTO: UserInfoDTO;
    let userEventRoleDto: UserEventRoleDto;
    let page: Page;
  
    test.beforeEach(async ({ context }) => {
      org_browser_context = await BrowserFactory.getBrowserContext({});
      organiserApiContext = await org_browser_context.request;
      eventTitle = "Disclaimer-events";
      eventId = await createNewEvent({
        api_request_context: organiserApiContext,
        event_title: eventTitle,
      });
      // adding event dto
      eventInfoDto = new EventInfoDTO(eventId, eventTitle, false); // true to denote magic link enabled
      eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
      const event_details_api_resp = await (
        await getEventDetails(organiserApiContext, eventId)
      ).json();
      console.log("event_details_api_resp :", event_details_api_resp);
      landingPageId = event_details_api_resp.eventLandingPageId;
      eventInfoDto.landingPageId = landingPageId;
     
      // updating event custom  fields
      let eventController: EventController = new EventController(
        organiserApiContext,
        eventId
      );

      let discalimerFieldsTestData =
        conditionalFieldsTestData["endToend-custom-discalimer-fields-data"];
      
      await eventController.addDiscalimerFieldsToEvent(discalimerFieldsTestData);
      // Updating event landing page type
      await updateLandingPageType(
        organiserApiContext,
        eventId,
        landingPageId,
        LandingPageType.STYLE2,
        "true",
        EventEntryType.REG_BASED
      );
  
      attendee_landing_page = eventInfoDto.attendeeLandingPage;
      page = await context.newPage();
    });
  
    test.afterEach(async () => {
      await org_browser_context?.close();
    });
    
    test("TC001: Attendee trying to register without checking Disclaimer option and getting Error Message @Landing Page 2", async () => {
      /*
            1. All reg field filled.
            2. Leaving desclaimer button Unchecked.
             */
            test.info().annotations.push({
              type: "TC",
              description:
                "https://linear.app/zuddl/issue/QAT-285/verify-on-lp2",
            });
      let attendeeEmail = DataUtl.getRandomAttendeeEmail();
      userInfoDTO = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        true
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDTO,
        eventInfoDto,
        EventRole.NOTEXISTS,
        false
      );
      let landingPageTwo = new LandingPageTwo(page);
      await test.step("Attendee Entered Landing Page 2",async () => {
        await landingPageTwo.load(attendee_landing_page); 
      });
      
      await test.step("Attendee Filled Registration Details and Not Clicked Disclaimer Button",async () => {
      await (
        await landingPageTwo.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData._defaultRegFormData,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
          clickOnDesclaimerButton: false,
        },false
      );
      });
      await test.step("Error message displayed to check in Disclaimer field", async () => {
        await (
          await landingPageTwo.getRegistrationFormComponent()
        ).verifyDisclaimerIsRequiredErrorMessageIsDisplayed();
      });
      
    });
  });
  
