import { test, BrowserContext, APIRequestContext } from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { EventController } from "../../../controller/EventController";
import { DudaLandingPage } from "../../../page-objects/events-pages/duda-website/dudaLandingPage";
import { RegistrationWidgetIframe } from "../../../page-objects/events-pages/duda-website/registrationIframe";

test.describe("Events with Disclaimer fields | Duda landing Page", () => {
  test.slow();
  test.describe.configure({ retries: 2 });

  let org_browser_context: BrowserContext;
  let organiserApiContext: APIRequestContext;
  let eventId;

  test.beforeEach(async () => {
    org_browser_context = await BrowserFactory.getBrowserContext({});
    organiserApiContext = org_browser_context.request;

    eventId =
      DataUtl.getApplicationTestDataObj()[
        "desclaimerAddedDudaLandingPageEventID"
      ];

    let eventController = new EventController(organiserApiContext, eventId);

    await eventController.changeEventStartAndEndDateTime({
      deltaByHoursInStartTime: 1,
      deltaByHoursInEndTime: 3,
    });
  });

  test.afterEach(async () => {
    await org_browser_context?.close();
  });

  test("TC001: Attendee trying to register without checking Disclaimer option and getting Error Message on Duda landing page", async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-283/verify-on-duda",
    });

    let dudaPublishedAttendeeLandingPage =
      DataUtl.getApplicationTestDataObj()["desclaimerAddedDudaLandingPage"];
    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;

    const attendeeEmail = DataUtl.getRandomAttendeeEmail();
    const attendeeInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "TestAttendee",
      "Automation",
      false
    );

    await test.step(`Loading the duda attendee landing page and click on register button to get registration iframe`, async () => {
      await page.goto(dudaPublishedAttendeeLandingPage);
      dudaAttendeeLandingPage = new DudaLandingPage(page);
      registrationWidgetIframe =
        await dudaAttendeeLandingPage.clickOnRegisterButton();
      await page.waitForLoadState("networkidle");
      await page.waitForLoadState("domcontentloaded");
      await page.waitForLoadState("load");
    });

    await test.step(`Fill-up the registration form`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.fillUpTheRegistrationFormForPredefinedFields(
        attendeeInfoDto.userRegFormData._defaultRegFormData,
        {
          fillMandatoryFields: true,
          fillOptionalFields: true,
          clickOnDesclaimerButton: false,
        }
      );
    });

    await test.step(`Verify that error message is displayed`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.verifyDisclaimerIsRequiredErrorMessageIsDisplayed();
    });
  });
});
