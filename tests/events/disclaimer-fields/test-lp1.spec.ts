import {
    test,
    BrowserContext,
    APIRequestContext,
    Page,
    expect,
  } from "@playwright/test";
  import BrowserFactory from "../../../util/BrowserFactory";
  import {
    getEventDetails,
    createNewEvent,
  } from "../../../util/apiUtil";
  import { DataUtl } from "../../../util/dataUtil";
  import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
  import { UserInfoDTO } from "../../../dto/userInfoDto";
  import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
  import { EventInfoDTO } from "../../../dto/eventInfoDto";
  import EventRole from "../../../enums/eventRoleEnum";
  import EventEntryType from "../../../enums/eventEntryEnum";
  import conditionalFieldsTestData from "../../../test-data/conditional-fields-testdata/createConditionalFields.json";
  import { EventController } from "../../../controller/EventController";
  
  test.describe("Events with Disclaimer fields | Landing Page 1", () => {
    let org_browser_context: BrowserContext;
    let organiserApiContext: APIRequestContext;
    let eventTitle: string;
    let eventId;
    let eventInfoDto: EventInfoDTO;
    let attendee_landing_page: string;
    let landingPageId;
    let userInfoDTO: UserInfoDTO;
    let userEventRoleDto: UserEventRoleDto;
    let page: Page;
  
    test.beforeEach(async ({ context }) => {

      org_browser_context = await BrowserFactory.getBrowserContext({});

      organiserApiContext = await org_browser_context.request;
      eventTitle = "Disclaimer-events";

      await test.step('Create a new event.',async () => {
        eventId = await createNewEvent({
          api_request_context: organiserApiContext,
          event_title: eventTitle,
        });
      });

      // adding event dto
      eventInfoDto = new EventInfoDTO(eventId, eventTitle, false); // true to denote magic link enabled
      eventInfoDto.eventEntryType = EventEntryType.REG_BASED;

      const event_details_api_resp = await (
        await getEventDetails(organiserApiContext, eventId)
      ).json();

      console.log("event_details_api_resp :", event_details_api_resp);
      landingPageId = event_details_api_resp.eventLandingPageId;
      eventInfoDto.landingPageId = landingPageId;


      
      // updating event custom  fields
      let eventController: EventController = new EventController(
        organiserApiContext,
        eventId
      );
     
      let discalimerFieldsTestData =
        conditionalFieldsTestData["endToend-custom-discalimer-fields-data"];

      console.log(
        "######## adding disclaimer fields to the events ##############"
      );

      await test.step('Adding discalimer fields.',async () => {
        await eventController.addDiscalimerFieldsToEvent(discalimerFieldsTestData);
      });

      // Updating event dto
      attendee_landing_page = eventInfoDto.attendeeLandingPage;
      page = await context.newPage();
    });
  
    test.afterEach(async () => {
      await org_browser_context?.close();
    });
    
    test("TC001: Attendee trying to register without checking Disclaimer option and getting Error Message", async () => {
      /*
      1. All reg field filled.
      2. Leaving desclaimer button Unchecked.
      */
     test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-284/verify-on-lp1",
      });

    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
      userInfoDTO = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        true
      );

      userEventRoleDto = new UserEventRoleDto(
        userInfoDTO,
        eventInfoDto,
        EventRole.NOTEXISTS,
        false
      );

      let landingPageOne = new LandingPageOne(page);

      await test.step("Attendee entered landing page-1.",async () => {
        await landingPageOne.load(attendee_landing_page); 
        await (
          await landingPageOne.getRegistrationFormComponent()
        ).verifyDisclaimerFieldIsVisible();
        
  
      });
            
      await test.step("Attendee filles registration details and not clicks disclaimer button.",async () => {
        await(await landingPageOne.getRegistrationFormComponent()).fillUpTheRegistrationForm(
          userInfoDTO.userRegFormData._defaultRegFormData,
          {
            fillOptionalFields: true,
            fillMandatoryFields: true,
            clickOnDesclaimerButton: false,
          },false
        );
      });

      await test.step("Error message displayed to check in Disclaimer field.", async () => {
        await (
          await landingPageOne.getRegistrationFormComponent()
        ).verifyDisclaimerIsRequiredErrorMessageIsDisplayed();
      });
      
    });
});
  
