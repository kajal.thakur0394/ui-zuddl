import { APIRequestContext, Page, expect } from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { OrganisationController } from "../../../controller/OrganisationController";
import { TeamController } from "../../../controller/TeamsController";
import {
  MemberRolesCapitaliseEnum,
  MemberRolesEnum,
} from "../../../enums/MemberRolesEnum";
import BillngPeriodEnum from "../../../enums/billingPeriodEnum";
import EventEntryType from "../../../enums/eventEntryEnum";
import PlanType from "../../../enums/planTypeEnum";
import ProductType from "../../../enums/productTypeEnum";
import { baseTeamTest } from "../../../fixtures/teamsandplans-fixtures/baseTeamFixture";
import {
  EventLicenseDetailsDto,
  LicenseDetailsDto,
  WebinarLicenseDetailsDto,
} from "../../../interfaces/OrganisationControllerInterface";
import { GeneralSettingsPage } from "../../../page-objects/teams-page/GeneralSettingsPage";
import { TeamMembersPage } from "../../../page-objects/teams-page/TeamMembersPage";
import {
  updateEventLandingPageDetails,
  userVerifyOtpByApi,
  userVerifyTeamInvitationUsingOtp,
} from "../../../util/apiUtil";
import { DataUtl } from "../../../util/dataUtil";
import { emailTemplateValidation } from "../../../util/emailTemplateValidation";
import { fetchMailasaurEmailObject } from "../../../util/emailUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

baseTeamTest.describe("@teams", async () => {
  let organisationId: string;
  let generalTeamId: string;
  let ownerPage: Page;
  let adminPage: Page;
  let ownerApiContext: APIRequestContext;
  let adminApiContext: APIRequestContext;
  let organisationController: OrganisationController;
  let teamContoller: TeamController;
  let ownerEmail: string;
  let adminEmail: string;
  let ownerMemberId: string;
  let adminMemberId: string;
  let organisationName: string;
  let orgMemberIds: string[] = [];

  const defaultEventLicenseDetailsDto: EventLicenseDetailsDto = {
    planStartDate: new Date().toISOString(),
    planEndDate: new Date(
      new Date().setDate(new Date().getDate() + 3)
    ).toISOString(),
    teamSize: 10,
    cloudStorageSizeInGb: 2,
    noOfAttendees: 100,
    perEventLengthInHours: 500,
    enabledEventTypes: ["STANDARD"],
  };
  const defaultWebinarLicenseDetailsDto: WebinarLicenseDetailsDto = {
    planStartDate: new Date().toISOString(),
    planEndDate: new Date(
      new Date().setDate(new Date().getDate() + 3)
    ).toISOString(),
    noOfTeams: 10,
    cloudStorageSizeInGb: 2,
    noOfAttendees: 100,
    restrictionRenewPeriod: BillngPeriodEnum.MONTHLY,
    attendeesBufferPercentage: 20,
  };
  const defaultLicenseDetailDtoEvents: LicenseDetailsDto = {
    productType: ProductType.EVENT, //enum
    planType: PlanType.ATTENDEE_CUSTOM, //enum
    billingPeriodUnit: BillngPeriodEnum.MONTHLY,
    eventLicenseDetailsDto: defaultEventLicenseDetailsDto,
  };
  const defaultLicenseDetailDtoWebinar: LicenseDetailsDto = {
    productType: ProductType.WEBINAR, //enum
    planType: PlanType.CUSTOM, //enum
    billingPeriodUnit: BillngPeriodEnum.MONTHLY,
    webinarLicenseDetailsDto: defaultWebinarLicenseDetailsDto,
  };
  const ownerGeneralSettingsPageUrl = `${
    DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
  }/dashboard/team`;

  baseTeamTest.beforeAll(async ({ ownerContextInfo, adminContextInfo }) => {
    ownerPage = ownerContextInfo.page;
    adminPage = adminContextInfo.page;
    ownerApiContext = ownerContextInfo.requestContext;
    adminApiContext = adminContextInfo.requestContext;

    ownerEmail = DataUtl.getRandomOrganisationOwnerEmail();
    adminEmail = DataUtl.getRandomAdminEmail();
    organisationName = DataUtl.getRandomOrganisationName();

    const createOrganizationDetailsData = {
      firstName: "PrateeekAutomation",
      lastName: "OwnerQA",
      email: ownerEmail,
      organizationName: organisationName,
      licenseDetailsDto: defaultLicenseDetailDtoEvents,
    };

    await baseTeamTest.step(
      `Creating new organisation with name : ${organisationName} and owner email : ${ownerEmail}`,
      async () => {
        organisationController = new OrganisationController(
          ownerContextInfo.requestContext
        );
        const createOrgApiResp =
          await organisationController.createNewOrganization(
            createOrganizationDetailsData
          );
        const createOrgApiRespJson = await createOrgApiResp.json();
        organisationId = createOrgApiRespJson["id"];
        console.log(
          `New organisation is created witd org id : ${organisationId}`
        );
        teamContoller = new TeamController(
          ownerContextInfo.requestContext,
          organisationId
        );
      }
    );

    await baseTeamTest.step(
      `Adding WEBINAR Plan to the same organisation`,
      async () => {
        const addWebinarInOrganizationDetailsData = {
          organizationId: organisationId,
          licenseDetailsDto: defaultLicenseDetailDtoWebinar,
        };
        const addWebinarPlanResp =
          await organisationController.renewPlanInOrganization(
            addWebinarInOrganizationDetailsData
          );
        console.log(`Organisation ${organisationName} now has WEBINAR Plan`);
      }
    );

    await baseTeamTest.step(
      `Now added member will login to setup.zuddl via otp to verify his invitation`,
      async () => {
        const otpVerificationIdForOwner =
          await userVerifyTeamInvitationUsingOtp(ownerApiContext, ownerEmail);
        console.log(
          `owner otp verification id is ${otpVerificationIdForOwner}`
        );
        //fetch otp for owner
        const otpCodeForOwner = await QueryUtil.fetchLoginOTPByVerificationId(
          ownerEmail,
          otpVerificationIdForOwner
        );
        //verify otp for owner
        console.log(`fetched otp for owner is ${otpCodeForOwner}`);
        await userVerifyOtpByApi(
          ownerApiContext,
          otpVerificationIdForOwner,
          otpCodeForOwner
        );
      }
    );

    await baseTeamTest.step(
      `Verify organisation-role to be OWNER for email->${ownerEmail} in General Team`,
      async () => {
        generalTeamId = await teamContoller.fetchGeneralTeamId();
        expect(
          await teamContoller.fetchRoleOfAMember(
            generalTeamId,
            ownerEmail,
            "organizationRole"
          )
        ).toEqual(MemberRolesEnum.OWNER);
      }
    );

    await baseTeamTest.step(
      `Create new member with Product role as ORGANIZER and invite into General Team`,
      async () => {
        await organisationController.addNewMemberToOrganisation({
          firstName: "Prateek",
          lastName: "Admin",
          email: adminEmail,
          productRole: MemberRolesEnum.ORGANIZER,
          myTeamList: [{ teamId: generalTeamId }],
        });
      }
    );

    await baseTeamTest.step(
      `Now added member will login to setup.zuddl via otp to verify his invitation`,
      async () => {
        const otpVerificationIdForNewMember =
          await userVerifyTeamInvitationUsingOtp(adminApiContext, adminEmail);
        //fetch otp by verification id
        const otpCodeForOrganiser =
          await QueryUtil.fetchLoginOTPByVerificationId(
            adminEmail,
            otpVerificationIdForNewMember
          );
        console.log(`fetched otp for organiser is ${otpCodeForOrganiser}`);
        //now organiser verify his otp
        await userVerifyOtpByApi(
          adminApiContext,
          otpVerificationIdForNewMember,
          otpCodeForOrganiser
        );
      }
    );

    await baseTeamTest.step(
      `Now update the organisation-role of ${adminEmail} to ADMIN via API`,
      async () => {
        await teamContoller.updateMemberRoleInOrganization(
          generalTeamId,
          adminEmail,
          MemberRolesEnum.ADMIN
        );
      }
    );

    await baseTeamTest.step(
      `Verify organisation-role to be ADMIN for admin email->${adminEmail} in General Team`,
      async () => {
        expect(
          await teamContoller.fetchRoleOfAMember(
            generalTeamId,
            adminEmail,
            "organizationRole"
          )
        ).toEqual(MemberRolesEnum.ADMIN);
      }
    );

    await baseTeamTest.step(
      `Fetch orgMemberId of OWNER and ADMIN via API`,
      async () => {
        ownerMemberId = await teamContoller.fetchOrgMemberIdOfAMember(
          generalTeamId,
          ownerEmail
        );
        adminMemberId = await teamContoller.fetchOrgMemberIdOfAMember(
          generalTeamId,
          adminEmail
        );
        orgMemberIds.push(ownerMemberId);
        orgMemberIds.push(adminMemberId);
      }
    );
  });

  baseTeamTest(
    "TC001 : Verify on creating new Team, Owner/Admin is able to add members with different product and organisation role in that team",
    async ({ organiserContextInfo, organiserEmail, moderatorEmail }) => {
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-459/verify-on-creating-new-team-owner-is-able-to-add-members-with",
      });
      await baseTeamTest.step(
        "Verify Addition of Members having organisation-role as OWNER/ADMIN in a newly created Team by OWNER",
        async () => {
          let ownerGeneralSettingsPage = new GeneralSettingsPage(ownerPage);
          let ownerTeamMembersPage = new TeamMembersPage(ownerPage);
          const teamName = DataUtl.getRandomTeamName();

          await baseTeamTest.step(
            `OWNER creates a new Team via UI`,
            async () => {
              await ownerPage.goto(ownerGeneralSettingsPageUrl);
              await ownerGeneralSettingsPage.createNewTeam(teamName);
            }
          );

          await baseTeamTest.step(
            `Verify OWNER and ADMIN's organisation-role users should get added by default`,
            async () => {
              let rolesToCheck = new Map<string, MemberRolesCapitaliseEnum>();
              rolesToCheck.set(ownerEmail, MemberRolesCapitaliseEnum.OWNER);
              rolesToCheck.set(adminEmail, MemberRolesCapitaliseEnum.ADMIN);

              await ownerGeneralSettingsPage.selectATeam(teamName);
              await ownerGeneralSettingsPage.openTeamMembersPage();
              await ownerTeamMembersPage.verifyOrganizationRoleOfMember(
                rolesToCheck
              );
            }
          );
        }
      );

      await baseTeamTest.step(
        "Verify Addition of Members having organisation-role as OWNER/ADMIN in a newly created Team by ADMIN",
        async () => {
          let adminGeneralSettingsPage = new GeneralSettingsPage(adminPage);
          let adminTeamMembersPage = new TeamMembersPage(adminPage);
          const teamName = DataUtl.getRandomTeamName();

          await baseTeamTest.step(
            `ADMIN creates a new Team via UI`,
            async () => {
              await adminPage.goto(ownerGeneralSettingsPageUrl);
              await adminGeneralSettingsPage.createNewTeam(teamName);
            }
          );

          await baseTeamTest.step(
            `Verify OWNER and ADMIN's organisation-role users should get added by default`,
            async () => {
              let rolesToCheck = new Map<string, MemberRolesCapitaliseEnum>();
              rolesToCheck.set(ownerEmail, MemberRolesCapitaliseEnum.OWNER);
              rolesToCheck.set(adminEmail, MemberRolesCapitaliseEnum.ADMIN);

              await adminGeneralSettingsPage.selectATeam(teamName);
              await adminGeneralSettingsPage.openTeamMembersPage();
              await adminTeamMembersPage.verifyOrganizationRoleOfMember(
                rolesToCheck
              );
            }
          );
        }
      );

      await baseTeamTest.step(
        "Verify Addition of Member having product-role as ORGANIZER in a newly created Team by OWNER",
        async () => {
          let ownerTeamController = new TeamController(
            ownerApiContext,
            organisationId
          );
          let newTeamIdByOwner: string;

          const teamName = "Owner Team";

          await baseTeamTest.step(
            `OWNER creates new team with name ${teamName}`,
            async () => {
              await organisationController.createATeam({
                teamName,
                orgMemberIds,
              });
              newTeamIdByOwner = await ownerTeamController.fetchTeamId(
                teamName
              );
              console.log(newTeamIdByOwner);
            }
          );

          await baseTeamTest.step(
            `Create a new member with Product role as ORGANIZER and invite into newly created Team`,
            async () => {
              await organisationController.addNewMemberToOrganisation({
                firstName: "Organizer",
                lastName: "Automation",
                email: organiserEmail,
                productRole: MemberRolesEnum.ORGANIZER,
                myTeamList: [{ teamId: newTeamIdByOwner }],
              });
            }
          );

          await baseTeamTest.step(
            `Now added member will login to setup.zuddl via otp to verify his invitation`,
            async () => {
              const otpVerificationIdForNewMember =
                await userVerifyTeamInvitationUsingOtp(
                  organiserContextInfo.requestContext,
                  organiserEmail
                );
              //fetch otp by verification id
              const otpCodeForOrganiser =
                await QueryUtil.fetchLoginOTPByVerificationId(
                  organiserEmail,
                  otpVerificationIdForNewMember
                );
              console.log(
                `fetched otp for organiser is ${otpCodeForOrganiser}`
              );
              //now organiser verify his otp
              await userVerifyOtpByApi(
                organiserContextInfo.requestContext,
                otpVerificationIdForNewMember,
                otpCodeForOrganiser
              );
            }
          );

          await baseTeamTest.step(
            `Verify organisation-role to be MEMBER and product-role to be ORGANIZER for email->${organiserEmail} in newly created Team`,
            async () => {
              expect(
                await teamContoller.fetchRoleOfAMember(
                  newTeamIdByOwner,
                  organiserEmail,
                  "organizationRole"
                )
              ).toEqual(MemberRolesEnum.MEMBER);
              expect(
                await teamContoller.fetchRoleOfAMember(
                  newTeamIdByOwner,
                  organiserEmail,
                  "productRole"
                )
              ).toEqual(MemberRolesEnum.ORGANIZER);
            }
          );
        }
      );

      await baseTeamTest.step(
        "Verify Addition of Member having product-role as MODERATOR in a newly created Team by ADMIN",
        async () => {
          let adminTeamController = new TeamController(
            adminApiContext,
            organisationId
          );
          let newTeamIdByAdmin: string;
          const teamName = "Admin Team";

          await baseTeamTest.step(
            `ADMIN creates new team with name ${teamName}`,
            async () => {
              await organisationController.createATeam({
                teamName,
                orgMemberIds,
              });
              newTeamIdByAdmin = await adminTeamController.fetchTeamId(
                teamName
              );
              console.log(newTeamIdByAdmin);
            }
          );

          await baseTeamTest.step(
            `Create a new member with Product role as MODERATOR and invite into newly created Team`,
            async () => {
              await organisationController.addNewMemberToOrganisation({
                firstName: "Moderator",
                lastName: "Automation",
                email: moderatorEmail,
                productRole: MemberRolesEnum.MODERATOR,
                myTeamList: [{ teamId: newTeamIdByAdmin }],
              });
            }
          );

          await baseTeamTest.step(
            `Now added member will login to setup.zuddl via otp to verify his invitation`,
            async () => {
              const otpVerificationIdForNewMember =
                await userVerifyTeamInvitationUsingOtp(
                  adminApiContext,
                  moderatorEmail
                );
              //fetch otp by verification id
              const otpCodeForModerator =
                await QueryUtil.fetchLoginOTPByVerificationId(
                  moderatorEmail,
                  otpVerificationIdForNewMember
                );
              console.log(
                `fetched otp for organiser is ${otpCodeForModerator}`
              );
              //now moderator verify his otp
              await userVerifyOtpByApi(
                adminApiContext,
                otpVerificationIdForNewMember,
                otpCodeForModerator
              );
            }
          );

          await baseTeamTest.step(
            `Verify organisation-role to be MEMBER and product-role to be MODERATOR for email->${adminEmail} in newly created Team`,
            async () => {
              expect(
                await teamContoller.fetchRoleOfAMember(
                  newTeamIdByAdmin,
                  moderatorEmail,
                  "organizationRole"
                )
              ).toEqual(MemberRolesEnum.MEMBER);
              expect(
                await teamContoller.fetchRoleOfAMember(
                  newTeamIdByAdmin,
                  moderatorEmail,
                  "productRole"
                )
              ).toEqual(MemberRolesEnum.MODERATOR);
            }
          );
        }
      );
    }
  );

  baseTeamTest(
    "TC002 : Verify user present in Team1 should not be present in Team2",
    async ({ organiserEmail, moderatorEmail }) => {
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-460/verify-user-present-in-team1-should-not-be-present-in-team2-if-not",
      });
      let newTeamId1: string;
      let newTeamId2: string;

      const team1 = {
        teamName: "Owner1" + DataUtl.getRandomTeamName(),
        orgMemberIds: orgMemberIds,
      };
      const team2 = {
        teamName: "Owner2" + DataUtl.getRandomTeamName(),
        orgMemberIds: orgMemberIds,
      };
      await baseTeamTest.step(`Fetch default/general Team Id`, async () => {
        generalTeamId = await teamContoller.fetchGeneralTeamId();
      });

      await baseTeamTest.step(
        `OWNER creates a new team with name ${team1.teamName} via API`,
        async () => {
          await organisationController.createATeam(team1);
          newTeamId1 = await teamContoller.fetchTeamId(team1.teamName);
        }
      );

      await baseTeamTest.step(
        `Create a new member with Product role as ORGANIZER and add into newly created Team ${team1.teamName}`,
        async () => {
          await organisationController.addNewMemberToOrganisation({
            firstName: "Organizer1",
            lastName: "Automation1",
            email: organiserEmail,
            productRole: MemberRolesEnum.ORGANIZER,
            myTeamList: [{ teamId: newTeamId1 }, { teamId: generalTeamId }],
          });
        }
      );

      await baseTeamTest.step(
        `OWNER again creates a new team with name ${team2.teamName} via API`,
        async () => {
          await organisationController.createATeam(team2);
          newTeamId2 = await teamContoller.fetchTeamId(team2.teamName);
        }
      );

      await baseTeamTest.step(
        `Create a new member with Product role as MODERATOR and add into newly created Team ${team2.teamName}`,
        async () => {
          await organisationController.addNewMemberToOrganisation({
            firstName: "Moderator",
            lastName: "Automation",
            email: moderatorEmail,
            productRole: MemberRolesEnum.MODERATOR,
            myTeamList: [{ teamId: newTeamId2 }, { teamId: generalTeamId }],
          });
        }
      );

      await baseTeamTest.step(
        `Verify member with email ${organiserEmail} inside ${team1.teamName} is not present in ${team2.teamName}`,
        async () => {
          expect(
            await teamContoller.verifyMemberToBePresentInATeam(
              newTeamId2,
              organiserEmail
            )
          ).toBeFalsy();
        }
      );

      await baseTeamTest.step(
        `Verify member with email ${moderatorEmail} inside ${team2.teamName} is not present in ${team1.teamName}`,
        async () => {
          expect(
            await teamContoller.verifyMemberToBePresentInATeam(
              newTeamId1,
              moderatorEmail
            )
          ).toBeFalsy();
        }
      );
    }
  );

  baseTeamTest(
    "TC003 : Verify search flow of Members in Organisation's General Setting and Team Members Listing page",
    async () => {
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-381/verify-team-member-search-is-working-fine",
      });

      let ownerGeneralSettingsPage = new GeneralSettingsPage(ownerPage);
      let ownerTeamMembersPage = new TeamMembersPage(ownerPage);
      let newTeamId: string;

      const team1 = {
        teamName: "Owner" + DataUtl.getRandomTeamName(),
        orgMemberIds: orgMemberIds,
      };

      await baseTeamTest.step(`Fetch default/general Team Id`, async () => {
        generalTeamId = await teamContoller.fetchGeneralTeamId();
      });

      await baseTeamTest.step(
        `OWNER creates a new team with name ${team1.teamName} via API`,
        async () => {
          await organisationController.createATeam(team1);
          newTeamId = await teamContoller.fetchTeamId(team1.teamName);
        }
      );

      await baseTeamTest.step(
        `Owner invites 3 members with product-role as ORGANIZER in the ${team1.teamName} team`,
        async () => {
          await organisationController.addMultipleMembersToOrganisation(
            {
              firstName: "Org",
              lastName: "Automation",
              myTeamList: [{ teamId: newTeamId }, { teamId: generalTeamId }],
            },
            3,
            true
          );
        }
      );

      await baseTeamTest.step(
        `Owner invites 2 members with product-role as MODERATOR in the ${team1.teamName} team`,
        async () => {
          await organisationController.addMultipleMembersToOrganisation(
            {
              firstName: "Mod",
              lastName: "Automation",
              myTeamList: [{ teamId: newTeamId }, { teamId: generalTeamId }],
            },
            2,
            false
          );
        }
      );

      await baseTeamTest.step(
        `Verify ${ownerEmail} email Search in General Settings Page`,
        async () => {
          await ownerPage.goto(ownerGeneralSettingsPageUrl);
          await ownerGeneralSettingsPage.verifyGeneralSettingsSearchMemberFlow(
            ownerEmail
          );
        }
      );

      await baseTeamTest.step(
        `Verify ${adminEmail} email Search in Team Members Page`,
        async () => {
          await ownerGeneralSettingsPage.selectATeam(team1.teamName);
          await ownerGeneralSettingsPage.openTeamMembersPage();
          await ownerTeamMembersPage.verifyTeamMembersSearchFlow(adminEmail);
        }
      );
    }
  );

  baseTeamTest(
    "TC004 : Verify flow of adding a new member to the team",
    async ({ organiserContextInfo, organiserEmail }) => {
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-387/verify-flow-of-adding-a-new-member-to-the-team",
      });
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-446/verify-email-triggers-when-a-user-is-invited-to-a-team",
      });
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-447/verify-his-state-remains-invite-until-he-accepts-the-invite",
      });
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-449/verify-flow-of-user-accepting-invite-and-then-by-default-he-lands-on",
      });

      let newTeamId1: string;
      let firstName: string;
      let goToDashboardButtonContents;
      let ownerGeneralSettingsPage = new GeneralSettingsPage(ownerPage);
      let organiserGeneralSettingsPage: GeneralSettingsPage;

      const team1 = {
        teamName: DataUtl.getRandomTeamName(),
        orgMemberIds: orgMemberIds,
      };

      await baseTeamTest.step(`Fetch default/general Team Id`, async () => {
        generalTeamId = await teamContoller.fetchGeneralTeamId();
      });

      await baseTeamTest.step(
        `OWNER creates a new team with name ${team1.teamName} via API`,
        async () => {
          await organisationController.createATeam(team1);
          newTeamId1 = await teamContoller.fetchTeamId(team1.teamName);
        }
      );

      await baseTeamTest.step(
        `Create a new member with Product role as ORGANIZER and add into newly created Team ${team1.teamName}`,
        async () => {
          firstName = "Organizzer";
          await organisationController.addNewMemberToOrganisation({
            firstName: firstName,
            lastName: "Automation1",
            email: organiserEmail,
            productRole: MemberRolesEnum.ORGANIZER,
            myTeamList: [{ teamId: generalTeamId }, { teamId: newTeamId1 }],
          });
        }
      );

      await baseTeamTest.step(
        `Verify email is triggered, when ORGANIZER is invited to ${team1.teamName}`,
        async () => {
          let emailTemplateObject = await fetchMailasaurEmailObject(
            organiserEmail,
            ""
          );
          let emailInvite = new emailTemplateValidation(emailTemplateObject);
          await baseTeamTest.step(
            `Validating Go to dashboard button text to be visible in email`,
            async () => {
              goToDashboardButtonContents =
                await emailInvite.fetchGoToDashboardButtonLink();
              expect(
                goToDashboardButtonContents[0],
                "Verifying text of goToDashboard button"
              ).toContain("Go to dashboard");
            }
          );
        }
      );

      await baseTeamTest.step(
        `OWNER verifies Invite sent message to be present on UI`,
        async () => {
          await ownerPage.goto(ownerGeneralSettingsPageUrl);
          await ownerGeneralSettingsPage.verifyInvitationMessageToBeVisible(
            firstName
          );
        }
      );

      await baseTeamTest.step(
        `Now added member will login to setup.zuddl via otp to verify his invitation`,
        async () => {
          const otpVerificationIdForNewMember =
            await userVerifyTeamInvitationUsingOtp(
              organiserContextInfo.requestContext,
              organiserEmail
            );
          //fetch otp by verification id
          const otpCodeForOrganiser =
            await QueryUtil.fetchLoginOTPByVerificationId(
              organiserEmail,
              otpVerificationIdForNewMember
            );
          console.log(`fetched otp for organiser is ${otpCodeForOrganiser}`);
          //now organiser verify his otp
          await userVerifyOtpByApi(
            organiserContextInfo.requestContext,
            otpVerificationIdForNewMember,
            otpCodeForOrganiser
          );
        }
      );

      await baseTeamTest.step(
        `Now, ORGANIZER logs into the dashboard`,
        async () => {
          await organiserContextInfo.page.waitForTimeout(5000);
          await organiserContextInfo.page.goto(goToDashboardButtonContents[1], {
            waitUntil: "networkidle",
          });
          await organiserContextInfo.page.waitForTimeout(5000);
        }
      );

      await baseTeamTest.step(
        `Then, OWNER verifies disappearance of Invite sent message from UI`,
        async () => {
          await ownerPage.reload();
          await ownerGeneralSettingsPage.verifyInvitationMessageToBeInvisible(
            firstName
          );
        }
      );

      await baseTeamTest.step(
        `Now, ORGANIZER verifies General team should be the default selected team`,
        async () => {
          organiserGeneralSettingsPage = new GeneralSettingsPage(
            organiserContextInfo.page
          );
          await organiserGeneralSettingsPage.verifyCurrentTeamFromTeamList([
            "General",
            team1.teamName,
          ]);
        }
      );

      await baseTeamTest.step(
        `Verify ORGANIZER is successfully able to switch to ${team1.teamName} team`,
        async () => {
          await organiserContextInfo.page
            .locator("button[data-highlight='Dismiss']")
            .click();
          await organiserGeneralSettingsPage.selectATeam(team1.teamName);
          await organiserGeneralSettingsPage.verifyCurrentTeamSelected(
            team1.teamName
          );
        }
      );
    }
  );

  baseTeamTest(
    `TC005: When more than 3 teams are assigned to user, verify by api that he has all the team assigned, invisibility of invite member button, deletion flow of a member, and team access flow`,
    async ({
      organiserContextInfo,
      moderatorContextInfo,
      organiserEmail,
      moderatorEmail,
    }) => {
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-455/when-more-than-3-teams-are-assigned-to-user-verify-by-api-that-he-has",
      });
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-456/invisibility-of-invite-members-button-as-organiser-and-moderator",
      });
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-454/verify-user-if-deleted-once-should-be-able-to-get-added-again",
      });
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          " https://linear.app/zuddl/issue/QAT-383/organiser-should-get-an-option-to-select-which-team-he-wants-to-remove",
      });

      let newTeamId1: string;
      let newTeamId2: string;
      let newTeamId3: string;
      let orgMemberIdOfOrganiser: string;
      let orgMemberIdOfModerator: string;
      let actualTeamListMap: Map<string, string>;
      let expectedTeamListMap: Map<string, string>;
      let ownerGeneralSettingsPage = new GeneralSettingsPage(ownerPage);

      let organiserGeneralSettingsPage: GeneralSettingsPage;
      let organiserTeamController: TeamController;
      let moderatorGeneralSettingsPage: GeneralSettingsPage;

      const team1 = {
        teamName: DataUtl.getRandomTeamName(),
        orgMemberIds: orgMemberIds,
      };

      const team2 = {
        teamName: DataUtl.getRandomTeamName(),
        orgMemberIds: orgMemberIds,
      };

      const team3 = {
        teamName: "team3" + Date.now().toString(), //needed big team name
        orgMemberIds: orgMemberIds,
      };

      await baseTeamTest.step(`Fetch default/general Team Id`, async () => {
        generalTeamId = await teamContoller.fetchGeneralTeamId();
      });

      await baseTeamTest.step(
        `OWNER creates a new team1 with name ${team1.teamName} via API`,
        async () => {
          await organisationController.createATeam(team1);
          newTeamId1 = await teamContoller.fetchTeamId(team1.teamName);
          console.log(
            `Created team1 ${team1.teamName} has teamId ${newTeamId1}`
          );
        }
      );

      await baseTeamTest.step(
        `OWNER creates a new team2 with name ${team2.teamName} via API`,
        async () => {
          await organisationController.createATeam(team2);
          newTeamId2 = await teamContoller.fetchTeamId(team2.teamName);
          console.log(
            `Created team2 ${team2.teamName} has teamId ${newTeamId2}`
          );
        }
      );

      await baseTeamTest.step(
        `OWNER creates a new team3 with name ${team3.teamName} via API`,
        async () => {
          await organisationController.createATeam(team3);
          newTeamId3 = await teamContoller.fetchTeamId(team3.teamName);
          console.log(
            `Created team3 ${team3.teamName} has teamId ${newTeamId3}`
          );
        }
      );

      await baseTeamTest.step(
        `Now, OWNER adds a new member with Product role as ORGANIZER and invite into the organisation`,
        async () => {
          await organisationController.addNewMemberToOrganisation({
            firstName: "Organizer",
            lastName: "Automation",
            email: organiserEmail,
            productRole: MemberRolesEnum.ORGANIZER,
            myTeamList: [{ teamId: generalTeamId }],
          });
          orgMemberIdOfOrganiser =
            await teamContoller.fetchOrgMemberIdOfAMember(
              generalTeamId,
              organiserEmail
            );
        }
      );

      await baseTeamTest.step(
        `Now added member will login to setup.zuddl via otp to verify his invitation`,
        async () => {
          const otpVerificationIdForNewMember =
            await userVerifyTeamInvitationUsingOtp(
              organiserContextInfo.requestContext,
              organiserEmail
            );
          //fetch otp by verification id
          const otpCodeForOrganiser =
            await QueryUtil.fetchLoginOTPByVerificationId(
              organiserEmail,
              otpVerificationIdForNewMember
            );
          console.log(`fetched otp for organiser is ${otpCodeForOrganiser}`);
          //now organiser verify his otp
          await userVerifyOtpByApi(
            organiserContextInfo.requestContext,
            otpVerificationIdForNewMember,
            otpCodeForOrganiser
          );
        }
      );

      await baseTeamTest.step(
        "Now, OWNER adds a new member with Product role as MODERATOR and invite into the organisation",
        async () => {
          await organisationController.addNewMemberToOrganisation({
            firstName: "Moderator",
            lastName: "Automation",
            email: moderatorEmail,
            productRole: MemberRolesEnum.MODERATOR,
            myTeamList: [{ teamId: generalTeamId }],
          });
          orgMemberIdOfModerator =
            await teamContoller.fetchOrgMemberIdOfAMember(
              generalTeamId,
              moderatorEmail
            );
        }
      );

      await baseTeamTest.step(
        `Now OWNER invites ORGANIZER ${organiserEmail} into all the above 3 teams created`,
        async () => {
          const addMemberInMultipleTeams = {
            teamToBeAdd: [newTeamId1, newTeamId2],
            orgMemberId: orgMemberIdOfOrganiser,
          };
          await teamContoller.addOrDeleteMemberInMultipleTeams(
            addMemberInMultipleTeams
          );
        }
      );

      await baseTeamTest.step(`Expected data map`, async () => {
        expectedTeamListMap = new Map<string, string>();
        expectedTeamListMap.set(generalTeamId, "General");
        expectedTeamListMap.set(newTeamId1, team1.teamName);
        expectedTeamListMap.set(newTeamId2, team2.teamName);
      });

      await baseTeamTest.step(
        `ORGANIZER ${organiserEmail} verifies all the 3 teams have been successfully added via API `,
        async () => {
          organiserTeamController = new TeamController(
            organiserContextInfo.requestContext,
            organisationId
          );
          actualTeamListMap =
            await organiserTeamController.fetchListOfTeamsAssignedToAMember(
              organiserEmail
            );
          expect(
            await organiserTeamController.verifyTeamsDetails(
              expectedTeamListMap,
              actualTeamListMap
            )
          ).toBeTruthy();
        }
      );

      await baseTeamTest.step(
        `ORGANIZER verifies via UI the invisibility of Invite Members button`,
        async () => {
          organiserGeneralSettingsPage = new GeneralSettingsPage(
            organiserContextInfo.page
          );
          await organiserContextInfo.page.goto(ownerGeneralSettingsPageUrl);
          await organiserGeneralSettingsPage.verifiyInvisibilityOfInviteMembersButton();
        }
      );

      await baseTeamTest.step(
        `Now OWNER invites/add MODERATOR ${moderatorEmail} into the team ${team3.teamName} created`,
        async () => {
          const addMemberToTeamDetals = {
            teamId: newTeamId3,
            orgMemberIds: [orgMemberIdOfModerator],
          };
          await teamContoller.addNewMemberToTeam(addMemberToTeamDetals);
        }
      );

      await baseTeamTest.step(
        `Now added member will login to setup.zuddl via otp to verify his invitation`,
        async () => {
          const otpVerificationIdForNewMember =
            await userVerifyTeamInvitationUsingOtp(
              moderatorContextInfo.requestContext,
              moderatorEmail
            );
          //fetch otp by verification id
          const otpCodeForModerator =
            await QueryUtil.fetchLoginOTPByVerificationId(
              moderatorEmail,
              otpVerificationIdForNewMember
            );
          console.log(`fetched otp for moderator is ${otpCodeForModerator}`);
          //now moderator verify his otp
          await userVerifyOtpByApi(
            moderatorContextInfo.requestContext,
            otpVerificationIdForNewMember,
            otpCodeForModerator
          );
        }
      );

      await baseTeamTest.step(
        `MODERATOR verifies via UI the invisibility of Invite Members button`,
        async () => {
          moderatorGeneralSettingsPage = new GeneralSettingsPage(
            moderatorContextInfo.page
          );
          await moderatorContextInfo.page.goto(ownerGeneralSettingsPageUrl);
          await moderatorGeneralSettingsPage.verifiyInvisibilityOfInviteMembersButton();
        }
      );

      await baseTeamTest.step(
        `OWNER now deletes the MODERATOR ${moderatorEmail} from Organisation via API`,
        async () => {
          await organisationController.deleteMemberFromOrganisation(
            orgMemberIdOfModerator
          );
        }
      );

      await baseTeamTest.step(
        `OWNER then verifies the non presence of ${moderatorEmail} in the member listing page post deletion`,
        async () => {
          expect(
            await organisationController.isMemberPartOfThisOrganisation(
              moderatorEmail
            )
          ).toBeFalsy();
        }
      );

      await baseTeamTest.step(
        `OWNER again adds the same MODERATOR ${moderatorEmail} to the organisation and verify should be able to add again`,
        async () => {
          await organisationController.addNewMemberToOrganisation({
            firstName: "Moderator",
            lastName: "Automation",
            email: moderatorEmail,
            productRole: MemberRolesEnum.MODERATOR,
            myTeamList: [{ teamId: generalTeamId }],
          });
          orgMemberIdOfModerator =
            await teamContoller.fetchOrgMemberIdOfAMember(
              generalTeamId,
              moderatorEmail
            );
          expect(
            await organisationController.isMemberPartOfThisOrganisation(
              moderatorEmail
            )
          ).toBeTruthy();
        }
      );

      await baseTeamTest.step(
        `Now OWNER invites/add MODERATOR ${moderatorEmail} into the team ${team3.teamName} created`,
        async () => {
          const addMemberToTeamDetals = {
            teamId: newTeamId3,
            orgMemberIds: [orgMemberIdOfModerator],
          };
          await teamContoller.addNewMemberToTeam(addMemberToTeamDetals);
        }
      );

      await baseTeamTest.step(
        `Now added member will login to setup.zuddl via otp to verify his invitation`,
        async () => {
          const otpVerificationIdForNewMember =
            await userVerifyTeamInvitationUsingOtp(
              moderatorContextInfo.requestContext,
              moderatorEmail
            );
          //fetch otp by verification id
          const otpCodeForModerator =
            await QueryUtil.fetchLoginOTPByVerificationId(
              moderatorEmail,
              otpVerificationIdForNewMember
            );
          console.log(`fetched otp for moderator is ${otpCodeForModerator}`);
          //now moderator verify his otp
          await userVerifyOtpByApi(
            moderatorContextInfo.requestContext,
            otpVerificationIdForNewMember,
            otpCodeForModerator
          );
        }
      );

      await baseTeamTest.step(
        `OWNER verifies he is able to select team access and assign ${team3.teamName} team to ORGANIZER via UI`,
        async () => {
          await ownerPage.goto(ownerGeneralSettingsPageUrl);
          await ownerGeneralSettingsPage.openManageAccessComponent(
            organiserEmail
          );
          await ownerGeneralSettingsPage.selectOrDeselectTeamFromManageAccessComponent(
            [team3.teamName]
          );
        }
      );

      await baseTeamTest.step(
        `ORGANIZER verifies team ${team3.teamName} is present in UI`,
        async () => {
          await organiserContextInfo.page.reload();
          await organiserGeneralSettingsPage.openTeamList();
          await organiserGeneralSettingsPage.verifyTeamNamePresentInAList(
            team3.teamName
          );
        }
      );

      await baseTeamTest.step(
        `OWNER verifies he is able to de-select team access and un-assign ${team3.teamName} team to MODERATOR via UI`,
        async () => {
          // await ownerGeneralSettingsPage.searchMemberByEmail(moderatorEmail);

          await ownerGeneralSettingsPage.openManageAccessComponent(
            moderatorEmail
          );
          await ownerGeneralSettingsPage.selectOrDeselectTeamFromManageAccessComponent(
            [team3.teamName]
          );
        }
      );

      await baseTeamTest.step(
        `MODERATOR verifies team ${team3.teamName} is not present in UI`,
        async () => {
          await moderatorContextInfo.page.reload();
          await moderatorGeneralSettingsPage.openTeamList();
          await moderatorGeneralSettingsPage.verifyTeamNameNotPresentInAList(
            team3.teamName
          );
        }
      );
    }
  );

  baseTeamTest(
    `TC006: Verify if OWNER deletes a user from organisation, he is deleted from all the teams he was part of and that user do not have access to any of the team if he logs in`,
    async ({ organiserContextInfo, organiserEmail, attendeeContextInfo }) => {
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-378/verify-if-organisation-deletes-a-user-from-organisation-he-is-deleted",
      });
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          " https://linear.app/zuddl/issue/QAT-444/verify-user-does-not-loose-event-level-access-which-are-of-speaker",
      });
      let newTeamId1: string;
      let newTeamId2: string;
      let newTeamId3: string;
      let accountId: string;
      let eventId: any;
      let eventTitle: string;
      let orgMemberIdOfOrganiser: string;
      let attendeeMagicLink: string;
      let eventController: EventController;

      const team1 = {
        teamName: DataUtl.getRandomTeamName(),
        orgMemberIds: orgMemberIds,
      };
      const team2 = {
        teamName: DataUtl.getRandomTeamName(),
        orgMemberIds: orgMemberIds,
      };
      const team3 = {
        teamName: DataUtl.getRandomTeamName(),
        orgMemberIds: orgMemberIds,
      };

      await baseTeamTest.step(`Fetch default/general Team Id`, async () => {
        generalTeamId = await teamContoller.fetchGeneralTeamId();
      });

      await baseTeamTest.step(
        `OWNER creates a new team1 with name ${team1.teamName} via API`,
        async () => {
          await organisationController.createATeam(team1);
          newTeamId1 = await teamContoller.fetchTeamId(team1.teamName);
          console.log(
            `Created team1 ${team1.teamName} has teamId ${newTeamId1}`
          );
        }
      );

      await baseTeamTest.step(
        `OWNER creates a new team2 with name ${team2.teamName} via API`,
        async () => {
          await organisationController.createATeam(team2);
          newTeamId2 = await teamContoller.fetchTeamId(team2.teamName);
          console.log(
            `Created team2 ${team2.teamName} has teamId ${newTeamId2}`
          );
        }
      );

      await baseTeamTest.step(
        `OWNER creates a new team3 with name ${team3.teamName} via API`,
        async () => {
          await organisationController.createATeam(team3);
          newTeamId3 = await teamContoller.fetchTeamId(team3.teamName);
          console.log(
            `Created team3 ${team3.teamName} has teamId ${newTeamId3}`
          );
        }
      );

      await baseTeamTest.step(
        `Now, OWNER adds a new member with Product role as ORGANIZER and invite into the organisation`,
        async () => {
          await organisationController.addNewMemberToOrganisation({
            firstName: "Organizer",
            lastName: "Automation",
            email: organiserEmail,
            productRole: MemberRolesEnum.ORGANIZER,
            myTeamList: [{ teamId: generalTeamId }],
          });
          orgMemberIdOfOrganiser =
            await teamContoller.fetchOrgMemberIdOfAMember(
              generalTeamId,
              organiserEmail
            );
        }
      );

      await baseTeamTest.step(
        `Now added member will login to setup.zuddl via otp to verify his invitation`,
        async () => {
          const otpVerificationIdForNewMember =
            await userVerifyTeamInvitationUsingOtp(
              organiserContextInfo.requestContext,
              organiserEmail
            );
          //fetch otp by verification id
          const otpCodeForOrganiser =
            await QueryUtil.fetchLoginOTPByVerificationId(
              organiserEmail,
              otpVerificationIdForNewMember
            );
          console.log(`fetched otp for organiser is ${otpCodeForOrganiser}`);
          //now organiser verify his otp
          await userVerifyOtpByApi(
            organiserContextInfo.requestContext,
            otpVerificationIdForNewMember,
            otpCodeForOrganiser
          );
        }
      );

      await baseTeamTest.step(
        `Verify the ORGANIZER ${organiserEmail} status to be Active in the DB in that organisation`,
        async () => {
          accountId =
            await organisationController.fetchAccountIdOfMemberInOrganization(
              organiserEmail
            );
          expect(
            await QueryUtil.verifyIfUserPresentInOrganisation(
              organiserEmail,
              organisationId,
              accountId
            )
          ).toBeTruthy();

          expect(
            await organisationController.isMemberPartOfThisOrganisation(
              organiserEmail
            )
          ).toBeTruthy();
        }
      );

      await baseTeamTest.step(
        `Now OWNER invites ORGANIZER ${organiserEmail} into the above 2 teams created`,
        async () => {
          const addMemberInMultipleTeams = {
            teamToBeAdd: [newTeamId2, newTeamId3],
            orgMemberId: orgMemberIdOfOrganiser,
          };
          await teamContoller.addOrDeleteMemberInMultipleTeams(
            addMemberInMultipleTeams
          );
          console.log(
            `${organiserEmail} is successfully added in ${team2.teamName}, ${team3.teamName}, and ${generalTeamId}`
          );
        }
      );

      await baseTeamTest.step(
        `OWNER creates a new event with title ${eventTitle} from ${newTeamId1} team`,
        async () => {
          eventTitle = DataUtl.getRandomEventTitle();
          eventId = await EventController.generateNewEvent(ownerApiContext, {
            event_title: eventTitle,
            teamId: newTeamId1,
          });
          eventController = new EventController(ownerApiContext, eventId);
          await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
            false
          );
          await eventController.getAccessGroupController.handleOnboardingChecksForSpeaker(
            false
          );
        }
      );

      await baseTeamTest.step(
        `OWNER enables magic link for attendee for this event`,
        async () => {
          await updateEventLandingPageDetails(ownerApiContext, eventId, {
            eventEntryType: EventEntryType.REG_BASED,
            isMagicLinkEnabled: true,
            isSpeakermagicLinkEnabled: true,
          });
        }
      );

      await baseTeamTest.step(
        `OWNER adds the same user->${organiserEmail} with Event-role as ATTENDEE to the event ${eventTitle}`,
        async () => {
          await eventController.inviteAttendeeToTheEvent(organiserEmail);
        }
      );

      await baseTeamTest.step(
        `Attendee ${organiserEmail} goes to magic link and enters into the event and then goes to lobby`,
        async () => {
          attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
            eventId,
            organiserEmail
          );
          await attendeeContextInfo.page.goto(attendeeMagicLink);
          await attendeeContextInfo.page.click("text=Enter Now");
          await attendeeContextInfo.page.waitForURL(/lobby/);
          await attendeeContextInfo.page.click("text=Skip");
        }
      );

      await baseTeamTest.step(
        `OWNER now deletes ${organiserEmail} having product role as ORGANIZER from the Organisation`,
        async () => {
          await organisationController.deleteMemberFromOrganisation(
            orgMemberIdOfOrganiser
          );
        }
      );

      await baseTeamTest.step(
        `OWNER then verifies the non presence of ${organiserEmail} in the member listing page post deletion`,
        async () => {
          expect(
            await organisationController.isMemberPartOfThisOrganisation(
              organiserEmail
            )
          ).toBeFalsy();
        }
      );

      await baseTeamTest.step(
        `Verify the ORGANIZER ${organiserEmail} status to be InActive in the DB in that organisation`,
        async () => {
          expect(
            await QueryUtil.verifyIfUserAbsentInOrganisation(
              organiserEmail,
              organisationId,
              accountId
            )
          ).toBeTruthy();
        }
      );

      await baseTeamTest.step(
        `Verify ${organiserEmail} should still have event role as ATTENDEE and be able to access the event`,
        async () => {
          await attendeeContextInfo.page.reload();
          await attendeeContextInfo.page.waitForURL(/lobby/);
        }
      );
    }
  );

  baseTeamTest(
    `TC007: Verify flow of role upgrade from ATTENDEE in an Event to Product role as ORGANIZER and attendee should not appear in attendee list and role downgrade should display ATTENDEE in an event`,
    async ({ attendeeEmail, organiserContextInfo }) => {
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-385/upgrade-a-user-within-same-team-and-give-him-product-role",
      });
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-386/verify-when-a-user-is-removed-from-team-his-event-role-access-also",
      });

      let newTeamId1: string;
      let eventId: any;
      let eventTitle: string;
      let orgMemberIdOfOrganiser: string;
      let eventController: EventController;

      const team1 = {
        teamName: DataUtl.getRandomTeamName(),
        orgMemberIds: orgMemberIds,
      };

      await baseTeamTest.step(`Fetch default/general Team Id`, async () => {
        generalTeamId = await teamContoller.fetchGeneralTeamId();
      });

      await baseTeamTest.step(
        `OWNER creates a new team1 with name ${team1.teamName} via API`,
        async () => {
          await organisationController.createATeam(team1);
          newTeamId1 = await teamContoller.fetchTeamId(team1.teamName);
          console.log(
            `Created team1 ${team1.teamName} has teamId ${newTeamId1}`
          );
        }
      );

      await baseTeamTest.step(
        `OWNER creates a new event with title ${eventTitle} from ${newTeamId1} team`,
        async () => {
          eventTitle = DataUtl.getRandomEventTitle();
          eventId = await EventController.generateNewEvent(ownerApiContext, {
            event_title: eventTitle,
            teamId: newTeamId1,
          });
          eventController = new EventController(ownerApiContext, eventId);
          await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
            false
          );
          await eventController.getAccessGroupController.handleOnboardingChecksForSpeaker(
            false
          );
        }
      );

      await baseTeamTest.step(
        `OWNER adds the attendee ${attendeeEmail} with Event-role as ATTENDEE to the event ${eventTitle}`,
        async () => {
          await eventController.inviteAttendeeToTheEvent(attendeeEmail);
        }
      );

      await baseTeamTest.step(
        `OWNER verifies that the attendee ${attendeeEmail} is present in an event ${eventTitle} should be true via DB`,
        async () => {
          expect(
            await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
              attendeeEmail,
              eventId
            )
          ).toBeTruthy();
        }
      );

      await baseTeamTest.step(
        `OWNER now add/upgrade the same user ${attendeeEmail} into the team ${newTeamId1} as product-role ORGANIZER`,
        async () => {
          await organisationController.addNewMemberToOrganisation({
            firstName: "Attendee",
            lastName: "Automation",
            email: attendeeEmail,
            productRole: MemberRolesEnum.ORGANIZER,
            myTeamList: [{ teamId: generalTeamId }, { teamId: newTeamId1 }],
          });
          orgMemberIdOfOrganiser =
            await teamContoller.fetchOrgMemberIdOfAMember(
              generalTeamId,
              attendeeEmail
            );
        }
      );

      await baseTeamTest.step(
        `Now added member will login to setup.zuddl via otp to verify his invitation`,
        async () => {
          const otpVerificationIdForNewMember =
            await userVerifyTeamInvitationUsingOtp(
              organiserContextInfo.requestContext,
              attendeeEmail
            );
          //fetch otp by verification id
          const otpCodeForOrganiser =
            await QueryUtil.fetchLoginOTPByVerificationId(
              attendeeEmail,
              otpVerificationIdForNewMember
            );
          console.log(`fetched otp for organiser is ${otpCodeForOrganiser}`);
          //now organiser verify his otp
          await userVerifyOtpByApi(
            organiserContextInfo.requestContext,
            otpVerificationIdForNewMember,
            otpCodeForOrganiser
          );
        }
      );

      await baseTeamTest.step(
        `OWNER then verifies that the user->${attendeeEmail} is NOT present in an attendee event list, as the role is updated from ATTENDEE to ORGANIZER`,
        async () => {
          expect(
            await QueryUtil.verifyIfUserEmailIsNotActiveInEventRegTableForGivenEvent(
              attendeeEmail,
              eventId
            )
          ).toBeFalsy();
        }
      );

      await baseTeamTest.step(
        `OWNER now deletes ${attendeeEmail} having product role as ORGANIZER from the Organisation`,
        async () => {
          await organisationController.deleteMemberFromOrganisation(
            orgMemberIdOfOrganiser
          );
        }
      );

      await baseTeamTest.step(
        `OWNER then verifies that the user->${attendeeEmail} is present in an attendee event list via DB, as the role is downgraded from ORGANZIER to ATTENDEE`,
        async () => {
          expect(
            await QueryUtil.verifyUserEmailIsPresentInEventRegTableForGivenEvent(
              attendeeEmail,
              eventId
            )
          ).toBeTruthy();
        }
      );
    }
  );

  baseTeamTest(
    `TC008: verify when OWNER tries to add a new member to a team, he gets the dropdown with all organisation member present to select from`,
    async ({ organiserContextInfo, organiserEmail }) => {
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-445/verify-when-tries-to-add-a-new-member-to-a-team-he-gets-the-dropdown",
      });
      let newTeamId1: string;
      let ownerGeneralSettingsPage: GeneralSettingsPage;
      let ownerTeamMembersPage: TeamMembersPage;

      const team1 = {
        teamName: DataUtl.getRandomTeamName(),
        orgMemberIds: orgMemberIds,
      };

      await baseTeamTest.step(`Fetch default/general Team Id`, async () => {
        generalTeamId = await teamContoller.fetchGeneralTeamId();
      });

      await baseTeamTest.step(
        `OWNER creates a new team1 with name ${team1.teamName} via API`,
        async () => {
          await organisationController.createATeam(team1);
          newTeamId1 = await teamContoller.fetchTeamId(team1.teamName);
          console.log(
            `Created team1 ${team1.teamName} has teamId ${newTeamId1}`
          );
        }
      );

      await baseTeamTest.step(
        `Now, OWNER adds a new member with Product role as ORGANIZER and invite into the organisation`,
        async () => {
          await organisationController.addNewMemberToOrganisation({
            firstName: "Organizer",
            lastName: "Automation",
            email: organiserEmail,
            productRole: MemberRolesEnum.ORGANIZER,
            myTeamList: [{ teamId: generalTeamId }],
          });
        }
      );

      await baseTeamTest.step(
        `Now added member will login to setup.zuddl via otp to verify his invitation`,
        async () => {
          const otpVerificationIdForNewMember =
            await userVerifyTeamInvitationUsingOtp(
              organiserContextInfo.requestContext,
              organiserEmail
            );
          //fetch otp by verification id
          const otpCodeForOrganiser =
            await QueryUtil.fetchLoginOTPByVerificationId(
              organiserEmail,
              otpVerificationIdForNewMember
            );
          console.log(`fetched otp for organiser is ${otpCodeForOrganiser}`);
          //now organiser verify his otp
          await userVerifyOtpByApi(
            organiserContextInfo.requestContext,
            otpVerificationIdForNewMember,
            otpCodeForOrganiser
          );
        }
      );

      await baseTeamTest.step(
        `OWNER opens team members page and open Add to Team component`,
        async () => {
          ownerGeneralSettingsPage = new GeneralSettingsPage(ownerPage);
          ownerTeamMembersPage = new TeamMembersPage(ownerPage);
          await ownerPage.goto(ownerGeneralSettingsPageUrl, {
            waitUntil: "networkidle",
          });
          await ownerGeneralSettingsPage.selectATeam(team1.teamName);
          await ownerGeneralSettingsPage.openTeamMembersPage();
          await ownerTeamMembersPage.addToATeamButton();
        }
      );

      await baseTeamTest.step(
        `OWNER verifies the presence of memmber dropdown components and verify member should get added into the team`,
        async () => {
          await ownerTeamMembersPage.verifyAddMembersToTeamDropdownToBePresent();
          await ownerTeamMembersPage.addNewMemberToATeam(organiserEmail);
        }
      );
    }
  );

  baseTeamTest(
    `TC009: Verify e2e flow of event level access role`,
    async ({ organiserContextInfo, organiserEmail }) => {
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-384/create-2-teams-team-1-team-2-and-create-event-under-team-2-and-verify",
      });

      let newTeamId1: string;
      let newTeamId2: string;
      let eventId: any;
      let eventTitle: string;
      let eventController: EventController;

      const team1 = {
        teamName: DataUtl.getRandomTeamName(),
        orgMemberIds: orgMemberIds,
      };
      const team2 = {
        teamName: DataUtl.getRandomTeamName(),
        orgMemberIds: orgMemberIds,
      };

      await baseTeamTest.step(`Fetch default/general Team Id`, async () => {
        generalTeamId = await teamContoller.fetchGeneralTeamId();
      });

      await baseTeamTest.step(
        `OWNER creates a new team1 with name ${team1.teamName} via API`,
        async () => {
          await organisationController.createATeam(team1);
          newTeamId1 = await teamContoller.fetchTeamId(team1.teamName);
          console.log(
            `Created team1 ${team1.teamName} has teamId ${newTeamId1}`
          );
        }
      );

      await baseTeamTest.step(
        `Create a new member with Product role as ORGANIZER and add into newly created Team ${team1.teamName}`,
        async () => {
          organiserEmail = DataUtl.getRandomOrganiserEmail();
          await organisationController.addNewMemberToOrganisation({
            firstName: "Organizer",
            lastName: "Automation1",
            email: organiserEmail,
            productRole: MemberRolesEnum.ORGANIZER,
            myTeamList: [{ teamId: generalTeamId }, { teamId: newTeamId1 }],
          });
        }
      );

      await baseTeamTest.step(
        `Now added member will login to setup.zuddl via otp to verify his invitation`,
        async () => {
          const otpVerificationIdForNewMember =
            await userVerifyTeamInvitationUsingOtp(
              organiserContextInfo.requestContext,
              organiserEmail
            );
          //fetch otp by verification id
          const otpCodeForOrganiser =
            await QueryUtil.fetchLoginOTPByVerificationId(
              organiserEmail,
              otpVerificationIdForNewMember
            );
          console.log(`fetched otp for organiser is ${otpCodeForOrganiser}`);
          //now organiser verify his otp
          await userVerifyOtpByApi(
            organiserContextInfo.requestContext,
            otpVerificationIdForNewMember,
            otpCodeForOrganiser
          );
        }
      );

      await baseTeamTest.step(
        `OWNER creates a new team2 with name ${team2.teamName} via API`,
        async () => {
          await organisationController.createATeam(team2);
          newTeamId2 = await teamContoller.fetchTeamId(team2.teamName);
          console.log(
            `Created team2 ${team2.teamName} has teamId ${newTeamId2}`
          );
        }
      );

      await baseTeamTest.step(
        `OWNER creates a new event with title ${eventTitle} from ${team2.teamName} team`,
        async () => {
          eventTitle = DataUtl.getRandomEventTitle();
          eventId = await EventController.generateNewEvent(ownerApiContext, {
            event_title: eventTitle,
            teamId: newTeamId2,
          });
          eventController = new EventController(ownerApiContext, eventId);
          await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
            false
          );
          await eventController.getAccessGroupController.handleOnboardingChecksForSpeaker(
            false
          );
        }
      );

      await baseTeamTest.step(
        `OWNER adds the same user->${organiserEmail} with Event-role as SPEAKER to the event ${eventTitle} `,
        async () => {
          await eventController.inviteSpeakerToTheEventByApi(
            organiserEmail,
            "speakearName",
            "Automation"
          );
          console.log(
            `successfully added speaker with email->${organiserEmail} inside event under team ${newTeamId2}`
          );
        }
      );

      await baseTeamTest.step(
        `Verify user ${organiserEmail}  event role should be SPEAKER in the event present in team ${team2.teamName}`,
        async () => {
          await baseTeamTest.step(
            `OWNER verifies that the attendee ${organiserEmail} is present in an event ${eventTitle} should be true via DB`,
            async () => {
              expect(
                await QueryUtil.verifyUserEmailIsPresentInSpeakerTableForAnEvent(
                  organiserEmail,
                  eventId
                )
              ).toBeTruthy();
            }
          );
        }
      );

      await baseTeamTest.step(
        `Verify user ${organiserEmail} product role should be ORGANIZER in the team ${team1.teamName}`,
        async () => {
          expect(
            await teamContoller.fetchRoleOfAMember(
              newTeamId1,
              organiserEmail,
              "productRole"
            )
          ).toEqual(MemberRolesEnum.ORGANIZER);
        }
      );
    }
  );

  baseTeamTest(
    "TC010 : Verify when Admin access if removed from any user, no invite member option should be visible to that user",
    async () => {
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-571/when-admin-access-if-removed-from-any-user-no-invite-member-option",
      });
      await baseTeamTest.step(
        "Admin role is removed from the ADMIN by the OWNER",
        async () => {
          let ownerGeneralSettingsPage = new GeneralSettingsPage(ownerPage);
          await ownerPage.goto(ownerGeneralSettingsPageUrl);
          await ownerGeneralSettingsPage.removeFromAdmin();
        }
      );

      await baseTeamTest.step(
        "Verify Invite member button is not visible for the removed ADMIN page",
        async () => {
          let adminGeneralSettingsPage = new GeneralSettingsPage(adminPage);
          await adminPage.goto(ownerGeneralSettingsPageUrl);
          await adminGeneralSettingsPage.verifyInviteBtn();
        }
      );
    }
  );

  baseTeamTest(
    "TC011 : Verify when product role is changed from organiser to moderator, create event, webinar option should not be available to moderator",
    async () => {
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-569/when-product-role-is-changed-from-organiser-to-moderator-create-event",
      });
      await baseTeamTest.step(
        "Verify if product role changes to moderator then webinar, event and studio button gets disabled",
        async () => {
          let ownerGeneralSettingsPage = new GeneralSettingsPage(ownerPage);
          await ownerPage.goto(ownerGeneralSettingsPageUrl);
          await teamContoller.changeRoleFromOrganizerToModerator();
          await ownerPage.reload();
          await ownerGeneralSettingsPage.clickOnHomeButton();
          await ownerPage.locator("button[data-highlight='Dismiss']").click();
          await ownerGeneralSettingsPage.verifyWebinarButton();
          await ownerGeneralSettingsPage.verifyEventButton();
        }
      );
    }
  );

  baseTeamTest(
    "TC012 : Verify flow of owner is making somebody else as new owner",
    async () => {
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-570/flow-of-owner-is-making-somebody-else-as-new-owner",
      });
      await baseTeamTest.step(
        "Verify if owner make someone else as owner then access get removed using API",
        async () => {
          let ownerGeneralSettingsPage = new GeneralSettingsPage(ownerPage);
          await ownerPage.goto(ownerGeneralSettingsPageUrl);
          await ownerGeneralSettingsPage.makeOtherMemberAsOwner();
          await ownerPage.waitForTimeout(2000);
          expect(
            await teamContoller.verifyOwnerRoleChangesToAdmin()
          ).toBeTruthy();
        }
      );
    }
  );
});
