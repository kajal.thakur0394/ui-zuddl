import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import { OrganisationController } from "../../../controller/OrganisationController";
import BillngPeriodEnum from "../../../enums/billingPeriodEnum";
import PlanType from "../../../enums/planTypeEnum";
import ProductType from "../../../enums/productTypeEnum";
import {
  userVerifyTeamInvitationUsingOtp,
  userVerifyOtpByApi,
} from "../../../util/apiUtil";
import { TeamController } from "../../../controller/TeamsController";
import {
  EventLicenseDetailsDto,
  LicenseDetailsDto,
  MemberDetailsInOrganisation,
  WebinarLicenseDetailsDto,
} from "../../../interfaces/OrganisationControllerInterface";
import { GeneralSettingsPage } from "../../../page-objects/teams-page/GeneralSettingsPage";
import { TeamMembersPage } from "../../../page-objects/teams-page/TeamMembersPage";
import {
  MemberRolesCapitaliseEnum,
  MemberRolesEnum,
} from "../../../enums/MemberRolesEnum";
import { baseTeamTest } from "../../../fixtures/teamsandplans-fixtures/baseTeamFixture";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

baseTeamTest.describe("@organisation @teams", async () => {
  let organisationId: string;
  let generalTeamId: string;
  let ownerPage: Page;
  let adminPage: Page;
  let ownerApiContext: APIRequestContext;
  let adminApiContext: APIRequestContext;
  let organisationController: OrganisationController;
  let teamContoller: TeamController;
  let ownerEmail: string;
  let adminEmail: string;
  let organisationName: string;

  const defaultEventLicenseDetailsDto: EventLicenseDetailsDto = {
    planStartDate: new Date().toISOString(),
    planEndDate: new Date(
      new Date().setDate(new Date().getDate() + 3)
    ).toISOString(),
    teamSize: 10,
    cloudStorageSizeInGb: 2,
    noOfAttendees: 100,
    perEventLengthInHours: 500,
    enabledEventTypes: ["STANDARD"],
  };
  const defaultWebinarLicenseDetailsDto: WebinarLicenseDetailsDto = {
    planStartDate: new Date().toISOString(),
    planEndDate: new Date(
      new Date().setDate(new Date().getDate() + 3)
    ).toISOString(),
    noOfTeams: 10,
    cloudStorageSizeInGb: 2,
    noOfAttendees: 100,
    restrictionRenewPeriod: BillngPeriodEnum.MONTHLY,
    attendeesBufferPercentage: 20,
  };
  const defaultLicenseDetailDtoEvents: LicenseDetailsDto = {
    productType: ProductType.EVENT, //enum
    planType: PlanType.ATTENDEE_CUSTOM, //enum
    billingPeriodUnit: BillngPeriodEnum.MONTHLY,
    eventLicenseDetailsDto: defaultEventLicenseDetailsDto,
  };
  const defaultLicenseDetailDtoWebinar: LicenseDetailsDto = {
    productType: ProductType.WEBINAR, //enum
    planType: PlanType.CUSTOM, //enum
    billingPeriodUnit: BillngPeriodEnum.MONTHLY,
    webinarLicenseDetailsDto: defaultWebinarLicenseDetailsDto,
  };
  const ownerGeneralSettingsPageUrl = `${
    DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
  }/dashboard/team`;

  //Creation of new Random Organisation
  baseTeamTest.beforeAll(async ({ ownerContextInfo, adminContextInfo }) => {
    ownerPage = ownerContextInfo.page;
    adminPage = adminContextInfo.page;
    ownerApiContext = ownerContextInfo.requestContext;
    adminApiContext = adminContextInfo.requestContext;

    ownerEmail = DataUtl.getRandomOrganisationOwnerEmail();
    adminEmail = DataUtl.getRandomAdminEmail();
    organisationName = DataUtl.getRandomOrganisationName();

    const createOrganizationDetailsData = {
      firstName: "PrateeekAutomation",
      lastName: "OwnerQA",
      email: ownerEmail,
      organizationName: organisationName,
      licenseDetailsDto: defaultLicenseDetailDtoEvents,
    };

    // await baseTeamTest.step(`Creating new organisation having EVENT Plan with name : ${organisationName} and owner email : ${ownerEmail}`, async () => {
    //     organisationController = new OrganisationController(ownerApiContext);
    //     const createOrgApiResp = await organisationController.createNewOrganization(createOrganizationDetailsData);
    //     const createOrgApiRespJson = await createOrgApiResp.json();
    //     //getting organisation id which is needed for teams automation
    //     organisationId = createOrgApiRespJson["id"];
    //     console.log(
    //         `New organisation is created witd org id : ${organisationId}`
    //     );

    //     await baseTeamTest.step(`Adding WEBINAR Plan to the same organisation`, async () => {
    //         const addWebinarInOrganizationDetailsData = {
    //             organizationId: organisationId,
    //             licenseDetailsDto: defaultLicenseDetailDtoWebinar
    //         }
    //         const addWebinarPlanResp = await organisationController.renewPlanInOrganization(addWebinarInOrganizationDetailsData);
    //         console.log(`Organisation ${organisationName} now has WEBINAR Plan`);
    //     });
    //     teamContoller = new TeamController(ownerApiContext, organisationId);
    // });

    // await baseTeamTest.step(`Added member will login to setup.zuddl via otp to verify his invitation`, async () => {
    //     const otpVerificationIdForOwner = await userVerifyTeamInvitationUsingOtp(
    //         ownerPage.request,
    //         ownerEmail
    //     );
    //     console.log(`owner otp verification id is ${otpVerificationIdForOwner}`);
    //     //fetch otp for owner
    //     const otpCodeForOwner = await QueryUtil.fetchLoginOTPByVerificationId(
    //         ownerEmail,
    //         otpVerificationIdForOwner
    //     );
    //     //verify otp for owner
    //     console.log(`fetched otp for owner is ${otpCodeForOwner}`);
    //     await userVerifyOtpByApi(
    //         ownerPage.request,
    //         otpVerificationIdForOwner,
    //         otpCodeForOwner
    //     );
    // });
    await baseTeamTest.step(
      `Creating new organisation having EVENT Plan with name : ${organisationName} and owner email : ${ownerEmail}`,
      async () => {
        organisationController = new OrganisationController(
          ownerContextInfo.requestContext
        );
        const createOrgApiResp =
          await organisationController.createNewOrganization(
            createOrganizationDetailsData
          );
        const createOrgApiRespJson = await createOrgApiResp.json();
        organisationId = createOrgApiRespJson["id"];
        console.log(
          `New organisation is created witd org id : ${organisationId}`
        );
        teamContoller = new TeamController(
          ownerContextInfo.requestContext,
          organisationId
        );
      }
    );

    await baseTeamTest.step(
      `Adding WEBINAR Plan to the same organisation`,
      async () => {
        const addWebinarInOrganizationDetailsData = {
          organizationId: organisationId,
          licenseDetailsDto: defaultLicenseDetailDtoWebinar,
        };
        const addWebinarPlanResp =
          await organisationController.renewPlanInOrganization(
            addWebinarInOrganizationDetailsData
          );
        console.log(`Organisation ${organisationName} now has WEBINAR Plan`);
      }
    );

    await baseTeamTest.step(
      `Now added member will login to setup.zuddl via otp to verify his invitation`,
      async () => {
        const otpVerificationIdForOwner =
          await userVerifyTeamInvitationUsingOtp(ownerApiContext, ownerEmail);
        console.log(
          `owner otp verification id is ${otpVerificationIdForOwner}`
        );
        //fetch otp for owner
        const otpCodeForOwner = await QueryUtil.fetchLoginOTPByVerificationId(
          ownerEmail,
          otpVerificationIdForOwner
        );
        //verify otp for owner
        console.log(`fetched otp for owner is ${otpCodeForOwner}`);
        await userVerifyOtpByApi(
          ownerApiContext,
          otpVerificationIdForOwner,
          otpCodeForOwner
        );
      }
    );
  });

  baseTeamTest(
    "TC001 : Verify on creating new organisation, a default/general team is created for it and owner is appearing in member listing for General Team",
    async () => {
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-375/verify-on-creating-new-organisation-a-defaultgeneral-team-is-created",
      });
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-457/in-the-listing-page-owner-email-role-as-organiser-team-name-and-owner",
      });

      await baseTeamTest.step(`Fetch default/general Team Id`, async () => {
        generalTeamId = await teamContoller.fetchGeneralTeamId();
      });

      await baseTeamTest.step(
        `Verify organisation-role to be OWNER for email->${ownerEmail} in General Team`,
        async () => {
          expect(
            await teamContoller.fetchRoleOfAMember(
              generalTeamId,
              ownerEmail,
              "organizationRole"
            )
          ).toEqual(MemberRolesEnum.OWNER);
        }
      );

      await baseTeamTest.step(
        "Verify in the listing page, owner email, role as organiser, TEAM NAME, and owner label should be visible",
        async () => {
          let ownerGeneralSettingsPage = new GeneralSettingsPage(ownerPage);
          const expectedMemberDetails: MemberDetailsInOrganisation = {
            email: ownerEmail,
            productRole: MemberRolesCapitaliseEnum.ORGANIZER,
            myTeamNameList: ["General"],
            label: MemberRolesCapitaliseEnum.OWNER,
          };
          await ownerPage.goto(ownerGeneralSettingsPageUrl);
          const actualMemberDetails =
            await ownerGeneralSettingsPage.fetchMemberDetailsInOrganisation(
              ownerEmail
            );
          await ownerGeneralSettingsPage.VerifyMemberDetailsInOrgListingPage(
            expectedMemberDetails,
            actualMemberDetails
          );
        }
      );
    }
  );

  baseTeamTest(
    "TC002 : Verify on creating new organisation, Owner/Admin is able to add members with different product and organisation role in an organisation",
    async ({
      organiserContextInfo,
      organiserEmail,
      moderatorContextInfo,
      moderatorEmail,
    }) => {
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-458/verify-on-creating-new-organisation-owner-is-able-to-add-members-with",
      });
      await baseTeamTest.step(`Fetch default/general Team Id`, async () => {
        generalTeamId = await teamContoller.fetchGeneralTeamId();
      });

      await baseTeamTest.step(
        "Verify Addition of Member having organisation-role as ADMIN in General Team",
        async () => {
          const memberToAddDetails = {
            firstName: "Prateek",
            lastName: "Admin",
            email: adminEmail,
            productRole: MemberRolesEnum.ORGANIZER,
            myTeamList: [{ teamId: generalTeamId }],
          };

          await baseTeamTest.step(
            `Create new member with Product role as ORGANIZER and invite into General Team`,
            async () => {
              await organisationController.addNewMemberToOrganisation(
                memberToAddDetails
              );
            }
          );

          await baseTeamTest.step(
            `Now added member will login to setup.zuddl via otp to verify his invitation`,
            async () => {
              const otpVerificationIdForNewMember =
                await userVerifyTeamInvitationUsingOtp(
                  adminApiContext,
                  adminEmail
                );
              //fetch otp by verification id
              const otpCodeForOrganiser =
                await QueryUtil.fetchLoginOTPByVerificationId(
                  adminEmail,
                  otpVerificationIdForNewMember
                );
              console.log(
                `fetched otp for organiser is ${otpCodeForOrganiser}`
              );
              //now organiser verify his otp
              await userVerifyOtpByApi(
                adminApiContext,
                otpVerificationIdForNewMember,
                otpCodeForOrganiser
              );
            }
          );

          await baseTeamTest.step(
            `Now update the organisation-role of ${adminEmail} to ADMIN via API`,
            async () => {
              await teamContoller.updateMemberRoleInOrganization(
                generalTeamId,
                adminEmail,
                MemberRolesEnum.ADMIN
              );
            }
          );

          await baseTeamTest.step(
            `Verify organisation-role to be ADMIN for admin email->${adminEmail} in General Team`,
            async () => {
              expect(
                await teamContoller.fetchRoleOfAMember(
                  generalTeamId,
                  adminEmail,
                  "organizationRole"
                )
              ).toEqual(MemberRolesEnum.ADMIN);
            }
          );
        }
      );

      await baseTeamTest.step(
        "Verify Addition of Member having organisation-role as MEMBER and product-role as ORGANIZER in General Team",
        async () => {
          const memberToAddDetails = {
            firstName: "Prateek",
            lastName: "Organizer",
            email: organiserEmail,
            productRole: MemberRolesEnum.ORGANIZER,
            myTeamList: [{ teamId: generalTeamId }],
          };

          await baseTeamTest.step(
            `Create new member with Product role as ORGANIZER and invite into General Team`,
            async () => {
              await organisationController.addNewMemberToOrganisation(
                memberToAddDetails
              );
            }
          );

          await baseTeamTest.step(
            `Now added member will login to setup.zuddl via otp to verify his invitation`,
            async () => {
              const otpVerificationIdForNewMember =
                await userVerifyTeamInvitationUsingOtp(
                  organiserContextInfo.requestContext,
                  organiserEmail
                );
              //fetch otp by verification id
              const otpCodeForOrganiser =
                await QueryUtil.fetchLoginOTPByVerificationId(
                  organiserEmail,
                  otpVerificationIdForNewMember
                );
              console.log(
                `fetched otp for organiser is ${otpCodeForOrganiser}`
              );
              //now organiser verify his otp
              await userVerifyOtpByApi(
                organiserContextInfo.requestContext,
                otpVerificationIdForNewMember,
                otpCodeForOrganiser
              );
            }
          );

          await baseTeamTest.step(
            `Verify organisation-role to be MEMBER and product-role to be ORGANIZER for email->${organiserEmail} in General Team`,
            async () => {
              generalTeamId = await teamContoller.fetchGeneralTeamId();
              expect(
                await teamContoller.fetchRoleOfAMember(
                  generalTeamId,
                  organiserEmail,
                  "organizationRole"
                )
              ).toEqual(MemberRolesEnum.MEMBER);
              expect(
                await teamContoller.fetchRoleOfAMember(
                  generalTeamId,
                  organiserEmail,
                  "productRole"
                )
              ).toEqual(MemberRolesEnum.ORGANIZER);
            }
          );
        }
      );

      await baseTeamTest.step(
        "Verify Addition of Member having organisation-role as MEMBER and product-role as MODREATOR in General Team",
        async () => {
          const memberToAddDetails = {
            firstName: "Prateek",
            lastName: "Organizer",
            email: moderatorEmail,
            productRole: MemberRolesEnum.MODERATOR,
            myTeamList: [{ teamId: generalTeamId }],
          };

          await baseTeamTest.step(
            `Create new member with Product role as MODERATOR and invite into General Team`,
            async () => {
              await organisationController.addNewMemberToOrganisation(
                memberToAddDetails
              );
            }
          );

          await baseTeamTest.step(
            `Now added member will login to setup.zuddl via otp to verify his invitation`,
            async () => {
              const otpVerificationIdForNewMember =
                await userVerifyTeamInvitationUsingOtp(
                  moderatorContextInfo.requestContext,
                  moderatorEmail
                );
              //fetch otp by verification id
              const otpCodeForModerator =
                await QueryUtil.fetchLoginOTPByVerificationId(
                  moderatorEmail,
                  otpVerificationIdForNewMember
                );
              console.log(
                `fetched otp for moderator is ${otpCodeForModerator}`
              );
              //now moderator verify his otp
              await userVerifyOtpByApi(
                moderatorContextInfo.requestContext,
                otpVerificationIdForNewMember,
                otpCodeForModerator
              );
            }
          );

          await baseTeamTest.step(
            `Verify organisation-role to be MEMBER and product-role to be MODERATOR for email->${moderatorEmail} in General Team`,
            async () => {
              generalTeamId = await teamContoller.fetchGeneralTeamId();
              expect(
                await teamContoller.fetchRoleOfAMember(
                  generalTeamId,
                  moderatorEmail,
                  "organizationRole"
                )
              ).toEqual(MemberRolesEnum.MEMBER);
              expect(
                await teamContoller.fetchRoleOfAMember(
                  generalTeamId,
                  moderatorEmail,
                  "productRole"
                )
              ).toEqual(MemberRolesEnum.MODERATOR);
            }
          );
        }
      );
    }
  );

  baseTeamTest(
    "TC003 : Verify if organisation has Event Custom Plan, OWNER should be able to add multiple teams to the organisation",
    async () => {
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-376/verify-if-organisation-have-event-custom-plan-or-webinar-custom-plan",
      });
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-437/verify-owner-is-by-default-part-of-all-the-teams-being-created",
      });
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-438/verify-admin-if-any-should-also-get-added-by-default-to-new-teams",
      });
      baseTeamTest.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-380/verify-organiser-is-able-to-switch-between-different-teams",
      });

      let ownerGeneralSettingsPage = new GeneralSettingsPage(ownerPage);
      let ownerTeamMembersPage = new TeamMembersPage(ownerPage);
      const teamName1 = DataUtl.getRandomTeamName();
      let teamName2: string;
      let teamId2: string;
      let adminEmail1 = DataUtl.getRandomAdminEmail();

      await baseTeamTest.step(`Fetch default/general Team Id`, async () => {
        generalTeamId = await teamContoller.fetchGeneralTeamId();
      });

      await baseTeamTest.step(
        "Add a new member having organisation-role as ADMIN in an organisation",
        async () => {
          const memberToAddDetails = {
            firstName: "Prateek2",
            lastName: "Admin2",
            email: adminEmail1,
            productRole: MemberRolesEnum.ORGANIZER,
            myTeamList: [{ teamId: generalTeamId }],
          };

          await baseTeamTest.step(
            `Create new member with Product role as ORGANIZER and invite into General Team`,
            async () => {
              await organisationController.addNewMemberToOrganisation(
                memberToAddDetails
              );
            }
          );

          await baseTeamTest.step(
            `Now added member will login to setup.zuddl via otp to verify his invitation`,
            async () => {
              const otpVerificationIdForNewMember =
                await userVerifyTeamInvitationUsingOtp(
                  adminApiContext,
                  adminEmail1
                );
              //fetch otp by verification id
              const otpCodeForOrganiser =
                await QueryUtil.fetchLoginOTPByVerificationId(
                  adminEmail1,
                  otpVerificationIdForNewMember
                );
              console.log(
                `fetched otp for organiser is ${otpCodeForOrganiser}`
              );
              //now organiser verify his otp
              await userVerifyOtpByApi(
                adminApiContext,
                otpVerificationIdForNewMember,
                otpCodeForOrganiser
              );
            }
          );

          await baseTeamTest.step(
            `Now update the organisation-role of ${adminEmail1} to ADMIN via API`,
            async () => {
              await teamContoller.updateMemberRoleInOrganization(
                generalTeamId,
                adminEmail1,
                MemberRolesEnum.ADMIN
              );
            }
          );

          await baseTeamTest.step(
            `Verify organisation-role to be ADMIN for admin email->${adminEmail1} in General Team`,
            async () => {
              expect(
                await teamContoller.fetchRoleOfAMember(
                  generalTeamId,
                  adminEmail1,
                  "organizationRole"
                )
              ).toEqual(MemberRolesEnum.ADMIN);
            }
          );
        }
      );

      await baseTeamTest.step(`OWNER creates a new Team1 via UI`, async () => {
        await ownerPage.goto(ownerGeneralSettingsPageUrl);
        await ownerGeneralSettingsPage.createNewTeam(teamName1);
      });

      await baseTeamTest.step(
        `OWNER selects ${teamName1} and open team-members listing page`,
        async () => {
          await ownerGeneralSettingsPage.selectATeam(teamName1);
          await ownerGeneralSettingsPage.openTeamMembersPage();
        }
      );

      await baseTeamTest.step(
        `Verify OWNER and ADMIN's organisation-role users should get added by default`,
        async () => {
          let rolesToCheck = new Map<string, MemberRolesCapitaliseEnum>();
          rolesToCheck.set(ownerEmail, MemberRolesCapitaliseEnum.OWNER);
          rolesToCheck.set(adminEmail1, MemberRolesCapitaliseEnum.ADMIN);
          await ownerTeamMembersPage.verifyOrganizationRoleOfMember(
            rolesToCheck
          );
        }
      );

      await baseTeamTest.step(
        `OWNER again creates a new Team2 via UI`,
        async () => {
          teamName2 = DataUtl.getRandomTeamName();
          await ownerGeneralSettingsPage.createNewTeam(teamName2);
        }
      );

      await baseTeamTest.step(
        `Now, fetch the ${teamName2} teamId via API`,
        async () => {
          teamId2 = await teamContoller.fetchTeamId(teamName2);
        }
      );

      await baseTeamTest.step(
        `OWNER switches from ${teamName1} to ${teamName2} and open team-members listing page`,
        async () => {
          const teamMemberRequestPromise = ownerPage.waitForRequest(
            (request) =>
              request.url().includes("team-member-activity") &&
              request.method() === "POST",
            { timeout: 30000 }
          );
          await Promise.all([
            ownerGeneralSettingsPage.selectATeam(teamName2),
            teamMemberRequestPromise,
          ]);
          const teamMemberRequest = await teamMemberRequestPromise;
          const actualTeamId = teamMemberRequest.postData();

          await baseTeamTest.step(
            `Verify correct teamId is sent in API call`,
            async () => {
              expect(teamId2).toEqual(actualTeamId);
            }
          );

          await ownerGeneralSettingsPage.openTeamMembersPage();
        }
      );

      await baseTeamTest.step(
        `Verify OWNER and ADMIN's organisation-role users should get added by default`,
        async () => {
          let rolesToCheck = new Map<string, MemberRolesCapitaliseEnum>();
          rolesToCheck.set(ownerEmail, MemberRolesCapitaliseEnum.OWNER);
          rolesToCheck.set(adminEmail1, MemberRolesCapitaliseEnum.ADMIN);
          await ownerTeamMembersPage.verifyOrganizationRoleOfMember(
            rolesToCheck
          );
        }
      );
    }
  );
});
