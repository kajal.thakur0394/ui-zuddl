import test, {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
} from "@playwright/test";
import { UserController } from "../../../controller/UserController";
import EventEntryType from "../../../enums/eventEntryEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { EditProfilePage } from "../../../page-objects/events-pages/live-side-components/EditProfilePage";
import { EditProfileData } from "../../../test-data/editProfileForm";
import {
  createNewEvent,
  registerUserToEventbyApi,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import { LobbyPage } from "../../../page-objects/events-pages/zones/LobbyPage";
import { EventController } from "../../../controller/EventController";
import conditionalFieldsTestData from "../../../test-data/conditional-fields-testdata/createConditionalFields.json";
import EventRole from "../../../enums/eventRoleEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe
  .parallel("@editprofile @defaultfields @basicdetails", async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventTitle: string;
  let eventId: any;
  let orgPage: Page;
  let userInfoDTO: UserInfoDTO;
  let eventInfoDto: EventInfoDTO;
  let userEventRoleDto: UserEventRoleDto;
  let page: Page;

  test.beforeEach(async ({ context }, testInfo) => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgPage = await orgBrowserContext.newPage();
    orgApiContext = orgBrowserContext.request;

    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: eventTitle,
    });
    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });
    if (testInfo.title.includes("predefined")) {
      let eventController: EventController = new EventController(
        orgApiContext,
        eventId
      );
      let ConditionalFieldsTestData =
        conditionalFieldsTestData["predefined-edit-profile-data"];
      await eventController.addCustomFieldsWithConditionToEvent(
        ConditionalFieldsTestData
      );
    }
    page = await context.newPage();
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test("TC001 Filling Basic details with all mandatory and optional fields filled", async ({
    page,
  }) => {
    let attendeeEmail = DataUtl.getRandomAttendeeTestJoynEmail();
    let defaultFieldsData = new EditProfileData().DEFAULT_FIELDS_TC001;

    await test.step(`User ${attendeeEmail} getting registered by API`, async () => {
      await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
    });

    await test.step(`User fetch magic link from DB and load the event and open edit profile`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      let landingPageOne: LandingPageOne = new LandingPageOne(page);
      await landingPageOne.load(magicLink);
      await landingPageOne.clickOnEnterNowButton();
      await landingPageOne.isLobbyLoaded();
    });

    await test.step(`Open edit profile from lobby and fill up the fields as per the test data`, async () => {
      let lobby: LobbyPage = new LobbyPage(page);
      let editProfilePage: EditProfilePage = await (
        await lobby.getTopNavBarComponent()
      ).openEditProfile();
      await expect(
        editProfilePage.firstNameInputBox,
        "expecting edit profile form to be visible"
      ).toBeVisible();
      await page.waitForTimeout(3000);
      const profileUpdatePromise = page.waitForResponse(
        `${
          DataUtl.getApplicationTestDataObj()["apiBaseUrl"]
        }/api/account/profile/update?event_id=${eventId}`
      );
      await editProfilePage.fillUserProfileForm(defaultFieldsData);
      const response = await profileUpdatePromise;
      expect(
        response.status(),
        "expecting profile update API to return 200 status code"
      ).toBe(200);
    });

    await test.step(`Validating the profile view API data with the test date we have filled`, async () => {
      // await page.pause();
      let userController = new UserController(page.request);
      await userController.validateUserProfileViewData(defaultFieldsData);
    });
  });
  test("TC002 Filling Basic details with only mandatory fields filled", async ({
    page,
  }) => {
    let attendeeEmail = DataUtl.getRandomAttendeeTestJoynEmail();
    let defaultFieldsData = new EditProfileData().DEFAULT_FIELDS_TC002;

    await test.step(`User ${attendeeEmail} getting registered by API`, async () => {
      await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
    });

    await test.step(`User fetch magic link from DB and load the event and open edit profile`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      let landingPageOne: LandingPageOne = new LandingPageOne(page);
      await landingPageOne.load(magicLink);
      await landingPageOne.clickOnEnterNowButton();
      await landingPageOne.isLobbyLoaded();
    });

    await test.step(`Open edit profile from lobby and fill up the fields as per the test data`, async () => {
      let lobby = new LobbyPage(page);
      let editProfilePage: EditProfilePage = await (
        await lobby.getTopNavBarComponent()
      ).openEditProfile();
      await expect(
        editProfilePage.firstNameInputBox,
        "expecting edit profile form to be visible"
      ).toBeVisible();
      await expect(editProfilePage.lastNameInputBox).toHaveValue("test");
      const profileUpdatePromise = page.waitForResponse(
        `${
          DataUtl.getApplicationTestDataObj()["apiBaseUrl"]
        }/api/account/profile/update?event_id=${eventId}`
      );
      await editProfilePage.fillUserProfileForm(defaultFieldsData);
      const response = await profileUpdatePromise;
      expect(
        response.status(),
        "expecting profile update API to return 200 status code"
      ).toBe(200);
    });

    await test.step(`Validating the profile view API data with the test date we have filled`, async () => {
      // await page.pause();
      let userController = new UserController(page.request);
      await userController.validateUserProfileViewData(defaultFieldsData);
    });
  });

  test("TC003: Opening edit profile in predefined fields", async ({ page }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-236/case-2-crash-happening-on-click-of-edit-profile-inside-event",
    });
    let attendeeEmail = DataUtl.getRandomAttendeeTestJoynEmail();
    let defaultFieldsData = new EditProfileData().DEFAULT_FIELDS_TC002;

    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData.predefinedFieldsFormDataEditProfile,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
      }
    );

    await test.step("verify succesfully registered message visible.", async () => {
      await (
        await landingPageOne.getRegistrationConfirmationScreen()
      ).isRegistrationConfirmationMessageVisible(true);
    });
    await test.step(`User fetch magic link from DB and load the event and open edit profile`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      let landingPageOne: LandingPageOne = new LandingPageOne(page);
      await landingPageOne.load(magicLink);
      await landingPageOne.clickOnEnterNowButton();
      await landingPageOne.isLobbyLoaded();
    });

    await test.step(`Open edit profile from lobby and verify page is not crashing`, async () => {
      let lobby = new LobbyPage(page);
      let editProfilePage: EditProfilePage = await (
        await lobby.getTopNavBarComponent()
      ).openEditProfile();
      await expect(
        editProfilePage.firstNameInputBox,
        "expecting edit profile form to be visible"
      ).toBeVisible();
      await expect(editProfilePage.lastNameInputBox).toHaveValue("QA");
    });
  });
});
