import {
  APIRequestContext,
  BrowserContext,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { ExpoController } from "../../../controller/ExpoController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { ExpoPage } from "../../../page-objects/events-pages/zones/ExpoPage";
import {
  createNewEvent,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@expo @interaction panel`, async () => {
  test.slow();
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId;
  let attendeeoneBrowserContext: BrowserContext;
  let attendeetwoBrowserContext: BrowserContext;
  let eventController: EventController;
  let boothId: string;
  let expoController: ExpoController;
  let attendeeLandingPage: string;
  let boothURL: string;
  let expoUrl: string;
  let boothName: string;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiContext = orgBrowserContext.request;

    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: "moderated-expo chat automation",
    });
    console.log(`event id ${eventId}`);

    attendeeoneBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    attendeetwoBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });

    eventController = new EventController(orgApiContext, eventId);
    expoController = new ExpoController(orgApiContext, eventId);

    await test.step("create Expo Booth", async () => {
      boothName = "AnyBooth one";
      const boothCreateResponse =
        await expoController.createSingleBoothWithoutZone(boothName);
      console.log(`boothCreateResponse = ${boothCreateResponse}`);
      boothId = boothCreateResponse.get(boothName);
      console.log(`boothId  for boothName ${boothName} is ${boothId}`);
    });

    await test.step(`Enable the moderated chat for booth`, async () => {
      await expoController.updateBoothEngagementSettingsForThisBooth({
        hasChat: true,
        hasChatModerate: true,
      });
    });

    let eventInfoDto = new EventInfoDTO(eventId, "Expo Booth automation", true);
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
    expoUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/expo/`;
    boothURL = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/expo/${boothId}`;
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
    await attendeeoneBrowserContext?.close();
    await attendeetwoBrowserContext?.close();
  });

  test(`TC001: @moderated-chat verify e2e flow of attendee posting a chat message and organiser moderate it to approve it`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-499/qat-487-verify-organiser-only-sees-moderated-chat-button-and-attendees",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-500/qat-486-verify-organiser-is-able-to-enable-disable-moderated-chat",
    });

    let attendeeOnePage = await attendeeoneBrowserContext.newPage();
    let attendeeTwoPage = await attendeetwoBrowserContext.newPage();
    let organizerPage = await orgBrowserContext.newPage();
    let organizerBooth: ExpoPage;
    let attendeeOneBooth: ExpoPage;
    let attendeeTwoBooth: ExpoPage;

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "oneQA";

    const attendeeTwoFirstName = "prateekAttendeeTwo";
    const attendeeTwoLastName = "twoQA";

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let attendeeEmailTwo = DataUtl.getRandomAttendeeEmail();

    //messages
    const orgMessageOne = "Hi , I am organiser 1";
    const attendeeMessageOne = "Hi , I am attendee 1";

    await test.step(`organiser invites attendee  1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await test.step(`organiser invites attendee 2 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeTwoFirstName,
        lastName: attendeeTwoLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailTwo,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the expo booth`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      console.log(`boothurl for attendee 1 ${boothURL}`);
      await attendeeOnePage.goto(expoUrl);
      await attendeeOnePage.click("div[class^='styles-module__newBoothCard']");
      await attendeeOnePage.click("button[class^='styles-module__secondary']");
    });

    await test.step(`attendee 2 logs into the expobooth`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailTwo
      );
      await attendeeTwoPage.goto(magicLink);
      await attendeeTwoPage.click(`text=Enter Now`);
      await attendeeTwoPage.waitForURL(/lobby/);
      await attendeeTwoPage.goto(expoUrl);
      await attendeeTwoPage.click("div[class^='styles-module__newBoothCard']");
      await attendeeTwoPage.click("button[class^='styles-module__secondary']");
      await attendeeTwoPage.waitForLoadState();
    });

    await test.step(`organizer login to expo booth`, async () => {
      await organizerPage.goto(attendeeLandingPage);
      await organizerPage.click("text=Enter Now");
      await organizerPage.goto(expoUrl);
      await organizerPage.click("div[class^='styles-module__newBoothCard']");
      await organizerPage.waitForLoadState();
    });
    //attendee click on interaction panel
    await test.step(`Verify organiser messages does not go under moderation and are visible to all user present inside stage`, async () => {
      organizerBooth = new ExpoPage(organizerPage);
      await organizerBooth.clickOnChatIcon();
      await organizerBooth.getChatComponent.sendChatMessage(orgMessageOne);

      await test.step(`Verify organiser is able to see his message on chat panel`, async () => {
        await organizerBooth.getChatComponent.verifyMessageWithGivenContentIsVisible(
          orgMessageOne
        );
      });

      await test.step(`Verify attendee 1 is able to see his message on chat panel`, async () => {
        attendeeOneBooth = new ExpoPage(attendeeOnePage);
        await attendeeOneBooth.clickOnChatIcon();
        await attendeeOneBooth.getChatComponent.verifyMessageWithGivenContentIsVisible(
          orgMessageOne
        );
      });

      await test.step(`Verify attendee 2 is able to see his message on chat panel`, async () => {
        attendeeTwoBooth = new ExpoPage(attendeeTwoPage);
        await attendeeTwoBooth.clickOnChatIcon();
        await attendeeTwoBooth.getChatComponent.verifyMessageWithGivenContentIsVisible(
          orgMessageOne
        );
      });

      await test.step(`Verify attendee 1 if posts message is not visible to anyone inside chat container and is visible to org under moderation container`, async () => {
        await attendeeOneBooth.getChatComponent.sendChatMessage(
          attendeeMessageOne
        );
        await test.step(`Verify organiser is not able to see his message on chat panel`, async () => {
          await organizerBooth.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
            attendeeMessageOne
          );
        });
        await test.step(`Verify attendee 1 is not  able to see his message on chat panel`, async () => {
          await attendeeOneBooth.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
            attendeeMessageOne
          );
        });
        await test.step(`Verify attendee 2 is not able to see his message on chat panel`, async () => {
          await attendeeTwoBooth.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
            attendeeMessageOne
          );
        });

        await test.step(`verify organiser sees chat moderator button inside chat container`, async () => {
          await organizerBooth.getChatComponent.verifyModeratedChatButtonIsVisibleOnChatContainer();
        });

        await test.step(`verify attendees does not sees chat moderator button inside chat container`, async () => {
          await attendeeOneBooth.getChatComponent.verifyModeratedChatButtonIsNotVisibleOnChatContainer();
        });
      });

      await test.step(`Organiser clicks on moderated chat button, then approves the message, then verify its visible to all on chat container `, async () => {
        await test.step("organiser clicks on chat moderator button and verify moderated chat container is visible", async () => {
          await organizerBooth.getChatComponent.updateStatusOfChatWaitingForModeration(
            attendeeMessageOne,
            true
          );
        });
      });

      await test.step(`Now verify post approval by organiser, chat message is visible in the chat container to all user`, async () => {
        await test.step(`Verify organiser is now able to see attendee 1 message on chat panel`, async () => {
          await organizerBooth.getChatComponent.verifyMessageWithGivenContentIsVisible(
            attendeeMessageOne
          );
        });
        await test.step(`Verify attendee 1 is now  able to see his message on chat panel`, async () => {
          await attendeeOneBooth.getChatComponent.verifyMessageWithGivenContentIsVisible(
            attendeeMessageOne
          );
        });
        await test.step(`Verify attendee 2 is now  able to see attendee 1 message on chat panel`, async () => {
          await attendeeTwoBooth.getChatComponent.verifyMessageWithGivenContentIsVisible(
            attendeeMessageOne
          );
        });
      });
    });
  });

  test(`TC002: @moderated-chat verify e2e flow of attendee posting a chat message and organiser moderate it to reject it `, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-499/qat-487-verify-organiser-only-sees-moderated-chat-button-and-attendees, https://linear.app/zuddl/issue/QAT-500/qat-486-verify-organiser-is-able-to-enable-disable-moderated-chat",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-500/qat-486-verify-organiser-is-able-to-enable-disable-moderated-chat",
    });
    let attendeeOnePage = await attendeeoneBrowserContext.newPage();
    let attendeeTwoPage = await attendeetwoBrowserContext.newPage();
    let organizerPage = await orgBrowserContext.newPage();
    let organizerBooth: ExpoPage;
    let attendeeOneBooth: ExpoPage;
    let attendeeTwoBooth: ExpoPage;

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "oneQA";

    const attendeeTwoFirstName = "prateekAttendeeTwo";
    const attendeeTwoLastName = "twoQA";

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let attendeeEmailTwo = DataUtl.getRandomAttendeeEmail();

    //messages
    const orgMessageOne = "Hi , I am organiser 1";
    const attendeeMessageOne = "Hi , I am attendee 1";

    await test.step(`organiser invites attendee  1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await test.step(`organiser invites attendee 2 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeTwoFirstName,
        lastName: attendeeTwoLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailTwo,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the expo booth`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      console.log(`boothurl for attendee 1 ${boothURL}`);
      await attendeeOnePage.goto(expoUrl);
      await attendeeOnePage.click("div[class^='styles-module__newBoothCard']");
      await attendeeOnePage.click("button[class^='styles-module__secondary']");
    });

    await test.step(`attendee 2 logs into the expo booth`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailTwo
      );
      await attendeeTwoPage.goto(magicLink);
      await attendeeTwoPage.click(`text=Enter Now`);
      await attendeeTwoPage.waitForURL(/lobby/);
      await attendeeTwoPage.goto(expoUrl);
      await attendeeTwoPage.click("div[class^='styles-module__newBoothCard']");
      await attendeeTwoPage.click("button[class^='styles-module__secondary']");
      await attendeeTwoPage.waitForLoadState();
    });

    await test.step(`organizer login to expo booth`, async () => {
      await organizerPage.goto(attendeeLandingPage);
      await organizerPage.click("text=Enter Now");
      await organizerPage.goto(expoUrl);
      await organizerPage.click("div[class^='styles-module__newBoothCard']");
      await organizerPage.waitForLoadState();
    });
    //attendee login in expo booth
    await test.step(`Verify organiser messages does not go under moderation and are visible to all user present inside stage`, async () => {
      organizerBooth = new ExpoPage(organizerPage);
      await organizerBooth.clickOnChatIcon();
      await organizerBooth.getChatComponent.sendChatMessage(orgMessageOne);

      await test.step(`Verify organiser is able to see his message on chat panel`, async () => {
        await organizerBooth.getChatComponent.verifyMessageWithGivenContentIsVisible(
          orgMessageOne
        );
      });

      await test.step(`Verify attendee 1 is able to see his message on chat panel`, async () => {
        attendeeOneBooth = new ExpoPage(attendeeOnePage);
        await attendeeOneBooth.clickOnChatIcon();
        await attendeeOneBooth.getChatComponent.verifyMessageWithGivenContentIsVisible(
          orgMessageOne
        );
      });

      await test.step(`Verify attendee 2 is able to see his message on chat panel`, async () => {
        attendeeTwoBooth = new ExpoPage(attendeeTwoPage);
        await attendeeTwoBooth.clickOnChatIcon();
        await attendeeTwoBooth.getChatComponent.verifyMessageWithGivenContentIsVisible(
          orgMessageOne
        );
      });
    });

    await test.step(`Verify attendee 1 if posts message is not visible to anyone inside chat container and is visible to org under moderation container`, async () => {
      await attendeeOneBooth.getChatComponent.sendChatMessage(
        attendeeMessageOne
      );
      await test.step(`Verify organiser is not able to see his message on chat panel`, async () => {
        await organizerBooth.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });
      await test.step(`Verify attendee 1 is not  able to see his message on chat panel`, async () => {
        await attendeeOneBooth.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });
      await test.step(`Verify attendee 2 is not able to see his message on chat panel`, async () => {
        await attendeeTwoBooth.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });

      await test.step(`verify organiser sees chat moderator button inside chat container`, async () => {
        await organizerBooth.getChatComponent.verifyModeratedChatButtonIsVisibleOnChatContainer();
      });

      await test.step(`verify attendees does not sees chat moderator button inside chat container`, async () => {
        await attendeeOneBooth.getChatComponent.verifyModeratedChatButtonIsNotVisibleOnChatContainer();
      });
    });

    await test.step(`Organiser clicks on moderated chat button, then rejects the message, then verify its not  visible to all on chat container`, async () => {
      await organizerBooth.getChatComponent.updateStatusOfChatWaitingForModeration(
        attendeeMessageOne,
        false
      );
    });

    await test.step(`Now verify post rejection by organiser, chat message is not visible in the chat container to all user`, async () => {
      await test.step(`Verify organiser is not able to see attendee 1 message on chat panel`, async () => {
        await organizerBooth.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });
      await test.step(`Verify attendee 1 is not  able to see his message on chat panel`, async () => {
        await attendeeOneBooth.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });
      await test.step(`Verify attendee 2 is not  able to see attendee 1 message on chat panel`, async () => {
        await attendeeTwoBooth.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });
    });
  });
});
