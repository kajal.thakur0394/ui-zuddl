import { test, BrowserContext, APIRequestContext } from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import {
  createNewEvent,
  getEventDetails,
  enableMagicLinkInEvent,
  inviteAttendeeByAPI,
  updateLandingPageType,
} from "../../../util/apiUtil";
import { fetchAttendeeInviteMagicLink } from "../../../util/emailUtil";
import { LandingPageTwo } from "../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import EventRole from "../../../enums/eventRoleEnum";
import { DataUtl } from "../../../util/dataUtil";
import { convertInAnotherTimeZone } from "../../../util/commonUtil";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";
import LandingPageType from "../../../enums/landingPageEnum";

test.describe
  .parallel("Time Zone Test suite|Invite Based|Magic link enabled | Attendee Landing Page| @landing-page-2", () => {
  let org_browser_context: BrowserContext;
  let attendeeContext: BrowserContext;
  let attendee_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let attendeeEmail: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let landingPageTwo: LandingPageTwo;
  let eventEndDateinAttendeeTimezone;
  let eventStartDateinAttendeeTimezone;

  test.afterEach(async () => {
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
    await attendeeContext?.close();
  });

  test('TC001: Verify attendee able to login from Timezone other than the created event Timezone, @Event Created Timezone: "Pacific/Auckland" , @Attendee timezone:  "Asia/Kolkata"| @landing-page-2', async ({}) => {
    // setup org context
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
      timeZoneId: "Pacific/Auckland",
    });
    organiserApiContext = await org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title: event_title,
      default_settings: true,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    // Updating event landing page type and entry type to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "true",
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;

    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    attendeeContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    let page = await attendeeContext.newPage();
    landingPageTwo = new LandingPageTwo(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    // invite this user to this event as an attendee
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    await landingPageTwo.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageTwo.getRegistrationFormComponent()).isNotVisible();
    // now click on already registered
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    // now enter your email
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForMagicLink(userEventRoleDto);
    await landingPageTwo.isSuccessToastMessageVisible();
    let magicLink = await fetchAttendeeInviteMagicLink(
      attendeeEmail,
      eventInfoDto.registrationConfirmationEmailSubj
    );
    await landingPageTwo.load(magicLink);
    await landingPageTwo.isLobbyLoaded();
  });

  test('TC002: If the event timer is displaying  Date and Time correctly and According to Attendee Timezone. @attendee TimeZone: Pacific/Auckland @event Time zone:"Asia/Kolkata| @landing-page-2', async ({}) => {
    // setup org context
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    organiserApiContext = await org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    // Creating same day event
    let api_request_context = organiserApiContext;
    const eventCreatedTime = new Date();
    console.log("Event created time: ", eventCreatedTime);
    event_id = await createNewEvent({
      api_request_context,
      event_title,
      sameDayEvent: true,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    // Updating event landing page type and entry type to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "true",
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;

    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    let attendeeTimezone = "Pacific/Auckland";
    attendeeContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      timeZoneId: attendeeTimezone,
    });
    let page = await attendeeContext.newPage();
    landingPageTwo = new LandingPageTwo(page);
    await landingPageTwo.load(attendee_landing_page);
    eventStartDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventCreatedTime,
      attendeeTimezone
    ).toString();
    eventEndDateinAttendeeTimezone = eventCreatedTime;
    eventEndDateinAttendeeTimezone = eventEndDateinAttendeeTimezone.setHours(
      eventEndDateinAttendeeTimezone.getHours() + 6
    );
    eventEndDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventEndDateinAttendeeTimezone,
      attendeeTimezone
    ).toString();
    console.log(
      "Event satrt date and end date" + eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
    console.log("Event created time: ", eventCreatedTime);
    /*
           If the start date is not same as the end date, it will display start date - end date, but if they are same it will display 
            the time of the event in hours and minutes. So we are checking in which case the event lies by comparing event start date and
            event end dates. 
           */
    await (
      await landingPageTwo.getDateTimeCounterComponent()
    ).isCorrectDatetimeDisplayed(
      eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
  });

  test('TC003: Verify event start date and end date are changing according to Attendee timezone for multiple location @attendee timezones = "Asia/Kolkata","America/New York", @event timeZone="Pacific/Auckland"|@landing-page-2 ', async ({}) => {
    // setup org context
    let organiserTimezone = "Pacific/Auckland";
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
      timeZoneId: organiserTimezone,
    });
    organiserApiContext = await org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    // Creating same day event
    let api_request_context = organiserApiContext;
    const eventCreatedTime = new Date();
    const eventCreatedTime1 = new Date();
    event_id = await createNewEvent({
      api_request_context,
      event_title,
      sameDayEvent: true,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    // Updating event landing page type and entry type to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "true",
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;

    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    let attendeeTimezone = "Pacific/Auckland";
    attendeeContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      timeZoneId: attendeeTimezone,
    });
    let page = await attendeeContext.newPage();
    landingPageTwo = new LandingPageTwo(page);
    await landingPageTwo.load(attendee_landing_page);
    eventStartDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventCreatedTime,
      attendeeTimezone
    ).toString();
    eventEndDateinAttendeeTimezone = eventCreatedTime;
    eventEndDateinAttendeeTimezone = eventEndDateinAttendeeTimezone.setHours(
      eventEndDateinAttendeeTimezone.getHours() + 6
    );
    eventEndDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventEndDateinAttendeeTimezone,
      attendeeTimezone
    ).toString();
    console.log(
      "Event satrt date and end date" + eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
    console.log("Event created time: ", eventCreatedTime);
    /*
           If the start date is not same as the end date, it will display start date - end date, but if they are same it will display 
            the time of the event in hours and minutes. So we are checking in which case the event lies by comparing event start date and
            event end dates. 
           */
    await (
      await landingPageTwo.getDateTimeCounterComponent()
    ).isCorrectDatetimeDisplayed(
      eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
    // Changing the attendee Timezone foor the same event to verify if it is displaying correct date time counter for different timezone.

    attendeeTimezone = "America/New_York";
    attendeeContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      timeZoneId: attendeeTimezone,
    });
    page = await attendeeContext.newPage();
    landingPageTwo = new LandingPageTwo(page);
    await landingPageTwo.load(attendee_landing_page);
    eventStartDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventCreatedTime1,
      attendeeTimezone
    ).toString();
    eventEndDateinAttendeeTimezone = eventCreatedTime1;
    eventEndDateinAttendeeTimezone = eventEndDateinAttendeeTimezone.setHours(
      eventEndDateinAttendeeTimezone.getHours() + 6
    );
    eventEndDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventEndDateinAttendeeTimezone,
      attendeeTimezone
    ).toString();
    await (
      await landingPageTwo.getDateTimeCounterComponent()
    ).isCorrectDatetimeDisplayed(
      eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
  });
});
