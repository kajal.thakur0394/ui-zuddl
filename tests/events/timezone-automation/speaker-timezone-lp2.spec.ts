import { test, BrowserContext, APIRequestContext } from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import {
  createNewEvent,
  getEventDetails,
  inviteSpeakerByApi,
  updateLandingPageType,
} from "../../../util/apiUtil";
import { LandingPageTwo } from "../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import EventRole from "../../../enums/eventRoleEnum";
import { DataUtl } from "../../../util/dataUtil";
import { convertInAnotherTimeZone } from "../../../util/commonUtil";
import { SpeakerData } from "../../../test-data/speakerData";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";
import LandingPageType from "../../../enums/landingPageEnum";

test.describe
  .parallel("Time Zone Test suite|Invite Based|Magic link enabled| Speaker Landing Page|@landing-page-2", () => {
  let org_browser_context: BrowserContext;
  let attendee_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let event_time: string;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let userDefaultRegFieldData: object;
  let attendeeEmail: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let landingPageTwo: LandingPageTwo;
  let eventStartDateinAttendeeTimezone;
  let eventEndDateinAttendeeTimezone;
  let speakerContext: BrowserContext;

  test.afterEach(async () => {
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
    await speakerContext?.close();
    await attendee_browser_context?.close();
  });

  test('TC001: Verify speaker able to login from Timezone other than the created event Timezone, @Event Created Timezone: "Pacific/Auckland" , @Speaker timezone:  "Asia/Kolkata"|@landing-page-2', async ({}) => {
    // setup org context
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
      timeZoneId: "Pacific/Auckland",
    });
    organiserApiContext = await org_browser_context.request;
    // org_session = await org_browser_context.newPage()
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: true,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    // Updating event landing page type and entry type to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "true",
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;

    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );
    let speakerContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    let page = await speakerContext.newPage();
    landingPageTwo = new LandingPageTwo(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    ); // true denote to send email
    await landingPageTwo.load(speaker_landing_page);
    await landingPageTwo.clickOnLoginButton();
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER
    );
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
  });

  test('TC002: If the event timer is displaying  Date and Time correctly and According to Speaker Timezone. @Event created TimeZone: Pacific/Auckland @speaker Time zone:"Asia/Kolkata"|@landing-page-2', async ({}) => {
    // setup org context
    let organiserTimezone = "Pacific/Auckland";
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
      timeZoneId: organiserTimezone,
    });
    organiserApiContext = await org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    // Creating same day event
    const eventCreatedTime = new Date();
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      sameDayEvent: true,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    // Updating event landing page type and entry type to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "true",
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;

    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );
    let speakerTimezone = "Asia/Kolkata";
    speakerContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      timeZoneId: speakerTimezone,
    });
    let page = await speakerContext.newPage();
    landingPageTwo = new LandingPageTwo(page);
    await landingPageTwo.load(speaker_landing_page);
    eventStartDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventCreatedTime,
      speakerTimezone
    ).toString();
    eventEndDateinAttendeeTimezone = eventCreatedTime;
    eventEndDateinAttendeeTimezone = eventEndDateinAttendeeTimezone.setHours(
      eventEndDateinAttendeeTimezone.getHours() + 6
    );
    eventEndDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventEndDateinAttendeeTimezone,
      speakerTimezone
    ).toString();
    console.log(
      "Event satrt date and end date" + eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
    console.log("Event created time: ", eventCreatedTime);
    /*
         If the start date is not same as the end date, it will display start date - end date, but if they are same it will display 
          the time of the event in hours and minutes. So we are checking in which case the event lies by comparing event start date and
          event end dates. 
         */
    await (
      await landingPageTwo.getDateTimeCounterComponent()
    ).isCorrectDatetimeDisplayed(
      eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
  });

  test('TC003: Verify event start date and end date are changing according to Speaker timezone for multiple location @attendee timezones = "Asia/Kolkata","America/New York", @event timeZone="Pacific/Auckland"|@landing-page-2', async ({}) => {
    // setup org context
    let organiserTimezone = "Pacific/Auckland";
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
      timeZoneId: organiserTimezone,
    });
    organiserApiContext = await org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    // Creating same day event
    const eventCreatedTime = new Date();
    const eventCreatedTime1 = new Date();
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      sameDayEvent: true,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    // Updating event landing page type and entry type to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "true",
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;

    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );
    let speakerTimezone = "Asia/Kolkata";
    speakerContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      timeZoneId: speakerTimezone,
    });
    let page = await speakerContext.newPage();
    landingPageTwo = new LandingPageTwo(page);
    await landingPageTwo.load(speaker_landing_page);
    eventStartDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventCreatedTime,
      speakerTimezone
    ).toString();
    eventEndDateinAttendeeTimezone = eventCreatedTime;
    eventEndDateinAttendeeTimezone = eventEndDateinAttendeeTimezone.setHours(
      eventEndDateinAttendeeTimezone.getHours() + 6
    );
    eventEndDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventEndDateinAttendeeTimezone,
      speakerTimezone
    ).toString();
    console.log(
      "Event satrt date and end date" + eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
    console.log("Event created time: ", eventCreatedTime);
    /*
         If the start date is not same as the end date, it will display start date - end date, but if they are same it will display 
          the time of the event in hours and minutes. So we are checking in which case the event lies by comparing event start date and
          event end dates. 
         */
    await (
      await landingPageTwo.getDateTimeCounterComponent()
    ).isCorrectDatetimeDisplayed(
      eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
    // Changing the speaker Timezone
    speakerTimezone = "America/New_York";
    speakerContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      timeZoneId: speakerTimezone,
    });
    page = await speakerContext.newPage();
    landingPageTwo = new LandingPageTwo(page);
    await landingPageTwo.load(attendee_landing_page);
    eventStartDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventCreatedTime1,
      speakerTimezone
    ).toString();
    eventEndDateinAttendeeTimezone = eventCreatedTime1;
    eventEndDateinAttendeeTimezone = eventEndDateinAttendeeTimezone.setHours(
      eventEndDateinAttendeeTimezone.getHours() + 6
    );
    eventEndDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventEndDateinAttendeeTimezone,
      speakerTimezone
    ).toString();
    console.log(
      "Event satrt date and end date" + eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
    console.log("Event created time: ", eventCreatedTime1);
    await (
      await landingPageTwo.getDateTimeCounterComponent()
    ).isCorrectDatetimeDisplayed(
      eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
  });
});
