import { test, BrowserContext, APIRequestContext } from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import {
  createNewEvent,
  getEventDetails,
  enableMagicLinkInEvent,
  inviteAttendeeByAPI,
} from "../../../util/apiUtil";
import { fetchAttendeeInviteMagicLink } from "../../../util/emailUtil";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import EventRole from "../../../enums/eventRoleEnum";
import { DataUtl } from "../../../util/dataUtil";
import { convertInAnotherTimeZone } from "../../../util/commonUtil";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";

test.describe
  .parallel("Time Zone Test suite|Invite Based|Magic link enabled | Attendee Landing Page", () => {
  let org_browser_context: BrowserContext;
  let attendeeContext: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let event_time: string;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let userDefaultRegFieldData: object;
  let attendeeEmail: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let landingPageOne: LandingPageOne;
  let eventEndDateinAttendeeTimezone;
  let eventStartDateinAttendeeTimezone;

  test.afterEach(async () => {
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
    await attendeeContext?.close();
  });

  test('TC001: Verify attendee able to login from Timezone other than the created event Timezone, @Event Created Timezone: "Pacific/Auckland" , @Attendee timezone:  "Asia/Kolkata"', async ({}) => {
    // setup org context

    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
      timeZoneId: "Pacific/Auckland",
    });
    organiserApiContext = org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title: event_title,
      default_settings: true,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;

    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    attendeeContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    let page = await attendeeContext.newPage();
    landingPageOne = new LandingPageOne(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    // invite this user to this event as an attendee
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    await landingPageOne.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageOne.getRegistrationFormComponent()).isNotVisible();
    // now click on already registered
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    // now enter your email
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForMagicLink(userEventRoleDto);
    await landingPageOne.isSuccessToastMessageVisible();
    let magicLink = await fetchAttendeeInviteMagicLink(
      attendeeEmail,
      eventInfoDto.registrationConfirmationEmailSubj
    );
    await landingPageOne.load(magicLink);
    await landingPageOne.isLobbyLoaded();
  });

  test('TC002: If the event timer is displaying  Date and Time correctly and According to Attendee Timezone. @attendee TimeZone: Pacific/Auckland @event Time zone:"Asia/Kolkata', async ({}) => {
    // setup org context
    let organiserTimezone = "Asia/Kolkata";
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    organiserApiContext = org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    // Creating same day event
    let api_request_context = organiserApiContext;
    const eventCreatedTime = new Date();
    console.log("Event created time: ", eventCreatedTime);
    event_id = await createNewEvent({
      api_request_context,
      event_title,
      sameDayEvent: true,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;

    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    let attendeeTimezone = "Pacific/Auckland";
    attendeeContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      timeZoneId: attendeeTimezone,
    });
    let page = await attendeeContext.newPage();
    landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendee_landing_page);
    eventStartDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventCreatedTime,
      attendeeTimezone
    ).toString();
    eventEndDateinAttendeeTimezone = eventCreatedTime;
    eventEndDateinAttendeeTimezone = eventEndDateinAttendeeTimezone.setHours(
      eventEndDateinAttendeeTimezone.getHours() + 6
    );
    eventEndDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventEndDateinAttendeeTimezone,
      attendeeTimezone
    ).toString();
    console.log(
      "Event satrt date and end date" + eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
    console.log("Event created time: ", eventCreatedTime);
    /*
       If the start date is not same as the end date, it will display start date - end date, but if they are same it will display 
        the time of the event in hours and minutes. So we are checking in which case the event lies by comparing event start date and
        event end dates. 
       */
    await (
      await landingPageOne.getDateTimeCounterComponent()
    ).isCorrectDatetimeDisplayed(
      eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
  });

  test('TC003: Verify event start date and end date are changing according to Attendee timezone for multiple location @attendee timezones = "Asia/Kolkata","America/New York", @event timeZone="Pacific/Auckland" ', async ({}) => {
    // setup org context
    let organiserTimezone = "Pacific/Auckland";
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
      timeZoneId: organiserTimezone,
    });
    organiserApiContext = org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    // Creating same day event
    let api_request_context = organiserApiContext;
    const eventCreatedTime = new Date();
    const eventCreatedTime1 = new Date();
    event_id = await createNewEvent({
      api_request_context,
      event_title,
      sameDayEvent: true,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;

    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    let attendeeTimezone = "Pacific/Auckland";
    attendeeContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      timeZoneId: attendeeTimezone,
    });
    let page = await attendeeContext.newPage();
    landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendee_landing_page);
    eventStartDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventCreatedTime,
      attendeeTimezone
    ).toString();
    eventEndDateinAttendeeTimezone = eventCreatedTime;
    eventEndDateinAttendeeTimezone = eventEndDateinAttendeeTimezone.setHours(
      eventEndDateinAttendeeTimezone.getHours() + 6
    );
    eventEndDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventEndDateinAttendeeTimezone,
      attendeeTimezone
    ).toString();
    console.log(
      "Event satrt date and end date" + eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
    console.log("Event created time: ", eventCreatedTime);
    /*
       If the start date is not same as the end date, it will display start date - end date, but if they are same it will display 
        the time of the event in hours and minutes. So we are checking in which case the event lies by comparing event start date and
        event end dates. 
       */
    await (
      await landingPageOne.getDateTimeCounterComponent()
    ).isCorrectDatetimeDisplayed(
      eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
    // Changing the attendee Timezone foor the same event to verify if it is displaying correct date time counter for different timezone.

    attendeeTimezone = "America/New_York";
    attendeeContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      timeZoneId: attendeeTimezone,
    });
    page = await attendeeContext.newPage();
    landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendee_landing_page);
    eventStartDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventCreatedTime1,
      attendeeTimezone
    ).toString();
    eventEndDateinAttendeeTimezone = eventCreatedTime1;
    eventEndDateinAttendeeTimezone = eventEndDateinAttendeeTimezone.setHours(
      eventEndDateinAttendeeTimezone.getHours() + 6
    );
    eventEndDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventEndDateinAttendeeTimezone,
      attendeeTimezone
    ).toString();
    await (
      await landingPageOne.getDateTimeCounterComponent()
    ).isCorrectDatetimeDisplayed(
      eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
  });
});
