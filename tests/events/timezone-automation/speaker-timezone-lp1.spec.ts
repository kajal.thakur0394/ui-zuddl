import { test, BrowserContext, APIRequestContext } from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import {
  createNewEvent,
  getEventDetails,
  enableMagicLinkInEvent,
  inviteSpeakerByApi,
} from "../../../util/apiUtil";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import EventRole from "../../../enums/eventRoleEnum";
import { DataUtl } from "../../../util/dataUtil";
import { convertInAnotherTimeZone } from "../../../util/commonUtil";
import { SpeakerData } from "../../../test-data/speakerData";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";

test.describe
  .parallel("Time Zone Test suite|Invite Based|Magic link enabled| Speaker Landing Page", () => {
  let org_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let landingPageOne: LandingPageOne;
  let eventStartDateinAttendeeTimezone;
  let eventEndDateinAttendeeTimezone;
  let speakerContext: BrowserContext;

  test.afterEach(async () => {
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
    await speakerContext?.close();
  });

  test('TC001: Verify speaker able to login from Timezone other than the created event Timezone, @Event Created Timezone: "Pacific/Auckland" , @Speaker timezone:  "Asia/Kolkata"', async ({}) => {
    // setup org context
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
      timeZoneId: "Pacific/Auckland",
    });
    organiserApiContext = await org_browser_context.request;
    // org_session = await org_browser_context.newPage()
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      default_settings: true,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;

    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );
    let context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    let page = await context.newPage();
    landingPageOne = new LandingPageOne(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    ); // true denote to send email
    await landingPageOne.load(speaker_landing_page);
    await landingPageOne.clickOnLoginButton();
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER
    );
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isStageLoaded();
  });

  test('TC002: If the event timer is displaying  Date and Time correctly and According to Speaker Timezone. @Event created TimeZone: Pacific/Auckland @speaker Time zone:"Asia/Kolkata', async ({}) => {
    // setup org context
    let organiserTimezone = "Pacific/Auckland";
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
      timeZoneId: organiserTimezone,
    });
    organiserApiContext = await org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    // Creating same day event
    const eventCreatedTime = new Date();
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      sameDayEvent: true,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;

    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );
    let speakerTimezone = "Asia/Kolkata";
    speakerContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      timeZoneId: speakerTimezone,
    });
    let page = await speakerContext.newPage();
    landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(speaker_landing_page);
    eventStartDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventCreatedTime,
      speakerTimezone
    ).toString();
    eventEndDateinAttendeeTimezone = eventCreatedTime;
    eventEndDateinAttendeeTimezone = eventEndDateinAttendeeTimezone.setHours(
      eventEndDateinAttendeeTimezone.getHours() + 6
    );
    eventEndDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventEndDateinAttendeeTimezone,
      speakerTimezone
    ).toString();
    console.log(
      "Event satrt date and end date" + eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
    console.log("Event created time: ", eventCreatedTime);
    /*
       If the start date is not same as the end date, it will display start date - end date, but if they are same it will display 
        the time of the event in hours and minutes. So we are checking in which case the event lies by comparing event start date and
        event end dates. 
       */
    await (
      await landingPageOne.getDateTimeCounterComponent()
    ).isCorrectDatetimeDisplayed(
      eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
  });

  test('TC003: Verify event start date and end date are changing according to Speaker timezone for multiple location @attendee timezones = "Asia/Kolkata","America/New York", @event timeZone="Pacific/Auckland"', async ({}) => {
    // setup org context
    let organiserTimezone = "Pacific/Auckland";
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
      timeZoneId: organiserTimezone,
    });
    organiserApiContext = await org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    // Creating same day event
    const eventCreatedTime = new Date();
    const eventCreatedTime1 = new Date();
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      sameDayEvent: true,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, true); // true to denote magic link enabled
    //update event entry to invite only
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await enableMagicLinkInEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;

    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );
    let speakerTimezone = "Asia/Kolkata";
    speakerContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      timeZoneId: speakerTimezone,
    });
    let page = await speakerContext.newPage();
    landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(speaker_landing_page);
    eventStartDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventCreatedTime,
      speakerTimezone
    ).toString();
    eventEndDateinAttendeeTimezone = eventCreatedTime;
    eventEndDateinAttendeeTimezone = eventEndDateinAttendeeTimezone.setHours(
      eventEndDateinAttendeeTimezone.getHours() + 6
    );
    eventEndDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventEndDateinAttendeeTimezone,
      speakerTimezone
    ).toString();
    console.log(
      "Event satrt date and end date" + eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
    console.log("Event created time: ", eventCreatedTime);
    /*
       If the start date is not same as the end date, it will display start date - end date, but if they are same it will display 
        the time of the event in hours and minutes. So we are checking in which case the event lies by comparing event start date and
        event end dates. 
       */
    await (
      await landingPageOne.getDateTimeCounterComponent()
    ).isCorrectDatetimeDisplayed(
      eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
    // Changing the speaker Timezone
    speakerTimezone = "America/New_York";
    speakerContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      timeZoneId: speakerTimezone,
    });
    page = await speakerContext.newPage();
    landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendee_landing_page);
    eventStartDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventCreatedTime1,
      speakerTimezone
    ).toString();
    eventEndDateinAttendeeTimezone = eventCreatedTime1;
    eventEndDateinAttendeeTimezone = eventEndDateinAttendeeTimezone.setHours(
      eventEndDateinAttendeeTimezone.getHours() + 6
    );
    eventEndDateinAttendeeTimezone = convertInAnotherTimeZone(
      eventEndDateinAttendeeTimezone,
      speakerTimezone
    ).toString();
    console.log(
      "Event satrt date and end date" + eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
    console.log("Event created time: ", eventCreatedTime1);
    await (
      await landingPageOne.getDateTimeCounterComponent()
    ).isCorrectDatetimeDisplayed(
      eventStartDateinAttendeeTimezone,
      eventEndDateinAttendeeTimezone
    );
  });
});
