import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { AVModal } from "../../../page-objects/events-pages/live-side-components/AvModal";
import { RoomModule } from "../../../page-objects/events-pages/site-modules/Rooms";
import {
  createNewEvent,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { async } from "node-ical";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@rooms @public @AV`, async () => {
  test.slow();
  test.describe.configure({ retries: 2 });
  let eventId;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let attendeeOneBrowserContext: BrowserContext;
  let attendeeTwoBrowserContext: BrowserContext;
  let eventController: EventController;
  let roomListingPage: string;
  let attendeeLandingPage: string;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiContext = orgBrowserContext.request;
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: "room automation",
    });
    attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    attendeeTwoBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });

    eventController = new EventController(orgApiContext, eventId);
    //add rooms
    await eventController.getRoomsController.addRoomsToEvent({
      seatsPerRoom: 10,
      numberOfRooms: 1,
      roomCategory: "PUBLIC",
    });

    let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
    roomListingPage = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/discussions`;
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
    await attendeeOneBrowserContext?.close();
    await attendeeTwoBrowserContext?.close();
  });

  test("TC001: Organiser and 2 attendee enter inside the room with their stream's AV on", async () => {
    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let attendeeTwoPage = await attendeeTwoBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let attendeeOneRoom: RoomModule;
    let attendeeTwoRoom: RoomModule;
    let organiserRoom: RoomModule;

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const attendeeTwoFirstName = "prateekAttendeeTwo";
    const attendeeTwoLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    const organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let attendeeEmailTwo = DataUtl.getRandomAttendeeEmail();

    await test.step(`organiser invites attendee  1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await test.step(`organiser invites attendee  2 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeTwoFirstName,
        lastName: attendeeTwoLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailTwo,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      await attendeeOnePage.goto(roomListingPage);

      let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeOneAvModal = new AVModal(attendeeOnePage);
      await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
    });

    await test.step(`attendee 2 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailTwo
      );
      await attendeeTwoPage.goto(magicLink);
      await attendeeTwoPage.click(`text=Enter Now`);
      await attendeeTwoPage.waitForURL(/lobby/);
      await attendeeTwoPage.goto(roomListingPage);
      let roomone_locator = attendeeTwoPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeTwoAvModal = new AVModal(attendeeTwoPage);
      await attendeeTwoAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeTwoAvModal.clickOnJoinButtonOnAvModal();
    });

    await test.step(`organiser logs into the room`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(roomListingPage);

      let roomone_locator = organiserPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const organiserAvModal = new AVModal(organiserPage);
      await organiserAvModal.isVideoStreamLoadedOnAVModal();
      await organiserPage.waitForTimeout(2000);
      await organiserAvModal.clickOnJoinButtonOnAvModal();
    });

    // presence and visibility of AV streams
    await test.step(`verify attendee 1 sees attendee 2 and organiser stream inside room`, async () => {
      attendeeOneRoom = new RoomModule(attendeeOnePage);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify attendee 2 sees attendee 1 and organiser stream inside room`, async () => {
      attendeeTwoRoom = new RoomModule(attendeeTwoPage);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 and attendee 2 stream inside room`, async () => {
      organiserRoom = new RoomModule(organiserPage);
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    //verify every one is able to turn off their audio and video
    await test.step(`attendee one turns off the video and audio`, async () => {
      // when someone is already joined, the attendee who is joining will have its audio muted
      await expect(
        attendeeOneRoom.getStreamOptionsComponent.unmuteaudioButtonForMyStream,
        "expecting unmute button is visible to attendee one as his audio is already muted"
      ).toBeVisible();
      //now mute the video
      await test.step(`attendee 1 muting the video`, async () => {
        await attendeeOneRoom.getStreamOptionsComponent.muteMyVideo();
      });
    });
    await test.step(`attendee two turns off the video and audio`, async () => {
      // when someone is already joined, the attendee who is joining will have its audio muted
      await expect(
        attendeeTwoRoom.getStreamOptionsComponent.unmuteaudioButtonForMyStream,
        "expecting unmute button is visible to attendee one as his audio is already muted"
      ).toBeVisible();
      //now mute the video
      await test.step(`attendee 2 muting the video`, async () => {
        await attendeeTwoRoom.getStreamOptionsComponent.muteMyVideo();
      });
    });
    await test.step(`organiser turns off the video and audio`, async () => {
      await organiserRoom.getStreamOptionsComponent.muteMyAudio();
      await organiserRoom.getStreamOptionsComponent.muteMyVideo();
    });

    // presence and visibility of AV streams after every one has muted their audio and video
    await test.step(`verify attendee 1 sees attendee 2 and organiser stream inside room`, async () => {
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify attendee 2 sees attendee 1 and organiser stream inside room`, async () => {
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 and attendee 2 stream inside room`, async () => {
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    // Now every one exits from the room
    await test.step(`verify attendee 1 is able to exit the room and land on room listing page`, async () => {
      await attendeeOneRoom.clickOnExitRoom();
      await attendeeOneRoom.handleExitConfirmationPopup(true);
    });

    // attendee two exit from the room
    await test.step(`verify attendee 2 is able to exit the room and land on room listing page`, async () => {
      await attendeeTwoRoom.clickOnExitRoom();
      await attendeeTwoRoom.handleExitConfirmationPopup(true);
    });

    // organiser exits from the room
    await test.step(`verify organiser is able to exit the room and land on room listing page`, async () => {
      await organiserRoom.clickOnExitRoom();
      await organiserRoom.handleExitConfirmationPopup(true);
    });
  });
  test("TC002: Organiser and 2 attendee enter inside the room with their stream's AV off", async () => {
    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let attendeeTwoPage = await attendeeTwoBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const attendeeTwoFirstName = "prateekAttendeeTwo";
    const attendeeTwoLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    const organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];
    let attendeeOneRoom: RoomModule;
    let attendeeTwoRoom: RoomModule;
    let organiserRoom: RoomModule;

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let attendeeEmailTwo = DataUtl.getRandomAttendeeEmail();

    await test.step(`organiser invites attendee  1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await test.step(`organiser invites attendee  2 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeTwoFirstName,
        lastName: attendeeTwoLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailTwo,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      await attendeeOnePage.goto(roomListingPage);

      let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeOneAvModal = new AVModal(attendeeOnePage);
      await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
      //by default attendee 1 mic will be muted as someone has already joined the room
      await test.step(`expecting attendee 1 audio is already muted on av modal as some one is already inside the room`, async () => {
        await expect(attendeeOneAvModal.buttonToUnmuteAudio).toBeVisible();
      });
      // await attendeeOneAvModal.muteAudioOnMyStream();
      await attendeeOneAvModal.muteVideoOnMyStream();
      await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
    });

    await test.step(`attendee 2 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailTwo
      );
      await attendeeTwoPage.goto(magicLink);
      await attendeeTwoPage.click(`text=Enter Now`);
      await attendeeTwoPage.waitForURL(/lobby/);
      await attendeeTwoPage.goto(roomListingPage);
      let roomone_locator = attendeeTwoPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeTwoAvModal = new AVModal(attendeeTwoPage);
      await attendeeTwoAvModal.isVideoStreamLoadedOnAVModal();
      //by default attendee 1 mic will be muted as someone has already joined the room
      await test.step(`expecting attendee 1 audio is already muted on av modal as some one is already inside the room`, async () => {
        await expect(attendeeTwoAvModal.buttonToUnmuteAudio).toBeVisible();
      });
      await attendeeTwoAvModal.muteVideoOnMyStream();
      await attendeeTwoAvModal.clickOnJoinButtonOnAvModal();
    });
    await test.step(`organiser logs into the room`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(roomListingPage);

      let roomone_locator = organiserPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const organiserAvModal = new AVModal(organiserPage);
      await organiserAvModal.isVideoStreamLoadedOnAVModal();
      await organiserPage.waitForTimeout(2000);
      await organiserAvModal.muteAudioOnMyStream();
      await organiserAvModal.muteVideoOnMyStream();
      await organiserAvModal.clickOnJoinButtonOnAvModal();
    });

    // presence and visibility of AV streams
    await test.step(`verify attendee 1 sees attendee 2 and organiser stream inside room`, async () => {
      attendeeOneRoom = new RoomModule(attendeeOnePage);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify attendee 2 sees attendee 1 and organiser stream inside room`, async () => {
      attendeeTwoRoom = new RoomModule(attendeeTwoPage);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 and attendee 2 stream inside room`, async () => {
      organiserRoom = new RoomModule(organiserPage);
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    //verify every one is able to turn off their audio and video
    await test.step(`attendee one turns off the video and audio`, async () => {
      await attendeeOneRoom.getStreamOptionsComponent.unmuteMyAudio();
      await attendeeOneRoom.getStreamOptionsComponent.unmuteMyVideo();
    });
    await test.step(`attendee two turns off the video and audio`, async () => {
      await attendeeTwoRoom.getStreamOptionsComponent.unmuteMyAudio();
      await attendeeTwoRoom.getStreamOptionsComponent.unmuteMyVideo();
    });
    await test.step(`organiser turns off the video and audio`, async () => {
      await organiserRoom.getStreamOptionsComponent.unmuteMyAudio();
      await organiserRoom.getStreamOptionsComponent.unmuteMyVideo();
    });

    // presence and visibility of AV streams after every one has muted their audio and video
    await test.step(`verify attendee 1 sees attendee 2 and organiser stream inside room`, async () => {
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify attendee 2 sees attendee 1 and organiser stream inside room`, async () => {
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 and attendee 2 stream inside room`, async () => {
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    // Now every one exits from the room
    await test.step(`verify attendee 1 is able to exit the room and land on room listing page`, async () => {
      await attendeeOneRoom.clickOnExitRoom();
      await attendeeOneRoom.handleExitConfirmationPopup(true);
    });

    // attendee two exit from the room
    await test.step(`verify attendee 2 is able to exit the room and land on room listing page`, async () => {
      await attendeeTwoRoom.clickOnExitRoom();
      await attendeeTwoRoom.handleExitConfirmationPopup(true);
    });

    // organiser exits from the room
    await test.step(`verify organiser is able to exit the room and land on room listing page`, async () => {
      await organiserRoom.clickOnExitRoom();
      await organiserRoom.handleExitConfirmationPopup(true);
    });
  });

  test("TC003: verify user is able to add and edit public rooms", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-113/verify-user-is-able-to-add-public-rooms",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-114/verify-user-is-able-to-edit-a-room",
    });
    let attendeePage = await attendeeOneBrowserContext.newPage();
    let speakerPage = await attendeeTwoBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let attendeeOneRoom: RoomModule;
    let speakerRoom: RoomModule;
    let organiserRoom: RoomModule;
    let roomName: string;
    let roomId: string;

    const attendeeOneFirstName = "prateekAttendee";
    const attendeeOneLastName = "QA";

    const speakerFirstName = "prateekSpeaker";
    const speakerLastName = "QA";

    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let speakerEmail = DataUtl.getRandomSpeakerEmail();
    const updatedRoomName = "QA-Room";

    await test.step(`verify room gets added in an event via API on setup side`, async () => {
      let roomListResponse = await (
        await eventController.getRoomsController.getListOfRooms()
      ).json();
      roomName = roomListResponse[0]["name"];
      roomId = roomListResponse[0]["discussionTableId"];
      expect(roomName).toEqual("Room 1");
    });

    await test.step(`organiser invites attendee to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmail,
      });
    });

    await test.step(`organiser invites speaker to the event`, async () => {
      await eventController.inviteSpeakerToTheEventByApi(
        speakerEmail,
        speakerFirstName,
        speakerLastName
      );
    });

    await test.step(`Disable onboarding checks for the event`, async () => {
      await eventController.disableOnboardingChecksForAttendee();
      await eventController.disableOnboardingChecksForSpeaker();
    });

    await test.step(`attendee logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await attendeePage.goto(magicLink);
      await attendeePage.click(`text=Enter Now`);
      await attendeePage.waitForURL(/lobby/);
      await attendeePage.goto(roomListingPage);

      let roomone_locator = attendeePage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeOneAvModal = new AVModal(attendeePage);
      await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
    });

    await test.step(`speaker logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );
      await speakerPage.goto(magicLink);
      await speakerPage.click(`text=Enter Now`);
      await speakerPage.goto(roomListingPage);
      let roomone_locator = speakerPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const speakerAvModal = new AVModal(speakerPage);
      await speakerAvModal.isVideoStreamLoadedOnAVModal();
      await speakerAvModal.clickOnJoinButtonOnAvModal();
    });

    await test.step(`organiser logs into the room`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(roomListingPage);

      let roomone_locator = organiserPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const organiserAvModal = new AVModal(organiserPage);
      await organiserAvModal.isVideoStreamLoadedOnAVModal();
      await organiserPage.waitForTimeout(2000);
      await organiserAvModal.clickOnJoinButtonOnAvModal();
    });

    await test.step(`Now, organiser verifies Room name to  ${roomName} on live event side via UI`, async () => {
      organiserRoom = new RoomModule(organiserPage);
      await organiserRoom.verifyRoomTitleMatches(roomName);
    });
    await test.step(`Now, speaker verifies Room name to  ${roomName} on live event side via UI`, async () => {
      speakerRoom = new RoomModule(speakerPage);
      await speakerRoom.verifyRoomTitleMatches(roomName);
    });
    await test.step(`Now, attendee verifies Room name to  ${roomName} on live event side via UI`, async () => {
      attendeeOneRoom = new RoomModule(attendeePage);
      await attendeeOneRoom.verifyRoomTitleMatches(roomName);
    });

    await test.step(`Now organiser updates/edit the roomName via API`, async () => {
      let detailsToUpdate = {
        capacity: 6,
        name: updatedRoomName,
        isLargeRoom: false,
      };
      await eventController.getRoomsController.updateRoomDetails(
        roomId,
        detailsToUpdate
      );
    });

    await test.step(`Now, organiser verifies Room name to  ${updatedRoomName} on live event side via UI`, async () => {
      await organiserRoom.verifyRoomTitleMatches(updatedRoomName);
    });
    await test.step(`Now, speaker verifies Room name to  ${updatedRoomName} on live event side via UI`, async () => {
      await speakerRoom.verifyRoomTitleMatches(updatedRoomName);
    });
    await test.step(`Now, attendee verifies Room name to  ${updatedRoomName} on live event side via UI`, async () => {
      await attendeeOneRoom.verifyRoomTitleMatches(updatedRoomName);
    });
  });
});
