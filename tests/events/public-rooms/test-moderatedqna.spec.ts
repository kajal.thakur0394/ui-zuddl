import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { AVModal } from "../../../page-objects/events-pages/live-side-components/AvModal";
import { RoomModule } from "../../../page-objects/events-pages/site-modules/Rooms";
import {
  createNewEvent,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { RoomController } from "../../../controller/RoomController";
import { async } from "node-ical";
import { QnAController } from "../../../controller/QnAController";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel(`@rooms @public @interaction-panel`, async () => {
  test.slow();
  // test.describe.configure({ retries: 1 });
  let eventId;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let attendeeOneBrowserContext: BrowserContext;
  let attendeeTwoBrowserContext: BrowserContext;
  let eventController: EventController;
  let roomListingPage: string;
  let attendeeLandingPage: string;
  let roomController: RoomController;
  let roomChannelId: string;
  let orgQnaController: QnAController;
  let at1QnaController: QnAController;
  let at2QnaController: QnAController;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = orgBrowserContext.request;
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: "room automation",
    });
    attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    attendeeTwoBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });

    eventController = new EventController(orgApiContext, eventId);
    //add rooms
    await eventController.getRoomsController.addRoomsToEvent({
      seatsPerRoom: 10,
      numberOfRooms: 1,
      roomCategory: "PUBLIC",
    });

    const roomId = await eventController.fetchRoomIdFromListOfRoomsByRoomName(
      "Room 1"
    );
    console.log(`Room Id for this test case is ${roomId}`);

    //add the room controller
    roomController = new RoomController(orgApiContext, eventId, roomId);
    //add the room controller
    roomChannelId = await roomController.getChannelId();

    await test.step(`Enable the moderated qna for this room`, async () => {
      await roomController.updateRoomEngaementSettingsForThisRoom({
        hasQuestion: true,
        hasQuestionModerate: true,
      });
    });

    let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
    roomListingPage = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/discussions`;

    orgQnaController = new QnAController(orgApiContext, eventId, roomChannelId);
    at1QnaController = new QnAController(
      attendeeOneBrowserContext.request,
      eventId,
      roomChannelId
    );
    at2QnaController = new QnAController(
      attendeeTwoBrowserContext.request,
      eventId,
      roomChannelId
    );
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
    await attendeeOneBrowserContext?.close();
    await attendeeTwoBrowserContext?.close();
  });

  test("TC001: @qna Verify e2e flow of posting a question and getting it approved", async () => {
    test.info().annotations.push(
      {
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-521/verify-org-view-of-qanda-component-in-case-of-moderation",
      },
      {
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-510/qanda-with-moderation",
      },
      {
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-522/e2e-flow-of-posting-a-question-and-getting-it-approved",
      }
    );

    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let attendeeTwoPage = await attendeeTwoBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let attendeeOneRoom: RoomModule;
    let attendeeTwoRoom: RoomModule;
    let organiserRoom: RoomModule;

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let attendeeEmailTwo = DataUtl.getRandomAttendeeEmail();

    const questionToAsk = "Where is Mumbai located";
    const answerToQuestion = "Maharashtra";

    await test.step(`organiser invites attendee 1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });
    await test.step(`organiser invites attendee 2 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneLastName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailTwo,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );

      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      await attendeeOnePage.goto(roomListingPage);

      let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeOneAvModal = new AVModal(attendeeOnePage);
      await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeOneAvModal.clickOnJoinButtonOnAvModal();

      //click on skip button to close edit profile popup
      await attendeeOnePage.locator("button:has-text('Skip')").click();
    });
    await test.step(`attendee 2 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailTwo
      );
      await attendeeTwoPage.goto(magicLink);
      await attendeeTwoPage.click(`text=Enter Now`);
      await attendeeTwoPage.waitForURL(/lobby/);
      await attendeeTwoPage.goto(roomListingPage);

      let roomone_locator = attendeeTwoPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeTwoAvModal = new AVModal(attendeeTwoPage);
      await attendeeTwoAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeTwoAvModal.clickOnJoinButtonOnAvModal();
      //click on skip button to close edit profile popup
      await attendeeTwoPage.locator("button:has-text('Skip')").click();
    });
    await test.step(`organiser logs into the room`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(roomListingPage);

      let roomone_locator = organiserPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const organiserAvModal = new AVModal(organiserPage);
      await organiserAvModal.isVideoStreamLoadedOnAVModal();
      await organiserPage.waitForTimeout(2000);
      await organiserAvModal.clickOnJoinButtonOnAvModal();
    });

    // presence and visibility of AV streams
    await test.step(`verify attendee 1 organiser stream inside room`, async () => {
      attendeeOneRoom = new RoomModule(attendeeOnePage);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify attendee 2 sees attendee 1 and organiser stream inside room`, async () => {
      attendeeTwoRoom = new RoomModule(attendeeTwoPage);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 stream inside room`, async () => {
      organiserRoom = new RoomModule(organiserPage);
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
    });

    //now all users goes to QnA section
    await test.step(`Organiser opens QnA section`, async () => {
      await organiserRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`Attendee1 opens QnA section`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`Attendee2 opens QnA section`, async () => {
      await attendeeTwoRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`verify organiser view is different than of attendee and he sees 2 tabs: For review and Published`, async () => {
      await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyOrganiserViewOnQnATab();
    });

    await test.step(`verify attendee1 view is different than of attendee and he sees 2 tabs: New Questions Tab and Answered Tab and 1 filter to sort question whose default selected state is Upvotes`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyAttendeeViewOnQuestionsTab();
    });

    await test.step(`verify Attendee1 when post question on new questions tab if its less than 3 words, we show a error message that please ask valid question`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.enterQuestionToAsk(
        "qna"
      );
      await attendeeOneRoom.getInteractionPanelComponent.getNotificationsComponent.verifyNotAValidQuestionMessage();
    });

    await test.step(`Attendee1 posts the question`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.clickOnNewQuestionsTab();
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.enterQuestionToAsk(
        questionToAsk
      );
    });

    await test.step(`Attendee1 verifies question is visible having status 'waiting for review'`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyWaitingForReviewTextPresent(
        questionToAsk
      );
    });

    await test.step(`Attendee2 verifies question is not visible`, async () => {
      await attendeeTwoRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyWaitingForReviewTextNotPresent(
        questionToAsk
      );
    });

    await test.step(`verify organiser sees the question in For Review tab`, async () => {
      await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyQuestionPresentInReviewTab(
        questionToAsk
      );
    });

    await test.step(`Organiser now approves the question`, async () => {
      await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.organiserApprovesTheQuestion(
        questionToAsk
      );
    });

    await test.step(`As soon as the question is approved, verify the following:`, async () => {
      await test.step(`verify its not anymore visible to organiser in for review tab`, async () => {
        await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyQuestionNotPresentInReviewTab();
      });
      await test.step(`Organiser goes to published tab and verify the presence of the approved question`, async () => {
        await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.clickOnPublishedTab();
        await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyQuestionPresentInPublishedTab(
          questionToAsk
        );
      });
      await test.step(`Attendee1 verifies 'waiting for review' status no more visible`, async () => {
        await attendeeOneRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyWaitingForReviewTextNotPresent(
          questionToAsk
        );
      });
      await test.step(`Organiser replies a question`, async () => {
        await organiserRoom.getInteractionPanelComponent.getqNaComponent.replyGivenQuestionWithText(
          questionToAsk,
          answerToQuestion
        );
      });
      await test.step(`Attendee1 verifies his question has been answered in Answered tab `, async () => {
        await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.attendeeSwitchToAnsweredTab();
        await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyElementsPresentPostReply(
          questionToAsk,
          answerToQuestion,
          false
        );
      });
    });
  });

  test("TC002: @qna Verify e2e flow of posting a question and getting it rejected", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-523/e2e-flow-of-posting-a-question-and-its-getting-rejected",
    });

    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let attendeeTwoPage = await attendeeTwoBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let attendeeOneRoom: RoomModule;
    let attendeeTwoRoom: RoomModule;
    let organiserRoom: RoomModule;

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let attendeeEmailTwo = DataUtl.getRandomAttendeeEmail();

    const questionToAsk = "Where is Mumbai located";
    const answerToQuestion = "Maharashtra";

    await test.step(`organiser invites attendee 1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });
    await test.step(`organiser invites attendee 2 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneLastName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailTwo,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      await attendeeOnePage.goto(roomListingPage);

      let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeOneAvModal = new AVModal(attendeeOnePage);
      await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
      //click on skip button to close edit profile popup
      await attendeeOnePage.locator("button:has-text('Skip')").click();
    });
    await test.step(`attendee 2 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailTwo
      );
      await attendeeTwoPage.goto(magicLink);
      await attendeeTwoPage.click(`text=Enter Now`);
      await attendeeTwoPage.waitForURL(/lobby/);
      await attendeeTwoPage.goto(roomListingPage);

      let roomone_locator = attendeeTwoPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeTwoAvModal = new AVModal(attendeeTwoPage);
      await attendeeTwoAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeTwoAvModal.clickOnJoinButtonOnAvModal();
      //click on skip button to close edit profile popup
      await attendeeTwoPage.locator("button:has-text('Skip')").click();
    });
    await test.step(`organiser logs into the room`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(roomListingPage);

      let roomone_locator = organiserPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const organiserAvModal = new AVModal(organiserPage);
      await organiserAvModal.isVideoStreamLoadedOnAVModal();
      await organiserPage.waitForTimeout(2000);
      await organiserAvModal.clickOnJoinButtonOnAvModal();
    });

    // presence and visibility of AV streams
    await test.step(`verify attendee 1 organiser stream inside room`, async () => {
      attendeeOneRoom = new RoomModule(attendeeOnePage);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify attendee 2 sees attendee 1 and organiser stream inside room`, async () => {
      attendeeTwoRoom = new RoomModule(attendeeTwoPage);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 stream inside room`, async () => {
      organiserRoom = new RoomModule(organiserPage);
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
    });

    //now all users goes to QnA section
    await test.step(`Organiser opens QnA section`, async () => {
      await organiserRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`Attendee1 opens QnA section`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`Attendee2 opens QnA section`, async () => {
      await attendeeTwoRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`verify organiser view is different than of attendee and he sees 2 tabs: For review and Published`, async () => {
      await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyOrganiserViewOnQnATab();
    });

    await test.step(`verify attendee1 view is different than of attendee and he sees 2 tabs: New Questions Tab and Answered Tab and 1 filter to sort question whose default selected state is Upvotes`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyAttendeeViewOnQuestionsTab();
    });

    await test.step(`verify Attendee1 when post question on new questions tab if its less than 3 words, we show a error message that please ask valid question`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.enterQuestionToAsk(
        "qna"
      );
      await attendeeOneRoom.getInteractionPanelComponent.getNotificationsComponent.verifyNotAValidQuestionMessage();
    });

    await test.step(`Attendee1 posts the question`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.clickOnNewQuestionsTab();
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.enterQuestionToAsk(
        questionToAsk
      );
    });

    await test.step(`Attendee1 verifies question is visible having status 'waiting for review'`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyWaitingForReviewTextPresent(
        questionToAsk
      );
    });

    await test.step(`Attendee2 verifies question is not visible`, async () => {
      await attendeeTwoRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyWaitingForReviewTextNotPresent(
        questionToAsk
      );
    });

    await test.step(`verify organiser sees the question in For Review tab`, async () => {
      await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyQuestionPresentInReviewTab(
        questionToAsk
      );
    });

    await test.step(`Organiser now rejects the question`, async () => {
      await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.organiserRejectsTheQuestion(
        questionToAsk
      );
    });

    await test.step(`As soon as the question is rejected, verify the following:`, async () => {
      await test.step(`verify its not anymore visible to organiser in for review and publisehd tab`, async () => {
        await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyQuestionNotPresentInReviewTab();
        await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.clickOnPublishedTab();
        await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyQuestionNotPresentInPublishedTab();
      });

      await test.step(`verify its not anymore visible to attendee1 in for NewQuestions and Answered tab`, async () => {
        await attendeeOneRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyQuestionNotPresentInNewQuestionsTab();
        await attendeeOneRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyQuestionNotPresentInAnsweredTab();
      });
    });
  });

  test("TC003: @qna Verify flow of org multi select questions to approve", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-524/verify-flow-of-org-multi-select-questions-to-approve",
    });
    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let attendeeTwoPage = await attendeeTwoBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let attendeeOneRoom: RoomModule;
    let attendeeTwoRoom: RoomModule;
    let organiserRoom: RoomModule;

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let attendeeEmailTwo = DataUtl.getRandomAttendeeEmail();

    const questionToAsk1 = "Where is Mumbai located 1";
    const questionToAsk2 = "Where is Mumbai located 2";
    const questionToAsk3 = "Where is Mumbai located 3";
    const answerToQuestion = "Maharashtra";

    await test.step(`organiser invites attendee 1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });
    await test.step(`organiser invites attendee 2 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneLastName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailTwo,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      await attendeeOnePage.goto(roomListingPage);

      let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeOneAvModal = new AVModal(attendeeOnePage);
      await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
      //click on skip button to close edit profile popup
      await attendeeOnePage.locator("button:has-text('Skip')").click();
    });
    await test.step(`attendee 2 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailTwo
      );
      await attendeeTwoPage.goto(magicLink);
      await attendeeTwoPage.click(`text=Enter Now`);
      await attendeeTwoPage.waitForURL(/lobby/);
      await attendeeTwoPage.goto(roomListingPage);

      let roomone_locator = attendeeTwoPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeTwoAvModal = new AVModal(attendeeTwoPage);
      await attendeeTwoAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeTwoAvModal.clickOnJoinButtonOnAvModal();
      //click on skip button to close edit profile popup
      await attendeeTwoPage.locator("button:has-text('Skip')").click();
    });
    await test.step(`organiser logs into the room`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(roomListingPage);

      let roomone_locator = organiserPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const organiserAvModal = new AVModal(organiserPage);
      await organiserAvModal.isVideoStreamLoadedOnAVModal();
      await organiserPage.waitForTimeout(2000);
      await organiserAvModal.clickOnJoinButtonOnAvModal();
    });

    // presence and visibility of AV streams
    await test.step(`verify attendee 1 organiser stream inside room`, async () => {
      attendeeOneRoom = new RoomModule(attendeeOnePage);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify attendee 2 sees attendee 1 and organiser stream inside room`, async () => {
      attendeeTwoRoom = new RoomModule(attendeeTwoPage);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 stream inside room`, async () => {
      organiserRoom = new RoomModule(organiserPage);
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
    });

    //now all users goes to QnA section
    await test.step(`Organiser opens QnA section`, async () => {
      await organiserRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`Attendee1 opens QnA section`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`Attendee2 opens QnA section`, async () => {
      await attendeeTwoRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`verify organiser view is different than of attendee and he sees 2 tabs: For review and Published`, async () => {
      await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyOrganiserViewOnQnATab();
    });

    await test.step(`verify attendee1 view is different than of attendee and he sees 2 tabs: New Questions Tab and Answered Tab and 1 filter to sort question whose default selected state is Upvotes`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyAttendeeViewOnQuestionsTab();
    });

    await test.step(`Attendee1 posts the question1 ${questionToAsk1} via API`, async () => {
      await at1QnaController.postAQuestion(questionToAsk1);
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.clickOnNewQuestionsTab();
    });

    await test.step(`Attendee1 posts the question2 ${questionToAsk2} via API`, async () => {
      await at1QnaController.postAQuestion(questionToAsk2);
    });

    await test.step(`Attendee2 posts the question3 ${questionToAsk3} via API`, async () => {
      await at2QnaController.postAQuestion(questionToAsk3);
      await attendeeTwoRoom.getInteractionPanelComponent.getqNaComponent.clickOnNewQuestionsTab();
    });

    await test.step(`Organiser verifies Select checkbox to be visible`, async () => {
      await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifySelectAllChecboxPresent();
    });

    await test.step(`Organiser now approves ${questionToAsk1} and ${questionToAsk2} via API`, async () => {
      let questionIdList: string[] = [];
      questionIdList.push(await at1QnaController.getQuestionId(questionToAsk1));
      questionIdList.push(await at1QnaController.getQuestionId(questionToAsk2));
      await orgQnaController.approveOrRejectQuestions(
        questionIdList,
        "APPROVED"
      );
    });

    await test.step(`Verify the following once questions are approved:`, async () => {
      await test.step(`Organiser sees ${questionToAsk3} in ForReview tab`, async () => {
        await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyQuestionPresentInReviewTab(
          questionToAsk3
        );
      });

      await test.step(`Organiser sees ${questionToAsk1} and ${questionToAsk2} in Published tab`, async () => {
        await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.organiserSwitchesToPublishedQuestionsTab();
        await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyQuestionPresentInPublishedTab(
          questionToAsk2
        );
        await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyQuestionPresentInPublishedTab(
          questionToAsk1
        );
      });

      await test.step(`Attendee2 verifies question ${questionToAsk3} is visible having status 'waiting for review'`, async () => {
        await attendeeTwoRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyWaitingForReviewTextPresent(
          questionToAsk3
        );
      });

      await test.step(`Attendee1 verifies 'waiting for review' status no more visible for ${questionToAsk1} and ${questionToAsk2}`, async () => {
        await attendeeOneRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyWaitingForReviewTextNotPresent(
          questionToAsk1
        );
        await attendeeOneRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyWaitingForReviewTextNotPresent(
          questionToAsk2
        );
      });
    });
  });

  test("TC004: @qna Verify flow of org multi select questions to reject", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-525/verify-flow-of-org-multi-select-questions-to-reject",
    });
    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let attendeeTwoPage = await attendeeTwoBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let attendeeOneRoom: RoomModule;
    let attendeeTwoRoom: RoomModule;
    let organiserRoom: RoomModule;

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let attendeeEmailTwo = DataUtl.getRandomAttendeeEmail();

    const questionToAsk1 = "Where is Mumbai located 1";
    const questionToAsk2 = "Where is Mumbai located 2";
    const questionToAsk3 = "Where is Mumbai located 3";
    const answerToQuestion = "Maharashtra";

    await test.step(`organiser invites attendee 1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });
    await test.step(`organiser invites attendee 2 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneLastName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailTwo,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      await attendeeOnePage.goto(roomListingPage);

      let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeOneAvModal = new AVModal(attendeeOnePage);
      await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
      //click on skip button to close edit profile popup
      await attendeeOnePage.locator("button:has-text('Skip')").click();
    });
    await test.step(`attendee 2 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailTwo
      );
      await attendeeTwoPage.goto(magicLink);
      await attendeeTwoPage.click(`text=Enter Now`);
      await attendeeTwoPage.waitForURL(/lobby/);
      await attendeeTwoPage.goto(roomListingPage);

      let roomone_locator = attendeeTwoPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeTwoAvModal = new AVModal(attendeeTwoPage);
      await attendeeTwoAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeTwoAvModal.clickOnJoinButtonOnAvModal();
      //click on skip button to close edit profile popup
      await attendeeTwoPage.locator("button:has-text('Skip')").click();
    });
    await test.step(`organiser logs into the room`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(roomListingPage);

      let roomone_locator = organiserPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const organiserAvModal = new AVModal(organiserPage);
      await organiserAvModal.isVideoStreamLoadedOnAVModal();
      await organiserPage.waitForTimeout(2000);
      await organiserAvModal.clickOnJoinButtonOnAvModal();
    });

    // presence and visibility of AV streams
    await test.step(`verify attendee 1 organiser stream inside room`, async () => {
      attendeeOneRoom = new RoomModule(attendeeOnePage);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify attendee 2 sees attendee 1 and organiser stream inside room`, async () => {
      attendeeTwoRoom = new RoomModule(attendeeTwoPage);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 stream inside room`, async () => {
      organiserRoom = new RoomModule(organiserPage);
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
    });

    //now all users goes to QnA section
    await test.step(`Organiser opens QnA section`, async () => {
      await organiserRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`Attendee1 opens QnA section`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`Attendee2 opens QnA section`, async () => {
      await attendeeTwoRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`verify organiser view is different than of attendee and he sees 2 tabs: For review and Published`, async () => {
      await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyOrganiserViewOnQnATab();
    });

    await test.step(`verify attendee1 view is different than of attendee and he sees 2 tabs: New Questions Tab and Answered Tab and 1 filter to sort question whose default selected state is Upvotes`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyAttendeeViewOnQuestionsTab();
    });

    await test.step(`Attendee1 posts the question1 ${questionToAsk1} via API`, async () => {
      await at1QnaController.postAQuestion(questionToAsk1);
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.clickOnNewQuestionsTab();
    });

    await test.step(`Attendee1 posts the question2 ${questionToAsk2} via API`, async () => {
      await at1QnaController.postAQuestion(questionToAsk2);
    });

    await test.step(`Attendee2 posts the question3 ${questionToAsk3} via API`, async () => {
      await at2QnaController.postAQuestion(questionToAsk3);
      await attendeeTwoRoom.getInteractionPanelComponent.getqNaComponent.clickOnNewQuestionsTab();
    });

    await test.step(`Organiser verifies Select checkbox to be visible`, async () => {
      await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifySelectAllChecboxPresent();
    });

    await test.step(`Organiser now rejects ${questionToAsk1} and ${questionToAsk2} via API`, async () => {
      let questionIdList: string[] = [];
      questionIdList.push(await at1QnaController.getQuestionId(questionToAsk1));
      questionIdList.push(await at1QnaController.getQuestionId(questionToAsk2));
      await orgQnaController.approveOrRejectQuestions(
        questionIdList,
        "REJECTED"
      );
    });

    await test.step(`Verify the following once questions are rejected:`, async () => {
      await test.step(`Organiser sees ${questionToAsk3} in ForReview tab`, async () => {
        await organiserRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyQuestionPresentInReviewTab(
          questionToAsk3
        );
      });

      await test.step(`Organiser verifies ${questionToAsk1} and ${questionToAsk2} moderatedStatus to be REJECTED in ForReview tab via API`, async () => {
        expect(
          await orgQnaController.verifyQuestionModeratedStatuse(
            questionToAsk1,
            "REJECTED"
          )
        ).toEqual("REJECTED");
        expect(
          await orgQnaController.verifyQuestionModeratedStatuse(
            questionToAsk2,
            "REJECTED"
          )
        ).toEqual("REJECTED");
        expect(
          await orgQnaController.verifyQuestionModeratedStatuse(
            questionToAsk3,
            "PENDING"
          )
        ).toEqual("PENDING");
      });

      await test.step(`Attendee2 verifies question ${questionToAsk3} is visible having status 'waiting for review'`, async () => {
        await attendeeTwoRoom.getInteractionPanelComponent.getModeratedqNaComponent.verifyWaitingForReviewTextPresent(
          questionToAsk3
        );
      });

      await test.step(`Attendee1 verifies ${questionToAsk1} and ${questionToAsk2} not visible in NewQuestions tab via API`, async () => {
        expect(
          await at1QnaController.verifyQuestionModeratedStatuse(
            questionToAsk1,
            "REJECTED"
          )
        ).toEqual("REJECTED");
        expect(
          await at1QnaController.verifyQuestionModeratedStatuse(
            questionToAsk1,
            "REJECTED"
          )
        ).toEqual("REJECTED");
        expect(
          await at1QnaController.verifyQuestionModeratedStatuse(
            questionToAsk3,
            "PENDING"
          )
        ).toEqual("PENDING");
      });
    });
  });
});
