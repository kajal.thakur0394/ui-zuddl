import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { AVModal } from "../../../page-objects/events-pages/live-side-components/AvModal";
import { RoomModule } from "../../../page-objects/events-pages/site-modules/Rooms";
import {
  createNewEvent,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { RoomController } from "../../../controller/RoomController";
import { QnAController } from "../../../controller/QnAController";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@rooms @public @interaction-panel`, async () => {
  test.slow();
  let eventId;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let attendeeOneBrowserContext: BrowserContext;
  let eventController: EventController;
  let roomListingPage: string;
  let attendeeLandingPage: string;
  let roomController: RoomController;
  let roomChannelId;
  let qnaController: QnAController;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = orgBrowserContext.request;
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: "room automation",
    });
    attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });

    eventController = new EventController(orgApiContext, eventId);
    //add rooms
    await eventController.getRoomsController.addRoomsToEvent({
      seatsPerRoom: 10,
      numberOfRooms: 1,
      roomCategory: "PUBLIC",
    });

    const roomId = await eventController.fetchRoomIdFromListOfRoomsByRoomName(
      "Room 1"
    );
    console.log(`Room Id for this test case is ${roomId}`);

    //add the room controller
    roomController = new RoomController(orgApiContext, eventId, roomId);
    roomChannelId = await roomController.getChannelId();

    let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
    roomListingPage = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/discussions`;
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
    await attendeeOneBrowserContext?.close();
  });

  test("TC001: @qna Verify E2E Flow of replying by text by ATTENDEE", async () => {
    test.info().annotations.push(
      {
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-514/e2e-flow-of-replying-by-text",
      },
      {
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-511/verify-organiser-view-is-different-than-of-attendee-and-he-sees-2",
      },
      {
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-512/verify-attendee-view-is-different-than-of-attendee-and-he-sees-2-tabs",
      },
      {
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-513/attendee-when-post-question-on-new-questions-tab-if-its-less-than-3",
      }
    );

    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let attendeeOneRoom: RoomModule;
    let organiserRoom: RoomModule;

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    const organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();

    const questionToAsk = "Where is Mumbai located";
    const answerToQuestion = "Maharashtra";

    //messages
    const orgMessageOne = "Hi , I am organiser 1";
    const attendeeMessageOne = "Hi , I am attendee 1";

    await test.step(`organiser invites attendee  1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      await attendeeOnePage.goto(roomListingPage);

      let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeOneAvModal = new AVModal(attendeeOnePage);
      await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
      //click on skip button to close edit profile popup
      await attendeeOnePage.locator("button:has-text('Skip')").click();
    });
    await test.step(`organiser logs into the room`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(roomListingPage);

      let roomone_locator = organiserPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const organiserAvModal = new AVModal(organiserPage);
      await organiserAvModal.isVideoStreamLoadedOnAVModal();
      await organiserPage.waitForTimeout(2000);
      await organiserAvModal.clickOnJoinButtonOnAvModal();
    });

    // presence and visibility of AV streams
    await test.step(`verify attendee 1 organiser stream inside room`, async () => {
      attendeeOneRoom = new RoomModule(attendeeOnePage);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    await test.step(`verify organiser 1 sees attendee 1 stream inside room`, async () => {
      organiserRoom = new RoomModule(organiserPage);
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    // presence and visibility of AV streams after every one has muted their audio and video
    await test.step(`verify attendee 1 sees organiser stream inside room`, async () => {
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 stream inside room`, async () => {
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    //now all users goes to QnA section
    await test.step(`Organiser opens QnA section`, async () => {
      await organiserRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });
    await test.step(`Attendee1 opens QnA section`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`verify organiser view is different than of attendee and he sees 2 filters with selected state as Upvotes and All`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyOrganiserViewOnQuestionsTab();
    });
    await test.step(`verify attendee1 view is different than of attendee and he sees 2 tabs : New Questions Tab and Answered Tab and 1 filter to sort question whose default selected state is Upvotes`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyAttendeeViewOnQuestionsTab();
    });
    await test.step(`verify Attendee1 when post question on new questions tab if its less than 3 words, we show a error message that please ask valid question`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.enterQuestionToAsk(
        "qna"
      );
      await attendeeOneRoom.getInteractionPanelComponent.getNotificationsComponent.verifyNotAValidQuestionMessage();
    });

    await test.step(`Attendee1 posts the question`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.clickOnNewQuestionsTab();
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.enterQuestionToAsk(
        questionToAsk
      );
    });
    await test.step(`Attendee1 verifies question->${questionToAsk} to be visible`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsVisible(
        questionToAsk
      );
    });
    await test.step(`Organiser verifies question->${questionToAsk} to be visible`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsVisible(
        questionToAsk
      );
    });

    await test.step(`Organiser verifies reply text, answer live, and delete button to be present`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyVisibilityOfReplyOrAnswerLiveOrDeleteButton();
    });

    await test.step(`Attendee1 verifies reply text, answer live, and delete button to be invisible`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyInvisibilityOfReplyOrAnswerLiveOrDeleteButton();
    });

    await test.step(`Organiser replies to question->${questionToAsk}`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.replyGivenQuestionWithText(
        questionToAsk,
        answerToQuestion
      );
    });
    await test.step(`Verify toast message appears when organiser replies to question->${questionToAsk} at organiser's side`, async () => {
      await organiserRoom.getInteractionPanelComponent.getNotificationsComponent.verifyReplyQuestionMessage(
        questionToAsk
      );
    });
    await test.step(`Verify toast message appears when organiser replies to question->${questionToAsk} at attendee's side`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getNotificationsComponent.verifyReplyQuestionMessage(
        questionToAsk
      );
    });
    await test.step(`Organiser verifies UI state change after question is replied`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyElementsPresentPostReply(
        questionToAsk,
        answerToQuestion,
        true
      );
    });
    await test.step(`Attendee verifies UI state change after question is replied in Answered tab `, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyElementsPresentPostReply(
        questionToAsk,
        answerToQuestion,
        false
      );
    });
  });

  test("TC002: @qna Verify E2E Flow of replying by going live by ATTENDEE", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-515/e2e-flow-of-replying-by-going-live",
    });

    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let attendeeOneRoom: RoomModule;
    let organiserRoom: RoomModule;

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    const organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    const questionToAsk = "Where is Mumbai located";
    const answerToQuestion = "Maharashtra";

    //messages
    const orgMessageOne = "Hi , I am organiser 1";
    const attendeeMessageOne = "Hi , I am attendee 1";

    await test.step(`organiser invites attendee  1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      await attendeeOnePage.goto(roomListingPage);

      let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeOneAvModal = new AVModal(attendeeOnePage);
      await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
      //click on skip button to close edit profile popup
      await attendeeOnePage.locator("button:has-text('Skip')").click();
    });
    await test.step(`organiser logs into the room`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(roomListingPage);

      let roomone_locator = organiserPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const organiserAvModal = new AVModal(organiserPage);
      await organiserAvModal.isVideoStreamLoadedOnAVModal();
      await organiserPage.waitForTimeout(2000);
      await organiserAvModal.clickOnJoinButtonOnAvModal();
    });

    // presence and visibility of AV streams
    await test.step(`verify attendee 1 organiser stream inside room`, async () => {
      attendeeOneRoom = new RoomModule(attendeeOnePage);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    await test.step(`verify organiser 1 sees attendee 1 stream inside room`, async () => {
      organiserRoom = new RoomModule(organiserPage);
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    // presence and visibility of AV streams after every one has muted their audio and video
    await test.step(`verify attendee 1 sees organiser stream inside room`, async () => {
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 stream inside room`, async () => {
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    //now all users goes to QnA section
    await test.step(`Organiser opens QnA section`, async () => {
      await organiserRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });
    await test.step(`Attendee1 opens QnA section`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`verify organiser view is different than of attendee and he sees 2 filters with selected state as Upvotes and All`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyOrganiserViewOnQuestionsTab();
    });
    await test.step(`verify attendee1 view is different than of attendee and he sees 2 tabs : New Questions Tab and Answered Tab and 1 filter to sort question whose default selected state is Upvotes`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyAttendeeViewOnQuestionsTab();
    });
    await test.step(`verify Attendee1 when post question on new questions tab if its less than 3 words, we show a error message that please ask valid question`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.enterQuestionToAsk(
        "qna"
      );
      await attendeeOneRoom.getInteractionPanelComponent.getNotificationsComponent.verifyNotAValidQuestionMessage();
    });

    await test.step(`Attendee1 posts the question`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.clickOnNewQuestionsTab();
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.enterQuestionToAsk(
        questionToAsk
      );
    });
    await test.step(`Attendee1 verifies question->${questionToAsk} to be visible`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsVisible(
        questionToAsk
      );
    });
    await test.step(`Organiser verifies question->${questionToAsk} to be visible`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsVisible(
        questionToAsk
      );
    });

    await test.step(`Organiser verifies reply text, answer live, and delete button to be present`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyVisibilityOfReplyOrAnswerLiveOrDeleteButton();
    });

    await test.step(`Attendee1 verifies reply text, answer live, and delete button to be invisible`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyInvisibilityOfReplyOrAnswerLiveOrDeleteButton();
    });

    await test.step(`Organiser answers the question live->${questionToAsk}`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.clickOnAnswerLiveTab();
    });
    await test.step(`Verify toast message appears when organiser replies to question->${questionToAsk} at organiser's side`, async () => {
      await organiserRoom.getInteractionPanelComponent.getNotificationsComponent.verifyReplyQuestionMessage(
        questionToAsk
      );
    });
    await test.step(`Verify toast message appears when organiser replies to question->${questionToAsk} at attendee's side`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getNotificationsComponent.verifyReplyQuestionMessage(
        questionToAsk
      );
    });

    await test.step(`Attendee verifies the question is still visible on new questions tab`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyVisibilityOfQuestion(
        questionToAsk
      );
    });
    await test.step(`Organiser verifies UI state change after live answer is clicked`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyElementspresentPostLiveAnswer();
    });
    await test.step(`Now Organiser clicks on Done button to confirm that question has been answered`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.markQuestionBeingAnsweredByLiveAsDone(
        questionToAsk
      );
    });
    await test.step(`Attendee gets a notification saying that Visit Room, your question has been answered`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getNotificationsComponent.verifyVisitRoomMessage();
    });
    await test.step(`Organiser verifies Answered Live button`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionHasBeenAnsweredLiveStatusIsVisible(
        questionToAsk
      );
    });

    await test.step(`Attendee switches to answered tab and see his question there`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.clickOnAnsweredTab();
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionHasBeenAnsweredLiveStatusIsVisible(
        questionToAsk
      );
    });
  });

  test("TC003: @qna Verify E2E Flow of replying by text by ORGANIZER", async () => {
    test.info().annotations.push(
      {
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-516/verify-organiser-also-have-access-to-submit-a-question-that-will-be",
      },
      {
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-511/verify-organiser-view-is-different-than-of-attendee-and-he-sees-2",
      },
      {
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-512/verify-attendee-view-is-different-than-of-attendee-and-he-sees-2-tabs",
      },
      {
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-513/attendee-when-post-question-on-new-questions-tab-if-its-less-than-3",
      }
    );

    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let attendeeOneRoom: RoomModule;
    let organiserRoom: RoomModule;

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    const organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    const questionToAsk = "Where is Mumbai located";
    const answerToQuestion = "Maharashtra";

    //messages
    const orgMessageOne = "Hi , I am organiser 1";
    const attendeeMessageOne = "Hi , I am attendee 1";

    await test.step(`organiser invites attendee  1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      await attendeeOnePage.goto(roomListingPage);

      let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeOneAvModal = new AVModal(attendeeOnePage);
      await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
      //click on skip button to close edit profile popup
      await attendeeOnePage.locator("button:has-text('Skip')").click();
    });
    await test.step(`organiser logs into the room`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(roomListingPage);

      let roomone_locator = organiserPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const organiserAvModal = new AVModal(organiserPage);
      await organiserAvModal.isVideoStreamLoadedOnAVModal();
      await organiserPage.waitForTimeout(2000);
      await organiserAvModal.clickOnJoinButtonOnAvModal();
    });

    // presence and visibility of AV streams
    await test.step(`verify attendee 1 organiser stream inside room`, async () => {
      attendeeOneRoom = new RoomModule(attendeeOnePage);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    await test.step(`verify organiser 1 sees attendee 1 stream inside room`, async () => {
      organiserRoom = new RoomModule(organiserPage);
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    // presence and visibility of AV streams after every one has muted their audio and video
    await test.step(`verify attendee 1 sees organiser stream inside room`, async () => {
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 stream inside room`, async () => {
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    //now all users goes to QnA section
    await test.step(`Organiser opens QnA section`, async () => {
      await organiserRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });
    await test.step(`Attendee1 opens QnA section`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`verify organiser view is different than of attendee and he sees 2 filters with selected state as Upvotes and All`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyOrganiserViewOnQuestionsTab();
    });
    await test.step(`verify attendee1 view is different than of attendee and he sees 2 tabs : New Questions Tab and Answered Tab and 1 filter to sort question whose default selected state is Upvotes`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyAttendeeViewOnQuestionsTab();
    });
    await test.step(`verify Organiser when post question on new questions tab if its less than 3 words, we show a error message that please ask valid question`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.enterQuestionToAsk(
        "qna"
      );
      await organiserRoom.getInteractionPanelComponent.getNotificationsComponent.verifyNotAValidQuestionMessage();
    });

    await test.step(`Organiser posts the question`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.enterQuestionToAsk(
        questionToAsk
      );
    });
    await test.step(`Attendee1 verifies question->${questionToAsk} to be visible`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsVisible(
        questionToAsk
      );
    });
    await test.step(`Organiser verifies question->${questionToAsk} to be visible`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsVisible(
        questionToAsk
      );
    });

    await test.step(`Organiser verifies reply text, answer live, and delete button to be present`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyVisibilityOfReplyOrAnswerLiveOrDeleteButton();
    });

    await test.step(`Attendee1 verifies reply text, answer live, and delete button to be invisible`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyInvisibilityOfReplyOrAnswerLiveOrDeleteButton();
    });

    await test.step(`Organiser replies to question->${questionToAsk}`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.replyGivenQuestionWithText(
        questionToAsk,
        answerToQuestion
      );
    });
    await test.step(`Verify toast message appears when organiser replies to question->${questionToAsk} at organiser's side`, async () => {
      await organiserRoom.getInteractionPanelComponent.getNotificationsComponent.verifyReplyQuestionMessage(
        questionToAsk
      );
    });
    await test.step(`Verify toast message appears when organiser replies to question->${questionToAsk} at attendee's side`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getNotificationsComponent.verifyReplyQuestionMessage(
        questionToAsk
      );
    });
    await test.step(`Organiser verifies UI state change after question is replied`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyElementsPresentPostReply(
        questionToAsk,
        answerToQuestion,
        true
      );
    });
    await test.step(`Attendee verifies UI state change after question is replied in Answered tab `, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyElementsPresentPostReply(
        questionToAsk,
        answerToQuestion,
        false
      );
    });
  });

  test("TC004: @qna Verify E2E Flow of replying by going live by ORGANIZER", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-516/verify-organiser-also-have-access-to-submit-a-question-that-will-be",
    });

    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let attendeeOneRoom: RoomModule;
    let organiserRoom: RoomModule;

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    const organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    const questionToAsk = "Where is Mumbai located";
    const answerToQuestion = "Maharashtra";

    //messages
    const orgMessageOne = "Hi , I am organiser 1";
    const attendeeMessageOne = "Hi , I am attendee 1";

    await test.step(`organiser invites attendee  1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      await attendeeOnePage.goto(roomListingPage);

      let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeOneAvModal = new AVModal(attendeeOnePage);
      await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
      //click on skip button to close edit profile popup
      await attendeeOnePage.locator("button:has-text('Skip')").click();
    });
    await test.step(`organiser logs into the room`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(roomListingPage);

      let roomone_locator = organiserPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const organiserAvModal = new AVModal(organiserPage);
      await organiserAvModal.isVideoStreamLoadedOnAVModal();
      await organiserPage.waitForTimeout(2000);
      await organiserAvModal.clickOnJoinButtonOnAvModal();
    });

    // presence and visibility of AV streams
    await test.step(`verify attendee 1 organiser stream inside room`, async () => {
      attendeeOneRoom = new RoomModule(attendeeOnePage);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    await test.step(`verify organiser 1 sees attendee 1 stream inside room`, async () => {
      organiserRoom = new RoomModule(organiserPage);
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    // presence and visibility of AV streams after every one has muted their audio and video
    await test.step(`verify attendee 1 sees organiser stream inside room`, async () => {
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 stream inside room`, async () => {
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    //now all users goes to QnA section
    await test.step(`Organiser opens QnA section`, async () => {
      await organiserRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });
    await test.step(`Attendee1 opens QnA section`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`verify organiser view is different than of attendee and he sees 2 filters with selected state as Upvotes and All`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyOrganiserViewOnQuestionsTab();
    });
    await test.step(`verify attendee1 view is different than of attendee and he sees 2 tabs : New Questions Tab and Answered Tab and 1 filter to sort question whose default selected state is Upvotes`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyAttendeeViewOnQuestionsTab();
    });
    await test.step(`verify organiser when post question on new questions tab if its less than 3 words, we show a error message that please ask valid question`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.enterQuestionToAsk(
        "qna"
      );
      await organiserRoom.getInteractionPanelComponent.getNotificationsComponent.verifyNotAValidQuestionMessage();
    });

    await test.step(`organiser posts the question`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.enterQuestionToAsk(
        questionToAsk
      );
    });
    await test.step(`Attendee1 verifies question->${questionToAsk} to be visible`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsVisible(
        questionToAsk
      );
    });
    await test.step(`Organiser verifies question->${questionToAsk} to be visible`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsVisible(
        questionToAsk
      );
    });

    await test.step(`Organiser verifies reply text, answer live, and delete button to be present`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyVisibilityOfReplyOrAnswerLiveOrDeleteButton();
    });

    await test.step(`Attendee1 verifies reply text, answer live, and delete button to be invisible`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyInvisibilityOfReplyOrAnswerLiveOrDeleteButton();
    });

    await test.step(`Organiser answers the question live->${questionToAsk}`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.clickOnAnswerLiveTab();
    });
    await test.step(`Verify toast message appears when organiser replies to question->${questionToAsk} at organiser's side`, async () => {
      await organiserRoom.getInteractionPanelComponent.getNotificationsComponent.verifyReplyQuestionMessage(
        questionToAsk
      );
    });
    await test.step(`Verify toast message appears when organiser replies to question->${questionToAsk} at attendee's side`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getNotificationsComponent.verifyReplyQuestionMessage(
        questionToAsk
      );
    });

    await test.step(`Attendee verifies the question is still visible on new questions tab`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyVisibilityOfQuestion(
        questionToAsk
      );
    });
    await test.step(`Organiser verifies UI state change after live answer is clicked`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyElementspresentPostLiveAnswer();
    });
    await test.step(`Now Organiser clicks on Done button to confirm that question has been answered`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.markQuestionBeingAnsweredByLiveAsDone(
        questionToAsk
      );
    });
    await test.step(`Organiser gets a notification saying that Visit Room, your question has been answered`, async () => {
      await organiserRoom.getInteractionPanelComponent.getNotificationsComponent.verifyVisitRoomMessage();
    });
    await test.step(`Organiser verifies Answered Live button`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionHasBeenAnsweredLiveStatusIsVisible(
        questionToAsk
      );
    });

    await test.step(`Attendee switches to answered tab and see his question there`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.clickOnAnsweredTab();
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionHasBeenAnsweredLiveStatusIsVisible(
        questionToAsk
      );
    });
  });

  test("TC005: @qna Verify delete question functionality before answering the question", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-517/verify-delete-question-functionality-before-answering-the-question",
    });
    qnaController = new QnAController(orgApiContext, eventId, roomChannelId);
    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let attendeeOneRoom: RoomModule;
    let organiserRoom: RoomModule;

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    const organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    const questionToAsk = "Where is Mumbai located";

    //messages
    const orgMessageOne = "Hi , I am organiser 1";
    const attendeeMessageOne = "Hi , I am attendee 1";

    await test.step(`organiser invites attendee  1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      await attendeeOnePage.goto(roomListingPage);

      let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeOneAvModal = new AVModal(attendeeOnePage);
      await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
      //click on skip button to close edit profile popup
      await attendeeOnePage.locator("button:has-text('Skip')").click();
    });
    await test.step(`organiser logs into the room`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(roomListingPage);

      let roomone_locator = organiserPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const organiserAvModal = new AVModal(organiserPage);
      await organiserAvModal.isVideoStreamLoadedOnAVModal();
      await organiserPage.waitForTimeout(2000);
      await organiserAvModal.clickOnJoinButtonOnAvModal();
    });

    // presence and visibility of AV streams
    await test.step(`verify attendee 1 organiser stream inside room`, async () => {
      attendeeOneRoom = new RoomModule(attendeeOnePage);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    await test.step(`verify organiser 1 sees attendee 1 stream inside room`, async () => {
      organiserRoom = new RoomModule(organiserPage);
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    // presence and visibility of AV streams after every one has muted their audio and video
    await test.step(`verify attendee 1 sees organiser stream inside room`, async () => {
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 stream inside room`, async () => {
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    //now all users goes to QnA section
    await test.step(`Organiser opens QnA section`, async () => {
      await organiserRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });
    await test.step(`Attendee1 opens QnA section`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`verify organiser view is different than of attendee and he sees 2 filters with selected state as Upvotes and All`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyOrganiserViewOnQuestionsTab();
    });
    await test.step(`verify attendee1 view is different than of attendee and he sees 2 tabs : New Questions Tab and Answered Tab and 1 filter to sort question whose default selected state is Upvotes`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyAttendeeViewOnQuestionsTab();
    });
    await test.step(`verify Attendee1 when posts the question via API`, async () => {
      await qnaController.postAQuestion(questionToAsk);
    });
    await test.step(`Attendee1 verifies question->${questionToAsk} to be visible`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsVisible(
        questionToAsk
      );
    });
    await test.step(`Organiser verifies question->${questionToAsk} to be visible`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsVisible(
        questionToAsk
      );
    });

    await test.step(`Organiser deletes the question via API`, async () => {
      await qnaController.deleteAQuestion(questionToAsk);
    });
    await test.step(`Attendee1 verifies question->${questionToAsk} to be Invisible`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsNotVisible(
        questionToAsk
      );
    });

    await test.step(`Organiser verifies question->${questionToAsk} to be Invisible`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsNotVisible(
        questionToAsk
      );
    });
  });

  test("TC006: @qna Verify upvote/downvote functionality", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-518/verify-upvote-functionality",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-519/verify-downvote-functionality",
    });

    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let attendeeOneRoom: RoomModule;
    let organiserRoom: RoomModule;
    qnaController = new QnAController(
      attendeeOneBrowserContext.request,
      eventId,
      roomChannelId
    );
    let orgqnaController = new QnAController(
      orgBrowserContext.request,
      eventId,
      roomChannelId
    );

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    const organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    const questionToAsk = "Where is Mumbai located";

    //messages
    const orgMessageOne = "Hi , I am organiser 1";
    const attendeeMessageOne = "Hi , I am attendee 1";

    await test.step(`organiser invites attendee  1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      await attendeeOnePage.goto(roomListingPage);

      let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeOneAvModal = new AVModal(attendeeOnePage);
      await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
      //click on skip button to close edit profile popup
      await attendeeOnePage.locator("button:has-text('Skip')").click();
    });
    await test.step(`organiser logs into the room`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(roomListingPage);

      let roomone_locator = organiserPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const organiserAvModal = new AVModal(organiserPage);
      await organiserAvModal.isVideoStreamLoadedOnAVModal();
      await organiserPage.waitForTimeout(2000);
      await organiserAvModal.clickOnJoinButtonOnAvModal();
    });

    // presence and visibility of AV streams
    await test.step(`verify attendee 1 organiser stream inside room`, async () => {
      attendeeOneRoom = new RoomModule(attendeeOnePage);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    await test.step(`verify organiser 1 sees attendee 1 stream inside room`, async () => {
      organiserRoom = new RoomModule(organiserPage);
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    // presence and visibility of AV streams after every one has muted their audio and video
    await test.step(`verify attendee 1 sees organiser stream inside room`, async () => {
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 stream inside room`, async () => {
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    //now all users goes to QnA section
    await test.step(`Organiser opens QnA section`, async () => {
      await organiserRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });
    await test.step(`Attendee1 opens QnA section`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`verify organiser view is different than of attendee and he sees 2 filters with selected state as Upvotes and All`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyOrganiserViewOnQuestionsTab();
    });
    await test.step(`verify attendee1 view is different than of attendee and he sees 2 tabs : New Questions Tab and Answered Tab and 1 filter to sort question whose default selected state is Upvotes`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyAttendeeViewOnQuestionsTab();
    });
    await test.step(`Now, Attendee1 when posts the question via API`, async () => {
      await qnaController.postAQuestion(questionToAsk);
    });
    await test.step(`Attendee1 verifies question->${questionToAsk} to be visible`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsVisible(
        questionToAsk
      );
    });
    await test.step(`Organiser verifies question->${questionToAsk} to be visible`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsVisible(
        questionToAsk
      );
    });

    await test.step(`Attendee verifies toast message to appear after upvoting his question and the upvote count`, async () => {
      const beforeUpvoteCount =
        await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.getUpvoteCount();
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.clickOnUpvoteButton();
      await attendeeOneRoom.getInteractionPanelComponent.getNotificationsComponent.verifyUpvoteMessage();
      const afterUpvoteCount =
        await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.getUpvoteCount();
      expect(beforeUpvoteCount).not.toEqual(afterUpvoteCount);
    });

    await test.step(`Attendee verifies button changed to upvoted`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyUpvotesButtonPresence();
    });

    await test.step(`Organiser verifies upvote count to be 1`, async () => {
      expect(await orgqnaController.getUpvoteCount(questionToAsk)).toEqual(1);
    });

    await test.step(`Attendee now downvotes the same question`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.clickOnDownvoteButton();
    });
    await test.step(`Attendee verifies upvote count to be 0`, async () => {
      expect(await qnaController.getUpvoteCount(questionToAsk)).toEqual(0);
    });
    await test.step(`Organiser verifies upvote count to be 0`, async () => {
      expect(await orgqnaController.getUpvoteCount(questionToAsk)).toEqual(0);
    });
  });

  test("TC007: @qna Verify filters on organiser side and attendee side", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-520/verify-filters-on-organiser-side-and-attendee-side",
    });
    let attendeeTwoBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let attendeeTwoPage = await attendeeTwoBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let attendeeOneRoom: RoomModule;
    let attendeeTwoRoom: RoomModule;
    let organiserRoom: RoomModule;

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const attendeeTwoFirstName = "prateekAttendeeTwo";
    const attendeeTwoLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    const organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let attendeeEmailTwo = DataUtl.getRandomAttendeeEmail();
    const questionToAsk1 = "Question no 1";
    const questionToAsk2 = "Question no 2";
    const questionToAsk3 = "Question no 3";
    let timestampMap = new Map<string, Date>();
    qnaController = new QnAController(
      attendeeOneBrowserContext.request,
      eventId,
      roomChannelId
    );
    let qnaController2 = new QnAController(
      attendeeTwoBrowserContext.request,
      eventId,
      roomChannelId
    );

    //messages
    const orgMessageOne = "Hi , I am organiser 1";
    const attendeeMessageOne = "Hi , I am attendee 1";

    await test.step(`organiser invites attendee  1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });
    await test.step(`organiser invites attendee  2 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeTwoFirstName,
        lastName: attendeeTwoLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailTwo,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      await attendeeOnePage.goto(roomListingPage);

      let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeOneAvModal = new AVModal(attendeeOnePage);
      await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
      //click on skip button to close edit profile popup
      await attendeeOnePage.locator("button:has-text('Skip')").click();
    });
    await test.step(`attendee 2 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailTwo
      );
      await attendeeTwoPage.goto(magicLink);
      await attendeeTwoPage.click(`text=Enter Now`);
      await attendeeTwoPage.waitForURL(/lobby/);
      await attendeeTwoPage.goto(roomListingPage);
      let roomone_locator = attendeeTwoPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeTwoAvModal = new AVModal(attendeeTwoPage);
      await attendeeTwoAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeTwoAvModal.clickOnJoinButtonOnAvModal();
      await attendeeTwoPage.locator("button:has-text('Skip')").click();
    });
    await test.step(`organiser logs into the room`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(roomListingPage);

      let roomone_locator = organiserPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const organiserAvModal = new AVModal(organiserPage);
      await organiserAvModal.isVideoStreamLoadedOnAVModal();
      await organiserPage.waitForTimeout(2000);
      await organiserAvModal.clickOnJoinButtonOnAvModal();
    });

    // presence and visibility of AV streams
    await test.step(`verify attendee 1 organiser stream inside room`, async () => {
      attendeeOneRoom = new RoomModule(attendeeOnePage);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify attendee 2 sees attendee 1 and organiser stream inside room`, async () => {
      attendeeTwoRoom = new RoomModule(attendeeTwoPage);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 stream inside room`, async () => {
      organiserRoom = new RoomModule(organiserPage);
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    // presence and visibility of AV streams after every one has muted their audio and video
    await test.step(`verify attendee 1 sees organiser stream inside room`, async () => {
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify attendee 2 sees attendee 1 and organiser stream inside room`, async () => {
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 stream inside room`, async () => {
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    //now all users goes to QnA section
    await test.step(`Organiser opens QnA section`, async () => {
      await organiserRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });
    await test.step(`Attendee1 opens QnA section`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });
    await test.step(`Attendee2 opens QnA section`, async () => {
      await attendeeTwoRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`verify organiser view is different than of attendee and he sees 2 filters with selected state as Upvotes and All`, async () => {
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyOrganiserViewOnQuestionsTab();
    });
    await test.step(`verify attendee1 view is different than of organiser and he sees 2 tabs : New Questions Tab and Answered Tab and 1 filter to sort question whose default selected state is Upvotes`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyAttendeeViewOnQuestionsTab();
    });

    await test.step(`verify attendee2 view is different than of organiser and he sees 2 tabs : New Questions Tab and Answered Tab and 1 filter to sort question whose default selected state is Upvotes`, async () => {
      await attendeeTwoRoom.getInteractionPanelComponent.getqNaComponent.verifyAttendeeViewOnQuestionsTab();
    });

    await test.step(`Filter by Latest`, async () => {
      await test.step(`Now, Attendee1  posts and upvotes the question1 via API`, async () => {
        await qnaController.postAQuestion(questionToAsk1);
        let time1 = await qnaController.upvoteAQuestion(questionToAsk1);
        timestampMap.set(questionToAsk1, new Date(time1));
      });

      await test.step(`Now, Attendee1 posts the question2 via API`, async () => {
        await qnaController.postAQuestion(questionToAsk2);
      });

      await test.step(`Now, Attendee1 posts and upvotes the question3 via API`, async () => {
        await qnaController.postAQuestion(questionToAsk3);
        let time2 = await qnaController.upvoteAQuestion(questionToAsk3);
        timestampMap.set(questionToAsk3, new Date(time2));
        timestampMap = new Map(
          [...timestampMap.entries()].sort(
            (a, b) => b[1].getTime() - a[1].getTime()
          )
        );
      });

      await test.step(`Now, Attendee2 upvotes the question3 via API`, async () => {
        await qnaController2.upvoteAQuestion(questionToAsk3);
      });

      await test.step(`Now, Organiser filters question by Latest and verify the question in list appears on their created timestamp order`, async () => {
        await organiserRoom.getInteractionPanelComponent.getqNaComponent.selectFilterDropdown(
          "Latest"
        );
        await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyTimeStampOrderByLatest(
          [...timestampMap.keys()]
        );
      });
    });

    await test.step(`Verify Filter by Upvotes`, async () => {
      await test.step(`Now, Organiser filters question by Upvotes and verify maximum upvotes is visible on top`, async () => {
        await organiserRoom.getInteractionPanelComponent.getqNaComponent.selectUpvoteOrLatestDropdown(
          "Upvotes"
        );
        expect(await qnaController.getUpvoteCount(questionToAsk3)).toEqual(2);
        await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyMaxUpvoteCount(
          2
        );
      });
    });

    await test.step(`Verify Filter by Answered (Org side)`, async () => {
      //upvote+answered
      await test.step(`Organiser replies to ${questionToAsk3}`, async () => {
        await qnaController.replyAQuestion(questionToAsk3, "answer no 3");
        // await organiserRoom.getInteractionPanelComponent.getqNaComponent.replyGivenQuestionWithText(questionToAsk3, "answer no 3");
        await organiserRoom.getInteractionPanelComponent.getqNaComponent.selectAnsweredOrUnAnsweredDropdown(
          "Answered"
        );
        await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsVisible(
          questionToAsk3
        );
      });
    });

    await test.step(`Verify Filter by Unanswered (Org side)`, async () => {
      //upvote+unanswered
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.selectAnsweredOrUnAnsweredDropdown(
        "Unanswered"
      );
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsVisible(
        questionToAsk1
      );
    });

    await test.step(`Verify Filter by All (Org side)`, async () => {
      //upvote+unanswered+answered
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.selectAnsweredOrUnAnsweredDropdown(
        "All"
      );
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsVisible(
        questionToAsk1
      );
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsVisible(
        questionToAsk2
      );
      await organiserRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsVisible(
        questionToAsk3
      );
    });

    await test.step(`Verify Filter by answer with text(attendee side) + sort by upvotes`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.clickOnAnsweredTab();
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.selectAnswerWithTextOrMyQuestionsDropdown(
        "Answered with text"
      );
      await attendeeOneRoom.getInteractionPanelComponent.getqNaComponent.verifyQuestionWithGivenTextIsVisible(
        questionToAsk3
      );
    });
  });
});
