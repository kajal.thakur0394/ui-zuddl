import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { AVModal } from "../../../page-objects/events-pages/live-side-components/AvModal";
import { RoomModule } from "../../../page-objects/events-pages/site-modules/Rooms";
import {
  createNewEvent,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { RoomController } from "../../../controller/RoomController";
import { QueryUtil } from "../../../DB/QueryUtil";

test.describe.parallel(`@rooms @public @interaction-panel`, async () => {
  test.slow();
  //test.describe.configure({ retries: 1 });
  let eventId;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let attendeeOneBrowserContext: BrowserContext;
  let attendeeTwoBrowserContext: BrowserContext;
  let eventController: EventController;
  let roomListingPage: string;
  let attendeeLandingPage: string;
  let roomController: RoomController;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = orgBrowserContext.request;
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: "room automation",
    });
    attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    attendeeTwoBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });

    eventController = new EventController(orgApiContext, eventId);
    //add rooms
    await eventController.getRoomsController.addRoomsToEvent({
      seatsPerRoom: 10,
      numberOfRooms: 1,
      roomCategory: "PUBLIC",
    });

    const roomId = await eventController.fetchRoomIdFromListOfRoomsByRoomName(
      "Room 1"
    );
    console.log(`Room Id for this test case is ${roomId}`);

    //add the room controller
    roomController = new RoomController(orgApiContext, eventId, roomId);

    await test.step(`Enable the moderated chat for this room`, async () => {
      await roomController.updateRoomEngaementSettingsForThisRoom({
        enableChat: true,
        enableModeratedChat: true,
      });
    });

    let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
    roomListingPage = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/discussions`;
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
    await attendeeOneBrowserContext?.close();
    await attendeeTwoBrowserContext?.close();
  });

  test("TC001: @moderated-chat verify e2e flow of attendee posting a chat message and organiser moderate it to approve it ", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-486/verify-organiser-is-able-to-enable-disable-moderated-chat-inside-rooms",
    });

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-487/verify-organiser-only-sees-moderated-chat-button-and-attendees-do-not",
    });

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-488/verify-flow-of-chat-message-approval-by-organiser",
    });
    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let attendeeTwoPage = await attendeeTwoBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let attendeeOneRoom: RoomModule;
    let attendeeTwoRoom: RoomModule;
    let organiserRoom: RoomModule;

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const attendeeTwoFirstName = "prateekAttendeeTwo";
    const attendeeTwoLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    const organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let attendeeEmailTwo = DataUtl.getRandomAttendeeEmail();

    //messages
    const orgMessageOne = "Hi , I am organiser 1";
    const attendeeMessageOne = "Hi , I am attendee 1";

    await test.step(`organiser invites attendee  1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await test.step(`organiser invites attendee  2 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeTwoFirstName,
        lastName: attendeeTwoLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailTwo,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      await attendeeOnePage.goto(roomListingPage);

      let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeOneAvModal = new AVModal(attendeeOnePage);
      await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
      //click on skip button to close edit profile popup
      await attendeeOnePage.locator("button:has-text('Skip')").click();
    });

    await test.step(`attendee 2 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailTwo
      );
      await attendeeTwoPage.goto(magicLink);
      await attendeeTwoPage.click(`text=Enter Now`);
      await attendeeTwoPage.waitForURL(/lobby/);
      await attendeeTwoPage.goto(roomListingPage);
      let roomone_locator = attendeeTwoPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeTwoAvModal = new AVModal(attendeeTwoPage);
      await attendeeTwoAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeTwoAvModal.clickOnJoinButtonOnAvModal();
      await attendeeTwoPage.locator("button:has-text('Skip')").click();
    });
    await test.step(`organiser logs into the room`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(roomListingPage);

      let roomone_locator = organiserPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const organiserAvModal = new AVModal(organiserPage);
      await organiserAvModal.isVideoStreamLoadedOnAVModal();
      await organiserPage.waitForTimeout(2000);
      await organiserAvModal.clickOnJoinButtonOnAvModal();
    });

    // presence and visibility of AV streams
    await test.step(`verify attendee 1 sees attendee 2 and organiser stream inside room`, async () => {
      attendeeOneRoom = new RoomModule(attendeeOnePage);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify attendee 2 sees attendee 1 and organiser stream inside room`, async () => {
      attendeeTwoRoom = new RoomModule(attendeeTwoPage);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 and attendee 2 stream inside room`, async () => {
      organiserRoom = new RoomModule(organiserPage);
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    // presence and visibility of AV streams after every one has muted their audio and video
    await test.step(`verify attendee 1 sees attendee 2 and organiser stream inside room`, async () => {
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify attendee 2 sees attendee 1 and organiser stream inside room`, async () => {
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 and attendee 2 stream inside room`, async () => {
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    //now attendee clicks on interaction panel
    await test.step(`Verify organiser messages does not go under moderation and are visible to all user present inside room`, async () => {
      await organiserRoom.getInteractionPanelComponent.getChatComponent.sendChatMessage(
        orgMessageOne
      );

      await test.step(`Verify organiser is able to see his message on chat panel`, async () => {
        await organiserRoom.getInteractionPanelComponent.getChatComponent.verifyMessageWithGivenContentIsVisible(
          orgMessageOne
        );
      });
      await test.step(`Verify attendee 1 is able to see his message on chat panel`, async () => {
        await attendeeOneRoom.getInteractionPanelComponent.getChatComponent.verifyMessageWithGivenContentIsVisible(
          orgMessageOne
        );
      });
      await test.step(`Verify attendee 2 is able to see his message on chat panel`, async () => {
        await attendeeTwoRoom.getInteractionPanelComponent.getChatComponent.verifyMessageWithGivenContentIsVisible(
          orgMessageOne
        );
      });
    });

    await test.step(`Verify attendee 1 if posts message is not visible to anyone inside chat container and is visible to org under moderation container`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getChatComponent.sendChatMessage(
        attendeeMessageOne
      );
      await test.step(`Verify organiser is not able to see his message on chat panel`, async () => {
        await organiserRoom.getInteractionPanelComponent.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });
      await test.step(`Verify attendee 1 is not  able to see his message on chat panel`, async () => {
        await attendeeOneRoom.getInteractionPanelComponent.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });
      await test.step(`Verify attendee 2 is not able to see his message on chat panel`, async () => {
        await attendeeTwoRoom.getInteractionPanelComponent.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });

      await test.step(`verify organiser sees chat moderator button inside chat contianer`, async () => {
        await organiserRoom.getInteractionPanelComponent.getChatComponent.verifyModeratedChatButtonIsVisibleOnChatContainer();
      });

      await test.step(`verify attendees does not sees chat moderator button inside chat contianer`, async () => {
        await attendeeOneRoom.getInteractionPanelComponent.getChatComponent.verifyModeratedChatButtonIsNotVisibleOnChatContainer();
      });
    });

    await test.step(`Organiser clicks on moderated chat button, then approves the message, then verify its visible to all on chat container `, async () => {
      await test.step("organiser clicks on chat moderator button and verify moderated chat container is visible", async () => {
        await organiserRoom.getInteractionPanelComponent.getChatComponent.updateStatusOfChatWaitingForModeration(
          attendeeMessageOne,
          true
        );
      });
    });

    await test.step(`Now verify post approval by organiser, chat message is visible in the chat container to all user`, async () => {
      await test.step(`Verify organiser is now able to see attendee 1 message on chat panel`, async () => {
        await organiserRoom.getInteractionPanelComponent.getChatComponent.verifyMessageWithGivenContentIsVisible(
          attendeeMessageOne
        );
      });
      await test.step(`Verify attendee 1 is now  able to see his message on chat panel`, async () => {
        await attendeeOneRoom.getInteractionPanelComponent.getChatComponent.verifyMessageWithGivenContentIsVisible(
          attendeeMessageOne
        );
      });
      await test.step(`Verify attendee 2 is now  able to see attendee 1 message on chat panel`, async () => {
        await attendeeTwoRoom.getInteractionPanelComponent.getChatComponent.verifyMessageWithGivenContentIsVisible(
          attendeeMessageOne
        );
      });
    });

    // Now every one exits from the room
    await test.step(`verify attendee 1 is able to exit the room and land on room listing page`, async () => {
      await attendeeOneRoom.clickOnExitRoom();
      await attendeeOneRoom.handleExitConfirmationPopup(true);
    });

    // attendee two exit from the room
    await test.step(`verify attendee 2 is able to exit the room and land on room listing page`, async () => {
      await attendeeTwoRoom.clickOnExitRoom();
      await attendeeTwoRoom.handleExitConfirmationPopup(true);
    });

    // organiser exits from the room
    await test.step(`verify organiser is able to exit the room and land on room listing page`, async () => {
      await organiserRoom.clickOnExitRoom();
      await organiserRoom.handleExitConfirmationPopup(true);
    });
  });

  test("TC002: @moderated-chat verify e2e flow of attendee posting a chat message and organiser moderate it to reject it ", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-486/verify-organiser-is-able-to-enable-disable-moderated-chat-inside-rooms",
    });

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-487/verify-organiser-only-sees-moderated-chat-button-and-attendees-do-not",
    });

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-489/verify-flow-of-chat-rejection-under-moderation-by-organiser",
    });
    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let attendeeTwoPage = await attendeeTwoBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    let attendeeOneRoom: RoomModule;
    let attendeeTwoRoom: RoomModule;
    let organiserRoom: RoomModule;

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const attendeeTwoFirstName = "prateekAttendeeTwo";
    const attendeeTwoLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    const organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let attendeeEmailTwo = DataUtl.getRandomAttendeeEmail();

    //messages
    const orgMessageOne = "Hi , I am organiser 1";
    const attendeeMessageOne = "Hi , I am attendee 1";

    await test.step(`organiser invites attendee  1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await test.step(`organiser invites attendee  2 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeTwoFirstName,
        lastName: attendeeTwoLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailTwo,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );

      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      await attendeeOnePage.goto(roomListingPage);

      let roomone_locator = attendeeOnePage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeOneAvModal = new AVModal(attendeeOnePage);
      await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
      //click on skip button to close edit profile popup
      await attendeeOnePage.locator("button:has-text('Skip')").click();
    });

    await test.step(`attendee 2 logs into the room`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailTwo
      );
      await attendeeTwoPage.goto(magicLink);
      await attendeeTwoPage.click(`text=Enter Now`);
      await attendeeTwoPage.waitForURL(/lobby/);
      await attendeeTwoPage.goto(roomListingPage);
      let roomone_locator = attendeeTwoPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const attendeeTwoAvModal = new AVModal(attendeeTwoPage);
      await attendeeTwoAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeTwoAvModal.clickOnJoinButtonOnAvModal();
      await attendeeTwoPage.locator("button:has-text('Skip')").click();
    });
    await test.step(`organiser logs into the room`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(roomListingPage);

      let roomone_locator = organiserPage.locator("text=Enter").nth(0);
      await roomone_locator.click();

      const organiserAvModal = new AVModal(organiserPage);
      await organiserAvModal.isVideoStreamLoadedOnAVModal();
      await organiserPage.waitForTimeout(2000);
      await organiserAvModal.clickOnJoinButtonOnAvModal();
    });

    // presence and visibility of AV streams
    await test.step(`verify attendee 1 sees attendee 2 and organiser stream inside room`, async () => {
      attendeeOneRoom = new RoomModule(attendeeOnePage);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify attendee 2 sees attendee 1 and organiser stream inside room`, async () => {
      attendeeTwoRoom = new RoomModule(attendeeTwoPage);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 and attendee 2 stream inside room`, async () => {
      organiserRoom = new RoomModule(organiserPage);
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    // presence and visibility of AV streams after every one has muted their audio and video
    await test.step(`verify attendee 1 sees attendee 2 and organiser stream inside room`, async () => {
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await attendeeOneRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify attendee 2 sees attendee 1 and organiser stream inside room`, async () => {
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await attendeeTwoRoom.verifyUserStreamIsVisible(organiserFirstName);
    });
    await test.step(`verify organiser 1 sees attendee 1 and attendee 2 stream inside room`, async () => {
      await organiserRoom.verifyUserStreamIsVisible(attendeeOneFirstName);
      await organiserRoom.verifyUserStreamIsVisible(attendeeTwoFirstName);
      await organiserRoom.verifyUserStreamIsVisible(organiserFirstName);
    });

    //now attendee clicks on interaction panel
    await test.step(`Verify organiser messages does not go under moderation and are visible to all user present inside room`, async () => {
      await organiserRoom.getInteractionPanelComponent.getChatComponent.sendChatMessage(
        orgMessageOne
      );

      await test.step(`Verify organiser is able to see his message on chat panel`, async () => {
        await organiserRoom.getInteractionPanelComponent.getChatComponent.verifyMessageWithGivenContentIsVisible(
          orgMessageOne
        );
      });
      await test.step(`Verify attendee 1 is able to see his message on chat panel`, async () => {
        await attendeeOneRoom.getInteractionPanelComponent.getChatComponent.verifyMessageWithGivenContentIsVisible(
          orgMessageOne
        );
      });
      await test.step(`Verify attendee 2 is able to see his message on chat panel`, async () => {
        await attendeeTwoRoom.getInteractionPanelComponent.getChatComponent.verifyMessageWithGivenContentIsVisible(
          orgMessageOne
        );
      });
    });

    await test.step(`Verify attendee 1 if posts message is not visible to anyone inside chat container and is visible to org under moderation container`, async () => {
      await attendeeOneRoom.getInteractionPanelComponent.getChatComponent.sendChatMessage(
        attendeeMessageOne
      );
      await test.step(`Verify organiser is not able to see his message on chat panel`, async () => {
        await organiserRoom.getInteractionPanelComponent.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });
      await test.step(`Verify attendee 1 is not  able to see his message on chat panel`, async () => {
        await attendeeOneRoom.getInteractionPanelComponent.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });
      await test.step(`Verify attendee 2 is not able to see his message on chat panel`, async () => {
        await attendeeTwoRoom.getInteractionPanelComponent.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });

      await test.step(`verify organiser sees chat moderator button inside chat contianer`, async () => {
        await organiserRoom.getInteractionPanelComponent.getChatComponent.verifyModeratedChatButtonIsVisibleOnChatContainer();
      });

      await test.step(`verify attendees does not sees chat moderator button inside chat contianer`, async () => {
        await attendeeOneRoom.getInteractionPanelComponent.getChatComponent.verifyModeratedChatButtonIsNotVisibleOnChatContainer();
      });
    });

    await test.step(`Organiser clicks on moderated chat button, then rejects the message, then verify its visible to all on chat container `, async () => {
      await test.step("organiser clicks on chat moderator button and verify moderated chat container is visible", async () => {
        await organiserRoom.getInteractionPanelComponent.getChatComponent.updateStatusOfChatWaitingForModeration(
          attendeeMessageOne,
          false
        );
      });
    });

    await test.step(`Now verify post rejection by organiser, chat message is not visible in the chat container to all user`, async () => {
      await test.step(`Verify organiser is not able to see attendee 1 message on chat panel`, async () => {
        await organiserRoom.getInteractionPanelComponent.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });
      await test.step(`Verify attendee 1 is not  able to see his message on chat panel`, async () => {
        await attendeeOneRoom.getInteractionPanelComponent.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });
      await test.step(`Verify attendee 2 is not  able to see attendee 1 message on chat panel`, async () => {
        await attendeeTwoRoom.getInteractionPanelComponent.getChatComponent.verifyMessageWithGivenContentIsNotVisible(
          attendeeMessageOne
        );
      });
    });

    // Now every one exits from the room
    await test.step(`verify attendee 1 is able to exit the room and land on room listing page`, async () => {
      await attendeeOneRoom.clickOnExitRoom();
      await attendeeOneRoom.handleExitConfirmationPopup(true);
    });

    // attendee two exit from the room
    await test.step(`verify attendee 2 is able to exit the room and land on room listing page`, async () => {
      await attendeeTwoRoom.clickOnExitRoom();
      await attendeeTwoRoom.handleExitConfirmationPopup(true);
    });

    // organiser exits from the room
    await test.step(`verify organiser is able to exit the room and land on room listing page`, async () => {
      await organiserRoom.clickOnExitRoom();
      await organiserRoom.handleExitConfirmationPopup(true);
    });
  });
});
