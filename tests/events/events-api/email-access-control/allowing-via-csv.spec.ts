import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import EmailRestrictionType from "../../../../enums/emailRestrictionTypeEnum";
import RestrictionEmailDomainType from "../../../../enums/restrictedEmailDomainType";
import BrowserFactory from "../../../../util/BrowserFactory";
import { checkIfFileExists } from "../../../../util/commonUtil";
import { CsvApiController } from "../../../../util/csv-api-controller";
import { DataUtl } from "../../../../util/dataUtil";

import {
  createNewEvent,
  updateEmailRestrictionSettings,
  getEventDetails,
  getLandingPageDetails,
  isEmailRestricted,
  registerUserToEventbyApi,
  inviteAttendeeByAPI,
  registerAttendeeByV1BetaAPI,
} from "../../../../util/apiUtil";

test.describe
  .parallel("Email Access Control | Allowing @newmail.com by csv @apitest @email-access-control", async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let csvApiController: CsvApiController;
  let eventId;
  let csvFileToAddDataIn;
  let eventTitle;
  test.beforeEach(async () => {
    /*
        Adding @newmail.com as the domain to the csv
        */
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = orgBrowserContext.request;
    csvApiController = new CsvApiController(orgApiContext);
    eventTitle = Date.now().toString() + "_automationevent";

    //get s3 url
    let fileName = await csvApiController.getSignedS3Url(
      "emailBlockingCsv.csv"
    );
    console.log(`fileName is ${fileName}`);
    // now upload this s3filename to server
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: eventTitle,
    });
    let csvUploadResJson = await csvApiController.processRestrciedEmailCsv(
      eventId,
      fileName
    );
    let successRecords = csvUploadResJson["succeedRecords"];
    expect(successRecords).toBe(1);
    //now getting the landing page id for this event
    const event_details_api_resp = await (
      await getEventDetails(orgApiContext, eventId)
    ).json();
    let landingPageId = event_details_api_resp.eventLandingPageId;
    //now updating the landing page settings
    await updateEmailRestrictionSettings(
      orgApiContext,
      eventId,
      landingPageId,
      fileName,
      EmailRestrictionType.ALLOW,
      [RestrictionEmailDomainType.CUSTOM]
    );
    //validating the changes we are doing and reflecting right in the landing page api
    let landingPageResJson = await (
      await getLandingPageDetails(orgApiContext, eventId)
    ).json();
    // csv file in case we need csv test case
    csvFileToAddDataIn = DataUtl.getRandomCsvName();
  });

  test.afterEach(async () => {
    const fs = require("fs");
    if (checkIfFileExists(csvFileToAddDataIn)) {
      console.log(`file exists`);
      fs.unlink(csvFileToAddDataIn, (err) => {
        if (err) {
          console.error(`error ${err} occurred when deleting the file`);
        }
      });
    } else {
      console.log("file does not exists");
    }
    await orgBrowserContext?.close();
  });

  const listOfScenariosForRestrictionAPI = [
    { emailToCheck: "prateek@newmail.com", expected: true }, //restricted email
    { emailToCheck: "prateek@NEWMAIL.com", expected: true }, //uppercase
    { emailToCheck: " prateek@newmail.com", expected: true }, //leading space
    { emailToCheck: "prateek@newmail.com ", expected: true }, //trailing space
    { emailToCheck: "prateek@notnewmail.com", expected: false }, //non restricted email
    { emailToCheck: null, expected: false }, //null check
  ];

  //testing IsRestrictedEmail APi for the given scenarios
  listOfScenariosForRestrictionAPI.forEach((testData) => {
    test(`Testing IsRestrictedEmailAPI for ${testData.emailToCheck}`, async () => {
      //now run isValidEmail api to check if our system allow or deny the email
      let isValid = await isEmailRestricted(
        orgApiContext,
        eventId,
        testData.emailToCheck
      );
      expect(isValid, `API should return ${testData.expected}`).toBe(
        testData.expected
      );
    });
  });

  const listOfScenariosForRegistrationAPI = [
    { userEmail: "prateek@newmail.com", statusCode: 200 }, //restricted email
    { userEmail: "prateek@NEWMAIL.com", statusCode: 200 }, //uppercase
    { userEmail: " prateek@newmail.com", statusCode: 200 }, //leading space
    { userEmail: "prateek@newmail.com ", statusCode: 200 }, //trailing space
    { userEmail: "prateek@notnewmail.com", statusCode: 400 }, //non restricted email
    { userEmail: null, statusCode: 400 }, //null check
  ];
  listOfScenariosForRegistrationAPI.forEach((testData) => {
    test(`Validating registration form API for ${testData.userEmail}`, async ({
      request,
    }) => {
      let registrationApiResp = await registerUserToEventbyApi(
        request,
        eventId,
        testData.userEmail,
        false
      );
      let registrationApiStatusCode = registrationApiResp.status();
      expect(
        registrationApiStatusCode,
        `Expecting reg api to give status code ${testData.statusCode}`
      ).toBe(testData.statusCode);
      if (testData.statusCode == 400) {
        let registrationApiResJson = await registrationApiResp.json();
        let error_message = registrationApiResJson.message;
        let description = registrationApiResJson.description;

        if (testData.userEmail == null) {
          expect(
            error_message,
            "expecting error message to be email restricted"
          ).toBe("EMAIL_FIRST_LAST_NAME");

          expect(
            description,
            "expecting description to be email is required"
          ).toBe("Email,First,Last Name should not be empty.");
        } else {
          expect(
            error_message,
            "expecting error message to be email restricted"
          ).toBe("EMAIL_RESTRICTED");
          expect(
            description,
            "This email domain is restricted. Try a different email."
          ).toBe("This email domain is restricted. Try a different email.");
        }
      }
    });

    test(`Validating Add Attendee Api for ${testData.userEmail}`, async () => {
      let addAttendeeAPIResp = await inviteAttendeeByAPI(
        orgApiContext,
        eventId,
        testData.userEmail
      );
      let addAttendeeAPIRespStatusCode = addAttendeeAPIResp.status();
      if (testData.userEmail == null) {
        let addAttendeeAPIRespJson = await addAttendeeAPIResp.json();
        let error_message = addAttendeeAPIRespJson.message;
        let description = addAttendeeAPIRespJson.description;

        expect(
          addAttendeeAPIRespStatusCode,
          `Expecting add attendee api to give status code ${testData.statusCode}`
        ).toBe(400); // updated, earlier it was 500

        expect(
          error_message,
          "expecting error message to be email first last name"
        ).toBe("EMAIL_FIRST_LAST_NAME");
        expect(description, "Email is empty/null.").toBe(
          "Email,First,Last Name should not be empty."
        );
      } else {
        expect(
          addAttendeeAPIRespStatusCode,
          `Expecting add attendee api to give status code ${testData.statusCode}`
        ).toBe(testData.statusCode);
        if (testData.statusCode == 400) {
          let addAttendeeAPIRespJson = await addAttendeeAPIResp.json();
          let error_message = addAttendeeAPIRespJson.message;
          let description = addAttendeeAPIRespJson.description;
          expect(
            error_message,
            "expecting error message to be email restricted"
          ).toBe("EMAIL_RESTRICTED");
          expect(
            description,
            "This email domain is restricted. Try a different email."
          ).toBe("This email domain is restricted. Try a different email.");
        }
      }
    });
  });

  const listOfScenariosForV1BetaAPI = [
    { userEmail: "prateek@newmail.com", statusCode: 200 }, //restricted email
    { userEmail: "prateek@NEWMAIL.com", statusCode: 200 }, //uppercase
    { userEmail: " prateek@newmail.com", statusCode: 200 }, //leading space
    { userEmail: "prateek@newmail.com ", statusCode: 200 }, //trailing space
    { userEmail: "prateek@notnewmail.com", statusCode: 400 }, //non restricted email
    { userEmail: null, statusCode: 400 }, //null check
  ];

  listOfScenariosForV1BetaAPI.forEach((testData) => {
    test(`Validating v1 beta API for ${testData.userEmail}`, async () => {
      const v1BetaRegApiResp = await registerAttendeeByV1BetaAPI(
        orgApiContext,
        eventId,
        testData.userEmail,
        "prateek",
        "qa"
      );
      let v1BetaRegApiRespStatusCode = v1BetaRegApiResp.status();
      expect(
        v1BetaRegApiRespStatusCode,
        `Expecting reg api to give status code ${testData.statusCode}`
      ).toBe(testData.statusCode);
      if (testData.statusCode == 400) {
        let registrationApiResJson = await v1BetaRegApiResp.json();
        let error_message = registrationApiResJson.message;
        let description = registrationApiResJson.description;
        expect(
          error_message,
          "expecting error message to be email restricted"
        ).toBe("EMAIL_RESTRICTED");
        expect(
          description,
          "This email domain is restricted. Try a different email."
        ).toBe("This email domain is restricted. Try a different email.");
      }
    });
  });

  test(`Validating csv import for multiple record with valid and invalid emails`, async () => {
    let listOfEmailsToAdd = [
      "prateek@newmail.com",
      "prateek@notnewmail.com",
      "prateek1@newmail.com",
      "prateek2@gmail.com",
    ];
    await csvApiController.addAttendeeRecordInCsv(
      csvFileToAddDataIn,
      listOfEmailsToAdd
    );
    let uploadedFileNameOnS3 = await csvApiController.getSignedS3Url(
      csvFileToAddDataIn
    );
    await csvApiController.processAttendeeCsvOnEndPoint(
      eventId,
      uploadedFileNameOnS3
    );
    let attendeeCsvProcessingResp =
      await csvApiController.waitForCsvFileProcessingToComplete(
        eventId.toString(),
        uploadedFileNameOnS3
      );
    let attendeeCsvProcessingRespJson = await attendeeCsvProcessingResp.json();
    expect(
      attendeeCsvProcessingRespJson.failedRecordsCount,
      "expecting failed record count to be 2"
    ).toBe(2);
    expect(
      attendeeCsvProcessingRespJson.succeedRecordsCount,
      "expecting succeed record count to be 2"
    ).toBe(2);
    let logurl = attendeeCsvProcessingRespJson.logUrl;
    let csvReportText = await (await orgApiContext.get(logurl)).text();
    expect(
      csvReportText,
      "expecting csv processing report to show business email error on line 2"
    ).toContain(
      "[Line:2] This email domain is restricted. Try a different email."
    );
    expect(
      csvReportText,
      "expecting csv processing report to show business email error on line 4"
    ).toContain(
      "[Line:4] This email domain is restricted. Try a different email."
    );
  });
});
