import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import BrowserFactory from "../../../../util/BrowserFactory";
import { CsvApiController } from "../../../../util/csv-api-controller";
import {
  createNewEvent,
  updateEmailRestrictionSettings,
  getEventDetails,
  getLandingPageDetails,
  isEmailRestricted,
  registerUserToEventbyApi,
  inviteAttendeeByAPI,
  registerAttendeeByV1BetaAPI,
} from "../../../../util/apiUtil";
import EmailRestrictionType from "../../../../enums/emailRestrictionTypeEnum";
import RestrictionEmailDomainType from "../../../../enums/restrictedEmailDomainType";
import { DataUtl } from "../../../../util/dataUtil";
import { checkIfFileExists } from "../../../../util/commonUtil";

test.describe
  .parallel("Email Access Control | Restricting @gmail via pre-defined list and @newmail via csv @apitest @email-access-control", async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let csvApiController: CsvApiController;
  let eventId;
  let csvFileToAddDataIn;
  let eventTitle: string;
  test.beforeEach(async () => {
    /*
        Adding @gmail.com from predefined list
        Adding @newmail.com from csv
        */
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    eventTitle = Date.now().toString() + "_automationevent";

    orgApiContext = orgBrowserContext.request;
    csvApiController = new CsvApiController(orgApiContext);
    //get s3 url
    let fileName = await csvApiController.getSignedS3Url(
      "emailBlockingCsv.csv"
    );
    console.log(`fileName is ${fileName}`);
    // now upload this s3filename to server
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: eventTitle,
    });
    let csvUploadResJson = await csvApiController.processRestrciedEmailCsv(
      eventId,
      fileName
    );
    let successRecords = csvUploadResJson["succeedRecords"];
    expect(successRecords).toBe(1);
    //now getting the landing page id for this event
    const event_details_api_resp = await (
      await getEventDetails(orgApiContext, eventId)
    ).json();
    let landingPageId = event_details_api_resp.eventLandingPageId;
    //now updating the landing page settings
    await updateEmailRestrictionSettings(
      orgApiContext,
      eventId,
      landingPageId,
      fileName,
      EmailRestrictionType.DENY,
      [RestrictionEmailDomainType.PREDEFINED, RestrictionEmailDomainType.CUSTOM]
    );
    //validating the changes we are doing and reflecting right in the landing page api
    let landingPageResJson = await (
      await getLandingPageDetails(orgApiContext, eventId)
    ).json();
    //csv file name
    csvFileToAddDataIn = DataUtl.getRandomCsvName();
  });

  test.afterEach(async () => {
    const fs = require("fs");
    if (checkIfFileExists(csvFileToAddDataIn)) {
      console.log(`file exists`);
      fs.unlink(csvFileToAddDataIn, (err) => {
        if (err) {
          console.error(`error ${err} occurred when deleting the file`);
        }
      });
    } else {
      console.log("file does not exists");
    }
    await orgBrowserContext?.close();
  });

  const listOfScenariosForRestrictionAPI = [
    { emailToCheck: "prateek@gmail.com", expected: false }, //restricted email via predefined
    { emailToCheck: "prateek@newmail.com", expected: false }, //restricted email via csv
    { emailToCheck: "prateek@notgmail.com", expected: true }, //non restricted email
    { emailToCheck: null, expected: false }, //null check
  ];

  //testing IsRestrictedEmail APi for the given scenarios
  listOfScenariosForRestrictionAPI.forEach((testData) => {
    test(`Testing IsRestrictedEmailAPI for ${testData.emailToCheck}`, async () => {
      //now run isValidEmail api to check if our system allow or deny the email
      let isValid = await isEmailRestricted(
        orgApiContext,
        eventId,
        testData.emailToCheck
      );
      expect(isValid, `API should return ${testData.expected}`).toBe(
        testData.expected
      );
    });
  });

  const listOfScenariosForRegistrationAPI = [
    { userEmail: "prateek@gmail.com", statusCode: 400 }, //restricted email via predefined list
    { userEmail: "PRATEEK@GMAIL.COM", statusCode: 400 }, //uppercase email check
    { userEmail: "prateek@newmail.com", statusCode: 400 }, //restricted email via csv
    { userEmail: " prateek@newmail.com", statusCode: 400 }, //leading space
    { userEmail: "prateek@newmail.com ", statusCode: 400 }, //trailing space
    { userEmail: "prateek@notgmail.com", statusCode: 200 }, //non restricted email
    { userEmail: null, statusCode: 400 }, //null check
  ];
  listOfScenariosForRegistrationAPI.forEach((testData) => {
    test(`Validating registration form API for ${testData.userEmail}`, async ({
      request,
    }) => {
      let registrationApiResp = await registerUserToEventbyApi(
        request,
        eventId,
        testData.userEmail,
        false
      );
      let registrationApiStatusCode = registrationApiResp.status();
      console.log(
        `userEmail is ${testData.userEmail} and statusCode is ${registrationApiStatusCode}`
      );
      expect(
        registrationApiStatusCode,
        `Expecting reg api to give status code ${testData.statusCode}`
      ).toBe(testData.statusCode);
      if (testData.statusCode == 400) {
        let registrationApiResJson = await registrationApiResp.json();
        let error_message = registrationApiResJson.message;
        let description = registrationApiResJson.description;

        if (testData.userEmail == null) {
          expect(
            error_message,
            "expecting error message to be email restricted"
          ).toBe("EMAIL_FIRST_LAST_NAME");

          expect(
            description,
            "expecting description to be email is required"
          ).toBe("Email,First,Last Name should not be empty.");
        } else {
          expect(
            error_message,
            "expecting error message to be email restricted"
          ).toBe("EMAIL_RESTRICTED");
          expect(
            description,
            "This email domain is restricted. Try a different email."
          ).toBe("This email domain is restricted. Try a different email.");
        }
      }
    });

    test(`Validating Add Attendee Api for ${testData.userEmail}`, async () => {
      let addAttendeeAPIResp = await inviteAttendeeByAPI(
        orgApiContext,
        eventId,
        testData.userEmail,
        false
      );
      let addAttendeeAPIRespStatusCode = addAttendeeAPIResp.status();
      if (testData.userEmail == null) {
        expect(
          addAttendeeAPIRespStatusCode,
          `Expecting reg api to give status code ${400}`
        ).toBe(400);

        let registrationApiResJson = await addAttendeeAPIResp.json();
        let error_message = registrationApiResJson.message;
        let description = registrationApiResJson.description;
        expect(
          error_message,
          "expecting error message to be email restricted"
        ).toBe("EMAIL_FIRST_LAST_NAME");

        expect(
          description,
          "expecting description to be email is required"
        ).toBe("Email,First,Last Name should not be empty.");
      } else {
        expect(
          addAttendeeAPIRespStatusCode,
          `Expecting add attendee api to give status code ${testData.statusCode}`
        ).toBe(testData.statusCode);
        if (testData.statusCode == 400) {
          let addAttendeeAPIRespJson = await addAttendeeAPIResp.json();
          let error_message = addAttendeeAPIRespJson.message;
          let description = addAttendeeAPIRespJson.description;
          expect(
            error_message,
            "expecting error message to be email restricted"
          ).toBe("EMAIL_RESTRICTED");
          expect(description, "description to be use business emails").toBe(
            "This email domain is restricted. Try a different email."
          );
        }
      }
    });
  });

  const listOfScenariosForV1BetaAPI = [
    { userEmail: "prateek@gmail.com", statusCode: 400 }, //restricted email via predefined list
    { userEmail: "PRATEEK@GMAIL.COM", statusCode: 400 }, //uppercase email check
    { userEmail: "prateek@newmail.com", statusCode: 400 }, //restricted email via csv
    { userEmail: " prateek@newmail.com", statusCode: 400 }, //leading space
    { userEmail: "prateek@newmail.com ", statusCode: 400 }, //trailing space
    { userEmail: "prateek@notgmail.com", statusCode: 200 }, //non restricted email
    { userEmail: null, statusCode: 400 }, //null check
  ];

  listOfScenariosForV1BetaAPI.forEach((testData) => {
    test(`Validating v1 beta API for ${testData.userEmail}`, async () => {
      const v1BetaRegApiResp = await registerAttendeeByV1BetaAPI(
        orgApiContext,
        eventId,
        testData.userEmail,
        "prateek",
        "qa"
      );
      let v1BetaRegApiRespStatusCode = v1BetaRegApiResp.status();
      expect(
        v1BetaRegApiRespStatusCode,
        `Expecting reg api to give status code ${testData.statusCode}`
      ).toBe(testData.statusCode);
      if (testData.statusCode == 400) {
        let registrationApiResJson = await v1BetaRegApiResp.json();
        let error_message = registrationApiResJson.message;
        let description = registrationApiResJson.description;
        expect(
          error_message,
          "expecting error message to be email restricted"
        ).toBe("EMAIL_RESTRICTED");
        expect(description, "description to be use business emails").toBe(
          "This email domain is restricted. Try a different email."
        );
      }
    });
  });

  test(`Validating csv import for multiple record with valid and invalid emails`, async () => {
    let listOfEmailsToAdd = [
      "prateek@gmail.com",
      "prateek@GMAIL.com",
      " prateek@gmail.com",
      "prateek@newmail.com",
      "prateek2@notgmail.com",
    ];
    await csvApiController.addAttendeeRecordInCsv(
      csvFileToAddDataIn,
      listOfEmailsToAdd
    );
    let uploadedFileNameOnS3 = await csvApiController.getSignedS3Url(
      csvFileToAddDataIn
    );
    await csvApiController.processAttendeeCsvOnEndPoint(
      eventId,
      uploadedFileNameOnS3
    );
    let attendeeCsvProcessingResp =
      await csvApiController.waitForCsvFileProcessingToComplete(
        eventId.toString(),
        uploadedFileNameOnS3
      );
    let attendeeCsvProcessingRespJson = await attendeeCsvProcessingResp.json();
    expect(
      attendeeCsvProcessingRespJson.failedRecordsCount,
      "expecting failed record count to be 4"
    ).toBe(4);
    expect(
      attendeeCsvProcessingRespJson.succeedRecordsCount,
      "expecting succeed record count to be 1"
    ).toBe(1);
    let logurl = attendeeCsvProcessingRespJson.logUrl;
    let csvReportText = await (await orgApiContext.get(logurl)).text();
    expect(
      csvReportText,
      "expecting csv processing report to show business email error on line 1"
    ).toContain(
      "[Line:1] This email domain is restricted. Try a different email."
    );
    expect(
      csvReportText,
      "expecting csv processing report to show business email error on line 2"
    ).toContain(
      "[Line:2] This email domain is restricted. Try a different email."
    );
    expect(
      csvReportText,
      "expecting csv processing report to show business email error on line 3"
    ).toContain(
      "[Line:3] This email domain is restricted. Try a different email."
    );
    expect(
      csvReportText,
      "expecting csv processing report to show business email error on line 4"
    ).toContain(
      "[Line:4] This email domain is restricted. Try a different email."
    );
  });
});
