import { APIRequestContext, expect } from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { RoomsController } from "../../../controller/RoomsController";
import { StageController } from "../../../controller/StageController";
import { ExpoController } from "../../../controller/ExpoController";
import VenueSection, { Room, Stage, Booth } from "./venueSection";
import { AccessGroupController } from "../../../controller/AccessGroupController";
import {
  getLandingPageDetails,
  getListOfSpeakersInEvent,
} from "../../../util/apiUtil";
import { inspect } from "util";
import { RecordingController } from "../../../controller/RecordingController";
import exp from "constants";
import { VenueController } from "../../../controller/VenueController";

export default class DuplicateValidation {
  protected apiContext: APIRequestContext;
  protected originalEventId;
  protected duplicateEventId;
  protected originalEventTitle;
  protected duplicateEventTitle;
  protected eventControllers: EventController[];
  protected roomControllers: RoomsController[];
  protected stageControllers: StageController[];
  protected expoControllers: ExpoController[];
  protected accessGroupControllers: AccessGroupController[];
  protected venueSections: VenueSection[];
  protected deltaHours: number;
  protected deltaDays: number;

  constructor(
    apiContext: APIRequestContext,
    originalEventId: string,
    duplicateEventId: string,
    originalEventTitle: string,
    duplicateEventTitle: string,
    deltaHours: number,
    deltaDays: number
  ) {
    this.apiContext = apiContext;
    this.originalEventId = originalEventId;
    this.duplicateEventId = duplicateEventId;
    this.originalEventTitle = originalEventTitle;
    this.duplicateEventTitle = duplicateEventTitle;
    this.eventControllers = [];
    this.roomControllers = [];
    this.expoControllers = [];
    this.accessGroupControllers = [];
    this.venueSections = [];
    this.deltaHours = deltaHours;
    this.deltaDays = deltaDays;
  }

  async initializeControllers() {
    this.eventControllers.push(
      new EventController(this.apiContext, this.originalEventId)
    );
    this.eventControllers.push(
      new EventController(this.apiContext, this.duplicateEventId)
    );

    this.roomControllers.push(
      new RoomsController(this.apiContext, this.originalEventId)
    );
    this.roomControllers.push(
      new RoomsController(this.apiContext, this.duplicateEventId)
    );

    this.expoControllers.push(
      new ExpoController(this.apiContext, this.originalEventId)
    );
    this.expoControllers.push(
      new ExpoController(this.apiContext, this.duplicateEventId)
    );

    this.venueSections.push(
      new VenueSection(this.apiContext, this.originalEventId)
    );
    this.venueSections.push(
      new VenueSection(this.apiContext, this.duplicateEventId)
    );

    this.accessGroupControllers.push(
      new AccessGroupController(this.apiContext, this.originalEventId)
    );
    this.accessGroupControllers.push(
      new AccessGroupController(this.apiContext, this.duplicateEventId)
    );
  }

  async validateEventDetails() {
    const originalEventDetails = await (
      await this.eventControllers[0].getEventInfo()
    ).json();
    const duplicateEventDetails = await (
      await this.eventControllers[1].getEventInfo()
    ).json();

    expect(originalEventDetails["eventId"]).toEqual(this.originalEventId);
    expect(originalEventDetails["title"]).toEqual(this.originalEventTitle);
    expect(duplicateEventDetails["eventId"]).toEqual(this.duplicateEventId);
    expect(duplicateEventDetails["title"]).toEqual(this.duplicateEventTitle);

    const toBeValidated = [
      "description",
      "tz",
      "visibility",
      "status",
      "coverImagePath",
      "registrationTemplate",
      "logoUrl",
      "colorTheme",
      "primaryColor",
      "mailLogo",
      "mailLogoHeight",
      "mailBtnColor",
      "secondaryColor",
      "navbarColor",
      "navbarPrimaryColor",
      "navbarSecondaryColor",
      "backgroundImageUrl",
      "mailSenderName",
      "isChecklistDone",
      "enableLeaderboard",
      "showLeaderboardPointsDistribution",
      "mailWebsite",
      "enablePrivateMeetings",
      "enableScheduleSync",
      "enableHelpdesk",
      "hasUserCfgTemplate",
      "hasCreatedFromOldOrgside",
      "type",
      "eventFormat",
      "teamId",
      "isFlex",
    ];

    for (var key of toBeValidated) {
      expect(originalEventDetails[key]).toEqual(duplicateEventDetails[key]);
    }

    const timingsToBeValidated = ["startDateTime", "endDateTime"];

    for (var key of timingsToBeValidated) {
      let originalDate = new Date(originalEventDetails[key]);
      let duplicateDate = new Date(duplicateEventDetails[key]);

      let deltaHours = duplicateDate.getHours() - originalDate.getHours();

      // let deltaDays = duplicateDate.getDate() - originalDate.getDate(); // fails during end of month when .getDate returns 30/31 and 1

      originalDate.setHours(0, 0, 0, 0);
      duplicateDate.setHours(0, 0, 0, 0);
      let deltaDays = Math.floor(
        (duplicateDate.getTime() - originalDate.getTime()) / (1000 * 3600 * 24)
      );

      // console.log("Recieved Delta Hours: ", deltaHours);
      // console.log("Recieved Delta Days: ", deltaDays);
      // console.log("Expected Delta Hours: ", this.deltaHours);
      // console.log("Expected Delta Days: ", this.deltaDays);
      // let diffHours = Math.abs(deltaHours - this.deltaHours);
      // let diffDays = Math.abs(deltaDays - this.deltaDays);
      // console.log("Diff Hours: ", diffHours);
      // console.log("Diff Days: ", diffDays);
      // expect(diffHours).toBeLessThanOrEqual(1);
      // expect(diffDays).toBeLessThanOrEqual(1);
    }
  }

  async duplicatedEventRoomValidation(venueSetup: boolean) {
    const duplicateRooms = await this.venueSections[1].getAllRooms();

    //If Venue Setup is turned off, then no room gets copied
    if (!venueSetup) expect(duplicateRooms.length).toBe(0);
    else {
      const originalRooms = await this.venueSections[0].getAllRooms();
      await this.validateRooms(originalRooms, duplicateRooms);
    }
  }

  private async validateRooms(originalRooms, duplicateRooms) {
    //Verify if the rooms json array lengths are same
    // console.log("Original Rooms", inspect(originalRooms));
    // console.log("Duplicate Rooms", inspect(duplicateRooms));
    expect(originalRooms.length).toEqual(duplicateRooms.length);

    console.log("............Rooms Validations............");
    for (var i = 0; i < originalRooms.length; i++) {
      const duplicateRoom = duplicateRooms.find(
        (room) => room.roomName == originalRooms[i].roomName
      );
      console.log("Original Room ...", originalRooms[i]);
      console.log("Duplicate Room ...", duplicateRoom);

      let originalRoomDetails = await this.roomControllers[0].getRoomDetails(
        originalRooms[i].roomId
      );
      console.log("Original Room Details: ", originalRoomDetails);

      let duplicateRoomDetails = await this.roomControllers[1].getRoomDetails(
        duplicateRoom.roomId
      );
      console.log("Duplicate Room Details: ", duplicateRoomDetails);

      // name, capacity, description, primaryColour, logo
      expect(originalRoomDetails.name).toEqual(duplicateRoomDetails.name);
      expect(originalRoomDetails.capacity).toEqual(
        duplicateRoomDetails.capacity
      );
      expect(originalRoomDetails.description).toEqual(
        duplicateRoomDetails.description
      );
      expect(originalRoomDetails.primaryColour).toEqual(
        duplicateRoomDetails.primaryColour
      );
      expect(originalRoomDetails.logo).toEqual(duplicateRoomDetails.logo);
    }
  }

  async validateRoomDetails(randomRoom: Room) {
    const originalRooms = await this.venueSections[0].getAllRooms();
    const duplicateRooms = await this.venueSections[1].getAllRooms();

    const originalRoomsWithoutId = originalRooms.map((room) => {
      return {
        roomName: room.roomName,
        capacity: room.capacity,
      };
    });

    const duplicateRoomsWithoutId = duplicateRooms.map((room) => {
      return {
        roomName: room.roomName,
        capacity: room.capacity,
      };
    });

    console.log(originalRoomsWithoutId);
    console.log(duplicateRoomsWithoutId);

    expect(originalRooms.length).toEqual(duplicateRooms.length);

    for (var i = 0; i < originalRooms.length; i++) {
      expect(originalRoomsWithoutId).toContainEqual(duplicateRoomsWithoutId[i]);
    }

    console.log("Random Room: ", randomRoom);
    // use the name to find the random room in the duplicate event
    const randomRoomDuplicate = duplicateRooms.find(
      (room) => room.roomName == randomRoom.roomName
    );
    console.log("Random Room Duplicate: ", randomRoomDuplicate);
    let originalRandomRoomDetails =
      await this.roomControllers[0].getRoomDetails(randomRoom.roomId);
    console.log("Original Random Room Details: ", originalRandomRoomDetails);
    let duplicateRandomRoomDetails =
      await this.roomControllers[1].getRoomDetails(randomRoomDuplicate.roomId);
    console.log("Duplicate Random Room Details: ", duplicateRandomRoomDetails);
    // name, capacity, description, primaryColour, logo
    expect(originalRandomRoomDetails.name).toEqual(
      duplicateRandomRoomDetails.name
    );
    expect(originalRandomRoomDetails.capacity).toEqual(
      duplicateRandomRoomDetails.capacity
    );
    expect(originalRandomRoomDetails.description).toEqual(
      duplicateRandomRoomDetails.description
    );
    expect(originalRandomRoomDetails.primaryColour).toEqual(
      duplicateRandomRoomDetails.primaryColour
    );
    expect(originalRandomRoomDetails.logo).toEqual(
      duplicateRandomRoomDetails.logo
    );

    const originalEventTags = await this.eventControllers[0].getAllTags();
    const duplicateEventTags = await this.eventControllers[1].getAllTags();

    const tagMap = new Map();

    for (var i = 0; i < originalEventTags.length; i++) {
      for (var j = 0; j < duplicateEventTags.length; j++) {
        if (originalEventTags[i].name == duplicateEventTags[j].name) {
          tagMap.set(originalEventTags[i].tagId, duplicateEventTags[j].tagId);
          break;
        }
      }
    }

    console.log(tagMap);

    let originalRoomTags = originalRandomRoomDetails.eventTagIds;
    let duplicateRoomTags = duplicateRandomRoomDetails.eventTagIds;

    for (var i = 0; i < originalRoomTags.length; i++) {
      originalRoomTags[i] = tagMap.get(originalRoomTags[i]);
    }

    for (var i = 0; i < duplicateRoomTags.length; i++) {
      expect(originalRoomTags).toContainEqual(duplicateRoomTags[i]);
    }
  }

  async duplicatedEventExpoValidation(venueSetup: boolean) {
    const duplicateExpo = await this.expoControllers[1].getAllZones();

    //If Venue Setup is turned off, then no expo gets copied
    if (!venueSetup) expect(duplicateExpo.length).toBe(0);
    else {
      const originalExpo = await this.expoControllers[0].getAllZones();
      await this.validateExpos(originalExpo, duplicateExpo);
    }
  }

  private async validateExpos(originalEventZones, duplicateEventZones) {
    //Verify if the rooms json array lengths are same
    expect(originalEventZones.length).toEqual(duplicateEventZones.length);

    const originalEventZonesWithoutId = originalEventZones.map((zone) => {
      return {
        zoneName: zone.title,
      };
    });
    const duplicateEventZonesWithoutId = duplicateEventZones.map((zone) => {
      return {
        zoneName: zone.title,
      };
    });

    expect(originalEventZones.length).toEqual(duplicateEventZones.length);
    for (var i = 0; i < originalEventZones.length; i++) {
      expect(originalEventZonesWithoutId).toContainEqual(
        duplicateEventZonesWithoutId[i]
      );
    }

    let zoneMap = new Map();
    for (var i = 0; i < originalEventZones.length; i++) {
      for (var j = 0; j < duplicateEventZones.length; j++) {
        if (originalEventZones[i].title == duplicateEventZones[j].title) {
          zoneMap.set(
            originalEventZones[i].boothZoneId,
            duplicateEventZones[j].boothZoneId
          );
          break;
        }
      }
    }
    console.log(zoneMap);

    const originalEventBooths = await this.expoControllers[0].getAllBooths();
    const duplicateEventBooths = await this.expoControllers[1].getAllBooths();

    expect(originalEventBooths.length).toEqual(duplicateEventBooths.length);
    console.log(
      "Original booths ...",
      inspect(originalEventBooths, { depth: null })
    );
    console.log(
      "Duplicate booths ...",
      inspect(duplicateEventBooths, { depth: null })
    );

    console.log(".........Validating widgets.........");
    for (var i = 0; i < originalEventBooths.length; i++) {
      const originalBooth = originalEventBooths[i];
      console.log("Original Booth: ", originalBooth);

      const duplicateBooth = duplicateEventBooths.find(
        (booth) => booth.name == originalBooth.name
      );
      console.log("Duplicate Booth: ", duplicateBooth);

      const widgetsOriginal = await this.expoControllers[0].getAllWidgets(
        originalBooth.boothId
      );
      const widgetsDuplicate = await this.expoControllers[1].getAllWidgets(
        duplicateBooth.boothId
      );
      console.log("Original Widgets: ", widgetsOriginal);
      console.log("Duplicate Widgets: ", widgetsDuplicate);

      for (var j = 0; j < widgetsOriginal.length; j++) {
        expect(widgetsOriginal[j].config).toEqual(widgetsDuplicate[j].config);
        expect(widgetsOriginal[j].widgetType).toEqual(
          widgetsDuplicate[j].widgetType
        );
      }
    }
  }

  async validateExpoDetails(randomBooth: Booth) {
    const originalEventZones = await this.expoControllers[0].getAllZones();
    const duplicateEventZones = await this.expoControllers[1].getAllZones();
    const originalEventZonesWithoutId = originalEventZones.map((zone) => {
      return {
        zoneName: zone.title,
      };
    });

    const duplicateEventZonesWithoutId = duplicateEventZones.map((zone) => {
      return {
        zoneName: zone.title,
      };
    });

    expect(originalEventZones.length).toEqual(duplicateEventZones.length);
    for (var i = 0; i < originalEventZones.length; i++) {
      expect(originalEventZonesWithoutId).toContainEqual(
        duplicateEventZonesWithoutId[i]
      );
    }

    let zoneMap = new Map();
    for (var i = 0; i < originalEventZones.length; i++) {
      for (var j = 0; j < duplicateEventZones.length; j++) {
        if (originalEventZones[i].title == duplicateEventZones[j].title) {
          zoneMap.set(
            originalEventZones[i].boothZoneId,
            duplicateEventZones[j].boothZoneId
          );
          break;
        }
      }
    }

    console.log(zoneMap);

    const originalEventBooths = await this.expoControllers[0].getAllBooths();
    const duplicateEventBooths = await this.expoControllers[1].getAllBooths();

    expect(originalEventBooths.length).toEqual(duplicateEventBooths.length);

    const minimizedBoothInfoOriginal = originalEventBooths.map((booth) => {
      return {
        boothName: booth.name,
        zoneId: booth.zoneId,
        description: booth.description,
        boothZoneId: zoneMap.get(booth.zoneId),
        boothCategory: booth.boothCategory,
        boothType: booth.boothType,
      };
    });

    const minimizedBoothInfoDuplicate = duplicateEventBooths.map((booth) => {
      return {
        boothName: booth.name,
        zoneId: booth.zoneId,
        description: booth.description,
        boothZoneId: booth.zoneId,
        boothCategory: booth.boothCategory,
        boothType: booth.boothType,
      };
    });

    for (var i = 0; i < originalEventBooths.length; i++) {
      expect(minimizedBoothInfoOriginal).toContainEqual(
        minimizedBoothInfoDuplicate[i]
      );
    }

    console.log("Random Booth: ", randomBooth);
    const randomBoothDuplicate = duplicateEventBooths.find(
      (booth) => booth.name == randomBooth.boothName
    );
    console.log("Random Booth Duplicate: ", randomBoothDuplicate);

    const widgetsOriginal = await this.expoControllers[0].getAllWidgets(
      randomBooth.boothId
    );
    const widgetsDuplicate = await this.expoControllers[1].getAllWidgets(
      randomBoothDuplicate.boothId
    );
    console.log("Original Widgets: ", widgetsOriginal);
    console.log("Duplicate Widgets: ", widgetsDuplicate);
    for (var i = 0; i < widgetsOriginal.length; i++) {
      expect(widgetsOriginal[i].config).toEqual(widgetsDuplicate[i].config);
      expect(widgetsOriginal[i].widgetType).toEqual(
        widgetsDuplicate[i].widgetType
      );
    }
  }

  async verifyDuplicateEventDefaultStageName(venueSettings: boolean) {
    const duplicateStages = await this.venueSections[1].getAllStages();
    let stageNames: Array<string> = new Array<string>();

    for (const stages of duplicateStages) {
      stageNames.push(stages.stageName);
    }

    console.log("Duplicate Stages -> ", stageNames);
    console.log("Expected Default Stage name -> ", this.duplicateEventTitle);
    if (venueSettings) {
      expect(stageNames).toContainEqual(this.originalEventTitle);
    } else {
      expect(stageNames).toContainEqual(this.duplicateEventTitle);
    }
  }

  async duplicatedEventStageValidation(venueSetup: boolean) {
    const duplicateStages = await this.venueSections[1].getAllStages();

    //If Venue Setup is turned off, then no stage gets copied
    if (!venueSetup) expect(duplicateStages.length).toBe(1);
    else {
      const originalStages = await this.venueSections[0].getAllStages();
      await this.validateStages(originalStages, duplicateStages);
    }
  }

  private async validateStages(originalStages, duplicateStages) {
    expect(originalStages.length).toEqual(duplicateStages.length);

    console.log("Original Stages: ", originalStages);
    console.log("Duplicate Stages: ", duplicateStages);

    for (var i = 0; i < originalStages.length; i++) {
      const originalStage = originalStages[i];

      console.log("Original Stage: ", originalStage);

      const duplicateStage = duplicateStages.find(
        (stage) => stage.stageName == originalStage.stageName
      );
      console.log("Duplicate Stage: ", duplicateStage);

      let stageControllerOriginal = new StageController(
        this.apiContext,
        this.originalEventId,
        originalStage.stageId
      );

      let stageControllerDuplicate = new StageController(
        this.apiContext,
        this.duplicateEventId,
        duplicateStage.stageId
      );

      let originalStageLayout = await stageControllerOriginal.getStageLayout();
      let duplicateStageLayout =
        await stageControllerDuplicate.getStageLayout();

      const toVerify = ["bgUrl", "bgType"];
      for (var key of toVerify) {
        expect(originalStageLayout[key]).toEqual(duplicateStageLayout[key]);
      }

      const engagementSettingsOriginal =
        await stageControllerOriginal.getEngagementSettings();
      const engagementSettingsDuplicate =
        await stageControllerDuplicate.getEngagementSettings();

      console.log("Original Engagement Settings: ", engagementSettingsOriginal);
      console.log(
        "Duplicate Engagement Settings: ",
        engagementSettingsDuplicate
      );

      expect(engagementSettingsOriginal).toEqual(engagementSettingsDuplicate);
    }
  }

  async validateStageDetails(randomStage: Stage) {
    const originalStages = await this.venueSections[0].getAllStages();
    const duplicateStages = await this.venueSections[1].getAllStages();

    const stagesWithoutId = originalStages.map((stage) => {
      return {
        stageName: stage.stageName,
        stageSize: stage.stageSize,
      };
    });

    const duplicateStagesWithoutId = duplicateStages.map((stage) => {
      return {
        stageName: stage.stageName,
        stageSize: stage.stageSize,
      };
    });

    expect(originalStages.length).toEqual(duplicateStages.length);

    for (var i = 0; i < originalStages.length; i++) {
      expect(stagesWithoutId).toContainEqual(duplicateStagesWithoutId[i]);
    }

    console.log("Original Stages: ", originalStages);
    console.log("Duplicate Stages: ", duplicateStages);
    console.log("Random Stage: ", randomStage);

    const randomStageDuplicate = duplicateStages.find(
      (stage) => stage.stageName == randomStage.stageName
    );
    console.log("Random Stage Duplicate: ", randomStageDuplicate);
    let stageControllerOriginal = new StageController(
      this.apiContext,
      this.originalEventId,
      randomStage.stageId
    );
    let stageControllerDuplicate = new StageController(
      this.apiContext,
      this.duplicateEventId,
      randomStageDuplicate.stageId
    );

    let originalStageLayout = await stageControllerOriginal.getStageLayout();
    let duplicateStageLayout = await stageControllerDuplicate.getStageLayout();

    const toVerify = ["bgUrl", "bgType"];
    for (var key of toVerify) {
      expect(originalStageLayout[key]).toEqual(duplicateStageLayout[key]);
    }

    const engagementSettingsOriginal =
      await stageControllerOriginal.getEngagementSettings();
    const engagementSettingsDuplicate =
      await stageControllerDuplicate.getEngagementSettings();

    console.log("Original Engagement Settings: ", engagementSettingsOriginal);
    console.log("Duplicate Engagement Settings: ", engagementSettingsDuplicate);

    expect(engagementSettingsOriginal).toEqual(engagementSettingsDuplicate);
  }

  async validateSegments() {
    let originalEventSegments = await this.eventControllers[0].getAllSessions(
      this.originalEventId
    );
    let duplicateEventSegments = await this.eventControllers[1].getAllSessions(
      this.duplicateEventId
    );

    expect(originalEventSegments.length).toEqual(duplicateEventSegments.length);

    let stageSegmentsOriginal = originalEventSegments.filter(
      (segment) => segment.type == "STAGE"
    );
    let stageSegmentsDuplicate = duplicateEventSegments.filter(
      (segment) => segment.type == "STAGE"
    );
    let roomSegmentsOriginal = originalEventSegments.filter(
      (segment) => segment.type == "ROOMS"
    );
    let roomSegmentsDuplicate = duplicateEventSegments.filter(
      (segment) => segment.type == "ROOMS"
    );
    let expoSegmentsOriginal = originalEventSegments.filter(
      (segment) => segment.type == "EXPO"
    );
    let expoSegmentsDuplicate = duplicateEventSegments.filter(
      (segment) => segment.type == "EXPO"
    );

    expect(stageSegmentsOriginal.length).toEqual(stageSegmentsDuplicate.length);
    expect(roomSegmentsOriginal.length).toEqual(roomSegmentsDuplicate.length);
    expect(expoSegmentsOriginal.length).toEqual(expoSegmentsDuplicate.length);

    console.log("Original Stage Segments: ", stageSegmentsOriginal);
    console.log("Duplicate Stage Segments: ", stageSegmentsDuplicate);

    console.log("Original Room Segments: ", roomSegmentsOriginal);
    console.log("Duplicate Room Segments: ", roomSegmentsDuplicate);

    console.log("Original Expo Segments: ", expoSegmentsOriginal);
    console.log("Duplicate Expo Segments: ", expoSegmentsDuplicate);

    for (var i = 0; i < originalEventSegments.length; i++) {
      for (var j = 0; j < duplicateEventSegments.length; j++) {
        if (
          originalEventSegments[i].title == duplicateEventSegments[j].title &&
          originalEventSegments[i].type == duplicateEventSegments[j].type
        ) {
          await this.verifySegmentDetails(
            originalEventSegments[i],
            duplicateEventSegments[j]
          );
          break;
        }
      }
    }
  }

  async verifySegmentDetails(originalSegment, duplicateSegment) {
    console.log(
      "Original Segment: ",
      inspect(originalSegment, { depth: null })
    );
    console.log(
      "Duplicate Segment: ",
      inspect(duplicateSegment, { depth: null })
    );

    const toVerify = ["title", "description", "type"];
    for (var key of toVerify) {
      expect(originalSegment[key]).toEqual(duplicateSegment[key]);
    }

    // for (var key of ["startDateTime", "endDateTime"]) {
    //   let originalDate = new Date(originalSegment[key]);
    //   let duplicateDate = new Date(duplicateSegment[key]);
    //   console.log("Original Segment date ->", originalDate);
    //   console.log("Duplicate Segment date ->", duplicateDate);

    //   let deltaHours = duplicateDate.getHours() - originalDate.getHours();
    //   originalDate.setHours(0, 0, 0, 0);
    //   duplicateDate.setHours(0, 0, 0, 0);
    //   let deltaDays = Math.floor(
    //     (duplicateDate.getTime() - originalDate.getTime()) / (1000 * 3600 * 24)
    //   );
    //   console.log("Recieved Delta Hours: ", deltaHours);
    //   console.log("Recieved Delta Days: ", deltaDays);
    //   console.log("Expected Delta Hours: ", this.deltaHours);
    //   console.log("Expected Delta Days: ", this.deltaDays);
    //   let diffHours = Math.abs(deltaHours - this.deltaHours);
    //   let diffDays = Math.abs(deltaDays - this.deltaDays);
    //   console.log("Diff Hours: ", diffHours);
    //   console.log("Diff Days: ", diffDays);
    //   expect(diffHours).toBeLessThanOrEqual(1);
    //   expect(diffDays).toBeLessThanOrEqual(1);
    // }

    console.log("Original Segment ...");
    let [originalSegmentHour, originalSegmentMinutes] =
      await this.getSegmentDateDiff(originalSegment);
    console.log("Duplicate Segment ...");
    let [duplicateSegmentHour, duplicateSegmentMinutes] =
      await this.getSegmentDateDiff(duplicateSegment);
    expect(duplicateSegmentHour).toBe(originalSegmentHour);
    expect(duplicateSegmentMinutes).toBe(originalSegmentMinutes);

    // let tagsOriginal = originalSegment["tags"];
    // let tagsDuplicate = duplicateSegment["tags"];
    // expect(tagsOriginal).toEqual(tagsDuplicate);

    // let tagNamesOriginal = tagsOriginal.map((tag) => tag.name);
    // let tagNamesDuplicate = tagsDuplicate.map((tag) => tag.name);
    // console.log(`Orginal tag names - ${tagNamesOriginal}`);
    // console.log(`Duplicate tag names - ${tagNamesDuplicate}`);
    // expect(tagNamesOriginal.length).toEqual(tagNamesDuplicate.length);

    // for (let i = 0; i < tagNamesDuplicate.length; i++) {
    //   expect(tagNamesOriginal).toContain(tagNamesDuplicate[i]);
    // }
  }

  private async getSegmentDateDiff(segment) {
    let startDate = new Date(segment["startDateTime"]);
    let endDate = new Date(segment["endDateTime"]);
    console.log("Segment start date ->", startDate);
    console.log("Segment end date ->", endDate);

    let diffMilliseconds = endDate.getTime() - startDate.getTime();
    let diffMinutes = Math.floor(diffMilliseconds / (1000 * 60));
    let diffHours = Math.floor(diffMinutes / 60);
    diffMinutes = diffMinutes % 60;

    console.log("Schedule time | Hours: ", diffHours);
    console.log("Schedule time | Minutes: ", diffMinutes);

    return [diffHours, diffMinutes];
  }

  async verifyAccessGroupDetails(
    customAccessGroupName: string,
    accessGroupSettings: boolean
  ) {
    const defaultAccessGroupList = [
      "Attendees",
      "Booth Owners",
      "Moderators",
      "Organizers",
      "Speakers",
    ];

    //Fetching all access groups for duplicate event
    let duplicateEventAccessGroups =
      await this.accessGroupControllers[1].getAllAccessGroups();

    let duplicateEventAccessGroupsName: Array<string> = new Array<string>();
    for (const eachAccessGroupObject of duplicateEventAccessGroups) {
      const groupName = eachAccessGroupObject["groupName"];
      console.log(groupName);
      duplicateEventAccessGroupsName.push(groupName);
    }

    //Verify if the custom group doesn't get copied to duplicated event
    expect(duplicateEventAccessGroupsName).not.toContainEqual(
      customAccessGroupName
    );

    if (!accessGroupSettings) {
      //Verify that default groups have default settings if AccessGroup toggle is Off during duplication if event
      //.......//
      //Verify all default groups are present in the duplicated event
      expect(defaultAccessGroupList.length).toEqual(
        duplicateEventAccessGroupsName.length
      );
      for (var i = 0; i < duplicateEventAccessGroupsName.length; i++) {
        expect(defaultAccessGroupList).toContainEqual(
          duplicateEventAccessGroupsName[i]
        );
      }

      for (const eachAccessGroupObject of duplicateEventAccessGroups) {
        const groupId = eachAccessGroupObject["groupId"];
        const groupName = eachAccessGroupObject["groupName"];

        console.log(
          "Validating settings of -> ",
          groupName,
          " | Group Id -> ",
          groupId
        );
        await this.validateDefaultAccessGroupSettings(groupId);
      }
    } else {
      //Fetching all access groups for the original event
      let originalEventAccessGroups =
        await this.accessGroupControllers[0].getAllAccessGroups();

      for (const accessGroup of defaultAccessGroupList) {
        const originalGroup = originalEventAccessGroups.find(
          (group) =>
            group["gorupName"] == originalEventAccessGroups["groupName"]
        );
        const duplicateGroup = originalEventAccessGroups.find(
          (group) =>
            group["gorupName"] == originalEventAccessGroups["groupName"]
        );

        const originalGroupId = originalGroup["groupId"];
        const duplicateGroupId = duplicateGroup["groupId"];

        //verify all access group feature status
        await this.validateDuplicateEventAccessGroupSettings(
          originalGroupId,
          duplicateGroupId
        );
      }

      // expect(originalEventAccessGroups.length).toEqual(
      //   duplicateEventAccessGroups.length
      // );

      // const minimizedAccessGroupOrginal = originalEventAccessGroups.map(
      //   (accessGroup) => {
      //     return {
      //       groupName: accessGroup.groupName,
      //       groupType: accessGroup.groupType,
      //     };
      //   }
      // );

      // const minimizedAccessGroupDuplicate = duplicateEventAccessGroups.map(
      //   (accessGroup) => {
      //     return {
      //       groupName: accessGroup.groupName,
      //       groupType: accessGroup.groupType,
      //     };
      //   }
      // );

      // for (var i = 0; i < minimizedAccessGroupOrginal.length; i++) {
      //   console.log(minimizedAccessGroupOrginal[i]);
      //   expect(minimizedAccessGroupDuplicate).toContainEqual(
      //     minimizedAccessGroupOrginal[i]
      //   );
      // }

      // let originalDayCount =
      //   await this.accessGroupControllers[0].getAccessDaysCount(
      //     customAccessGroupName
      //   );
      // let duplicateDayCount =
      //   await this.accessGroupControllers[1].getAccessDaysCount(
      //     customAccessGroupName
      //   );

      // expect(originalDayCount["allowedDays"]).toEqual(
      //   duplicateDayCount["allowedDays"]
      // );
      // expect(originalDayCount["totalDays"]).toEqual(
      //   duplicateDayCount["totalDays"]
      // );

      // const featuresToValidate = ["ROOM", "STAGE", "SCHEDULE"];

      // for (var feature of featuresToValidate) {
      //   let originalFeatureAccess =
      //     await this.accessGroupControllers[0].getAccessForFeatureStatus(
      //       feature,
      //       customAccessGroupName
      //     );
      //   let duplicateFeatureAccess =
      //     await this.accessGroupControllers[1].getAccessForFeatureStatus(
      //       feature,
      //       customAccessGroupName
      //     );
      //   console.log("Original Feature Access: ", originalFeatureAccess);
      //   console.log("Duplicate Feature Access: ", duplicateFeatureAccess);
      //   expect(originalFeatureAccess).toEqual(duplicateFeatureAccess);
      // }

      // let speakerListDuplicate = await getListOfSpeakersInEvent(
      //   this.apiContext,
      //   this.duplicateEventId
      // );

      // expect(speakerListDuplicate.length).toEqual(0);

      // let attendeeCountDuplicate =
      //   await this.accessGroupControllers[1].getTotalAttendeeCount();
      // expect(attendeeCountDuplicate).toEqual(0);
    }
  }

  async verifyAccessGroupsInTicketedEventDuplicate(duplicateEventAccessGroups) {
    //Verifying only default groups are present in the duplicate of a ticketed event
    const defaultAccessGroupList = [
      "Attendees",
      "Booth Owners",
      "Moderators",
      "Organizers",
      "Speakers",
    ];

    let duplicateEventAccessGroupsName: Array<string> = new Array<string>();
    for (const eachAccessGroupObject of duplicateEventAccessGroups) {
      const groupName = eachAccessGroupObject["groupName"];
      console.log(groupName);
      duplicateEventAccessGroupsName.push(groupName);
    }
    console.log(
      "Duplicate event access groups ...",
      duplicateEventAccessGroupsName
    );

    expect(defaultAccessGroupList.length).toEqual(
      duplicateEventAccessGroupsName.length
    );
    for (var i = 0; i < duplicateEventAccessGroupsName.length; i++) {
      expect(defaultAccessGroupList).toContainEqual(
        duplicateEventAccessGroupsName[i]
      );
    }
  }

  async verifyGroupsUnderAccessControlInTicketedEventDuplicate(accessGroups) {
    for (const eachGroup of accessGroups) {
      await this.verifyAccessGroupsUnderAccessControlOfGroup(
        eachGroup["groupId"]
      );
    }
  }

  private async verifyAccessGroupsUnderAccessControlOfGroup(groupId) {
    const featureList = await this.fetchAccessGroupFeatures(groupId);
    let pvtChat;
    let pvtMeetings;

    console.log("Features ... ", inspect(featureList, { depth: null }));

    for (const feature of featureList) {
      if (feature["featureName"] == "PVT_CHAT") {
        pvtChat = feature["subFeatures"];
      } else if (feature["featureName"] == "PVT_MEETING") {
        pvtMeetings = feature["subFeatures"];
      }
    }

    expect(pvtChat).toBeDefined();
    expect(pvtMeetings).toBeDefined();

    let pvtChatAccessGroups = new Array<string>();
    let pvtMeetingsAccessGroups = new Array<string>();

    for (const subFeature of pvtChat) {
      pvtChatAccessGroups.push(subFeature["dynSubFeatureName"]);
    }

    for (const subFeature of pvtMeetings) {
      pvtMeetingsAccessGroups.push(subFeature["dynSubFeatureName"]);
    }

    console.log("Access Groups under Pvt Chat feature : ", pvtChatAccessGroups);
    console.log(
      "Access Groups under Pvt Meetings feature : ",
      pvtMeetingsAccessGroups
    );

    const defaultAccessGroupList = [
      "Attendees",
      "Booth Owners",
      "Moderators",
      "Organizers",
      "Speakers",
    ];

    expect(defaultAccessGroupList.length).toEqual(pvtChatAccessGroups.length);
    expect(defaultAccessGroupList.length).toEqual(
      pvtMeetingsAccessGroups.length
    );

    for (const group of defaultAccessGroupList) {
      expect(pvtChatAccessGroups).toContainEqual(group);
      expect(pvtMeetingsAccessGroups).toContainEqual(group);
    }
  }

  private async fetchAccessGroupFeatures(groupId: string | number) {
    const getFeaturesUri = `/api/access_groups/${this.duplicateEventId}/${groupId}`;
    let response = await this.apiContext.get(getFeaturesUri);

    if (!response.ok()) {
      throw new Error(
        `API to get list of features failed with response -> ${response.status()}`
      );
    }

    const responseJson = await response.json();
    const featureList = responseJson["features"];

    return featureList;
  }

  private async validateDefaultAccessGroupSettings(
    accessGroupId: string | number
  ) {
    const featureList = await this.fetchAccessGroupFeatures(accessGroupId);

    for (const eachFeature of featureList) {
      console.log("...Feature...");
      console.log("Feature Name -> ", eachFeature["featureName"]);
      console.log("Feature Details -> ", inspect(eachFeature, { depth: null }));
      const featureStatus = eachFeature["isFeatureActive"];
      const subFeaturesList = eachFeature["subFeatures"];

      expect(featureStatus).toBe(true);

      if (subFeaturesList.length != 0) {
        for (const subFeature of subFeaturesList) {
          const subFeatureStatus = subFeature["isSubFeatureActive"];
          if (subFeatureStatus != null) expect(subFeatureStatus).toBe(true);
        }
      }
    }
  }

  private async validateDuplicateEventAccessGroupSettings(
    originalGroupId: string | number,
    duplicateGroupId: string | number
  ) {
    const originalFeatures = await this.fetchAccessGroupFeatures(
      originalGroupId
    );
    const duplicateFeatures = await this.fetchAccessGroupFeatures(
      duplicateGroupId
    );

    expect(originalFeatures.length).toEqual(duplicateFeatures.length);

    console.log("...Features...");
    for (var i = 0; i < originalFeatures.length; i++) {
      const ogFeature = originalFeatures[i];
      console.log("Original Feature Name -> ", ogFeature["featureName"]);
      console.log(
        "Original Feature Details -> ",
        inspect(ogFeature, { depth: null })
      );

      const dpFeature = duplicateFeatures.find(
        (feature) => feature["featureName"] == dpFeature["featureName"]
      );
      console.log("Original Feature Name -> ", dpFeature["featureName"]);
      console.log(
        "Original Feature Details -> ",
        inspect(dpFeature, { depth: null })
      );

      expect(ogFeature["isFeatureActive"]).toBe(dpFeature["isFeatureActive"]);
    }
  }

  async verifyRegistrationPageDetails(eventSetup: boolean) {
    let originalLandingPageDetails = await (
      await getLandingPageDetails(this.apiContext, this.originalEventId)
    ).json();

    let duplicateLandingPageDetails = await (
      await getLandingPageDetails(this.apiContext, this.duplicateEventId)
    ).json();

    console.log(
      "Original Landing Page Details........ \n",
      inspect(originalLandingPageDetails, { depth: null })
    );
    console.log(
      "Duplicate Landing Page Details........ \n",
      inspect(duplicateLandingPageDetails, { depth: null })
    );

    if (eventSetup) {
      const toVerifyRoot = [
        "landingPageTemplateName",
        "formBgColour",
        "formFontColour",
        "eventEntryType",
        "speakerMagicLinkEnabled",
        "magicLinkEnabled",
      ];
      for (var key of toVerifyRoot) {
        expect(originalLandingPageDetails[key]).toEqual(
          duplicateLandingPageDetails[key]
        );
      }

      const ToVerifyCustomFields = [
        "fieldName",
        "labelName",
        "fieldType",
        "isRequired",
        "dataType",
        "dropdownValues",
      ];
      let originalCustomFields =
        originalLandingPageDetails["registrationCustomFieldList"];
      let duplicateCustomFields =
        duplicateLandingPageDetails["registrationCustomFieldList"];

      expect(originalCustomFields.length).toEqual(duplicateCustomFields.length);

      for (var i = 0; i < originalCustomFields.length; i++) {
        for (var key of ToVerifyCustomFields) {
          expect(originalCustomFields[i][key]).toEqual(
            duplicateCustomFields[i][key]
          );
        }
      }
    } else {
      let duplicateCustomFields =
        duplicateLandingPageDetails["registrationCustomFieldList"];

      expect(duplicateCustomFields).toBeNull();
    }
  }

  async verifyRecordingsInDuplicatedEvent() {
    const originalRecordingController = new RecordingController(
      this.apiContext,
      this.originalEventId
    );
    const duplicateRecordingController = new RecordingController(
      this.apiContext,
      this.duplicateEventId
    );

    const originalRecordings =
      await originalRecordingController.fetchRecordings();
    const duplicateRecordings =
      await duplicateRecordingController.fetchRecordings();

    console.log(
      "Original recordings -> ",
      inspect(originalRecordings, { depth: null })
    );
    console.log(
      "Duplicate recordings -> ",
      inspect(duplicateRecordings, { depth: null })
    );

    expect(duplicateRecordings).toBe("");
  }

  async validateVenueSettings(venueSettings: boolean) {
    if (venueSettings) {
      await this.compareVenueSettings();
    } else {
      await this.validateDefaultVenueSettings();
    }
  }

  private async compareVenueSettings() {
    const originalVenueController = new VenueController(
      this.apiContext,
      this.originalEventId
    );
    const duplicateVenueController = new VenueController(
      this.apiContext,
      this.duplicateEventId
    );

    const originalVenueResponse =
      await originalVenueController.getVenueZoneList();
    const duplicateVenueResponse =
      await duplicateVenueController.getVenueZoneList();

    const originalSettings = await originalVenueResponse.json();
    const duplicateSettings = await duplicateVenueResponse.json();

    console.log(
      "Original Venue Settings..... ",
      inspect(originalSettings, { depth: null })
    );
    console.log(
      "Duplicate Venue Settings..... ",
      inspect(duplicateSettings, { depth: null })
    );

    for (const original of originalSettings) {
      for (const duplicate of duplicateSettings) {
        if (original["type"] === duplicate["type"]) {
          expect(original["active"]).toBe(duplicate["active"]);
        }
      }
    }
  }

  private async validateDefaultVenueSettings() {
    const duplicateVenueController = new VenueController(
      this.apiContext,
      this.duplicateEventId
    );

    const duplicateVenueResponse =
      await duplicateVenueController.getVenueZoneList();
    const duplicateVenueSettings = await duplicateVenueResponse.json();

    console.log(
      "Original Venue Settings..... ",
      inspect(duplicateVenueSettings, { depth: null })
    );

    for (const duplicate of duplicateVenueSettings) {
      expect(duplicate["active"]).toBe(true);
    }
  }
}
