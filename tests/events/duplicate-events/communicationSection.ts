import { APIRequestContext, Page } from "@playwright/test";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";
import EventRole from "../../../enums/eventRoleEnum";

export class CommunicationSection {
  protected orgApiRequestContext: APIRequestContext;
  protected eventId: any;
  protected page;

  constructor(orgApiReuqestContext: APIRequestContext, eventId: any) {
    this.orgApiRequestContext = orgApiReuqestContext;
    this.eventId = eventId;
  }

  async toogleCommunicationSettings(
    roleType: EventRole,
    communicationType: EventSettingID,
    enable: boolean
  ) {
    await enableDisableEmailTrigger(
      this.orgApiRequestContext,
      this.eventId,
      communicationType,
      true,
      roleType,
      enable
    );
  }
}
