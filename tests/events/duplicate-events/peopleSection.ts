import { APIRequestContext, Page, expect } from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { DataUtl } from "../../../util/dataUtil";
import { getAllRegistrationIdOfEvent } from "../../../util/apiUtil";
import { AccessGroupController } from "../../../controller/AccessGroupController";
import { group } from "console";

export class PeopleSection {
  protected orgApiRequestContext: APIRequestContext;
  protected eventId: any;
  protected eventController: EventController;
  protected accessGroupController: AccessGroupController;
  protected page;

  constructor(
    orgApiReuqestContext: APIRequestContext,
    eventId: any,
    page: Page
  ) {
    this.orgApiRequestContext = orgApiReuqestContext;
    this.eventId = eventId;
    this.page = page;

    this.eventController = new EventController(
      this.orgApiRequestContext,
      this.eventId
    );

    this.accessGroupController = new AccessGroupController(
      this.orgApiRequestContext,
      this.eventId
    );
  }

  async addRandomSpeakersToEvent(numberOfSpeakersToBeAdded: number = 5) {
    let i = 0;
    while (i < numberOfSpeakersToBeAdded) {
      let spekaerEmail = DataUtl.getRandomSpeakerEmail();
      let speakerFirstName = DataUtl.getRandomName();

      await this.eventController.inviteSpeakerToTheEventByApi(
        spekaerEmail,
        speakerFirstName,
        `RandomSpeaker${i + 1}`
      );
      i++;
    }
  }

  async addRandomAttendeesToEvent(numberOfAttendeesToBeAdded: number = 30) {
    let i = 0;
    while (i < numberOfAttendeesToBeAdded) {
      let attendeeEmail = DataUtl.getRandomAttendeeEmail();

      await this.eventController.inviteAttendeeToTheEvent(attendeeEmail);
      i++;
    }
  }

  async verifyCountOfSpeakersInSetupPage(expectedCountOfSpeakers: number) {
    let setupPageUrl = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/event/${this.eventId}/people/speakers`;

    await this.page.goto(setupPageUrl);
    await this.page.waitForTimeout(10000);

    let totalCountString = await this.page
      .locator("div[class*='plan-restriction-info_infoContainer'] p")
      .textContent();

    console.log(`Speaker string - ${totalCountString}`);

    let numberString = await totalCountString.match(/(\d+)/);

    let actualCountOfSpeakers = +numberString[0];

    console.log(`Count of Speakers - ${actualCountOfSpeakers[0]}`);

    expect(actualCountOfSpeakers).toBe(expectedCountOfSpeakers);
  }

  async verifyCountOfAttendeesInSetupPage(expectedCountOfAttendees: number) {
    // let setupPageUrl = `${
    //   DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    // }/event/${this.eventId}/people/attendees`;

    let registeredUsers = await getAllRegistrationIdOfEvent(
      this.orgApiRequestContext,
      this.eventId
    );

    const actualCountOfAttendees = await registeredUsers.length;

    // await this.page.goto(setupPageUrl);
    // await this.page.waitForTimeout(10000);

    // let totalCountString = await this.page
    //   .locator("div[class*='plan-restriction-info_infoContainer'] p")
    //   .textContent();

    // console.log(`Attendee string - ${totalCountString}`);

    // let numberString = await totalCountString.match(/(\d+)/);

    // let actualCountOfAttendees: number = +numberString[0];
    // console.log(`Count of Attendees - ${actualCountOfAttendees}`);

    expect(actualCountOfAttendees).toBe(expectedCountOfAttendees);
  }

  async verifyAccessGroupPresence(accessGroupName: string) {
    let setupPageUrl = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/event/${this.eventId}/people/accessGroups`;

    await this.page.goto(setupPageUrl);

    let accessGroupLocator = await this.page
      .locator("button[class*='access-groups_arrowButton']")
      .filter({ hasText: accessGroupName });

    try {
      await expect(accessGroupLocator).toBeVisible({ timeout: 5000 });
    } catch (error) {
      await this.page.reload();
      await expect(accessGroupLocator).toBeVisible({ timeout: 5000 });
    }
  }

  async verifyAccessGroupInEventViaAPI(
    groupName: string,
    present: boolean = true
  ) {
    const accessGroupsJson =
      await this.accessGroupController.getAllAccessGroups();

    let accessGroupNames: Array<string> = new Array<string>();

    for (const accessGroup of accessGroupsJson) {
      accessGroupNames.push(accessGroup["groupName"]);
    }

    if (present) expect(accessGroupNames).toContainEqual(groupName);
    else expect(accessGroupNames).not.toContainEqual(groupName);
  }
}
