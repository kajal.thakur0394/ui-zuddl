import { BrowserContext, APIRequestContext, Page } from "@playwright/test";
import { getEventDetails } from "../../../util/apiUtil";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import LandingPageType from "../../../enums/landingPageEnum";
import { updateLandingPageType } from "../../../util/apiUtil";
import { EventController } from "../../../controller/EventController";
import { RegistrationFormController } from "../../../controller/RegistrationFormController";
import conditionalFieldsTestData from "../../../test-data/conditional-fields-testdata/createConditionalFields.json";

export default class RegistrationSection {
  protected page: Page;
  protected apiContext: APIRequestContext;
  protected browserContext: BrowserContext;
  protected eventId: string;
  protected eventTitle: string;
  protected eventController: EventController;
  protected eventInfoDto: EventInfoDTO;
  protected landingPageId;
  protected registrationFormController: RegistrationFormController;

  constructor(
    apiContext: APIRequestContext,
    eventId: string,
    eventTitle: string
  ) {
    this.apiContext = apiContext;
    this.eventId = eventId;
    this.eventTitle = eventTitle;

    this.eventController = new EventController(apiContext, eventId);
    this.eventInfoDto = new EventInfoDTO(this.eventId, eventTitle);
  }

  async setupLandingPage() {
    this.eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
    const event_details_api_resp = await (
      await getEventDetails(this.apiContext, this.eventId)
    ).json();
    console.log("event_details_api_resp :", event_details_api_resp);

    this.landingPageId = event_details_api_resp.eventLandingPageId;
    this.eventInfoDto.landingPageId = this.landingPageId;

    let customConditionalFieldsTestData =
      conditionalFieldsTestData["endToend-custom-conditional-fields-data"];
    let discalimerFieldsTestData =
      conditionalFieldsTestData["endToend-custom-discalimer-fields-data"];

    console.log("######## adding custom fields to the events ##############");
    await this.eventController.addCustomFieldsWithConditionToEvent(
      customConditionalFieldsTestData
    );

    console.log(
      "######## adding disclaimer fields to the events ##############"
    );
    await this.eventController.addDiscalimerFieldsToEvent(
      discalimerFieldsTestData
    );

    let attendee_landing_page = this.eventInfoDto.attendeeLandingPage;
    console.log(attendee_landing_page);
  }

  async updateLandingPageType() {
    await updateLandingPageType(
      this.apiContext,
      this.eventId,
      this.landingPageId,
      LandingPageType.STYLE2,
      "true",
      EventEntryType.REG_BASED
    );

    let eventInfo = await this.eventController.getEventInfo();
    let eventInfoResp = await eventInfo.json();
    console.log("eventInfoResp :", eventInfoResp);
  }
}
