import test, { APIRequestContext, BrowserContext } from "@playwright/test";
import BrowserName from "../../../enums/BrowserEnum";
import BrowserFactory from "../../../util/BrowserFactory";
import { EventController } from "../../../controller/EventController";
import {
  updateEventLandingPageDetails,
  cancelEvent,
  createNewEvent,
  updateEventType,
} from "../../../util/apiUtil";
import { PeopleSection } from "./peopleSection";
import { AccessGroupController } from "../../../controller/AccessGroupController";
import DuplicateValidation from "./duplicateValidation";
import EventType from "../../../enums/eventTypeEnum";
import { TicketingController } from "../../../controller/TicketingController";
import { inspect } from "util";
import { StageController } from "../../../controller/StageController";
import { StudioController } from "../../../controller/StudioController";

test.describe(
  `Duplicate Events | Ticketed Event @duplicateEvent @ticketedEvent`,
  {
    tag: "@smoke",
  },
  async () => {
    let eventTitle = "Ticketing-TestEvent";
    let eventId;
    let stageId;

    let orgApiContext: APIRequestContext;
    let orgBrowserContext: BrowserContext;
    let organiserPage;

    let peopleSection: PeopleSection;
    let eventController: EventController;
    let accessGroupController: AccessGroupController;

    let duplicateValidation: DuplicateValidation;

    test.beforeAll(async () => {
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-901",
      });
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-902",
      });
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-903",
      });

      await test.step("Initializing Organiser browser and api context.", async () => {
        orgBrowserContext = await BrowserFactory.getBrowserContext({
          browserName: BrowserName.CHROME,
          laodOrganiserCookies: true,
        });

        orgApiContext = orgBrowserContext.request;
      });

      await test.step("Creating new ticketed event.", async () => {
        eventId = await createNewEvent({
          api_request_context: orgApiContext,
          event_title: eventTitle,
          isFlex: true,
        });
        console.log(`event id ${eventId}`);
      });

      await test.step(`update an event with IN-PERSON as event type`, async () => {
        await updateEventType(orgApiContext, eventId, EventType.IN_PERSON);
        await updateEventLandingPageDetails(orgApiContext, eventId, {
          isMagicLinkEnabled: true,
        });
      });

      await test.step("Initalizing controllers.", async () => {
        eventController = new EventController(orgApiContext, eventId);
        accessGroupController = new AccessGroupController(
          orgApiContext,
          eventId
        );
        accessGroupController = new AccessGroupController(
          orgApiContext,
          eventId
        );
        stageId = await eventController.getDefaultStageId();
      });

      await test.step("Initalizing Organiser page.", async () => {
        organiserPage = await orgBrowserContext.newPage();
      });

      await test.step("Initalizing PeopleSection class object.", async () => {
        peopleSection = new PeopleSection(
          orgApiContext,
          eventId,
          organiserPage
        );
      });

      let eventDetails = await eventController.getEventInfo();
      const eventInfo = await eventDetails.json();

      console.log("Event ID: ", eventId);
      console.log("Stage ID: ", stageId);
      console.log("Event Info: ", eventInfo);
    });

    test.afterAll(async () => {
      await cancelEvent(orgApiContext, eventId);

      await organiserPage?.close();
      await orgBrowserContext?.close();
    });

    test("TC001: Access groups for ticketed event are not copied to duplicated event. @eventSetup", async () => {
      let virtualTickeName: string = "Virtual";
      let inPersonTicketName: string = "InPerson";
      const deltaHours = -5;
      const deltaDays = 3;

      await test.step(`Create tickets`, async () => {
        let inPersonTicket = {
          name: inPersonTicketName,
          description: "desc",
          ticketTagType: EventType.IN_PERSON,
          numberOfTickets: 10,
          pricePerTicket: 0,
          isBulkPurchaseEnabled: true,
          minTicketsPerOrder: 2,
          maxTicketsPerOrder: 10,
          soldTickets: 0,
        };
        let virtualTicket = {
          name: virtualTickeName,
          description: "desc virtual",
          ticketTagType: EventType.VIRTUAL,
          numberOfTickets: 10,
          pricePerTicket: 0,
          isBulkPurchaseEnabled: true,
          minTicketsPerOrder: 2,
          maxTicketsPerOrder: 10,
          soldTickets: 0,
        };

        let ticketingController = new TicketingController(
          orgApiContext,
          eventId
        );
        await ticketingController.selectGateway();
        await ticketingController.createTicket(virtualTicket);
        await ticketingController.createTicket(inPersonTicket);
      });

      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-916",
      });
      await test.step(`Verify the Access group for tickets is created and Attendee group is removed.`, async () => {
        const accessGroups = await accessGroupController.getAllAccessGroups();
        console.log(
          "Access groups in original event ... ",
          inspect(accessGroups, { depth: null })
        );

        const groupsExpectedToBePresent: Array<string> = [
          "Ticket " + inPersonTicketName,
          "Ticket " + virtualTickeName,
        ];

        const groupsExpectedNotToBePresent: Array<string> = ["Attendee"];

        for (const groupName of groupsExpectedToBePresent) {
          await peopleSection.verifyAccessGroupInEventViaAPI(groupName);
        }

        for (const groupName of groupsExpectedNotToBePresent) {
          await peopleSection.verifyAccessGroupInEventViaAPI(groupName, false);
        }
      });

      let duplicateEventTitle;
      let duplicateEventId;
      await test.step(`Duplicate the event.`, async () => {
        await test.step("Create duplicate of event", async () => {
          let eventInfo = await eventController.getEventInfo();
          let eventInfoResp = await eventInfo.json();
          console.log("eventInfoResp :", eventInfoResp);

          duplicateEventTitle = eventTitle + " - Duplicate Automation";
          console.log("Duplicate Event title: ", duplicateEventTitle);
          duplicateEventId = await eventController.duplicateEvent({
            title: duplicateEventTitle,
            deltaHours: deltaHours,
            deltaDays: deltaDays,
          });

          console.log("Duplicate event id: ", duplicateEventId);

          let duplicateEventController = new EventController(
            orgApiContext,
            duplicateEventId
          );

          let duplicateEventInfo =
            await duplicateEventController.getEventInfo();
          let duplicateEventInfoResp = await duplicateEventInfo.json();
          console.log("duplicateEventInfoResp :", duplicateEventInfoResp);
        });

        duplicateValidation = new DuplicateValidation(
          orgApiContext,
          eventId,
          duplicateEventId,
          eventTitle,
          duplicateEventTitle,
          deltaHours,
          deltaDays
        );
      });

      let duplicateAccessGroupController: AccessGroupController =
        new AccessGroupController(orgApiContext, duplicateEventId);
      const duplicateAccessGroups =
        await duplicateAccessGroupController.getAllAccessGroups();
      console.log(
        "Access groups in duplicate event ... ",
        inspect(duplicateAccessGroups, { depth: null })
      );

      await test.step(`Verify the Access groups are not copied to the duplicate event.`, async () => {
        await duplicateValidation.verifyAccessGroupsInTicketedEventDuplicate(
          duplicateAccessGroups
        );
      });

      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-915",
      });

      // await test.step(`Verify the recording functionality is working properly in duplicated event.`, async () => {
      //   let studioId = await eventController.enableStudioAsBackstage(stageId);
      //   let studioController = new StudioController(
      //     orgApiContext,
      //     studioId,
      //     stageId
      //   );
      //   let sessionResponse;

      //   await studioController.disableDryRun();
      //   sessionResponse = await studioController.startSessionOnStudio();
      //   await organiserPage.waitForTime(15000);
      //   await studioController.stopSessionOnStudio();

      //   console.log(
      //     "Session start response - ",
      //     inspect(sessionResponse, { depth: null })
      //   );
      // });
    });
  }
);
