import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import BrowserName from "../../../enums/BrowserEnum";
import EventEntryType from "../../../enums/eventEntryEnum";
import BrowserFactory from "../../../util/BrowserFactory";
import { EventController } from "../../../controller/EventController";
import {
  updateEventLandingPageDetails,
  cancelEvent,
} from "../../../util/apiUtil";
import { BrandingController } from "../../../controller/BrandingController";
import { DataUtl } from "../../../util/dataUtil";
import { PeopleSection } from "./peopleSection";
import { AccessGroupController } from "../../../controller/AccessGroupController";
import VenueSection, { Room, Tag, Stage, Booth } from "./venueSection";
import RegistrationSection from "./registrationSection";
import { CommunicationSection } from "./communicationSection";
import EventRole from "../../../enums/eventRoleEnum";
import EventSettingID from "../../../enums/EventSettingEnum";
import DuplicateValidation from "./duplicateValidation";
import { StageController } from "../../../controller/StageController";
import { ScheduleAccessControl } from "../../../controller/AccessControl/AccessControl_Schedule";
import {
  CustomAccessGroupController,
  FilterLogicPair,
} from "../../../controller/AccessControl/CustomAccessGroupController";
import { AccessControlAttendeeVerification } from "../../../page-objects/access-control/roles/Attendees";
import { AccessControlController } from "../../../controller/AccessControl/AccessControlController";
import { Roles } from "../../../enums/AccessControlEnum";
import {
  FilterLogic,
  FilterOperators,
  Filters,
  FiltersUserInput,
} from "../../../enums/AccessGroupEnum";
import StageSize from "../../../enums/StageSizeEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

let speakerCount = 1;
let attendeeCount = 5;

let customAccessGroupName = "CustomAccessGroup";

test.describe(
  `Duplicate Events | AccessGroup Setting @duplicateEvent @accessGroup`,
  {
    tag: "@smoke",
  },
  async () => {
    let eventTitle;
    let eventId;
    let stageId;

    let orgApiContext: APIRequestContext;
    let orgBrowserContext: BrowserContext;
    let organiserPage;

    let attendeeEmail: string;
    let attendeeOnePage;
    let attendeeBrowserContext: BrowserContext;

    let peopleSection: PeopleSection;
    let eventController: EventController;
    let brandingController: BrandingController;
    let accessGroupController: AccessGroupController;
    let randomRoom: Room;
    let randomStage: Stage;
    let randomBooth: Booth;
    let scheduleSection: VenueSection;
    let stageController: StageController;
    let customAccessGroupController: CustomAccessGroupController;
    const deltaHours = -5;
    const deltaDays = 3;

    test.beforeAll(async () => {
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-878",
      });
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-913",
      });

      await test.step("Initializing Organiser browser and api context.", async () => {
        orgBrowserContext = await BrowserFactory.getBrowserContext({
          browserName: BrowserName.CHROME,
          laodOrganiserCookies: true,
        });

        orgApiContext = orgBrowserContext.request;
      });

      eventTitle = DataUtl.getRandomEventTitle();
      await test.step("Creating new event.", async () => {
        eventId = await EventController.generateNewEvent(orgApiContext, {
          event_title: eventTitle,
          add_end_date_from_today: 2,
        });
      });

      await test.step("Initalizing controllers.", async () => {
        eventController = new EventController(orgApiContext, eventId);
        brandingController = new BrandingController(orgApiContext, eventId);
        accessGroupController = new AccessGroupController(
          orgApiContext,
          eventId
        );
        customAccessGroupController = new CustomAccessGroupController(
          orgApiContext,
          eventId
        );
        stageController = new StageController(orgApiContext, eventId);
        scheduleSection = new VenueSection(orgApiContext, eventId);
        accessGroupController = new AccessGroupController(
          orgApiContext,
          eventId
        );
      });

      await test.step("Initalizing Organiser page.", async () => {
        organiserPage = await orgBrowserContext.newPage();
      });

      await test.step("Initalizing PeopleSection class object.", async () => {
        peopleSection = new PeopleSection(
          orgApiContext,
          eventId,
          organiserPage
        );
      });

      await test.step("Fetching deafult stage id.", async () => {
        stageId = await eventController.getDefaultStageId();
      });

      await test.step("Disable onboarding checks for attendee", async () => {
        await eventController.disableOnboardingChecksForAttendee();
      });

      await test.step("Initializing Attendee browser context.", async () => {
        attendeeEmail = DataUtl.getRandomAttendeeEmail();
        attendeeBrowserContext = await BrowserFactory.getBrowserContext({
          browserName: BrowserName.CHROME,
          laodOrganiserCookies: false,
        });
      });

      await test.step("Registering attendee.", async () => {
        await eventController.inviteAttendeeToTheEvent(attendeeEmail);
      });

      await test.step("Enabling Magiclink for attendee.", async () => {
        await updateEventLandingPageDetails(orgApiContext, eventId, {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
        });
      });

      await test.step("Initializing Attende page.", async () => {
        attendeeOnePage = await attendeeBrowserContext.newPage();
      });

      await test.step("Attendee one logs in to the event.", async () => {
        let magicLink: string;
        await test.step("Get magic link for attendee one.", async () => {
          magicLink = await QueryUtil.fetchMagicLinkFromDB(
            eventId,
            attendeeEmail
          );
        });

        await test.step("Attendee one logs in to the event.", async () => {
          await attendeeOnePage.goto(magicLink);

          await attendeeOnePage.click("text=Enter Now");
        });
      });

      let eventDetails = await eventController.getEventInfo();
      const eventInfo = await eventDetails.json();

      console.log("Event ID: ", eventId);
      console.log("Stage ID: ", stageId);
      console.log("Event Info: ", eventInfo);
    });

    test.afterAll(async () => {
      await cancelEvent(orgApiContext, eventId);

      await organiserPage?.close();
      await attendeeOnePage?.close();
      await orgBrowserContext?.close();
      await attendeeBrowserContext?.close();
    });

    test("TC001: Event Setup. @eventSetup", async () => {
      await test.step("Randomise the branding of complete event.", async () => {
        await brandingController.randomiseBranding();
      });

      await test.step("Verify branding of complete event.", async () => {
        let eventInfo = await eventController.getEventInfo();
        let eventInfoResp = await eventInfo.json();
        console.log("eventInfoResp :", eventInfoResp);
      });

      await attendeeOnePage.reload();
    });

    test("TC002: Access Group Setup. @accessGroupSetup", async () => {
      await test.step("Add Custom access group.", async () => {
        const filters: FiltersUserInput[] = [
          {
            filterName: Filters.EMAIL,
            filterOperator: FilterOperators.CONTAINS,
            compareValue: "gmail",
          },
        ];
        const filterLogic: FilterLogicPair[] = [
          { key: Filters.EMAIL, value: FilterLogic.OR },
        ];

        await customAccessGroupController.createCustomGroup(
          customAccessGroupName,
          "ACTIVE",
          true,
          filters,
          filterLogic,
          FilterLogic.AND
        );
      });

      let accessdayCount: number;
      await test.step("Get access day count.", async () => {
        let accessDaysInfo = await accessGroupController.getAccessDaysCount(
          customAccessGroupName
        );
        expect(accessDaysInfo["allowedDays"]).toBe(accessDaysInfo["totalDays"]);
        accessdayCount = accessDaysInfo["totalDays"];
      });

      await test.step("Remove access of the accessGroup from day 1.", async () => {
        await accessGroupController.toggleDayForAccessGroup(
          customAccessGroupName,
          1,
          false
        );
      });

      await test.step("Verify access day count.", async () => {
        let accessDaysInfo = await accessGroupController.getAccessDaysCount(
          customAccessGroupName
        );
        expect(accessDaysInfo["allowedDays"]).toBe(accessdayCount - 1);
      });

      await test.step("Add Speakers.", async () => {
        await peopleSection.addRandomSpeakersToEvent(speakerCount);
      });

      await test.step("Add Attendees.", async () => {
        await peopleSection.addRandomAttendeesToEvent(attendeeCount);
      });

      await test.step("Verify Speaker count.", async () => {
        await peopleSection.verifyCountOfSpeakersInSetupPage(speakerCount);
      });

      await test.step("Verify Attendee count.", async () => {
        await peopleSection.verifyCountOfAttendeesInSetupPage(
          attendeeCount + 1
        );
      });

      await test.step("Remove access to ROOM, STAGE and SCHEDULE", async () => {
        const toBeToggled = ["ROOM", "STAGE", "SCHEDULE"];
        for (let feature of toBeToggled) {
          await test.step(`Remove access to ${feature}`, async () => {
            await accessGroupController.toggleFeatureForAccessGroup(
              feature,
              customAccessGroupName,
              false
            );
          });
        }
      });
    });

    test("TC003: Venue Setup. @venueSetup", async () => {
      let venueSection = new VenueSection(orgApiContext, eventId);

      await test.step("Create random rooms.", async () => {
        await venueSection.createRandomRooms();
      });

      let rooms: Room[] = [];
      await test.step("Get all rooms.", async () => {
        rooms = await venueSection.getAllRooms();
      });

      randomRoom = rooms[DataUtl.randomIntFromInterval(0, rooms.length - 1)];
      console.log("Random room: ", randomRoom);

      let tags: Tag[] = [];
      await test.step("Create Tags with name tag_1 to tag_5", async () => {
        tags = await venueSection.createTags();
      });

      let randomTags = tags.slice(
        0,
        DataUtl.randomIntFromInterval(1, tags.length)
      );

      await test.step("Add tags to the room", async () => {
        await venueSection.addTagsToRoom(randomRoom.roomId, randomTags);
      });

      await test.step("Randomise the room details", async () => {
        let randomisedRoomDetails = await venueSection.randomiseRoomDetails(
          randomRoom.roomId
        );
        console.log("Randomised room details: ", randomisedRoomDetails);
        randomRoom = randomisedRoomDetails;
      });

      let stages: Stage[];
      await test.step("Create random stages", async () => {
        stages = await venueSection.createRandomStages();
      });

      console.log("Stages: ", stages);
      randomStage = stages[DataUtl.randomIntFromInterval(0, stages.length - 1)];

      await test.step("Randomise the stage details", async () => {
        randomStage = await venueSection.randomiseStageDetails(
          randomStage.stageId
        );
      });

      let booths: Booth[];

      await test.step("Create random expo with multiple booths and sizes", async () => {
        booths = await venueSection.setupExpo();
      });

      console.log("Booths: ", booths);

      randomBooth = booths[DataUtl.randomIntFromInterval(0, booths.length - 1)];

      await test.step("Delete random booth", async () => {
        await venueSection.deleteBooth(randomBooth.boothId);
        booths = booths.filter((booth) => booth.boothId != randomBooth.boothId);
      });

      randomBooth = booths[DataUtl.randomIntFromInterval(0, booths.length - 1)];

      await test.step("Add widgets to some booths", async () => {
        await venueSection.addWidgetsToBooths([randomBooth]);
      });

      await test.step("Create segment in room", async () => {
        await venueSection.createSegments("ROOMS", randomTags);
      });

      await test.step("Create segment in stage", async () => {
        await venueSection.createSegments("STAGE", randomTags);
      });

      await test.step("Create segment in booth", async () => {
        await venueSection.createSegments("EXPO", randomTags);
      });
    });

    test("TC004: Registration Setup. @registrationSetup", async () => {
      let registrationSection = new RegistrationSection(
        orgApiContext,
        eventId,
        eventTitle
      );

      await test.step("Setup landing page", async () => {
        await registrationSection.setupLandingPage();
      });

      await test.step("Change landing page type", async () => {
        await registrationSection.updateLandingPageType();
      });
    });

    test("TC005: Communication Setup. @communicationSetup", async () => {
      let communicationSettings = new CommunicationSection(
        orgApiContext,
        eventId
      );

      await test.step("Enable invitation setting from email people section.", async () => {
        await communicationSettings.toogleCommunicationSettings(
          EventRole.ATTENDEE,
          EventSettingID.EventInvitationFromEmailFromPeopleSection,
          true
        );

        await communicationSettings.toogleCommunicationSettings(
          EventRole.SPEAKER,
          EventSettingID.EventInvitationFromEmailFromPeopleSection,
          true
        );
      });
    });

    test("TC006: Toogle access control settings. @accessControlSettings", async () => {
      await test.step("Toggle Attendee Access control settings.", async () => {
        let attendeeGroupId = await accessGroupController.fetchAccessGroupId(
          Roles.ATTENDEES
        );

        const stageName = "AccessControlStage";

        await test.step("Create random stages", async () => {
          await stageController.addNewStageToEvent({
            name: stageName,
            size: StageSize.MEDIUM,
          });
        });

        let accessControl = new AccessControlController(
          orgApiContext,
          eventId,
          attendeeGroupId
        );

        await test.step("Toggle day access", async () => {
          await accessControl.toggleDayAccess(2, false);
        });

        await test.step("Toggle stage access", async () => {
          await accessControl.toggleStageAccess(stageName, false);
        });

        await test.step("Toggle rooms access", async () => {
          await accessControl.toggleRoomAccess("ACCESS_PVT_ROOMS", false);
        });
      });
    });

    test("TC007: Create duplicate event | Access Group - ON", async () => {
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-880",
      });

      let duplicateEventTitle;
      let duplicateEventId;

      await test.step("Create duplicate of event", async () => {
        let eventInfo = await eventController.getEventInfo();
        let eventInfoResp = await eventInfo.json();
        console.log("eventInfoResp :", eventInfoResp);

        duplicateEventTitle = eventTitle + " - Duplicate Automation";
        duplicateEventId = await eventController.duplicateEvent({
          title: duplicateEventTitle,
          deltaHours: deltaHours,
          deltaDays: deltaDays,
          isEventSetup: false,
          isVenueSetup: false,
          isAccessGroups: true,
          isCommunication: false,
          isRegistration: false,
        });

        console.log("Duplicate event id: ", duplicateEventId);

        let duplicateEventController = new EventController(
          orgApiContext,
          duplicateEventId
        );

        let duplicateEventInfo = await duplicateEventController.getEventInfo();
        let duplicateEventInfoResp = await duplicateEventInfo.json();
        console.log("duplicateEventInfoResp :", duplicateEventInfoResp);
      });

      let duplicateValidation = new DuplicateValidation(
        orgApiContext,
        eventId,
        duplicateEventId,
        eventTitle,
        duplicateEventTitle,
        deltaHours,
        deltaDays
      );

      await test.step("Initialize controllers", async () => {
        await duplicateValidation.initializeControllers();
      });

      await test.step("Validate duplicate event default stage.", async () => {
        await duplicateValidation.verifyDuplicateEventDefaultStageName(false);
      });

      await test.step("Validate Basic event details", async () => {
        await duplicateValidation.validateEventDetails();
      });

      await test.step("Validate Room details", async () => {
        await duplicateValidation.duplicatedEventRoomValidation(false);
      });

      await test.step("Validate Expo details", async () => {
        await duplicateValidation.duplicatedEventExpoValidation(false);
      });

      await test.step("Validate Stage details", async () => {
        await duplicateValidation.duplicatedEventStageValidation(false);
      });

      await test.step("Validate Access Groups", async () => {
        await duplicateValidation.verifyAccessGroupDetails(
          customAccessGroupName,
          true
        );
      });

      await test.step("Registration Page verification", async () => {
        await duplicateValidation.verifyRegistrationPageDetails(false);
      });
    });

    test("TC008: Create duplicate event | Access Group - OFF", async () => {
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-881",
      });
      let duplicateEventTitle;
      let duplicateEventId;

      await test.step("Create duplicate of event", async () => {
        let eventInfo = await eventController.getEventInfo();
        let eventInfoResp = await eventInfo.json();
        console.log("eventInfoResp :", eventInfoResp);

        duplicateEventTitle = eventTitle + " - Duplicate Automation";
        duplicateEventId = await eventController.duplicateEvent({
          title: duplicateEventTitle,
          deltaHours: deltaHours,
          deltaDays: deltaDays,
          isEventSetup: false,
          isVenueSetup: false,
          isAccessGroups: false,
          isCommunication: false,
          isRegistration: false,
        });

        console.log("Duplicate event id: ", duplicateEventId);

        let duplicateEventController = new EventController(
          orgApiContext,
          duplicateEventId
        );

        let duplicateEventInfo = await duplicateEventController.getEventInfo();
        let duplicateEventInfoResp = await duplicateEventInfo.json();
        console.log("duplicateEventInfoResp :", duplicateEventInfoResp);
      });

      let duplicateValidation = new DuplicateValidation(
        orgApiContext,
        eventId,
        duplicateEventId,
        eventTitle,
        duplicateEventTitle,
        deltaHours,
        deltaDays
      );

      await test.step("Initialize controllers", async () => {
        await duplicateValidation.initializeControllers();
      });

      await test.step("Validate duplicate event default stage.", async () => {
        await duplicateValidation.verifyDuplicateEventDefaultStageName(false);
      });

      await test.step("Validate Basic event details", async () => {
        await duplicateValidation.validateEventDetails();
      });

      await test.step("Validate Room details", async () => {
        await duplicateValidation.duplicatedEventRoomValidation(false);
      });

      await test.step("Validate Expo details", async () => {
        await duplicateValidation.duplicatedEventExpoValidation(false);
      });

      await test.step("Validate Stage details", async () => {
        await duplicateValidation.duplicatedEventStageValidation(false);
      });

      await test.step("Validate Access Groups", async () => {
        await duplicateValidation.verifyAccessGroupDetails(
          customAccessGroupName,
          false
        );
      });

      await test.step("Registration Page verification", async () => {
        await duplicateValidation.verifyRegistrationPageDetails(false);
      });
    });

    test("TC009: Create duplicate event | Venue Setup - ON", async () => {
      let duplicateEventTitle;
      let duplicateEventId;

      await test.step("Create duplicate of event", async () => {
        let eventInfo = await eventController.getEventInfo();
        let eventInfoResp = await eventInfo.json();
        console.log("eventInfoResp :", eventInfoResp);

        duplicateEventTitle = eventTitle + " - Duplicate Automation";
        duplicateEventId = await eventController.duplicateEvent({
          title: duplicateEventTitle,
          deltaHours: deltaHours,
          deltaDays: deltaDays,
          isEventSetup: false,
          isVenueSetup: true,
          isAccessGroups: false,
          isCommunication: false,
          isRegistration: false,
        });

        console.log("Duplicate event id: ", duplicateEventId);

        let duplicateEventController = new EventController(
          orgApiContext,
          duplicateEventId
        );

        let duplicateEventInfo = await duplicateEventController.getEventInfo();
        let duplicateEventInfoResp = await duplicateEventInfo.json();
        console.log("duplicateEventInfoResp :", duplicateEventInfoResp);
      });

      let duplicateValidation = new DuplicateValidation(
        orgApiContext,
        eventId,
        duplicateEventId,
        eventTitle,
        duplicateEventTitle,
        deltaHours,
        deltaDays
      );

      await test.step("Initialize controllers", async () => {
        await duplicateValidation.initializeControllers();
      });

      await test.step("Validate duplicate event default stage.", async () => {
        await duplicateValidation.verifyDuplicateEventDefaultStageName(true);
      });

      await test.step("Validate Basic event details", async () => {
        await duplicateValidation.validateEventDetails();
      });

      await test.step("Validate Room details", async () => {
        await duplicateValidation.duplicatedEventRoomValidation(true);
      });

      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-911",
      });
      await test.step("Validate Venue settings", async () => {
        await duplicateValidation.validateVenueSettings(true);
      });

      await test.step("Validate Expo details", async () => {
        await duplicateValidation.duplicatedEventExpoValidation(true);
      });

      await test.step("Validate Stage details", async () => {
        await duplicateValidation.duplicatedEventStageValidation(true);
      });

      await test.step("Validate Access Groups", async () => {
        await duplicateValidation.verifyAccessGroupDetails(
          customAccessGroupName,
          false
        );
      });

      await test.step("Registration Page verification", async () => {
        await duplicateValidation.verifyRegistrationPageDetails(false);
      });
    });

    test("TC010: Create duplicate event | Venue Setup - OFF", async () => {
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-882",
      });
      let duplicateEventTitle;
      let duplicateEventId;

      await test.step("Create duplicate of event", async () => {
        let eventInfo = await eventController.getEventInfo();
        let eventInfoResp = await eventInfo.json();
        console.log("eventInfoResp :", eventInfoResp);

        duplicateEventTitle = eventTitle + " - Duplicate Automation";
        duplicateEventId = await eventController.duplicateEvent({
          title: duplicateEventTitle,
          deltaHours: deltaHours,
          deltaDays: deltaDays,
          isEventSetup: false,
          isVenueSetup: false,
          isAccessGroups: false,
          isCommunication: false,
          isRegistration: false,
        });

        console.log("Duplicate event id: ", duplicateEventId);

        let duplicateEventController = new EventController(
          orgApiContext,
          duplicateEventId
        );

        let duplicateEventInfo = await duplicateEventController.getEventInfo();
        let duplicateEventInfoResp = await duplicateEventInfo.json();
        console.log("duplicateEventInfoResp :", duplicateEventInfoResp);
      });

      let duplicateValidation = new DuplicateValidation(
        orgApiContext,
        eventId,
        duplicateEventId,
        eventTitle,
        duplicateEventTitle,
        deltaHours,
        deltaDays
      );

      await test.step("Initialize controllers", async () => {
        await duplicateValidation.initializeControllers();
      });

      await test.step("Validate duplicate event default stage.", async () => {
        await duplicateValidation.verifyDuplicateEventDefaultStageName(false);
      });

      await test.step("Validate Basic event details", async () => {
        await duplicateValidation.validateEventDetails();
      });

      await test.step("Validate Room details", async () => {
        await duplicateValidation.duplicatedEventRoomValidation(false);
      });

      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-911",
      });
      await test.step("Validate Venue settings", async () => {
        await duplicateValidation.validateVenueSettings(false);
      });

      await test.step("Validate Expo details", async () => {
        await duplicateValidation.duplicatedEventExpoValidation(false);
      });

      await test.step("Validate Stage details", async () => {
        await duplicateValidation.duplicatedEventStageValidation(false);
      });

      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-885",
      });
      test.info().annotations.push({
        type: "TC",
        description: "https://linear.app/zuddl/issue/QAT-88",
      });
      await test.step("Validate Access Groups", async () => {
        await duplicateValidation.verifyAccessGroupDetails(
          customAccessGroupName,
          false
        );
      });

      await test.step("Registration Page verification", async () => {
        await duplicateValidation.verifyRegistrationPageDetails(false);
      });
    });
  }
);
