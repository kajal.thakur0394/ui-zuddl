import test, { APIRequestContext, BrowserContext } from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { DataUtl } from "../../../util/dataUtil";
import { RoomsController } from "../../../controller/RoomsController";
import StageSize from "../../../enums/StageSizeEnum";
import { StageController } from "../../../controller/StageController";
import { ExpoController } from "../../../controller/ExpoController";
import sessionDataPayload from "../../../test-data/sessionDataPayload.json";
import { inspect } from "util";

export type Room = {
  roomName: string;
  roomId: string;
  capacity: number;
};

export type Tag = {
  tagId: string;
  tagName: string;
};

export type Stage = {
  stageId: string;
  stageName: string;
  stageSize?: string;
};

export type Booth = {
  boothId: string;
  boothName: string;
  zoneId?: string;
  zoneName?: string;
};

export type Speaker = {
  speakerId: string;
  firstName: string;
  lastName: string;
  speakerEmail: string;
};

type venueType = "STAGE" | "EXPO" | "ROOMS";

export default class VenueSection {
  protected orgApiRequestContext: APIRequestContext;
  protected eventId: any;
  protected eventController: EventController;
  protected roomsController: RoomsController;
  protected stageController: StageController;
  protected expoController: ExpoController;

  constructor(orgApiReuqestContext: APIRequestContext, eventId: string) {
    this.orgApiRequestContext = orgApiReuqestContext;
    this.eventId = eventId;

    this.eventController = new EventController(
      this.orgApiRequestContext,
      this.eventId
    );

    this.roomsController = new RoomsController(
      this.orgApiRequestContext,
      this.eventId
    );

    this.expoController = new ExpoController(
      this.orgApiRequestContext,
      this.eventId
    );
  }

  async createRandomRooms(seats: number = 0, roomCount: number = 0) {
    if (roomCount == 0) {
      roomCount = DataUtl.randomIntFromInterval(5, 9);
    }
    if (seats == 0) {
      seats = DataUtl.randomIntFromInterval(2, 15);
    }

    await this.roomsController.addRoomsToEvent({
      seatsPerRoom: seats,
      numberOfRooms: roomCount,
      roomCategory: "PUBLIC",
    });
  }

  async getAllRooms() {
    let rooms: Room[] = [];
    let roomsList = await (await this.roomsController.getListOfRooms()).json();

    for (let room of roomsList) {
      console.log(`Room - ${JSON.stringify(room)}`);
      rooms.push({
        roomName: room.name,
        roomId: room.discussionTableId,
        capacity: room.capacity,
      });
    }

    // console.log(`Rooms - ${JSON.stringify(rooms)}`);

    return rooms;
  }

  async createTags(
    tageNamePrefix: string = "RandomTag",
    tagCount: number = DataUtl.randomIntFromInterval(1, 5)
  ) {
    let tags: Tag[] = [];

    for (let i = 0; i < tagCount; i++) {
      let tagName = tageNamePrefix + "_" + i;
      let tagId = await this.eventController.createTag(tagName);

      tags.push({
        tagId: tagId,
        tagName: tagName,
      });
    }

    console.log(`Tags - ${JSON.stringify(tags)}`);
    return tags;
  }

  async addTagsToRoom(roomId: string, tags: Tag[]) {
    let tagsToBeAdded: Tag[] = [];
    let tagCount = DataUtl.randomIntFromInterval(1, tags.length);
    tagsToBeAdded = tags.slice(0, tagCount);
    console.log(`Tags to be added - ${JSON.stringify(tagsToBeAdded)}`);

    await this.roomsController.addTagsToRoom(roomId, tagsToBeAdded);
  }

  async randomiseRoomDetails(roomId: string) {
    await this.roomsController.randomizeRoomDetails(roomId);
    let newRoomDetails = await this.roomsController.getRoomDetails(roomId);
    let randomisedRoomDetails: Room = {
      roomName: newRoomDetails.name,
      roomId: newRoomDetails.discussionTableId,
      capacity: newRoomDetails.capacity,
    };
    return randomisedRoomDetails;
  }

  async createRandomStages(stageCount: number = 0) {
    let stageController = new StageController(
      this.orgApiRequestContext,
      this.eventId
    );
    if (stageCount == 0) {
      stageCount = DataUtl.randomIntFromInterval(1, 5);
    }
    let stages: Stage[] = [];

    for (let i = 0; i < stageCount; i++) {
      const stageName = `Stage ${i + 1}`;

      let stageId = await stageController.addNewStageToEvent({
        name: stageName,
        size: [StageSize.SMALL, StageSize.MEDIUM, StageSize.LARGE][
          Math.floor(Math.random() * 3)
        ],
      });

      console.log(`Stage - ${stageName} - ${stageId}`);
      stages.push({
        stageId: stageId,
        stageName: stageName,
      });
    }

    const response = await stageController.getAllStages();
    console.log(
      "All stages in this event are ...",
      inspect(response, { depth: null })
    );
    return stages;
  }

  async getAllStages() {
    let stages: Stage[] = [];
    let stagesList = await this.eventController.getAllStages();

    for (let stage of stagesList) {
      console.log(`Stage - ${JSON.stringify(stage)}`);
      stages.push({
        stageId: stage.stageId,
        stageName: stage.name,
        stageSize: stage.size,
      });
    }

    console.log(`Stages - ${JSON.stringify(stages)}`);

    return stages;
  }

  async randomiseStageDetails(stageId: string) {
    const stageController = new StageController(
      this.orgApiRequestContext,
      this.eventId,
      stageId
    );
    let randomStage: Stage;

    randomStage = await stageController.updateRandomiseStageSettings();
    return randomStage;
  }

  async setupExpo(
    zoneCount: number = 0,
    boothsPerZone: number = 0,
    boothsWithoutZoneCount: number = 0
  ) {
    if (zoneCount == 0) {
      zoneCount = DataUtl.randomIntFromInterval(5, 10);
    }
    if (boothsPerZone == 0) {
      boothsPerZone = DataUtl.randomIntFromInterval(1, 5);
    }
    if (boothsWithoutZoneCount == 0) {
      boothsWithoutZoneCount = DataUtl.randomIntFromInterval(1, 5);
    }

    let booths: Booth[] = [];

    await test.step("Create random expo with multiple booths and sizes", async () => {
      for (let i = 0; i < zoneCount; i++) {
        console.log("Creating zone");
        let zoneResponseJson = await this.expoController.createZone();
        console.log("Zone response: ", zoneResponseJson);

        for (let j = 0; j < boothsPerZone; j++) {
          console.log("Creating booth in zone");
          let boothResponseJson =
            await this.expoController.createSingleBoothWithinZone(
              zoneResponseJson.boothZoneId
            );
          console.log("Booth response: ", boothResponseJson);
          const [boothName, boothId] = boothResponseJson.entries().next().value;

          booths.push({
            boothId: boothId,
            boothName: boothName,
            zoneId: zoneResponseJson.boothZoneId,
            zoneName: zoneResponseJson.title,
          });
        }
      }
    });

    console.log(`Booths - ${JSON.stringify(booths)}`);
    return booths;
  }

  async getAllBooths() {
    const boothsJson = await this.expoController.getAllBooths();
    let booths: Booth[] = [];
    for (let booth of boothsJson) {
      booths.push({
        boothId: booth.boothId,
        boothName: booth.name,
      });
    }

    return booths;
  }

  async deleteBooth(boothId: string) {
    await this.expoController.deleteBooth(boothId);
  }

  async addWidgetsToBooths(booths: Booth[]) {
    for (let booth of booths) {
      let layoutId = await this.expoController.getLayoutId(booth.boothId);
      console.log(`Layout id - ${layoutId}`);
      console.log(`Booth id - ${booth.boothId}`);
      await this.expoController.addTextWidget(layoutId.layoutId);
    }
  }

  async createSegments(venue: venueType, tags: Tag[]) {
    // case stage
    // case expo
    // case room
    const eventInfoResp = await this.eventController.getEventInfo();
    const eventInfo = await eventInfoResp.json();
    console.log("Event info: ", eventInfo);

    switch (venue) {
      case "STAGE": {
        const stages = await this.getAllStages();
        const randomStage =
          stages[DataUtl.randomIntFromInterval(0, stages.length - 1)];
        const [sessionId, sessionData] = await this.createSegment(
          venue,
          randomStage.stageId,
          tags
        );
        return [sessionId, sessionData];
      }

      case "ROOMS": {
        const rooms = await this.getAllRooms();
        const randomRoom =
          rooms[DataUtl.randomIntFromInterval(0, rooms.length - 1)];
        const [sessionId, sessionData] = await this.createSegment(
          venue,
          randomRoom.roomId,
          tags
        );
        return [sessionId, sessionData];
      }

      case "EXPO": {
        const booths = await this.getAllBooths();
        const randomBooth =
          booths[DataUtl.randomIntFromInterval(0, booths.length - 1)];
        const [sessionId, sessionData] = await this.createSegment(
          venue,
          randomBooth.boothId,
          tags
        );
        return [sessionId, sessionData];
      }
    }
  }

  async createSegment(
    venue: string,
    refId: string,
    tags: Tag[],
    speakers: Speaker[] = [],
    customSessionName: string = ""
  ) {
    const randomIndex = Math.floor(
      Math.random() * Object.keys(sessionDataPayload).length
    );

    const sessionData = sessionDataPayload[randomIndex];

    const tagIds = tags.map((tag) => tag.tagId);

    const sessionId = await this.eventController.createSession(
      randomIndex,
      refId,
      0,
      0,
      3,
      5,
      0,
      0,
      venue,
      speakers,
      tagIds,
      customSessionName
    );

    const recievedSessionData = await this.eventController.getRequiredInfo(
      this.eventId,
      sessionId
    );
    sessionData["start_time"] = recievedSessionData["startDateTime"];
    sessionData["end_time"] = recievedSessionData["endDateTime"];
    sessionData["title"] = recievedSessionData["title"];
    sessionData["description"] = recievedSessionData["description"];
    console.log(sessionData);

    return [sessionId, sessionData];
  }
}
