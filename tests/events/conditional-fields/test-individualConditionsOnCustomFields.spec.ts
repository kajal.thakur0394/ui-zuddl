import test, {
  APIRequestContext,
  BrowserContext,
  Page,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import EventEntryType from "../../../enums/eventEntryEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import {
  createNewEvent,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import EventRole from "../../../enums/eventRoleEnum";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import conditionalFieldsTestData from "../../../test-data/conditional-fields-testdata/createConditionalFields.json";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";

test.describe
  .parallel("@editprofile @defaultfields @basicdetails", async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventTitle: string;
  let eventId: any;
  let orgPage: Page;
  let userInfoDTO: UserInfoDTO;
  let eventInfoDto: EventInfoDTO;
  let userEventRoleDto: UserEventRoleDto;
  let page: Page;

  test.beforeEach(async ({ context }) => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});

    orgPage = await orgBrowserContext.newPage();
    orgApiContext = await orgBrowserContext.request;

    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: eventTitle,
    });
    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });
    page = await context.newPage();
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  /*
  In this test:
  - An event with text based fields
  - Is answered condition is checked and then the current field is answered.
  */
  test("TC001: Evaluating text based condition for for is answered condition", async () => {
    let eventController: EventController = new EventController(
      orgApiContext,
      eventId
    );
    let ConditionalFieldsTestData =
      conditionalFieldsTestData["text-conditional-fields-data"];
    await eventController.addCustomFieldsWithConditionToEvent(
      ConditionalFieldsTestData
    );
    let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData.TextconditionalFieldFormDataIsAnswered,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
      }
    );

    await test.step("verify succesfully registered message visible.", async () => {
      await (
        await landingPageOne.getRegistrationConfirmationScreen()
      ).isRegistrationConfirmationMessageVisible(true);
    });
  });

  /*
  In this test:
  - An event with text based fields
  - Is not answered condition is checked and then the current field is answered.
  */
  test("TC002: Evaluating text based condition for is not answered condition", async () => {
    let eventController: EventController = new EventController(
      orgApiContext,
      eventId
    );
    let ConditionalFieldsTestData =
      conditionalFieldsTestData["text-conditional-fields-data"];
    await eventController.addCustomFieldsWithConditionToEvent(
      ConditionalFieldsTestData
    );
    let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData.TextconditionalFieldFormDataIsNotAnswered,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
      }
    );
    await test.step("verify succesfully registered message visible.", async () => {
      await (
        await landingPageOne.getRegistrationConfirmationScreen()
      ).isRegistrationConfirmationMessageVisible(true);
    });
  });

  /*
  In this test:
  - An event with numeric based fields
  - Is answered condition is checked and then the current field is answered.
  */
  test("TC003: Evaluating numeric based condition for is answered.", async () => {
    let eventController: EventController = new EventController(
      orgApiContext,
      eventId
    );
    let numericBasedConditionalFieldsTestData =
      conditionalFieldsTestData["numeric-conditional-fields-data"];
    await eventController.addCustomFieldsWithConditionToEvent(
      numericBasedConditionalFieldsTestData
    );
    let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData.NumericconditionalFieldFormDataIsAnswered,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
      }
    );
    await test.step("verify succesfully registered message visible.", async () => {
      await (
        await landingPageOne.getRegistrationConfirmationScreen()
      ).isRegistrationConfirmationMessageVisible(true);
    });
  });

  /*
  In this test:
  - An event with numeric based fields
  - Is not answered condition is checked and then the current field is answered.
  */
  test("TC004: Evaluating numeric based condition for is not answered.", async () => {
    let eventController: EventController = new EventController(
      orgApiContext,
      eventId
    );
    let numericBasedConditionalFieldsTestData =
      conditionalFieldsTestData["numeric-conditional-fields-data"];
    await eventController.addCustomFieldsWithConditionToEvent(
      numericBasedConditionalFieldsTestData
    );
    let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData.NumericconditionalFieldFormDataIsNotAnswered,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
      }
    );
    await test.step("verify succesfully registered message visible.", async () => {
      await (
        await landingPageOne.getRegistrationConfirmationScreen()
      ).isRegistrationConfirmationMessageVisible(true);
    });
  });

  /*
  In this test:
  - An event with dropdown based fields
  - Is selected condition is checked and then the current field is answered.
  */
  test("TC005: Evaluating dropdown based condition for is selected", async () => {
    let eventController: EventController = new EventController(
      orgApiContext,
      eventId
    );
    let ConditionalFieldsTestData =
      conditionalFieldsTestData["dropdown-conditional-fields-data"];
    await eventController.addCustomFieldsWithConditionToEvent(
      ConditionalFieldsTestData
    );
    let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData.DropdownconditionalFieldFormDataIsselected,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
      }
    );
    await test.step("verify succesfully registered message visible.", async () => {
      await (
        await landingPageOne.getRegistrationConfirmationScreen()
      ).isRegistrationConfirmationMessageVisible(true);
    });
  });

  /*
  In this test:
  - An event with dropdown based fields
  - Is not selected condition is checked and then the current field is answered.
  */
  test("TC006: Evaluating dropdown based condition for is not selected", async () => {
    let eventController: EventController = new EventController(
      orgApiContext,
      eventId
    );
    let ConditionalFieldsTestData =
      conditionalFieldsTestData["dropdown-conditional-fields-data"];
    await eventController.addCustomFieldsWithConditionToEvent(
      ConditionalFieldsTestData
    );
    let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData.DropDownconditionalFieldFormDataIsNotSelected,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
      }
    );
    await test.step("verify succesfully registered message visible.", async () => {
      await (
        await landingPageOne.getRegistrationConfirmationScreen()
      ).isRegistrationConfirmationMessageVisible(true);
    });
  });

  /*
  In this test:
  - An event with multi-select dropdown based fields
  - Is All selected condition is checked and then the current field is answered.
  */
  test("TC007: Evaluating multi-select dropdown based condition for All selected", async () => {
    let eventController: EventController = new EventController(
      orgApiContext,
      eventId
    );
    let ConditionalFieldsTestData =
      conditionalFieldsTestData["multi-select-conditional-fields-data"];
    await eventController.addCustomFieldsWithConditionToEvent(
      ConditionalFieldsTestData
    );
    let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData
        .MultiSelectconditionalFieldFormDataAllselected,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
      }
    );
    await test.step("verify succesfully registered message visible.", async () => {
      await (
        await landingPageOne.getRegistrationConfirmationScreen()
      ).isRegistrationConfirmationMessageVisible(true);
    });
  });

  /*
  In this test:
  - An event with multi-select dropdown based fields
  - Is Any selected condition is checked and then the current field is answered.
  */
  test("TC008: Evaluating multi-select dropdown based condition for Any selected", async () => {
    let eventController: EventController = new EventController(
      orgApiContext,
      eventId
    );
    let ConditionalFieldsTestData =
      conditionalFieldsTestData["multi-select-conditional-fields-data"];
    await eventController.addCustomFieldsWithConditionToEvent(
      ConditionalFieldsTestData
    );
    let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData
        .MultiSelectconditionalFieldFormDataAnySelected,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
      }
    );
    await test.step("verify succesfully registered message visible.", async () => {
      await (
        await landingPageOne.getRegistrationConfirmationScreen()
      ).isRegistrationConfirmationMessageVisible(true);
    });
  });

  /*
  In this test:
  - An event with multi-select dropdown based fields and text fields
  - Is Any selected condition is checked and then one text 
      based field is answered then the option will appear else.
  */
  test("TC009: Conditional field with nested condition depending on multi-select and text any of the and removing multi-select condition.", async () => {
    let eventController: EventController = new EventController(
      orgApiContext,
      eventId
    );
    let ConditionalFieldsTestData =
      conditionalFieldsTestData["nested-conditional-fields-anyofthe"];
    await eventController.addCustomFieldsWithConditionToEvent(
      ConditionalFieldsTestData
    );
    let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData
        .nestedconditionalFieldFormDataRemmovingmultselectcondition,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
        fieldsToBeHidden: ["Do you watch American leagues?"],
      }
    );
    await test.step("verify succesfully registered message visible.", async () => {
      await (
        await landingPageOne.getRegistrationConfirmationScreen()
      ).isRegistrationConfirmationMessageVisible(true);
    });
  });

  test("TC010: Conditional field with nested condition depending on multi-select and text any of the and removing text condition.", async () => {
    let eventController: EventController = new EventController(
      orgApiContext,
      eventId
    );
    let ConditionalFieldsTestData =
      conditionalFieldsTestData["nested-conditional-fields-anyofthe"];
    await eventController.addCustomFieldsWithConditionToEvent(
      ConditionalFieldsTestData
    );
    let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData
        .nestedconditionalFieldFormDataRemmovingTextCondition,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
        fieldsToBeHidden: ["Do you watch American leagues?"],
      }
    );
    await test.step("verify succesfully registered message visible.", async () => {
      await (
        await landingPageOne.getRegistrationConfirmationScreen()
      ).isRegistrationConfirmationMessageVisible(true);
    });
  });

  test("TC011: Conditional field with nested condition depending on multi-select and text all of the and removing multi-select condition.", async () => {
    let eventController: EventController = new EventController(
      orgApiContext,
      eventId
    );
    let ConditionalFieldsTestData =
      conditionalFieldsTestData["nested-conditional-fields-allofthe"];
    await eventController.addCustomFieldsWithConditionToEvent(
      ConditionalFieldsTestData
    );
    let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData
        .nestedconditionalFieldFormDataRemmovingmultselectcondition,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
        fieldsToBeHidden: ["Do you watch American leagues?"],
      }
    );
    await test.step("verify succesfully registered message visible.", async () => {
      await (
        await landingPageOne.getRegistrationConfirmationScreen()
      ).isRegistrationConfirmationMessageVisible(true);
    });
  });

  test("TC012: Conditional field with nested condition depending on multi-select and text all of the and removing text condition", async () => {
    let eventController: EventController = new EventController(
      orgApiContext,
      eventId
    );
    let ConditionalFieldsTestData =
      conditionalFieldsTestData["nested-conditional-fields-allofthe"];
    await eventController.addCustomFieldsWithConditionToEvent(
      ConditionalFieldsTestData
    );
    let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData
        .nestedconditionalFieldFormDataRemmovingTextCondition,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
        fieldsToBeHidden: ["Do you watch American leagues?"],
      }
    );
    await test.step("verify succesfully registered message visible.", async () => {
      await (
        await landingPageOne.getRegistrationConfirmationScreen()
      ).isRegistrationConfirmationMessageVisible(true);
    });
  });
});
