import {
  test,
  BrowserContext,
  APIRequestContext,
  expect,
  Page,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import {
  getEventDetails,
  createNewEvent,
  inviteAttendeeByAPI,
  enableMagicLinkInEvent,
  updateEventDateTime,
  updateLandingPageType,
} from "../../../util/apiUtil";
import { DataUtl } from "../../../util/dataUtil";
import { LandingPageTwo } from "../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventRole from "../../../enums/eventRoleEnum";
import EventEntryType from "../../../enums/eventEntryEnum";
import { fetchAttendeeInviteMagicLink } from "../../../util/emailUtil";
import conditionalFieldsTestData from "../../../test-data/conditional-fields-testdata/createConditionalFields.json";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";
import {
  validateEditProfileConditionalFieldDataFromDB,
  validateEditProfileConditionalFieldDataFromEventRoleApi,
} from "../../../util/validation-util";
import { EventController } from "../../../controller/EventController";
import { EditProfileData } from "../../../test-data/editProfileForm";
import { EditProfilePage } from "../../../page-objects/events-pages/live-side-components/EditProfilePage";
import LandingPageType from "../../../enums/landingPageEnum";
import { LobbyPage } from "../../../page-objects/events-pages/zones/LobbyPage";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe
  .parallel("Events with conditional fields | Landing Page 2", () => {
  test.describe.configure({ retries: 2 });

  let org_browser_context: BrowserContext;
  let organiserApiContext: APIRequestContext;
  let eventTitle: string;
  let eventId;
  let csvFileToAddDataIn: string; //filename
  let eventInfoDto: EventInfoDTO;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let landingPageId;
  let userInfoDTO: UserInfoDTO;
  let userEventRoleDto: UserEventRoleDto;
  let page: Page;

  test.beforeEach(async ({ context }) => {
    org_browser_context = await BrowserFactory.getBrowserContext({});
    organiserApiContext = await org_browser_context.request;
    eventTitle = "Conditional-events";
    eventId = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title: eventTitle,
    });
    csvFileToAddDataIn = DataUtl.getRandomCsvName();
    // adding event dto
    eventInfoDto = new EventInfoDTO(eventId, eventTitle, false); // true to denote magic link enabled
    eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, eventId)
    ).json();
    console.log("event_details_api_resp :", event_details_api_resp);
    landingPageId = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landingPageId;
    await enableMagicLinkInEvent(
      organiserApiContext,
      eventId,
      landingPageId,
      EventEntryType.REG_BASED
    );
    // Send email on registration:
    await enableDisableEmailTrigger(
      organiserApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      false,
      EventRole.ATTENDEE,
      true
    );
    // updating event custom  fields
    let eventController: EventController = new EventController(
      organiserApiContext,
      eventId
    );
    let customConditionalFieldsTestData =
      conditionalFieldsTestData["endToend-custom-conditional-fields-data"];
    let discalimerFieldsTestData =
      conditionalFieldsTestData["endToend-custom-discalimer-fields-data"];
    await eventController.addCustomFieldsWithConditionToEvent(
      customConditionalFieldsTestData
    );
    await eventController.addDiscalimerFieldsToEvent(discalimerFieldsTestData);
    // Updating event landing page type
    await updateLandingPageType(
      organiserApiContext,
      eventId,
      landingPageId,
      LandingPageType.STYLE2,
      "true",
      EventEntryType.REG_BASED
    );

    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    page = await context.newPage();
  });

  test.afterEach(async () => {
    await org_browser_context?.close();
  });

  /*
        Haapy path test, user is successfully able to register after filling all mandatory conditional fields.
        */
  test("TC001: Attendee is able to register in Reg Based Event by filling all conditional fields @landing-page-2", async () => {
    /*
            1. All reg field filled.
            2. If A selected only A dependent field should be visible not the other option.
            3. If not selected logic is working.
            4. Any of selcted and all of selected.
            5. Add desclaimer button is working.
             */
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    let landingPageTwo = new LandingPageTwo(page);
    await landingPageTwo.load(attendee_landing_page);
    // update the default reg fields to conditional feilds
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData.userConditionalFieldFormData1,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
        clickOnDesclaimerButton: true,
      }
    );
    await test.step("Verify if attendee is successfully registered", async () => {
      await (
        await landingPageTwo.getRegistrationConfirmationScreen()
      ).isRegistrationConfirmationMessageVisible(true);
    });
    await test.step("Fetch Registered user custom fields data from the DB", async () => {
      const fetchedCustomFieldsDataFromDb =
        await QueryUtil.fetchCustomFieldsFilledByRegisteredUser(
          eventId,
          attendeeEmail
        );
      console.log(
        `registered user fetched custom fields data is ${fetchedCustomFieldsDataFromDb}`
      );
      expect(
        await validateEditProfileConditionalFieldDataFromDB(
          fetchedCustomFieldsDataFromDb,
          userInfoDTO.userRegFormData.userConditionalFieldFormData1,
          {
            checkOptionalFields: true,
            checkMandatoryFields: true,
            fieldsToSkip: ["First Name", "Last Name", "Email"],
          }
        )
      ).toBe(true);
    });
  });

  /*
       If conditional mandatory field is not filled then it should throw an error.
       */
  test("TC002: Attendee is not able to register in Reg Based Event by skipping mandatory conditional field @landing-page-2", async () => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    let landingPageTwo = new LandingPageTwo(page);
    await landingPageTwo.load(attendee_landing_page);
    // update the default reg fields to conditional feilds
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData._conditionalFieldFormData2,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
        clickOnDesclaimerButton: true,
      },
      false
    );
    await test.step("verify error message displayes asking user to fill mandatory field", async () => {
      await (
        await landingPageTwo.getRegistrationFormComponent()
      ).verifyThisFieldIsRequiredErrorMessageIsDisplayed();
    });
  });

  /*
        Validating conditional fields data using edit profile info.
        */

  // Adding skip as conditional fields are not working with magic link
  test.skip("TC003: Attendee is  invited and also registers and verifying details via edit profile api @landing-page-2", async () => {
    /*
            1. All reg field filled.
            2. If A selected only A dependent field should be visible not the other option.
            3. If not selected logic is working.
            4. Any of selcted and all of selected.
            5. Add desclaimer button is working.
             */
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    // Update event start time
    await updateEventDateTime(
      organiserApiContext,
      eventId,
      landingPageId,
      new Date().toISOString()
    );
    await inviteAttendeeByAPI(organiserApiContext, eventId, attendeeEmail);
    let magicLink = await fetchAttendeeInviteMagicLink(
      attendeeEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    let landingPageTwo = new LandingPageTwo(page);
    const eventRoleApiPromise = page.waitForResponse(
      (response) =>
        response.url().includes(`/event/${eventId}/role`) &&
        response.status() === 200
    );

    await landingPageTwo.load(magicLink);
    const getEventRoleResponse = await eventRoleApiPromise;
    expect(
      getEventRoleResponse.status(),
      "expecting event role api respoinse to be 200"
    ).toBe(200);
    // update the default reg fields to conditional feilds
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData.userConditionalFieldFormData1,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
        clickOnDesclaimerButton: true,
        clickOnContinueButton: true,
        // Select covid vaccine doses is still not working change it after the bug is fixed.
        fieldsToSkip: ["First Name", "Last Name", "Email"],
      }
    );
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isLobbyLoaded();
    let res = await page.request.get(`/api/event/${eventId}/role`);
    console.log("Res Json", await res.json());
    validateEditProfileConditionalFieldDataFromEventRoleApi(
      await res.json(),
      userInfoDTO.userRegFormData.userConditionalFieldFormData1,
      {
        checkOptionalFields: true,
        checkMandatoryFields: true,
        fieldsToSkip: ["First Name", "Last Name", "Email"],
      }
    );
  });

  // Adding skip as conditional fields are not working with magic link
  test.skip("TC004: Attendee changes feilds in edit profile and verifying details via edit profile api @landing-page-2", async () => {
    /*
            1. All reg field filled.
            2. If A selected only A dependent field should be visible not the other option.
            3. If not selected logic is working.
            4. Any of selcted and all of selected.
            5. Add desclaimer button is working.
             */
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    // Update event start time
    await updateEventDateTime(
      organiserApiContext,
      eventId,
      landingPageId,
      new Date().toISOString()
    );
    await inviteAttendeeByAPI(organiserApiContext, eventId, attendeeEmail);
    let magicLink = await fetchAttendeeInviteMagicLink(
      attendeeEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    let landingPageTwo = new LandingPageTwo(page);
    const eventRoleApiPromise = page.waitForResponse(
      (response) =>
        response.url().includes(`/event/${eventId}/role`) &&
        response.status() === 200
    );

    await landingPageTwo.load(magicLink);
    const getEventRoleResponse = await eventRoleApiPromise;
    expect(
      getEventRoleResponse.status(),
      "expecting event role api respoinse to be 200"
    ).toBe(200);
    // update the default reg fields to conditional feilds
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData.userConditionalFieldFormData1,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
        clickOnDesclaimerButton: true,
        clickOnContinueButton: true,
        // Select covid vaccine doses is still not working change it after the bug is fixed.
        fieldsToSkip: ["First Name", "Last Name", "Email"],
      }
    );
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isLobbyLoaded();

    await test.step(`Open edit profile from lobby and fill up the fields as per the test data`, async () => {
      let customConditionaltFieldsData = new EditProfileData()
        .Custom_Conditional_Fields;
      let lobby: LobbyPage = new LobbyPage(page);
      let editProfilePage: EditProfilePage = await (
        await lobby.getTopNavBarComponent()
      ).openEditProfile();
      await expect(
        editProfilePage.firstNameInputBox,
        "expecting edit profile form to be visible"
      ).toBeVisible();
      // const profileUpdatePromise = page.waitForResponse(
      //   `${DataUtl.getApplicationTestDataObj().baseUrl
      //   }/api/account/profile/update?event_id=${eventId}`
      // );
      await editProfilePage.fillUserProfileForm(customConditionaltFieldsData);
      // const response = await profileUpdatePromise;
      // expect(
      //   response.status(),
      //   "expecting profile update API to return 200 status code"
      // ).toBe(200);
    });

    await test.step(`Verify the details filled`, async () => {
      let res = await page.request.get(`/api/event/${eventId}/role`);
      console.log("Res Json", await res.json());
      validateEditProfileConditionalFieldDataFromEventRoleApi(
        await res.json(),
        userInfoDTO.userRegFormData.customConditionalFieldFormDataeditProfile,
        {
          checkOptionalFields: true,
          checkMandatoryFields: true,
          fieldsToSkip: ["First Name", "Last Name", "Email"],
        }
      );
    });
  });

  // Adding skip as conditional fields are not working with magic link
  test.skip("TC005: Attendee changes feilds in registration form, verifying nested condition @landing-page-2", async () => {
    /*
            1. All reg field filled.
            2. If A selected only A dependent field should be visible not the other option.
            3. If not selected logic is working.
            4. Any of selcted and all of selected.
            5. Add desclaimer button is working.
            6. User changes option, the fields assosciated with previous option should not be displayed and made to fill.
             */
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    // Update event start time
    await updateEventDateTime(
      organiserApiContext,
      eventId,
      landingPageId,
      new Date().toISOString()
    );
    await inviteAttendeeByAPI(organiserApiContext, eventId, attendeeEmail);
    let magicLink = await fetchAttendeeInviteMagicLink(
      attendeeEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    let landingPageTwo = new LandingPageTwo(page);
    const eventRoleApiPromise = page.waitForResponse(
      (response) =>
        response.url().includes(`/event/${eventId}/role`) &&
        response.status() === 200
    );

    await landingPageTwo.load(magicLink);
    const getEventRoleResponse = await eventRoleApiPromise;
    expect(
      getEventRoleResponse.status(),
      "expecting event role api respoinse to be 200"
    ).toBe(200);
    // update the default reg fields to conditional feilds
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData
        .customConditionalFieldFormDataChangeinRegField,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
        clickOnDesclaimerButton: true,
        clickOnContinueButton: true,
        // Select covid vaccine doses is still not working change it after the bug is fixed.
        fieldsToSkip: ["First Name", "Last Name", "Email"],
      }
    );
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isLobbyLoaded();

    await test.step(`Verify the details filled`, async () => {
      let res = await page.request.get(`/api/event/${eventId}/role`);
      console.log("Res Json", await res.json());
      await validateEditProfileConditionalFieldDataFromEventRoleApi(
        await res.json(),
        userInfoDTO.userRegFormData.customConditionalFieldFormDataeditProfile,
        {
          checkOptionalFields: true,
          checkMandatoryFields: true,
          fieldsToSkip: ["First Name", "Last Name", "Email"],
        }
      );
    });
  });

  // Adding skip as conditional fields are not working with magic link
  test.skip("TC006: Attendee changes feilds in registration form, verifying nested condition for multi select and '&' condition @landing-page-2", async () => {
    /*
          1. All reg field filled.
          2. If A selected only A dependent field should be visible not the other option.
          3. If not selected logic is working.
          4. Any of selcted and all of selected.
          5. Add desclaimer button is working.
          6. User changes option, the fields assosciated with previous option should not be displayed and made to fill.
           */
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    // Update event start time
    await updateEventDateTime(
      organiserApiContext,
      eventId,
      landingPageId,
      new Date().toISOString()
    );
    await inviteAttendeeByAPI(organiserApiContext, eventId, attendeeEmail);
    let magicLink = await fetchAttendeeInviteMagicLink(
      attendeeEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    let landingPageTwo = new LandingPageTwo(page);
    const eventRoleApiPromise = page.waitForResponse(
      (response) =>
        response.url().includes(`/event/${eventId}/role`) &&
        response.status() === 200
    );

    await landingPageTwo.load(magicLink);
    const getEventRoleResponse = await eventRoleApiPromise;
    expect(
      getEventRoleResponse.status(),
      "expecting event role api respoinse to be 200"
    ).toBe(200);
    // update the default reg fields to conditional feilds
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData
        .customConditionalFieldAndConidtionFormDataChangeinRegField,
      {
        fillOptionalFields: true,
        fillMandatoryFields: true,
        clickOnDesclaimerButton: true,
        clickOnContinueButton: true,
        // Select covid vaccine doses is still not working change it after the bug is fixed.
        fieldsToSkip: ["First Name", "Last Name", "Email"],
        fieldsToBeHidden: ["Then why are you taking vaccines?"],
      }
    );
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isLobbyLoaded();

    await test.step(`Verify the details filled`, async () => {
      let res = await page.request.get(`/api/event/${eventId}/role`);
      console.log("Res Json", await res.json());
      validateEditProfileConditionalFieldDataFromEventRoleApi(
        await res.json(),
        userInfoDTO.userRegFormData
          .customConditionalFieldFormDataAndeditProfile,
        {
          checkOptionalFields: true,
          checkMandatoryFields: true,
          fieldsToSkip: ["First Name", "Last Name", "Email"],
        }
      );
    });
  });
});
