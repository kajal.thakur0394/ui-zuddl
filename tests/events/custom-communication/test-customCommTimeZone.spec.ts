import {
  test,
  BrowserContext,
  APIRequestContext,
  Page,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { CustomCommunicationPage } from "../../../page-objects/new-org-side-pages/CustomCommunicationPage";
import { EventController } from "../../../controller/EventController";
import { DataUtl } from "../../../util/dataUtil";
import { AudienceTypeEnum } from "../../../enums/CustomCommunicationEnums";
import CustomCommunication from "./customCommunicationVerificationFunctions";

test.describe.parallel("Custom Communication", async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventController: EventController;
  let orgPage: Page;
  let eventId;
  let eventName;
  let attendee1EmailId: string;
  let attendee2EmailId: string;
  let speaker1EmailId: string;
  let speaker2EmailId: string;
  let customCommVerification: CustomCommunication;

  let speaker1BrowserContext: BrowserContext;
  let speaker2BrowserContext: BrowserContext;
  let speaker1Page: Page;
  let speaker2Page: Page;
  let attendee1BrowserContext: BrowserContext;
  let attendee2BrowserContext: BrowserContext;
  let attendee1Page: Page;
  let attendee2Page: Page;

  const emailSenderName = "QA Automation";
  const emailSubject = "Custom Communication - Timezone Test";
  const organiserTZ = "Asia/Dubai";
  const attendee1TZ = "Australia/Sydney";
  const attendee2TZ = "America/New_York";
  const speaker1TZ = "America/Los_Angeles";
  const speaker2TZ = "Asia/Tokyo";

  test.beforeEach(async () => {
    await test.step("Initialize Organiser browser context, API context and page", async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        timeZoneId: organiserTZ,
      });
      orgApiContext = orgBrowserContext.request;
      orgPage = await orgBrowserContext.newPage();
    });

    await test.step("Initialize Speaker1 browser context page", async () => {
      speaker1BrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
        timeZoneId: speaker1TZ,
      });
      speaker1Page = await speaker1BrowserContext.newPage();
    });

    await test.step("Initialize Speaker2 browser context page", async () => {
      speaker2BrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
        timeZoneId: speaker2TZ,
      });
      speaker2Page = await speaker2BrowserContext.newPage();
    });

    await test.step("Initialize Attendee1 browser context page", async () => {
      attendee1BrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
        timeZoneId: attendee1TZ,
      });
      attendee1Page = await attendee1BrowserContext.newPage();
    });

    await test.step("Initialize Attendee2 browser context page", async () => {
      attendee2BrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
        timeZoneId: attendee2TZ,
      });
      attendee2Page = await attendee2BrowserContext.newPage();
    });

    await test.step("Create a new event", async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
      });
      eventController = new EventController(orgApiContext, eventId);
      customCommVerification = new CustomCommunication(eventId, orgApiContext);
    });

    await test.step("Get event info", async () => {
      let eventInfo = await (await eventController.getEventInfo()).json();
      console.log("eventInfo", eventInfo);
      eventName = eventInfo.eventName;
      console.log("Event Name", eventName);
    });

    await test.step("Update event start and end dates", async () => {
      await eventController.changeEventStartAndEndDateTime({
        deltaByDayInStartDate: 0,
        deltaByDayInEndDate: 12,
        deltaByHoursInEndTime: 2,
        deltaByHoursInStartTime: -2,
        eventTimeZone: organiserTZ,
      });
      const eventInfo = await (await eventController.getEventInfo()).json();
      console.log("eventInfo", eventInfo);
    });

    await test.step("Genereate random user email ids and add users to the event", async () => {
      attendee1EmailId = DataUtl.getRandomAttendeeMailosaurEmail();
      attendee2EmailId = DataUtl.getRandomAttendeeMailosaurEmail();
      speaker1EmailId = DataUtl.getRandomSpeakerMailosaurEmail();
      speaker2EmailId = DataUtl.getRandomSpeakerMailosaurEmail();

      await test.step("Adding Speaker1...", async () => {
        console.log("Speaker1 emailId ->", speaker1EmailId);

        await eventController.inviteSpeakerToTheEventByApi(
          speaker1EmailId,
          "QA Automation",
          "TestSpeaker1"
        );
      });

      await test.step("Adding Speaker2...", async () => {
        console.log("Speaker2 emailId ->", speaker1EmailId);

        await eventController.inviteSpeakerToTheEventByApi(
          speaker2EmailId,
          "QA Automation",
          "TestSpeaker2"
        );
      });

      await test.step("Adding Attendee1...", async () => {
        console.log("Attendee1 emailId ->", speaker1EmailId);

        await eventController.inviteAttendeeToTheEvent(attendee1EmailId);
      });

      await test.step("Adding Attendee2...", async () => {
        console.log("Attendee2 emailId ->", speaker1EmailId);

        await eventController.inviteAttendeeToTheEvent(attendee2EmailId);
      });
    });
  });

  test.afterEach(async () => {
    await eventController.cancelAnEvent();
    await orgPage.close();
    await speaker1Page.close();
    await speaker2Page.close();
    await attendee1Page.close();
    await attendee2Page.close();
    await orgBrowserContext.close();
    await speaker1BrowserContext.close();
    await speaker2BrowserContext.close();
    await attendee1BrowserContext.close();
    await attendee2BrowserContext.close();
  });

  test("TC001: Verify, if user from one time zone creates custom communication, it should be triggered at the same time to all the user who are in different time zone", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-776",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-782",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-816",
    });

    let customCommunicationPage = new CustomCommunicationPage(orgPage, eventId);
    let customCommunicationName = "Custom Communication - Time-zone test";

    await test.step("Create a Custom-Communication", async () => {
      await test.step("Populate Details page fields", async () => {
        await customCommunicationPage.createNewCustomCommunicationDetailsPage(
          [AudienceTypeEnum.ATTENDEE],
          "Send now",
          customCommunicationName
        );
      });

      await test.step("Select opitons in Custom Email page", async () => {
        await customCommunicationPage.createNewCustomCommunicationEmailPage(
          emailSenderName,
          emailSubject
        );
      });

      await test.step("Review page", async () => {
        await customCommunicationPage.newCustomCommunicationReviewPage();
      });
    });

    await test.step("Verify the email is received by attendees.", async () => {
      let a1MagicLink =
        await customCommVerification.verifyMagicLinkInCustomCommEmail(
          attendee1EmailId,
          emailSubject
        );
      await attendee1Page.goto(a1MagicLink);

      let a2MagicLink =
        await customCommVerification.verifyMagicLinkInCustomCommEmail(
          attendee2EmailId,
          emailSubject
        );
      await attendee2Page.goto(a2MagicLink);
    });

    //Code to be uncommented after discussion with developer
    // await test.step("Verify the email is not received by speakers.", async () => {
    //   await customCommVerification.verifyEmailIsNotReceived(
    //     speaker1EmailId,
    //     emailSubject
    //   );

    //   await customCommVerification.verifyMagicLinkInCustomCommEmail(
    //     speaker2EmailId,
    //     emailSubject
    //   );
    // });

    // await test.step("Verify status is changed to Completed.", async () => {
    //   await customCommunicationPage.verifyToggleStatus(
    //     customCommunicationName,
    //     CustomCommTypeEnum.LIVE_EVENT,
    //     "Completed",
    //     500
    //   );
    // });
  });
});
