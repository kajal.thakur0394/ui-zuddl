import {
  test,
  BrowserContext,
  APIRequestContext,
  Page,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { CustomCommunicationPage } from "../../../page-objects/new-org-side-pages/CustomCommunicationPage";
import { EventController } from "../../../controller/EventController";
import { DataUtl } from "../../../util/dataUtil";
import {
  AudienceTypeEnum,
  CustomCommTypeEnum,
  TriggerTypeEnum,
} from "../../../enums/CustomCommunicationEnums";
import CustomCommunication from "./customCommunicationVerificationFunctions";

test.describe.parallel("Custom Communication", async () => {
  test.describe.configure({ retries: 2 });
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventController: EventController;
  let orgPage: Page;
  let eventId;
  let eventName;
  let attendeeEmailId: string;
  let speakerEmailId: string;
  let customCommVerification: CustomCommunication;

  const emailSenderName = "QA Automation";
  const emailSubject = "Custom Communication";

  test.beforeEach(async () => {
    await test.step("Initialize Organiser browser context, API context and page", async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({});
      orgApiContext = orgBrowserContext.request;
      orgPage = await orgBrowserContext.newPage();
    });

    await test.step("Create a new event", async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
      });
      eventController = new EventController(orgApiContext, eventId);
      customCommVerification = new CustomCommunication(eventId, orgApiContext);
    });

    await test.step("Get event info", async () => {
      let eventInfo = await (await eventController.getEventInfo()).json();
      console.log("eventInfo", eventInfo);
      eventName = eventInfo.eventName;
      console.log("Event Name", eventName);
    });

    await test.step("Update event start and end dates", async () => {
      await eventController.changeEventStartAndEndDateTime({
        deltaByDayInStartDate: 0,
        deltaByDayInEndDate: 12,
        deltaByHoursInEndTime: 2,
        deltaByHoursInStartTime: -2,
      });
      const eventInfo = await (await eventController.getEventInfo()).json();
      console.log("eventInfo", eventInfo);
    });

    await test.step("Genereate random user email ids and add users to the event", async () => {
      attendeeEmailId = DataUtl.getRandomAttendeeMailosaurEmail();
      speakerEmailId = DataUtl.getRandomSpeakerMailosaurEmail();

      eventController.inviteAttendeeToTheEvent(attendeeEmailId);
      eventController.inviteSpeakerToTheEventByApi(
        speakerEmailId,
        "QA Automation",
        "TestSpeaker"
      );
    });
  });

  test.afterEach(async () => {
    await eventController.cancelAnEvent();
    await orgPage.close();
    await orgBrowserContext.close();
  });

  test("TC001: Create a new custom communication as 'Send Now'", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-773",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-774",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-778",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-779",
    });

    let customCommunicationPage = new CustomCommunicationPage(orgPage, eventId);
    let customCommunicationName = "Custom Communication - Send-now test";

    await test.step("Create a Custom-Communication", async () => {
      await test.step("Populate Details page fields", async () => {
        await customCommunicationPage.createNewCustomCommunicationDetailsPage(
          [AudienceTypeEnum.ATTENDEE, AudienceTypeEnum.SPEAKER],
          "Send now",
          customCommunicationName
        );
      });

      await test.step("Select opitons in Custom Email page", async () => {
        await customCommunicationPage.createNewCustomCommunicationEmailPage(
          emailSenderName,
          emailSubject
        );
      });

      await test.step("Review page", async () => {
        await customCommunicationPage.newCustomCommunicationReviewPage();
      });
    });

    await test.step("Verify the email.", async () => {
      await customCommVerification.verifyMagicLinkInCustomCommEmail(
        attendeeEmailId,
        emailSubject
      );

      await customCommVerification.verifyMagicLinkInCustomCommEmail(
        speakerEmailId,
        emailSubject
      );
    });

    await test.step("Verify toggle button is disabled for Send-now communication.", async () => {
      await customCommunicationPage.verifyToggleButtonIsDisabled(
        customCommunicationName,
        CustomCommTypeEnum.LIVE_EVENT
      );
    });
  });

  test("TC002: Verify, Undoing Send-now will redirect to the edit page", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-815",
    });

    let customCommunicationPage = new CustomCommunicationPage(orgPage, eventId);
    let customCommunicationName = "Custom Comm - Undo test";

    await test.step("Create a Custom-Communication", async () => {
      await test.step("Populate Details page fields", async () => {
        await customCommunicationPage.createNewCustomCommunicationDetailsPage(
          [AudienceTypeEnum.ATTENDEE, AudienceTypeEnum.SPEAKER],
          "Send now",
          customCommunicationName
        );
      });

      await test.step("Select opitons in Custom Email page", async () => {
        await customCommunicationPage.createNewCustomCommunicationEmailPage(
          emailSenderName,
          emailSubject
        );
      });

      await test.step("Review page", async () => {
        await customCommunicationPage.newCustomCommunicationReviewPage();
      });
    });

    await test.step("Verify Undoing the Send-now navigates to edit screen.", async () => {
      await customCommunicationPage.verifyUndoRedirectsToEditPage();
    });
  });

  test("TC003: Verify, at a time only one option can be selected from communication type", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-775",
    });

    let customCommunicationPage = new CustomCommunicationPage(orgPage, eventId);

    await test.step("Verify only one Communication type can be selected at a time.", async () => {
      await customCommunicationPage.verifyOnlyOneCommTypeCanBeSelectedAtATime();
    });
  });

  test("TC004: Verify, schedule for later organiser shouldn’t be able to select previous time", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-777",
    });

    let customCommunicationPage = new CustomCommunicationPage(orgPage, eventId);
    let customCommunicationName = "Custom Comm - Schedule Time test";

    await test.step("Create a Custom-Communication", async () => {
      await customCommunicationPage.verifyScheduleTimeCannotBeInPast(
        [AudienceTypeEnum.ATTENDEE],
        customCommunicationName
      );
    });
  });

  test("TC005: Verify, if schedule for later communication status toggle is disabled while creating then after creation it should be disabled", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-780",
    });

    let customCommunicationPage = new CustomCommunicationPage(orgPage, eventId);
    let customCommunicationName = "Custom Comm - Toogle Test";

    await test.step("Create a Custom-Communication", async () => {
      await test.step("Populate Details page fields", async () => {
        await customCommunicationPage.createNewCustomCommunicationDetailsPage(
          [AudienceTypeEnum.ATTENDEE, AudienceTypeEnum.SPEAKER],
          "Schedule",
          customCommunicationName,
          false
        );
      });

      await test.step("Select opitons in Custom Email page", async () => {
        await customCommunicationPage.createNewCustomCommunicationEmailPage(
          emailSenderName,
          emailSubject
        );
      });

      await test.step("Review page", async () => {
        await customCommunicationPage.newCustomCommunicationReviewPage(false);
      });
    });

    await test.step("Verify the communication toggle status.", async () => {
      await customCommunicationPage.verifyToggleStatus(
        customCommunicationName,
        CustomCommTypeEnum.LIVE_EVENT,
        "Inactive"
      );
    });
  });

  test("TC006: Verify, once communication created it can be edited", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-781",
    });

    let customCommunicationPage = new CustomCommunicationPage(orgPage, eventId);
    let customCommunicationName = "Custom Comm - Edit Comm Test";

    await test.step("Create a Custom-Communication", async () => {
      await test.step("Populate Details page fields", async () => {
        await customCommunicationPage.createNewCustomCommunicationDetailsPage(
          [AudienceTypeEnum.ATTENDEE, AudienceTypeEnum.SPEAKER],
          "Schedule",
          customCommunicationName,
          false
        );
      });

      await test.step("Select opitons in Custom Email page", async () => {
        await customCommunicationPage.createNewCustomCommunicationEmailPage(
          emailSenderName,
          emailSubject
        );
      });

      await test.step("Review page", async () => {
        await customCommunicationPage.newCustomCommunicationReviewPage(false);
      });
    });

    await test.step("Verify the communication toggle status.", async () => {
      await customCommunicationPage.verifyToggleStatus(
        customCommunicationName,
        CustomCommTypeEnum.LIVE_EVENT,
        "Inactive"
      );
    });

    await test.step("Verify the communication can be edited.", async () => {
      await customCommunicationPage.verifyCommunicationIsEditable(
        customCommunicationName,
        CustomCommTypeEnum.LIVE_EVENT
      );
    });
  });

  test("TC007: Verify, once communication created it can be deleted", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-819",
    });

    let customCommunicationPage = new CustomCommunicationPage(orgPage, eventId);
    let customCommunicationName = "Custom Comm - Delete Comm Test";

    await test.step("Create a Custom-Communication", async () => {
      await test.step("Populate Details page fields", async () => {
        await customCommunicationPage.createNewCustomCommunicationDetailsPage(
          [AudienceTypeEnum.ATTENDEE, AudienceTypeEnum.SPEAKER],
          "Schedule",
          customCommunicationName,
          false
        );
      });

      await test.step("Select opitons in Custom Email page", async () => {
        await customCommunicationPage.createNewCustomCommunicationEmailPage(
          emailSenderName,
          emailSubject
        );
      });

      await test.step("Review page", async () => {
        await customCommunicationPage.newCustomCommunicationReviewPage(false);
      });
    });

    await test.step("Verify the communication toggle status.", async () => {
      await customCommunicationPage.verifyCommunicationIsDeleted(
        customCommunicationName,
        CustomCommTypeEnum.LIVE_EVENT
      );
    });
  });
});
