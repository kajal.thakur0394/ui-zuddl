import { APIRequestContext, expect } from "@playwright/test";
import {
  fetchMailasaurEmailObject,
  verifyMailasaurEmailNotReceived,
} from "../../../util/emailUtil";
import { emailTemplateValidation } from "../../../util/emailTemplateValidation";

export default class CustomCommunication {
  protected eventId: any;
  protected orgApiRequestContext: APIRequestContext;

  constructor(eventId: any, orgApiReuqestContext: APIRequestContext) {
    this.eventId = eventId;
    this.orgApiRequestContext = orgApiReuqestContext;
  }

  async verifyMagicLinkInCustomCommEmail(userEmailId, eventName) {
    let fetched_email = await fetchMailasaurEmailObject(userEmailId, eventName);

    let emailValidationObject = new emailTemplateValidation(fetched_email);
    let links = await emailValidationObject.fetchAllLinks();
    let magicLink: string = links[0];

    console.log(`Fetched magic link -> ${magicLink}`);

    expect(magicLink).toBeDefined();
    return magicLink;
  }

  async verifyEmailIsNotReceived(userEmailId, eventName) {
    await verifyMailasaurEmailNotReceived(userEmailId, eventName);
  }
}
