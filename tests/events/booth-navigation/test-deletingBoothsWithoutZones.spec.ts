import test, { APIRequestContext, BrowserContext } from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import {
  inviteAttendeeByAPI,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { ExpoPage } from "../../../page-objects/events-pages/zones/ExpoPage";
import { ExpoController } from "../../../controller/ExpoController";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel(`@expo @boothWithoutZones`, async () => {
  let eventId;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let expoListingPage: string;
  let boothId: string;
  let boothName: string;
  let boothIds: string[] = [];
  let boothNames: string[] = [];
  let eventController: EventController;
  let expoController: ExpoController;
  let magicLink: string;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});

    orgApiContext = orgBrowserContext.request;

    await test.step("Creating new event.", async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
      });
    });

    eventController = new EventController(orgApiContext, eventId);
    expoController = new ExpoController(orgApiContext, eventId);

    await test.step("Update event landing page details.", async () => {
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
      });
    });

    let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
    expoListingPage = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/expo`;

    await test.step("Update event landing page details.", async () => {
      await eventController.disableOnboardingChecksForAttendee();
    });
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test("TC001: Organizer deletes one booth.", async ({ page }) => {
    test.info().annotations.push({
      type: "P1",
      description:
        "https://linear.app/zuddl/issue/QAT-244/deleting-1-booths-or-all-booths-on-setup-side-should-not-result-in",
    });

    await test.step("Create a booth in Expo.", async () => {
      let boothCreationResponse =
        await expoController.createSingleBoothWithoutZone();

      for (let [key, value] of boothCreationResponse) {
        console.log(key + " -> " + value);
        boothId = key;
        boothName = value;
      }
    });

    const attendeeEmail = DataUtl.getRandomAttendeeEmail();

    await test.step("Invite Attendee by API.", async () => {
      await inviteAttendeeByAPI(orgApiContext, eventId, attendeeEmail, true);
    });

    await test.step("Fetch Magic-Link from database.", async () => {
      magicLink = await QueryUtil.fetchMagicLinkFromDB(eventId, attendeeEmail);
    });

    await test.step("Attendee joins the event via Magic-Link.", async () => {
      await page.goto(magicLink);
      await page.click("text=Enter Now");
      await page.goto(expoListingPage);
    });

    let expoPage = new ExpoPage(page);

    await expoPage.verifyBoothVisibility(boothId, boothName);

    await test.step("Deleting booth.", async () => {
      await expoController.deleteBooth(boothId);
    });

    await test.step("Reload attendee expo page.", async () => {
      await page.reload();
    });

    await expoPage.verifyBoothVisibility(boothId, boothName, false);
  });

  test("TC002: Organizer deletes multiple booths.", async ({}) => {
    test.info().annotations.push({
      type: "P1",
      description:
        "https://linear.app/zuddl/issue/QAT-244/deleting-1-booths-or-all-booths-on-setup-side-should-not-result-in",
    });
    const speakerBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    const page = await speakerBrowserContext.newPage();
    const speakerRequestContext = speakerBrowserContext.request;
    let response = new Map();

    await test.step("Create multiple booths in Expo.", async () => {
      response = await expoController.createMultipleBoothsWithoutZone(5);

      for (let [key, value] of response) {
        console.log(key + " -> " + value);
        boothIds.push(key);
        boothNames.push(value);
      }
    });

    const attendeeEmail = DataUtl.getRandomAttendeeEmail();

    await test.step("Invite Attendee by API.", async () => {
      await inviteAttendeeByAPI(orgApiContext, eventId, attendeeEmail, true);
    });

    await test.step("Fetch Magic-Link from database.", async () => {
      magicLink = await QueryUtil.fetchMagicLinkFromDB(eventId, attendeeEmail);
    });

    await test.step("Attendee joins the event via Magic-Link.", async () => {
      await page.goto(magicLink);
      await page.click("text=Enter Now");
      await page.goto(expoListingPage);
    });

    let expoPage = new ExpoPage(page);

    for (let [boothid, boothname] of response) {
      await expoPage.verifyBoothVisibility(boothid, boothname);
    }

    await test.step("Deleting booth.", async () => {
      for (let boothid of response.keys()) {
        await expoController.deleteBooth(boothid);
      }
    });

    await test.step("Reload attendee expo page.", async () => {
      await page.reload();
    });

    await expoPage.verifyBoothVisibility("", "", false);
  });
});
