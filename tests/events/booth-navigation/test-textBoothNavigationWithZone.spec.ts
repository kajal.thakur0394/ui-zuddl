import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import {
  inviteAttendeeByAPI,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { ExpoPage } from "../../../page-objects/events-pages/zones/ExpoPage";
import { ExpoController } from "../../../controller/ExpoController";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@expo @withZones`, async () => {
  let eventId;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let expoListingPage: string;
  let attendeeLandingPage: string;
  let zoneName: string;
  let boothName: string;
  let zoneId: string;
  let boothId: string;
  let layoutId: string;
  let textWidgetContent: string;
  let magicLink: string;
  let eventController: EventController;
  let expoController: ExpoController;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});

    orgApiContext = orgBrowserContext.request;

    await test.step("Creating new event.", async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
      });
    });

    eventController = new EventController(orgApiContext, eventId);
    expoController = new ExpoController(orgApiContext, eventId);

    await test.step("Update event landing page details.", async () => {
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
      });
    });

    let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
    expoListingPage = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/expo`;

    await test.step("Update event landing page details.", async () => {
      await eventController.disableOnboardingChecksForAttendee();
    });
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test("TC001: Attendee is able to enter booth with text widget inside a zone.", async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "P1",
      description:
        "https://linear.app/zuddl/issue/QAT-182/navigation-to-booths",
    });

    await test.step("Create a Zone inside Expo.", async () => {
      let zoneCreationResponse = await expoController.createZone();
      zoneId = zoneCreationResponse["boothZoneId"];
      zoneName = zoneCreationResponse["title"];
    });

    await test.step("Create a Booth inside Zone.", async () => {
      boothName = DataUtl.generateRandomBoothName();
      let boothCreationResponse =
        await expoController.createSingleBoothWithinZone(zoneId, boothName);

      boothId = boothCreationResponse.get(boothName);
    });

    await test.step("Get layoutId of booth.", async () => {
      let jsonResponse = JSON;
      jsonResponse = await expoController.getLayoutId(boothId);

      layoutId = jsonResponse["layoutId"];
    });

    await test.step("Add text widget in booth.", async () => {
      expoController.addTextWidget(layoutId);
    });

    const attendeeEmail = DataUtl.getRandomAttendeeEmail();

    await test.step("Invite Attendee by API.", async () => {
      await inviteAttendeeByAPI(orgApiContext, eventId, attendeeEmail, true);
    });

    await test.step("Fetch Magic-Link from database.", async () => {
      magicLink = await QueryUtil.fetchMagicLinkFromDB(eventId, attendeeEmail);
    });

    await test.step("Attendee joins the event via Magic-Link.", async () => {
      await page.goto(magicLink);
      await page.click("text=Enter Now");
      await page.goto(expoListingPage);
    });

    let expoPage = new ExpoPage(page);

    await test.step("Attendee enters the zone.", async () => {
      await expoPage.enterZone(zoneName);
    });

    await test.step("Attendee enters the booth.", async () => {
      await expoPage.verifyBoothVisibility(boothId, boothName);
      await expoPage.enterBoothWithId(boothId);
    });

    await test.step("Verify the text widget.", async () => {
      await expoPage.verifyTextWidget(textWidgetContent);
    });
  });
});
