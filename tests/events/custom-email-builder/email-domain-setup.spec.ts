import test, {
  APIRequestContext,
  BrowserContext,
  expect,
  Page,
} from "@playwright/test";
import EventRole from "../../../enums/eventRoleEnum";
import EventSettingID from "../../../enums/EventSettingEnum";
import { EmailSendersPage } from "../../../page-objects/new-org-side-pages/EmailSendersPage";
import { EmailTemplateEditPage } from "../../../page-objects/new-org-side-pages/EmailTemplateEditPage";
import { SpeakerData } from "../../../test-data/speakerData";
import {
  createNewEvent,
  inviteSpeakerByApi,
  registerUserToEventbyApi,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { EmailDomainAPIController } from "../../../util/email-domain-api-controller";
import { fetchSettingsID } from "../../../util/email-validation-api-util";
import { fetchMailasaurEmailObject } from "../../../util/emailUtil";

test.describe("@custom-email-sender @new-org-side custom email senders setup", async () => {
  test.describe.configure({ retries: 2 });

  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let organiserPage: Page;
  let emailSendersSetupPage: EmailSendersPage;
  let byDefaultEmailDomain: string;
  let newCustomDomainToAdd: string;
  let emailDomainApiController: EmailDomainAPIController;
  let defaultCountOfSenderEmail = 1;
  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = orgBrowserContext.request;
    organiserPage = await orgBrowserContext.newPage();

    // Email controller for env setup
    emailDomainApiController = new EmailDomainAPIController(
      organiserPage.request
    );
    await test.step("deleting all custom emails for this org except zuddl email", async () => {
      await emailDomainApiController.deleteAllDomainsExceptZuddlEmail();
    });

    let emailSendersPageUrl = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/dashboard/email-senders`;
    await organiserPage.goto(emailSendersPageUrl);
    emailSendersSetupPage = new EmailSendersPage(organiserPage);
    byDefaultEmailDomain = "noreply@zuddl.com";
    newCustomDomainToAdd = "automation_domain@qa.zuddl.io";
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test("TC001: Empty state checks on Email senders setup page", async () => {
    //expect attendee count is 0
    await emailSendersSetupPage.verifyTotalRecordCountToBe(
      defaultCountOfSenderEmail
    );
    await expect(
      await emailSendersSetupPage.getSendersEmailAddressWithDefaultBadge(),
      `Expecting ${byDefaultEmailDomain} to be shown with default badge`
    ).toContainText("noreply@zuddl.com");
  });

  test("TC002: Add new sender id with unverified dns record with all fields filled", async () => {
    //get count before adding this email

    let sendersEmailAddress =
      DataUtl.getRandomSenderEmailDomainWithUnVerifiedDns();
    await emailSendersSetupPage.clickOnAddEmailAddressButton();
    await emailSendersSetupPage.fillUpAddDomainForm({
      sendersEmailAddress: sendersEmailAddress,
      replyEmailAddress: DataUtl.getRandomSenderEmailDomainWithUnVerifiedDns(),
      sendersName: "Automation Emails",
    });
    // await organiserPage.pause();
    await emailSendersSetupPage.clickOnVerifyRecordOnDNSRecordForm();
    //since the record is not yet added, it should show verification failed on ui

    await expect(
      emailSendersSetupPage.verifyRecordButtonOnVerifyForm,
      "Expecting verification failed button to be shown on verify form"
    ).toHaveText("Verification failed");

    await emailSendersSetupPage.closeTheDomainAdditionForm();

    await emailSendersSetupPage.verifyTotalRecordCountToBe(
      defaultCountOfSenderEmail + 1
    );
    // verify the status of newly added email is verification pending
    await expect(
      await emailSendersSetupPage.getVerificationStatusForSenderEmail(
        sendersEmailAddress
      ),
      `Expecting newly added domain status to be verification pending`
    ).toHaveText("Verification pending");
    //verify default status does not change after adding any new email
    await expect(
      await emailSendersSetupPage.getSendersEmailAddressWithDefaultBadge(),
      `Expecting ${byDefaultEmailDomain} to be shown with default badge`
    ).toContainText("noreply@zuddl.com");
  });

  test.fixme(
    "TC003: Add and then delete an email domain from list",
    async () => {
      /**
       * Its failing on ci pipeline, playwright is not able to enter email and password.
       * it remains empty, runs fine on local
       */
      let sendersEmailAddress =
        DataUtl.getRandomSenderEmailDomainWithUnVerifiedDns();
      await emailSendersSetupPage.clickOnAddEmailAddressButton();
      await emailSendersSetupPage.fillUpAddDomainForm({
        sendersEmailAddress: sendersEmailAddress,
        replyEmailAddress:
          DataUtl.getRandomSenderEmailDomainWithUnVerifiedDns(),
        sendersName: "Automation Emails",
      });
      await emailSendersSetupPage.closeTheDomainAdditionForm();
      await emailSendersSetupPage.verifyTotalRecordCountToBe(
        defaultCountOfSenderEmail + 1
      );
      await emailSendersSetupPage.deleteRecord(sendersEmailAddress);
      // verify after delete record count is again 1
      await emailSendersSetupPage.verifyTotalRecordCountToBe(
        defaultCountOfSenderEmail
      );
    }
  );

  test.fixme("TC004: Add an email domain and make it default", async () => {
    await emailSendersSetupPage.clickOnAddEmailAddressButton();
    await emailSendersSetupPage.fillUpAddDomainForm({
      sendersEmailAddress: newCustomDomainToAdd,
      replyEmailAddress: DataUtl.getRandomSenderEmailDomainWithVerifiedDns(),
      sendersName: "Automation Emails",
    });
    //since the record is already added, it should show verified on view dns form
    await expect(
      emailSendersSetupPage.verifyRecordButtonOnVerifyForm,
      "Expecting verification failed button to be shown on verify form"
    ).toHaveText("Record verified");

    await emailSendersSetupPage.closeTheDomainAdditionForm();
    await emailSendersSetupPage.verifyTotalRecordCountToBe(
      defaultCountOfSenderEmail + 1
    );
    // verify the status of newly added email is verification pending
    await expect(
      await emailSendersSetupPage.getVerificationStatusForSenderEmail(
        newCustomDomainToAdd
      ),
      `Expecting newly added domain status to be verified`
    ).toHaveText("Verified");
    await emailSendersSetupPage.makeRecordDefault(newCustomDomainToAdd);
    //verify default status has changed to newly added email
    await expect(
      await emailSendersSetupPage.getSendersEmailAddressWithDefaultBadge(),
      `Expecting ${newCustomDomainToAdd} to be shown with default badge`
    ).toContainText(newCustomDomainToAdd);
  });

  test.fixme(
    "TC005: Deleting a custom default email should make zuddl email default",
    async () => {
      await emailSendersSetupPage.clickOnAddEmailAddressButton();
      await emailSendersSetupPage.fillUpAddDomainForm({
        sendersEmailAddress: newCustomDomainToAdd,
        replyEmailAddress: DataUtl.getRandomSenderEmailDomainWithVerifiedDns(),
        sendersName: "Automation Emails",
      });
      //since the record is already added, it should show verified on view dns form
      await expect(
        emailSendersSetupPage.verifyRecordButtonOnVerifyForm,
        "Expecting Record verified button text to be shown on verify form"
      ).toHaveText("Record verified");

      await emailSendersSetupPage.closeTheDomainAdditionForm();
      await emailSendersSetupPage.verifyTotalRecordCountToBe(
        defaultCountOfSenderEmail + 1
      );
      // verify the status of newly added email is verification pending
      await expect(
        await emailSendersSetupPage.getVerificationStatusForSenderEmail(
          newCustomDomainToAdd
        ),
        `Expecting newly added domain status to be verified`
      ).toHaveText("Verified");
      await emailSendersSetupPage.makeRecordDefault(newCustomDomainToAdd);
      //verify default status has changed to newly added email
      await expect(
        await emailSendersSetupPage.getSendersEmailAddressWithDefaultBadge(),
        `Expecting ${newCustomDomainToAdd} to be shown with default badge`
      ).toContainText(newCustomDomainToAdd);

      //now delete the newly added custom email
      await emailSendersSetupPage.deleteRecord(newCustomDomainToAdd);
      //verify default status has changed to newly added email
      await expect(
        await emailSendersSetupPage.getSendersEmailAddressWithDefaultBadge(),
        `Expecting ${byDefaultEmailDomain} to be shown with default badge`
      ).toContainText(byDefaultEmailDomain);
    }
  );

  test.fixme(
    "TC006: Updating Sender email for Attendee registration email with a verified domain email",
    async () => {
      // add a new domain by api and verify it
      let eventTitle = Date.now().toString() + "_automationevent";
      let eventId;
      let newSubjectLine = `Automation Suite ${eventTitle}`;
      let newSendersName = DataUtl.getRandomName();
      let sendersEmailIdWithVerifiedDNS =
        DataUtl.getRandomSenderEmailDomainWithVerifiedDns();
      let attendeeEmail = DataUtl.getRandomAttendeeEmail();
      let emailTemplateEditPage: EmailTemplateEditPage;

      await test.step(`Add new domain to org with verified dns ${sendersEmailIdWithVerifiedDNS}`, async () => {
        await emailDomainApiController.addNewEmailDomain({
          senderEmail: sendersEmailIdWithVerifiedDNS,
          senderName: newSendersName,
          replyEmail: DataUtl.getRandomSenderEmailDomainWithUnVerifiedDns(),
        });
      });

      await test.step(`Create new event where we will override the senders email in attendee reg template`, async () => {
        eventId = await createNewEvent({
          api_request_context: orgApiContext,
          event_title: eventTitle,
        });
      });

      await test.step(`Load attendee registration edit page template for ${eventId}`, async () => {
        let attendeeRegEmailSettingId = await fetchSettingsID(
          orgApiContext,
          eventId,
          EventSettingID.EmailOnRegistration,
          EventRole.ATTENDEE
        );
        let attendeeRegEmailEditUrl = `${
          DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
        }/event/${eventId}/emails/${attendeeRegEmailSettingId}/attendees/registration`;
        console.log(
          `attendee reg email edit url is ${attendeeRegEmailEditUrl}`
        );
        await organiserPage.goto(attendeeRegEmailEditUrl);
      });

      await test.step(`Verify the default state of edit email template`, async () => {
        emailTemplateEditPage = new EmailTemplateEditPage(organiserPage);
        // verify by default the edit page shows that the email is inactive
        await expect(
          emailTemplateEditPage.activeToggleButton,
          "verify by default the edit page shows that the email is inactive"
        ).not.toBeChecked();
        // verify when email template is inactive, the dropdown to update sender email is disabled
        await emailTemplateEditPage.verifyTheSendersEmailDropDownIsDisabled();
      });

      await test.step(`Enable the attendee reg email template by toggle`, async () => {
        // make the email active
        await emailTemplateEditPage.enableActiveTemplateToggle();
      });

      await test.step(`Update sender email to ${sendersEmailIdWithVerifiedDNS} and sender name to ${newSendersName} and subject to ${newSubjectLine}`, async () => {
        // select the newly added verified domain from dropdown
        await emailTemplateEditPage.selectEmailDromSendersEmailDropdown(
          sendersEmailIdWithVerifiedDNS
        );
        // update senders name
        await emailTemplateEditPage.updateSendersName(newSendersName);
        // update email subject
        await emailTemplateEditPage.updateEmailSubject(newSubjectLine);
        // register a new user now via api, should get the new email
        await expect(emailTemplateEditPage.emailSubjectInputBox).toHaveText(
          newSubjectLine
        );
        await organiserPage.waitForLoadState("networkidle");

        await expect(emailTemplateEditPage.emailSubjectInputBox).toHaveText(
          newSubjectLine
        );
      });
      // click on sender email dropdown, should show the verified domain list in the dropdown

      await test.step(`Registering a new user via API and verify if he recieves the email with updated params`, async () => {
        await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
        const messageObject = await fetchMailasaurEmailObject(
          attendeeEmail,
          newSubjectLine,
          true,
          60
        );
        const sender_name_from_message_object = messageObject.from[0].name;
        const sender_email_from_message_object = messageObject.from[0].email;
        expect(
          sender_name_from_message_object,
          "Expecting mail to be recieved with new sender name"
        ).toBe(newSendersName);
        expect(
          sender_email_from_message_object,
          "Expecting mail to be recieved with new sender email "
        ).toBe(sendersEmailIdWithVerifiedDNS.toLowerCase());
      });
    }
  );

  test.fixme(
    "TC007: Updating Sender email for Speaker Invite email with a verified domain email",
    async () => {
      // add a new domain by api and verify it
      let eventTitle = Date.now().toString() + "_automationevent";
      let eventId;
      let newSubjectLine = `Automation Suite ${eventTitle}`;
      let newSendersName = DataUtl.getRandomName();
      let sendersEmailIdWithVerifiedDNS =
        DataUtl.getRandomSenderEmailDomainWithVerifiedDns();
      let speakerEmail = DataUtl.getRandomSpeakerEmail();
      let emailTemplateEditPage: EmailTemplateEditPage;

      await test.step(`Add new domain to org with verified dns ${sendersEmailIdWithVerifiedDNS}`, async () => {
        await emailDomainApiController.addNewEmailDomain({
          senderEmail: sendersEmailIdWithVerifiedDNS,
          senderName: newSendersName,
          replyEmail: DataUtl.getRandomSenderEmailDomainWithUnVerifiedDns(),
        });
      });

      await test.step(`Create new event where we will override the senders email in speaker invite template`, async () => {
        eventId = await createNewEvent({
          api_request_context: orgApiContext,
          event_title: eventTitle,
        });
      });

      await test.step(`Load speaker invite email edit template page for ${eventId}`, async () => {
        let speakerInviteEmailSettingId = await fetchSettingsID(
          orgApiContext,
          eventId,
          EventSettingID.EventInvitationFromEmailFromPeopleSection,
          EventRole.SPEAKER
        );
        let speakerInviteEmailEditUrl = `${
          DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
        }/event/${eventId}/emails/${speakerInviteEmailSettingId}/speakers/invitation`;
        console.log(
          `speaker invite email edit url is ${speakerInviteEmailEditUrl}`
        );
        await organiserPage.goto(speakerInviteEmailEditUrl);
      });

      await test.step(`Verify the default state of edit email template`, async () => {
        emailTemplateEditPage = new EmailTemplateEditPage(organiserPage);
        // verify by default the edit page shows that the email is inactive
        await expect(
          emailTemplateEditPage.activeToggleButton,
          "verify by default the edit page shows that the email is inactive"
        ).not.toBeChecked();
        // verify when email template is inactive, the dropdown to update sender email is disabled
        await emailTemplateEditPage.verifyTheSendersEmailDropDownIsDisabled();
      });

      await test.step(`Enable the speaker invite email template by toggle`, async () => {
        // make the email active
        await emailTemplateEditPage.enableActiveTemplateToggle();
      });

      await test.step(`Update sender email to ${sendersEmailIdWithVerifiedDNS} and sender name to ${newSendersName} and subject to ${newSubjectLine}`, async () => {
        // select the newly added verified domain from dropdown
        await emailTemplateEditPage.selectEmailDromSendersEmailDropdown(
          sendersEmailIdWithVerifiedDNS
        );
        // update senders name
        await emailTemplateEditPage.updateSendersName(newSendersName);
        // update email subject
        await emailTemplateEditPage.updateEmailSubject(newSubjectLine);
        // register a new user now via api, should get the new email
        await expect(emailTemplateEditPage.emailSubjectInputBox).toHaveText(
          newSubjectLine
        );
        await organiserPage.waitForLoadState("networkidle");
        await expect(emailTemplateEditPage.emailSubjectInputBox).toHaveText(
          newSubjectLine
        );
      });
      // click on sender email dropdown, should show the verified domain list in the dropdown

      await test.step(`Invite a new speaker via API and verify if he recieves the email with updated params`, async () => {
        let speakerUserDTO = new SpeakerData(speakerEmail, "Prateek", "QA");
        await inviteSpeakerByApi(
          orgApiContext,
          speakerUserDTO.getSpeakerDataObject(),
          eventId,
          true
        );
        const messageObject = await fetchMailasaurEmailObject(
          speakerEmail,
          newSubjectLine,
          true,
          60
        );
        const sender_name_from_message_object = messageObject.from[0].name;
        const sender_email_from_message_object = messageObject.from[0].email;
        expect(
          sender_name_from_message_object,
          "Expecting mail to be recieved with new sender name"
        ).toBe(newSendersName);
        expect(
          sender_email_from_message_object,
          "Expecting mail to be recieved with new sender email "
        ).toBe(sendersEmailIdWithVerifiedDNS.toLowerCase());
      });
    }
  );
});
