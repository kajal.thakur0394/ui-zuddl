import {
  APIRequestContext,
  BrowserContext,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../../controller/EventController";
import BrowserFactory from "../../../../util/BrowserFactory";
import { DataUtl } from "../../../../util/dataUtil";
import { updateEventLandingPageDetails } from "../../../../util/apiUtil";
import EventEntryType from "../../../../enums/eventEntryEnum";
import { LobbyPage } from "../../../../page-objects/events-pages/zones/LobbyPage";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel(`@chat @eventchat @prod-issues`, async () => {
  let eventId;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventController: EventController;
  let attendeeEmail: string;
  let attendeeLobbyPage: LobbyPage;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});

    orgApiContext = orgBrowserContext.request;
    await test.step(`Create a new event`, async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
      });
      console.log(`event id is ${eventId}`);
    });
    eventController = new EventController(orgApiContext, eventId);

    await test.step(`Enable magic link for this event`, async () => {
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
      });
    });

    await test.step(`Invite new attendee to the event`, async () => {
      attendeeEmail = DataUtl.getRandomAttendeeEmail();
      await eventController.inviteAttendeeToTheEvent(attendeeEmail);
    });

    await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
      false
    );
  });
  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`Verify dom only renders 50 chat messages at a time and if new message comes in , the old msgs should be reomved from DOM`, async ({
    page,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "P0",
      description:
        "https://linear.app/zuddl/issue/QAT-79/before-we-used-to-only-show-50-chat-and-qanda-messages-at-a-time-753",
    });
    await test.step(`Add 40 chat messages to the event chat`, async () => {
      for (let i = 0; i < 40; i++) {
        await test.step(`Posting message number : ${i + 1}`, async () => {
          await eventController.postEventChatMessage(
            `Hi, first phase, This is prateek from Automation message no: ${
              i + 1
            } `,
            false
          );
        });
      }
    });

    await test.step(`Login as an attendee , open chat interaction panel and verify 50 elements renders in chat component`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await page.goto(attendeeMagicLink);
      await page.click("text=Enter Now", { timeout: 20000 });
      await page.waitForURL(/lobby/);
      await page.click("text=Skip");
      attendeeLobbyPage = new LobbyPage(page);
      await attendeeLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await attendeeLobbyPage.getChatComponent.verifyCountOfMessageMatches(40);
    });

    await test.step(`Add 10 more chat messages to the event chat`, async () => {
      for (let i = 0; i < 10; i++) {
        await test.step(`Posting message number : ${i + 1}`, async () => {
          await eventController.postEventChatMessage(
            `Hi, second phase, This is prateek from Automation message no: ${
              i + 1
            } `,
            false
          );
        });
      }
    });

    await test.step(`Verify attendee sees total 50 message in chat panel`, async () => {
      await attendeeLobbyPage.reload();
      await attendeeLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await attendeeLobbyPage.getChatComponent.verifyCountOfMessageMatches(50);
    });

    await test.step(`Add 10 more chat messages to the event chat`, async () => {
      for (let i = 0; i < 10; i++) {
        await test.step(`Posting message number : ${i + 1}`, async () => {
          await eventController.postEventChatMessage(
            `Hi, third phase, This is prateek from Automation message no: ${
              i + 1
            } `,
            false
          );
        });
      }
    });
    await test.step(`Verify attendee sees total 50 message in chat panel`, async () => {
      await attendeeLobbyPage.getChatComponent.verifyCountOfMessageMatches(50);
    });
    await test.step(`Verify the top 10 messages are not visible, and first message starts from count 11`, async () => {
      const firstMessageDivLocator =
        attendeeLobbyPage.getChatComponent.messageDiv.nth(0);
      await expect(firstMessageDivLocator).toContainText(
        "Hi, first phase, This is prateek from Automation message no: 11"
      );
    });
  });
});
