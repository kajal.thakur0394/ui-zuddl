import {
  APIRequestContext,
  BrowserContext,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../../controller/EventController";
import BrowserFactory from "../../../../util/BrowserFactory";
import { DataUtl } from "../../../../util/dataUtil";
import { updateEventLandingPageDetails } from "../../../../util/apiUtil";
import EventEntryType from "../../../../enums/eventEntryEnum";
import { LobbyPage } from "../../../../page-objects/events-pages/zones/LobbyPage";
import { ChatController } from "../../../../controller/ChatController";
import { StagePage } from "../../../../page-objects/events-pages/zones/StagePage";
import { AVModal } from "../../../../page-objects/events-pages/live-side-components/AvModal";
import { RoomsListingPage } from "../../../../page-objects/events-pages/zones/RoomsPage";
import ZoneType from "../../../../enums/ZoneTypeEnum";
import { TopNavBarComponent } from "../../../../page-objects/events-pages/live-side-components/TopNavBarComponent";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel(`@chat @pinned-chat`, async () => {
  //test.describe.configure({ retries: 2 });
  let eventId;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventController: EventController;
  let attendeeEmail: string;
  let speakerEmail: string;
  let attendeeLobbyPage: LobbyPage;
  let chatController: ChatController;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});

    orgApiContext = orgBrowserContext.request;
    await test.step(`Create a new event`, async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
      });
      console.log(`event id is ${eventId}`);
    });
    eventController = new EventController(orgApiContext, eventId);
    chatController = new ChatController(orgApiContext, eventId);

    await test.step(`Enable magic link for this event`, async () => {
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
        isSpeakermagicLinkEnabled: true,
      });
    });

    await test.step(`Invite new attendee to the event`, async () => {
      attendeeEmail = DataUtl.getRandomAttendeeEmail();
      await eventController.inviteAttendeeToTheEvent(attendeeEmail);
    });

    await test.step(`Invite new speaker to the event`, async () => {
      speakerEmail = DataUtl.getRandomSpeakerEmail();
      await eventController.inviteSpeakerToTheEventByApi(
        speakerEmail,
        "speakerName",
        "speakerLastName"
      );
    });

    await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
      false
    );
  });
  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`TC001: Verify presence of pinned chat UI component on different zones`, async ({}) => {
    test.info().annotations.push({
      type: "P0",
      description:
        "https://linear.app/zuddl/issue/QAT-343/verify-visibility-of-pinned-chat-message-component-on-different-zones",
    });
    let orgPage = await orgBrowserContext.newPage();
    let page = await (
      await BrowserFactory.getBrowserContext({ laodOrganiserCookies: false })
    ).newPage();
    let speakerPage = await (
      await BrowserFactory.getBrowserContext({ laodOrganiserCookies: false })
    ).newPage();
    let attendeeLobbyPage: LobbyPage;
    let speakerLobbyPage: LobbyPage;
    let organiserLobbyPage: LobbyPage;
    let lobbyUrl: string;
    let speakerAvModal: AVModal;
    let orgAvModal: AVModal;
    let attendeeAvModal: AVModal;
    let orgNavBar: TopNavBarComponent;
    let speakerNavBar: TopNavBarComponent;
    let attendeeNavBar: TopNavBarComponent;

    await test.step(`Create an event with rooms, expo, and schedule session`, async () => {
      await eventController.eventCreationWithMultipleVenueSetup();
    });

    await test.step(`Attendee fetches attendee magic link and login to event and go to lobby`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await page.goto(attendeeMagicLink);
      await page.click("text=Enter Now");
      await page.waitForURL(/lobby/);
      await page.click("text=Skip");
      attendeeLobbyPage = new LobbyPage(page);
    });

    await test.step(`Speaker fetches Speaker magic link and login to event and go to lobby`, async () => {
      const speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );
      await speakerPage.goto(speakerMagicLink);
      const enterNowButton = speakerPage.locator("text=Enter Now");
      await expect(enterNowButton).toBeVisible();
      await enterNowButton.click();
      await speakerPage.click("text=Lobby");
      await speakerPage.waitForURL(/lobby/);
      await speakerPage.click("text=Skip");
      speakerLobbyPage = new LobbyPage(speakerPage);
    });
    await test.step(`Organiser goes to the lobby`, async () => {
      lobbyUrl = page.url();
      await orgPage.goto(lobbyUrl);
      await orgPage.waitForURL(/lobby/);
      organiserLobbyPage = new LobbyPage(orgPage);
    });

    await test.step(`Verify organiser/speaker/attendee are able to see/unsee pinned chat component in Lobby zone`, async () => {
      await attendeeLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await attendeeLobbyPage.getChatComponent.verifyPinChatUIComponenttobeInvisible();
      await speakerLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await speakerLobbyPage.getChatComponent.verifyPinChatUIComponenttobeInvisible();
      await organiserLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await organiserLobbyPage.getChatComponent.verifyPinChatUIComponenttobeVisible();
    });

    await test.step(`Verify organiser/speaker/attendee are able to see/unsee pinned chat component in Schedule zone`, async () => {
      await page.locator("text=Schedule").click();
      await attendeeLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await attendeeLobbyPage.getChatComponent.verifyPinChatUIComponenttobeInvisible();
      await speakerPage.locator("text=Schedule").click();
      await speakerLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await speakerLobbyPage.getChatComponent.verifyPinChatUIComponenttobeInvisible();
      await orgPage.locator("text=Schedule").click();
      await organiserLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await organiserLobbyPage.getChatComponent.verifyPinChatUIComponenttobeVisible();
    });

    await test.step(`Verify organiser/speaker/attendee are able to see/unsee pinned chat component in Stage zone`, async () => {
      await page.locator("text=Stage").click();
      await attendeeLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await attendeeLobbyPage.getChatComponent.verifyPinChatUIComponenttobeInvisible();
      await speakerPage.locator("text=Stage").click();
      await speakerLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await speakerLobbyPage.getChatComponent.verifyPinChatUIComponenttobeInvisible();
      await orgPage.locator("text=Stage").click();
      await organiserLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await organiserLobbyPage.getChatComponent.verifyPinChatUIComponenttobeVisible();
    });

    await test.step(`Verify organiser/speaker/attendee are able to see/unsee pinned chat component in Rooms zone`, async () => {
      await test.step(`Attendee switches to ROOMS zone and verifies invisibility of Pinned chat component`, async () => {
        attendeeAvModal = new AVModal(page);
        attendeeNavBar = new TopNavBarComponent(page);
        let attendeeZonePage = new RoomsListingPage(page);
        await attendeeNavBar.switchToZone(ZoneType.ROOMS);
        await page.waitForTimeout(3000);
        await attendeeAvModal.clickOnJoinButtonOnAvModal();
        await test.step(`Attendee opens the Event Tab`, async () => {
          await attendeeZonePage.getChatComponent.clickOnEventTab();
        });
        await attendeeLobbyPage.getChatComponent.verifyPinChatUIComponenttobeInvisible();
        await attendeeZonePage.exitTheRoom();
      });

      await test.step(`Speaker switches to ROOMS zone and verifies invisibility of Pinned chat component`, async () => {
        speakerAvModal = new AVModal(speakerPage);
        speakerNavBar = new TopNavBarComponent(speakerPage);
        let speakerZonePage = new RoomsListingPage(speakerPage);
        await speakerNavBar.switchToZone(ZoneType.ROOMS);
        await speakerPage.waitForTimeout(3000);
        await speakerAvModal.clickOnJoinButtonOnAvModal();
        await test.step(`Speaker opens the Event Tab`, async () => {
          await speakerZonePage.getChatComponent.clickOnEventTab();
        });
        await speakerLobbyPage.getChatComponent.verifyPinChatUIComponenttobeInvisible();
        await speakerZonePage.exitTheRoom();
      });

      await test.step(`Organiser switches to ROOMS zone and verifies invisibility of Pinned chat component`, async () => {
        orgAvModal = new AVModal(orgPage);
        let orgZonePage = new RoomsListingPage(orgPage);
        orgNavBar = new TopNavBarComponent(orgPage);
        await orgNavBar.switchToZone(ZoneType.ROOMS);
        await orgPage.waitForTimeout(3000);
        await orgAvModal.clickOnJoinButtonOnAvModal();
        await test.step(`Organiser opens the Event Tab`, async () => {
          await orgZonePage.getChatComponent.clickOnEventTab();
        });
        await organiserLobbyPage.getChatComponent.verifyPinChatUIComponenttobeVisible();
        await orgZonePage.exitTheRoom();
      });
    });

    await test.step(`Verify organiser/speaker/attendee are able to see/unsee pinned chat component in Expo zone`, async () => {
      await page.locator("text=Expo").click();
      await attendeeLobbyPage.getChatComponent.verifyPinChatUIComponenttobeInvisible();
      await speakerPage.locator("text=Expo").click();
      await speakerLobbyPage.getChatComponent.verifyPinChatUIComponenttobeInvisible();
      await orgPage.locator("text=Expo").click();
      await organiserLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await organiserLobbyPage.getChatComponent.verifyPinChatUIComponenttobeVisible();
    });

    await test.step(`Verify organiser/speaker/attendee are able to see/unsee pinned chat component in Networking zone`, async () => {
      await page.locator("text=Networking").click();
      await attendeeLobbyPage.getChatComponent.verifyPinChatUIComponenttobeInvisible();
      await speakerPage.locator("text=Networking").click();
      await speakerLobbyPage.getChatComponent.verifyPinChatUIComponenttobeInvisible();
      await orgPage.locator("text=Networking").click();
      await organiserLobbyPage.getChatComponent.verifyPinChatUIComponenttobeVisible();
    });
  });

  test(`TC002: Verify organiser can pin his own chat as well as chat by an attendee`, async ({
    page,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-345/verify-organiser-is-able-to-pin-his-own-chat-as-well-as-attendee-chat",
    });
    let orgPage = await orgBrowserContext.newPage();
    let attendeeLobbyPage: LobbyPage;
    let organiserLobbyPage: LobbyPage;
    let lobbyUrl: string;
    const organiserSentMessage = "Hi I am org";
    const attendeeSentMessage = "Hi I am attendee";
    await test.step(`Attendee  fetches attendee magic link and login to event and go to lobby`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await page.goto(attendeeMagicLink);
      await page.click("text=Enter Now");
      await page.waitForURL(/lobby/);
      await page.click("text=Skip");
      attendeeLobbyPage = new LobbyPage(page);
    });

    await test.step(`Attende opens the chat component`, async () => {
      await attendeeLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await attendeeLobbyPage.getChatComponent.sendChatMessage(
        attendeeSentMessage
      );
      lobbyUrl = page.url();
    });

    await test.step(`Organiser goes to the lobby and open chat panel`, async () => {
      await orgPage.goto(lobbyUrl);
      // await orgPage.click("text=Enter Now");
      await orgPage.waitForURL(/lobby/);
      organiserLobbyPage = new LobbyPage(orgPage);
      await organiserLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
    });

    await test.step(`Organiser sends a chat message`, async () => {
      await organiserLobbyPage.getChatComponent.sendChatMessage(
        organiserSentMessage
      );
    });

    await test.step(`Verify organiser is able to pin his own chat message`, async () => {
      await organiserLobbyPage.getChatComponent.pinTheChatMessage(
        organiserSentMessage
      );
    });

    await test.step(`Verify organiser is able to pin attendee's chat message`, async () => {
      await organiserLobbyPage.getChatComponent.pinTheChatMessage(
        attendeeSentMessage
      );
    });

    await test.step(`Organiser and attendee expands the pin chat section`, async () => {
      await organiserLobbyPage.getChatComponent.clickToExpandOrCollapsePinnedChatSection();
      await attendeeLobbyPage.getChatComponent.clickToExpandOrCollapsePinnedChatSection();
    });

    await test.step(`Organiser verify both pinned messages are visible on pinned chat section`, async () => {
      await organiserLobbyPage.getChatComponent.verifyIfPinnedChatContainerContainsExpectedNumberOfMessages(
        2
      );
      expect(
        await organiserLobbyPage.getChatComponent.verifyMessageWithGivenContentIsVisibleInPinnedContainer(
          organiserSentMessage
        )
      ).toBe(true);
      expect(
        await organiserLobbyPage.getChatComponent.verifyMessageWithGivenContentIsVisibleInPinnedContainer(
          attendeeSentMessage
        )
      ).toBe(true);
    });

    await test.step(`Attendee verify both pinned messages are visible on pinned chat section`, async () => {
      await attendeeLobbyPage.getChatComponent.verifyIfPinnedChatContainerContainsExpectedNumberOfMessages(
        2
      );
      expect(
        await attendeeLobbyPage.getChatComponent.verifyMessageWithGivenContentIsVisibleInPinnedContainer(
          organiserSentMessage
        )
      ).toBe(true);
      expect(
        await attendeeLobbyPage.getChatComponent.verifyMessageWithGivenContentIsVisibleInPinnedContainer(
          attendeeSentMessage
        )
      ).toBe(true);
    });
  });

  test(`TC003: Verify only 3 chat messages can be pinned at a time by organiser`, async ({
    page,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-337/verify-max-3-chat-messages-only-can-be-pinned",
    });
    let orgPage = await orgBrowserContext.newPage();

    let attendeeLobbyPage: LobbyPage;
    let organiserLobbyPage: LobbyPage;

    const lobbyUrl = `${
      DataUtl.getApplicationTestDataObj()["baseUrl"]
    }/l/event/${eventId}/lobby`;
    const organiserSentMessageOne = "Message 1, Hi I am org";
    const organiserSentMessageTwo = "Message 2, Hi I am org";
    const organiserSentMessageThree = "Message 3, Hi I am org";
    const organiserSentMessageFour = "Message 4, Hi I am org";
    const listOfMessagesToSent = [
      organiserSentMessageOne,
      organiserSentMessageTwo,
      organiserSentMessageThree,
      organiserSentMessageFour,
    ];

    await test.step(`Organiser using API send list of message in event chat`, async () => {
      for (const eachMessage of listOfMessagesToSent) {
        await eventController.postEventChatMessage(eachMessage, false);
      }
    });
    await test.step(`Organiser goes to the lobby and open chat panel`, async () => {
      await orgPage.goto(lobbyUrl);
      // await orgPage.click("text=Enter Now");
      await orgPage.waitForURL(/lobby/);
      organiserLobbyPage = new LobbyPage(orgPage);
      await organiserLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
    });

    await test.step(`Organiser pins first three chat messages from the list`, async () => {
      await organiserLobbyPage.getChatComponent.pinTheChatMessage(
        listOfMessagesToSent[0]
      );
      await organiserLobbyPage.getChatComponent.pinTheChatMessage(
        listOfMessagesToSent[1]
      );
      await organiserLobbyPage.getChatComponent.pinTheChatMessage(
        listOfMessagesToSent[2]
      );
    });

    await test.step(`Now verify organiser when hovers over 4th messsage, in dropdown, no pin chat option apears`, async () => {
      await organiserLobbyPage.getChatComponent.verifyPinOptinIsDisabledForThisMessage(
        listOfMessagesToSent[3]
      );
    });
  });

  test(`TC004: verify organiser is able to delete his own pinned chat as well as attendee's pinned chat`, async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-347/verify-organiser-is-able-to-delete-his-own-pinned-chat-as-well-as",
    });

    let orgPage = await orgBrowserContext.newPage();
    let attendeeLobbyPage: LobbyPage;
    let organiserLobbyPage: LobbyPage;
    let lobbyUrl: string;
    const organiserSentMessage = "Hi I am org";
    const attendeeSentMessage = "Hi I am attendee";
    await test.step(`Attendee  fetches attendee magic link and login to event and go to lobby`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await page.goto(attendeeMagicLink);
      await page.click("text=Enter Now");
      await page.waitForURL(/lobby/);
      await page.click("text=Skip");
      attendeeLobbyPage = new LobbyPage(page);
    });

    await test.step(`Attende opens the chat component`, async () => {
      await attendeeLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await attendeeLobbyPage.getChatComponent.sendChatMessage(
        attendeeSentMessage
      );
      lobbyUrl = page.url();
    });

    await test.step(`Organiser goes to the lobby and open chat panel`, async () => {
      await orgPage.goto(lobbyUrl);
      // await orgPage.click("text=Enter Now");
      await orgPage.waitForURL(/lobby/);
      organiserLobbyPage = new LobbyPage(orgPage);
      await organiserLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
    });

    await test.step(`Organiser sends a chat message`, async () => {
      await organiserLobbyPage.getChatComponent.sendChatMessage(
        organiserSentMessage
      );
    });

    await test.step(`Verify organiser is able to pin his own chat message`, async () => {
      await organiserLobbyPage.getChatComponent.pinTheChatMessage(
        organiserSentMessage
      );
    });

    await test.step(`Verify organiser is able to pin attendee's chat message`, async () => {
      await organiserLobbyPage.getChatComponent.pinTheChatMessage(
        attendeeSentMessage
      );
    });

    await test.step(`Organiser and attendee expands the pin chat section`, async () => {
      await organiserLobbyPage.getChatComponent.clickToExpandOrCollapsePinnedChatSection();
      await attendeeLobbyPage.getChatComponent.clickToExpandOrCollapsePinnedChatSection();
    });

    await test.step(`Organiser verify both pinned messages are visible on pinned chat section`, async () => {
      await organiserLobbyPage.getChatComponent.verifyIfPinnedChatContainerContainsExpectedNumberOfMessages(
        2
      );
      expect(
        await organiserLobbyPage.getChatComponent.verifyMessageWithGivenContentIsVisibleInPinnedContainer(
          organiserSentMessage
        )
      ).toBe(true);
      expect(
        await organiserLobbyPage.getChatComponent.verifyMessageWithGivenContentIsVisibleInPinnedContainer(
          attendeeSentMessage
        )
      ).toBe(true);
    });

    await test.step(`Attendee verify both pinned messages are visible on pinned chat section`, async () => {
      await attendeeLobbyPage.getChatComponent.verifyIfPinnedChatContainerContainsExpectedNumberOfMessages(
        2
      );
      expect(
        await attendeeLobbyPage.getChatComponent.verifyMessageWithGivenContentIsVisibleInPinnedContainer(
          organiserSentMessage
        )
      ).toBe(true);
      expect(
        await attendeeLobbyPage.getChatComponent.verifyMessageWithGivenContentIsVisibleInPinnedContainer(
          attendeeSentMessage
        )
      ).toBe(true);
    });

    await test.step(`Organiser deletes his own pinned message from main message container`, async () => {
      await organiserLobbyPage.getChatComponent.deleteMessageFromMainMessageContainer(
        organiserSentMessage
      );
    });

    await test.step(`Organiser verify now only 1 pinned message is visible on pinned chat container`, async () => {
      await organiserLobbyPage.getChatComponent.verifyIfPinnedChatContainerContainsExpectedNumberOfMessages(
        1
      );
      expect(
        await organiserLobbyPage.getChatComponent.verifyMessageWithGivenContentIsVisibleInPinnedContainer(
          organiserSentMessage
        )
      ).toBe(false);
      await organiserLobbyPage.getChatComponent.verifyCountOfMessageMatches(1);
    });

    await test.step(`Organiser verify now only 1 pinned message is visible on pinned chat container`, async () => {
      await attendeeLobbyPage.getChatComponent.verifyIfPinnedChatContainerContainsExpectedNumberOfMessages(
        1
      );
      expect(
        await attendeeLobbyPage.getChatComponent.verifyMessageWithGivenContentIsVisibleInPinnedContainer(
          organiserSentMessage
        )
      ).toBe(false);
      expect(
        await attendeeLobbyPage.getChatComponent.verifyMessageWithGivenContentIsVisibleInPinnedContainer(
          attendeeSentMessage
        )
      ).toBe(true);
      await attendeeLobbyPage.getChatComponent.verifyCountOfMessageMatches(1);
    });

    await test.step(`Organiser deletes attendee's  pinned message from main message container`, async () => {
      await organiserLobbyPage.getChatComponent.deleteMessageFromMainMessageContainer(
        attendeeSentMessage
      );
    });
    await test.step(`Organiser verify now 0 messages are visible on pinned chat container as well in main container`, async () => {
      await organiserLobbyPage.getChatComponent.verifyIfPinnedChatContainerContainsExpectedNumberOfMessages(
        0
      );
      expect(
        await organiserLobbyPage.getChatComponent.verifyMessageWithGivenContentIsVisibleInPinnedContainer(
          attendeeSentMessage
        )
      ).toBe(false);
      await organiserLobbyPage.getChatComponent.verifyEmptyChatContainerIsVisible();
    });

    await test.step(`Attendee verify now 0 messages are visible on pinned chat container as well in main container`, async () => {
      await attendeeLobbyPage.getChatComponent.verifyIfPinnedChatContainerContainsExpectedNumberOfMessages(
        0
      );

      expect(
        await attendeeLobbyPage.getChatComponent.verifyMessageWithGivenContentIsVisibleInPinnedContainer(
          attendeeSentMessage
        )
      ).toBe(false);
      await attendeeLobbyPage.getChatComponent.verifyEmptyChatContainerIsVisible();
    });
  });

  test(`TC005: Verify gifs can not be pinned from UI`, async ({ page }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-338/verify-gifs-do-not-get-option-to-be-pinned-from-ui",
    });
    let orgPage = await orgBrowserContext.newPage();

    let attendeeLobbyPage: LobbyPage;
    let organiserLobbyPage: LobbyPage;

    const lobbyUrl = `${
      DataUtl.getApplicationTestDataObj()["baseUrl"]
    }/l/event/${eventId}/lobby`;
    const gifIdContent = "ljuSksqL9j0yI";

    await test.step(`Organiser using API to send gif message in event chat`, async () => {
      await eventController.postGifInEventChat(gifIdContent, false);
    });

    await test.step(`Organiser goes to the lobby and open chat panel`, async () => {
      await orgPage.goto(lobbyUrl);
      // await orgPage.click("text=Enter Now");
      await orgPage.waitForURL(/lobby/);
      organiserLobbyPage = new LobbyPage(orgPage);
      await organiserLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
    });

    await test.step(`Verify organiser does not get pin option when hover over gif message`, async () => {
      await organiserLobbyPage.getChatComponent.verifyPinChatOptionVisiblityForMessageByIndex(
        0,
        false
      );
    });
  });

  test(`TC006: Verify chat messages if unpinned , does not impact their ordering`, async ({
    page,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-339/verify-if-chat-message-unpinned-the-ordering-of-message-does-not-get",
    });
    let orgPage = await orgBrowserContext.newPage();

    let attendeeLobbyPage: LobbyPage;
    let organiserLobbyPage: LobbyPage;
    let lobbyUrl: string;
    //list of organiser messages to pin
    const organiserSentMessageOne = "Message 1, Hi I am org";
    const organiserSentMessageTwo = "Message 2, Hi I am org";
    const organiserSentMessageThree = "Message 3, Hi I am org";
    const listOfMessagesToSent = [
      organiserSentMessageOne,
      organiserSentMessageTwo,
      organiserSentMessageThree,
    ];

    let listOfMessageId = new Array();

    await test.step(`Attendee  fetches attendee magic link and login to event and go to lobby`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await page.goto(attendeeMagicLink);
      await page.click("text=Enter Now");
      await page.waitForURL(/lobby/);
      await page.click("text=Skip");
      attendeeLobbyPage = new LobbyPage(page);
    });

    await test.step(`Organiser using API send list of messages in event chat`, async () => {
      for (const eachMessage of listOfMessagesToSent) {
        const messageId =
          await eventController.postEventChatMessageAndGetMessageId(
            eachMessage,
            false
          );
        listOfMessageId.push(messageId);
      }
    });

    await test.step(`Organiser pins all his 3 chat messages one by one`, async () => {
      for (const eachMessageId of listOfMessageId) {
        await chatController.triggerApiToPinChatMessage(eachMessageId);
      }
    });

    await test.step(`Attende opens the chat component`, async () => {
      await attendeeLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      lobbyUrl = page.url();
    });

    await test.step(`Organiser goes to the lobby and open chat panel`, async () => {
      await orgPage.goto(lobbyUrl);
      // await orgPage.click("text=Enter Now");
      await orgPage.waitForURL(/lobby/);
      organiserLobbyPage = new LobbyPage(orgPage);
      await organiserLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
    });

    await test.step(`Organiser and attendee expands the pin chat section`, async () => {
      await organiserLobbyPage.getChatComponent.clickToExpandOrCollapsePinnedChatSection();
      await attendeeLobbyPage.getChatComponent.clickToExpandOrCollapsePinnedChatSection();
    });

    await test.step(`Organiser un-pins all his 3 chat messages one by one`, async () => {
      for (const eachMessageId of listOfMessageId) {
        await chatController.triggerApiToUnPinChatMessage(eachMessageId);
      }
    });

    await test.step(`Organiser verifies that un-pinning of message has not impacted the message ordering `, async () => {
      for (let i = 0; i < listOfMessagesToSent.length; i++) {
        await test.step(`Verify message content at order ${i} matches the expected message body for organiser`, async () => {
          const expMessageContent = listOfMessagesToSent[i];
          const actualMessageContent =
            organiserLobbyPage.getChatComponent.messageDiv.nth(i);
          await expect(
            actualMessageContent,
            `expecting message content from order ${i} in message container to be ${expMessageContent}`
          ).toContainText(expMessageContent);
        });
      }
    });
    await test.step(`Attendee verifies that un-pinning of message has not impacted the message ordering `, async () => {
      for (let i = 0; i < listOfMessagesToSent.length; i++) {
        await test.step(`Verify message content at order ${i} matches the expected message body for attendee`, async () => {
          const expMessageContent = listOfMessagesToSent[i];
          const actualMessageContent =
            attendeeLobbyPage.getChatComponent.messageDiv.nth(i);
          await expect(
            actualMessageContent,
            `expecting message content from order ${i} in message container to be ${expMessageContent}`
          ).toContainText(expMessageContent);
        });
      }
    });
  });

  test(`TC007: Verify pinned chat message components like, profile pic, time, content`, async ({
    page,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "P0",
      description:
        "https://linear.app/zuddl/issue/QAT-360/verify-pinned-chat-message-components-like-profile-pic-time-content",
    });
    let orgPage = await orgBrowserContext.newPage();

    let attendeeLobbyPage: LobbyPage;
    let organiserLobbyPage: LobbyPage;
    let lobbyUrl: string;
    const organiserSentMessage = "Hi I am organiser";
    const attendeeSentMessage = "Hi I am attendee";
    let attendeeContentDetails: { [key: string]: string };
    let organiserContentDetails: { [key: string]: string };

    await test.step(`Attendee fetches attendee magic link and login to event and go to lobby`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await page.goto(attendeeMagicLink);
      await page.click("text=Enter Now");
      await page.waitForURL(/lobby/);
      await page.click("text=Skip");
      attendeeLobbyPage = new LobbyPage(page);
    });

    await test.step(`Attendee opens the chat component and sends chat`, async () => {
      await attendeeLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await attendeeLobbyPage.getChatComponent.sendChatMessage(
        attendeeSentMessage
      );
      lobbyUrl = page.url();
    });

    await test.step(`Organiser using API sends message in event chat`, async () => {
      const messageId =
        await eventController.postEventChatMessageAndGetMessageId(
          organiserSentMessage,
          false
        );
    });

    await test.step(`Organiser goes to the lobby and open chat panel`, async () => {
      await orgPage.goto(lobbyUrl);
      await orgPage.waitForURL(/lobby/);
      organiserLobbyPage = new LobbyPage(orgPage);
      await organiserLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
    });

    await test.step(`Fetch attendee/organiser chat content details`, async () => {
      organiserContentDetails =
        await organiserLobbyPage.getChatComponent.fetchOrganiserContent();
      attendeeContentDetails =
        await attendeeLobbyPage.getChatComponent.fetchAttendeeContent();
    });

    await test.step(`Verify organiser is able to pin his own chat message`, async () => {
      await organiserLobbyPage.getChatComponent.pinTheChatMessage(
        organiserSentMessage
      );
    });

    await test.step(`Verify organiser is able to pin attendee's chat message`, async () => {
      await organiserLobbyPage.getChatComponent.pinTheChatMessage(
        attendeeSentMessage
      );
    });

    await test.step(`Organiser and attendee expands the pin chat section`, async () => {
      await organiserLobbyPage.getChatComponent.clickToExpandOrCollapsePinnedChatSection();
      await attendeeLobbyPage.getChatComponent.clickToExpandOrCollapsePinnedChatSection();
    });

    await test.step(`Organiser verify both pinned messages are visible on pinned chat section`, async () => {
      await organiserLobbyPage.getChatComponent.verifyIfPinnedChatContainerContainsExpectedNumberOfMessages(
        2
      );
      expect(
        await organiserLobbyPage.getChatComponent.verifyMessageWithGivenContentIsVisibleInPinnedContainer(
          organiserSentMessage
        )
      ).toBe(true);
      expect(
        await organiserLobbyPage.getChatComponent.verifyMessageWithGivenContentIsVisibleInPinnedContainer(
          attendeeSentMessage
        )
      ).toBe(true);
    });
    await test.step(`Verify organiser is able to see correct contents in pin chat`, async () => {
      await organiserLobbyPage.getChatComponent.verifyAllOrganiserContentsVisibleInPinnedContainer(
        organiserContentDetails
      );
    });

    await test.step(`Attendee verify both pinned messages are visible on pinned chat section`, async () => {
      await attendeeLobbyPage.getChatComponent.verifyIfPinnedChatContainerContainsExpectedNumberOfMessages(
        2
      );
      expect(
        await attendeeLobbyPage.getChatComponent.verifyMessageWithGivenContentIsVisibleInPinnedContainer(
          organiserSentMessage
        )
      ).toBe(true);
      expect(
        await attendeeLobbyPage.getChatComponent.verifyMessageWithGivenContentIsVisibleInPinnedContainer(
          attendeeSentMessage
        )
      ).toBe(true);
    });
    await test.step(`Verify attendee is able to see correct contents in pin chat`, async () => {
      await attendeeLobbyPage.getChatComponent.verifyAllAttendeeContentsVisibleInPinnedContainer(
        attendeeContentDetails
      );
    });
  });

  test(`TC008: verify attendee/speaker do not have option to pin/unpin a chat message`, async ({
    page,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "P0",
      description:
        "https://linear.app/zuddl/issue/QAT-336/verify-attendeespeaker-do-not-have-option-to-pinunpin-a-chat-message",
    });
    let orgPage = await orgBrowserContext.newPage();
    let speakerBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    let stagePage: StagePage;
    let speakerPage = await speakerBrowserContext.newPage();

    let attendeeLobbyPage: LobbyPage;
    let organiserLobbyPage: LobbyPage;
    let speakerLobbyPage: LobbyPage;
    let lobbyUrl: string;
    const organiserSentMessage = "Hi I am organiser";
    const attendeeSentMessage = "Hi I am attendee";
    const speakerSentMessage = "Hi I am speaker";
    let attendeeContentDetails: { [key: string]: string };
    let organiserContentDetails: { [key: string]: string };

    await test.step(`Attendee fetches attendee magic link and login to event and go to lobby`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await page.goto(attendeeMagicLink);
      await page.click("text=Enter Now");
      await page.waitForURL(/lobby/);
      await page.click("text=Skip");
      attendeeLobbyPage = new LobbyPage(page);
    });

    await test.step(`Speaker fetches Speaker magic link and login to event and go to lobby`, async () => {
      const speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );
      //verify speaker can navigate to speaker magic link and sees enter now button
      await speakerPage.goto(speakerMagicLink);
      console.log(speakerPage.url());
      const enterNowButton = speakerPage.locator("text=Enter Now");
      await expect(enterNowButton).toBeVisible();
      await enterNowButton.click();
      stagePage = new StagePage(speakerPage);
      await stagePage.closeWelcomeStagePopUp();
      await speakerPage.click("text=Lobby");
      await speakerPage.waitForURL(/lobby/);
      await speakerPage.click("text=Skip");
      speakerLobbyPage = new LobbyPage(speakerPage);
    });

    await test.step(`Attendee opens the chat component and sends chat`, async () => {
      await attendeeLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await attendeeLobbyPage.getChatComponent.sendChatMessage(
        attendeeSentMessage
      );
      lobbyUrl = page.url();
    });

    await test.step(`verify attendee does not see option of pin message`, async () => {
      await attendeeLobbyPage.getChatComponent.verifyPinMessageComponentTobeInvisible(
        attendeeSentMessage
      );
    });

    await test.step(`Speaker opens the chat component and sends chat`, async () => {
      await speakerLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await speakerLobbyPage.getChatComponent.sendChatMessage(
        speakerSentMessage
      );
      lobbyUrl = page.url();
    });

    await test.step(`verify speaker does not see option of pin message`, async () => {
      await attendeeLobbyPage.getChatComponent.verifyPinMessageComponentTobeInvisible(
        speakerSentMessage
      );
    });

    await test.step(`Organiser goes to the lobby and open chat panel`, async () => {
      await orgPage.goto(lobbyUrl);
      await orgPage.waitForURL(/lobby/);
      organiserLobbyPage = new LobbyPage(orgPage);
      await organiserLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
    });

    await test.step(`Organiser pins the attendee message`, async () => {
      await organiserLobbyPage.getChatComponent.pinTheChatMessage(
        attendeeSentMessage
      );
    });

    await test.step(`Verify attendee could not see/do unpin message`, async () => {
      //expand the component and verify
      await attendeeLobbyPage.getChatComponent.clickToExpandOrCollapsePinnedChatSection();
      await attendeeLobbyPage.getChatComponent.verifyPinMessageComponentTobeInvisible(
        attendeeSentMessage
      );
      await organiserLobbyPage.getChatComponent.unpinTheChatMessage(
        attendeeSentMessage
      );
    });

    await test.step(`Organiser pins the speaker message`, async () => {
      await organiserLobbyPage.getChatComponent.pinTheChatMessage(
        speakerSentMessage
      );
    });

    await test.step(`Verify speaker could not see/do unpin message component`, async () => {
      //expand the component and verify
      await speakerLobbyPage.getChatComponent.clickToExpandOrCollapsePinnedChatSection();
      await speakerLobbyPage.getChatComponent.verifyPinMessageComponentTobeInvisible(
        speakerSentMessage
      );
      await organiserLobbyPage.getChatComponent.unpinTheChatMessage(
        speakerSentMessage
      );
    });
  });

  test(`TC009: verify count of pinned chat message get correct update as per user action on events`, async ({
    page,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-348/verify-count-of-pinned-chat-message-get-correct-update-as-per-user",
    });
    let orgPage = await orgBrowserContext.newPage();

    let attendeeLobbyPage: LobbyPage;
    let organiserLobbyPage: LobbyPage;
    let lobbyUrl: string;
    //list of organiser messages to pin
    const organiserSentMessageOne = "Message 1, Hi I am org";
    const organiserSentMessageTwo = "Message 2, Hi I am org";
    const organiserSentMessageThree = "Message 3, Hi I am org";
    const listOfMessagesToSent = [
      organiserSentMessageOne,
      organiserSentMessageTwo,
      organiserSentMessageThree,
    ];

    let listOfMessageId = new Array();

    await test.step(`Attendee  fetches attendee magic link and login to event and go to lobby`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await page.goto(attendeeMagicLink);
      await page.click("text=Enter Now");
      await page.waitForURL(/lobby/);
      await page.click("text=Skip");
      attendeeLobbyPage = new LobbyPage(page);
    });

    await test.step(`Organiser using API send list of messages in event chat`, async () => {
      for (const eachMessage of listOfMessagesToSent) {
        const messageId =
          await eventController.postEventChatMessageAndGetMessageId(
            eachMessage,
            false
          );
        listOfMessageId.push(messageId);
      }
    });

    await test.step(`Organiser pins all his 3 chat messages one by one`, async () => {
      for (const eachMessageId of listOfMessageId) {
        await chatController.triggerApiToPinChatMessage(eachMessageId);
      }
    });

    await test.step(`Attende opens the chat component`, async () => {
      await attendeeLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      lobbyUrl = page.url();
    });

    await test.step(`Organiser goes to the lobby and open chat panel`, async () => {
      await orgPage.goto(lobbyUrl);
      // await orgPage.click("text=Enter Now");
      await orgPage.waitForURL(/lobby/);
      organiserLobbyPage = new LobbyPage(orgPage);
      await organiserLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
    });

    await test.step(`Organiser and attendee expands the pin chat section`, async () => {
      await organiserLobbyPage.getChatComponent.clickToExpandOrCollapsePinnedChatSection();
      await attendeeLobbyPage.getChatComponent.clickToExpandOrCollapsePinnedChatSection();
    });

    await test.step(`Organiser verifies he sees pinned chat count as 3 on pin chat container`, async () => {
      await organiserLobbyPage.getChatComponent.verifyPinnedChatCountIndicatorMatches(
        3
      );
    });

    await test.step(`Attendee verifies he sees pinned chat count as 3 on pin chat container`, async () => {
      await attendeeLobbyPage.getChatComponent.verifyPinnedChatCountIndicatorMatches(
        3
      );
    });

    await test.step(`Organiser un-pins all his 3 chat messages one by one`, async () => {
      for (const eachMessageId of listOfMessageId) {
        await chatController.triggerApiToUnPinChatMessage(eachMessageId);
      }
    });

    await test.step(`Organiser verifies he sees pinned chat count as 0 on pin chat container`, async () => {
      await organiserLobbyPage.getChatComponent.verifyPinnedChatCountIndicatorMatches(
        0
      );
    });

    await test.step(`Attendee verifies he sees pinned chat count as 0 on pin chat container`, async () => {
      await attendeeLobbyPage.getChatComponent.verifyPinnedChatCountIndicatorMatches(
        0
      );
    });
  });
});
