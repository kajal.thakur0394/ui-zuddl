import {
  APIRequestContext,
  BrowserContext,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../../controller/EventController";
import BrowserFactory from "../../../../util/BrowserFactory";
import { DataUtl } from "../../../../util/dataUtil";
import {
  getAccountIdForLoggedInContext,
  updateEventLandingPageDetails,
} from "../../../../util/apiUtil";
import EventEntryType from "../../../../enums/eventEntryEnum";
import { LobbyPage } from "../../../../page-objects/events-pages/zones/LobbyPage";
import { StagePage } from "../../../../page-objects/events-pages/zones/StagePage";
import { EventInfoDTO } from "../../../../dto/eventInfoDto";
import { async } from "node-ical";
import { StageController } from "../../../../controller/StageController";
import { PeopleController } from "../../../../controller/PeopleController";
import { SchedulePage } from "../../../../page-objects/events-pages/zones/SchedulePage";
import { ExpoPage } from "../../../../page-objects/events-pages/zones/ExpoPage";
import { UserProfileController } from "../../../../controller/UserProfileController";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel(`@private-chat`, async () => {
  test.slow();
  let eventId: any;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventController: EventController;
  let eventTitle: string;
  let attendeeLandingPage: string;
  let defaultStageId: string;
  let peopleController: PeopleController;
  let boothId: string;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiContext = orgBrowserContext.request;
    eventTitle = DataUtl.getRandomEventTitle();

    await test.step(`Create an event with rooms, expo, and schedule session`, async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: eventTitle,
      });
      eventController = new EventController(orgApiContext, eventId);
      await eventController.eventCreationWithMultipleVenueSetup();
      defaultStageId = await eventController.getDefaultStageId();
      peopleController = new PeopleController(orgApiContext, eventId);
      let boothList = await eventController.getExpoController.getAllBooths();
      boothId = boothList[0]["boothId"];
      console.log("here");
    });

    await test.step("Handle onboarding check", async () => {
      await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
        false
      );
      await eventController.getAccessGroupController.handleOnboardingChecksForSpeaker(
        false
      );
    });

    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });
    let eventInfoDto = new EventInfoDTO(eventId, eventTitle, true);
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test("TC001: verify chat between Attendee1 -> Attendee2 in LOBBY", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-504/verify-chat-between-attendee-attendee",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-160/private-11-chat",
    });

    let attendeeOneBrowserContext: BrowserContext;
    let attendeeTwoBrowserContext: BrowserContext;
    attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    attendeeTwoBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let attendeeTwoPage = await attendeeTwoBrowserContext.newPage();

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const attendeeTwoFirstName = "prateekAttendeeTwo";
    const attendeeTwoLastName = "QA";
    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let attendeeEmailTwo = DataUtl.getRandomAttendeeEmail();
    let attendeOneExpoPage: LobbyPage;
    let attendeTwoExpoPage: LobbyPage;
    const messageAttendeeOne = "This is attendee1 message";
    const messageAttendeeTwo = "This is attendee2 message";

    await test.step(`organiser invites attendee 1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await test.step(`organiser invites attendee 2 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeTwoFirstName,
        lastName: attendeeTwoLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailTwo,
      });
    });

    await test.step(`attendee 1 logs into the LOBBY`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      //click on skip button to close edit profile popup
      await attendeeOnePage.locator("button:has-text('Skip')").click();
    });

    await test.step(`attendee 2 logs into the LOBBY`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailTwo
      );
      await attendeeTwoPage.goto(magicLink);
      await attendeeTwoPage.click(`text=Enter Now`);
      await attendeeTwoPage.waitForURL(/lobby/);
      //click on skip button to close edit profile popup
      await attendeeTwoPage.locator("button:has-text('Skip')").click();
    });

    await test.step(`attendee 1 opens chat component`, async () => {
      attendeOneExpoPage = new LobbyPage(attendeeOnePage);
      await attendeOneExpoPage.clickOnChatIconOfClosedInteractionPanelState();
      await attendeOneExpoPage.clickOnPeopleTabOnInteractionPanel();
    });

    await test.step(`attendee 2 opens chat component`, async () => {
      attendeTwoExpoPage = new LobbyPage(attendeeTwoPage);
      await attendeTwoExpoPage.clickOnChatIconOfClosedInteractionPanelState();
      await attendeTwoExpoPage.clickOnPeopleTabOnInteractionPanel();
    });

    await test.step(`attendee 1 opens private chat of attendee 2`, async () => {
      await attendeOneExpoPage.getPeopleComponent.searchForPeople(
        attendeeTwoFirstName
      );
      await attendeOneExpoPage.getPeopleComponent.clickOnPrivateChatIcon(
        attendeeTwoFirstName
      );
    });

    await test.step(`attendee 2 opens private chat of attendee 1`, async () => {
      await attendeTwoExpoPage.getPeopleComponent.searchForPeople(
        attendeeOneFirstName
      );
      await attendeTwoExpoPage.getPeopleComponent.clickOnPrivateChatIcon(
        attendeeOneFirstName
      );
    });

    await test.step(`Now, attendee1 sends message->${messageAttendeeOne} to attendee2`, async () => {
      await attendeOneExpoPage.getChatComponent.sendChatMessage(
        messageAttendeeOne
      );
      await attendeOneExpoPage.getChatComponent.verifyMessageWithGivenContentIsVisible(
        messageAttendeeOne
      );
    });

    await test.step(`Finally, attendee2 verifies message is received via UI`, async () => {
      await attendeTwoExpoPage.getChatComponent.verifyMessageWithGivenContentIsVisible(
        messageAttendeeOne
      );
    });

    await test.step(`Now, attendee2 sends message->${messageAttendeeTwo} to attendee1`, async () => {
      await attendeTwoExpoPage.getChatComponent.sendChatMessage(
        messageAttendeeTwo
      );
      await attendeTwoExpoPage.getChatComponent.verifyMessageWithGivenContentIsVisible(
        messageAttendeeTwo
      );
    });

    await test.step(`Finally, attendee1 verifies message is received via UI`, async () => {
      await attendeOneExpoPage.getChatComponent.verifyMessageWithGivenContentIsVisible(
        messageAttendeeTwo
      );
    });
  });

  test("TC002: verify chat between Organiser -> Attendee in STAGE", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-505/verify-chat-between-org-attendee-and-vice-versa",
    });
    let stageId: string;
    let attendeeOneBrowserContext: BrowserContext;
    let attendeeOneStage: StagePage;
    let organiserStage: StagePage;
    attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";
    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let attpeopleController: PeopleController;
    let orgpeopleController: PeopleController;
    let attendeeAccountId: string;
    let organiserAccountId: string;
    let stageChannelId: any;
    let attendeeMessageToSend = "attendeeHasMessaged";
    let organiserMessageToSend = "organiserHasMessaged";

    await test.step("Fetching default stage Id", async () => {
      stageId = await eventController.getDefaultStageId();
      stageChannelId = await new StageController(
        orgApiContext,
        eventId,
        stageId
      ).getChannelId();
      attpeopleController = new PeopleController(
        attendeeOneBrowserContext.request,
        eventId
      );
      orgpeopleController = new PeopleController(orgApiContext, eventId);
    });
    let stageUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/stages/${stageId}`;

    await test.step(`organiser invites attendee1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await test.step(`organiser logs into the STAGE`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(stageUrl);
      organiserStage = new StagePage(organiserPage);
      await organiserStage.closeWelcomeStagePopUp();
    });

    await test.step(`attendee logs into the STAGE`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      await attendeeOnePage.goto(stageUrl);
      attendeeOneStage = new StagePage(attendeeOnePage);
    });

    await test.step(`Now, both organiser and attendee open People section`, async () => {
      await attendeeOneStage.getChatComponent.clickOnEventTab();
      await organiserStage.getChatComponent.clickOnEventTab();
      await attendeeOneStage.clickOnPeopleTabOnInteractionPanel();
      await organiserStage.clickOnPeopleTabOnInteractionPanel();
    });

    await test.step(`organiser opens private chat of attendee`, async () => {
      await organiserStage.getPeopleComponent.searchForPeople(
        attendeeOneFirstName
      );
      await organiserStage.getPeopleComponent.clickOnPrivateChatIcon(
        attendeeOneFirstName
      );
    });

    await test.step(`organiser sends message to attendee via API`, async () => {
      attendeeAccountId = await getAccountIdForLoggedInContext(
        attendeeOneBrowserContext.request
      );
      await orgpeopleController.postMessageInPeopleEventChat(
        organiserMessageToSend,
        attendeeAccountId,
        stageChannelId
      );
    });

    await test.step(`Finally attendee verifies message via API`, async () => {
      organiserAccountId = await getAccountIdForLoggedInContext(orgApiContext);
      let chatList = await attpeopleController.getMessageListInPeopleEventChat(
        organiserAccountId,
        stageChannelId
      );
      expect(await chatList[0]["content"]).toEqual(organiserMessageToSend);
    });

    await test.step(`Now, attendee sends message to organiser via API`, async () => {
      let attendeeMessageToSend = "attendeeHasMessaged";
      await attpeopleController.postMessageInPeopleEventChat(
        attendeeMessageToSend,
        organiserAccountId,
        stageChannelId
      );
    });

    await test.step(`Finally organiser verifies message via API`, async () => {
      let chatList = await orgpeopleController.getMessageListInPeopleEventChat(
        attendeeAccountId,
        stageChannelId
      );
      expect(await chatList[1]["content"]).toEqual(attendeeMessageToSend);
    });
  });

  test("TC003: verify chat between Speaker -> Attendee in SCHEDULE", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-506/verify-chat-between-speaker-attendee-and-vice-versa",
    });

    let attendeeBrowserContext: BrowserContext;
    let speakerBrowserContext: BrowserContext;
    attendeeBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    speakerBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    let attendeePage = await attendeeBrowserContext.newPage();
    let speakerPage = await speakerBrowserContext.newPage();

    const attendeeFirstName = "prateekAttendee";
    const attendeeLastName = "QA";

    const speakerFirstName = "prateekSpeaker";
    const speakerLastName = "QA";
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let speakerEmail = DataUtl.getRandomSpeakerEmail();
    let attendeeSchedulePage: SchedulePage;
    let speakerSchedulePage: SchedulePage;
    const messageAttendee = "This is attendee message";
    const messageSpeaker = "This is speaker message";
    let scheduleUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/schedule`;

    await test.step(`organiser invites attendee to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeFirstName,
        lastName: attendeeLastName,
        phoneNumber: "1234567899",
        email: attendeeEmail,
      });
    });

    await test.step(`organiser invites speaker to the event`, async () => {
      await eventController.inviteSpeakerToTheEventByApi(
        speakerEmail,
        speakerFirstName,
        speakerLastName
      );
    });

    await test.step(`attendee logs into the SCHEDULE`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await attendeePage.goto(magicLink);
      await attendeePage.click(`text=Enter Now`);
      await attendeePage.waitForURL(/lobby/);
      //click on skip button to close edit profile popup
      await attendeePage.locator("button:has-text('Skip')").click();
      await attendeePage.goto(scheduleUrl);
    });

    await test.step(`speaker logs into the SCHEDULE`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );
      await speakerPage.goto(magicLink);
      await speakerPage.click(`text=Enter Now`);
      //click on skip button to close edit profile popup
      await speakerPage.locator("button:has-text('Skip')").click();
      await speakerPage.goto(scheduleUrl);
    });

    await test.step(`attendee opens chat component`, async () => {
      attendeeSchedulePage = new SchedulePage(attendeePage);
      await attendeeSchedulePage.clickOnChatIconOfClosedInteractionPanelState();
      await attendeeSchedulePage.clickOnPeopleTabOnInteractionPanel();
    });

    await test.step(`speaker opens chat component`, async () => {
      speakerSchedulePage = new SchedulePage(speakerPage);
      await speakerSchedulePage.clickOnChatIconOfClosedInteractionPanelState();
      await speakerSchedulePage.clickOnPeopleTabOnInteractionPanel();
    });

    await test.step(`attendee opens private chat of speaker`, async () => {
      await attendeeSchedulePage.getPeopleComponent.searchForPeople(
        speakerFirstName
      );
      await attendeeSchedulePage.getPeopleComponent.clickOnPrivateChatIcon(
        speakerFirstName
      );
    });

    await test.step(`speaker opens private chat of attendee`, async () => {
      await speakerSchedulePage.getPeopleComponent.searchForPeople(
        attendeeFirstName
      );
      await speakerSchedulePage.getPeopleComponent.clickOnPrivateChatIcon(
        attendeeFirstName
      );
    });

    await test.step(`Now, attendee sends message->${messageAttendee} to speaker`, async () => {
      await attendeeSchedulePage.getChatComponent.sendChatMessage(
        messageAttendee
      );
      await attendeeSchedulePage.getChatComponent.verifyMessageWithGivenContentIsVisible(
        messageAttendee
      );
    });

    await test.step(`Finally, speaker verifies message is received via UI`, async () => {
      await attendeeSchedulePage.getChatComponent.verifyMessageWithGivenContentIsVisible(
        messageAttendee
      );
    });

    await test.step(`Now, speaker sends message->${messageSpeaker} to attendee`, async () => {
      await speakerSchedulePage.getChatComponent.sendChatMessage(
        messageSpeaker
      );
      await speakerSchedulePage.getChatComponent.verifyMessageWithGivenContentIsVisible(
        messageSpeaker
      );
    });

    await test.step(`Finally, attendee verifies message is received via UI`, async () => {
      await speakerSchedulePage.getChatComponent.verifyMessageWithGivenContentIsVisible(
        messageSpeaker
      );
    });
  });

  test("TC004: verify chat between Organiser -> Speaker in LOBBY", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-507/verify-chat-between-org-speaker-and-vice-versa",
    });
    let speakerBrowserContext: BrowserContext;
    speakerBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    let organiserPage = await orgBrowserContext.newPage();
    let speakerPage = await speakerBrowserContext.newPage();

    const speakerFirstName = "prateekSpeaker";
    const speakerLastName = "QA";
    let speakerEmail = DataUtl.getRandomSpeakerEmail();
    let organiserLobbyPage: LobbyPage;
    let speakerLobbyPage: LobbyPage;
    const messageOrganiser = "This is organiser message";
    const messageSpeaker = "This is speaker message";
    let lobbyUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/lobby`;
    let speakerpeopleController = new PeopleController(
      speakerBrowserContext.request,
      eventId
    );
    let orgpeopleController = new PeopleController(orgApiContext, eventId);
    let channelId = await orgpeopleController.getChannelId(eventId);
    let speakerAccountId: string;
    let organiserAccountId: string;

    await test.step(`organiser invites speaker to the event`, async () => {
      await eventController.inviteSpeakerToTheEventByApi(
        speakerEmail,
        speakerFirstName,
        speakerLastName
      );
    });

    await test.step(`speaker logs into the LOBBY`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );
      await speakerPage.goto(magicLink);
      await speakerPage.click(`text=Enter Now`);
      //click on skip button to close edit profile popup
      await speakerPage.locator("button:has-text('Skip')").click();
      await speakerPage.goto(lobbyUrl);
    });

    await test.step(`organiser logs into the LOBBY`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(lobbyUrl);
    });

    await test.step(`speaker opens chat component`, async () => {
      speakerLobbyPage = new LobbyPage(speakerPage);
      await speakerLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await speakerLobbyPage.clickOnPeopleTabOnInteractionPanel();
    });

    await test.step(`organiser opens chat component`, async () => {
      organiserLobbyPage = new LobbyPage(organiserPage);
      await organiserLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await organiserLobbyPage.clickOnPeopleTabOnInteractionPanel();
    });

    await test.step(`organiser opens private chat of speaker`, async () => {
      await organiserLobbyPage.getPeopleComponent.searchForPeople(
        speakerFirstName
      );
      await organiserLobbyPage.getPeopleComponent.clickOnPrivateChatIcon(
        speakerFirstName
      );
    });

    await test.step(`organiser sends message to speaker via API`, async () => {
      speakerAccountId = await getAccountIdForLoggedInContext(
        speakerBrowserContext.request
      );
      await orgpeopleController.postMessageInPeopleEventChat(
        messageOrganiser,
        speakerAccountId,
        channelId
      );
    });

    await test.step(`Finally speaker verifies message via API`, async () => {
      organiserAccountId = await getAccountIdForLoggedInContext(orgApiContext);
      let chatList =
        await speakerpeopleController.getMessageListInPeopleEventChat(
          organiserAccountId,
          channelId
        );
      expect(await chatList[0]["content"]).toEqual(messageOrganiser);
    });

    await test.step(`Now, speaker sends message to organiser via API`, async () => {
      await speakerpeopleController.postMessageInPeopleEventChat(
        messageSpeaker,
        organiserAccountId,
        channelId
      );
    });

    await test.step(`Finally organiser verifies message via API`, async () => {
      let chatList = await orgpeopleController.getMessageListInPeopleEventChat(
        speakerAccountId,
        channelId
      );
      expect(await chatList[1]["content"]).toEqual(messageSpeaker);
    });
  });

  test("TC005: verify chat between booth moderator -> attendee in BOOTH", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-508/verify-chat-between-booth-moderator-attendee-and-vice-versa-inside",
    });

    let attendeeOneBrowserContext: BrowserContext;
    let attendeeTwoBrowserContext: BrowserContext;
    attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    attendeeTwoBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let attendeeTwoPage = await attendeeTwoBrowserContext.newPage();

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const boothOwnerFirstName = "prateekBoothOwner";
    const boothOwnerLastName = "QA";
    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let boothOwnerEmail = DataUtl.getRandomAttendeeEmail();
    let attendeeOneExpoPage: ExpoPage;
    let attendeeTwoExpoPage: ExpoPage;
    const messageAttendeeOne = "This is attendee1 message";
    const messageBoothOwner = "This is boothOwner message";
    let boothUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/booth/${boothId}`;

    await test.step(`organiser invites attendee 1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await test.step(`organiser invites attendee 2 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: boothOwnerFirstName,
        lastName: boothOwnerLastName,
        phoneNumber: "1234567899",
        email: boothOwnerEmail,
      });
    });

    await test.step(`attendee 1 logs into the BOOTH`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      //click on skip button to close edit profile popup
      await attendeeOnePage.locator("button:has-text('Skip')").click();
      await attendeeOnePage.goto(boothUrl);
    });

    await test.step(`attendee 2 logs into the BOOTH`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        boothOwnerEmail
      );
      await attendeeTwoPage.goto(magicLink);
      await attendeeTwoPage.click(`text=Enter Now`);
      await attendeeTwoPage.waitForURL(/lobby/);
      //click on skip button to close edit profile popup
      await attendeeTwoPage.locator("button:has-text('Skip')").click();
      await attendeeTwoPage.goto(boothUrl);
    });

    await test.step(`Now, organiser makes attendee2 ${boothOwnerEmail} as booth owner via API`, async () => {
      let attendeeAccountId = await getAccountIdForLoggedInContext(
        attendeeTwoBrowserContext.request
      );
      await eventController.getExpoController.addBoothOwnerInsideBooth(
        boothId,
        attendeeAccountId
      );
    });

    await test.step(`Now, attendee 1 opens chat component`, async () => {
      attendeeOneExpoPage = new ExpoPage(attendeeOnePage);
      await attendeeOneExpoPage.clickOnChatIconOfClosedInteractionPanelState();
      await attendeeOneExpoPage.getChatComponent.clickOnEventTab();
      await attendeeOneExpoPage.clickOnPeopleTabOnInteractionPanel();
    });

    await test.step(`boothOwner opens chat component`, async () => {
      attendeeTwoExpoPage = new ExpoPage(attendeeTwoPage);
      await attendeeTwoExpoPage.clickOnChatIconOfClosedInteractionPanelState();
      await attendeeTwoExpoPage.getChatComponent.clickOnEventTab();
      await attendeeTwoExpoPage.clickOnPeopleTabOnInteractionPanel();
    });

    await test.step(`attendee 1 opens private chat of booth Owner`, async () => {
      await attendeeOneExpoPage.getPeopleComponent.searchForPeople(
        boothOwnerFirstName
      );
      await attendeeOneExpoPage.getPeopleComponent.clickOnPrivateChatIcon(
        boothOwnerFirstName
      );
    });

    await test.step(`boothOwner opens private chat of attendee 1`, async () => {
      await attendeeTwoExpoPage.getPeopleComponent.searchForPeople(
        attendeeOneFirstName
      );
      await attendeeTwoExpoPage.getPeopleComponent.clickOnPrivateChatIcon(
        attendeeOneFirstName
      );
    });

    await test.step(`Now, attendee1 sends message->${messageAttendeeOne} to boothOwner`, async () => {
      await attendeeOneExpoPage.getChatComponent.sendChatMessage(
        messageAttendeeOne
      );
      await attendeeOneExpoPage.getChatComponent.verifyMessageWithGivenContentIsVisible(
        messageAttendeeOne
      );
    });

    await test.step(`Finally, boothOwner verifies message is received via UI`, async () => {
      await attendeeTwoExpoPage.getChatComponent.verifyMessageWithGivenContentIsVisible(
        messageAttendeeOne
      );
    });

    await test.step(`Now, boothOwner sends message->${messageBoothOwner} to attendee1`, async () => {
      await attendeeTwoExpoPage.getChatComponent.sendChatMessage(
        messageBoothOwner
      );
      await attendeeTwoExpoPage.getChatComponent.verifyMessageWithGivenContentIsVisible(
        messageBoothOwner
      );
    });

    await test.step(`Finally, attendee1 verifies message is received via UI`, async () => {
      await attendeeOneExpoPage.getChatComponent.verifyMessageWithGivenContentIsVisible(
        messageBoothOwner
      );
    });
  });

  test("TC006: verify if privacy feature has set by attendee to not to recieve the chat messages, then other attendee/org/speaker should not be able to send him the chat message", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-509/verify-if-privacy-feature-has-set-by-attendee-to-not-recieve-the-chat",
    });
    let attendeeOneBrowserContext: BrowserContext;
    let speakerBrowserContext: BrowserContext;
    attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    speakerBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let speakerPage = await speakerBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();
    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const speakerFirstName = "prateekSpeaker";
    const speakerLastName = "QA";
    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let speakerEmail = DataUtl.getRandomSpeakerEmail();
    let attendeOneLobbyPage: LobbyPage;
    let speakerLobbyPage: LobbyPage;
    let organiserLobbyPage: LobbyPage;
    let lobbyUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/lobby`;

    await test.step(`organiser invites attendee 1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await test.step(`attendee 1 logs into the LOBBY`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.waitForURL(/lobby/);
      //click on skip button to close edit profile popup
      await attendeeOnePage.locator("button:has-text('Skip')").click();
    });

    await test.step(`attendee 1 opens chat component`, async () => {
      attendeOneLobbyPage = new LobbyPage(attendeeOnePage);
      await attendeOneLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await attendeOneLobbyPage.clickOnPeopleTabOnInteractionPanel();
    });

    await test.step(`organiser invites speaker to the event`, async () => {
      await eventController.inviteSpeakerToTheEventByApi(
        speakerEmail,
        speakerFirstName,
        speakerLastName
      );
    });

    await test.step(`speaker logs into the LOBBY`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );
      await speakerPage.goto(magicLink);
      await speakerPage.click(`text=Enter Now`);
      //click on skip button to close edit profile popup
      await speakerPage.locator("button:has-text('Skip')").click();
      await speakerPage.goto(lobbyUrl);
      speakerLobbyPage = new LobbyPage(speakerPage);
    });

    await test.step(`organiser logs into the LOBBY`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(lobbyUrl);
      organiserLobbyPage = new LobbyPage(organiserPage);
    });

    await test.step(`Now, attendee disables the send private chat option via API`, async () => {
      const profileDetailsPayload = {
        privateMessagingEnabled: false,
        privateMeetingsEnabled: true,
        profileForNetworkingEnabled: true,
      };
      let profileSettingController = new UserProfileController(
        attendeeOneBrowserContext.request,
        eventId
      );
      await profileSettingController.editProfileDetailsSettings(
        profileDetailsPayload
      );
    });

    await test.step(`organiser opens private chat of attendee`, async () => {
      await organiserLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await organiserLobbyPage.clickOnPeopleTabOnInteractionPanel();
      await organiserLobbyPage.getPeopleComponent.searchForPeople(
        attendeeOneFirstName
      );
    });

    await test.step(`Now, organiser verifies disabled chat popup appears on attendee`, async () => {
      await organiserLobbyPage.getPeopleComponent.verifyHoverMessage(
        "User has disabled chat"
      );
    });

    await test.step(`speaker opens private chat of attendee`, async () => {
      await speakerLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await speakerLobbyPage.clickOnPeopleTabOnInteractionPanel();
      await speakerLobbyPage.getPeopleComponent.searchForPeople(
        attendeeOneFirstName
      );
    });

    await test.step(`Now, speaker verifies disabled chat popup appears on attendee`, async () => {
      await speakerLobbyPage.getPeopleComponent.verifyHoverMessage(
        "User has disabled chat"
      );
    });
  });
});
