import {
  test,
  Page,
  BrowserContext,
  APIRequestContext,
} from "@playwright/test";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  createNewEvent,
  getEventDetails,
  inviteAttendeeByAPI,
  updateLandingPageType,
} from "../../../../../util/apiUtil";
import { LandingPageTwo } from "../../../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
import { UserInfoDTO } from "../../../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import EventEntryType from "../../../../../enums/eventEntryEnum";
import { UserEventRoleDto } from "../../../../../dto/userEventRoleDto";
import EventRole from "../../../../../enums/eventRoleEnum";
import { DataUtl } from "../../../../../util/dataUtil";
import LandingPageType from "../../../../../enums/landingPageEnum";
import { EventController } from "../../../../../controller/EventController";

test.describe
  .parallel("Invite Based | Attendee Ladning Page | @New-users @inviteonly @Landing-page 2", () => {
  let org_browser_context: BrowserContext;
  let attendee_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let userDefaultRegFieldData: object;
  let attendeeEmail: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let landingPageTwo: LandingPageTwo;
  let eventController: EventController;

  test.beforeEach(async () => {
    // setup org context
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    organiserApiContext = org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      isMagicLinkEnabledAttendee: false,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // true to denote magic link enabled
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    eventController = new EventController(organiserApiContext, event_id);
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    // Enabling magic link, event entry type and Landing page type
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.INVITE_ONLY
    );
    eventInfoDto.eventEntryType = EventEntryType.INVITE_ONLY;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
    const attendeeBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    //page = await attendeeBrowserContext.newPage();
    //page = await context.newPage();
  });

  test.afterEach(async () => {
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
  });

  test("Uninvited attendee try to login via otp to event @attendee @Landing-page 2 @otp", async ({
    page,
  }) => {
    landingPageTwo = new LandingPageTwo(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await eventController.updateEventLandingPageSettings({
      eventEntryType: EventEntryType.INVITE_ONLY,
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageTwo.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageTwo.getRegistrationFormComponent()).isNotVisible();
    // now click on already registered
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    // now enter your email
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForMagicLink(userEventRoleDto);
    await (
      await landingPageTwo.getRestrcitedEventScreen()
    ).isPrivateEventScreenVisibleForOTPFlow();
  });

  test("Un-invited attendee try to login via password to event @attendee @Landing-page 2 @password", async ({
    page,
  }) => {
    landingPageTwo = new LandingPageTwo(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await eventController.updateEventLandingPageSettings({
      eventEntryType: EventEntryType.INVITE_ONLY,
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL"],
    });
    await landingPageTwo.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageTwo.getRegistrationFormComponent()).isNotVisible();
    // now click on already registered
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    // now enter your email
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await (
      await landingPageTwo.getRestrcitedEventScreen()
    ).isPrivateEventScreenVisible();
  });

  test("Invited attendee try to login via otp to event @attendee @Landing-page 2 @otp", async ({
    page,
  }) => {
    landingPageTwo = new LandingPageTwo(page);
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    // invite this user to this event as an attendee
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    await eventController.updateEventLandingPageSettings({
      eventEntryType: EventEntryType.INVITE_ONLY,
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageTwo.load(attendee_landing_page);
    // since the event is invite only reg form should not be visible
    await (await landingPageTwo.getRegistrationFormComponent()).isNotVisible();
    // now click on already registered
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    // now enter your email
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isLobbyLoaded();
  });

  test("Invited attendee try to login via password to event @attendee @Landing-page 2 @password", async ({
    page,
  }) => {
    attendeeEmail = DataUtl.getRandomAttendeeEmail(); // random email as an attendee
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    // invite this user to this event as an attendee
    let inviteresp = await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    if (inviteresp.status() == 200) {
      await page.waitForTimeout(5000);
      landingPageTwo = new LandingPageTwo(page);
      userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.ATTENDEE,
        true
      );
      await eventController.updateEventLandingPageSettings({
        eventEntryType: EventEntryType.INVITE_ONLY,
        isMagicLinkEnabled: false,
        attendeeAuthOptions: ["EMAIL"],
      });
      await landingPageTwo.load(attendee_landing_page);
      // await page.pause()
      // since the event is invite only reg form should not be visible
      await (
        await landingPageTwo.getRegistrationFormComponent()
      ).isNotVisible();
      // now click on already registered
      await (
        await landingPageTwo.getRegistrationFormComponent()
      ).clickOnAlreadyRegisteredButton();
      await (
        await landingPageTwo.getLoginOptionsComponent()
      ).submitEmailForOtpVerification(userEventRoleDto);
      await landingPageTwo.clickOnEnterNowButton();
      await landingPageTwo.isLobbyLoaded();
    }
  });
});
