import {
  test,
  expect,
  BrowserContext,
  APIRequestContext,
} from "@playwright/test";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  createNewEvent,
  getEventDetails,
  inviteSpeakerByApi,
  deleteSpeakerFromEvent,
  updateEventEntryType,
} from "../../../../../util/apiUtil";
import { getInviteLinkFromSpeakerInviteEmail } from "../../../../../util/emailUtil";
import { LandingPageOne } from "../../../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { UserInfoDTO } from "../../../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import { UserRegFormData } from "../../../../../test-data/userRegForm";
import { UserEventRoleDto } from "../../../../../dto/userEventRoleDto";
import EventRole from "../../../../../enums/eventRoleEnum";
import { DataUtl } from "../../../../../util/dataUtil";
import { SpeakerData } from "../../../../../test-data/speakerData";
import EventEntryType from "../../../../../enums/eventEntryEnum";
import { enableDisableEmailTrigger } from "../../../../../util/email-validation-api-util";
import EventSettingID from "../../../../../enums/EventSettingEnum";
import { fetchMagicLinkFromSendGridurl } from "../../../../../util/validation-util";
import { EventController } from "../../../../../controller/EventController";

test.describe
  .parallel("Reg Based | Magic link Disabled | @speaker-landing-page | Completely new Users", () => {
  let org_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let attendee_email_add: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let userDefaultRegFieldData: object;
  let eventController: EventController;

  test.beforeEach(async () => {
    // attendee_email_add = DataUtl.getRandomAttendeeEmail()
    userDefaultRegFieldData = new UserRegFormData(attendee_email_add)
      .userRegFormDataForDefaultFields;
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    organiserApiContext = await org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      isMagicLinkEnabledSpeaker: false,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link enabled
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    eventController = new EventController(organiserApiContext, event_id);
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await updateEventEntryType(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.REG_BASED
    );
    eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;

    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
  });

  test.afterEach(async () => {
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
  });

  test("TC001: User who is not speaker try to enter via otp from speaker landing page @speaker @otp", async ({
    page,
  }) => {
    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "automation",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );
    await eventController.updateEventLandingPageSettings({
      isSpeakermagicLinkEnabled: false,
      speakerAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageOne.load(speaker_landing_page);
    await (
      await landingPageOne.getSpeakerLandingComponent()
    ).clickOnLoginButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await (await landingPageOne.getEventRestrictionSpeakersPage()).isVisible();
    //verify user gets an option to go to attendee page from restricted entry page
    await (
      await landingPageOne.getEventRestrictionSpeakersPage()
    ).chooseToGoToAttendeePage();
    await expect(
      page,
      `expecting ${attendee_landing_page} url to be loaded`
    ).toHaveURL(attendee_landing_page, {
      timeout: 10000,
    });
  });
  test("TC002: User who is not speaker try to enter via password from speaker landing page @speaker @otp", async ({
    page,
  }) => {
    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "automation",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );
    await eventController.updateEventLandingPageSettings({
      isSpeakermagicLinkEnabled: false,
      speakerAuthOptions: ["EMAIL"],
    });
    await landingPageOne.load(speaker_landing_page);
    await (
      await landingPageOne.getSpeakerLandingComponent()
    ).clickOnLoginButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto, "speakerLandingPage");
    await (await landingPageOne.getEventRestrictionSpeakersPage()).isVisible();
    //verify user gets an option to go to attendee page from restricted entry page
    await (
      await landingPageOne.getEventRestrictionSpeakersPage()
    ).chooseToGoToAttendeePage();
    await expect(
      page,
      `expecting ${attendee_landing_page} url to be loaded`
    ).toHaveURL(attendee_landing_page, {
      timeout: 10000,
    });
  });

  test("TC003: Invited speaker, logins to event via email invite link  @speaker @invitemail", async ({
    page,
  }) => {
    eventInfoDto.isMagicLinkEnabled=true;
    let landingPageOne = new LandingPageOne(page);
    let speakerUser = new SpeakerData(
      DataUtl.getRandomAttendeeEmail(),
      "Prateek"
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      true
    ); // true denote to send email
    let speakerInviteLink = await getInviteLinkFromSpeakerInviteEmail(
      speakerUser["email"],
      eventInfoDto.getInviteEmailSubj()
    );
    speakerInviteLink = speakerInviteLink.includes("click")
      ? await fetchMagicLinkFromSendGridurl(speakerInviteLink)
      : speakerInviteLink;
    let isMagicLinkRecieved = false;
    if (speakerInviteLink.includes(eventInfoDto.speakerLandingPage)) {
      isMagicLinkRecieved = true;
    }
    console.log(`speaker invite link is ${speakerInviteLink}`);
    expect(
      isMagicLinkRecieved,
      "expecting magic link to be recieved in speaker invitation email"
    ).toBeTruthy();
    await landingPageOne.load(speakerInviteLink); // should load the speaker landing page
    // await landingPageOne.clickOnEnterNowButton()
    // await landingPageOne.isStageLoaded()
    await landingPageOne.page.waitForURL(speaker_landing_page);
  });

  test("TC004: Invited user as speaker, login via otp on speaker landing page @speaker @invitemail", async ({
    page,
  }) => {
    let landingPageOne = new LandingPageOne(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    ); // true denote to send email
    await eventController.updateEventLandingPageSettings({
      isSpeakermagicLinkEnabled: false,
      speakerAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageOne.load(speaker_landing_page);
    await landingPageOne.clickOnLoginButton();
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER
    );
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isStageLoaded();
  });

  /*
       this is not working as it should, as speaker who do not have passowrd should 
       trigger otp flow if he chooses to
       login via password but for now, I have automated it by setting login password by forget
       password flow
      */
  test("TC005: Invited user as speaker, login via password on speaker landing page @speaker @invitemail", async ({
    page,
  }) => {
    let landingPageOne = new LandingPageOne(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    ); // true denote to send email
    await eventController.updateEventLandingPageSettings({
      isSpeakermagicLinkEnabled: false,
      speakerAuthOptions: ["EMAIL"],
    });
    await landingPageOne.load(speaker_landing_page);
    await landingPageOne.clickOnLoginButton();
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER,
      true
    );
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto, "speakerLandingPage", true);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isStageLoaded();
  });

  test("TC006: Deleted speaker trying to login from speaker landing page via password @speaker @organiser", async ({
    context,
  }) => {
    // add a new user as speaker
    let page = await context.newPage();
    let landingPageOne = new LandingPageOne(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    // true denote to send email
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    );
    // now this user will login to the event
    await eventController.updateEventLandingPageSettings({
      isSpeakermagicLinkEnabled: false,
      speakerAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageOne.load(speaker_landing_page);
    await landingPageOne.clickOnLoginButton();
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER,
      true
    );
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isStageLoaded();
    await context.clearCookies();
    // now delete this speaker via API
    await deleteSpeakerFromEvent(
      organiserApiContext,
      eventInfoDto.eventId,
      speakerEmail
    ); // delete this speaker from the event
    // now this user logs in again
    userEventRoleDto.setUserRole(EventRole.NOTEXISTS);
    await landingPageOne.load(speaker_landing_page);
    await landingPageOne.clickOnLoginButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await (await landingPageOne.getEventRestrictionSpeakersPage()).isVisible();
  });
});
