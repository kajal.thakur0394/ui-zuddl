import {
  test,
  expect,
  Page,
  BrowserContext,
  APIRequestContext,
} from "@playwright/test";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  createNewEvent,
  getEventDetails,
  inviteSpeakerByApi,
  deleteSpeakerFromEvent,
  updateLandingPageType,
} from "../../../../../util/apiUtil";
import { getInviteLinkFromSpeakerInviteEmail } from "../../../../../util/emailUtil";
import { LandingPageTwo } from "../../../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
import { UserInfoDTO } from "../../../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import { UserRegFormData } from "../../../../../test-data/userRegForm";
import { UserEventRoleDto } from "../../../../../dto/userEventRoleDto";
import EventRole from "../../../../../enums/eventRoleEnum";
import { DataUtl } from "../../../../../util/dataUtil";
import { SpeakerData } from "../../../../../test-data/speakerData";
import EventEntryType from "../../../../../enums/eventEntryEnum";
import LandingPageType from "../../../../../enums/landingPageEnum";
import { fetchMagicLinkFromSendGridurl } from "../../../../../util/validation-util";
import { EventController } from "../../../../../controller/EventController";

test.describe
  .parallel("Reg Based | Magic link Disabled | @speaker @Landing-page-2-landing-page | Completely new Users", () => {
  test.describe.configure({ retries: 2 });
  let org_session: Page;
  let org_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let attendee_email_add: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let userDefaultRegFieldData: object;
  let eventController: EventController;

  test.beforeEach(async () => {
    // attendee_email_add = DataUtl.getRandomAttendeeEmail()
    userDefaultRegFieldData = new UserRegFormData(attendee_email_add)
      .userRegFormDataForDefaultFields;
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    organiserApiContext = await org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      isMagicLinkEnabledSpeaker: false,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link enabled
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    eventController = new EventController(organiserApiContext, event_id);
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    // Updating event entry type and Landing page type
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );
    eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;
  });

  test.afterEach(async () => {
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
  });

  test("TC001: User who is not speaker try to enter via otp from speaker landing page @speaker @Landing-page-2 @otp", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "automation",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );
    await eventController.updateEventLandingPageSettings({
      isSpeakermagicLinkEnabled: false,
      speakerAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageTwo.load(speaker_landing_page);
    // await (await landingPageTwo.getSpeakerLandingComponent()).clickOnLoginButton()
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await (await landingPageTwo.getEventRestrictedSpeakersPage()).isVisible();
    //verify user gets an option to go to attendee page from restricted entry page
    await (
      await landingPageTwo.getEventRestrictedSpeakersPage()
    ).chooseToGoToAttendeePage();
    await page.waitForURL(attendee_landing_page);
  });

  test("TC002: User who is not speaker try to enter via password from speaker landing page @speaker @Landing-page-2 @otp", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "automation",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );
    await eventController.updateEventLandingPageSettings({
      isSpeakermagicLinkEnabled: false,
      speakerAuthOptions: ["EMAIL"],
    });
    await landingPageTwo.load(speaker_landing_page);
    // await (await landingPageTwo.getSpeakerLandingComponent()).clickOnLoginButton()
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto, "speakerLandingPage");
    await (await landingPageTwo.getEventRestrictedSpeakersPage()).isVisible();
    //verify user gets an option to go to attendee page from restricted entry page
    await (
      await landingPageTwo.getEventRestrictedSpeakersPage()
    ).chooseToGoToAttendeePage();
    await page.waitForURL(attendee_landing_page);
  });

  test("TC003: Invited speaker, logins to event via email invite link  @speaker @Landing-page-2 @invitemail", async ({
    page,
  }) => {
    eventInfoDto.isMagicLinkEnabled=true;
    let landingPageTwo = new LandingPageTwo(page);
    let speakerUser = new SpeakerData(
      DataUtl.getRandomAttendeeEmail(),
      "Prateek"
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      true
    ); // true denote to send email
    let speakerInviteLink = await getInviteLinkFromSpeakerInviteEmail(
      speakerUser["email"],
      eventInfoDto.getInviteEmailSubj()
    );
    speakerInviteLink = speakerInviteLink.includes("click")
      ? await fetchMagicLinkFromSendGridurl(speakerInviteLink)
      : speakerInviteLink;
    let isMagicLinkRecieved = false;
    if (speakerInviteLink.includes(eventInfoDto.speakerLandingPage)) {
      isMagicLinkRecieved = true;
    }
    console.log(`speaker invite link is ${speakerInviteLink}`);
    expect(
      isMagicLinkRecieved,
      "expecting magic link to be recieved in speaker invitation email"
    ).toBeTruthy();
    await landingPageTwo.load(speakerInviteLink); // should load the speaker landing page
    await page.waitForURL(speaker_landing_page); //expecting speaker to land on stage
  });

  test("TC004: Invited user as speaker, login via otp on speaker landing page @speaker @Landing-page-2 @invitemail", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    ); // true denote to send email
    await eventController.updateEventLandingPageSettings({
      isSpeakermagicLinkEnabled: false,
      speakerAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageTwo.load(speaker_landing_page);
    await landingPageTwo.clickOnLoginButton();
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER
    );
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
  });

  /*
       this is not working as it should, as speaker who do not have passowrd should 
       trigger otp flow if he chooses to
       login via password but for now, I have automated it by setting login password by forget
       password flow
      */
  test("TC005: Invited user as speaker, login via password on speaker landing page @speaker @Landing-page-2 @invitemail", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    ); // true denote to send email
    await eventController.updateEventLandingPageSettings({
      isSpeakermagicLinkEnabled: false,
      speakerAuthOptions: ["EMAIL"],
    });
    await landingPageTwo.load(speaker_landing_page);
    await landingPageTwo.clickOnSigninButton();
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER
    );
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto, "speakerLandingPage");
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
  });

  test("TC006: Deleted speaker trying to login from speaker landing page via password @speaker @Landing-page-2 @organiser", async ({
    context,
  }) => {
    // add a new user as speaker
    let page = await context.newPage();
    let landingPageTwo = new LandingPageTwo(page);
    let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    userInfoDto = new UserInfoDTO(
      speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    console.log(`Speaker data to add is ${speakerUser}`);
    // true denote to send email
    await inviteSpeakerByApi(
      organiserApiContext,
      speakerUser.getSpeakerDataObject(),
      eventInfoDto.eventId,
      false
    );
    // now this user will login to the event
    await eventController.updateEventLandingPageSettings({
      isSpeakermagicLinkEnabled: false,
      speakerAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageTwo.load(speaker_landing_page);
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER
    );
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
    await context.clearCookies();
    // now delete this speaker via API
    await deleteSpeakerFromEvent(
      organiserApiContext,
      eventInfoDto.eventId,
      speakerEmail
    ); // delete this speaker from the event
    // now this user logs in again
    userEventRoleDto.setUserRole(EventRole.NOTEXISTS);
    await landingPageTwo.load(speaker_landing_page);
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
    await (await landingPageTwo.getEventRestrictedSpeakersPage()).isVisible();
    await page?.close();
  });
});
