import {
  test,
  expect,
  Page,
  BrowserContext,
  APIRequestContext,
} from "@playwright/test";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  createNewEvent,
  getEventDetails,
  inviteAttendeeByAPI,
  updateLandingPageType,
  addCustomFieldsToEvent,
} from "../../../../../util/apiUtil";
import { fetchAttendeeInviteMagicLink } from "../../../../../util/emailUtil";
import { LandingPageTwo } from "../../../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
import { UserInfoDTO } from "../../../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import EventEntryType from "../../../../../enums/eventEntryEnum";
import {
  EventCustomFieldFormObj,
  UserRegFormData,
} from "../../../../../test-data/userRegForm";
import { UserEventRoleDto } from "../../../../../dto/userEventRoleDto";
import EventRole from "../../../../../enums/eventRoleEnum";
import { DataUtl } from "../../../../../util/dataUtil";
import LandingPageType from "../../../../../enums/landingPageEnum";
import { EventController } from "../../../../../controller/EventController";

test.describe
  .parallel("Reg Based | Magic link Disabled | Attendee Landing Page | @attendee @Landing-page-2@reg-based", () => {
  let org_session: Page;
  let org_browser_context: BrowserContext;
  let attendee_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let userDefaultRegFieldData: object;
  let attendee_email_add: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let eventController: EventController;
  let page: Page;

  test.beforeEach(async ({ context }) => {
    // attendee_email_add = DataUtl.getRandomAttendeeEmail()
    userDefaultRegFieldData = new UserRegFormData(attendee_email_add)
      .userRegFormDataForDefaultFields;
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    organiserApiContext = await org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      isMagicLinkEnabledAttendee: false,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link enabled
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    eventController = new EventController(organiserApiContext, event_id);
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    // Updating event entry type and Landing page type
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );
    eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;

    page = await context.newPage();
  });

  test.afterEach(async () => {
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
  });

  test("TC001 New attendee registers, then login via OTP @new-user @otp @Landing-page-2", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "automation",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageTwo.load(attendee_landing_page);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    await (
      await landingPageTwo.getRegistrationConfirmationScreen()
    ).isRegistrationConfirmationMessageVisible();
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await page.waitForURL(/lobby/);
  });

  test("TC002 New attendee registers, then login via password @new-user @password @Landing-page-2", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL"],
    });
    await landingPageTwo.load(attendee_landing_page);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    await (
      await landingPageTwo.getRegistrationConfirmationScreen()
    ).isRegistrationConfirmationMessageVisible();
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await page.waitForURL(/lobby/);
  });

  test("TC003 New attendee try login via otp by skipping reg step, then registers, then login @attendee @Landing-page-2", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageTwo.load(attendee_landing_page);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).emailFieldIsPrefilledWith(userInfoDto.userEmail);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    await (
      await landingPageTwo.getRegistrationConfirmationScreen()
    ).isRegistrationConfirmationMessageVisible();
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    // now try login in with otp
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await page.waitForURL(/lobby/);
  });

  test("TC004 New attendee try login via password by skipping reg step, then registers, then login @attendee @Landing-page-2", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL"],
    });
    await landingPageTwo.load(attendee_landing_page);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).emailFieldIsPrefilledWith(userInfoDto.userEmail);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    await (
      await landingPageTwo.getRegistrationConfirmationScreen()
    ).isRegistrationConfirmationMessageVisible();
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await page.waitForURL(/lobby/);
  });

  test("TC005 New Attendee after registering, trying to login via OTP @attendee @Landing-page-2@otp", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageTwo.load(attendee_landing_page);
    // first let user register to this event
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    await (
      await landingPageTwo.getRegistrationConfirmationScreen()
    ).isRegistrationConfirmationMessageVisible();
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    // now lets reload the attendee landing page and try to fill the form again
    await landingPageTwo.load(attendee_landing_page);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await page.waitForURL(/lobby/);
  });

  test("TC006 New Attendee after registering, trying to login via password @attendee @Landing-page-2@password", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL"],
    });
    await landingPageTwo.load(attendee_landing_page);
    // first let user register to this event
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    // verify that the event invite has been sent screen is shown to user
    await (
      await landingPageTwo.getRegistrationConfirmationScreen()
    ).isRegistrationConfirmationMessageVisible();
    // now since user is registered, we will update eventRoleDto
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    // now lets reload the attendee landing page and try to fill the form again
    await landingPageTwo.load(attendee_landing_page);
    // try to fill the same form again
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await page.waitForURL(/lobby/);
  });

  test("TC007 New attenede gets invited, then login via otp from the invitation link @attendee @Landing-page-2@attendee-invite", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    let attendeeEmailToInvite = DataUtl.getRandomAttendeeEmail();
    userInfoDto = new UserInfoDTO(
      attendeeEmailToInvite,
      "",
      "prateek",
      "parashar",
      false
    );
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // after adding an attendee by email, account role will get created, event role will get created and reg record also
    userInfoDto.hasEntryInAccountTable = true;
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    // this user now fetch magic link which he should have recieved in his email
    let attendeePageLinkFromEmailInvite: string =
      await fetchAttendeeInviteMagicLink(
        attendeeEmailToInvite,
        eventInfoDto.getInviteEmailSubj()
      );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageTwo.load(attendeePageLinkFromEmailInvite);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await page.waitForURL(/lobby/);
  });

  // Reset password option is not visisble as the page crashes.
  test("TC008 New attendee gets invited, then login via password @attendee @Landing-page-2@attendee-invite @password", async ({
    page,
  }) => {
    test.fail(
      true,
      "This will fail as reset password screen is not coming on LP2 for new users"
    );
    test.info().annotations.push({
      type: "Known Bug",
      description:
        "https://linear.app/zuddl/issue/BUG-1839/reset-password-screen-is-not-coming-on-lp2-for-new-users",
    });
    let landingPageTwo = new LandingPageTwo(page);
    let attendeeEmailToInvite = DataUtl.getRandomAttendeeEmail();
    userInfoDto = new UserInfoDTO(
      attendeeEmailToInvite,
      "",
      "prateek",
      "parashar",
      false
    );
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // after adding an attendee by email, account role will get created, event role will get created and reg record also
    userInfoDto.hasEntryInAccountTable = true;
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    // now user goes to attendee landing page and try to re register
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL"],
    });
    await landingPageTwo.load(attendee_landing_page);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await page.waitForURL(/lobby/);
  });

  test("TC009 New attenede gets invited, but try to register again to the event @attendee @Landing-page-2@attendee-invite", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    let attendeeEmailToInvite = DataUtl.getRandomAttendeeEmail();
    userInfoDto = new UserInfoDTO(
      attendeeEmailToInvite,
      "",
      "prateek",
      "parashar",
      false
    );
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // after adding an attendee by email, account role will get created, event role will get created and reg record also
    userInfoDto.hasEntryInAccountTable = true;
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    // now user goes to attendee landing page and try to re register
    await landingPageTwo.load(attendee_landing_page);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    // it should show that you are already registered
    await expect(page.locator("text=You've already registered")).toBeVisible();
  });

  test(`TC010 New Attendee filling up the registration form with custom fields @attendee @Landing-page-2@customfield`, async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await addCustomFieldsToEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      new EventCustomFieldFormObj(event_id, landing_page_id).customFieldObj
    );
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageTwo.load(attendee_landing_page);
    // update the default reg fields to custom feilds
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userCustomFieldFormData
    );
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    await (
      await landingPageTwo.getRegistrationConfirmationScreen()
    ).isRegistrationConfirmationMessageVisible();
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await page.waitForURL(/lobby/);
  });
});
