import {
  test,
  Page,
  BrowserContext,
  APIRequestContext,
} from "@playwright/test";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  createNewEvent,
  getEventDetails,
  inviteAttendeeByAPI,
  updateLandingPageType,
} from "../../../../../util/apiUtil";
import { fetchAttendeeInviteMagicLink } from "../../../../../util/emailUtil";
import { LandingPageTwo } from "../../../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
import { UserInfoDTO } from "../../../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import EventEntryType from "../../../../../enums/eventEntryEnum";
import { UserRegFormData } from "../../../../../test-data/userRegForm";
import { UserEventRoleDto } from "../../../../../dto/userEventRoleDto";
import EventRole from "../../../../../enums/eventRoleEnum";
import { DataUtl } from "../../../../../util/dataUtil";
import LandingPageType from "../../../../../enums/landingPageEnum";
import { EventController } from "../../../../../controller/EventController";

test.describe
  .parallel("Reg Based | Magic link Disabled | Attendee Landing Page | Existing Users @Landing-page-2", () => {
  let org_session: Page;
  let org_browser_context: BrowserContext;
  let attendee_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let userDefaultRegFieldData: object;
  let attendee_email_add: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let eventController: EventController;
  let page: Page;

  test.beforeEach(async ({ context }) => {
    // attendee_email_add = DataUtl.getRandomAttendeeEmail()
    userDefaultRegFieldData = new UserRegFormData(attendee_email_add)
      .userRegFormDataForDefaultFields;
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    organiserApiContext = await org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      isMagicLinkEnabledAttendee: false,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // true to denote magic link enabled
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    eventController = new EventController(organiserApiContext, event_id);
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    // Updating event entry type and Landing page type
    await updateLandingPageType(
      organiserApiContext,
      event_id,
      landing_page_id,
      LandingPageType.STYLE2,
      "false",
      EventEntryType.REG_BASED
    );
    eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;

    // get the existing user data
    userInfoDto = new UserInfoDTO(
      DataUtl.getApplicationTestDataObj()["attendeeEmailWithAccountEntry"],
      DataUtl.getApplicationTestDataObj()["attendeePasswordWithAccountEntry"],
      "prateek",
      "parashar",
      true
    );
    page = await context.newPage();
  });

  test.afterEach(async () => {
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
  });

  test(" @LP2 TC001 User who has an account, registers to this event and try login via Password through already registered flow @attendee @Landing-page-2", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL"],
    });
    await landingPageTwo.load(attendee_landing_page);
    // first let user register to this event
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).isRegistrationConfirmationMessageVisible(false);
    // now we will update the eventrole dto
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    // now lets reload the attendee landing page and try to fill the form again
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    // try to fill the same form again
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isLobbyLoaded();
  });

  test("TC002 User who has an account, registers to this event and try login via OTP through already registered flow @attendee @Landing-page-2", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageTwo.load(attendee_landing_page);
    // first let user register to this event
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).isRegistrationConfirmationMessageVisible(false);
    // now we will update the eventrole dto
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    // now lets reload the attendee landing page and try to fill the form again
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    // try to fill the same form again
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isLobbyLoaded();
  });

  test("TC003 Invited an attendee who has an account, then he logs in through otp flow @attendee @Landing-page-2", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    let attendeePageLink: string = await fetchAttendeeInviteMagicLink(
      userInfoDto.userEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageTwo.load(attendeePageLink);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isLobbyLoaded();
  });

  test("TC004 Invited an attendee who has an account, he logins via password through already reg flow @attendee @Landing-page-2 @otp", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    let attendeePageLink: string = await fetchAttendeeInviteMagicLink(
      userInfoDto.userEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL"],
    });
    await landingPageTwo.load(attendeePageLink);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isLobbyLoaded();
  });

  test("TC005 Invited an attendee who has an account, he try to register again on landing page , then log in via otp @attendee @Landing-page-2 @otp", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    let attendeePageLink: string = await fetchAttendeeInviteMagicLink(
      userInfoDto.userEmail,
      eventInfoDto.getInviteEmailSubj()
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageTwo.load(attendeePageLink);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    await (
      await landingPageTwo.getRegistrationConfirmationScreen()
    ).isAlreadyRegisteredMessageVisible();
    //now user have to click on go back button to go to reg form component
    await (
      await landingPageTwo.getRegistrationConfirmationScreen()
    ).clickOnBackButton();
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isLobbyLoaded();
  });

  test("TC006 Organiser in already authenticated state,  opens the event landing page @organiser @Landing-page-2", async () => {
    let org_session = await org_browser_context.newPage();
    let landingPageTwo = new LandingPageTwo(org_session);
    await landingPageTwo.load(attendee_landing_page);
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
    await org_session.close();
  });

  test("TC007 Organiser of this event trying to login via otp with his email @organiser @Landing-page-2", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getApplicationTestDataObj()["eventsOrganiserEmail"],
      "",
      "prateek",
      "organiser",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ORGANISER,
      true
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageTwo.load(attendee_landing_page);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
  });

  test("TC008 Organiser of this event trying to login via password @organiser @Landing-page-2", async ({
    page,
  }) => {
    let landingPageTwo = new LandingPageTwo(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getApplicationTestDataObj()["eventsOrganiserEmail"],
      DataUtl.getApplicationTestDataObj()["organiserPassword"],
      "prateek",
      "organiser",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ORGANISER,
      true
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL"],
    });
    await landingPageTwo.load(attendee_landing_page);
    await (
      await landingPageTwo.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageTwo.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageTwo.clickOnEnterNowButton();
    await landingPageTwo.isStageLoaded();
  });

  // You've sucessfully registered message is visible.
  test.fixme(
    "TC009 Organiser of this event trying to register himself as attendee @organiser @Landing-page-2",
    async ({ page }) => {
      let landingPageTwo = new LandingPageTwo(page);
      userInfoDto = new UserInfoDTO(
        DataUtl.getApplicationTestDataObj()["eventsOrganiserEmail"],
        DataUtl.getApplicationTestDataObj()["organiserPassword"],
        "prateek",
        "organiser",
        true
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.ORGANISER,
        true
      );
      await landingPageTwo.load(attendee_landing_page);
      await (
        await landingPageTwo.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDto.userRegFormData.userRegFormDataForDefaultFields
      );
      await (
        await landingPageTwo.getRegistrationConfirmationScreen()
      ).isAlreadyRegisteredMessageVisible();
    }
  );
});
