import {
  test,
  expect,
  BrowserContext,
  APIRequestContext,
  Page,
} from "@playwright/test";
import BrowserFactory from "../../../../../util/BrowserFactory";
import {
  createNewEvent,
  getEventDetails,
  updateEventEntryType,
  inviteAttendeeByAPI,
  addCustomFieldsToEvent,
} from "../../../../../util/apiUtil";
import { fetchAttendeeInviteMagicLink } from "../../../../../util/emailUtil";
import { LandingPageOne } from "../../../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { UserInfoDTO } from "../../../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../../../dto/eventInfoDto";
import EventEntryType from "../../../../../enums/eventEntryEnum";
import {
  EventCustomFieldFormObj,
  UserRegFormData,
} from "../../../../../test-data/userRegForm";
import { UserEventRoleDto } from "../../../../../dto/userEventRoleDto";
import EventRole from "../../../../../enums/eventRoleEnum";
import { DataUtl } from "../../../../../util/dataUtil";
import { enableDisableEmailTrigger } from "../../../../../util/email-validation-api-util";
import EventSettingID from "../../../../../enums/EventSettingEnum";
import { EventController } from "../../../../../controller/EventController";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe
  .parallel("Reg Based | Magic link Disabled | Attendee Landing Page | @attendee @reg-based", () => {
  let org_browser_context: BrowserContext;
  let attendee_browser_context: BrowserContext;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let event_title: string;
  let event_id: number;
  let organiserApiContext: APIRequestContext;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let userDefaultRegFieldData: object;
  let attendee_email_add: string;
  let userEventRoleDto: UserEventRoleDto;
  let landing_page_id;
  let page: Page;
  let eventController: EventController;

  test.beforeEach(async ({ context }) => {
    // attendee_email_add = DataUtl.getRandomAttendeeEmail()
    userDefaultRegFieldData = new UserRegFormData(attendee_email_add)
      .userRegFormDataForDefaultFields;
    org_browser_context = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    organiserApiContext = await org_browser_context.request;
    // add event via api
    event_title = Date.now().toString() + "_automationevent";
    event_id = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title,
      isMagicLinkEnabledAttendee: false,
    });
    eventInfoDto = new EventInfoDTO(event_id, event_title, false); // false to denote magic link enabled
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, event_id)
    ).json();
    eventController = new EventController(organiserApiContext, event_id);
    landing_page_id = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landing_page_id;
    await updateEventEntryType(
      organiserApiContext,
      event_id,
      landing_page_id,
      EventEntryType.REG_BASED
    );
    eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;

    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    page = await context.newPage();
  });

  test.afterEach(async () => {
    console.log("In teardown, closing the browser context");
    await org_browser_context?.close();
  });

  test("TC001 New attendee registers, then login via OTP @new-user @otp", async ({
    page,
  }) => {
    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "automation",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageOne.load(attendee_landing_page);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    await expect(
      page.locator("div[class^='styles-module__logoHeaderBody']")
    ).toContainText("You've successfully registered!");
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC002 New attendee registers, then login via password @new-user @password", async ({
    page,
  }) => {
    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL"],
    });
    await landingPageOne.load(attendee_landing_page);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    await expect(
      page.locator("div[class^='styles-module__logoHeaderBody']")
    ).toContainText("You've successfully registered!");
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC003 New attendee try login via otp by skipping reg step, then registers, then login @attendee", async ({
    page,
  }) => {
    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageOne.load(attendee_landing_page);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).emailFieldIsPrefilledWith(userInfoDto.userEmail);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    await expect(
      page.locator("div[class^='styles-module__logoHeaderBody']")
    ).toContainText("You've successfully registered!");
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    // now try login in with otp
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC004 New attendee try login via password by skipping reg step, then registers, then login @attendee", async ({
    page,
  }) => {
    // bug is raised :https://linear.app/zuddl/issue/QAR-1059/flow-should-be-consistent-for-new-user-who-skips-reg-form-and-try-to
    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL"],
    });
    await landingPageOne.load(attendee_landing_page);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).emailFieldIsPrefilledWith(userInfoDto.userEmail);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    await expect(
      page.locator("div[class^='styles-module__logoHeaderBody']")
    ).toContainText("You've successfully registered!");
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).clickOnSubmitButton();
    let otp = await QueryUtil.fetchLoginOTPForEvent(
      userEventRoleDto.userInfoDto.userEmail,
      userEventRoleDto.eventInfoDto.eventId
    );
    // let otp = await fetchOtpFromEmail(userEventRoleDto.userInfoDto.userEmail,userEventRoleDto.eventInfoDto.eventTitle)
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).enterAndSubmitOtp(otp);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC005 New Attendee after registering, trying to login via OTP @attendee @otp", async ({
    page,
  }) => {
    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageOne.load(attendee_landing_page);
    // first let user register to this event
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    await expect(
      page.locator("div[class^='styles-module__logoHeaderBody']")
    ).toContainText("You've successfully registered!");
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    // now lets reload the attendee landing page and try to fill the form again
    await landingPageOne.load(attendee_landing_page);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC006 New Attendee after registering, trying to login via password @attendee @password", async ({
    page,
  }) => {
    let landingPageOne = new LandingPageOne(page);
    userInfoDto = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL"],
    });
    await landingPageOne.load(attendee_landing_page);
    // first let user register to this event
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    // verify that the event invite has been sent screen is shown to user
    await expect(
      page.locator("div[class^='styles-module__logoHeaderBody']")
    ).toContainText("You've successfully registered!");
    // now since user is registered, we will update eventRoleDto
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    // now lets reload the attendee landing page and try to fill the form again
    await landingPageOne.load(attendee_landing_page);
    // try to fill the same form again
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC007 New attenede gets invited, then login via otp from the invitation link @attendee @attendee-invite", async ({
    page,
  }) => {
    let landingPageOne = new LandingPageOne(page);
    let attendeeEmailToInvite = DataUtl.getRandomAttendeeEmail();
    userInfoDto = new UserInfoDTO(
      attendeeEmailToInvite,
      "",
      "prateek",
      "parashar",
      false
    );
    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      event_id,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.ATTENDEE,
      true
    );
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // after adding an attendee by email, account role will get created, event role will get created and reg record also
    userInfoDto.hasEntryInAccountTable = true;
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    // this user now fetch magic link which he should have recieved in his email
    let attendeePageLinkFromEmailInvite: string =
      await fetchAttendeeInviteMagicLink(
        attendeeEmailToInvite,
        eventInfoDto.getInviteEmailSubj()
      );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageOne.load(attendeePageLinkFromEmailInvite);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC008 New attendee gets invited, then login via password @attendee @attendee-invite @password", async ({
    page,
  }) => {
    let landingPageOne = new LandingPageOne(page);
    let attendeeEmailToInvite = DataUtl.getRandomAttendeeEmail();
    userInfoDto = new UserInfoDTO(
      attendeeEmailToInvite,
      "",
      "prateek",
      "parashar",
      false
    );
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    userInfoDto.hasEntryInAccountTable = false;
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    // now user goes to attendee landing page and try to re register
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL"],
    });
    await landingPageOne.load(attendee_landing_page);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForPasswordLogin(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC009 New attenede gets invited, but try to register again to the event @attendee @attendee-invite", async ({
    page,
  }) => {
    let landingPageOne = new LandingPageOne(page);
    let attendeeEmailToInvite = DataUtl.getRandomAttendeeEmail();
    userInfoDto = new UserInfoDTO(
      attendeeEmailToInvite,
      "",
      "prateek",
      "parashar",
      false
    );
    await inviteAttendeeByAPI(
      organiserApiContext,
      eventInfoDto.eventId,
      userInfoDto.userEmail
    );
    // after adding an attendee by email, account role will get created, event role will get created and reg record also
    userInfoDto.hasEntryInAccountTable = true;
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );
    // now user goes to attendee landing page and try to re register
    await landingPageOne.load(attendee_landing_page);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userRegFormDataForDefaultFields
    );
    // it should show that you are already registered
    await expect(page.locator("text=You've already registered")).toBeVisible();
  });

  test(`TC010 New Attendee filling up the registration form with custom fields @attendee @customfield`, async ({
    page,
  }) => {
    let landingPageOne = new LandingPageOne(page);
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await addCustomFieldsToEvent(
      organiserApiContext,
      event_id,
      landing_page_id,
      new EventCustomFieldFormObj(event_id, landing_page_id).customFieldObj
    );
    userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await eventController.updateEventLandingPageSettings({
      isMagicLinkEnabled: false,
      attendeeAuthOptions: ["EMAIL_OTP"],
    });
    await landingPageOne.load(attendee_landing_page);
    // update the default reg fields to custom feilds
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDto.userRegFormData.userCustomFieldFormData
    );
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    await expect(
      page.locator("div[class^='styles-module__logoHeaderBody']")
    ).toContainText("You've successfully registered!");
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });
});
