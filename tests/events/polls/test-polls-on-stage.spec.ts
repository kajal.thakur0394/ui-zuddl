import {
  APIRequestContext,
  expect,
  test,
  BrowserContext,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { EventController } from "../../../controller/EventController";
import { PollsOrQuizController } from "../../../controller/PollsController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import { updateEventLandingPageDetails } from "../../../util/apiUtil";
import EventEntryType from "../../../enums/eventEntryEnum";
import { PollsComponent } from "../../../page-objects/events-pages/live-side-components/PollsComponent";
import PollStatus from "../../../enums/PollStatusEnum";
import { TopNavBarComponent } from "../../..//page-objects/events-pages/live-side-components/TopNavBarComponent";
import ZoneType from "../../../enums/ZoneTypeEnum";
import { RoomsListingPage } from "../../../page-objects/events-pages/zones/RoomsPage";
import { AVModal } from "../../../page-objects/events-pages/live-side-components/AvModal";
import { ExpoPage } from "../../../page-objects/events-pages/zones/ExpoPage";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel("@stage @polls", async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventTitle: string;
  let eventId;
  let defaultStageId;
  let defaultStageChannelId;
  let eventController: EventController;
  let attendeeEmail: string;
  let speakerEmail: string;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = orgBrowserContext.request;
    eventTitle = DataUtl.getRandomEventTitle();
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
    speakerEmail = DataUtl.getRandomSpeakerEmail();

    await test.step(`Create a new event with title ${eventTitle}`, async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: eventTitle,
      });
      eventController = new EventController(orgApiContext, eventId);
      await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
        false
      );
      await eventController.getAccessGroupController.handleOnboardingChecksForSpeaker(
        false
      );
    });

    await test.step(`Organiser enables magic link for attendee and speaker for this event`, async () => {
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
        isSpeakermagicLinkEnabled: true,
      });
    });

    await test.step(`Organiser adds an attendee to the event`, async () => {
      await eventController.inviteAttendeeToTheEvent(attendeeEmail);
    });

    await test.step(`Organiser adds an speaker to the event`, async () => {
      await eventController.inviteSpeakerToTheEventByApi(
        speakerEmail,
        "PrateekSpeaker",
        "QA"
      );
    });

    await test.step(`Get default stage Id for this event`, async () => {
      defaultStageId = await eventController.getDefaultStageId();
    });

    await test.step(`Get default stage channel Id to post polls`, async () => {
      defaultStageChannelId = await eventController.getChannelId(
        defaultStageId
      );
    });
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test("TC001 : verify e2e flow of organiser publishing a drafted poll which gets voted by attendee and speaker", async ({
    browser,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-390/verify-flow-of-organiser-creating-a-poll-in-drafted-status-and-then",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-391/verify-polls-drafted-by-organiser-is-visible-to-speakers-and-can-be",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-397/verify-attendees-speaker-and-organiser-all-can-cast-vote-on-poll",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-398/verify-organiser-flow-of-closing-a-poll-and-same-should-reflect-to",
    });

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-401/verify-attendee-can-not-cast-vote-on-closed-polls",
    });

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-402/verify-presence-of-vote-count-on-poll-ui-if-attendee-has-voted-on-it",
    });

    let orgStagePage: StagePage;
    let attendeeStagePage: StagePage;
    let speakerStagePage: StagePage;
    let orgPollComponent: PollsComponent;
    let attendeePollComponent: PollsComponent;
    let speakerPollComponent: PollsComponent;
    let attendeeMagicLink: string;
    let speakerMagicLink: string;
    let stageUrl: string;
    let attendeePage = await (await browser.newContext()).newPage();
    let speakerPage = await (await browser.newContext()).newPage();

    const pollController = new PollsOrQuizController(orgApiContext, eventId);
    const pollTitle = "Poll 1";
    const pollOptions = [{ text: "option1" }, { text: "option2" }];
    const orgPage = await orgBrowserContext.newPage();
    const attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;

    await test.step(`Create new poll by api with title ${pollTitle} in draft status`, async () => {
      const pollCreatedInDraftResp = await pollController.createNewPollInDraft(
        defaultStageChannelId,
        {
          prompt: pollTitle,
          options: pollOptions,
        }
      );
    });

    await test.step(`Organiser goes to attendee landing page and enter into the event and then goes to stage`, async () => {
      await orgPage.goto(attendeeLandingPage);
      await orgPage.click("text=Enter Now");
      stageUrl = `${
        DataUtl.getApplicationTestDataObj()["baseUrl"]
      }/l/event/${eventId}/stages/${defaultStageId}`;
      await orgPage.goto(stageUrl);
      orgStagePage = new StagePage(orgPage);
      await orgStagePage.closeWelcomeStagePopUp();
    });

    await test.step(`Organiser opens Polls tab and then opens draft section`, async () => {
      orgPollComponent = await orgStagePage.openPollsTab();
      await orgPollComponent.openDraftPollsTab();
    });

    await test.step(`Verify organiser is able to view the polls on draft tab`, async () => {
      await orgPollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Attendee goes to magic link and enters into the event and then go to stage`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.click("text=Enter Now");
      await attendeePage.goto(stageUrl);
    });

    await test.step(`Attendee verifies there is no poll visible on his poll tab`, async () => {
      attendeeStagePage = new StagePage(attendeePage);
      attendeePollComponent = await attendeeStagePage.openPollsTab();
      await attendeePollComponent.verifyPollIsNotVisibleOnList(pollTitle);
    });

    await test.step(`Speaker goes to magic link and enters into the event and then go to stage`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );
      await speakerPage.goto(speakerMagicLink);
      await speakerPage.click("text=Enter Now");
      await speakerPage.goto(stageUrl);
    });

    await test.step(`Speaker verifies there a poll visible on draft tab`, async () => {
      speakerStagePage = new StagePage(speakerPage);
      await speakerStagePage.closeWelcomeStagePopUp();
      speakerPollComponent = await speakerStagePage.openPollsTab();
      await speakerPollComponent.openDraftPollsTab();
      await speakerPollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Organiser publishes the poll from the drafted tab`, async () => {
      await orgPollComponent.publishPollWithTitle(pollTitle);
      await test.step(`Verify organiser sees the poll published`, async () => {
        await orgPollComponent.openPublishedPollsTab();
        await orgPollComponent.verifyPollIsVisibleOnList(pollTitle);
      });
    });

    await test.step(`Speaker verifies he sees the poll on the publised tab`, async () => {
      await speakerPollComponent.verifyPollIsNotVisibleOnList(pollTitle);
      await speakerPollComponent.openPublishedPollsTab();
      await speakerPollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Attendee verifies he sees the poll now`, async () => {
      await attendeePollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Attendee votes on option 1`, async () => {
      await attendeePollComponent.selectOptionToVoteOnPoll(
        pollTitle,
        pollOptions[0]["text"]
      );
    });

    await test.step(`Speaker votes on option 1`, async () => {
      await speakerPollComponent.selectOptionToVoteOnPoll(
        pollTitle,
        pollOptions[0]["text"]
      );
    });

    await test.step(`Organiser votes on option 1`, async () => {
      await orgPollComponent.selectOptionToVoteOnPoll(
        pollTitle,
        pollOptions[0]["text"]
      );
    });

    //verify count of votes to each user
    await test.step(`Verify organiser sees 3 vote count on the poll`, async () => {
      await orgPage.reload();
      await orgStagePage.closeWelcomeStagePopUp();
      await orgStagePage.openPollsTab();
      await orgPollComponent.openPublishedPollsTab();
      await orgPollComponent.verifyVoteCountOnPollMatches(pollTitle, 3);
    });

    await test.step(`Verify speaker sees 3 vote count on the poll`, async () => {
      await speakerPage.reload();
      await speakerStagePage.closeWelcomeStagePopUp();
      await speakerStagePage.openPollsTab();
      await speakerPollComponent.openPublishedPollsTab();
      await speakerPollComponent.verifyVoteCountOnPollMatches(pollTitle, 3);
    });

    await test.step(`Verify attendee does not see vote count on poll`, async () => {
      await attendeePollComponent.verifyVoteCountToShowEmpty(pollTitle);
    });

    await test.step(`verify Organiser is able to close the poll`, async () => {
      await orgPollComponent.closeThePoll(pollTitle);
    });

    await test.step(`verify if poll is closed, attendee sees closed foot note in the poll`, async () => {
      await attendeePollComponent.verifyPollIsClosed(pollTitle);
    });

    await test.step(`verify if poll is closed, speaker sees closed foot note in the poll`, async () => {
      await speakerPollComponent.verifyPollIsClosed(pollTitle);
    });

    await test.step(`verify if poll is closed, organiser sees closed foot note in the poll`, async () => {
      await orgPollComponent.verifyPollIsClosed(pollTitle);
    });
  });

  test("TC002 : verify e2e flow of speaker/organiser publishing a drafted poll which gets voted by attendee and speaker", async ({
    browser,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-395/verify-flow-of-speaker-creating-a-poll-in-drafted-status-and-then",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-393/verify-polls-drafted-by-speaker-are-visible-to-organiser-and-can-be",
    });

    let orgStagePage: StagePage;
    let attendeeStagePage: StagePage;
    let speakerStagePage: StagePage;
    let orgPollComponent: PollsComponent;
    let attendeePollComponent: PollsComponent;
    let speakerPollComponent: PollsComponent;
    let attendeeMagicLink: string;
    let speakerMagicLink: string;
    let stageUrl: string;
    let pollTitle: string;
    let pollOptions: any[];
    let attendeePage = await (await browser.newContext()).newPage();
    let speakerPage = await (await browser.newContext()).newPage();
    let speakerApiContext = speakerPage.request;
    const orgPage = await orgBrowserContext.newPage();
    const attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;

    await test.step(`Organiser goes to attendee landing page and enter into the event and then goes to stage`, async () => {
      await orgPage.goto(attendeeLandingPage);
      await orgPage.click("text=Enter Now");
      stageUrl = `${
        DataUtl.getApplicationTestDataObj()["baseUrl"]
      }/l/event/${eventId}/stages/${defaultStageId}`;
      await orgPage.goto(stageUrl);
      orgStagePage = new StagePage(orgPage);
      await orgStagePage.closeWelcomeStagePopUp();
    });

    await test.step(`Attendee goes to magic link and enters into the event and then go to stage`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.click("text=Enter Now");
      await attendeePage.goto(stageUrl);
    });

    await test.step(`Speaker goes to magic link and enters into the event and then go to stage`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );
      await speakerPage.goto(speakerMagicLink);
      await speakerPage.click("text=Enter Now");
      await speakerPage.goto(stageUrl);
    });

    await test.step(`Speaker creates a poll data`, async () => {
      const pollController = new PollsOrQuizController(
        speakerApiContext,
        eventId
      );
      pollTitle = "Speaker Poll 1";
      pollOptions = [{ text: "option1" }, { text: "option2" }];
      await test.step(`Speaker creates a new poll by api with title ${pollTitle} in draft status`, async () => {
        const pollCreatedInDraftResp =
          await pollController.createNewPollInDraft(defaultStageChannelId, {
            prompt: pollTitle,
            options: pollOptions,
          });
      });
    });

    await test.step(`Speaker opens Polls tab and then opens draft section`, async () => {
      speakerStagePage = new StagePage(speakerPage);
      await speakerStagePage.closeWelcomeStagePopUp();
      speakerPollComponent = await speakerStagePage.openPollsTab();
      await speakerPollComponent.openDraftPollsTab();
    });

    await test.step(`Verify speaker is able to view the polls on draft tab`, async () => {
      await speakerPollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Attendee verifies there is no poll visible on his poll tab`, async () => {
      attendeeStagePage = new StagePage(attendeePage);
      attendeePollComponent = await attendeeStagePage.openPollsTab();
      await attendeePollComponent.verifyPollIsNotVisibleOnList(pollTitle);
    });

    await test.step(`Speaker publishes the poll from the drafted tab`, async () => {
      await speakerPollComponent.publishPollWithTitle(pollTitle);
      await test.step(`Verify speaker sees the poll published`, async () => {
        await speakerPollComponent.openPublishedPollsTab();
        await speakerPollComponent.verifyPollIsVisibleOnList(pollTitle);
      });
    });

    await test.step(`Attendee verifies he sees the poll now`, async () => {
      await attendeePollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Attendee votes on option 1`, async () => {
      await attendeePollComponent.selectOptionToVoteOnPoll(
        pollTitle,
        pollOptions[0]["text"]
      );
    });

    await test.step(`Verify speaker sees 1 vote count on the poll`, async () => {
      await speakerPage.reload();
      await speakerStagePage.closeWelcomeStagePopUp();
      await speakerStagePage.openPollsTab();
      await speakerPollComponent.openPublishedPollsTab();
      await speakerPollComponent.verifyVoteCountOnPollMatches(pollTitle, 1);
    });

    await test.step(`Speaker creates another poll data`, async () => {
      const pollController = new PollsOrQuizController(
        speakerApiContext,
        eventId
      );
      pollTitle = "Speaker Poll 2";
      pollOptions = [{ text: "option1" }, { text: "option2" }];
      await test.step(`Speaker creates a new poll by api with title ${pollTitle} in draft status`, async () => {
        const pollCreatedInDraftResp =
          await pollController.createNewPollInDraft(defaultStageChannelId, {
            prompt: pollTitle,
            options: pollOptions,
          });
      });
    });

    await test.step(`Organiser opens Polls tab and then opens draft section`, async () => {
      orgPollComponent = await orgStagePage.openPollsTab();
      await orgPollComponent.openDraftPollsTab();
    });

    await test.step(`Organiser publishes the poll from the drafted tab`, async () => {
      await orgPollComponent.publishPollWithTitle(pollTitle);
      await test.step(`Verify organiser sees the poll published`, async () => {
        await orgPollComponent.openPublishedPollsTab();
        await orgPollComponent.verifyPollIsVisibleOnList(pollTitle);
      });
    });

    await test.step(`Organiser verifies he sees the poll now`, async () => {
      await orgPollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Attendee verifies he sees the poll now`, async () => {
      await attendeePage.reload();
      await attendeeStagePage.openPollsTab();
      await attendeePollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Speaker verifies he sees the poll now`, async () => {
      await speakerPage.reload();
      await speakerStagePage.closeWelcomeStagePopUp();
      await speakerStagePage.openPollsTab();
      await speakerPollComponent.openPublishedPollsTab();
      await speakerPollComponent.verifyPollIsVisibleOnList(pollTitle);
    });
  });

  test("TC003 : verify e2e flow of speaker/organiser creating a published poll which gets voted by attendee and speaker", async ({
    browser,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-392/verify-flow-of-organiser-directly-publishing-a-poll-and-once-done-will",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-396/verify-flow-of-speaker-directly-publishing-a-poll-and-once-done-will",
    });

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-399/verify-speaker-flow-of-closing-a-poll-and-same-should-reflect-to",
    });

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-401/verify-attendee-can-not-cast-vote-on-closed-polls",
    });

    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-405/verify-when-a-new-poll-is-added-new-poll-button-is-visible-to",
    });

    let orgStagePage: StagePage;
    let attendeeStagePage: StagePage;
    let speakerStagePage: StagePage;
    let orgPollComponent: PollsComponent;
    let attendeePollComponent: PollsComponent;
    let speakerPollComponent: PollsComponent;
    let speakerPollController: PollsOrQuizController;
    let attendeeMagicLink: string;
    let speakerMagicLink: string;
    let stageUrl: string;
    let pollTitle: string;
    let pollOptions: any[];
    let attendeePage = await (await browser.newContext()).newPage();
    let speakerPage = await (await browser.newContext()).newPage();
    let speakerApiContext = speakerPage.request;
    let attendeeApiContext = attendeePage.request;
    let orgPage = await orgBrowserContext.newPage();
    let pollController: PollsOrQuizController;
    const attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;

    await test.step(`Organiser goes to attendee landing page and enter into the event and then goes to stage`, async () => {
      await orgPage.goto(attendeeLandingPage);
      await orgPage.click("text=Enter Now");
      stageUrl = `${
        DataUtl.getApplicationTestDataObj()["baseUrl"]
      }/l/event/${eventId}/stages/${defaultStageId}`;
      await orgPage.goto(stageUrl);
      orgStagePage = new StagePage(orgPage);
      await orgStagePage.closeWelcomeStagePopUp();
    });

    await test.step(`Attendee goes to magic link and enters into the event and then go to stage`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.click("text=Enter Now");
      await attendeePage.goto(stageUrl);
    });

    await test.step(`Speaker goes to magic link and enters into the event and then go to stage`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );
      await speakerPage.goto(speakerMagicLink);
      await speakerPage.click("text=Enter Now");
      await speakerPage.goto(stageUrl);
    });

    await test.step(`Attendee verifies there is no poll visible on his poll tab`, async () => {
      attendeeStagePage = new StagePage(attendeePage);
      attendeePollComponent = await attendeeStagePage.openPollsTab();
      await attendeePollComponent.verifyPollIsNotVisibleOnList(pollTitle);
    });

    await test.step(`Organiser creates a poll data`, async () => {
      pollController = new PollsOrQuizController(orgApiContext, eventId);
      pollTitle = " Organiser Poll 1";
      pollOptions = [{ text: "option1" }, { text: "option2" }];
      await test.step(`Organiser creates a new poll by api with title ${pollTitle} in published status`, async () => {
        const pollCreatedInDraftResp =
          await pollController.createNewPollInPublish(defaultStageChannelId, {
            prompt: pollTitle,
            options: pollOptions,
          });
      });
    });

    await test.step(`Speaker verifies there a poll visible on published tab`, async () => {
      speakerStagePage = new StagePage(speakerPage);
      await speakerStagePage.closeWelcomeStagePopUp();
      speakerPollComponent = await speakerStagePage.openPollsTab();
      await speakerPollComponent.openPublishedPollsTab();
      await speakerPollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Attendee verifies he sees the poll now`, async () => {
      await attendeePollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Attendee votes on option 1`, async () => {
      await attendeePollComponent.selectOptionToVoteOnPoll(
        pollTitle,
        pollOptions[0]["text"]
      );
    });

    await test.step(`Verify organiser sees 1 vote count on the poll`, async () => {
      await orgPage.reload();
      await orgStagePage.closeWelcomeStagePopUp();
      orgPollComponent = await orgStagePage.openPollsTab();
      await orgPollComponent.openPublishedPollsTab();
      await orgPollComponent.verifyVoteCountOnPollMatches(pollTitle, 1);
    });

    await test.step(`Speaker creates poll data`, async () => {
      pollController = new PollsOrQuizController(speakerApiContext, eventId);
      pollTitle = "Speaker Poll 2";
      pollOptions = [{ text: "option1" }, { text: "option2" }];
      await test.step(`Speaker creates a new poll by api with title ${pollTitle} in publish status`, async () => {
        const pollCreatedInDraftResp =
          await pollController.createNewPollInPublish(defaultStageChannelId, {
            prompt: pollTitle,
            options: pollOptions,
          });
      });
    });

    await test.step(`Organiser verifies NewPolls button to be visible and should render new poll in published tab`, async () => {
      await orgPollComponent.verifyNewPollsButtonVisible();
      await orgPollComponent.clickonNewPollsButton();
      await orgPollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Attendee verifies NewPolls button to be visible and should render new poll in published tab`, async () => {
      await attendeePollComponent.verifyNewPollsButtonVisible();
      await attendeePollComponent.clickonNewPollsButton();
      await attendeePollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Attendee votes on option 1`, async () => {
      await attendeePollComponent.selectOptionToVoteOnPoll(
        pollTitle,
        pollOptions[0]["text"]
      );
    });

    await test.step(`Speaker closes the published poll`, async () => {
      speakerPollController = new PollsOrQuizController(
        speakerApiContext,
        eventId
      );
      const speakerPollDetails = await speakerPollController.getPollFromList(
        defaultStageChannelId,
        pollTitle
      );
      await speakerPollController.closePoll(speakerPollDetails["pollId"]);
    });

    await test.step(`Verify speaker now sees the closed status`, async () => {
      const speakerPollDetails = await speakerPollController.getPollFromList(
        defaultStageChannelId,
        pollTitle
      );
      expect(speakerPollDetails["status"]).toEqual(PollStatus.CLOSED);
    });

    await test.step(`Verify attendee now sees the closed status`, async () => {
      const attendeePollController = new PollsOrQuizController(
        attendeeApiContext,
        eventId
      );
      const attendeePollDetails = await attendeePollController.getPollFromList(
        defaultStageChannelId,
        pollTitle
      );
      expect(attendeePollDetails["status"]).toEqual(PollStatus.CLOSED);
    });

    await test.step(`Verify attendee is unable to cast vote on closed Poll `, async () => {
      await attendeePollComponent.verifyPollCannotBeCasted(pollTitle);
    });
  });

  test("TC004 : verify when poll is edited by organiser, there is a presence with updated content on the published tab/polls tab for attendee and speaker", async ({
    browser,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-394/verify-flow-of-organiser-editing-the-poll-in-drafted-state-and-adding",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-400/verify-polls-if-closed-n-hidden-are-not-visible-to-attendee-but-are",
    });
    let orgStagePage: StagePage;
    let attendeeStagePage: StagePage;
    let speakerStagePage: StagePage;

    let orgPollComponent: PollsComponent;
    let speakerPollComponent: PollsComponent;
    let pollController: PollsOrQuizController;
    let attendeePollComponent: PollsComponent;
    let attendeeMagicLink: string;
    let speakerMagicLink: string;
    let stageUrl: string;
    let pollTitle: string;
    let optionToAdd: string;
    let beforeUpdationPollDetails;
    let afterUpdationPollDetails;
    let pollOptions: any[];
    let attendeePage = await (await browser.newContext()).newPage();
    let speakerPage = await (await browser.newContext()).newPage();
    let speakerApiContext = speakerPage.request;
    let orgPage = await orgBrowserContext.newPage();
    const attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;

    await test.step(`Organiser goes to attendee landing page and enter into the event and then goes to stage`, async () => {
      await orgPage.goto(attendeeLandingPage);
      await orgPage.click("text=Enter Now");
      stageUrl = `${
        DataUtl.getApplicationTestDataObj()["baseUrl"]
      }/l/event/${eventId}/stages/${defaultStageId}`;
      await orgPage.goto(stageUrl);
      orgStagePage = new StagePage(orgPage);
      await orgStagePage.closeWelcomeStagePopUp();
    });

    await test.step(`Attendee goes to magic link and enters into the event and then go to stage`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.click("text=Enter Now");
      await attendeePage.goto(stageUrl);
    });

    await test.step(`Speaker goes to magic link and enters into the event and then go to stage`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );
      await speakerPage.goto(speakerMagicLink);
      await speakerPage.click("text=Enter Now");
      await speakerPage.goto(stageUrl);
    });

    await test.step(`Organiser creates a poll data`, async () => {
      pollController = new PollsOrQuizController(orgApiContext, eventId);
      pollTitle = " Organiser Poll 1";
      pollOptions = [{ text: "option1" }, { text: "option2" }];
      await test.step(`Organiser creates a new poll by api with title ${pollTitle} in draft status`, async () => {
        const pollCreatedInDraftResp =
          await pollController.createNewPollInDraft(defaultStageChannelId, {
            prompt: pollTitle,
            options: pollOptions,
          });
      });
    });

    await test.step(`Speaker saves the poll details before edition`, async () => {
      const speakerPollController = new PollsOrQuizController(
        speakerApiContext,
        eventId
      );
      beforeUpdationPollDetails = await speakerPollController.getPollDetails(
        defaultStageChannelId,
        pollTitle
      );
    });

    await test.step(`organiser edits the poll by adding new option`, async () => {
      optionToAdd = "option3";
      let pollTest = await pollController.addNewOption(
        defaultStageChannelId,
        pollTitle,
        {
          text: optionToAdd,
        }
      );
      await pollController.updateADraftedPollOrQuiz(
        await pollController.getPollId(defaultStageChannelId, pollTitle),
        {
          options: pollTest,
        }
      );
      afterUpdationPollDetails = await pollController.getPollDetails(
        defaultStageChannelId,
        pollTitle
      );
    });

    await test.step(`Speaker verifies that poll is being updated in draft state`, async () => {
      expect(afterUpdationPollDetails["status"]).toEqual(PollStatus.SCHEDULED);
      expect(afterUpdationPollDetails["options"].length).toBeGreaterThan(
        beforeUpdationPollDetails["options"].length
      );
    });

    await test.step(`Organiser opens Polls tab and then opens draft section`, async () => {
      await orgPage.reload();
      await orgStagePage.closeWelcomeStagePopUp();
      orgPollComponent = await orgStagePage.openPollsTab();
      await orgPollComponent.openDraftPollsTab();
    });

    await test.step(`Organiser publishes the poll from the drafted tab`, async () => {
      await orgPollComponent.publishPollWithTitle(pollTitle);
      await test.step(`Verify organiser sees the poll published`, async () => {
        await orgPollComponent.openPublishedPollsTab();
        await orgPollComponent.verifyPollIsVisibleOnList(pollTitle);
      });
    });

    await test.step(`Attendee verifies he sees the poll with new option now`, async () => {
      attendeeStagePage = new StagePage(attendeePage);
      await attendeePage.reload();
      attendeePollComponent = await attendeeStagePage.openPollsTab();
      await attendeePollComponent.verifyPollIsVisibleOnList(pollTitle);
      await attendeePollComponent.verifyExpectedOptionVisibleinPoll(
        optionToAdd
      );
    });

    await test.step(`Attendee votes on option 1`, async () => {
      await attendeePollComponent.selectOptionToVoteOnPoll(
        pollTitle,
        pollOptions[0]["text"]
      );
    });

    await test.step(`Verify organiser sees 1 vote count on the poll via API`, async () => {
      const voteCount = await pollController.getCountOfVotedPoll(
        defaultStageChannelId,
        pollTitle
      );
      expect(voteCount).toEqual(1);
    });

    await test.step(`Organiser now close and hide the poll`, async () => {
      const pollId = await pollController.getPollId(
        defaultStageChannelId,
        pollTitle
      );
      await pollController.closeNhidePollOrQuiz(pollId);
    });

    await test.step(`Verify organiser should see the close-and-hide poll`, async () => {
      await orgPollComponent.verifyCloseAndHideButtonIsVisible();
    });

    await test.step(`Verify speaker should see the close-and-hide poll`, async () => {
      speakerStagePage = new StagePage(speakerPage);
      await speakerPage.reload();
      await speakerStagePage.closeWelcomeStagePopUp();
      speakerPollComponent = await speakerStagePage.openPollsTab();
      await speakerPollComponent.openPublishedPollsTab();
      await speakerPollComponent.verifyCloseAndHideButtonIsVisible();
    });

    await test.step(`Verify attendee should not see the close-and-hide poll`, async () => {
      await attendeePollComponent.verifyCloseAndHideButtonIsInvisible();
    });
  });

  test("TC005 : verify organiser and speaker can delete a poll in drafted state", async ({
    browser,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-403/verify-organisers-can-delete-a-poll-in-drafted-state",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-404/verify-speaker-can-delete-a-poll-in-drafted-state",
    });
    let orgStagePage: StagePage;
    let speakerStagePage: StagePage;

    let orgPollComponent: PollsComponent;
    let speakerPollComponent: PollsComponent;
    let pollController: PollsOrQuizController;
    let speakerPollController: PollsOrQuizController;
    let speakerMagicLink: string;
    let stageUrl: string;
    let pollTitle: string;
    let pollId: string;
    let pollOptions: any[];
    let speakerPage = await (await browser.newContext()).newPage();
    let speakerApiContext = speakerPage.request;
    let orgPage = await orgBrowserContext.newPage();
    const attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;

    await test.step(`Organiser goes to attendee landing page and enter into the event and then goes to stage`, async () => {
      await orgPage.goto(attendeeLandingPage);
      await orgPage.click("text=Enter Now");
      stageUrl = `${
        DataUtl.getApplicationTestDataObj()["baseUrl"]
      }/l/event/${eventId}/stages/${defaultStageId}`;
      await orgPage.goto(stageUrl);
      orgStagePage = new StagePage(orgPage);
      await orgStagePage.closeWelcomeStagePopUp();
    });

    await test.step(`Speaker goes to magic link and enters into the event and then go to stage`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );
      await speakerPage.goto(speakerMagicLink);
      await speakerPage.click("text=Enter Now");
      await speakerPage.goto(stageUrl);
    });

    await test.step(`Organiser creates a poll data`, async () => {
      pollController = new PollsOrQuizController(orgApiContext, eventId);
      pollTitle = " Organiser Poll 1";
      pollOptions = [{ text: "option1" }, { text: "option2" }];
      await test.step(`Organiser creates a new poll by api with title ${pollTitle} in draft status`, async () => {
        const pollCreatedInDraftResp =
          await pollController.createNewPollInDraft(defaultStageChannelId, {
            prompt: pollTitle,
            options: pollOptions,
          });
      });
    });

    await test.step(`Speaker verifies there is a poll visible on drafted tab`, async () => {
      speakerStagePage = new StagePage(speakerPage);
      await speakerStagePage.closeWelcomeStagePopUp();
      speakerPollComponent = await speakerStagePage.openPollsTab();
      await speakerPollComponent.openDraftPollsTab();
      await speakerPollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Organiser now deletes the drafted poll`, async () => {
      pollId = await pollController.getPollId(defaultStageChannelId, pollTitle);
      await pollController.deleteDraftedPollOrQuiz(pollId);
    });

    await test.step(`Verify speaker does not see any drafted poll`, async () => {
      await speakerPollComponent.verifyPollIsNotVisibleOnList(pollTitle);
    });

    await test.step(`Speaker creates a poll data`, async () => {
      speakerPollController = new PollsOrQuizController(
        speakerApiContext,
        eventId
      );
      pollTitle = "Speaker Poll 1";
      pollOptions = [{ text: "option1" }, { text: "option2" }];
      await test.step(`Speaker creates a new poll by api with title ${pollTitle} in draft status`, async () => {
        await speakerPollController.createNewPollInDraft(
          defaultStageChannelId,
          {
            prompt: pollTitle,
            options: pollOptions,
          }
        );
      });
    });

    await test.step(`Speaker verifies there is a poll visible on drafted tab`, async () => {
      await speakerPage.reload();
      await speakerStagePage.closeWelcomeStagePopUp();
      speakerPollComponent = await speakerStagePage.openPollsTab();
      await speakerPollComponent.openDraftPollsTab();
      await speakerPollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Speaker now deletes the drafted poll`, async () => {
      pollId = await speakerPollController.getPollId(
        defaultStageChannelId,
        pollTitle
      );
      await speakerPollController.deleteDraftedPollOrQuiz(pollId);
    });

    await test.step(`Verify organiser does not see any drafted poll`, async () => {
      await orgPage.reload();
      await orgStagePage.closeWelcomeStagePopUp();
      orgPollComponent = await orgStagePage.openPollsTab();
      await orgPollComponent.verifyPollIsNotVisibleOnList(pollTitle);
    });
  });

  test("TC006 : verify e2e flow of polls tab being not visible in live-event nav bar components for all users", async ({
    browser,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-406/verify-polls-tab-is-not-visible-on-lobby-interaction-panel",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-407/verify-polls-tab-is-not-visible-on-stage-listing",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-408/verify-polls-tab-is-not-visible-on-rooms-listing",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-409/verify-polls-tab-is-not-visible-on-networking-interaction-panel",
    });

    let orgPollComponent: PollsComponent;
    let speakerPollComponent: PollsComponent;
    let attendeePollComponent: PollsComponent;
    let attendeeZonePage: StagePage;
    let speakerZonePage: StagePage;
    let orgZonePage: StagePage;
    let speakerAvModal: AVModal;
    let attendeeAvModal: AVModal;
    let orgAvModal: AVModal;
    let orgNavBar: TopNavBarComponent;
    let speakerNavBar: TopNavBarComponent;
    let attendeeNavBar: TopNavBarComponent;
    let lobbyUrl: string;
    let attendeePage = await (
      await BrowserFactory.getBrowserContext({ laodOrganiserCookies: false })
    ).newPage();
    let speakerPage = await (
      await BrowserFactory.getBrowserContext({ laodOrganiserCookies: false })
    ).newPage();
    let orgPage = await orgBrowserContext.newPage();
    const attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;

    await test.step(`Create an event with rooms, expo, and schedule session`, async () => {
      await eventController.eventCreationWithMultipleVenueSetup();
    });

    await test.step(`Organiser goes to attendee landing page and enter into the event and then goes to LOBBY`, async () => {
      await orgPage.goto(attendeeLandingPage);
      await orgPage.click("text=Enter Now");
      lobbyUrl = `${
        DataUtl.getApplicationTestDataObj()["baseUrl"]
      }/l/event/${eventId}/lobby/`;
      await orgPage.goto(lobbyUrl);
    });

    await test.step(`Attendee goes to magic link and enters into the event and then go to LOBBY`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.click("text=Enter Now");
      await attendeePage.waitForURL(/lobby/);
      await attendeePage.click("text=Skip");
    });

    await test.step(`Speaker goes to magic link and enters into the event and then go to LOBBY`, async () => {
      const speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );
      await speakerPage.goto(speakerMagicLink);
      await speakerPage.click("text=Enter Now");
      await speakerPage.goto(lobbyUrl);
      await speakerPage.click("text=Skip");
    });

    await test.step(`Attendee verifies Polls tab being not visible in LOBBY zone`, async () => {
      await test.step(`Attendee opens the interaction panel`, async () => {
        attendeeZonePage = new StagePage(attendeePage);
        attendeePollComponent =
          await attendeeZonePage.clickOnChatIconOfClosedInteractionPanelState();
      });
      await attendeePollComponent.verifyPollsTabtoBeInvisibleInInteractionPanel();
    });

    await test.step(`Speaker verifies Polls tab being not visible in LOBBY zone`, async () => {
      await test.step(`Speaker opens the interaction panel`, async () => {
        speakerZonePage = new StagePage(speakerPage);
        speakerPollComponent =
          await speakerZonePage.clickOnChatIconOfClosedInteractionPanelState();
      });
      await speakerPollComponent.verifyPollsTabtoBeInvisibleInInteractionPanel();
    });

    await test.step(`Organiser verifies Polls tab being not visible in LOBBY zone`, async () => {
      await test.step(`Organiser opens the interaction panel`, async () => {
        orgZonePage = new StagePage(orgPage);
        orgPollComponent =
          await orgZonePage.clickOnChatIconOfClosedInteractionPanelState();
      });
      await orgPollComponent.verifyPollsTabtoBeInvisibleInInteractionPanel();
    });

    await test.step(`Organiser switches to SCHEDULE zone and verifies invisibility of Polls tab`, async () => {
      orgNavBar = new TopNavBarComponent(orgPage);
      await orgNavBar.switchToZone(ZoneType.SCHEDULE);
      await test.step(`Organiser opens the interaction panel`, async () => {
        orgZonePage = new StagePage(orgPage);
        orgPollComponent =
          await orgZonePage.clickOnChatIconOfClosedInteractionPanelState();
      });
      await orgPollComponent.verifyPollsTabtoBeInvisibleInInteractionPanel();
    });

    await test.step(`Speaker switches to SCHEDULE zone and verifies invisibility of Polls tab`, async () => {
      speakerNavBar = new TopNavBarComponent(speakerPage);
      await speakerNavBar.switchToZone(ZoneType.SCHEDULE);
      await test.step(`Speaker opens the interaction panel`, async () => {
        speakerPollComponent =
          await speakerZonePage.clickOnChatIconOfClosedInteractionPanelState();
      });
      await speakerPollComponent.verifyPollsTabtoBeInvisibleInInteractionPanel();
    });

    await test.step(`Attendee switches to SCHEDULE zone and verifies invisibility of Polls tab`, async () => {
      attendeeNavBar = new TopNavBarComponent(attendeePage);
      await attendeeNavBar.switchToZone(ZoneType.SCHEDULE);
      await test.step(`Attendee opens the interaction panel`, async () => {
        attendeeZonePage = new StagePage(attendeePage);
        attendeePollComponent =
          await attendeeZonePage.clickOnChatIconOfClosedInteractionPanelState();
      });
      await attendeePollComponent.verifyPollsTabtoBeInvisibleInInteractionPanel();
    });

    await test.step(`Organiser switches to STAGE zone and verifies invisibility of Polls tab`, async () => {
      orgNavBar = new TopNavBarComponent(orgPage);
      await orgNavBar.switchToZone(ZoneType.STAGE);
      await test.step(`Organiser opens the interaction panel`, async () => {
        orgPollComponent =
          await orgZonePage.clickOnChatIconOfClosedInteractionPanelState();
      });
      await orgPollComponent.verifyPollsTabtoBeInvisibleInInteractionPanel();
    });

    await test.step(`Speaker switches to STAGE zone and verifies invisibility of Polls tab`, async () => {
      speakerNavBar = new TopNavBarComponent(speakerPage);
      await speakerNavBar.switchToZone(ZoneType.STAGE);
      await test.step(`Speaker opens the interaction panel`, async () => {
        speakerPollComponent =
          await speakerZonePage.clickOnChatIconOfClosedInteractionPanelState();
      });
      await speakerPollComponent.verifyPollsTabtoBeInvisibleInInteractionPanel();
    });

    await test.step(`Attendee switches to STAGE zone and verifies invisibility of Polls tab`, async () => {
      attendeeNavBar = new TopNavBarComponent(attendeePage);
      await attendeeNavBar.switchToZone(ZoneType.STAGE);
      await test.step(`Attendee opens the interaction panel`, async () => {
        attendeeZonePage = new StagePage(attendeePage);
        attendeePollComponent =
          await attendeeZonePage.clickOnChatIconOfClosedInteractionPanelState();
      });
      await attendeePollComponent.verifyPollsTabtoBeInvisibleInInteractionPanel();
    });

    await test.step(`Speaker switches to ROOMS zone and verifies invisibility of Polls tab`, async () => {
      speakerAvModal = new AVModal(speakerPage);
      let speakerZonePage = new RoomsListingPage(speakerPage);
      await speakerNavBar.switchToZone(ZoneType.ROOMS);
      await speakerPage.waitForTimeout(3000);
      await speakerAvModal.clickOnJoinButtonOnAvModal();
      await test.step(`Speaker opens the Event Tab`, async () => {
        await speakerZonePage.getChatComponent.clickOnEventTab();
      });
      await speakerPollComponent.verifyPollsTabtoBeInvisibleInInteractionPanel();
      await speakerZonePage.exitTheRoom();
    });

    await test.step(`Organiser switches to ROOMS zone and verifies invisibility of Polls tab`, async () => {
      orgAvModal = new AVModal(orgPage);
      let orgZonePage = new RoomsListingPage(orgPage);
      await orgNavBar.switchToZone(ZoneType.ROOMS);
      await orgPage.waitForTimeout(3000);
      await orgAvModal.clickOnJoinButtonOnAvModal();
      await test.step(`Organiser opens the Event Tab`, async () => {
        await orgZonePage.getChatComponent.clickOnEventTab();
      });
      await orgPollComponent.verifyPollsTabtoBeInvisibleInInteractionPanel();
      await orgZonePage.exitTheRoom();
    });

    await test.step(`Attendee switches to ROOMS zone and verifies invisibility of Polls tab`, async () => {
      attendeeAvModal = new AVModal(attendeePage);
      let attendeeZonePage = new RoomsListingPage(attendeePage);
      await attendeeNavBar.switchToZone(ZoneType.ROOMS);
      await attendeePage.waitForTimeout(3000);
      await attendeeAvModal.clickOnJoinButtonOnAvModal();
      await test.step(`Attendee opens the Event Tab`, async () => {
        await attendeeZonePage.getChatComponent.clickOnEventTab();
      });
      await attendeePollComponent.verifyPollsTabtoBeInvisibleInInteractionPanel();
      await attendeeZonePage.exitTheRoom();
    });

    await test.step(`Organiser switches to EXPO zone and verifies invisibility of Polls tab`, async () => {
      await orgNavBar.switchToZone(ZoneType.EXPO);
      await orgPollComponent.verifyPollsTabtoBeInvisibleInInteractionPanel();
    });

    await test.step(`Speaker switches to EXPO zone and verifies invisibility of Polls tab`, async () => {
      await speakerNavBar.switchToZone(ZoneType.EXPO);
      await speakerPollComponent.verifyPollsTabtoBeInvisibleInInteractionPanel();
    });

    await test.step(`Attendee switches to EXPO zone and verifies invisibility of Polls tab`, async () => {
      await attendeeNavBar.switchToZone(ZoneType.EXPO);
      await attendeePollComponent.verifyPollsTabtoBeInvisibleInInteractionPanel();
    });

    await test.step(`Organiser switches to NETWORKING zone and verifies invisibility of Polls tab`, async () => {
      await orgNavBar.switchToZone(ZoneType.NETWORKING);
      await orgPollComponent.verifyPollsTabtoBeInvisibleInInteractionPanel();
    });

    await test.step(`Speaker switches to NETWORKING zone and verifies invisibility of Polls tab`, async () => {
      await speakerNavBar.switchToZone(ZoneType.NETWORKING);
      await speakerPollComponent.verifyPollsTabtoBeInvisibleInInteractionPanel();
    });

    await test.step(`Attendee switches to NETWORKING zone and verifies invisibility of Polls tab`, async () => {
      await attendeeNavBar.switchToZone(ZoneType.NETWORKING);
      await attendeePollComponent.verifyPollsTabtoBeInvisibleInInteractionPanel();
    });
  });

  test("TC007 : verify flow of creation of polls by organiser/speaker in both Rooms and Expo zones", async ({
    browser,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-410/verify-by-api-that-organiserspeaker-can-create-poll-in-room-channel",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-411/verify-by-api-that-organiserspeaker-can-create-poll-in-expo-channel",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-413/verify-the-presence-of-polls-tab-inside-an-expo-booth",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-412/verify-the-presence-of-polls-tab-inside-a-room",
    });

    let orgRoomPage: RoomsListingPage;
    let speakerRoomPage: RoomsListingPage;
    let attendeeRoomPage: RoomsListingPage;
    let orgAvModal: AVModal;
    let speakerAvModal: AVModal;
    let attendeeAvModal: AVModal;
    let pollController: PollsOrQuizController;
    let orgPollComponent: PollsComponent;
    let speakerPollComponent: PollsComponent;
    let attendeePollComponent: PollsComponent;
    let orgNavBar: TopNavBarComponent;
    let speakerNavBar: TopNavBarComponent;
    let attendeeNavBar: TopNavBarComponent;
    let orgExpoPage: ExpoPage;
    let speakerExpoPage: ExpoPage;
    let attendeeExpoPage: ExpoPage;
    let pollTitle: string;
    let roomId: string;
    let roomChannelId: string;
    let boothId: string;
    let boothChannelId: string;
    let pollOptions: any[];
    let roomsUrl: string;
    let attendeePage = await (
      await BrowserFactory.getBrowserContext({ laodOrganiserCookies: false })
    ).newPage();
    let speakerPage = await (
      await BrowserFactory.getBrowserContext({ laodOrganiserCookies: false })
    ).newPage();
    let speakerApiContext = speakerPage.request;
    let orgPage = await orgBrowserContext.newPage();
    const attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;

    await test.step(`Create an event with rooms, expo, and schedule session`, async () => {
      await eventController.eventCreationWithMultipleVenueSetup();
    });

    await test.step(`Get room channel Id to post polls`, async () => {
      await test.step(`Get room Id for this event`, async () => {
        roomId = await eventController.getRoomId(eventId);
      });
      roomChannelId = await eventController.getChannelId(roomId);
    });
    await test.step(`Get expo channel Id to post polls`, async () => {
      await test.step(`Get booth Id for this event`, async () => {
        boothId = await eventController.getBoothId(eventId);
      });
      boothChannelId = await eventController.getChannelId(boothId);
    });

    await test.step(`Organiser goes to attendee landing page and enter into the event and then goes to ROOMS`, async () => {
      await orgPage.goto(attendeeLandingPage);
      await orgPage.click("text=Enter Now");
      roomsUrl = `${
        DataUtl.getApplicationTestDataObj()["baseUrl"]
      }/l/event/${eventId}/discussions/`;
      await orgPage.goto(roomsUrl);
    });

    await test.step(`Organiser now enters into the ROOMS`, async () => {
      orgRoomPage = new RoomsListingPage(orgPage);
      orgAvModal = new AVModal(orgPage);
      await orgRoomPage.enterIntoRoom();
      await orgAvModal.muteAudioVideoStreams();
      await orgAvModal.clickOnJoinButtonOnAvModal();
    });

    await test.step(`Organiser verifies create Poll button to be visible in ROOMS`, async () => {
      orgPollComponent = await orgRoomPage.openPollsTab();
      await test.step(`Organiser verifies Create Poll button to be visible in ROOMS`, async () => {
        await orgPollComponent.verifyCreatePollTabToBePresent();
      });
    });

    await test.step(`Organiser creates a new poll via API`, async () => {
      pollController = new PollsOrQuizController(orgApiContext, eventId);
      pollTitle = " Organiser Poll 1";
      pollOptions = [{ text: "option1" }, { text: "option2" }];
      await test.step(`Organiser creates a new poll by api with title ${pollTitle} in publish status`, async () => {
        const pollCreatedInPublishResp =
          await pollController.createNewPollInPublish(roomChannelId, {
            prompt: pollTitle,
            options: pollOptions,
          });
      });
    });

    await test.step(`Speaker goes to magic link and enters into the event and then go to ROOMS`, async () => {
      const speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );
      await speakerPage.goto(speakerMagicLink);
      await speakerPage.click("text=Enter Now");
      await speakerPage.goto(roomsUrl);
      await speakerPage.click("text=Skip");
    });

    await test.step(`Speaker now enters into the ROOMS`, async () => {
      speakerRoomPage = new RoomsListingPage(speakerPage);
      speakerAvModal = new AVModal(speakerPage);
      await speakerRoomPage.enterIntoRoom();
      await speakerAvModal.muteAudioVideoStreams();
      await speakerAvModal.clickOnJoinButtonOnAvModal();
    });

    await test.step(`Speaker verifies there is a poll visible on published tab`, async () => {
      speakerPollComponent = await speakerRoomPage.openPollsTab();
      await test.step(`Speaker verifies Create Poll button to be visible`, async () => {
        await speakerPollComponent.verifyCreatePollTabToBePresent();
      });
      await test.step(`Speaker verifies Published Poll tab to be visible`, async () => {
        await speakerPollComponent.verifyPublishedPollTabToBePresent();
      });
      await speakerPollComponent.openPublishedPollsTab();
      await speakerPollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Attendee goes to magic link and enters into the event and then go to ROOMS`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.click("text=Enter Now");
      await attendeePage.goto(roomsUrl);
      await attendeePage.click("text=Skip");
    });

    await test.step(`Attendee goes to attendee landing page and enter into the event and then goes to ROOMS`, async () => {
      attendeeRoomPage = new RoomsListingPage(attendeePage);
      attendeeAvModal = new AVModal(attendeePage);
      await attendeeRoomPage.enterIntoRoom();
      await attendeeAvModal.muteAudioVideoStreams();
      await attendeeAvModal.clickOnJoinButtonOnAvModal();
    });

    await test.step(`Attendee now verifies newly created poll`, async () => {
      attendeePollComponent = await attendeeRoomPage.openPollsTab();
      await test.step(`Attendee verifies Poll tab to be visible in ROOMS`, async () => {
        await attendeePollComponent.verifyPollsTabtoBeVisibleInInteractionPanel();
      });
      await attendeePollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Speaker creates a poll data`, async () => {
      const pollController = new PollsOrQuizController(
        speakerApiContext,
        eventId
      );
      pollTitle = "Speaker Poll 1";
      pollOptions = [{ text: "option1" }, { text: "option2" }];
      await test.step(`Speaker creates a new poll by api with title ${pollTitle} in publish status`, async () => {
        const pollCreatedInPublishtResp =
          await pollController.createNewPollInPublish(roomChannelId, {
            prompt: pollTitle,
            options: pollOptions,
          });
      });
    });

    await test.step(`Organiser now verifies newly created poll`, async () => {
      await test.step(`Organiser verifies Published Poll tab to be visible`, async () => {
        await orgPollComponent.verifyPublishedPollTabToBePresent();
      });
      await orgPollComponent.openPublishedPollsTab();
      await orgPollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Attendee now verifies newly created poll`, async () => {
      await attendeePage.reload();
      await attendeeAvModal.muteAudioVideoStreams();
      await attendeeAvModal.clickOnJoinButtonOnAvModal();
      attendeePollComponent = await attendeeRoomPage.openPollsTab();
      await attendeePollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`All users exits the room`, async () => {
      await orgRoomPage.exitTheRoom();
      await speakerRoomPage.exitTheRoom();
      await attendeeRoomPage.exitTheRoom();
    });

    await test.step(`All Users switches to EXPO and enter into a booth`, async () => {
      orgNavBar = new TopNavBarComponent(orgPage);
      speakerNavBar = new TopNavBarComponent(speakerPage);
      attendeeNavBar = new TopNavBarComponent(attendeePage);
      orgExpoPage = new ExpoPage(orgPage);
      speakerExpoPage = new ExpoPage(speakerPage);
      attendeeExpoPage = new ExpoPage(attendeePage);
      await orgNavBar.switchToZone(ZoneType.EXPO);
      await orgExpoPage.enterIntoBooth();
      await speakerNavBar.switchToZone(ZoneType.EXPO);
      await speakerExpoPage.enterIntoBooth();
      await attendeeNavBar.switchToZone(ZoneType.EXPO);
      await attendeeExpoPage.enterIntoBooth();
    });
    await test.step(`Organiser verifies create Poll button to be visible in EXPO`, async () => {
      //await orgExpoPage.clickOnChatIconOfClosedInteractionPanelState();
      orgPollComponent = await orgRoomPage.openPollsTab();
      await test.step(`Organiser verifies Create Poll button to be visible in EXPO`, async () => {
        await orgPollComponent.verifyCreatePollTabToBePresent();
      });
    });

    await test.step(`Organiser creates a new poll via API`, async () => {
      pollController = new PollsOrQuizController(orgApiContext, eventId);
      pollTitle = " Organiser Poll 1";
      pollOptions = [{ text: "option1" }, { text: "option2" }];
      await test.step(`Organiser creates a new poll by api with title ${pollTitle} in publish status`, async () => {
        const pollCreatedInPublishResp =
          await pollController.createNewPollInPublish(boothChannelId, {
            prompt: pollTitle,
            options: pollOptions,
          });
      });
    });

    await test.step(`Organiser verifies there a poll visible on published tab`, async () => {
      await orgPage.reload();
      await orgExpoPage.clickOnChatIconOfClosedInteractionPanelState();
      orgPollComponent = await orgRoomPage.openPollsTab();
      await test.step(`Organiser verifies Published Poll tab to be visible`, async () => {
        await orgPollComponent.verifyPublishedPollTabToBePresent();
      });
      await orgPollComponent.openPublishedPollsTab();
      await orgPollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Speaker verifies there a poll visible on published tab`, async () => {
      await speakerPage.reload();
      await speakerExpoPage.clickOnChatIconOfClosedInteractionPanelState();
      speakerPollComponent = await speakerRoomPage.openPollsTab();
      await speakerPollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Attendee verifies there a poll visible on published tab`, async () => {
      await attendeePage.reload();
      await attendeeExpoPage.clickOnChatIconOfClosedInteractionPanelState();
      attendeePollComponent = await attendeeRoomPage.openPollsTab();
      await test.step(`Attendee verifies Poll tab to be visible in EXPO `, async () => {
        await attendeePollComponent.verifyPollsTabtoBeVisibleInInteractionPanel();
      });
      await attendeePollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Speaker creates a poll data`, async () => {
      const pollController = new PollsOrQuizController(
        speakerApiContext,
        eventId
      );
      pollTitle = "Speaker Poll 1";
      pollOptions = [{ text: "option1" }, { text: "option2" }];
      await test.step(`Speaker creates a new poll by api with title ${pollTitle} in publish status`, async () => {
        const pollCreatedInPublishtResp =
          await pollController.createNewPollInPublish(boothChannelId, {
            prompt: pollTitle,
            options: pollOptions,
          });
      });
    });

    await test.step(`Organiser verifies there a poll visible on published tab`, async () => {
      await orgPage.reload();
      await orgExpoPage.clickOnChatIconOfClosedInteractionPanelState();
      orgPollComponent = await orgRoomPage.openPollsTab();
      await orgPollComponent.openPublishedPollsTab();
      await orgPollComponent.verifyPollIsVisibleOnList(pollTitle);
    });

    await test.step(`Attendee verifies there a poll visible on published tab`, async () => {
      await attendeePage.reload();
      await attendeeExpoPage.clickOnChatIconOfClosedInteractionPanelState();
      attendeePollComponent = await attendeeRoomPage.openPollsTab();
      await attendeePollComponent.verifyPollIsVisibleOnList(pollTitle);
    });
  });

  test("TC008 : verify e2e flow of organiser publishing a drafted quiz which gets voted by attendee and speaker", async ({
    browser,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-390/verify-flow-of-organiser-creating-a-poll-in-drafted-status-and-then",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-391/verify-polls-drafted-by-organiser-is-visible-to-speakers-and-can-be",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-397/verify-attendees-speaker-and-organiser-all-can-cast-vote-on-poll",
    });

    let orgStagePage: StagePage;
    let attendeeStagePage: StagePage;
    let speakerStagePage: StagePage;
    let orgPollComponent: PollsComponent;
    let attendeePollComponent: PollsComponent;
    let speakerPollComponent: PollsComponent;
    let pollController: PollsOrQuizController;
    let attendeeMagicLink: string;
    let speakerMagicLink: string;
    let stageUrl: string;
    let quizTitle: string;
    let quizOptions: any[];
    let attendeePage = await (await browser.newContext()).newPage();
    let speakerPage = await (await browser.newContext()).newPage();
    let orgPage = await orgBrowserContext.newPage();
    let speakerApiContext = speakerPage.request;
    const attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;

    await test.step(`Organiser creates a quiz data`, async () => {
      pollController = new PollsOrQuizController(orgApiContext, eventId);
      quizTitle = "Org Quiz 1";
      quizOptions = [{ text: "option1" }, { text: "option2" }];
      await test.step(`Create a new quiz by api with title ${quizTitle} in draft status`, async () => {
        await pollController.createNewQuizInDraft(defaultStageChannelId, {
          prompt: quizTitle,
          options: quizOptions,
        });
      });
    });

    await test.step(`Organiser goes to attendee landing page and enter into the event and then goes to stage`, async () => {
      await orgPage.goto(attendeeLandingPage);
      await orgPage.click("text=Enter Now");
      stageUrl = `${
        DataUtl.getApplicationTestDataObj()["baseUrl"]
      }/l/event/${eventId}/stages/${defaultStageId}`;
      await orgPage.goto(stageUrl);
      orgStagePage = new StagePage(orgPage);
      await orgStagePage.closeWelcomeStagePopUp();
    });

    await test.step(`Organiser opens Polls tab and then opens draft section`, async () => {
      orgPollComponent = await orgStagePage.openPollsTab();
      await orgPollComponent.openDraftPollsTab();
    });

    await test.step(`Verify organiser is able to view the quiz on draft tab`, async () => {
      await orgPollComponent.verifyPollIsVisibleOnList(quizTitle);
    });

    await test.step(`Attendee goes to magic link and enters into the event and then go to stage`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.click("text=Enter Now");
      await attendeePage.goto(stageUrl);
    });

    await test.step(`Attendee verifies there is no quiz visible on his poll tab`, async () => {
      attendeeStagePage = new StagePage(attendeePage);
      attendeePollComponent = await attendeeStagePage.openPollsTab();
      await attendeePollComponent.verifyPollIsNotVisibleOnList(quizTitle);
    });

    await test.step(`Speaker goes to magic link and enters into the event and then go to stage`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );
      await speakerPage.goto(speakerMagicLink);
      await speakerPage.click("text=Enter Now");
      await speakerPage.goto(stageUrl);
    });

    await test.step(`Speaker verifies there is a quiz visible on draft tab`, async () => {
      speakerStagePage = new StagePage(speakerPage);
      await speakerStagePage.closeWelcomeStagePopUp();
      speakerPollComponent = await speakerStagePage.openPollsTab();
      await speakerPollComponent.openDraftPollsTab();
      await speakerPollComponent.verifyPollIsVisibleOnList(quizTitle);
    });

    await test.step(`Organiser publishes the quiz from the drafted tab`, async () => {
      await orgPollComponent.publishPollWithTitle(quizTitle);
      await test.step(`Verify organiser sees the quiz published`, async () => {
        await orgPollComponent.openPublishedPollsTab();
        await orgPollComponent.verifyPollIsVisibleOnList(quizTitle);
      });
    });

    await test.step(`Speaker verifies he sees the quiz on the publised tab`, async () => {
      await speakerPollComponent.verifyPollIsNotVisibleOnList(quizTitle);
      await speakerPollComponent.openPublishedPollsTab();
      await speakerPollComponent.verifyPollIsVisibleOnList(quizTitle);
    });

    await test.step(`Attendee verifies he sees the quiz now`, async () => {
      await attendeePollComponent.verifyPollIsVisibleOnList(quizTitle);
    });

    await test.step(`Attendee votes on option 1`, async () => {
      await attendeePollComponent.selectOptionToVoteOnPoll(
        quizTitle,
        quizOptions[0]["text"]
      );
    });

    await test.step(`Speaker votes on option 1`, async () => {
      await speakerPollComponent.selectOptionToVoteOnPoll(
        quizTitle,
        quizOptions[0]["text"]
      );
    });

    await test.step(`Organiser votes on option 1`, async () => {
      await orgPollComponent.selectOptionToVoteOnPoll(
        quizTitle,
        quizOptions[0]["text"]
      );
    });

    //verify count of votes to each user
    await test.step(`Verify organiser sees 3 vote count on the poll via API`, async () => {
      const voteCount = await pollController.getCountOfVotedPoll(
        defaultStageChannelId,
        quizTitle
      );
      expect(voteCount).toEqual(3);
    });

    await test.step(`Verify speaker sees 3 vote count on the poll`, async () => {
      await speakerPage.reload();
      await speakerStagePage.closeWelcomeStagePopUp();
      await speakerStagePage.openPollsTab();
      await speakerPollComponent.openPublishedPollsTab();
      await speakerPollComponent.verifyVoteCountOnPollMatches(quizTitle, 3);
    });

    await test.step(`Verify attendee does not see vote count on poll`, async () => {
      await attendeePollComponent.verifyVoteCountToShowEmpty(quizTitle);
    });

    await test.step(`Speaker creates a quiz data`, async () => {
      pollController = new PollsOrQuizController(speakerApiContext, eventId);
      quizTitle = "Speaker Quiz 1";
      quizOptions = [{ text: "option1" }, { text: "option2" }];
      await test.step(`Create a new quiz by api with title ${quizTitle} in draft status`, async () => {
        await pollController.createNewQuizInDraft(defaultStageChannelId, {
          prompt: quizTitle,
          options: quizOptions,
        });
      });
    });

    await test.step(`Verify speaker is able to view the quiz on draft tab`, async () => {
      await speakerPage.reload();
      await speakerStagePage.closeWelcomeStagePopUp();
      await speakerStagePage.openPollsTab();
      await speakerPollComponent.verifyPollIsVisibleOnList(quizTitle);
    });

    await test.step(`Verify organiser is able to view the quiz on draft tab`, async () => {
      await orgPage.reload();
      await orgStagePage.closeWelcomeStagePopUp();
      await orgStagePage.openPollsTab();
      await orgPollComponent.verifyPollIsVisibleOnList(quizTitle);
    });

    await test.step(`Attendee verifies there is no quiz visible on his poll tab`, async () => {
      await attendeePage.reload();
      attendeePollComponent = await attendeeStagePage.openPollsTab();
      await attendeePollComponent.verifyPollIsNotVisibleOnList(quizTitle);
    });

    await test.step(`Speaker publishes the quiz from the drafted tab`, async () => {
      await speakerPollComponent.publishPollWithTitle(quizTitle);
      await test.step(`Verify Speaker sees the quiz published`, async () => {
        await speakerPollComponent.openPublishedPollsTab();
        await speakerPollComponent.verifyPollIsVisibleOnList(quizTitle);
      });
    });

    await test.step(`Organiser verifies he sees the quiz on the published tab`, async () => {
      await orgPollComponent.verifyPollIsNotVisibleOnList(quizTitle);
      await orgPollComponent.openPublishedPollsTab();
      await orgPollComponent.verifyPollIsVisibleOnList(quizTitle);
    });

    await test.step(`Attendee verifies he sees the quiz now`, async () => {
      await attendeePage.reload();
      attendeePollComponent = await attendeeStagePage.openPollsTab();
      await attendeePollComponent.verifyPollIsVisibleOnList(quizTitle);
    });

    await test.step(`Attendee votes on option 1`, async () => {
      await attendeePollComponent.selectOptionToVoteOnPoll(
        quizTitle,
        quizOptions[0]["text"]
      );
    });

    await test.step(`Speaker votes on option 1`, async () => {
      await speakerPollComponent.selectOptionToVoteOnPoll(
        quizTitle,
        quizOptions[0]["text"]
      );
    });

    await test.step(`Organiser votes on option 1`, async () => {
      await orgPollComponent.selectOptionToVoteOnPoll(
        quizTitle,
        quizOptions[0]["text"]
      );
    });
    //verify count of votes to each user
    await test.step(`Verify organiser sees 3 vote count on the poll via API`, async () => {
      const voteCount = await pollController.getCountOfVotedPoll(
        defaultStageChannelId,
        quizTitle
      );
      expect(voteCount).toEqual(3);
    });

    await test.step(`Verify speaker sees 3 vote count on the poll`, async () => {
      await speakerPage.reload();
      await speakerStagePage.closeWelcomeStagePopUp();
      await speakerStagePage.openPollsTab();
      await speakerPollComponent.openPublishedPollsTab();
      await speakerPollComponent.verifyVoteCountOnPollMatches(quizTitle, 3);
    });

    await test.step(`Verify attendee does not see vote count on poll`, async () => {
      await attendeePollComponent.verifyVoteCountToShowEmpty(quizTitle);
    });
  });

  test("TC009 : verify when quiz is edited by organiser, there is a presence with updated content on the published tab/polls tab for attendee and speaker", async ({
    browser,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-416/verify-flow-of-editing-a-quiz-and-changing-the-correct-answer-and-same",
    });
    let orgStagePage: StagePage;
    let attendeeStagePage: StagePage;
    let orgPollComponent: PollsComponent;
    let attendeePollComponent: PollsComponent;
    let pollController: PollsOrQuizController;
    let attendeeMagicLink: string;
    let speakerMagicLink: string;
    let optionToAdd: string;
    let stageUrl: string;
    let quizTitle: string;
    let quizOptions: any[];
    let attendeePage = await (await browser.newContext()).newPage();
    let speakerPage = await (await browser.newContext()).newPage();
    let orgPage = await orgBrowserContext.newPage();
    let beforeUpdationQuizDetails;
    let afterUpdationQuizDetails;
    let speakerApiContext = speakerPage.request;
    const attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
      .attendeeLandingPage;

    await test.step(`Organiser goes to attendee landing page and enter into the event and then goes to stage`, async () => {
      await orgPage.goto(attendeeLandingPage);
      await orgPage.click("text=Enter Now");
      stageUrl = `${
        DataUtl.getApplicationTestDataObj()["baseUrl"]
      }/l/event/${eventId}/stages/${defaultStageId}`;
      await orgPage.goto(stageUrl);
      orgStagePage = new StagePage(orgPage);
      await orgStagePage.closeWelcomeStagePopUp();
    });

    await test.step(`Attendee goes to magic link and enters into the event and then go to stage`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.click("text=Enter Now");
      await attendeePage.goto(stageUrl);
    });

    await test.step(`Speaker goes to magic link and enters into the event and then go to stage`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );
      await speakerPage.goto(speakerMagicLink);
      await speakerPage.click("text=Enter Now");
      await speakerPage.goto(stageUrl);
    });

    await test.step(`Organiser creates a quiz data`, async () => {
      pollController = new PollsOrQuizController(orgApiContext, eventId);
      quizTitle = "Org Quiz Before Edits";
      quizOptions = [{ text: "option1" }, { text: "option2" }];
      await test.step(`Create a new quiz by api with title ${quizTitle} in draft status`, async () => {
        await pollController.createNewQuizInDraft(defaultStageChannelId, {
          prompt: quizTitle,
          options: quizOptions,
        });
      });
    });

    await test.step(`Speaker saves the Quiz details before edition`, async () => {
      const speakerPollController = new PollsOrQuizController(
        speakerApiContext,
        eventId
      );
      beforeUpdationQuizDetails = await speakerPollController.getPollDetails(
        defaultStageChannelId,
        quizTitle
      );
    });

    await test.step(`organiser edits the quiz by adding new option`, async () => {
      optionToAdd = "option3";
      let pollTest = await pollController.addNewOption(
        defaultStageChannelId,
        quizTitle,
        {
          text: optionToAdd,
        }
      );

      await pollController.updateADraftedPollOrQuiz(
        await pollController.getPollId(defaultStageChannelId, quizTitle),
        {
          options: pollTest,
          isQuiz: true,
        }
      );
      afterUpdationQuizDetails = await pollController.getPollDetails(
        defaultStageChannelId,
        quizTitle
      );
    });

    await test.step(`Speaker verifies that quiz is being updated in draft state`, async () => {
      expect(afterUpdationQuizDetails["status"]).toEqual(PollStatus.SCHEDULED);
      expect(afterUpdationQuizDetails["options"].length).toBeGreaterThan(
        beforeUpdationQuizDetails["options"].length
      );
    });

    await test.step(`Organiser opens Polls tab and then opens draft section`, async () => {
      await orgPage.reload();
      await orgStagePage.closeWelcomeStagePopUp();
      orgPollComponent = await orgStagePage.openPollsTab();
      await orgPollComponent.openDraftPollsTab();
    });

    await test.step(`Organiser publishes the quiz from the drafted tab`, async () => {
      await orgPollComponent.publishPollWithTitle(quizTitle);
      await test.step(`Verify organiser sees the poll published`, async () => {
        await orgPollComponent.openPublishedPollsTab();
        await orgPollComponent.verifyPollIsVisibleOnList(quizTitle);
      });
    });

    await test.step(`Attendee verifies he sees the quiz with new option now`, async () => {
      attendeeStagePage = new StagePage(attendeePage);
      await attendeePage.reload();
      attendeePollComponent = await attendeeStagePage.openPollsTab();
      await attendeePollComponent.verifyPollIsVisibleOnList(quizTitle);
      await attendeePollComponent.verifyExpectedOptionVisibleinPoll(
        optionToAdd
      );
    });

    await test.step(`Attendee votes on option 1`, async () => {
      await attendeePollComponent.selectOptionToVoteOnPoll(
        quizTitle,
        quizOptions[0]["text"]
      );
    });

    await test.step(`Verify organiser sees 1 vote count on the poll via API`, async () => {
      const voteCount = await pollController.getCountOfVotedPoll(
        defaultStageChannelId,
        quizTitle
      );
      expect(voteCount).toEqual(1);
    });
  });
});
