import test, {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
} from "@playwright/test";
import EmailRestrictionType from "../../../enums/emailRestrictionTypeEnum";
import { EmailDomainRestrictionComponent } from "../../../page-objects/new-org-side-pages/EmailDomainRestrictionComponent";
import {
  createNewEvent,
  updateEmailRestrictionSettings,
  getEventDetails,
  registerUserToEventbyApi,
  enableMagicLinkInEvent,
  addCustomFieldsToEvent,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { DataUtl } from "../../../util/dataUtil";
import { CsvApiController } from "../../../util/csv-api-controller";
import RestrictionEmailDomainType from "../../../enums/restrictedEmailDomainType";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import EventRole from "../../../enums/eventRoleEnum";
import EventEntryType from "../../../enums/eventEntryEnum";
import { EventCustomFieldFormObj } from "../../../test-data/userRegForm";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe
  .parallel("@email-access-control Live side checks for 'Deny' custom domain @newmail.com", async () => {
  test.describe.configure({ retries: 2 });

  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId;
  let eventTitle;
  let landingPageId;
  let fileName;
  let registrationSetupUrl: string;
  let organiserPage: Page;
  let emailRestrictionComp: EmailDomainRestrictionComponent;
  let eventInfoDto: EventInfoDTO;
  let csvApiController: CsvApiController;
  let page: Page;
  test.beforeEach(async ({ context }) => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = await orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: eventTitle,
    });
    eventInfoDto = new EventInfoDTO(eventId, eventTitle, false);
    csvApiController = new CsvApiController(orgApiContext);
    //enable restriction
    fileName = await csvApiController.getSignedS3Url("emailBlockingCsv.csv");
    let csvUploadResJson = await csvApiController.processRestrciedEmailCsv(
      eventId,
      fileName
    );
    let successRecords = csvUploadResJson["succeedRecords"];
    expect(successRecords).toBe(1);
    //now getting the landing page id for this event
    const event_details_api_resp = await (
      await getEventDetails(orgApiContext, eventId)
    ).json();
    landingPageId = event_details_api_resp.eventLandingPageId;
    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      orgApiContext,
      eventId,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      orgApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    page = await context.newPage();
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test("TC001: Attendee is able to register with email domain which is not in denied list", async () => {
    //apply restriction
    await updateEmailRestrictionSettings(
      orgApiContext,
      eventId,
      landingPageId,
      fileName,
      EmailRestrictionType.DENY,
      [RestrictionEmailDomainType.CUSTOM]
    );
    let landingPageOne = new LandingPageOne(page);
    let userInfoObj = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "qa",
      false
    );
    await landingPageOne.load(eventInfoDto.attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoObj.userRegFormData.userRegFormDataForDefaultFields
    );
    await (
      await landingPageOne.getRegistrationConfirmationScreen()
    ).isRegistrationConfirmationMessageVisible();
  });

  test("TC002: attendee registration with denied domain should be restricted on reg form", async () => {
    //apply restriction
    await updateEmailRestrictionSettings(
      orgApiContext,
      eventId,
      landingPageId,
      fileName,
      EmailRestrictionType.DENY,
      [RestrictionEmailDomainType.CUSTOM]
    );
    let landingPageOne = new LandingPageOne(page);
    let userInfoObj = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail("newmail.com"),
      "",
      "prateek",
      "qa",
      false
    );
    await landingPageOne.load(eventInfoDto.attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoObj.userRegFormData.userRegFormDataForDefaultFields,
      {
        fillMandatoryFields: true,
      },
      false
    );
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fieldValidationErrorMessageAppearsToUseBusinessEmails();
  });

  test("TC003: already registered user should not be blocked if restrictions are applied after their reg", async () => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail("newmail.com");
    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "qa",
      false
    );
    await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
    // now we will apply restriction when user is already registered
    await updateEmailRestrictionSettings(
      orgApiContext,
      eventId,
      landingPageId,
      fileName,
      EmailRestrictionType.DENY,
      [RestrictionEmailDomainType.CUSTOM]
    );
    let userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(eventInfoDto.attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationConfirmationScreen()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
  });

  test("TC004: user having magic link should not be blocked if restrictions are applied after their reg", async () => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail("newmail.com");
    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "qa",
      false
    );
    await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
    let attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
      eventId,
      attendeeEmail
    );
    // now we will apply restriction when user is already registered
    await updateEmailRestrictionSettings(
      orgApiContext,
      eventId,
      landingPageId,
      fileName,
      EmailRestrictionType.DENY,
      [RestrictionEmailDomainType.CUSTOM]
    );
    //enable magic link in the event
    await enableMagicLinkInEvent(
      orgApiContext,
      eventId,
      landingPageId,
      EventEntryType.REG_BASED
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendeeMagicLink);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC005: user should be able to fill new custom fields if he is already registerd before restriction", async () => {
    test.fail(
      true,
      "https://linear.app/zuddl/issue/BUG-1799/user-should-be-able-to-fill-new-custom-fields-if-he-is-already"
    );
    // linear ticket info
    test.info().annotations.push({
      type: "Known Bug",
      description:
        "https://linear.app/zuddl/issue/BUG-1799/user-should-be-able-to-fill-new-custom-fields-if-he-is-already",
    });

    let attendeeEmail = DataUtl.getRandomAttendeeEmail("newmail.com");
    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "qa",
      false
    );
    await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
    let attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
      eventId,
      attendeeEmail
    );
    // now we will apply restriction when user is already registered
    await updateEmailRestrictionSettings(
      orgApiContext,
      eventId,
      landingPageId,
      fileName,
      EmailRestrictionType.DENY,
      [RestrictionEmailDomainType.CUSTOM]
    );
    // add new mandatory field
    let newMandatoryCustomFieldObj = new EventCustomFieldFormObj(
      eventId,
      landingPageId
    ).getAnExtraMandatoryCustomFieldObj();
    await addCustomFieldsToEvent(
      orgApiContext,
      eventId,
      landingPageId,
      newMandatoryCustomFieldObj
    );
    //enable magic link in the event
    await enableMagicLinkInEvent(
      orgApiContext,
      eventId,
      landingPageId,
      EventEntryType.REG_BASED
    );
    // load the magic link
    let landingPageOne = new LandingPageOne(page);
    const eventRoleApiPromise = page.waitForResponse(
      (response) =>
        response.url().includes(`/event/${eventId}/role`) &&
        response.status() === 200
    );
    await landingPageOne.load(attendeeMagicLink);
    const getEventRoleResponse = await eventRoleApiPromise;
    expect(
      getEventRoleResponse.status(),
      "expecting event role api respoinse to be 200"
    ).toBe(200);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).enterDataInRegFormField(
      newMandatoryCustomFieldObj[0]["labelName"], //since its a list of object
      newMandatoryCustomFieldObj[0]["fieldType"].toLowerCase(),
      "chooseme"
    );
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnContinueButtonOnRegisterationForm();
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });
});
