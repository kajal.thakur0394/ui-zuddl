import test, {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
} from "@playwright/test";
import EmailRestrictionType from "../../../enums/emailRestrictionTypeEnum";
import { EmailDomainRestrictionComponent } from "../../../page-objects/new-org-side-pages/EmailDomainRestrictionComponent";
import {
  createNewEvent,
  updateEmailRestrictionSettings,
  getEventDetails,
  registerUserToEventbyApi,
  enableMagicLinkInEvent,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { DataUtl } from "../../../util/dataUtil";
import { CsvApiController } from "../../../util/csv-api-controller";
import RestrictionEmailDomainType from "../../../enums/restrictedEmailDomainType";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import EventRole from "../../../enums/eventRoleEnum";
import { fetchAttendeeInviteMagicLink } from "../../../util/emailUtil";
import EventEntryType from "../../../enums/eventEntryEnum";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";

test.describe
  .parallel("@email-access-control Live side checks for 'Allow' only @newmail.com", async () => {
  test.describe.configure({ retries: 2 });
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId;
  let eventTitle;
  let landingPageId;
  let fileName;
  let registrationSetupUrl: string;
  let organiserPage: Page;
  let emailRestrictionComp: EmailDomainRestrictionComponent;
  let eventInfoDto: EventInfoDTO;
  let csvApiController: CsvApiController;
  let page: Page;
  test.beforeEach(async ({ context }) => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = await orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: eventTitle,
    });
    eventInfoDto = new EventInfoDTO(eventId, eventTitle, false);
    csvApiController = new CsvApiController(orgApiContext);
    //enable restriction
    fileName = await csvApiController.getSignedS3Url("emailBlockingCsv.csv");
    let csvUploadResJson = await csvApiController.processRestrciedEmailCsv(
      eventId,
      fileName
    );
    let successRecords = csvUploadResJson["succeedRecords"];
    expect(successRecords).toBe(1);
    //now getting the landing page id for this event
    const event_details_api_resp = await (
      await getEventDetails(orgApiContext, eventId)
    ).json();
    landingPageId = event_details_api_resp.eventLandingPageId;

    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      orgApiContext,
      eventId,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      orgApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );
    page = await context.newPage();
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test("TC001: Attendee is able to register with allowed email domain", async () => {
    await updateEmailRestrictionSettings(
      orgApiContext,
      eventId,
      landingPageId,
      fileName,
      EmailRestrictionType.ALLOW,
      [RestrictionEmailDomainType.CUSTOM]
    );
    let landingPageOne = new LandingPageOne(page);
    let userInfoObj = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail("newmail.com"),
      "",
      "prateek",
      "qa",
      false
    );
    await landingPageOne.load(eventInfoDto.attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoObj.userRegFormData.userRegFormDataForDefaultFields
    );
    await (
      await landingPageOne.getRegistrationConfirmationScreen()
    ).isRegistrationConfirmationMessageVisible();
  });

  test("TC002: attendee with email other than allowed domain should be restricted on reg form", async () => {
    await updateEmailRestrictionSettings(
      orgApiContext,
      eventId,
      landingPageId,
      fileName,
      EmailRestrictionType.ALLOW,
      [RestrictionEmailDomainType.CUSTOM]
    );
    let landingPageOne = new LandingPageOne(page);
    let userInfoObj = new UserInfoDTO(
      DataUtl.getRandomAttendeeEmail(),
      "",
      "prateek",
      "qa",
      false
    );
    await landingPageOne.load(eventInfoDto.attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoObj.userRegFormData.userRegFormDataForDefaultFields,
      { fillMandatoryFields: true },
      false
    );
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fieldValidationErrorMessageAppearsToUseBusinessEmails();
  });

  test("TC003: already registered user should not be blocked if restrictions are applied after their reg", async () => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "qa",
      false
    );
    await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
    // now we will apply restriction when user is already registered
    await updateEmailRestrictionSettings(
      orgApiContext,
      eventId,
      landingPageId,
      fileName,
      EmailRestrictionType.ALLOW,
      [RestrictionEmailDomainType.CUSTOM]
    );
    let userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      true
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(eventInfoDto.attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationConfirmationScreen()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
  });

  test("TC004: user having magic link should not be blocked if restrictions are applied after their reg", async () => {
    //enable magic link in the event
    await enableMagicLinkInEvent(
      orgApiContext,
      eventId,
      landingPageId,
      EventEntryType.REG_BASED
    );
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let userInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "qa",
      false
    );
    await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
    //fetch magic link
    let attendeeMagicLink: string = await fetchAttendeeInviteMagicLink(
      userInfoDto.userEmail,
      eventInfoDto.registrationConfirmationEmailSubj
    );
    // now we will apply restriction when user is already registered
    await updateEmailRestrictionSettings(
      orgApiContext,
      eventId,
      landingPageId,
      fileName,
      EmailRestrictionType.ALLOW,
      [RestrictionEmailDomainType.CUSTOM]
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendeeMagicLink);
    await landingPageOne.isLobbyLoaded();
  });
});
