import test, {
  APIRequestContext,
  BrowserContext,
  Page,
} from "@playwright/test";
import EmailRestrictionType from "../../../enums/emailRestrictionTypeEnum";
import { AdvancedSettingsPage } from "../../../page-objects/new-org-side-pages/AdvancedTabComponent";
import { EmailDomainRestrictionComponent } from "../../../page-objects/new-org-side-pages/EmailDomainRestrictionComponent";
import { createNewEvent } from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel("@email-access-control Org side UI checks", async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId;
  let eventTitle;
  let advacnedSettingsUrl: string;
  let advancedSettingsPage: AdvancedSettingsPage;
  let organiserPage: Page;
  let emailRestrictionComp: EmailDomainRestrictionComponent;
  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: eventTitle,
    });
    let baseUrl = DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl;
    advacnedSettingsUrl = `${baseUrl}/event/${eventId}/advanced-settings`;
    organiserPage = await orgBrowserContext.newPage();
    advancedSettingsPage = new AdvancedSettingsPage(organiserPage);
    await organiserPage.goto(advacnedSettingsUrl);
    emailRestrictionComp =
      await advancedSettingsPage.getEmailDomainRestrictionComponent();
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test("TC001: verify default state on UI and DB for email domain restriction on new event", async () => {
    await emailRestrictionComp?.validateTheDropDownOptions();
    // DB validation
    await QueryUtil.validateEventEmailRestrictionInDB(
      eventId,
      null,
      null,
      null
    );
  });

  test("TC002: Org can enable 'Allow' Email Domains by uploading csv and validate changes in DB", async () => {
    await emailRestrictionComp?.openSection();
    await emailRestrictionComp?.selectOptionFromDropDown(
      EmailRestrictionType.ALLOW
    );
    // verify upload csv option is displayed
    await emailRestrictionComp?.clickOnUploadCsv();
    await emailRestrictionComp?.verifyUploadCsvFromIsVisible();
    await emailRestrictionComp?.uploadCsvForEmailRestriction(
      "emailBlockingCsv.csv"
    );
    await emailRestrictionComp?.verifyUploadedFileIsVisbleBelowMenuSection();
    // DB validation
    await QueryUtil.validateEventEmailRestrictionInDB(
      eventId,
      "ALLOW",
      "{CUSTOM}",
      "emailBlockingCsv.csv"
    );
  });

  test("TC003: Org can enable both custom csv & predefined email restriction from UI and validate DB", async () => {
    await emailRestrictionComp?.openSection();
    await emailRestrictionComp?.selectOptionFromDropDown(
      EmailRestrictionType.DENY
    );
    await emailRestrictionComp.verifyCheckBoxesAreVisible();
    await emailRestrictionComp.clickOnRestrictNonBusinessEmailCheckbox();
    await emailRestrictionComp.clickOnRestrictEmailFromCsvCheckbox();
    //verify upload csv form is displayed as soon as selected second option
    await emailRestrictionComp.clickOnUploadCsv();
    await emailRestrictionComp.verifyUploadCsvFromIsVisible();
    await emailRestrictionComp.uploadCsvForEmailRestriction(
      "emailBlockingCsv.csv"
    );
    await emailRestrictionComp.verifyUploadedFileIsVisbleBelowMenuSection();
    // DB validation
    await QueryUtil.validateEventEmailRestrictionInDB(
      eventId,
      "DENY",
      "{PREDEFINED,CUSTOM}",
      "emailBlockingCsv.csv"
    );
  });

  test("TC004: Org can choose only custom csv option for email restriction from UI and validate DB", async () => {
    await emailRestrictionComp?.openSection();
    await emailRestrictionComp?.selectOptionFromDropDown(
      EmailRestrictionType.DENY
    );
    await emailRestrictionComp.verifyCheckBoxesAreVisible();
    await emailRestrictionComp.clickOnRestrictEmailFromCsvCheckbox();
    //verify upload csv form is displayed as soon as selected second option
    await emailRestrictionComp.clickOnUploadCsv();
    await emailRestrictionComp.verifyUploadCsvFromIsVisible();
    await emailRestrictionComp.uploadCsvForEmailRestriction(
      "emailBlockingCsv.csv"
    );
    await emailRestrictionComp.verifyUploadedFileIsVisbleBelowMenuSection();
    // DB validation
    await QueryUtil.validateEventEmailRestrictionInDB(
      eventId,
      "DENY",
      "{CUSTOM}",
      "emailBlockingCsv.csv"
    );
  });

  test("TC005: Org can choose only predefined option for email restriction from UI and validate DB", async () => {
    await emailRestrictionComp?.openSection();
    await emailRestrictionComp?.selectOptionFromDropDown(
      EmailRestrictionType.DENY
    );
    await emailRestrictionComp.verifyCheckBoxesAreVisible();
    await emailRestrictionComp.clickOnRestrictNonBusinessEmailCheckbox();
    // DB validation
    await QueryUtil.validateEventEmailRestrictionInDB(
      eventId,
      "DENY",
      "{PREDEFINED}",
      ""
    );
  });

  test("TC006: Org should be able to update email restriction from Allow to Deny and validate DB", async () => {
    await emailRestrictionComp?.openSection();
    await emailRestrictionComp?.selectOptionFromDropDown(
      EmailRestrictionType.ALLOW
    );
    await emailRestrictionComp.clickOnUploadCsv();
    await emailRestrictionComp.uploadCsvForEmailRestriction(
      "emailBlockingCsv.csv"
    );
    await emailRestrictionComp.verifyUploadedFileIsVisbleBelowMenuSection();
    await QueryUtil.validateEventEmailRestrictionInDB(
      eventId,
      "ALLOW",
      "{CUSTOM}",
      "emailBlockingCsv.csv"
    );
    // now change the email restirction to deny
    await emailRestrictionComp.selectOptionFromDropDown(
      EmailRestrictionType.DENY
    );

    await emailRestrictionComp.clickOnProceedButtonOnConfirmationDialogue();
    // verify previously uploaded csv for allow is not visible
    await emailRestrictionComp.verifyUploadedFileIsNotVisbleBelowMenuSection();
    // verify 2 checkboxes for predefined and custom csv are visible
    await emailRestrictionComp.verifyCheckBoxesAreVisible();
    // now selelct restrict by predefind email
    await emailRestrictionComp.clickOnRestrictNonBusinessEmailCheckbox();
    // verify DB is reflecting the new changes now
    await QueryUtil.validateEventEmailRestrictionInDB(
      eventId,
      "DENY",
      "{PREDEFINED}",
      ""
    );
  });

  test("TC007: Org should be able to remove all email restriction and validate DB", async () => {
    await emailRestrictionComp?.openSection();
    await emailRestrictionComp?.selectOptionFromDropDown(
      EmailRestrictionType.ALLOW
    );
    await emailRestrictionComp.clickOnUploadCsv();
    await emailRestrictionComp.uploadCsvForEmailRestriction(
      "emailBlockingCsv.csv"
    );
    await emailRestrictionComp.verifyUploadedFileIsVisbleBelowMenuSection();
    await QueryUtil.validateEventEmailRestrictionInDB(
      eventId,
      "ALLOW",
      "{CUSTOM}",
      "emailBlockingCsv.csv"
    );
    // now change the email restirction to deny
    await emailRestrictionComp.selectOptionFromDropDown(
      EmailRestrictionType.NONE
    );
    await emailRestrictionComp.clickOnProceedButtonOnConfirmationDialogue();
    // verify DB is reflecting the new changes now
    await QueryUtil.validateEventEmailRestrictionInDB(
      eventId,
      "NONE",
      "{NONE}",
      ""
    );
  });

  //download file is not working
  test.skip("TC008: Org should be able to download the uploaded csv for email restriction", async () => {
    await emailRestrictionComp?.openSection();
    await emailRestrictionComp?.selectOptionFromDropDown(
      EmailRestrictionType.ALLOW
    );
    await emailRestrictionComp.clickOnUploadCsv();
    await emailRestrictionComp.uploadCsvForEmailRestriction(
      "emailBlockingCsv.csv"
    );
    await emailRestrictionComp.verifyUploadedFileIsVisbleBelowMenuSection();
    await emailRestrictionComp.downloadUploadedFileFromMenuSection();
  });
});
