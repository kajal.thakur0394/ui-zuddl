import { EventZonesUpperCase } from "../../../enums/EventZoneEnum";
import { announcement_tests } from "../../../fixtures/event-fixtures/announcementSuiteFixture";

announcement_tests.describe(
  `@Announcements @internal-redirection`,
  async () => {
    //announcement_tests.describe.configure({ mode: "parallel", retries: 2 });

    const listOfZonesToTestIn = [
      EventZonesUpperCase.LOBBY,
      EventZonesUpperCase.EXPO,
      EventZonesUpperCase.NETWORKING,
      EventZonesUpperCase.ROOMS,
      EventZonesUpperCase.SCHEDULE,
      EventZonesUpperCase.STAGE,
    ];

    announcement_tests(
      `TC001 verify users present inside Lobby are able to interact with the internal redirection announcement`,
      async ({
        organiserContextInfo,
        attendeeContextInfo,
        speakerContextInfo,
        eventController,
        organiserPages,
        speakerPages,
        attendeePages,
        eventInfoModal,
      }) => {
        const zoneUrl = eventInfoModal.getZoneUrl(EventZonesUpperCase.LOBBY);

        // First everyone is moving to lobby

        await announcement_tests.step(
          `Org, attendee and speaker moves to ${EventZonesUpperCase.LOBBY}`,
          async () => {
            await organiserContextInfo.page.goto(zoneUrl);
            await speakerContextInfo.page.goto(zoneUrl);
            await attendeeContextInfo.page.goto(zoneUrl);
            await attendeeContextInfo.page.waitForTimeout(2000);
          }
        );

        await announcement_tests.step(
          `Organiser using API publishes an announcement to redirect to Networking keeping visibility options enabled for all zones`,
          async () => {
            await eventController.getAnnouncementController.publishAnInternalRedirectionAnnouncement(
              "I am the announcement",
              EventZonesUpperCase.NETWORKING
            );
          }
        );

        await announcement_tests.step(
          `Verify organiser present on Lobby see announcement published`,
          async () => {
            await (
              await organiserPages.lobbyPage.getPublishedAnnouncementComponent()
            ).isPublishedAnnouncementVisible();
          }
        );
        await announcement_tests.step(
          `Verify speaker present on Lobby see announcement published`,
          async () => {
            await (
              await speakerPages.lobbyPage.getPublishedAnnouncementComponent()
            ).isPublishedAnnouncementVisible();
          }
        );
        await announcement_tests.step(
          `Verify attendee present on Lobby see announcement published`,
          async () => {
            await (
              await attendeePages.lobbyPage.getPublishedAnnouncementComponent()
            ).isPublishedAnnouncementVisible();
          }
        );

        await announcement_tests.step(
          `Verify clicking on take me button take all users to networking`,
          async () => {
            await (
              await organiserPages.lobbyPage.getPublishedAnnouncementComponent()
            ).verifyAnnouncementRedirectsTo(
              eventInfoModal.getZoneUrl(EventZonesUpperCase.NETWORKING)
            );
          }
        );
      }
    );

    announcement_tests(
      `TC002 verify users present inside Rooms are able to interact with the internal redirection announcement`,
      async ({
        organiserContextInfo,
        attendeeContextInfo,
        speakerContextInfo,
        dbUtil,
        eventController,
        organiserPages,
        speakerPages,
        attendeePages,
        eventInfoModal,
      }) => {
        const zoneUrl = eventInfoModal.getZoneUrl(EventZonesUpperCase.ROOMS);

        // creating object of lobby
        await announcement_tests.step(
          `Org, attendee and speaker moves to ${EventZonesUpperCase.ROOMS}`,
          async () => {
            await organiserContextInfo.page.goto(zoneUrl);
            await speakerContextInfo.page.goto(zoneUrl);
            await attendeeContextInfo.page.goto(zoneUrl);
            await attendeeContextInfo.page.waitForTimeout(2000);
          }
        );

        await announcement_tests.step(
          `Organiser using API publishes an announcement to redirect to Networking keeping visibility options enabled for all zones`,
          async () => {
            await eventController.getAnnouncementController.publishAnInternalRedirectionAnnouncement(
              "I am the announcement",
              EventZonesUpperCase.NETWORKING
            );
          }
        );

        await announcement_tests.step(
          `Verify organiser present on Rooms listing see announcement published`,
          async () => {
            await (
              await organiserPages.roomsListingPage.getPublishedAnnouncementComponent()
            ).isPublishedAnnouncementVisible();
            // await (
            //   await orgLobbyPage.getPublishedAnnouncementComponent()
            // ).isPublishedAnnouncementVisible();
          }
        );
        await announcement_tests.step(
          `Verify speaker present on Rooms listing see announcement published`,
          async () => {
            await (
              await speakerPages.roomsListingPage.getPublishedAnnouncementComponent()
            ).isPublishedAnnouncementVisible();
          }
        );
        await announcement_tests.step(
          `Verify attendee present on Rooms listing see announcement published`,
          async () => {
            await (
              await attendeePages.roomsListingPage.getPublishedAnnouncementComponent()
            ).isPublishedAnnouncementVisible();
          }
        );

        await announcement_tests.step(
          `Verify clicking on take me button take all users to networking`,
          async () => {
            await (
              await organiserPages.roomsListingPage.getPublishedAnnouncementComponent()
            ).verifyAnnouncementRedirectsTo(
              eventInfoModal.getZoneUrl(EventZonesUpperCase.NETWORKING)
            );
          }
        );
      }
    );

    announcement_tests(
      `TC003 verify users present inside Stage are able to interact with the internal redirection announcement`,
      async ({
        organiserContextInfo,
        attendeeContextInfo,
        speakerContextInfo,
        dbUtil,
        eventController,
        eventInfoModal,
        organiserPages,
        speakerPages,
        attendeePages,
      }) => {
        const zoneUrl = eventInfoModal.getZoneUrl(EventZonesUpperCase.STAGE);
        console.log(`stage url is ${zoneUrl}`);

        // creating object of lobby
        await announcement_tests.step(
          `Organiser moves to ${EventZonesUpperCase.STAGE}`,
          async () => {
            await organiserContextInfo.page.goto(zoneUrl);
            //organiser will have to close the welcome stage av modal also
            await organiserPages.stagePage.closeWelcomeStagePopUp();
          }
        );
        await announcement_tests.step(
          `Speaker  moves to ${EventZonesUpperCase.STAGE}`,
          async () => {
            await speakerContextInfo.page.goto(zoneUrl);
            //speaker will also have to close the welcome stage av modal
            await speakerPages.stagePage.closeWelcomeStagePopUp();
          }
        );
        await announcement_tests.step(
          `Attendee moves to ${EventZonesUpperCase.STAGE}`,
          async () => {
            await attendeeContextInfo.page.goto(zoneUrl);
            await attendeeContextInfo.page.waitForTimeout(2000);
          }
        );

        await announcement_tests.step(
          `Organiser using API publishes an announcement to redirect to Networking keeping visibility options enabled for all zones`,
          async () => {
            await eventController.getAnnouncementController.publishAnInternalRedirectionAnnouncement(
              "I am the announcement",
              EventZonesUpperCase.NETWORKING
            );
          }
        );

        await announcement_tests.step(
          `Verify organiser present on Stage listing see announcement published`,
          async () => {
            await (
              await organiserPages.stagePage.getPublishedAnnouncementComponent()
            ).isPublishedAnnouncementVisible();
          }
        );
        await announcement_tests.step(
          `Verify speaker present on Stage listing see announcement published`,
          async () => {
            await (
              await speakerPages.stagePage.getPublishedAnnouncementComponent()
            ).isPublishedAnnouncementVisible();
          }
        );
        await announcement_tests.step(
          `Verify attendee present on Stage listing see announcement published`,
          async () => {
            await (
              await attendeePages.stagePage.getPublishedAnnouncementComponent()
            ).isPublishedAnnouncementVisible();
          }
        );

        await announcement_tests.step(
          `Verify clicking on take me button take all users to networking`,
          async () => {
            await (
              await organiserPages.stagePage.getPublishedAnnouncementComponent()
            ).verifyAnnouncementRedirectsTo(
              eventInfoModal.getZoneUrl(EventZonesUpperCase.NETWORKING)
            );
          }
        );
      }
    );

    announcement_tests(
      `TC004 verify users present inside Schedule are able to interact with the internal redirection announcement`,
      async ({
        organiserContextInfo,
        attendeeContextInfo,
        speakerContextInfo,
        dbUtil,
        eventController,
        eventInfoModal,
        organiserPages,
        speakerPages,
        attendeePages,
      }) => {
        const zoneUrl = eventInfoModal.getZoneUrl(EventZonesUpperCase.SCHEDULE);
        console.log(`zone url is ${zoneUrl}`);
        await announcement_tests.step(
          `Org, attendee and speaker moves to ${EventZonesUpperCase.SCHEDULE}`,
          async () => {
            await organiserContextInfo.page.goto(zoneUrl);
            await speakerContextInfo.page.goto(zoneUrl);
            await attendeeContextInfo.page.goto(zoneUrl);
            await attendeeContextInfo.page.waitForTimeout(2000);
          }
        );

        await announcement_tests.step(
          `Organiser using API publishes an announcement to redirect to Networking keeping visibility options enabled for all zones`,
          async () => {
            await eventController.getAnnouncementController.publishAnInternalRedirectionAnnouncement(
              "I am the announcement",
              EventZonesUpperCase.NETWORKING
            );
          }
        );

        await announcement_tests.step(
          `Verify organiser present on Rooms listing see announcement published`,
          async () => {
            await (
              await organiserPages.stagePage.getPublishedAnnouncementComponent()
            ).isPublishedAnnouncementVisible();
            // await (
            //   await orgLobbyPage.getPublishedAnnouncementComponent()
            // ).isPublishedAnnouncementVisible();
          }
        );
        await announcement_tests.step(
          `Verify speaker present on Rooms listing see announcement published`,
          async () => {
            await (
              await speakerPages.stagePage.getPublishedAnnouncementComponent()
            ).isPublishedAnnouncementVisible();
          }
        );
        await announcement_tests.step(
          `Verify attendee present on Rooms listing see announcement published`,
          async () => {
            await (
              await attendeePages.stagePage.getPublishedAnnouncementComponent()
            ).isPublishedAnnouncementVisible();
          }
        );

        await announcement_tests.step(
          `Verify clicking on take me button take all users to networking`,
          async () => {
            await (
              await organiserPages.stagePage.getPublishedAnnouncementComponent()
            ).verifyAnnouncementRedirectsTo(
              eventInfoModal.getZoneUrl(EventZonesUpperCase.NETWORKING)
            );
          }
        );
      }
    );

    announcement_tests(
      `TC005 verify users present inside Expo Listing are able to interact with the internal redirection announcement`,
      async ({
        organiserContextInfo,
        attendeeContextInfo,
        speakerContextInfo,
        dbUtil,
        eventController,
        eventInfoModal,
        organiserPages,
        speakerPages,
        attendeePages,
      }) => {
        const zoneUrl = eventInfoModal.getZoneUrl(EventZonesUpperCase.EXPO);
        console.log(`stage url is ${zoneUrl}`);

        await announcement_tests.step(
          `Org, attendee and speaker moves to ${EventZonesUpperCase.EXPO}`,
          async () => {
            await organiserContextInfo.page.goto(zoneUrl);
            await speakerContextInfo.page.goto(zoneUrl);
            await attendeeContextInfo.page.goto(zoneUrl);
            await attendeeContextInfo.page.waitForTimeout(2000);
          }
        );

        await announcement_tests.step(
          `Organiser using API publishes an announcement to redirect to Networking keeping visibility options enabled for all zones`,
          async () => {
            await eventController.getAnnouncementController.publishAnInternalRedirectionAnnouncement(
              "I am the announcement",
              EventZonesUpperCase.NETWORKING
            );
          }
        );

        await announcement_tests.step(
          `Verify organiser present on Rooms listing see announcement published`,
          async () => {
            await (
              await organiserPages.stagePage.getPublishedAnnouncementComponent()
            ).isPublishedAnnouncementVisible();
            // await (
            //   await orgLobbyPage.getPublishedAnnouncementComponent()
            // ).isPublishedAnnouncementVisible();
          }
        );
        await announcement_tests.step(
          `Verify speaker present on Rooms listing see announcement published`,
          async () => {
            await (
              await speakerPages.stagePage.getPublishedAnnouncementComponent()
            ).isPublishedAnnouncementVisible();
          }
        );
        await announcement_tests.step(
          `Verify attendee present on Rooms listing see announcement published`,
          async () => {
            await (
              await attendeePages.stagePage.getPublishedAnnouncementComponent()
            ).isPublishedAnnouncementVisible();
          }
        );

        await announcement_tests.step(
          `Verify clicking on take me button take all users to networking`,
          async () => {
            await (
              await organiserPages.stagePage.getPublishedAnnouncementComponent()
            ).verifyAnnouncementRedirectsTo(
              eventInfoModal.getZoneUrl(EventZonesUpperCase.NETWORKING)
            );
          }
        );
      }
    );

    announcement_tests(
      `TC006 verify users present inside Networking are able to interact with the internal redirection announcement`,
      async ({
        organiserContextInfo,
        attendeeContextInfo,
        speakerContextInfo,
        dbUtil,
        eventController,
        eventInfoModal,
        organiserPages,
        speakerPages,
        attendeePages,
      }) => {
        const zoneUrl = eventInfoModal.getZoneUrl(
          EventZonesUpperCase.NETWORKING
        );
        console.log(`zone url is ${zoneUrl}`);

        await announcement_tests.step(
          `Org, attendee and speaker moves to ${EventZonesUpperCase.NETWORKING}`,
          async () => {
            await organiserContextInfo.page.goto(zoneUrl);
            await speakerContextInfo.page.goto(zoneUrl);
            await attendeeContextInfo.page.goto(zoneUrl);
            await attendeeContextInfo.page.waitForTimeout(2000);
          }
        );

        await announcement_tests.step(
          `Organiser using API publishes an announcement to redirect to Networking keeping visibility options enabled for all zones`,
          async () => {
            await eventController.getAnnouncementController.publishAnInternalRedirectionAnnouncement(
              "I am the announcement",
              EventZonesUpperCase.NETWORKING
            );
          }
        );

        await announcement_tests.step(
          `Verify organiser present on Rooms listing see announcement published`,
          async () => {
            await (
              await organiserPages.stagePage.getPublishedAnnouncementComponent()
            ).isPublishedAnnouncementVisible();
            // await (
            //   await orgLobbyPage.getPublishedAnnouncementComponent()
            // ).isPublishedAnnouncementVisible();
          }
        );
        await announcement_tests.step(
          `Verify speaker present on Rooms listing see announcement published`,
          async () => {
            await (
              await speakerPages.stagePage.getPublishedAnnouncementComponent()
            ).isPublishedAnnouncementVisible();
          }
        );
        await announcement_tests.step(
          `Verify attendee present on Rooms listing see announcement published`,
          async () => {
            await (
              await attendeePages.stagePage.getPublishedAnnouncementComponent()
            ).isPublishedAnnouncementVisible();
          }
        );

        await announcement_tests.step(
          `Verify clicking on take me button take all users to networking`,
          async () => {
            await (
              await organiserPages.stagePage.getPublishedAnnouncementComponent()
            ).verifyAnnouncementRedirectsTo(
              eventInfoModal.getZoneUrl(EventZonesUpperCase.NETWORKING)
            );
          }
        );
      }
    );
  }
);
