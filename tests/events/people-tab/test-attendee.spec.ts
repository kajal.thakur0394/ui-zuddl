import test, {
  APIRequestContext,
  BrowserContext,
  expect,
  Page,
} from "@playwright/test";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import {
  AttendeeFormData,
  AttendeeListPage,
} from "../../../page-objects/new-org-side-pages/AttendeeListPage";
import { PeopleSectionPage } from "../../../page-objects/new-org-side-pages/PeopleSectionPage";
import { createNewEvent } from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { fetchAttendeeInviteMagicLink } from "../../../util/emailUtil";

test.describe
  .parallel("@people-tab @new-org-side Attendee Tab org side UI", async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId;
  let eventTitle: string;
  let peopleSectionUrl: string;
  let organiserPage: Page;
  let attendeeListPage: AttendeeListPage;
  let peopleSectionPage: PeopleSectionPage;
  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: eventTitle,
    });
    //get the people tab url
    peopleSectionUrl = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/event/${eventId}/people/attendees`;
    organiserPage = await orgBrowserContext.newPage();
    await organiserPage.goto(peopleSectionUrl);
    peopleSectionPage = new PeopleSectionPage(organiserPage);
  });

  test.afterEach(async () => {
    console.log("In teardown, closing the browser context");
    await orgBrowserContext?.close();
  });

  test("TC001: Empty state checks on attendee list", async () => {
    //expect attendee count is 0
    attendeeListPage = await peopleSectionPage.openAttendeeTab();
    await attendeeListPage.runEmptyStateChecksForAttendeeList();
  });

  test("TC002: Adding attendee manually by form with just mandatory field", async () => {
    attendeeListPage = await peopleSectionPage.openAttendeeTab();
    const attendeeFormData: AttendeeFormData = {
      firstName: "prateek",
      emailId: DataUtl.getRandomAttendeeEmail(),
      lastName: "qa",
    };
    await attendeeListPage.openAddAttendeeForm();
    await attendeeListPage.fillTheAttendeeForm(attendeeFormData);
    await attendeeListPage.handleSendEmailConfirmationPopup(true);
    await attendeeListPage.verifyTheCountOfAttendeeToBe(1);
  });

  test("TC003: Searching the attendee in list using search function", async () => {
    const attendeeFormData: AttendeeFormData = {
      firstName: "prateek",
      emailId: DataUtl.getRandomAttendeeEmail(),
      lastName: "qa",
    };
    attendeeListPage = await peopleSectionPage.openAttendeeTab();
    await attendeeListPage.openAddAttendeeForm();
    await attendeeListPage.fillTheAttendeeForm(attendeeFormData);
    await attendeeListPage.handleSendEmailConfirmationPopup(true);
    await attendeeListPage.verifyTheCountOfAttendeeToBe(1);
    //search with wrong input first
    await attendeeListPage.clickOnSearchIcon();
    await attendeeListPage.triggerSearchFor("anythingwhichisnotthere");
    await attendeeListPage.verifyAttendeeRecordCountToBe(0);
    //now search with right string
    await attendeeListPage.triggerSearchFor(attendeeFormData.emailId);
    await attendeeListPage.verifyAttendeeRecordCountToBe(1);
  });

  test("TC005: Set device limit only ui form check", async () => {
    attendeeListPage = await peopleSectionPage.openAttendeeTab();
    await attendeeListPage.setDeviceLimit({
      noOfDeviceAllowed: "2",
      applyToAllAttendee: true,
    });
    //TODO: we can check in DB if the device limit is applied or not
  });

  test("TC006: Resend invite functionality check", async () => {
    attendeeListPage = await peopleSectionPage.openAttendeeTab();
    const attendeeFormData: AttendeeFormData = {
      firstName: "prateek",
      emailId: DataUtl.getRandomAttendeeEmail(),
      lastName: "qa",
    };
    await attendeeListPage.openAddAttendeeForm();
    await attendeeListPage.fillTheAttendeeForm(attendeeFormData);
    await attendeeListPage.handleSendEmailConfirmationPopup(true);
    await attendeeListPage.verifyTheCountOfAttendeeToBe(1);
    const attendeeInviteLink1 = await fetchAttendeeInviteMagicLink(
      attendeeFormData.emailId,
      new EventInfoDTO(eventId, eventTitle, false).getInviteEmailSubj()
    );
    expect(
      attendeeInviteLink1,
      "expecting attendee recieved invite email"
    ).not.toBeUndefined();
    await attendeeListPage.resendConfirmationEmail(attendeeFormData.emailId);
    await attendeeListPage.verifyNotifcationPopsUpWithMessage(
      `Invite sent to ${attendeeFormData.emailId}`
    );
    // TODO , add an email check too
    const attendeeInviteLink2 = await fetchAttendeeInviteMagicLink(
      attendeeFormData.emailId,
      new EventInfoDTO(eventId, eventTitle, false).getInviteEmailSubj()
    );
    expect(
      attendeeInviteLink2,
      "expecting attendee recieved invite email after resend was triggered"
    ).not.toBeUndefined();
  });

  test("TC007: Delete attendee functionality check", async () => {
    attendeeListPage = await peopleSectionPage.openAttendeeTab();
    const attendeeFormData: AttendeeFormData = {
      firstName: "prateek",
      emailId: DataUtl.getRandomAttendeeEmail(),
      lastName: "qa",
    };
    await attendeeListPage.openAddAttendeeForm();
    await attendeeListPage.fillTheAttendeeForm(attendeeFormData);
    await attendeeListPage.handleSendEmailConfirmationPopup(true);
    await attendeeListPage.verifyTheCountOfAttendeeToBe(1);
    await attendeeListPage.deleteAttendeeRecord(attendeeFormData.emailId);
    await attendeeListPage.handleDeleteConfirmationPopup(true);
    await attendeeListPage.verifyNotifcationPopsUpWithMessage(
      `Registration deactivated successfully`
    );
  });

  test("TC008: Add attendee just by email from csv modal", async () => {
    attendeeListPage = await peopleSectionPage.openAttendeeTab();
    await attendeeListPage.openCsvDropdown();
    await attendeeListPage.selectImportCsvFromCsvDropDown();
    await attendeeListPage.verifyUploadCsvContainerIsVisible();
    await attendeeListPage.addListOfEmailsForManualAddition([
      "prateek@abc.com",
    ]);
    await attendeeListPage.clickOnImportButtonToSubmitForm();
    await attendeeListPage.handleSendEmailConfirmationPopup(true);
    await attendeeListPage.verifyTheCountOfAttendeeToBe(1);
  });
});
