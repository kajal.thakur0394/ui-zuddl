import test, {
  APIRequestContext,
  BrowserContext,
  expect,
  Page,
} from "@playwright/test";
import {
  SpeakerFormData,
  SpeakersListPage,
} from "../../../page-objects/new-org-side-pages/SpeakersListPage";
import { PeopleSectionPage } from "../../../page-objects/new-org-side-pages/PeopleSectionPage";
import { createNewEvent } from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { getInviteLinkFromSpeakerInviteEmail } from "../../../util/emailUtil";
import { EventInfoDTO } from "../../../dto/eventInfoDto";

test.describe
  .parallel("@people-tab @new-org-side Speaker Tab org side UI", async () => {
    let orgBrowserContext: BrowserContext;
    let orgApiContext: APIRequestContext;
    let eventId;
    let eventTitle;
    let peopleSectionUrl: string;
    let organiserPage: Page;
    let speakersListPage: SpeakersListPage;
    let peopleSectionPage: PeopleSectionPage;
    test.beforeEach(async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({});
      orgApiContext = orgBrowserContext.request;
      eventTitle = Date.now().toString() + "_automationevent";
      eventId = await createNewEvent({
        api_request_context: orgApiContext,
        event_title: eventTitle,
      });
      //get the people tab url
      peopleSectionUrl = `${DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
        }/event/${eventId}/people`;
      organiserPage = await orgBrowserContext.newPage();
      await organiserPage.goto(peopleSectionUrl);
      peopleSectionPage = new PeopleSectionPage(organiserPage);
    });

    test.afterEach(async () => {
      console.log("In teardown, closing the browser context");
      await orgBrowserContext?.close();
    });

    test("TC001: Empty state checks on speakers list page", async () => {
      //expect attendee count is 0
      speakersListPage = await peopleSectionPage.openSpeakerTab();
      await speakersListPage.runEmptyStateChecksForSpeakersList();
    });

    test("TC002: Adding speaker manually by form with just mandatory field", async () => {
      speakersListPage = await peopleSectionPage.openSpeakerTab();
      const speakerFormData: SpeakerFormData = {
        firstName: "prateek",
        emailId: DataUtl.getRandomSpeakerEmail(),
        lastName: "qa",
      };
      await speakersListPage.openAddSpeakerForm();
      await speakersListPage.fillTheSpeakerForm(speakerFormData);
      await speakersListPage.handleSendEmailConfirmationPopup(true);
      await speakersListPage.verifySpeakersRecordCountToBe(1);
    });

    test("TC003: Searching the speaker in list using search function", async () => {
      const speakerFormData: SpeakerFormData = {
        firstName: "prateek",
        emailId: DataUtl.getRandomAttendeeEmail(),
        lastName: "qa",
      };
      speakersListPage = await peopleSectionPage.openSpeakerTab();
      await speakersListPage.openAddSpeakerForm();
      await speakersListPage.fillTheSpeakerForm(speakerFormData);
      await speakersListPage.handleSendEmailConfirmationPopup(true);
      await speakersListPage.verifySpeakersRecordCountToBe(1);
      //search with wrong input first
      await speakersListPage.clickOnSearchIcon();
      await speakersListPage.triggerSearchFor("anythingwhichisnotthere");
      await speakersListPage.verifySpeakersRecordCountToBe(0);
      //now search with right string
      await speakersListPage.triggerSearchFor(speakerFormData.emailId);
      await speakersListPage.verifySpeakersRecordCountToBe(1);
    });

    test("TC004: Resend invite to speaker functionality check", async () => {
      const speakerFormData: SpeakerFormData = {
        firstName: "prateek",
        emailId: DataUtl.getRandomAttendeeEmail(),
        lastName: "qa",
      };
      speakersListPage = await peopleSectionPage.openSpeakerTab();
      await speakersListPage.openAddSpeakerForm();
      await speakersListPage.fillTheSpeakerForm(speakerFormData);
      await speakersListPage.handleSendEmailConfirmationPopup(true);
      const speakerInviteLink1 = await getInviteLinkFromSpeakerInviteEmail(
        speakerFormData.emailId,
        new EventInfoDTO(eventId, eventTitle, false).getInviteEmailSubj()
      );
      expect(
        speakerInviteLink1,
        "expecting speaker recieved invite email"
      ).not.toBeUndefined();

      await speakersListPage.verifySpeakersRecordCountToBe(1);
      await speakersListPage.resendConfirmationEmail(speakerFormData.emailId);
      await speakersListPage.verifyNotifcationPopsUpWithMessage(
        `Invite sent successfully`
      );
      const speakerInviteLink2 = await getInviteLinkFromSpeakerInviteEmail(
        speakerFormData.emailId,
        new EventInfoDTO(eventId, eventTitle, false).getInviteEmailSubj()
      );
      expect(
        speakerInviteLink2,
        "expecting speaker recieved invite email after resend"
      ).not.toBeUndefined();
    });

    test("TC005: Delete speaker functionality check", async () => {
      const speakerFormData: SpeakerFormData = {
        firstName: "prateek",
        emailId: DataUtl.getRandomAttendeeEmail(),
        lastName: "qa",
      };
      speakersListPage = await peopleSectionPage.openSpeakerTab();
      await speakersListPage.openAddSpeakerForm();
      await speakersListPage.fillTheSpeakerForm(speakerFormData);
      await speakersListPage.handleSendEmailConfirmationPopup(true);
      await speakersListPage.verifySpeakersRecordCountToBe(1);
      await speakersListPage.deleteSpeakerRecord(speakerFormData.emailId);
      // await organiserPage.reload();
      await speakersListPage.verifySpeakersRecordCountToBe(0);
    });

    test("TC006: Import csv functionality check", async () => {
      speakersListPage = await peopleSectionPage.openSpeakerTab();
      await speakersListPage.openCsvDropdown();
      await speakersListPage.selectImportCsvFromCsvDropDown();
      await speakersListPage.verifyUploadCsvContainerIsVisible();
      await speakersListPage.triggerImportCsv("speakerImport.csv");
      await speakersListPage.handleSendEmailConfirmationPopup(true);
      await speakersListPage.verifySpeakersRecordCountToBe(1);
    });
  });
