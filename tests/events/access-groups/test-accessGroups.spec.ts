import test, {
  Page,
  BrowserContext,
  APIRequestContext,
} from "@playwright/test";
import {
  Filters,
  FilterLogic,
  FilterOperators,
  FiltersUserInput,
  FilterOperatorsUI,
} from "../../../enums/AccessGroupEnum";
import { DataUtl } from "../../../util/dataUtil";
import StageSize from "../../../enums/StageSizeEnum";
import BrowserName from "../../../enums/BrowserEnum";
import BrowserFactory from "../../../util/BrowserFactory";
import { Features, Roles } from "../../../enums/AccessControlEnum";
import { EventController } from "../../../controller/EventController";
import { AccessGroupController } from "../../../controller/AccessGroupController";
import { CustomAccessGroup } from "../../../page-objects/access-group/CustomAccessGroup";
import { DefaultAccessGroup } from "../../../page-objects/access-group/DefaultAccessGroup";
import { AccessControlController } from "../../../controller/AccessControl/AccessControlController";
import {
  CustomAccessGroupController,
  FilterLogicPair,
} from "../../../controller/AccessControl/CustomAccessGroupController";
import { AccessControlAttendeeVerification } from "../../../page-objects/access-control/roles/Attendees";

test.describe.parallel(`Default access groups`, async () => {
  test.describe.configure({ retries: 2 });

  let eventId;
  let stageId;
  let eventTitle: string;

  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventController: EventController;
  let organiserPageForCustomAccessGroup: Page;
  let organiserPageForDefaultAccessGroup: Page;
  let customAccessGroupController: CustomAccessGroupController;

  test.beforeAll(async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-829",
    });

    await test.step("Initializing Organiser browser and api context.", async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROMIUM,
        laodOrganiserCookies: true,
      });

      orgApiContext = orgBrowserContext.request;
    });

    eventTitle = DataUtl.getRandomEventTitle();
    await test.step("Creating new event.", async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: eventTitle,
      });
    });

    await test.step("Initalizing controllers.", async () => {
      eventController = new EventController(orgApiContext, eventId);
      customAccessGroupController = new CustomAccessGroupController(
        orgApiContext,
        eventId
      );
    });

    await test.step("Initalizing Organiser page.", async () => {
      organiserPageForDefaultAccessGroup = await orgBrowserContext.newPage();
      organiserPageForCustomAccessGroup = await orgBrowserContext.newPage();
    });

    await test.step("Fetching deafult stage id.", async () => {
      stageId = await eventController.getDefaultStageId();
      eventController.disableOnboardingChecksForAttendee();
      eventTitle = await eventController.getEventTitle();
    });
  });

  test.afterAll(async () => {
    await organiserPageForDefaultAccessGroup?.close();
    await organiserPageForCustomAccessGroup?.close();
    await orgBrowserContext?.close();
  });

  test("TC001: API test case. @customAccessGroup", async () => {
    let emailDomain1 = "gmail.com";
    let emailDomain2 = "outlook.com";
    let attendeeOneEmail = DataUtl.getRandomAttendeeEmail(emailDomain1);
    let attendeeTwoEmail = DataUtl.getRandomAttendeeEmail(emailDomain2);
    let accessGroupFiltersLogic = FilterLogic.AND;

    let accessGroupPageObject = new CustomAccessGroup(
      organiserPageForCustomAccessGroup,
      eventId
    );

    await test.step("Invite attendee to the event.", async () => {
      await eventController.inviteAttendeeToTheEvent(attendeeOneEmail);
      await eventController.inviteAttendeeToTheEvent(attendeeTwoEmail);
    });

    let groupId;

    let customGroupName = "Custom Test Group";
    const filters: FiltersUserInput[] = [
      {
        filterName: Filters.EMAIL,
        filterOperator: FilterOperators.CONTAINS,
        compareValue: "gmail",
      },
      {
        filterName: Filters.EMAIL,
        filterOperator: FilterOperators.CONTAINS,
        compareValue: "abc",
      },
    ];
    const filterLogic: FilterLogicPair[] = [
      { key: Filters.EMAIL, value: FilterLogic.OR },
    ];

    await test.step("Create Custom access groups", async () => {
      groupId = await customAccessGroupController.createCustomGroup(
        customGroupName,
        "ACTIVE",
        true,
        filters,
        filterLogic,
        accessGroupFiltersLogic
      );
    });

    await test.step("Get attendee email list", async () => {
      await customAccessGroupController.fetchAudienceList(
        groupId,
        accessGroupFiltersLogic
      );
    });

    await test.step("Verify attendeeOne is added but attendeeTwo and attendeeThree is not added to GroupOne.", async () => {
      await accessGroupPageObject.getDefaultAccessGroup.verifyTheUserEmailVisibilityUnderRelevantSection(
        attendeeOneEmail,
        "Attendee"
      );

      await accessGroupPageObject.verifyEmailIdAddedInCustomGroup(
        customGroupName,
        attendeeOneEmail
      );
    });
  });

  test("TC002: Default Access Group. @defaultAccessGroup", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-834",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-835",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-836",
    });

    let accessGroupPageObject = new DefaultAccessGroup(
      organiserPageForDefaultAccessGroup,
      eventId
    );
    let attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
    let speakerOneEmail = DataUtl.getRandomSpeakerEmail();

    await test.step("Invite attendee to the event.", async () => {
      await eventController.inviteAttendeeToTheEvent(attendeeOneEmail);
    });

    await test.step("Verify the 'Registration' section is visible and attendee email is added under 'Registrations' group", async () => {
      await accessGroupPageObject.verifyRoleVisibility("Registrations", true);

      await accessGroupPageObject.verifyTheUserEmailVisibilityUnderRelevantSection(
        attendeeOneEmail,
        "Attendee"
      );
    });

    await test.step("Invite speaker to the event.", async () => {
      await eventController.inviteSpeakerToTheEventByApi(
        speakerOneEmail,
        "Automation",
        "TestSpeaker"
      );
    });

    await test.step("Verify the 'Registration' section is visible and speaker email is added under 'Speaker' group", async () => {
      await accessGroupPageObject.verifyRoleVisibility("Registrations", true);

      await accessGroupPageObject.verifyTheUserEmailVisibilityUnderRelevantSection(
        speakerOneEmail,
        "Speaker"
      );
    });
  });

  test("TC003: Custom Access Group. @customAccessGroup", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-833",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-837",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-838",
    });

    let groupName1 = "Test Custom Group1";
    let groupName2 = "Test Custom Group2";
    let emailDomain1 = "gmail.com";
    let emailDomain2 = "outlook.com";
    let accessGroupPageObject = new CustomAccessGroup(
      organiserPageForCustomAccessGroup,
      eventId
    );
    let attendeeOneEmail = DataUtl.getRandomAttendeeEmail(emailDomain1);
    let attendeeTwoEmail = DataUtl.getRandomAttendeeEmail(emailDomain2);
    let attendeeThreeEmail = DataUtl.getRandomAttendeeEmail();

    await test.step("Create Custom access groups", async () => {
      await accessGroupPageObject.createCustomAccessGroup({
        groupName: groupName1,
        groupType: "Active",
        enableAccessControls: true,
        filters: [Filters.EMAIL],
        ifConditions: [FilterOperatorsUI.CONTAINS],
        conditionData: [emailDomain1],
      });

      await accessGroupPageObject.createCustomAccessGroup({
        groupName: groupName2,
        groupType: "Active",
        enableAccessControls: true,
        filters: [Filters.EMAIL, Filters.STATUS],
        ifConditions: [FilterOperatorsUI.CONTAINS, FilterOperatorsUI.FALSE],
        conditionData: [emailDomain2],
        filterLogic: "AND",
      });
    });

    await test.step("Verify group is created successfully.", async () => {
      await accessGroupPageObject.verifyCustomAccessGroupIsVisible(groupName1);
      await accessGroupPageObject.verifyCustomAccessGroupIsVisible(groupName2);
    });

    await test.step("Invite attendee to the event.", async () => {
      await eventController.inviteAttendeeToTheEvent(attendeeOneEmail);
      await eventController.inviteAttendeeToTheEvent(attendeeTwoEmail);
      await eventController.inviteAttendeeToTheEvent(attendeeThreeEmail);
    });

    await test.step("Verify attendee is added in the Registrations group.", async () => {
      await accessGroupPageObject.getDefaultAccessGroup.verifyTheUserEmailVisibilityUnderRelevantSection(
        attendeeOneEmail,
        "Attendee"
      );

      await accessGroupPageObject.getDefaultAccessGroup.verifyTheUserEmailVisibilityUnderRelevantSection(
        attendeeTwoEmail,
        "Attendee"
      );

      await accessGroupPageObject.getDefaultAccessGroup.verifyTheUserEmailVisibilityUnderRelevantSection(
        attendeeThreeEmail,
        "Attendee"
      );
    });

    await test.step("Verify attendeeOne is added but attendeeTwo and attendeeThree is not added to GroupOne.", async () => {
      await accessGroupPageObject.verifyEmailIdAddedInCustomGroup(
        groupName1,
        attendeeOneEmail
      );

      await accessGroupPageObject.verifyEmailIdAddedInCustomGroup(
        groupName1,
        attendeeTwoEmail,
        false
      );

      await accessGroupPageObject.verifyEmailIdAddedInCustomGroup(
        groupName1,
        attendeeThreeEmail,
        false
      );
    });

    await test.step("Verify attendeeTwo is added but attendeeOne and attendeeThree is not added to GroupTwo.", async () => {
      await accessGroupPageObject.verifyEmailIdAddedInCustomGroup(
        groupName2,
        attendeeTwoEmail
      );

      await accessGroupPageObject.verifyEmailIdAddedInCustomGroup(
        groupName2,
        attendeeOneEmail,
        false
      );

      await accessGroupPageObject.verifyEmailIdAddedInCustomGroup(
        groupName2,
        attendeeThreeEmail,
        false
      );
    });
  });

  test("TC004: Access Control. @accessControl", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-845",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-865",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-866",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-867",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-868",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-869",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-873",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-545",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-546",
    });

    let groupId;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let accessControlObject: AccessControlController;
    let attendeeBrowserContext: BrowserContext;
    let attendeePage: Page;

    await test.step("Initializing Attendee browser context and page.", async () => {
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROMIUM,
        laodOrganiserCookies: false,
      });
      attendeePage = await attendeeBrowserContext.newPage();
    });

    let attendeeVerificaton = new AccessControlAttendeeVerification(
      attendeePage,
      eventId,
      attendeeEmail
    );

    await test.step("Invite attendee to the event.", async () => {
      await eventController.inviteAttendeeToTheEvent(attendeeEmail);
    });

    await test.step("Fetch attendee group id", async () => {
      let accessGroupController = new AccessGroupController(
        orgApiContext,
        eventId
      );
      groupId = await accessGroupController.fetchAccessGroupId(Roles.ATTENDEES);
    });

    await test.step("Initialize Access Control Controller.", async () => {
      accessControlObject = new AccessControlController(
        orgApiContext,
        eventId,
        groupId
      );
    });

    await test.step("Lobby", async () => {
      await test.step("Disable Lobby for user.", async () => {
        await accessControlObject.toggleAccessForFeature(Features.LOBBY, false);
      });

      await test.step("Verify user is unable to access Lobby.", async () => {
        await attendeeVerificaton.attendeeJoinsTheEvent();
        await attendeeVerificaton.verifyNavbarSections(Features.LOBBY, false);
      });

      await test.step("Enable Lobby for user.", async () => {
        await accessControlObject.toggleAccessForFeature(Features.LOBBY, true);
      });

      await test.step("Verify user is able to access Lobby.", async () => {
        await attendeeVerificaton.reloadPage();
        await attendeeVerificaton.verifyNavbarSections(Features.LOBBY, true);
      });
    });

    await test.step("Schedule", async () => {
      await test.step("Disable Schedule for user.", async () => {
        await accessControlObject.toggleAccessForFeature(
          Features.SCHEDULE,
          false
        );
      });

      await test.step("Verify user is unable to access Schedule.", async () => {
        await attendeeVerificaton.reloadPage();
        await attendeeVerificaton.verifyNavbarSections(
          Features.SCHEDULE,
          false
        );
      });

      await test.step("Enable Schedule for user.", async () => {
        await accessControlObject.toggleAccessForFeature(
          Features.SCHEDULE,
          true
        );
      });

      await test.step("Verify user is able to access Schedule.", async () => {
        await attendeeVerificaton.reloadPage();
        await attendeeVerificaton.verifyNavbarSections(Features.SCHEDULE, true);
      });
    });

    await test.step("Stage", async () => {
      await test.step("Disable Stage for user.", async () => {
        await accessControlObject.toggleDefaultStageAccess(eventTitle, false);
      });

      await test.step("Verify user is unable to access Stage.", async () => {
        await attendeeVerificaton.attendeeJoinsTheEvent();
        await attendeeVerificaton.verifyNavbarSections(Features.STAGE, false);
      });

      await test.step("Enable Stage for user.", async () => {
        await accessControlObject.toggleDefaultStageAccess(eventTitle, true);
      });

      await test.step("Verify user is able to access Stage.", async () => {
        await attendeeVerificaton.reloadPage();
        await attendeeVerificaton.verifyNavbarSections(Features.STAGE, true);
      });
    });

    await test.step("Rooms", async () => {
      await test.step("Disable Rooms for user.", async () => {
        await accessControlObject.toggleAccessForFeature(Features.ROOM, false);
      });

      await test.step("Verify user is unable to access Room.", async () => {
        await attendeeVerificaton.reloadPage();
        await attendeeVerificaton.verifyNavbarSections(Features.ROOM, false);
      });

      await test.step("Enable Rooms for user.", async () => {
        await accessControlObject.toggleAccessForFeature(Features.ROOM, true);
      });

      await test.step("Verify user is able to access Rooms.", async () => {
        await attendeeVerificaton.reloadPage();
        await attendeeVerificaton.verifyNavbarSections(Features.ROOM, true);
      });
    });

    await test.step("Expo", async () => {
      await test.step("Disable Expo for user.", async () => {
        await accessControlObject.toggleAccessForFeature(Features.EXPO, false);
      });

      await test.step("Verify user is unable to access Expo.", async () => {
        await attendeeVerificaton.reloadPage();
        await attendeeVerificaton.verifyNavbarSections(Features.EXPO, false);
      });

      await test.step("Enable Expo for user.", async () => {
        await accessControlObject.toggleAccessForFeature(Features.EXPO, true);
      });

      await test.step("Verify user is able to access Expo.", async () => {
        await attendeeVerificaton.reloadPage();
        await attendeeVerificaton.verifyNavbarSections(Features.EXPO, true);
      });
    });

    await test.step("Networking", async () => {
      await test.step("Disable Networking for user.", async () => {
        await accessControlObject.toggleAccessForFeature(
          Features.NETWORKING,
          false
        );
      });

      await test.step("Verify user is unable to access Networking.", async () => {
        await attendeeVerificaton.reloadPage();
        await attendeeVerificaton.verifyNavbarSections(
          Features.NETWORKING,
          false
        );
      });

      await test.step("Enable Networking for user.", async () => {
        await accessControlObject.toggleAccessForFeature(
          Features.NETWORKING,
          true
        );
      });

      await test.step("Verify user is able to access Networking.", async () => {
        await attendeeVerificaton.reloadPage();
        await attendeeVerificaton.verifyNavbarSections(
          Features.NETWORKING,
          true
        );
      });
    });
  });

  test("TC005: Access Control - Stage. @accessControlStage", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-549",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-551",
    });

    let groupId;
    let attendeeBrowserContext: BrowserContext;
    let attendeePage: Page;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let accessControlObject: AccessControlController;
    let attendeeVerificaton: AccessControlAttendeeVerification;

    let newStageName1 = "SecondStage";
    let newStageName2 = "ThirdStage";

    await test.step("Fetch attendee group id", async () => {
      let accessGroupController = new AccessGroupController(
        orgApiContext,
        eventId
      );
      groupId = await accessGroupController.fetchAccessGroupId(Roles.ATTENDEES);
    });

    await test.step("Initialize Access Control Controller.", async () => {
      accessControlObject = new AccessControlController(
        orgApiContext,
        eventId,
        groupId
      );
    });

    await test.step("Invite attendee to the event.", async () => {
      await eventController.inviteAttendeeToTheEvent(attendeeEmail);
    });

    await test.step("Initializing Attendee browser context and page.", async () => {
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROMIUM,
        laodOrganiserCookies: false,
      });

      attendeePage = await attendeeBrowserContext.newPage();

      attendeeVerificaton = new AccessControlAttendeeVerification(
        attendeePage,
        eventId,
        attendeeEmail
      );
    });

    await test.step("Verify the Stage is visible.", async () => {
      await attendeeVerificaton.attendeeJoinsTheEvent();
      await attendeeVerificaton.verifyNavbarSections(Features.STAGE, true);
    });

    await test.step("Disable default Stage access for user.", async () => {
      await accessControlObject.toggleDefaultStageAccess(eventTitle, false);
    });

    await test.step("Verify the Stage is not visible once the default stage is disabled.", async () => {
      await attendeeVerificaton.reloadPage();
      await attendeeVerificaton.verifyNavbarSections(Features.STAGE, false);
    });

    await test.step("Enable default Stage access for user.", async () => {
      await accessControlObject.toggleDefaultStageAccess(eventTitle, true);
    });

    await test.step("Verify the Stage is visible once the default stage is enabled.", async () => {
      await attendeeVerificaton.reloadPage();
      await attendeeVerificaton.verifyNavbarSections(Features.STAGE, true);
    });

    await test.step("Add new stages to the event.", async () => {
      await accessControlObject.getStageController.addNewStageToEvent({
        name: newStageName1,
        size: StageSize.MEDIUM,
      });

      await accessControlObject.getStageController.addNewStageToEvent({
        name: newStageName2,
        size: StageSize.MEDIUM,
      });
    });

    await test.step("Disable new Stage access for user.", async () => {
      await accessControlObject.toggleStageAccess(newStageName1, false);
    });

    await test.step("Verify user is unable to see diabled stage Stage.", async () => {
      await attendeeVerificaton.reloadPage();
      await attendeeVerificaton.verifyStageVisibility(newStageName1, false);
    });

    await test.step("Enable Stage for user.", async () => {
      await accessControlObject.toggleStageAccess(newStageName1, true);
    });

    await test.step("Verify user is able to see and access the Stage.", async () => {
      await attendeeVerificaton.reloadPage();
      await attendeeVerificaton.verifyStageVisibility(newStageName1, true);
      await attendeeVerificaton.verifyStageAccessibiity(newStageName1);
    });
  });
});
