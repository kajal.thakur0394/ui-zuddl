import test, { BrowserContext, APIRequestContext } from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import StageSize from "../../../enums/StageSizeEnum";
import BrowserName from "../../../enums/BrowserEnum";
import { Roles } from "../../../enums/AccessControlEnum";
import BrowserFactory from "../../../util/BrowserFactory";
import VenueSection from "../duplicate-events/venueSection";
import { StageController } from "../../../controller/StageController";
import { EventController } from "../../../controller/EventController";
import { StudioController } from "../../../controller/StudioController";
import { AccessGroupController } from "../../../controller/AccessGroupController";
import { ScheduleAccessControl } from "../../../controller/AccessControl/AccessControl_Schedule";
import { AccessControlController } from "../../../controller/AccessControl/AccessControlController";
import { AccessControlAttendeeVerification } from "../../../page-objects/access-control/roles/Attendees";
import { CustomAccessGroupController } from "../../../controller/AccessControl/CustomAccessGroupController";

test.describe(`Access Control`, async () => {
  let eventId;
  let eventTitle: string;
  let defaultStageId;

  let orgApiContext: APIRequestContext;
  let orgBrowserContext: BrowserContext;

  let eventController: EventController;
  let stageController: StageController;
  let scheduleSection: VenueSection;
  let scheduleAccessController: ScheduleAccessControl;
  let customAccessGroupController: CustomAccessGroupController;
  let accessGroupController: AccessGroupController;
  let attendeeVerificaton: AccessControlAttendeeVerification;
  let accessControl: AccessControlController;

  let attendeeEmail: string;
  let attendeeBrowserContext: BrowserContext;
  let attendeePage;

  test.beforeAll(async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-856",
    });

    await test.step("Initializing Organiser browser and api context.", async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: true,
      });

      orgApiContext = orgBrowserContext.request;
    });

    eventTitle = DataUtl.getRandomEventTitle();
    await test.step("Creating new event.", async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: eventTitle,
      });
    });

    await test.step("Initalizing controllers.", async () => {
      eventController = new EventController(orgApiContext, eventId);
      customAccessGroupController = new CustomAccessGroupController(
        orgApiContext,
        eventId
      );
      stageController = new StageController(orgApiContext, eventId);
      scheduleSection = new VenueSection(orgApiContext, eventId);
      accessGroupController = new AccessGroupController(orgApiContext, eventId);
    });

    await test.step("Fetching deafult stage id.", async () => {
      defaultStageId = await eventController.getDefaultStageId();
    });

    await test.step("Disabling attendee onboarding checks.", async () => {
      await eventController.disableOnboardingChecksForAttendee();
    });

    await test.step("Generate random attendee email.", async () => {
      attendeeEmail = DataUtl.getRandomAttendeeEmail();
    });

    await test.step("Fetch Access group id and initialize Schedule and AccessControl Controllers.", async () => {
      let groupId = await accessGroupController.fetchAccessGroupId(
        Roles.ATTENDEES
      );

      scheduleAccessController = new ScheduleAccessControl(
        orgApiContext,
        eventId,
        groupId
      );

      accessControl = new AccessControlController(
        orgApiContext,
        eventId,
        groupId
      );
    });

    await test.step("Invite attendee to the event.", async () => {
      await eventController.inviteAttendeeToTheEvent(attendeeEmail);
    });

    await test.step("Initializing Attendee browser context and page.", async () => {
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });

      attendeePage = await attendeeBrowserContext.newPage();

      attendeeVerificaton = new AccessControlAttendeeVerification(
        attendeePage,
        eventId,
        attendeeEmail
      );
    });

    await test.step("Attendee joins the event.", async () => {
      await attendeeVerificaton.attendeeJoinsTheEvent();
    });
  });

  test.afterAll(async () => {
    await orgBrowserContext?.close();
  });

  test("TC001: Access Control - Session.", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-550",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-552",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-588",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-872",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-588",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-542",
    });

    const stageTwoName = "TestStage2";
    let newStageId;

    await test.step("Fetch Sessions.", async () => {
      await scheduleAccessController.fetchSessions();
    });

    let sessionOneId;
    let sessionOneName = "DefaultStage-Session";
    await test.step("Add Session to the event.", async () => {
      const sessionResponse = await scheduleSection.createSegment(
        "STAGE",
        defaultStageId,
        [],
        [],
        sessionOneName
      );
      sessionOneId = sessionResponse[0];
    });

    await test.step("Verify Session on live event side.", async () => {
      await attendeeVerificaton.verifyScheduleVisibility(sessionOneName, true);
    });

    await test.step("Remove the today's day access and verify that 'No access today' message is displayed.", async () => {
      await accessControl.toggleDayAccess(1, false);
      await attendeeVerificaton.verifyNoAccessMessage();
    });

    await test.step("Enable today's day access and verify that today's Sessions are visible to Attendee on live event side.", async () => {
      await accessControl.toggleDayAccess(1, true);
      await attendeeVerificaton.verifyScheduleVisibility(sessionOneName, true);
    });

    await test.step("Remove Session Access for attendee and verify Session is not visible to Attendee on live event side.", async () => {
      await scheduleAccessController.toggleSessionAccess(sessionOneId, false);
      await attendeeVerificaton.verifyScheduleVisibility(sessionOneName, false);
    });

    await test.step("Enable Session Access for attendee and verify Session is visible to Attendee on live event side.", async () => {
      await scheduleAccessController.toggleSessionAccess(sessionOneId, true);
      await attendeeVerificaton.verifyScheduleVisibility(sessionOneName, true);
    });

    await test.step("Add new Stage to the event.", async () => {
      newStageId = await stageController.addNewStageToEvent({
        name: stageTwoName,
        size: StageSize.MEDIUM,
      });
    });

    let sessionTwoName = "NewStage-Session";
    await test.step("Add a Session to the event.", async () => {
      await scheduleSection.createSegment(
        "STAGE",
        newStageId,
        [],
        [],
        sessionTwoName
      );
    });

    await test.step("Verify Session is visible on live event side.", async () => {
      await attendeeVerificaton.verifyScheduleVisibility(sessionTwoName, true);
    });

    await test.step("Verify the Stage name is visible under schedule stage filter.", async () => {
      await attendeeVerificaton.verifyStageNameVisibilityUnderScheduleFilter(
        stageTwoName,
        true
      );
    });

    let roomId;
    let roomName = "Private Room 1";
    await test.step("Add Private Room to the event.", async () => {
      await accessControl.getRoomsController.addRoomsToEvent({
        seatsPerRoom: 10,
        numberOfRooms: 2,
        roomCategory: "PRIVATE",
      });
      roomId = await accessControl.getRoomsController.getRoomId(roomName);
    });

    await test.step("Verify the room is added to the event.", async () => {
      await accessControl.getRoomsController.getListOfRooms();
      await attendeeVerificaton.verifyRoomVisibility(roomName, true);
    });

    let sessionThreeName = "Room-Session";
    await test.step("Add Session to the event linked to Room.", async () => {
      await scheduleSection.createSegment(
        "ROOMS",
        roomId,
        [],
        [],
        sessionThreeName
      );
    });

    await test.step("Verify Session is visible on live event side.", async () => {
      await attendeeVerificaton.verifyScheduleVisibility(
        sessionThreeName,
        true
      );
    });

    await test.step("Remove the Stage access linked with Session and verify that the Session is not visible to Attendee on live event side.", async () => {
      await accessControl.toggleStageAccess(stageTwoName, false);
      await attendeeVerificaton.verifyScheduleVisibility(sessionTwoName, false);
    });

    await test.step("Verify the disabled Stage name is not visible under schedule stage filter.", async () => {
      await attendeeVerificaton.verifyStageNameVisibilityUnderScheduleFilter(
        stageTwoName,
        false
      );
    });

    await test.step("Remove the Room access linked with Session and verify that the Session is not visible to Attendee on live event side.", async () => {
      await accessControl.toggleRoomAccess("ACCESS_PVT_ROOMS", false);
      await attendeeVerificaton.verifyRoomVisibility(roomName, false);
    });
  });

  test("TC002: Access Control - Rooms.", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-555",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-556",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-557",
    });

    let roomId;
    let roomName = "Room 1";
    await test.step("Add Public rooms in the event.", async () => {
      await accessControl.getRoomsController.addRoomsToEvent({
        seatsPerRoom: 10,
        numberOfRooms: 2,
        roomCategory: "PUBLIC",
      });
      roomId = await accessControl.getRoomsController.getRoomId(roomName);

      await test.step("Verify the room is added to the event.", async () => {
        await accessControl.getRoomsController.getListOfRooms();
        await attendeeVerificaton.verifyRoomVisibility(roomName, true);
      });
    });

    await test.step("Remove Private rooms access and verify the user is only able to see Public rooms.", async () => {
      await accessControl.toggleRoomAccess("ACCESS_PVT_ROOMS", false);
      await attendeeVerificaton.verifyRoomVisibility(roomName, true);
    });

    await test.step(".", async () => {});
  });

  test("TC003: Access Control - Stage.", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-553",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-874",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-875",
    });
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-547",
    });

    let stageOneName = "Accessible Stage";
    let stageTwoName = "Non-Accessible Stage";
    let stageOneId;
    let stageTwoId;
    await test.step("Add two stages to the event.", async () => {
      await test.step("Add new Stage to the event.", async () => {
        stageOneId = await stageController.addNewStageToEvent({
          name: stageOneName,
          size: StageSize.MEDIUM,
        });
      });
      await test.step("Add new Stage to the event.", async () => {
        stageTwoId = await stageController.addNewStageToEvent({
          name: stageTwoName,
          size: StageSize.MEDIUM,
        });
      });

      await test.step("Verify the Stages are added to the event.", async () => {
        await attendeeVerificaton.verifyStageVisibility(stageOneName, true);
        await attendeeVerificaton.verifyStageVisibility(stageTwoName, true);
      });

      await test.step("Verify the Stages are are accessible via link.", async () => {
        await attendeeVerificaton.verifyStageAccessViaLink(stageOneId, true);
        await attendeeVerificaton.verifyStageAccessViaLink(stageTwoId, true);
      });
    });

    let sessionOneId;
    let sessionOneName = "AccessibleStage-Session";
    await test.step("Add Session to the event.", async () => {
      const sessionResponse = await scheduleSection.createSegment(
        "STAGE",
        stageOneId,
        [],
        [],
        sessionOneName
      );
      sessionOneId = sessionResponse[0];
    });

    await test.step("Verify Session on live event side.", async () => {
      await attendeeVerificaton.verifyScheduleVisibility(sessionOneName, true);
    });

    let sessionTwoId;
    let sessionTwoName = "NonAccessible-Session";
    await test.step("Add Session to the event.", async () => {
      const sessionResponse = await scheduleSection.createSegment(
        "STAGE",
        stageTwoId,
        [],
        [],
        sessionTwoName
      );
      sessionTwoId = sessionResponse[0];
    });

    await test.step("Verify Session on live event side.", async () => {
      await attendeeVerificaton.verifyScheduleVisibility(sessionTwoName, true);
    });

    await test.step("Remove access for StageTwo.", async () => {
      await accessControl.toggleStageAccess(stageTwoName, false);
    });

    await test.step("Verify the StageTwo is not accessible via link.", async () => {
      await attendeeVerificaton.verifyStageAccessViaLink(stageTwoId, false);
    });

    await test.step("Verify Session linked to StageTwo is not visible on live event side.", async () => {
      await attendeeVerificaton.verifyScheduleVisibility(sessionTwoName, false);
    });

    let studioOneId;
    let studioTwoId;
    await test.step("Enable Studio and disable Recording for both stages.", async () => {
      await test.step("Enabling Studio as backstage for StageOne.", async () => {
        studioOneId = await eventController.enableStudioAsBackstage(stageOneId);
      });

      await test.step("Enabling Studio as backstage for StageTwo.", async () => {
        studioTwoId = await eventController.enableStudioAsBackstage(stageTwoId);
      });

      await test.step(`Disabling recording for StageOne.`, async () => {
        let stageController = new StageController(
          orgApiContext,
          eventId,
          stageOneId
        );
        await stageController.disableRecordingForStudioAsBackstage();
      });

      await test.step(`Disabling recording for StageTwo.`, async () => {
        let stageController = new StageController(
          orgApiContext,
          eventId,
          stageTwoId
        );
        await stageController.disableRecordingForStudioAsBackstage();
      });
    });

    let studioOneController;
    let studioTwoController;
    await test.step(`Initialize Studio Controllers for both Studios.`, async () => {
      studioOneController = new StudioController(
        orgApiContext,
        studioOneId,
        stageOneId
      );

      studioTwoController = new StudioController(
        orgApiContext,
        studioTwoId,
        stageTwoId
      );
    });

    await test.step("Start session on StageOne (Access enabled for attendee) and verify it's visible to the Attendee.", async () => {
      await studioOneController.startSessionOnStudio(sessionOneId, false);

      await attendeeVerificaton.verifySessionLiveNotification(
        sessionOneName,
        true
      );
    });

    await test.step("End the session.", async () => {
      await studioOneController.stopSessionOnStudio();
    });

    await test.step("Start session on StageTwo (Access disabled for attendee) and verify it's visible to the Attendee.", async () => {
      await studioTwoController.startSessionOnStudio(sessionTwoId, false);

      await attendeeVerificaton.verifySessionLiveNotification(
        sessionTwoName,
        false
      );
    });

    await test.step("End the session.", async () => {
      await studioTwoController.stopSessionOnStudio();
    });
  });
});
