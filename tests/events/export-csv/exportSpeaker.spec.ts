import test, {
  APIRequestContext,
  APIResponse,
  BrowserContext,
  expect,
  Page,
} from "@playwright/test";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import LandingPageType from "../../../enums/landingPageEnum";
import { LandingPageTwo } from "../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
import { LoginOptionsComponent } from "../../../page-objects/events-pages/landing-page-2/LoginOptionsComponent";
import { AttendeeListPage } from "../../../page-objects/new-org-side-pages/AttendeeListPage";
import {
  checkInAttendee,
  createNewEvent,
  deleteAttendeeFromEvent,
  getEventDetails,
  getRegistrationId,
  inviteSpeakerByApi,
  registerUserToEventbyApi,
  updateEventLandingPageDetails,
  updateLandingPageType,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { readCsvFileAsJson } from "../../../util/commonUtil";
import { DataUtl } from "../../../util/dataUtil";
import { SpeakerData } from "../../../test-data/speakerData";
import { SpeakersListPage } from "../../../page-objects/new-org-side-pages/SpeakersListPage";
import { getInviteLinkFromSpeakerInviteEmail } from "../../../util/emailUtil";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";

test.describe
  .parallel("@registration @deleteAttendee @landing-page-2", async () => {
  let eventTitle: string;
  let eventId;
  let attendeeLandingPage: string;
  let speakerLandingpPage: string;
  let eventInfoDto: EventInfoDTO;
  let attendeePage: Page;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let organiserPage: Page;
  let landingPageId;

  test.beforeEach(async ({ context }) => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = await orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: eventTitle,
    });
    eventInfoDto = new EventInfoDTO(eventId, eventTitle, true); // true to denote magic link enabled
    const event_details_api_resp = await (
      await getEventDetails(orgApiContext, eventId)
    ).json();
    console.log("event_details_api_resp :", event_details_api_resp);
    landingPageId = event_details_api_resp.eventLandingPageId;
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
    speakerLandingpPage = eventInfoDto.speakerLandingPage;
    // get driver for attendee
    attendeePage = await context.newPage();
    // Updating event landing page type
    await updateLandingPageType(
      orgApiContext,
      eventId,
      landingPageId,
      LandingPageType.STYLE2,
      "true",
      EventEntryType.REG_BASED
    );
  });

  test.afterEach(async () => {
    console.log("In teardown, closing the browser context");
    await orgBrowserContext?.close();
  });
  test("TC001: export csv should show the magic link of speaker", async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-352/verify-in-export-csv-for-speaker-the-magic-link-is-present-and-its",
    });
    organiserPage = await orgBrowserContext.newPage();
    let speakerUser = new SpeakerData(
      DataUtl.getRandomAttendeeEmail(),
      "Prateek"
    );
    let jsonData;
    await test.step(` Adding speaker to the event`, async () => {
      console.log(`Speaker data to add is ${speakerUser}`);
      await inviteSpeakerByApi(
        orgApiContext,
        speakerUser.getSpeakerDataObject(),
        eventInfoDto.eventId,
        true
      );
    });

    await test.step(`verify the download csv contains 1 speaker data`, async () => {
      await organiserPage.goto(
        `${
          DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
        }/event/${eventId}/people/speakers`
      );
      const [download] = await Promise.all([
        organiserPage.waitForEvent("download"),
        new SpeakersListPage(organiserPage).downloadCsv(),
      ]);
      console.log(await download.path());
      await download.saveAs("test-data/downloadSpeaker.csv");
      jsonData = await readCsvFileAsJson("test-data/downloadSpeaker.csv");
      console.log(jsonData);
      expect(jsonData, "expecting csv to contain 1 record").toHaveLength(1);
    });

    await test.step(`verify the download csv contains magic link and magic link is working`, async () => {
      let speakerInviteLink: string = jsonData[0]["Magic Link"];
      console.log(`speaker invite link is ${speakerInviteLink}`);
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(speakerInviteLink); // should load the speaker landing page
      await landingPageOne.isStageLoaded();
    });
  });
});
