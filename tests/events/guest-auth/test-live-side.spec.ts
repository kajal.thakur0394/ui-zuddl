import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import LandingPageType from "../../../enums/landingPageEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { LandingPageTwo } from "../../../page-objects/events-pages/landing-page-2/LandingPageTwo";

import {
  createNewEvent,
  enableGuestAuthForEvent,
  updateEventLandingPageDetails,
  updateLandingPageType,
  getEventDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";

test.describe.parallel("@registration @guestauth", async () => {
  // test.describe.configure({ retries: 2 });
  let eventTitle: string;
  let eventId;
  let attendeeLandingPage: string;
  let speakerLandingpPage: string;
  let eventInfoDto: EventInfoDTO;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let landingPageId;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: eventTitle,
    });
    //enable guest auth
    await enableGuestAuthForEvent(
      orgApiContext,
      eventId,
      EventEntryType.REG_BASED
    );
    eventInfoDto = new EventInfoDTO(eventId, eventTitle, true); // true to denote magic link enabled
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
    speakerLandingpPage = eventInfoDto.speakerLandingPage;
    // get driver for attendee
    const event_details_api_resp = await (
      await getEventDetails(orgApiContext, eventId)
    ).json();
    console.log("event_details_api_resp :", event_details_api_resp);
    landingPageId = event_details_api_resp.eventLandingPageId;
  });

  test.afterEach(async () => {
    console.log("In teardown, closing the browser context");
    await orgBrowserContext?.close();
  });

  test("TC001: Guest auth on landing page 1 with magic link disabled", async ({
    page,
  }) => {
    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: false,
      isGuestAuthEnabled: true,
    });
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await page.goto(attendeeLandingPage);
    let landingPageOne = new LandingPageOne(page);
    let guestAuthFormComponent = await landingPageOne.clickOnLoginAsGuest();
    await guestAuthFormComponent.fillGuestAuthForm({
      firstName: "prateek",
      lastName: "automation",
      email: attendeeEmail,
    });
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC002: Guest auth on landing page 1 with magic link enabled", async ({
    page,
  }) => {
    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
      isGuestAuthEnabled: true,
    });
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await page.goto(attendeeLandingPage);
    let landingPageOne = new LandingPageOne(page);
    let guestAuthFormComponent = await landingPageOne.clickOnLoginAsGuest();
    await guestAuthFormComponent.fillGuestAuthForm({
      firstName: "prateek",
      lastName: "automation",
      email: attendeeEmail,
    });
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC003: verify if guest auth is disabled, its not visible on landing pages", async ({
    page,
  }) => {
    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: false,
      isGuestAuthEnabled: false,
    });
    await page.goto(attendeeLandingPage);
    let landingPageOne = new LandingPageOne(page);
    await expect(
      landingPageOne.loginAsGuestButton,
      "expecting guest login button to be hidden"
    ).toBeHidden();
  });

  test("TC004: User if even uses same email , new user will be created", async () => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let guestAuthRegResp1 = await orgApiContext.post(
      `/api/auth/guest?eventId=${eventId}`,
      {
        data: {
          email: attendeeEmail,
          firstName: "Prateek",
          lastName: "Automation",
        },
      }
    );
    if (guestAuthRegResp1.status() != 200) {
      console.log(
        `The API to register attendee as guest failed with ${guestAuthRegResp1.status()}`
      );
    }

    let accountIdOnReg1 = await guestAuthRegResp1.json()["accountId"];
    let systemGenEmail1 = await guestAuthRegResp1.json()["email"];
    console.log(
      `On submitting guest auth reg form, the account id = ${accountIdOnReg1} and email = ${systemGenEmail1} `
    );

    //calling same post request again with same attendee email
    let guestAuthRegResp2 = await orgApiContext.post(
      `/api/auth/guest?eventId=${eventId}`,
      {
        data: {
          email: attendeeEmail,
          firstName: "Prateek",
          lastName: "Automation",
        },
      }
    );
    if (guestAuthRegResp2.status() != 200) {
      console.log(
        `The API to register attendee as guest failed with ${guestAuthRegResp2.status()}`
      );
    }

    let accountIdOnReg2 = await guestAuthRegResp2.json()["accountId"];
    let systemGenEmail2 = await guestAuthRegResp2.json()["email"];
    console.log(
      `On submitting guest auth reg form again, the account id = ${accountIdOnReg1} and email = ${systemGenEmail1} `
    );

    expect(
      accountIdOnReg1,
      "expecting account id to be different when same email was submitted for guest auth"
    ).toBe(accountIdOnReg2);

    expect(
      systemGenEmail1,
      "expecting system gen email id to be different when same email was submitted for guest auth"
    ).toBe(systemGenEmail2);
  });

  test("TC005: Guest auth should not be visible on speaker landing page", async ({
    page,
  }) => {
    await page.goto(speakerLandingpPage);
    let landingPageOne = new LandingPageOne(page);
    await expect(
      landingPageOne.loginAsGuestButton,
      "expecting guest auth button to be hidden on speaker landing page"
    ).toBeHidden();
  });

  //Login Using Guest Auth in Landing Page 2
  test("TC006: Guest auth on landing page 2", async ({ page }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-246/automating-guest-auth-login-for-landing-page-v2",
    });

    await test.step("Updating Event Landing Page Details", async () => {
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isGuestAuthEnabled: true,
      });
    });

    await test.step("Updating Landing Page 1 to Landing Page 2", async () => {
      await updateLandingPageType(
        orgApiContext,
        eventId,
        landingPageId,
        LandingPageType.STYLE2,
        "true",
        EventEntryType.REG_BASED
      );
    });

    await test.step("Attendee Entered Landing Page 2", async () => {
      await page.goto(attendeeLandingPage);
    });

    await test.step("Attendee Clicked on Login as Guest Button, filled the details and clicked on Enter Now Button", async () => {
      let landingPageTwo = new LandingPageTwo(page);
      let guestAuthFormComponent = await landingPageTwo.clickOnLoginAsGuest();
      let attendeeEmail = DataUtl.getRandomAttendeeEmail();

      await guestAuthFormComponent.fillGuestAuthForm({
        firstName: "prateek",
        lastName: "automation",
        email: attendeeEmail,
      });

      await landingPageTwo.clickOnEnterNowButton();
      await landingPageTwo.isLobbyLoaded();
    });
  });
});
