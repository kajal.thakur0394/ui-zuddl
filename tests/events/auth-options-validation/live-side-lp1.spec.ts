import test, {
  APIRequestContext,
  BrowserContext,
  expect,
  Page,
} from "@playwright/test";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventAuthType from "../../../enums/eventAuthTypeEnum";
import EventEntryType from "../../../enums/eventEntryEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { LoginOptionsComponent } from "../../../page-objects/events-pages/landing-page-1/LoginOptionsComponent";
import {
  createNewEvent,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";

test.describe.parallel("@registration @socialauth", async () => {
  let eventTitle: string;
  let eventId;
  let attendeeLandingPage: string;
  let speakerLandingpPage: string;
  let eventInfoDto: EventInfoDTO;
  let attendeePage: Page;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;

  test.beforeEach(async ({ context }) => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = await orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: eventTitle,
    });
    eventInfoDto = new EventInfoDTO(eventId, eventTitle, true); // true to denote magic link enabled
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
    speakerLandingpPage = eventInfoDto.speakerLandingPage;
    // get driver for attendee
    attendeePage = await context.newPage();
  });

  test.afterEach(async () => {
    console.log("In teardown, closing the browser context");
    await orgBrowserContext?.close();
  });

  const list_auth_options = [
    {
      attendee_auth_option: [EventAuthType.GOOGLE],
      speaker_auth_option: [EventAuthType.GOOGLE],
      should_navigate_to: "https://accounts.google.com/v3/signin",
    },
    // { // LinkedIn is not enabled for now
    //   attendee_auth_option: [EventAuthType.LINKEDIN],
    //   speaker_auth_option: [EventAuthType.LINKEDIN],
    //   should_navigate_to: "https://www.linkedin.com/uas/login",
    // },
    {
      attendee_auth_option: [EventAuthType.FACEBOOK],
      speaker_auth_option: [EventAuthType.FACEBOOK],
      should_navigate_to: "https://www.facebook.com/login.php",
    },
    {
      attendee_auth_option: [EventAuthType.SSO],
      speaker_auth_option: [EventAuthType.SSO],
      should_navigate_to: "https://accounts.google.com/v3/signin",
    },
  ];
  list_auth_options.forEach((testData) => {
    test(`Verify integration of ${testData.attendee_auth_option} on attendee landing page`, async ({
      context,
    }) => {
      await test.step(`Enable only ${testData.attendee_auth_option} for the event`, async () => {
        await updateEventLandingPageDetails(orgApiContext, eventId, {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
          isGuestAuthEnabled: true,
          attendeeAuthOptions: [testData.attendee_auth_option[0]],
          speakerAuthOptions: [],
        });
      });
      await test.step(`Go to attendee landing page and verify behaviour of ${testData.attendee_auth_option} should navigate to ${testData.should_navigate_to}`, async () => {
        let landingPageOne = new LandingPageOne(attendeePage);
        await landingPageOne.load(attendeeLandingPage);
        let loginOptionsComponent: LoginOptionsComponent = await (
          await landingPageOne.getRegistrationFormComponent()
        ).clickOnAlreadyRegisteredButton();
        // wait for page to get idle

        // await attendeePage.waitForLoadState("networkidle");

        if (testData.attendee_auth_option[0] == EventAuthType.SSO) {
          //expecting an error message to display to show sso is not enabled for this domain
          await loginOptionsComponent.clickOnLoginOption(
            testData.attendee_auth_option[0]
          );
          await expect(
            landingPageOne.notificationContainer,
            "expecting sso error toast message"
          ).toBeVisible();
          const expectedErrorNotificationMessage =
            "SSO for this domain is not enabled";
          await expect(
            landingPageOne.notificationContainer,
            `expecting error notification to have message ${expectedErrorNotificationMessage}`
          ).toContainText(expectedErrorNotificationMessage, {
            ignoreCase: true,
          });
        } else {
          await expect(async () => {
            const page5Promise = context.waitForEvent("page", {
              timeout: 15000,
            });
            await loginOptionsComponent.clickOnLoginOption(
              testData.attendee_auth_option[0]
            );
            const page5 = await page5Promise;
            const url = page5.url();
            expect(url).toContain(testData.should_navigate_to);
          }, `expecting clicking on social auth icon will navigate to ${testData.should_navigate_to}`).toPass(
            { timeout: 30000 }
          );
        }
      });
    });

    test(`Verify integration of ${testData.speaker_auth_option} on speaker landing page`, async ({
      context,
    }) => {
      await test.step(`Enable only ${testData.speaker_auth_option} for the event`, async () => {
        await updateEventLandingPageDetails(orgApiContext, eventId, {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
          isGuestAuthEnabled: true,
          attendeeAuthOptions: [],
          speakerAuthOptions: [testData.speaker_auth_option[0]],
        });
      });
      await test.step(`Go to speaker landing page and verify behaviour of ${testData.attendee_auth_option} should navigate to ${testData.should_navigate_to}`, async () => {
        let landingPageOne = new LandingPageOne(attendeePage);
        await landingPageOne.load(speakerLandingpPage);
        let loginOptionsComponent: LoginOptionsComponent =
          await landingPageOne.clickOnLoginButton();

        // wait for page to get idle

        // await attendeePage.waitForLoadState("networkidle");
        if (testData.speaker_auth_option[0] == EventAuthType.SSO) {
          //expecting an error message to display to show sso is not enabled for this domain
          await loginOptionsComponent.clickOnLoginOption(
            testData.speaker_auth_option[0]
          );
          await expect(
            landingPageOne.notificationContainer,
            "expecting sso error toast message"
          ).toBeVisible();
          const expectedErrorNotificationMessage =
            "SSO for this domain is not enabled";
          await expect(
            landingPageOne.notificationContainer,
            `expecting error notification to have message ${expectedErrorNotificationMessage}`
          ).toContainText(expectedErrorNotificationMessage, {
            ignoreCase: true,
          });
        } else {
          await expect(async () => {
            const page5Promise = context.waitForEvent("page", {
              timeout: 15000,
            });
            await loginOptionsComponent.clickOnLoginOption(
              testData.speaker_auth_option[0]
            );
            const page5 = await page5Promise;
            const url = page5.url();
            expect(url).toContain(testData.should_navigate_to);
          }, `expecting clicking on social auth icon will navigate to ${testData.should_navigate_to}`).toPass(
            { timeout: 25000 }
          );
        }
      });
    });
  });
});
