import test, {
  APIRequestContext,
  BrowserContext,
  expect,
  FrameLocator,
  Page,
} from "@playwright/test";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventAuthType from "../../../enums/eventAuthTypeEnum";
import EventEntryType from "../../../enums/eventEntryEnum";
import LandingPageType from "../../../enums/landingPageEnum";
import { LandingPageTwo } from "../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
import { LoginOptionsComponent } from "../../../page-objects/events-pages/landing-page-2/LoginOptionsComponent";
import {
  createNewEvent,
  getEventDetails,
  updateEventLandingPageDetails,
  updateLandingPageType,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";

test.describe.parallel("@registration @socialauth", async () => {
  let eventTitle: string;
  let eventId;
  let attendeeLandingPage: string;
  let speakerLandingpPage: string;
  let eventInfoDto: EventInfoDTO;
  let attendeePage: Page;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;

  test.beforeEach(async ({ context }) => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});

    orgApiContext = await orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: eventTitle,
    });
    eventInfoDto = new EventInfoDTO(eventId, eventTitle, true); // true to denote magic link enabled
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
    speakerLandingpPage = eventInfoDto.speakerLandingPage;
    // Updating event landing page type
    const event_details_api_resp = await (
      await getEventDetails(orgApiContext, eventId)
    ).json();
    console.log("event_details_api_resp :", event_details_api_resp);
    let landingPageId = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landingPageId;
    await updateLandingPageType(
      orgApiContext,
      eventId,
      landingPageId,
      LandingPageType.STYLE2,
      "true",
      EventEntryType.REG_BASED
    );
    // get driver for attendee
    attendeePage = await context.newPage();
  });

  test.afterEach(async () => {
    console.log("In teardown, closing the browser context");
    await orgBrowserContext?.close();
  });

  const list_auth_options = [
    {
      attendee_auth_option: [EventAuthType.GOOGLE],
      speaker_auth_option: [EventAuthType.GOOGLE],
      should_navigate_to: "https://accounts.google.com/v3/signin/",
    },
    // { // not relevant for now
    //   attendee_auth_option: [EventAuthType.LINKEDIN],
    //   speaker_auth_option: [EventAuthType.LINKEDIN],
    //   should_navigate_to: "https://www.linkedin.com/uas/login",
    // },
    {
      attendee_auth_option: [EventAuthType.FACEBOOK],
      speaker_auth_option: [EventAuthType.FACEBOOK],
      should_navigate_to: "https://www.facebook.com/login.php",
    },
    {
      attendee_auth_option: [EventAuthType.SSO],
      speaker_auth_option: [EventAuthType.SSO],
      should_navigate_to: "https://accounts.google.com/v3/signin/",
    },
  ];
  list_auth_options.forEach((testData) => {
    test(`Verify integration of ${testData.attendee_auth_option} on attendee landing page 2`, async ({
      context,
    }) => {
      await test.step(`Enable only ${testData.attendee_auth_option} for the event`, async () => {
        await updateEventLandingPageDetails(orgApiContext, eventId, {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
          isGuestAuthEnabled: true,
          attendeeAuthOptions: [testData.attendee_auth_option[0]],
          speakerAuthOptions: [],
        });
      });
      await test.step(`Go to attendee landing page and verify behaviour of ${testData.attendee_auth_option} should navigate to ${testData.should_navigate_to}`, async () => {
        let landingPageTwo = new LandingPageTwo(attendeePage);
        await landingPageTwo.load(attendeeLandingPage);
        let loginOptionsComponent: LoginOptionsComponent = await (
          await landingPageTwo.getRegistrationFormComponent()
        ).clickOnAlreadyRegisteredButton();

        // await attendeePage.waitForLoadState("networkidle");

        if (testData.attendee_auth_option[0] == EventAuthType.SSO) {
          //expecting an error message to display to show sso is not enabled for this domain
          await loginOptionsComponent.clickOnLoginOption(
            testData.attendee_auth_option[0]
          );
          await expect(
            landingPageTwo.notificationContainer,
            "expecting sso error toast message"
          ).toBeVisible();
          const expectedErrorNotificationMessage =
            "SSO for this domain is not enabled";
          await expect(
            landingPageTwo.notificationContainer,
            `expecting error notification to have message ${expectedErrorNotificationMessage}`
          ).toContainText(expectedErrorNotificationMessage, {
            ignoreCase: true,
          });
        } else {
          await expect(async () => {
            const page5Promise = context.waitForEvent("page", {
              timeout: 15000,
            });
            await loginOptionsComponent.clickOnLoginOption(
              testData.attendee_auth_option[0]
            );
            const page5 = await page5Promise;
            const url = page5.url();
            expect(url).toContain(testData.should_navigate_to);
          }, `expecting clicking on social auth icon will navigate to ${testData.should_navigate_to}`).toPass(
            { timeout: 25000 }
          );
        }
      });
    });

    test(`Verify integration of ${testData.speaker_auth_option} on speaker landing page 2`, async ({
      context,
    }) => {
      await test.step(`Enable only ${testData.speaker_auth_option} for the event`, async () => {
        await updateEventLandingPageDetails(orgApiContext, eventId, {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
          isGuestAuthEnabled: true,
          attendeeAuthOptions: [],
          speakerAuthOptions: [testData.speaker_auth_option[0]],
        });
      });
      await test.step(`Go to speaker landing page and verify behaviour of ${testData.attendee_auth_option} should navigate to ${testData.should_navigate_to}`, async () => {
        let landingPageTwo = new LandingPageTwo(attendeePage);
        await landingPageTwo.load(speakerLandingpPage);
        let loginOptionsComponent = new LoginOptionsComponent(attendeePage);
        // await attendeePage.waitForLoadState("networkidle");
        if (testData.speaker_auth_option[0] == EventAuthType.SSO) {
          //expecting an error message to display to show sso is not enabled for this domain
          await loginOptionsComponent.clickOnLoginOption(
            testData.speaker_auth_option[0]
          );
          await attendeePage
            .locator("input[name='inputEmail']")
            .fill("anything@zuddl.com");
          await attendeePage.locator("button:has-text('Continue')").click();
          await expect(
            landingPageTwo.notificationContainer,
            "expecting sso error toast message"
          ).toBeVisible();
        } else {
          await expect(async () => {
            const page5Promise = context.waitForEvent("page", {
              timeout: 15000,
            });
            await loginOptionsComponent.clickOnLoginOption(
              testData.speaker_auth_option[0]
            );
            const page5 = await page5Promise;
            const url = page5.url();
            expect(url).toContain(testData.should_navigate_to);
          }, `expecting clicking on social auth icon will navigate to ${testData.should_navigate_to}`).toPass(
            { timeout: 25000 }
          );
        }
      });
    });
  });
});
