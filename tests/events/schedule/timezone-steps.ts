import {
  test,
  BrowserContext,
  APIRequestContext,
  Page,
} from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import BrowserName from "../../../enums/BrowserEnum";
import BrowserFactory from "../../../util/BrowserFactory";
import EventEntryType from "../../../enums/eventEntryEnum";
import { StageController } from "../../../controller/StageController";
import { EventController } from "../../../controller/EventController";
import { updateEventLandingPageDetails } from "../../../util/apiUtil";
import { getTimezoneList } from "../../../test-data/timezone-testdata";
import { StudioController } from "../../../controller/StudioController";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import { SchedulePage } from "../../../page-objects/events-pages/zones/SchedulePage";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

export class TimezoneSteps {
  readonly timezoneList = getTimezoneList();

  readonly speakerNamePrefix = "-Speaker";
  readonly attendeeNamePrefix = "-Attendee";
  readonly defaultLastName = "QA";

  speakerBrowserContextMap: Map<string, BrowserContext> = new Map();
  attendeeBrowserContextMap: Map<string, BrowserContext> = new Map();

  speakerFirstNameList: Array<string> = new Array();
  attendeeFirstNameList: Array<string> = new Array();

  eventId;
  stageId;
  studioId;

  orgBrowserContext: BrowserContext;
  orgApiContext: APIRequestContext;

  defaultScheduleUrl: string;
  eventController: EventController;

  readonly defaultTimeZone = this.timezoneList[0];
  readonly numberOfTimezones = this.timezoneList.length;

  sessionId;
  studioController: StudioController;

  speakerEmailList: Array<string> = new Array();
  speakerPageList: Page[] = new Array();
  speakerSchedulePageList: Array<SchedulePage> = new Array();

  attendeeEmailList: Array<string> = new Array();
  attendeePageList: Page[] = new Array();
  attendeeSchedulePageList: Array<SchedulePage> = new Array();

  organiserPage: Page;
  organiserStagePage: StagePage;
  organiserBackstage: StudioPage;
  organiserSchedulePage: SchedulePage;

  //Functions
  async beforeTest() {
    await test.step("Initialising organizer's browser and api contexts", async () => {
      this.orgBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: true,
        timeZoneId: this.defaultTimeZone,
      });
      this.orgApiContext = this.orgBrowserContext.request;
    });

    await test.step("Creating new event.", async () => {
      this.eventId = await EventController.generateNewEvent(
        this.orgApiContext,
        {
          event_title: DataUtl.getRandomEventTitle(),
          default_settings: false,
          add_start_date_from_today: 0,
          timezone: this.defaultTimeZone,
          add_hours_to_end_date: 5,
          add_hours_to_start_date: -5,
        }
      );
    });

    await test.step("Initialising new event controller.", async () => {
      this.eventController = new EventController(
        this.orgApiContext,
        this.eventId
      );
    });

    await test.step("Fetching deafult stage id.", async () => {
      this.stageId = await this.eventController.getDefaultStageId();
    });

    await test.step("Initialising Speaker(s) || Attendee(s) names and browser context.", async () => {
      let i = 1;
      var timeZone;
      var speakerFirstName: string;
      var attendeeFirstName: string;

      while (i <= this.numberOfTimezones) {
        timeZone = this.timezoneList[i - 1];
        // console.log(`Timezone - ${timeZone}`);

        await test.step(`${i} || ${timeZone} :- Initialising Speaker(s) || Attendee(s) names and browser context.`, async () => {
          speakerFirstName = timeZone + this.speakerNamePrefix;
          attendeeFirstName = timeZone + this.attendeeNamePrefix;

          console.log(`Speaker first name - ${speakerFirstName}`);
          console.log(`Attendee first name - ${attendeeFirstName}`);

          this.speakerFirstNameList.push(speakerFirstName);
          this.attendeeFirstNameList.push(attendeeFirstName);

          console.log(
            `Timezone - ${timeZone} || Speaker - ${speakerFirstName} || Attendee - ${attendeeFirstName}`
          );
        });

        await test.step(`${i} || ${timeZone} :- Initialising browser context.`, async () => {
          const speakerBrowserContext: BrowserContext =
            await BrowserFactory.getBrowserContext({
              browserName: BrowserName.CHROME,
              laodOrganiserCookies: false,
              timeZoneId: timeZone,
            });

          const attendeeBrowserContext: BrowserContext =
            await BrowserFactory.getBrowserContext({
              browserName: BrowserName.CHROME,
              laodOrganiserCookies: false,
              timeZoneId: timeZone,
            });

          this.speakerBrowserContextMap.set(
            speakerFirstName,
            speakerBrowserContext
          );

          this.attendeeBrowserContextMap.set(
            attendeeFirstName,
            attendeeBrowserContext
          );
        });

        i++;
      }
    });

    await test.step("Enabling Magiclink for attendee(s).", async () => {
      await updateEventLandingPageDetails(this.orgApiContext, this.eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
      });
    });

    await test.step("Initialising default schedule url.", async () => {
      this.defaultScheduleUrl = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/l/event/${this.eventId}/schedule`;
    });

    await test.step("Disabling onboarding checks for speaker(s) and attendee(s) access group.", async () => {
      await this.eventController.getAccessGroupController.handleOnboardingChecksForSpeaker(
        false
      );

      await this.eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
        false
      );
    });

    await test.step("Enabling Studio as backstage for default stage.", async () => {
      this.studioId = await this.eventController.enableStudioAsBackstage(
        this.stageId
      );
    });

    await test.step("Toggle schedule sync on.", async () => {
      await this.eventController.toggleScheduleSync(true);
    });

    await test.step(`Disabling recording for stage.`, async () => {
      const stageController = new StageController(
        this.orgApiContext,
        this.eventId,
        this.stageId
      );
      await stageController.disableRecordingForStudioAsBackstage();
    });
  }

  async afterTest() {
    await test.step("Deleting event.", async () => {
      await this.eventController.cancelAnEvent(this.eventId);
    });

    // close pages and browser contexts
    await test.step("Closing organiser's pages.", async () => {
      await this.orgBrowserContext.pages().forEach(async (page) => {
        await page.close();
      });
    });

    await test.step("Closing speaker's pages.", async () => {
      for (const page of this.speakerPageList) {
        await page.close();
      }
    });

    await test.step("Closing attendee's pages.", async () => {
      for (const page of this.attendeePageList) {
        await page.close();
      }
    });

    await test.step("Closing organizer's browser context.", async () => {
      await this.orgBrowserContext.close();
    });

    await test.step("Closing speaker browser contexts.", async () => {
      for (const browserContext of this.speakerBrowserContextMap.values()) {
        browserContext.close();
      }
    });

    await test.step("Closing attendee browser contexts.", async () => {
      for (const browserContext of this.attendeeBrowserContextMap.values()) {
        browserContext.close();
      }
    });
  }

  async createSession({
    sessionDataId = 1,
    deltaStartDate = 0,
    deltaEndDate = 0,
    deltaStartHour = -1,
    deltaEndHour = 1,
    deltaStartMinute = 0,
    deltaEndMinute = 0,
  }) {
    await test.step("Add a segment with custom time.", async () => {
      this.sessionId = await this.eventController.createSession(
        sessionDataId,
        this.stageId,
        deltaStartDate,
        deltaEndDate,
        deltaStartHour,
        deltaEndHour,
        deltaStartMinute,
        deltaEndMinute
      );
    });
  }

  async organiserBrowserContext() {
    await test.step("Organiser browser context.", async () => {
      this.organiserPage = await this.orgBrowserContext.newPage();
    });

    await test.step(`Initialize studio controller.`, async () => {
      this.studioController = new StudioController(
        this.orgApiContext,
        this.studioId,
        this.stageId
      );
    });

    await test.step(`Disable dry-run.`, async () => {
      await this.studioController.disableDryRun();
    });
  }

  async organiserJoinsBackstage() {
    await test.step(`Organizer navigates to Schedule page and joins the backstage of current session.`, async () => {
      await test.step(`Organiser click on join backstage for current session.`, async () => {
        await this.organiserPage.goto(this.defaultScheduleUrl);
        this.organiserSchedulePage = new SchedulePage(this.organiserPage);
        await this.organiserSchedulePage.clickOnSessionBackstageButton(
          this.sessionId
        );
      });

      await test.step(`Organiser click on join backstage for current session.`, async () => {
        this.organiserStagePage = new StagePage(this.organiserPage);
        await this.organiserStagePage.getAVModal.isVideoStreamLoadedOnAVModalSAB();
        await this.organiserStagePage.getAVModal.clickOnEnterStudioOnAvModal();
      });

      await test.step(`Verify Organiser's stream in backstage.`, async () => {
        this.organiserBackstage = new StudioPage(
          this.organiserPage,
          this.studioController
        );
        await this.organiserBackstage.isBackstageVisible();
        await this.organiserBackstage.verifyCountOfStreamInBackstage(1);
      });
    });
  }

  async organsierStartsSession() {
    await test.step(`Organizer selects the session.`, async () => {
      await this.organiserBackstage.selectTheSession(1);
    });

    await test.step(`Organizer initiate session.`, async () => {
      await this.organiserBackstage.startTheSession();
    });
  }

  async organsierEndsSession() {
    await test.step(`End the session.`, async () => {
      await this.organiserBackstage.endTheSession();
    });
  }

  async attendeeAndSpeakerSetup() {
    await test.step("Get random speaker emails.", async () => {
      let i = 0;
      while (i < this.numberOfTimezones) {
        await test.step(`${this.speakerFirstNameList[i]} - Random email`, async () => {
          let speakerEmail: string = DataUtl.getRandomSpeakerEmail();
          this.speakerEmailList.push(speakerEmail);
          console.log("Attendee Email", speakerEmail);
        });

        i++;
      }
    });

    await test.step("Get random attendee emails.", async () => {
      let i = 0;
      while (i < this.numberOfTimezones) {
        await test.step(`${this.attendeeFirstNameList[i]} - Random email`, async () => {
          let attendeeEmail: string = DataUtl.getRandomAttendeeEmail();
          this.attendeeEmailList.push(attendeeEmail);
          console.log("Attendee Email", attendeeEmail);
        });

        i++;
      }
    });

    await test.step("Organiser invites speakers to the event.", async () => {
      let i = 0;
      while (i < this.numberOfTimezones) {
        await test.step(`Inviting Speaker - ${this.speakerFirstNameList[i]}.`, async () => {
          await this.eventController.inviteSpeakerToTheEventByApi(
            this.speakerEmailList[i],
            this.speakerFirstNameList[i],
            this.defaultLastName
          );
        });

        i++;
      }
    });

    await test.step("Organiser invites attendees to the event.", async () => {
      let i = 0;
      while (i < this.numberOfTimezones) {
        await test.step(`Inviting Attendee - ${this.attendeeFirstNameList[i]}.`, async () => {
          await this.eventController.registerUserToEventWithRegistrationApi({
            firstName: this.attendeeFirstNameList[i],
            lastName: this.defaultLastName,
            email: this.attendeeEmailList[i],
          });
        });

        i++;
      }
    });

    await test.step("Initialize speaker pages.", async () => {
      for (var browserContext of this.speakerBrowserContextMap.values()) {
        let speakerPage: Page = await browserContext.newPage();
        this.speakerPageList.push(speakerPage);
      }
    });

    await test.step("Initialize attendee pages.", async () => {
      for (var browserContext of this.attendeeBrowserContextMap.values()) {
        let attendeePage: Page = await browserContext.newPage();
        this.attendeePageList.push(attendeePage);
      }
    });

    await test.step("Attendee/Speaker joins the event.", async () => {
      let i = 0;

      while (i < this.numberOfTimezones) {
        let attendeePage = this.attendeePageList[i];
        let speakerPage = this.speakerPageList[i];
        let attendeeSchedulePage: SchedulePage;
        let speakerSchedulePage: SchedulePage;

        await test.step(`${this.attendeeFirstNameList[i]} - Joins event`, async () => {
          const magicLink = await QueryUtil.fetchMagicLinkFromDB(
            this.eventId,
            this.attendeeEmailList[i]
          );

          await attendeePage.goto(magicLink);
          await attendeePage.click(`text=Enter Now`);
          await attendeePage.goto(this.defaultScheduleUrl);
          await attendeePage.waitForURL(this.defaultScheduleUrl);

          attendeeSchedulePage = new SchedulePage(attendeePage);
          this.attendeeSchedulePageList.push(attendeeSchedulePage);
        });

        await test.step(`${this.speakerFirstNameList[i]} - Joins event`, async () => {
          let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
            this.eventId,
            this.speakerEmailList[i]
          );

          await speakerPage.goto(magicLink);
          await speakerPage.click(`text=Enter Now`);
          await speakerPage.goto(this.defaultScheduleUrl);

          speakerSchedulePage = new SchedulePage(speakerPage);
          this.speakerSchedulePageList.push(speakerSchedulePage);
        });

        await test.step("Skip profile compreadonlyion.", async () => {
          await attendeeSchedulePage.skipProfileCompletionPopup();
          await speakerSchedulePage.skipProfileCompletionPopup();
        });

        i++;
      }
    });
  }

  async attendeeVerification({
    joinButtonVisibility = false,
    delayedStatusVisibility = false,
    completedStatusVisibility = false,
  }) {
    await test.step("Attendee Verification.", async () => {
      let i = 0;

      while (i < this.numberOfTimezones) {
        let attendeeSchedulePage = this.attendeeSchedulePageList[i];

        await test.step(`Verify ${this.attendeeFirstNameList[i]} can see "Join" button.`, async () => {
          if (joinButtonVisibility) {
            await attendeeSchedulePage.verifyJoinButtonIsVisible();
          }
          if (delayedStatusVisibility) {
            await attendeeSchedulePage.verifyDelayedStatusIsVisible();
          }
          if (completedStatusVisibility) {
            await attendeeSchedulePage.verifyCompletedButtonIsVisible();
          }
        });

        i++;
      }
    });
  }

  async speakerVerification({
    joinBackstageButtonVisibility = false,
    delayedStatusVisibility = false,
    completedStatusVisibility = false,
  }) {
    await test.step("Speaker Verification.", async () => {
      let i = 0;

      while (i < this.numberOfTimezones) {
        let speakerSchedulePage = this.speakerSchedulePageList[i];

        await test.step(`Verify ${this.speakerFirstNameList[i]} can see "Join Backstage" button.`, async () => {
          if (joinBackstageButtonVisibility) {
            await speakerSchedulePage.verifyBackstageButtonIsVisible();
          }
          if (delayedStatusVisibility) {
            await speakerSchedulePage.verifyDelayedStatusIsVisible();
          }
          if (completedStatusVisibility) {
            await speakerSchedulePage.verifyCompletedButtonIsVisible();
          }
        });

        i++;
      }
    });
  }
}
