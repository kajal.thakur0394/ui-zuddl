import { test, BrowserContext, APIRequestContext } from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import BrowserName from "../../../enums/BrowserEnum";
import { SchedulePage } from "../../../page-objects/events-pages/zones/SchedulePage";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { VenueSetup } from "../../../page-objects/new-org-side-pages/VenueSetup";

let eventId;
let stageId;
let organiserPage;
let defaultScheduleUrl: string;
let scheduleSetupPage: VenueSetup;
let orgApiContext: APIRequestContext;
let eventController: EventController;
let orgBrowserContext: BrowserContext;
let organiserSchedulePage: SchedulePage;
let testSessionName: string = "Test Session";
let csvFileName: string = "CreateSession.csv";

test.describe.parallel("@createSessionByCSV", async () => {
  test.describe.configure({ retries: 2 });

  test.beforeEach(async () => {
    await test.step("Initialising organizer's browser and api contexts", async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: true,
      });
      orgApiContext = orgBrowserContext.request;
    });

    await test.step("Creating new event.", async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
        default_settings: false,
        add_start_date_from_today: 0,
        add_end_date_from_today: 1,
        add_hours_to_start_date: -5,
      });
    });

    await test.step("Initialising new event controller.", async () => {
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step("Fetching deafult stage id.", async () => {
      stageId = await eventController.getDefaultStageId();
    });

    await test.step("Initialising default schedule url.", async () => {
      defaultScheduleUrl = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/l/event/${eventId}/schedule`;
    });

    await test.step("Generate CSV for creaating a session.", async () => {
      await eventController.GenerateCSV(csvFileName, testSessionName);
    });
  });

  test.afterEach(async () => {
    await test.step("Deleting event.", async () => {
      await eventController.cancelAnEvent(eventId);
    });

    await test.step("Closing organizer's browser context.", async () => {
      await orgBrowserContext.close();
    });
  });

  test("TC001: Create a session by uploading CSV file.", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-528",
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step("Organiser upload CSV file to create a session.", async () => {
      scheduleSetupPage = new VenueSetup(organiserPage);
      await scheduleSetupPage.navigateToVenueSetup("schedule", eventId);
      await scheduleSetupPage.uploadScheduleCSVFile(csvFileName);
    });

    await test.step(`Organizer joins the event and go to schedule page.`, async () => {
      await organiserPage.goto(defaultScheduleUrl);
    });

    await test.step(`Verify session is visible to Organiser.`, async () => {
      organiserSchedulePage = new SchedulePage(organiserPage);
      await organiserSchedulePage.verifySessionPresenceByName(testSessionName);
    });
  });
});
