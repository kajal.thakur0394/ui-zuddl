import { test, BrowserContext, APIRequestContext } from "@playwright/test";
import { EventController } from "../../../../controller/EventController";
import BrowserName from "../../../../enums/BrowserEnum";
import EventEntryType from "../../../../enums/eventEntryEnum";
import { SchedulePage } from "../../../../page-objects/events-pages/zones/SchedulePage";
import BrowserFactory from "../../../../util/BrowserFactory";
import { updateEventLandingPageDetails } from "../../../../util/apiUtil";
import { DataUtl } from "../../../../util/dataUtil";
import { StudioController } from "../../../../controller/StudioController";
import { StagePage } from "../../../../page-objects/events-pages/zones/StagePage";
import { StudioPage } from "../../../../page-objects/studio-pages/StudioPage";
import { StageController } from "../../../../controller/StageController";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

let orgBrowserContext: BrowserContext;
let orgApiContext: APIRequestContext;
let eventId;
let eventController: EventController;
let stageId;
let attendeeOneBrowserContext: BrowserContext;
let speakerOneBrowserContext: BrowserContext;
let defaultScheduleUrl: string;
let studioId;
let sessionId;
let studioController: StudioController;
let organiserPage;
let organiserSchedulePage: SchedulePage;
let organiserStagePage: StagePage;
let organiserBackstage: StudioPage;
let speakerOnePage;
let speakerOneSchedulePage: SchedulePage;

test.describe
  .parallel("Schedule Sync On, Studio Backstage | @scheduleSyncOn @studioAsBackstage", async () => {
  // test.describe.configure({ retries: 2 });

  test.beforeEach(async () => {
    await test.step("Initialising organizer's browser and api contexts", async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: true,
      });
      orgApiContext = orgBrowserContext.request;
    });

    await test.step("Creating new event.", async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
        default_settings: false,
        add_start_date_from_today: 0,
        add_end_date_from_today: 1,
        add_hours_to_start_date: -5,
      });
    });

    await test.step("Initialising new event controller.", async () => {
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step("Fetching deafult stage id.", async () => {
      stageId = await eventController.getDefaultStageId();
    });

    await test.step("Initialising new attendee", async () => {
      attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });
    });

    await test.step("Initialising new speaker context", async () => {
      speakerOneBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });
    });

    await test.step("Enabling Magiclink for attendee.", async () => {
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
      });
    });

    await test.step("Initialising default schedule url.", async () => {
      defaultScheduleUrl = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/l/event/${eventId}/schedule`;
    });

    await test.step("Disabling onboarding checks for speaker and attendees access group.", async () => {
      await eventController.getAccessGroupController.handleOnboardingChecksForSpeaker(
        false
      );
      await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
        false
      );
    });

    await test.step("Enabling Studio as backstage for default stage.", async () => {
      studioId = await eventController.enableStudioAsBackstage(stageId);
    });

    await test.step("Toggle schedule sync on.", async () => {
      await eventController.toggleScheduleSync(true);
    });

    await test.step(`Disabling recording for this stage.`, async () => {
      let stageController = new StageController(
        orgApiContext,
        eventId,
        stageId
      );
      await stageController.disableRecordingForStudioAsBackstage();
    });
  });

  test.afterEach(async () => {
    await test.step("Deleting event.", async () => {
      await eventController.cancelAnEvent(eventId);
    });

    await test.step("Closing organizer's browser context.", async () => {
      await orgBrowserContext.close();
    });

    await test.step("Closing attendee's browser context.", async () => {
      await attendeeOneBrowserContext.close();
    });
  });

  // Redundant test cases skipped
  test.skip("TC001 : Attendee see Delayed Status if current time is greater than session end time and session is not started by organizer", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-562/attendee-see-delayed-status-if-current-time-is-greater-than-session",
    });

    const attendeeOneFirstName = "JxtinAttendeeOne";
    const attendeeOneLastName = "QA";
    let attendeeOneEmail;
    let attendeeOnePage;
    let attendeeOneSchedulePage: SchedulePage;
    let sessionId;

    await test.step("Add a segment with schedule end time and start time before current time.", async () => {
      sessionId = await eventController.createSession(1, stageId, 0, 0, -2, -1);
    });

    await test.step("Get random attendee emails.", async () => {
      attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
      console.log("attendeeOneEmail", attendeeOneEmail);
    });

    await test.step("Organiser invites attendee to the event", async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        email: attendeeOneEmail,
      });
    });

    await test.step("Attendees browser context.", async () => {
      attendeeOnePage = await attendeeOneBrowserContext.newPage();
    });

    await test.step("Attendee joins the event and enter default stage", async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeOneEmail
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click("text=Enter Now");
      await attendeeOnePage.goto(defaultScheduleUrl);
      attendeeOneSchedulePage = new SchedulePage(attendeeOnePage);
    });

    await test.step("Verify delayed status is visible for the session.", async () => {
      await attendeeOneSchedulePage.verifyDelayedStatusIsVisible();
    });

    await test.step("Deleting the session created.", async () => {
      await eventController.deleteSession(sessionId);
    });
  });

  test.skip("TC002 : Speaker see Delayed and Join Backstage button if current time is greater than session end time and session is not started by organizer", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-563/speaker-see-delayed-and-join-backstage-button-if-current-time-is",
    });
    const speakerOneFirstName = "JxtinSpeakerOne";
    const speakerOneLastName = "QA";
    let speakerOneEmail;
    let speakerOnePage;
    let speakerOneSchedulePage: SchedulePage;
    let studioController: StudioController;
    let sessionId;

    await test.step("Add a segment with schedule end time and start time before current time.", async () => {
      sessionId = await eventController.createSession(1, stageId, 0, 0, -2, -1);
    });

    studioController = new StudioController(orgApiContext, studioId, stageId);
    // no need to use this when the case is where session is not started

    await test.step("Get random speaker emails.", async () => {
      speakerOneEmail = DataUtl.getRandomSpeakerEmail();
      console.log("speakerOneEmail", speakerOneEmail);
    });

    await test.step("Organiser invites speaker to the event", async () => {
      await eventController.inviteSpeakerToTheEventByApi(
        speakerOneEmail,
        speakerOneFirstName,
        speakerOneLastName
      );
    });

    await test.step("Speakers browser context.", async () => {
      speakerOnePage = await attendeeOneBrowserContext.newPage();
    });

    await test.step("Speaker joins the event and enter default stage", async () => {
      let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerOneEmail
      );

      await speakerOnePage.goto(magicLink);
      await speakerOnePage.click(`text=Enter Now`);
      await speakerOnePage.goto(defaultScheduleUrl);
      await speakerOnePage.waitForURL(defaultScheduleUrl);

      speakerOneSchedulePage = new SchedulePage(speakerOnePage);
    });

    await test.step("Verify delayed status is visible for the session.", async () => {
      await speakerOneSchedulePage.verifyDelayedStatusIsVisible();
    });

    await test.step("Verify Join Backstage button is visible for the session.", async () => {
      await speakerOneSchedulePage.verifyBackstageButtonIsVisible();
    });
  });

  test("TC003 : Attendee see Join button if current time is between session start and end time and and session is started", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-564/attendee-see-join-button-if-current-time-is-between-session-start-and",
    });

    const attendeeOneFirstName = "TestAttendeeOne";
    const attendeeOneLastName = "QA";

    let attendeeOnePage;
    let attendeeOneEmail;
    let studioController: StudioController;
    let attendeeOneSchedulePage: SchedulePage;

    let sessionId;
    let organiserPage;
    let organiserStagePage: StagePage;
    let organiserBackstage: StudioPage;
    let organiserSchedulePage: SchedulePage;

    await test.step("Add a segment with schedule start before current time and end time after current time.", async () => {
      sessionId = await eventController.createSession(1, stageId, 0, 0, -1, 1);
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step(`Initialize studio controller.`, async () => {
      studioController = new StudioController(orgApiContext, studioId, stageId);
    });

    await test.step(`Disable dry-run.`, async () => {
      await studioController.disableDryRun();
    });

    await test.step(`Organizer joins the event and go to schedule page.`, async () => {
      await organiserPage.goto(defaultScheduleUrl);
    });

    await test.step(`Organizer joins the backstage of current session.`, async () => {
      await test.step(`Organiser click on join backstage for current session.`, async () => {
        organiserSchedulePage = new SchedulePage(organiserPage);
        await organiserSchedulePage.clickOnSessionBackstageButton(sessionId);
      });

      await test.step(`Organiser click on join backstage for current session.`, async () => {
        organiserStagePage = new StagePage(organiserPage);
        await organiserStagePage.getAVModal.isVideoStreamLoadedOnAVModalSAB();
        await organiserStagePage.getAVModal.clickOnEnterStudioOnAvModal();
      });

      await test.step(`Verify Organiser's stream in backstage.`, async () => {
        organiserBackstage = new StudioPage(organiserPage, studioController);
        await organiserBackstage.isBackstageVisible();
        await organiserBackstage.verifyCountOfStreamInBackstage(1);
      });
    });

    await test.step(`Organizer selects the session.`, async () => {
      await organiserBackstage.selectTheSession(1);
    });

    await test.step(`Organizer initiate session.`, async () => {
      await organiserBackstage.startTheSession();
    });

    await test.step("Get random attendee emails.", async () => {
      attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
      console.log("Attendee Email", attendeeOneEmail);
    });

    await test.step("Organiser invites attendee to the event.", async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        email: attendeeOneEmail,
      });
    });

    await test.step("Initialize attendee's browser context.", async () => {
      attendeeOnePage = await attendeeOneBrowserContext.newPage();
    });

    await test.step("Attendee joins the event and enter default stage.", async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeOneEmail
      );

      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.goto(defaultScheduleUrl);
      await attendeeOnePage.waitForURL(defaultScheduleUrl);

      attendeeOneSchedulePage = new SchedulePage(attendeeOnePage);
    });

    await test.step("Skip profile completion.", async () => {
      await attendeeOneSchedulePage.skipProfileCompletionPopup();
    });

    await test.step("Verify Join button is visible to Attendee.", async () => {
      await attendeeOneSchedulePage.verifyJoinButtonIsVisible();
    });

    await test.step(`End the session.`, async () => {
      await organiserBackstage.endTheSession();
    });
  });

  test("TC004 : Attendee see join button if current time is greater than session end time and organizer has not ended the session", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-565/attendee-see-join-button-if-current-time-is-greater-than-session-end",
    });

    const attendeeOneFirstName = "TestAttendeeOne";
    const attendeeOneLastName = "QA";

    let attendeeOnePage;
    let attendeeOneEmail;
    let studioController: StudioController;
    let attendeeOneSchedulePage: SchedulePage;

    let sessionId;
    let organiserPage;
    let organiserStagePage: StagePage;
    let organiserBackstage: StudioPage;
    let organiserSchedulePage: SchedulePage;

    await test.step("Add a segment with schedule start before current time and schedule end as current time.", async () => {
      sessionId = await eventController.createSession(
        1,
        stageId,
        0,
        0,
        -1,
        0,
        0,
        0
      );
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step(`Initialize studio controller.`, async () => {
      studioController = new StudioController(orgApiContext, studioId, stageId);
    });

    await test.step(`Disable dry-run.`, async () => {
      await studioController.disableDryRun();
    });

    await test.step(`Organizer joins the event and go to schedule page.`, async () => {
      await organiserPage.goto(defaultScheduleUrl);
    });

    await test.step(`Organizer joins the backstage of current session.`, async () => {
      await test.step(`Organiser click on join backstage for current session.`, async () => {
        organiserSchedulePage = new SchedulePage(organiserPage);
        await organiserSchedulePage.clickOnSessionBackstageButton(sessionId);
      });

      await test.step(`Organiser click on join backstage for current session.`, async () => {
        organiserStagePage = new StagePage(organiserPage);
        await organiserStagePage.getAVModal.isVideoStreamLoadedOnAVModalSAB();
        await organiserStagePage.getAVModal.clickOnEnterStudioOnAvModal();
      });

      await test.step(`Verify Organiser's stream in backstage.`, async () => {
        organiserBackstage = new StudioPage(organiserPage, studioController);
        await organiserBackstage.isBackstageVisible();
        await organiserBackstage.verifyCountOfStreamInBackstage(1);
      });
    });

    await test.step(`Organizer selects the session.`, async () => {
      await organiserBackstage.selectTheSession(1);
    });

    await test.step(`Organizer initiate session.`, async () => {
      await organiserBackstage.startTheSession();
    });

    await test.step("Get random attendee emails.", async () => {
      attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
      console.log("Attendee Email", attendeeOneEmail);
    });

    await test.step("Organiser invites attendee to the event.", async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        email: attendeeOneEmail,
      });
    });

    await test.step("Initialize attendee's browser context.", async () => {
      attendeeOnePage = await attendeeOneBrowserContext.newPage();
    });

    await test.step("Attendee joins the event and enter default stage.", async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeOneEmail
      );

      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.goto(defaultScheduleUrl);
      await attendeeOnePage.waitForURL(defaultScheduleUrl);

      attendeeOneSchedulePage = new SchedulePage(attendeeOnePage);
    });

    await test.step("Skip profile completion.", async () => {
      await attendeeOneSchedulePage.skipProfileCompletionPopup();
    });

    await test.step("Verify Join button is visible to Attendee.", async () => {
      await attendeeOneSchedulePage.verifyJoinButtonIsVisible();
    });

    await test.step(`End the session.`, async () => {
      await organiserBackstage.endTheSession();
    });
  });

  test("TC005 : Speaker see Join Backstage and Completed button if current time is greater than session end time and session is completed", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-567/speaker-see-join-backstage-and-completed-button-if-current-time-is",
    });

    let speakerOneEmail;
    let speakerOneFirstName = "pramila";
    let speakerOneLastName = "speakerOne";

    await test.step("Add a segment with schedule start time before current time and end time as current time.", async () => {
      sessionId = await eventController.createSession(
        1,
        stageId,
        0,
        0,
        -1,
        0,
        0,
        0
      );
    });

    await test.step(`Initialize studio controller.`, async () => {
      studioController = new StudioController(orgApiContext, studioId, stageId);
    });

    await test.step(`Disable dry-run.`, async () => {
      await studioController.disableDryRun();
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step(`Organizer joins the event and go to schedule page.`, async () => {
      await organiserPage.goto(defaultScheduleUrl);
    });

    await test.step(`Organizer joins the backstage of current session.`, async () => {
      await test.step(`Organiser click on join backstage for current session.`, async () => {
        organiserSchedulePage = new SchedulePage(organiserPage);
        await organiserSchedulePage.clickOnSessionBackstageButton(sessionId);
      });

      await test.step(`Organiser click on join backstage for current session.`, async () => {
        organiserStagePage = new StagePage(organiserPage);
        await organiserStagePage.getAVModal.isVideoStreamLoadedOnAVModalSAB();
        await organiserStagePage.getAVModal.clickOnEnterStudioOnAvModal();
      });

      await test.step(`Verify Organiser's stream in backstage.`, async () => {
        organiserBackstage = new StudioPage(organiserPage, studioController);
        await organiserBackstage.isBackstageVisible();
        await organiserBackstage.verifyCountOfStreamInBackstage(1);
      });
    });

    await test.step(`Organizer selects the session.`, async () => {
      await organiserBackstage.selectTheSession(1);
    });

    await test.step(`Organizer initiate session.`, async () => {
      await organiserBackstage.startTheSession();
    });

    await test.step(`End the session.`, async () => {
      await organiserBackstage.endTheSession();
    });

    await test.step(`speaker join the stage and see Join Backstage and Completed status`, async () => {
      await test.step("Get random speaker emails.", async () => {
        speakerOneEmail = DataUtl.getRandomSpeakerEmail();
      });
      await test.step("Organiser invites speaker to the event", async () => {
        await eventController.inviteSpeakerToTheEventByApi(
          speakerOneEmail,
          speakerOneFirstName,
          speakerOneLastName
        );
      });
      await test.step("Speakers browser context.", async () => {
        speakerOnePage = await speakerOneBrowserContext.newPage();
      });
      await test.step("Speaker joins the event and enter default stage", async () => {
        let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          eventId,
          speakerOneEmail
        );
        await speakerOnePage.goto(magicLink);
        await speakerOnePage.click(`text=Enter Now`);
        await speakerOnePage.goto(defaultScheduleUrl);
        await speakerOnePage.waitForURL(defaultScheduleUrl);
        speakerOneSchedulePage = new SchedulePage(speakerOnePage);
      });

      await test.step("Verify Join Backstage button is visible for the session.", async () => {
        await speakerOneSchedulePage.verifyBackstageButtonIsVisible();
      });

      await test.step("Verify Compeleted button is visible for the session,", async () => {
        await speakerOneSchedulePage.verifyCompletedButtonIsVisible();
      });
    });
  });

  test("TC006: Speaker see only Join Backstage button if session is Ongoing.", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-566/speaker-see-only-join-backstage-button-if-session-is-ongoing",
    });
    let speakerOneEmail;
    let speakerOneFirstName = "pramilaOneSpeaker";
    let speakerOneLastName = "OneSpeaker";

    await test.step("Add a ongoing segment", async () => {
      sessionId = await eventController.createSession(
        1,
        stageId,
        0,
        0,
        -1,
        1,
        0,
        0
      );
    });
    await test.step(`Initialize studio controller.`, async () => {
      studioController = new StudioController(orgApiContext, studioId, stageId);
    });

    await test.step(`Disable dry-run.`, async () => {
      await studioController.disableDryRun();
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step(`Organizer joins the event and go to schedule page.`, async () => {
      await organiserPage.goto(defaultScheduleUrl);
    });

    await test.step(`Organizer joins the backstage of current session.`, async () => {
      await test.step(`Organiser click on join backstage for current session.`, async () => {
        organiserSchedulePage = new SchedulePage(organiserPage);
        await organiserSchedulePage.clickOnSessionBackstageButton(sessionId);
      });

      await test.step(`Organiser click on join backstage for current session.`, async () => {
        organiserStagePage = new StagePage(organiserPage);
        await organiserStagePage.getAVModal.isVideoStreamLoadedOnAVModalSAB();
        await organiserStagePage.getAVModal.clickOnEnterStudioOnAvModal();
      });

      await test.step(`Verify Organiser's stream in backstage.`, async () => {
        organiserBackstage = new StudioPage(organiserPage, studioController);
        await organiserBackstage.isBackstageVisible();
        await organiserBackstage.verifyCountOfStreamInBackstage(1);
      });
    });

    await test.step(`Organizer selects the session.`, async () => {
      await organiserBackstage.selectTheSession(1);
    });

    await test.step(`Organizer initiate session.`, async () => {
      await organiserBackstage.startTheSession();
    });

    await test.step("Get random speaker emails.", async () => {
      speakerOneEmail = DataUtl.getRandomSpeakerEmail();
      console.log("speakerOneEmail", speakerOneEmail);
    });

    await test.step("Organiser invites speaker to the event", async () => {
      await eventController.inviteSpeakerToTheEventByApi(
        speakerOneEmail,
        speakerOneFirstName,
        speakerOneLastName
      );
    });

    await test.step("Speakers browser context.", async () => {
      speakerOnePage = await speakerOneBrowserContext.newPage();
    });

    await test.step("Speaker joins the event and enter default stage", async () => {
      let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerOneEmail
      );

      await speakerOnePage.goto(magicLink);
      await speakerOnePage.click(`text=Enter Now`);
      await speakerOnePage.goto(defaultScheduleUrl);
      await speakerOnePage.waitForURL(defaultScheduleUrl);

      speakerOneSchedulePage = new SchedulePage(speakerOnePage);
    });

    await test.step("Verify Join Backstage is visible to the speaker.", async () => {
      await speakerOneSchedulePage.verifyBackstageButtonIsVisible();
    });
  });

  test("TC007 : Org can start session which is completed and attendee see Join tab and speaker see Join Backstage and Completed status", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-572/org-can-start-session-which-is-completed-and-attendee-see-join-tab-and",
    });

    const attendeeOneFirstName = "TestAttendeeOne";
    const attendeeOneLastName = "QA";

    let attendeeOnePage;
    let attendeeOneEmail;
    let studioController: StudioController;
    let attendeeOneSchedulePage: SchedulePage;

    let sessionId;
    let organiserPage;
    let organiserStagePage: StagePage;
    let organiserBackstage: StudioPage;
    let organiserSchedulePage: SchedulePage;

    let speakerOneFirstName = "TestSpeakerOne";
    let speakerOneLastName = "QA";
    let speakerOneEmail;
    let speakerOnePage;
    let speakerOneBrowserContext;
    let speakerOneSchedulePage: SchedulePage;

    await test.step("Get random speaker emails.", async () => {
      speakerOneEmail = DataUtl.getRandomSpeakerEmail();
      console.log("Speaker Email", speakerOneEmail);
    });

    await test.step("Get random attendee email.", async () => {
      attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
      console.log("Attendee Email", attendeeOneEmail);
    });

    await test.step("Initialising new speaker", async () => {
      speakerOneBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });
    });

    await test.step("Initialize speaker's browser context.", async () => {
      speakerOnePage = await speakerOneBrowserContext.newPage();
    });

    await test.step("Initialize attendee's browser context.", async () => {
      attendeeOnePage = await attendeeOneBrowserContext.newPage();
    });

    await test.step("Add a segment with schedule start time before current time and end time after 1 minute.", async () => {
      sessionId = await eventController.createSession(
        1,
        stageId,
        0,
        0,
        -5,
        0,
        0,
        1
      );
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step(`Initialize studio controller.`, async () => {
      studioController = new StudioController(orgApiContext, studioId, stageId);
    });

    await test.step(`Disable dry-run.`, async () => {
      await studioController.disableDryRun();
    });

    await test.step(`Organizer joins the event and go to schedule page.`, async () => {
      await organiserPage.goto(defaultScheduleUrl);
    });

    await test.step(`Organizer joins the backstage of current session.`, async () => {
      await test.step(`Organiser click on join backstage for current session.`, async () => {
        organiserSchedulePage = new SchedulePage(organiserPage);
        await organiserSchedulePage.clickOnSessionBackstageButton(sessionId);
      });

      await test.step(`Organiser click on join backstage for current session.`, async () => {
        organiserStagePage = new StagePage(organiserPage);
        await organiserStagePage.getAVModal.isVideoStreamLoadedOnAVModalSAB();
        await organiserStagePage.getAVModal.clickOnEnterStudioOnAvModal();
      });

      await test.step(`Verify Organiser's stream in backstage.`, async () => {
        organiserBackstage = new StudioPage(organiserPage, studioController);
        await organiserBackstage.isBackstageVisible();
        await organiserBackstage.verifyCountOfStreamInBackstage(1);
      });
    });

    await test.step(`Organizer selects the session.`, async () => {
      await organiserBackstage.selectTheSession(1);
    });

    await test.step(`Organizer initiate session.`, async () => {
      await organiserBackstage.startTheSession();
    });

    await test.step("Organiser invites speaker to the event.", async () => {
      await eventController.inviteSpeakerToTheEventByApi(
        speakerOneEmail,
        speakerOneFirstName,
        speakerOneLastName
      );
    });

    await test.step("Organiser invites attendee to the event.", async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        email: attendeeOneEmail,
      });
    });

    await test.step("Attendee joins the event and enter default stage.", async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeOneEmail
      );

      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.goto(defaultScheduleUrl);
      await attendeeOnePage.waitForURL(defaultScheduleUrl);

      attendeeOneSchedulePage = new SchedulePage(attendeeOnePage);
    });

    await test.step("Speaker joins the event and enter default stage.", async () => {
      let speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerOneEmail
      );

      await speakerOnePage.goto(speakerMagicLink);
      await speakerOnePage.click(`text=Enter Now`);
      await speakerOnePage.goto(defaultScheduleUrl);
      await speakerOnePage.waitForURL(defaultScheduleUrl);

      speakerOneSchedulePage = new SchedulePage(speakerOnePage);
    });

    await test.step("Skip profile completion for Attendee.", async () => {
      await attendeeOneSchedulePage.skipProfileCompletionPopup();
    });

    await test.step("Skip profile completion Speaker.", async () => {
      await speakerOneSchedulePage.skipProfileCompletionPopup();
    });

    await test.step(`Organiser ends the session.`, async () => {
      await organiserBackstage.endTheSession();
    });

    await test.step(`Organizer joins the event and go to schedule page.`, async () => {
      organiserPage = await orgBrowserContext.newPage();
      await organiserPage.goto(defaultScheduleUrl);
    });

    await test.step(`Verify organiser see completed status on schedule page.`, async () => {
      organiserSchedulePage = new SchedulePage(organiserPage);
      await organiserSchedulePage.verifyCompletedButtonIsVisible();
    });

    await test.step(`Close OrganiserSchedulePage.`, async () => {
      await organiserSchedulePage.page.close();
    });

    await test.step(`Organizer selects the session.`, async () => {
      await organiserBackstage.selectTheSession(1);
    });

    await test.step(`Organizer initiate session.`, async () => {
      await organiserBackstage.startTheSession();
    });

    await test.step("Verify attendee is able to see join button.", async () => {
      await attendeeOneSchedulePage.verifyJoinButtonIsVisible();
    });

    await test.step("Verify speaker see completed status and join backstage button.", async () => {
      await speakerOneSchedulePage.verifyBackstageButtonIsVisible();
    });

    await test.step(`End the session.`, async () => {
      await organiserBackstage.endTheSession();
    });

    await test.step(`Close OrganiserBackstage.`, async () => {
      await organiserBackstage.page.close();
    });

    await test.step("Close speaker browser context.", async () => {
      await speakerOneBrowserContext.close();
    });
  });
});
