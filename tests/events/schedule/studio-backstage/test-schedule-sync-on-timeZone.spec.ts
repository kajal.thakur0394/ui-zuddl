import { test } from "@playwright/test";
import { TimezoneSteps } from "../timezone-steps";

test.describe("Schedule-Sync On | @timezone @scheduleSyncOn", async () => {
  test.slow();
  test.describe.configure({ retries: 2 });

  let timezoneSteps: TimezoneSteps;

  test.beforeEach(async () => {
    timezoneSteps = new TimezoneSteps();
    await timezoneSteps.beforeTest();
  });

  test.afterEach(async () => {
    await timezoneSteps.afterTest();
  });

  test(`TC001 : Event is created in different time zone with condition - sessionEndTime > currentTime and Organiser starts the session.`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-581",
    });

    await timezoneSteps.createSession({
      deltaStartHour: -1,
      deltaEndHour: 1,
    });

    await timezoneSteps.organiserBrowserContext();
    await timezoneSteps.organiserJoinsBackstage();
    await timezoneSteps.organsierStartsSession();
    await timezoneSteps.attendeeAndSpeakerSetup();
    await timezoneSteps.attendeeVerification({
      joinButtonVisibility: true,
      delayedStatusVisibility: false,
      completedStatusVisibility: false,
    });
    await timezoneSteps.speakerVerification({
      joinBackstageButtonVisibility: true,
      delayedStatusVisibility: false,
      completedStatusVisibility: false,
    });
    await timezoneSteps.organsierEndsSession();
  });

  test(`TC002 : Event is created in different time zone with condition - sessionEndTime < currentTime.`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-582/",
    });

    await timezoneSteps.createSession({
      deltaStartHour: -2,
      deltaEndHour: -1,
    });

    await timezoneSteps.organiserBrowserContext();
    await timezoneSteps.organiserJoinsBackstage();
    await timezoneSteps.attendeeAndSpeakerSetup();
    await timezoneSteps.attendeeVerification({
      joinButtonVisibility: false,
      delayedStatusVisibility: false, // Delayed status is not visible anymore
      completedStatusVisibility: false,
    });
    await timezoneSteps.speakerVerification({
      joinBackstageButtonVisibility: true,
      delayedStatusVisibility: false, // Delayed status is not visible anymore
      completedStatusVisibility: false,
    });
  });

  test(`TC003 : Event is created in different time zone with condition - sessionEndTime < currentTime and Organiser starts a completed session.`, async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-583/",
    });

    await timezoneSteps.createSession({
      deltaStartHour: -1,
      deltaEndHour: 0,
      deltaEndMinute: 1,
    });

    await timezoneSteps.organiserBrowserContext();
    await timezoneSteps.organiserJoinsBackstage();
    await timezoneSteps.organsierStartsSession();
    await timezoneSteps.organsierEndsSession();
    await timezoneSteps.attendeeAndSpeakerSetup();

    await timezoneSteps.attendeeVerification({
      joinButtonVisibility: false,
      delayedStatusVisibility: false,
      completedStatusVisibility: true,
    });
    await timezoneSteps.speakerVerification({
      joinBackstageButtonVisibility: true,
      delayedStatusVisibility: false,
      completedStatusVisibility: true,
    });

    await timezoneSteps.organsierStartsSession();

    await timezoneSteps.attendeeVerification({
      joinButtonVisibility: true,
      delayedStatusVisibility: false,
      completedStatusVisibility: false,
    });
    await timezoneSteps.speakerVerification({
      joinBackstageButtonVisibility: true,
      delayedStatusVisibility: false,
      completedStatusVisibility: false,
    });

    await timezoneSteps.organsierEndsSession();
  });
});
