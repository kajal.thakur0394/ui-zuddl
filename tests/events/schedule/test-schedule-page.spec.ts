import {
  test,
  BrowserContext,
  APIRequestContext,
  expect,
  Page,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import BrowserName from "../../../enums/BrowserEnum";
import EventEntryType from "../../../enums/eventEntryEnum";
import { SchedulePage } from "../../../page-objects/events-pages/zones/SchedulePage";
import BrowserFactory from "../../../util/BrowserFactory";
import { updateEventLandingPageDetails } from "../../../util/apiUtil";
import { DataUtl } from "../../../util/dataUtil";
import sessionDataPayload from "../../../test-data/sessionDataPayload.json";
import { RoomsController } from "../../../controller/RoomsController";
import { ScheduleController } from "../../../controller/ScheduleController";
import { getListOfSpeakersInEvent } from "../../../util/apiUtil";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { StudioController } from "../../../controller/StudioController";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

const venues = [
  { venue: "STAGE", ticketId: 588 },
  { venue: "ROOMS", ticketId: 589 },
  { venue: "LOBBY", ticketId: 590 },
  { venue: "EXPO", ticketId: 591 },
  { venue: "NETWORKING", ticketId: 592 },
  { venue: "IN_PERSON", ticketId: 593 },
  { venue: "NONE", ticketId: 594 },
  { venue: "SCHEDULE", ticketId: 595 },
];

async function createSegment(
  apiContext: APIRequestContext,
  eventId: string,
  defaultStageId: string,
  segmentName: string,
  venue: string,
  eventController: EventController,
  speakers: {
    speakerEmail: string;
    firstName: string;
    lastName: string;
    speakerId: string;
  }[] = [],
  tags = []
): Promise<[string, any]> {
  // choose random value in length of sessionDataPayload using random index
  const randomIndex = Math.floor(
    Math.random() * Object.keys(sessionDataPayload).length
  );
  const sessionData = sessionDataPayload[randomIndex];
  const sessionId = await eventController.createSession(
    randomIndex,
    defaultStageId,
    0,
    0,
    -1,
    1,
    0,
    0,
    venue,
    speakers,
    tags
  );

  const recievedSessionData = await eventController.getRequiredInfo(
    eventId,
    sessionId
  );

  sessionData["start_time"] = recievedSessionData["startDateTime"];
  sessionData["end_time"] = recievedSessionData["endDateTime"];
  sessionData["title"] = recievedSessionData["title"];
  sessionData["description"] = recievedSessionData["description"];
  console.log(sessionData);

  return [sessionId, sessionData];
}

async function loginToEvent(
  page: Page,
  magicLink: string,
  defaultScheduleUrl: string
) {
  await page.goto(magicLink);
  await page.click(`text=Enter Now`);
  await page.goto(defaultScheduleUrl);
  await page.waitForURL(defaultScheduleUrl);
}

test.describe
  .parallel("Schedule tab on live event site | @schedulePage", () => {
  test.describe.configure({ retries: 2 });

  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId;
  let stageId;
  let eventController: EventController;
  let attendeeOneBrowserContext: BrowserContext;
  let defaultScheduleUrl: string;
  let eventName: string;
  let attendeeOneFirstName = "Attendee";
  let attendeeOneLastName = `JxtinOne`;
  let attendeeOneEmail;
  let attendeeOnePage;
  let attendeeOneSchedulePage: SchedulePage;
  let sessionData;
  let sessionId;

  test.beforeEach(async () => {
    attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
    await test.step("Initialising organizer's browser and api contexts", async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: true,
      });

      orgApiContext = orgBrowserContext.request;
    });

    eventName = DataUtl.getRandomEventTitle();

    await test.step("Creating new live event.", async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: eventName,
        default_settings: false,
        add_start_date_from_today: 0,
        add_end_date_from_today: 1,
        add_hours_to_start_date: -2,
      });
    });

    await test.step("Initialising new event controller.", async () => {
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step("Fetching deafult stage id.", async () => {
      stageId = await eventController.getDefaultStageId();
    });

    await test.step("Initialising new attendee", async () => {
      attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });
    });

    await test.step("Enabling Magiclink for attendee.", async () => {
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
      });
    });

    await test.step("Initialising default schedule url.", async () => {
      defaultScheduleUrl = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/l/event/${eventId}/schedule`;
    });

    await test.step("Disabling onboarding checks for speaker and attendees access group.", async () => {
      await eventController.getAccessGroupController.handleOnboardingChecksForSpeaker(
        false
      );
      await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
        false
      );
    });

    await test.step("Toggle schedule sync off.", async () => {
      await eventController.toggleScheduleSync(false);
    });

    await test.step("Fetching default stage id.", async () => {
      stageId = await eventController.getDefaultStageId();
    });
  });

  test.afterEach(async () => {
    await test.step("Deleting event.", async () => {
      await eventController.cancelAnEvent(eventId);
    });

    await orgBrowserContext?.close();
    await attendeeOneBrowserContext?.close();
  });

  test("TC001 : Verify empty state is visible on schedule tab if there are no segments created for any of the event venues. | @emptySchedule", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-530/verify-empty-state-is-visible-on-schedule-tab-if-there-are-no-segments",
    });

    let attendeeOneEmail = DataUtl.getRandomAttendeeEmail();

    let attendeeOneSchedulePage: SchedulePage;
    await test.step("Organiser invites attendee to the event.", async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        email: attendeeOneEmail,
      });
    });

    await test.step("Attendee one's browser context.", async () => {
      attendeeOnePage = await attendeeOneBrowserContext.newPage();
    });
    await test.step("Attendee one logs in to the event.", async () => {
      let magicLink: string;
      await test.step("Get magic link for attendee one.", async () => {
        magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeOneEmail
        );
      });
      await loginToEvent(attendeeOnePage, magicLink, defaultScheduleUrl);
      attendeeOneSchedulePage = new SchedulePage(attendeeOnePage);
    });

    await test.step("Skip profile completion for attendee.", async () => {
      await attendeeOneSchedulePage.skipProfileCompletionPopup();
    });

    await test.step("Verify empty state is visible on schedule tab.", async () => {
      await attendeeOneSchedulePage.verifyNoScheduleOutMessageIsVisible();
    });
  });

  venues.forEach((venue) => {
    test(`TC00${2 + venues.indexOf(venue)} : ${
      venue.venue
    } as venue. | @createSchedule`, async () => {
      test.info().annotations.push({
        type: "TC",
        description: `https://linear.app/zuddl/issue/QAT-${venue.ticketId}/`,
      });

      let attendeeOneFirstName = "Attendee";
      let attendeeOneLastName = `JxtinOne ${venue}`;
      let attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
      let attendeeOnePage;
      let attendeeOneSchedulePage: SchedulePage;
      let sessionData;
      let sessionId;
      if (venue.venue === "ROOMS" || venue.venue === "EXPO") {
        stageId = null;
      }
      await test.step("Creating segment.", async () => {
        [sessionId, sessionData] = await createSegment(
          orgApiContext,
          eventId,
          stageId,
          "Test Segment",
          venue.venue,
          eventController
        );
      });
      await test.step("Initialising attendee one's browser context.", async () => {
        attendeeOnePage = await attendeeOneBrowserContext.newPage();
      });

      await test.step("Organiser invites attendee to the event.", async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeOneFirstName,
          lastName: attendeeOneLastName,
          email: attendeeOneEmail,
        });
      });

      await test.step("Attendee one logs in to the event.", async () => {
        let magicLink: string;
        await test.step("Get magic link for attendee one.", async () => {
          magicLink = await QueryUtil.fetchMagicLinkFromDB(
            eventId,
            attendeeOneEmail
          );
        });
        await loginToEvent(attendeeOnePage, magicLink, defaultScheduleUrl);
        attendeeOneSchedulePage = new SchedulePage(attendeeOnePage);
        console.log("Attendee one schedule page initialised.");
      });

      await test.step("Skip profile completion for attendee.", async () => {
        await attendeeOneSchedulePage.skipProfileCompletionPopup();
      });

      await test.step("Verify session is visible on schedule tab.", async () => {
        await attendeeOneSchedulePage.validateVenue(venue.venue);
        await attendeeOneSchedulePage.validateTime(
          sessionData["start_time"],
          sessionData["end_time"]
        );
      });

      await test.step("Verify Schedule title and description.", async () => {
        await attendeeOneSchedulePage.validateScheduleName(
          sessionData["title"]
        );
        await attendeeOneSchedulePage.validateScheduleDescription(
          sessionData["description"]
        );
      });

      await test.step("Verify favourite icon is visible on schedule tab.", async () => {
        await attendeeOneSchedulePage.validateFavouriteIconisVisble();
      });
    });
  });

  test("TC010 : Verify presence of sub venue name if its added while creating a segment. | @createSchedule", async () => {
    test.info().annotations.push({
      type: "TC",
      description: `https://linear.app/zuddl/issue/QAT-533/verify-presence-of-sub-venue-name-if-its-added-while-creating-a`,
    });

    let roomsController = new RoomsController(orgApiContext, eventId);
    await roomsController.addRoomsToEvent({
      seatsPerRoom: 10,
      numberOfRooms: 2,
      roomCategory: "PUBLIC",
    });
    const getListOfRoomsResp = await roomsController.getListOfRooms();
    const roomList = await getListOfRoomsResp.json();
    console.log(roomList);
    const roomName = roomList[0]["name"];
    console.log(`Room name is ${roomName}`);
    const roomId = roomList[0]["discussionTableId"];
    console.log(`Room id is ${roomId}`);

    await test.step("Creating segment.", async () => {
      [sessionId, sessionData] = await createSegment(
        orgApiContext,
        eventId,
        roomId,
        "Test Segment",
        "ROOMS",
        eventController
      );
    });

    await test.step("Initialising attendee one's browser context.", async () => {
      attendeeOnePage = await attendeeOneBrowserContext.newPage();
    });

    await test.step("Organiser invites attendee to the event.", async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        email: attendeeOneEmail,
      });
    });

    await test.step("Attendee one logs in to the event.", async () => {
      let magicLink: string;
      await test.step("Get magic link for attendee one.", async () => {
        magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeOneEmail
        );
      });
      await loginToEvent(attendeeOnePage, magicLink, defaultScheduleUrl);
    });
    await test.step("Attendee navigates to schedule tab.", async () => {
      await attendeeOnePage.waitForURL(defaultScheduleUrl);
      attendeeOneSchedulePage = new SchedulePage(attendeeOnePage);
      console.log("Attendee one schedule page initialised.");
    });

    await test.step("Skip profile completion for attendee.", async () => {
      await attendeeOneSchedulePage.skipProfileCompletionPopup();
    });

    await test.step("Verify Schedule title and description.", async () => {
      await attendeeOneSchedulePage.validateScheduleName(sessionData["title"]);
      await attendeeOneSchedulePage.validateScheduleDescription(
        sessionData["description"]
      );
    });

    await test.step("Verify sub venue name is visible on schedule tab.", async () => {
      await attendeeOneSchedulePage.validateTime(
        sessionData["start_time"],
        sessionData["end_time"]
      );
      await attendeeOneSchedulePage.validateSubVenueName("ROOMS", roomName);
    });
  });

  test("TC011 : Verify flow of updating segment venue. | @updateSchedule", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-535/verify-flow-of-updating-segment-venue",
    });

    let roomsController = new RoomsController(orgApiContext, eventId);
    let scheduleController = new ScheduleController(orgApiContext, eventId);
    await roomsController.addRoomsToEvent({
      seatsPerRoom: 10,
      numberOfRooms: 2,
      roomCategory: "PUBLIC",
    });

    const getListOfRoomsResp = await roomsController.getListOfRooms();
    const roomList = await getListOfRoomsResp.json();
    console.log(roomList);

    let roomName = roomList[0]["name"];
    console.log(`Room name is ${roomName}`);
    let roomId = roomList[0]["discussionTableId"];

    await test.step("Creating segment.", async () => {
      [sessionId, sessionData] = await createSegment(
        orgApiContext,
        eventId,
        roomId,
        "Test Segment",
        "ROOMS",
        eventController
      );
    });

    await test.step("Initialising attendee one's browser context.", async () => {
      attendeeOnePage = await attendeeOneBrowserContext.newPage();
    });

    await test.step("Organiser invites attendee to the event.", async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        email: attendeeOneEmail,
      });
    });

    await test.step("Attendee one logs in to the event.", async () => {
      let magicLink: string;
      await test.step("Get magic link for attendee one.", async () => {
        magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeOneEmail
        );
      });
      await loginToEvent(attendeeOnePage, magicLink, defaultScheduleUrl);
    });
    await test.step("Attendee navigates to schedule tab.", async () => {
      await attendeeOnePage.waitForURL(defaultScheduleUrl);
      attendeeOneSchedulePage = new SchedulePage(attendeeOnePage);
      console.log("Attendee one schedule page initialised.");
    });

    await test.step("Skip profile completion for attendee.", async () => {
      await attendeeOneSchedulePage.skipProfileCompletionPopup();
    });

    await test.step("Verify Schedule title and description.", async () => {
      await attendeeOneSchedulePage.validateScheduleName(sessionData["title"]);
      await attendeeOneSchedulePage.validateScheduleDescription(
        sessionData["description"]
      );
    });

    await test.step("Verify favourite icon is visible on schedule tab.", async () => {
      await attendeeOneSchedulePage.validateFavouriteIconisVisble();
    });

    await test.step("Verify sub venue name is visible on schedule tab.", async () => {
      await attendeeOneSchedulePage.validateTime(
        sessionData["start_time"],
        sessionData["end_time"]
      );
      await attendeeOneSchedulePage.validateSubVenueName("ROOMS", roomName);
    });

    roomName = roomList[1]["name"];
    console.log(`New Room name is ${roomName}`);
    roomId = roomList[1]["discussionTableId"];
    console.log(`New Room id is ${roomId}`);

    // update sessionData["start_time"] and sessionData["end_time"] to new values (add 20 minutes to each)

    sessionData["start_time"] = new Date(sessionData["start_time"]);
    sessionData["start_time"].setMinutes(
      sessionData["start_time"].getMinutes() + 20
    );
    sessionData["end_time"] = new Date(sessionData["end_time"]);
    sessionData["end_time"].setMinutes(
      sessionData["end_time"].getMinutes() + 20
    );
    sessionData["start_time"] = sessionData["start_time"].toISOString();
    sessionData["end_time"] = sessionData["end_time"].toISOString();

    await test.step("Updating segment.", async () => {
      await scheduleController.updateSession(sessionId, {
        title: sessionData["title"],
        description: sessionData["description"],
        segmentId: sessionId,
        eventId: eventId,
        refId: roomId,
        startDateTime: sessionData["start_time"],
        endDateTime: sessionData["end_time"],
        type: "ROOMS",
      });
    });
    let newSessionData;

    await test.step("Get updated session data.", async () => {
      newSessionData = await eventController.getRequiredInfo(
        eventId,
        sessionId
      );
    });
    console.log(newSessionData);
    console.log(sessionData);

    await test.step("Verify newSessionData is same as what we updated.", async () => {
      const sessionStartTime = new Date(sessionData["start_time"]);
      const sessionEndTime = new Date(sessionData["end_time"]);
      const newSessionStartTime = new Date(newSessionData["startDateTime"]);
      const newSessionEndTime = new Date(newSessionData["endDateTime"]);

      expect(
        Math.abs(sessionStartTime.getTime() - newSessionStartTime.getTime()) /
          60000
      ).toBeLessThanOrEqual(2);
      expect(
        Math.abs(sessionEndTime.getTime() - newSessionEndTime.getTime()) / 60000
      ).toBeLessThanOrEqual(2);

      expect(sessionData["title"]).toEqual(newSessionData["title"]);
      expect(sessionData["description"]).toEqual(newSessionData["description"]);
      expect(roomId).toEqual(newSessionData["refId"]);
    });

    await test.step("Verify sub venue name is visible on schedule tab.", async () => {
      await attendeeOneSchedulePage.page.reload();
      await attendeeOneSchedulePage.validateTime(
        sessionData["start_time"],
        sessionData["end_time"]
      );
      await attendeeOneSchedulePage.validateVenue("ROOMS");
      await attendeeOneSchedulePage.validateSubVenueName("ROOMS", roomName);
    });

    await test.step("Update segment venue to STAGE.", async () => {
      await scheduleController.updateSession(sessionId, {
        title: sessionData["title"] + " updated",
        description: sessionData["description"] + " updated",
        segmentId: sessionId,
        eventId: eventId,
        refId: stageId,
        startDateTime: sessionData["start_time"],
        endDateTime: sessionData["end_time"],
        type: "STAGE",
      });
    });

    await test.step("Verify sub venue name is visible on schedule tab.", async () => {
      await attendeeOneSchedulePage.page.reload();
      await attendeeOneSchedulePage.validateTime(
        sessionData["start_time"],
        sessionData["end_time"]
      );
      await attendeeOneSchedulePage.validateVenue("STAGE");
      await attendeeOneSchedulePage.validateSubVenueName("STAGE", eventName);
      await attendeeOneSchedulePage.validateScheduleName(
        sessionData["title"] + " updated"
      );
      await attendeeOneSchedulePage.validateScheduleDescription(
        sessionData["description"] + " updated"
      );
    });
  });

  test("TC012 : Verify flow of deleting segment. @deleteSchedule", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-536/verify-flow-of-deleting-segment-venue",
    });

    await test.step("Creating segment.", async () => {
      [sessionId, sessionData] = await createSegment(
        orgApiContext,
        eventId,
        stageId,
        "Test Segment",
        "STAGE",
        eventController
      );
    });

    await test.step("Initialising attendee one's browser context.", async () => {
      attendeeOnePage = await attendeeOneBrowserContext.newPage();
    });

    await test.step("Organiser invites attendee to the event.", async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        email: attendeeOneEmail,
      });
    });

    await test.step("Attendee one logs in to the event.", async () => {
      let magicLink: string;
      await test.step("Get magic link for attendee one.", async () => {
        magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeOneEmail
        );
      });
      await loginToEvent(attendeeOnePage, magicLink, defaultScheduleUrl);
    });

    await test.step("Attendee navigates to schedule tab.", async () => {
      await attendeeOnePage.waitForURL(defaultScheduleUrl);
      attendeeOneSchedulePage = new SchedulePage(attendeeOnePage);
      console.log("Attendee one schedule page initialised.");
    });

    await test.step("Skip profile completion for attendee.", async () => {
      await attendeeOneSchedulePage.skipProfileCompletionPopup();
    });

    await test.step("Verify Schedule title and description.", async () => {
      await attendeeOneSchedulePage.validateScheduleName(sessionData["title"]);
      await attendeeOneSchedulePage.validateScheduleDescription(
        sessionData["description"]
      );
    });

    await test.step("Verify favourite icon is visible on schedule tab.", async () => {
      await attendeeOneSchedulePage.validateFavouriteIconisVisble();
    });

    await test.step("Delete the segment.", async () => {
      await eventController.deleteSession(sessionId);
    });

    await test.step("Verify segment is deleted.", async () => {
      await attendeeOneSchedulePage.page.reload();
      await attendeeOneSchedulePage.verifyNoScheduleOutMessageIsVisible();
    });
  });

  test("TC013 : verify restriction of segment overlapping if we try creating segment on same venue, same time and same date. | @updateSchedule @scheduleOverlap", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-534/verify-restriction-of-segment-overlapping-if-we-try-creating-segment",
    });

    let attendeeOneFirstName = "Attendee";
    let attendeeOneLastName = `JxtinOne`;

    let attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
    let attendeeOnePage;
    let attendeeOneSchedulePage: SchedulePage;
    let sessionData;
    let sessionId;

    let roomsController = new RoomsController(orgApiContext, eventId);

    await roomsController.addRoomsToEvent({
      seatsPerRoom: 10,
      numberOfRooms: 2,
      roomCategory: "PUBLIC",
    });
    const getListOfRoomsResp = await roomsController.getListOfRooms();
    const roomList = await getListOfRoomsResp.json();
    console.log(roomList);
    const roomName = roomList[0]["name"];
    console.log(`Room name is ${roomName}`);
    const roomId = roomList[0]["discussionTableId"];
    console.log(`Room id is ${roomId}`);

    await test.step("Creating segment.", async () => {
      [sessionId, sessionData] = await createSegment(
        orgApiContext,
        eventId,
        roomId,
        "Test Segment",
        "ROOMS",
        eventController
      );
    });

    await test.step("Initialising attendee one's browser context.", async () => {
      attendeeOnePage = await attendeeOneBrowserContext.newPage();
    });

    await test.step("Organiser invites attendee to the event.", async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        email: attendeeOneEmail,
      });
    });

    await test.step("Attendee one logs in to the event.", async () => {
      let magicLink: string;
      await test.step("Get magic link for attendee one.", async () => {
        magicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventId,
          attendeeOneEmail
        );
      });
      await loginToEvent(attendeeOnePage, magicLink, defaultScheduleUrl);
    });

    await test.step("Attendee navigates to schedule tab.", async () => {
      await attendeeOnePage.waitForURL(defaultScheduleUrl);
      attendeeOneSchedulePage = new SchedulePage(attendeeOnePage);
      console.log("Attendee one schedule page initialised.");
    });

    await test.step("Skip profile completion for attendee.", async () => {
      await attendeeOneSchedulePage.skipProfileCompletionPopup();
    });

    await test.step("Verify Schedule title and description.", async () => {
      await attendeeOneSchedulePage.validateScheduleName(sessionData["title"]);
      await attendeeOneSchedulePage.validateScheduleDescription(
        sessionData["description"]
      );
    });

    await test.step("Verify favourite icon is visible on schedule tab.", async () => {
      await attendeeOneSchedulePage.validateFavouriteIconisVisble();
    });
    let newSessionId;
    let newSessionData;
    await test.step("Create another segment for the same venue.", async () => {
      // expect an error here
      let e;
      try {
        [newSessionId, newSessionData] = await createSegment(
          orgApiContext,
          eventId,
          roomId,
          "Test Segment",
          "ROOMS",
          eventController
        );
      } catch (error) {
        console.log(error);
        e = error;
      }

      expect(e).toBeDefined();
      expect(e.message).toContain(
        "Segment Date and Time should not overlaps with  other segments"
      );

      console.log(newSessionId);
      console.log(newSessionData);
    });

    await test.step("Verify segment is not created.", async () => {
      await attendeeOneSchedulePage.page.reload();
      await attendeeOneSchedulePage.verifySessionPresenceById(sessionId);
    });
    await test.step("Verify Schedule title and description.", async () => {
      await attendeeOneSchedulePage.validateScheduleName(sessionData["title"]);
      await attendeeOneSchedulePage.validateScheduleDescription(
        sessionData["description"]
      );
    });
  });

  test("TC014 : Verify flow of adding new speaker when creating a segment for stage.| @addSpeaker @speakerIcon ", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-537/verify-flow-of-adding-new-speaker-when-creating-a-segment-for-stage",
    });

    let speakerFirstName = "Speaker";
    let speakerLastName = `JxtinOne`;
    let speakerEmail = DataUtl.getRandomSpeakerEmail();
    let speakerList;
    let speakerPage;
    let speakerBrowserContext = await BrowserFactory.getBrowserContext({
      browserName: BrowserName.CHROME,
      laodOrganiserCookies: false,
    });
    let speakerSchedulePage: SchedulePage;

    await test.step("Invite speaker to the event.", async () => {
      await eventController.inviteSpeakerToTheEventByApi(
        speakerEmail,
        speakerFirstName,
        speakerLastName
      );
    });

    await test.step("Get list of speakers.", async () => {
      speakerList = await getListOfSpeakersInEvent(orgApiContext, eventId);
      console.log(speakerList);
    });

    let speakerDetails: {
      speakerId: string;
      speakerFirstName: string;
      speakerLastName: string;
      speakerDesignation?: string;
      speakerBio?: string;
      speakerCompany?: string;
    };

    await test.step("Find if speaker is present in the list.", async () => {
      for (let speaker of speakerList) {
        if (speaker.email === speakerEmail) {
          speakerDetails = {
            speakerId: speaker.speakerId,
            speakerFirstName: speaker.firstName,
            speakerLastName: speaker.lastName,
            speakerDesignation: speaker.designation,
            speakerBio: speaker.bio,
            speakerCompany: speaker.company,
          };
          console.log(`Speaker id is ${speakerDetails.speakerId}`);
          console.log(speakerDetails);
          break;
        }
      }
    });

    [sessionId, sessionData] = await createSegment(
      orgApiContext,
      eventId,
      stageId,
      "Test Segment",
      "STAGE",
      eventController,
      [
        {
          firstName: speakerDetails.speakerFirstName,
          lastName: speakerDetails.speakerLastName,
          speakerEmail: speakerEmail,
          speakerId: speakerDetails.speakerId,
        },
      ]
    );

    await test.step("Initialising speaker's browser context.", async () => {
      speakerPage = await speakerBrowserContext.newPage();
    });

    await test.step("Speaker logs in to the event.", async () => {
      let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );

      loginToEvent(speakerPage, magicLink, defaultScheduleUrl);
    });

    await test.step("Speaker navigates to schedule tab.", async () => {
      speakerSchedulePage = new SchedulePage(speakerPage);
    });

    await test.step("Skip profile completion for speaker.", async () => {
      await speakerSchedulePage.skipProfileCompletionPopup();
      await speakerSchedulePage.page.waitForTimeout(2000);
    });

    await test.step("Verify Schedule title and description.", async () => {
      await speakerSchedulePage.validateScheduleName(sessionData["title"]);
      await speakerSchedulePage.validateScheduleDescription(
        sessionData["description"]
      );
    });

    await test.step("Verify Speaker details on schedule page.", async () => {
      await speakerSchedulePage.verifySpeakerDetails(speakerDetails);
    });

    await test.step("Close speaker's browser context.", async () => {
      await speakerBrowserContext.close();
    });
  });

  test("TC015 : Verify if a segment is created and added a tag, that tag is visible on schedule tab and attendee should be able to filter segments with it. | @addTags @filter", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-538/verify-if-a-segment-is-created-and-added-a-tag-that-tag-is-visible-on",
    });

    let eventController = new EventController(orgApiContext, eventId);
    let attendeeOneEmail = DataUtl.getRandomAttendeeEmail();

    let attendeeOnePage;

    let sessionOneTags: { tagId: string; tagName: string }[] = [];
    let sessionTwoTags: { tagId: string; tagName: string }[] = [];
    for (let i = 0; i < 3; i++) {
      await test.step("Create a tag, which will be assigned.", async () => {
        let tagId = await eventController.createTag(`Test Tag ${i}`);
        sessionOneTags.push({
          tagId: tagId,
          tagName: `Test Tag ${i}`,
        });
      });
    }

    for (let i = 0; i < 3; i++) {
      await test.step("Create a tag, which will not be.", async () => {
        let tagId = await eventController.createTag(`Second Test Tag ${i}`);
        sessionTwoTags.push({
          tagId: tagId,
          tagName: `Second Test Tag ${i}`,
        });
      });
    }

    await test.step("Create a segment and add tag to it.", async () => {
      [sessionId, sessionData] = await createSegment(
        orgApiContext,
        eventId,
        stageId,
        "Test Segment",
        "STAGE",
        eventController,
        [],
        sessionOneTags.map((tag) => tag.tagId)
      );
    });

    const roomsController = new RoomsController(orgApiContext, eventId);
    let roomId: string;
    await test.step("Create rooms to be used as venue", async () => {
      await roomsController.addRoomsToEvent({
        seatsPerRoom: 10,
        numberOfRooms: 1,
        roomCategory: "PUBLIC",
      });
    });
    let roomName: string;
    await test.step("Get room id", async () => {
      const getListOfRoomsResp = await roomsController.getListOfRooms();
      const roomList = await getListOfRoomsResp.json();
      console.log(roomList);
      roomName = roomList[0]["name"];
      console.log(`Room name is ${roomName}`);
      roomId = roomList[0]["discussionTableId"];
      console.log(`Room id is ${roomId}`);
    });

    let sessionTwoId;
    let sessionTwoData;

    await test.step("Create another segment and add tag to it.", async () => {
      [sessionTwoId, sessionTwoData] = await createSegment(
        orgApiContext,
        eventId,
        roomId,
        "Test Segment",
        "ROOMS",
        eventController,
        [],
        sessionTwoTags.map((tag) => tag.tagId)
      );

      await test.step("Initialise attendee one's browser context.", async () => {
        attendeeOnePage = await attendeeOneBrowserContext.newPage();
      });

      await test.step("Organiser invites attendee to the event.", async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeOneFirstName,
          lastName: attendeeOneLastName,
          email: attendeeOneEmail,
        });
      });

      await test.step("Attendee one logs in to the event.", async () => {
        let magicLink: string;
        await test.step("Get magic link for attendee one.", async () => {
          magicLink = await QueryUtil.fetchMagicLinkFromDB(
            eventId,
            attendeeOneEmail
          );
        });
        await loginToEvent(attendeeOnePage, magicLink, defaultScheduleUrl);
        attendeeOneSchedulePage = new SchedulePage(attendeeOnePage);
      });

      await test.step("Skip profile completion for attendee.", async () => {
        await attendeeOneSchedulePage.skipProfileCompletionPopup();
        await attendeeOneSchedulePage.page.waitForTimeout(2000);
      });
    });

    await test.step("Verify Presence of session one.", async () => {
      await attendeeOneSchedulePage.verifySessionPresenceById(sessionId);
    });

    await test.step("Verify Presence of session two.", async () => {
      await attendeeOneSchedulePage.verifySessionPresenceById(sessionTwoId);
    });

    await test.step("Verify session one tags.", async () => {
      await attendeeOneSchedulePage.verifyTags(
        sessionOneTags.map((tag) => tag.tagName)
      );
    });

    await test.step("Verify session two tags.", async () => {
      await attendeeOneSchedulePage.verifyTags(
        sessionTwoTags.map((tag) => tag.tagName)
      );
    });

    await test.step("Filter sessions by tag.", async () => {
      await attendeeOneSchedulePage.selectTag(sessionOneTags[0].tagName);
      await attendeeOneSchedulePage.verifySessionPresenceById(sessionId);
      await attendeeOneSchedulePage.verifySessionAbsence(sessionTwoId);

      await attendeeOneSchedulePage.unselectActiveTag(
        sessionOneTags[0].tagName
      );

      await attendeeOneSchedulePage.selectTag(sessionTwoTags[1].tagName);
      await attendeeOneSchedulePage.verifySessionPresenceById(sessionTwoId);
      await attendeeOneSchedulePage.verifySessionAbsence(sessionId);

      await attendeeOneSchedulePage.selectTag(sessionOneTags[0].tagName);
      await attendeeOneSchedulePage.verifySessionPresenceById(sessionId);
      await attendeeOneSchedulePage.verifySessionPresenceById(sessionTwoId);
    });
  });

  test("TC016 : Verify flow of organiser/speakers moving to stage through segment backstage button.| @joinBackstage @segment", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-539",
    });

    let studioId;

    let eventController = new EventController(orgApiContext, eventId);

    await test.step("Enabling Studio as backstage for default stage.", async () => {
      studioId = await eventController.enableStudioAsBackstage(stageId);
    });

    let studioController: StudioController;
    let sessionId;
    let organiserPage;
    let organiserStagePage: StagePage;
    let organiserBackstage: StudioPage;
    let organiserSchedulePage: SchedulePage;

    let speakerEmail;
    let speakerPage;
    let speakerFirstName = "TestSpeaker";
    let speakerLastName = "QA";
    let speakerBrowserContext;
    let speakerSchedulePage: SchedulePage;
    let speakerStagePage: StagePage;
    let speakerBackstage: StudioPage;

    await test.step("Get random speaker emails.", async () => {
      speakerEmail = DataUtl.getRandomSpeakerEmail();
      console.log("Speaker Email", speakerEmail);
    });

    await test.step("Initialising new speaker", async () => {
      speakerBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });
    });

    await test.step("Disabling onboarding checks for speaker and attendee access group", async () => {
      await eventController.getAccessGroupController.handleOnboardingChecksForSpeaker(
        false
      );
    });

    await test.step(`Organiser invites attendee and speaker to the event.`, async () => {
      await test.step(`Invites Speaker.`, async () => {
        await eventController.inviteSpeakerToTheEventByApi(
          speakerEmail,
          speakerFirstName,
          speakerLastName
        );
      });
    });

    await test.step("Initialize speaker's browser context.", async () => {
      speakerPage = await speakerBrowserContext.newPage();
    });

    await test.step("Add a segment with schedule start time before current time and end time after 1 minute.", async () => {
      sessionId = await eventController.createSession(1, stageId, 0, 0, -1, 2);
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step("Speaker browser context.", async () => {
      speakerPage = await speakerBrowserContext.newPage();
    });

    await test.step(`Initialize studio controller.`, async () => {
      studioController = new StudioController(orgApiContext, studioId, stageId);
    });

    await test.step(`Organizer joins the event and go to schedule page.`, async () => {
      await organiserPage.goto(defaultScheduleUrl);
    });

    await test.step(`Speaker joins the event mid-session and enter backstage.`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );

      await speakerPage.goto(magicLink);
      await speakerPage.click(`text=Enter Now`);
      await speakerPage.goto(defaultScheduleUrl);
    });

    await test.step(`Organizer joins the backstage of current session.`, async () => {
      await test.step(`Organiser click on join backstage for current session.`, async () => {
        organiserSchedulePage = new SchedulePage(organiserPage);
        await organiserSchedulePage.clickOnSessionBackstageButton(sessionId);
      });

      await test.step(`Organiser joins backstage.`, async () => {
        organiserStagePage = new StagePage(organiserPage);
        await organiserStagePage.getAVModal.isVideoStreamLoadedOnAVModalSAB();
        await organiserStagePage.getAVModal.clickOnEnterStudioOnAvModal();
      });

      await test.step(`Verify Organiser's stream in backstage.`, async () => {
        organiserBackstage = new StudioPage(organiserPage, studioController);
        await organiserBackstage.isBackstageVisible();
        await organiserBackstage.verifyCountOfStreamInBackstage(1);
      });
    });

    await test.step(`Speaker joins the backstage of current session.`, async () => {
      await test.step(`Speaker click on join backstage for current session.`, async () => {
        speakerSchedulePage = new SchedulePage(speakerPage);

        await test.step("Skip profile completion Speaker.", async () => {
          await speakerSchedulePage.skipProfileCompletionPopup();
        });

        await speakerSchedulePage.clickOnSessionBackstageButton(sessionId);
      });

      await test.step(`Speaker joins backstage.`, async () => {
        speakerStagePage = new StagePage(speakerPage);
        await speakerStagePage.getAVModal.isVideoStreamLoadedOnAVModalSAB();
        await speakerStagePage.getAVModal.clickOnEnterStudioOnAvModal();
      });

      await test.step(`Verify Speaker's stream in backstage.`, async () => {
        speakerBackstage = new StudioPage(speakerPage, studioController);
        await speakerBackstage.isBackstageVisible();
        await speakerBackstage.verifyCountOfStreamInBackstage(2);
      });
    });
  });
});
