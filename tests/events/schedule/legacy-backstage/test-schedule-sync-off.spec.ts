import { test, BrowserContext, APIRequestContext } from "@playwright/test";
import { EventController } from "../../../../controller/EventController";
import BrowserName from "../../../../enums/BrowserEnum";
import EventEntryType from "../../../../enums/eventEntryEnum";
import { SchedulePage } from "../../../../page-objects/events-pages/zones/SchedulePage";
import BrowserFactory from "../../../../util/BrowserFactory";
import { updateEventLandingPageDetails } from "../../../../util/apiUtil";
import { DataUtl } from "../../../../util/dataUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe
  .parallel("Schedule Sync Off, Legacy Backstage| @scheduleSyncOff @legacyBackstage", () => {
  test.describe.configure({ retries: 2 });

  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId;
  let eventController: EventController;
  let stageId;
  let attendeeOneBrowserContext: BrowserContext;
  let defaultScheduleUrl: string;

  test.beforeEach(async () => {
    await test.step("Initialising organizer's browser and api contexts", async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: true,
      });

      orgApiContext = orgBrowserContext.request;
    });

    await test.step("Creating new event.", async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: DataUtl.getRandomEventTitle(),
        default_settings: false,
        add_start_date_from_today: 0,
        add_end_date_from_today: 1,
        add_hours_to_start_date: -5,
      });
    });

    await test.step("Initialising new event controller.", async () => {
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step("Fetching deafult stage id.", async () => {
      stageId = await eventController.getDefaultStageId();
    });

    await test.step("Initialising new attendee", async () => {
      attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });
    });

    await test.step("Enabling Magiclink for attendee.", async () => {
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
      });
    });

    await test.step("Initialising default schedule url.", async () => {
      defaultScheduleUrl = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/l/event/${eventId}/schedule`;
    });

    await test.step("Disabling onboarding checks for speaker and attendees access group.", async () => {
      await eventController.getAccessGroupController.handleOnboardingChecksForSpeaker(
        false
      );
      await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
        false
      );
    });

    await test.step("Toggle schedule sync off.", async () => {
      await eventController.toggleScheduleSync(false);
    });
  });

  test.afterEach(async () => {
    await test.step("Toggle schedule sync back on.", async () => {
      await eventController.toggleScheduleSync(true);
    });

    await test.step("Deleting event.", async () => {
      await eventController.cancelAnEvent(eventId);
    });

    await orgBrowserContext?.close();
    await attendeeOneBrowserContext?.close();
  });

  test("TC001 : Attendee see Completed Status if current time is greater than session end time", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-559/attendee-see-completed-status-if-current-time-is-greater-than-session",
    });

    const attendeeOneFirstName = "PrateekAttendeeOne";
    const attendeeOneLastName = "QA";
    let attendeeOneEmail;
    let sessionId;

    let attendeeOnePage;
    let attendeeOneSchedulePage: SchedulePage;
    await test.step("Add a segment with schedule end time before current time.", async () => {
      sessionId = await eventController.createSession(1, stageId, 0, 0, -2, -1);
    });

    await test.step("Get random attendee emails.", async () => {
      attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
      console.log(`Attendee One Email: ${attendeeOneEmail}`);
    });

    await test.step(`Organiser invites  attendee to the event.`, async () => {
      await test.step(`Invites Attendees.`, async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeOneFirstName,
          lastName: attendeeOneLastName,
          phoneNumber: "1234567899",
          email: attendeeOneEmail,
        });
      });
    });

    await test.step("Attendees browser context.", async () => {
      attendeeOnePage = await attendeeOneBrowserContext.newPage();
    });

    await test.step(`Attendee joins the event and enter default stage.`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeOneEmail
      );

      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.goto(defaultScheduleUrl);
      await attendeeOnePage.waitForURL(defaultScheduleUrl);

      attendeeOneSchedulePage = new SchedulePage(attendeeOnePage);
    });

    await test.step("Skip profile completion for attendee.", async () => {
      await attendeeOneSchedulePage.skipProfileCompletionPopup();
    });

    await test.step('Verify that attendee is able to see "completed" status.', async () => {
      await attendeeOneSchedulePage.verifyTopScheduleButtonIsVisible();
      await attendeeOneSchedulePage.verifyCompletedButtonIsVisible();
    });
  });

  test("TC002 : Speaker see Completed and Join Backstage button if current time is greater than session end time", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-560/speaker-see-completed-and-join-backstage-button-if-current-time-is",
    });

    const speakerOneFirstName = "jxtinSpeakerOne";
    const speakerOneLastName = "QA";
    let speakerOneEmail;
    let sessionId;

    let speakerOnePage;
    let speakerOneSchedulePage: SchedulePage;

    await test.step("Add a segment with schedule end time before current time.", async () => {
      sessionId = await eventController.createSession(1, stageId, 0, 0, -2, -1);
    });

    await test.step("Get random speaker emails.", async () => {
      speakerOneEmail = DataUtl.getRandomSpeakerEmail();
      console.log(`Speaker One Email: ${speakerOneEmail}`);
    });

    await test.step(`Organiser invites  speaker to the event.`, async () => {
      await test.step(`Invites Speakers.`, async () => {
        await eventController.inviteSpeakerToTheEventByApi(
          speakerOneEmail,
          speakerOneFirstName,
          speakerOneLastName
        );
      });
    });

    await test.step("Speaker browser context.", async () => {
      speakerOnePage = await attendeeOneBrowserContext.newPage();
    });

    await test.step(`Speaker joins the event and enter default stage.`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerOneEmail
      );

      await speakerOnePage.goto(magicLink);
      await speakerOnePage.click(`text=Enter Now`);
      await speakerOnePage.goto(defaultScheduleUrl);
      await speakerOnePage.waitForURL(defaultScheduleUrl);

      speakerOneSchedulePage = new SchedulePage(speakerOnePage);
    });
    await test.step("Skip profile completion for speaker.", async () => {
      await speakerOneSchedulePage.skipProfileCompletionPopup();
    });

    await test.step('Verify that attendee is able to see "completed" status.', async () => {
      await speakerOneSchedulePage.verifyTopScheduleButtonIsVisible();
      await speakerOneSchedulePage.verifyCompletedButtonIsVisible();
    });

    await test.step("Verify join backstage button is also visible.", async () => {
      await speakerOneSchedulePage.verifyBackstageButtonIsVisible();
    });
  });

  test("TC003 : Attendee see Join button if current time is between session start and end time", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-561/attendee-see-join-button-if-current-time-is-between-session-start-and",
    });

    const attendeeOneFirstName = "Jxtin";
    const attendeeOneLastName = "QA";
    let attendeeOneEmail;

    let attendeeOnePage;
    let attendeeOneSchedulePage: SchedulePage;
    let sessionId;

    await test.step("Create a schedule session with start time 1 hour before current time and end time 1 hour after current time.", async () => {
      sessionId = await eventController.createSession(1, stageId, 0, 0, -1, 1);
    });

    await test.step("Get random attendee emails.", async () => {
      attendeeOneEmail = DataUtl.getRandomAttendeeEmail();
      console.log(`Attendee One Email: ${attendeeOneEmail}`);
    });

    await test.step(`Organiser invites  attendee to the event.`, async () => {
      await test.step(`Invites Attendees.`, async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeOneFirstName,
          lastName: attendeeOneLastName,
          phoneNumber: "1234567899",
          email: attendeeOneEmail,
        });
      });
    });

    await test.step("Attendees browser context.", async () => {
      attendeeOnePage = await attendeeOneBrowserContext.newPage();
    });

    await test.step(`Attendee joins the event and enter default stage.`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeOneEmail
      );

      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
    });

    await test.step("Attendee navigates to schedule page.", async () => {
      await attendeeOnePage.goto(defaultScheduleUrl);
      await attendeeOnePage.waitForURL(defaultScheduleUrl);

      attendeeOneSchedulePage = new SchedulePage(attendeeOnePage);
    });

    await test.step("Skip profile completion for attendee.", async () => {
      await attendeeOneSchedulePage.skipProfileCompletionPopup();
    });

    await test.step('Verify that attendee is able to see "Join" button.', async () => {
      await attendeeOneSchedulePage.verifyTopScheduleButtonIsVisible();
      await attendeeOneSchedulePage.verifyJoinButtonIsVisible();
    });
  });
});
