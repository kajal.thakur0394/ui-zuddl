import {
  APIRequestContext,
  BrowserContext,
  Page,
  test,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { ChatComponent } from "../../../page-objects/events-pages/live-side-components/ChatComponent";
import { InteractionPanel } from "../../../page-objects/events-pages/live-side-components/InteractionPanel";
import { LobbyPage } from "../../../page-objects/events-pages/zones/LobbyPage";
import {
  inviteAttendeeByAPI,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { DBUtil } from "../../../util/dbUtil";
import BrowserName from "../../../enums/BrowserEnum";
import { QueryUtil } from "../../../DB/QueryUtil";

test.describe(`@zone-sanity @lobby`, async () => {
  test.describe.configure({ retries: 2 });
  let orgBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  let eventTitle: string;
  let eventId;
  let attendeeEmail: string;
  let attendeePage: Page;
  let landingPageOne: LandingPageOne;
  let lobbyPage: LobbyPage;
  let interactionPanel: InteractionPanel;
  let chatComponent: ChatComponent;
  let eventInfoDto: EventInfoDTO;
  let attendeeContext: BrowserContext;
  let lobbyPageUrl: string;

  test.beforeAll(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiRequestContext = await orgBrowserContext.request;
    eventId =
      DataUtl.getApplicationTestDataObj()["eventIdForLobbyWidgetTesting"];
    let eventController = new EventController(orgApiRequestContext, eventId);
    let eventInfo = await (await eventController.getEventInfo()).json();
    if (eventInfo.status == "CANCELED") {
      let duplicateEventId = await eventController.duplicateEvent({
        title: "Lobby Widgets Test",
      });
      eventId = duplicateEventId;
      eventController = new EventController(orgApiRequestContext, eventId);
    }
    await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
      false
    );
    // update the event start and end time
    await eventController.changeEventStartAndEndDateTime({
      deltaByHoursInStartTime: -1,
      deltaByHoursInEndTime: 3,
    });
    await updateEventLandingPageDetails(orgApiRequestContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });
    eventInfoDto = new EventInfoDTO(eventId, "lobby-widgets-donottouch", true); // true to denote magic link enabled
    attendeeContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      browserName: BrowserName.CHROME,
    });
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await inviteAttendeeByAPI(
      orgApiRequestContext,
      eventId,
      attendeeEmail,
      false
    );
    attendeePage = await attendeeContext.newPage();
    const magicLink = await QueryUtil.fetchMagicLinkFromDB(
      eventId,
      attendeeEmail
    );
    landingPageOne = new LandingPageOne(attendeePage);
    await landingPageOne.load(magicLink);
    // await attendeePage.waitForLoadState("networkidle");
    await landingPageOne.clickOnEnterNowButton();
    lobbyPage = await landingPageOne.isLobbyLoaded();
    lobbyPageUrl = landingPageOne.page.url();
    console.log(lobbyPageUrl);
    await lobbyPage.handleEditProfilePrompt(true);
    await lobbyPage.handleTourPrompt(true);
  });

  test.beforeEach(async ({}) => {
    await lobbyPage.reload();
  });

  test.afterEach(async () => {});

  test.afterAll(async () => {
    // Delete all attendees from event
    // await deleteAllAttendeesFromEvent(orgApiRequestContext, eventId);
    // Closing attendee context
    await attendeeContext?.close();
    // Close org browser context
    await orgBrowserContext?.close();
  });

  test(`TC001: @attendee lands on lobby, verifying image widget`, async () => {
    // Validating widgets
    await test.step(`Validating Image widget`, async () => {
      let imageAlt = "uploadedImage";
      let redirectedHref = "https://www.google.com/";
      await lobbyPage.verifyImageWidget(imageAlt, redirectedHref);
    });
  });

  test(`TC002: @attendee lands on lobby, verifying video widgets`, async () => {
    // Validating Video widget
    await test.step(`Validating Video widget`, async () => {
      await lobbyPage.verifyVideoWidget();
    });
  });

  test(`TC003: @attendee lands on lobby, verifying File download widgets`, async () => {
    // Verify File download widget
    await test.step(`Validating  Iframe widget`, async () => {
      let redirectedHref =
        "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf";
      await lobbyPage.verifyFileDownloadWidget(redirectedHref);
    });
  });

  test(`TC004: @attendee lands on lobby, verifying iframe widgets`, async () => {
    // Verify iframe widget
    await test.step(`Validating  Iframe widget`, async () => {
      let srcIframe = "https://en.wikipedia.org/wiki/Main_Page";
      await lobbyPage.verifyIframeWidget(srcIframe);
    });
  });

  test(`TC005: @attendee lands on lobby, verifying Hotspot widgets`, async () => {
    // Validating Hotspot button
    await test.step(`Validating  Hotspot widget`, async () => {
      let redirectedHref = `${
        DataUtl.getApplicationTestDataObj()["baseUrl"]
      }/l/event/${eventId}/networking`;
      await lobbyPage.verifyHotspotWidget(redirectedHref);
    });
  });

  test(`TC006: @attendee lands on lobby, verifying Go to button widgets`, async () => {
    // Validating Go to button
    await test.step(`Validating Go to widget`, async () => {
      let redirectedHref = `${
        DataUtl.getApplicationTestDataObj()["baseUrl"]
      }/l/event/${eventId}/discussions`;
      await lobbyPage.verifyGoToWidget(redirectedHref);
    });
  });

  test(`TC007: @attendee lands on lobby, verifying Ticker widgets`, async () => {
    // Ticker widget
    await test.step(`Validating Ticker widget and its content`, async () => {
      let tickercontent =
        "This is ticker widget to test how it works and whether it works fine.";
      await lobbyPage.verifyTickerWidget(tickercontent);
    });
  });

  test(`TC008: @attendee lands on lobby, verifying External Image carousel widgets`, async () => {
    await test.step(`Validating External Image Carousel widget`, async () => {
      let redirectedHref = `https://www.youtube.com/`;
      await lobbyPage.verifyExternalImageCarouselWidget(1, redirectedHref);
    });
  });

  test(`TC009: @attendee lands on lobby, verifying Internal image widgets`, async () => {
    await test.step(`Validating Internal Image Carousel widget`, async () => {
      let redirectedHref = `${
        DataUtl.getApplicationTestDataObj()["baseUrl"]
      }/l/event/${eventId}/expo`;
      await lobbyPage.verifyInternalImageCarouselWidget(redirectedHref, 0);
    });
  });
});
