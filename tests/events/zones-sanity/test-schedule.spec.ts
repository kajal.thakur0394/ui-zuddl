import {
  APIRequestContext,
  BrowserContext,
  test,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import EventEntryType from "../../../enums/eventEntryEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { LobbyPage } from "../../../page-objects/events-pages/zones/LobbyPage";
import { SchedulePage } from "../../../page-objects/events-pages/zones/SchedulePage";
import {
  createNewEvent,
  inviteAttendeeByAPI,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import ZoneType from "../../../enums/ZoneTypeEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel(`@zone-sanity @schedule`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  let eventTitle: string;
  let eventId;
  let attendeeEmail: string;
  let schedulePage: SchedulePage;
  let lobbyPage: LobbyPage;
  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiRequestContext = await orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiRequestContext,
      event_title: eventTitle,
    });
    const eventController = new EventController(orgApiRequestContext, eventId);
    await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
      false
    );
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await inviteAttendeeByAPI(
      orgApiRequestContext,
      eventId,
      attendeeEmail,
      false
    );
    await updateEventLandingPageDetails(orgApiRequestContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`@attendee lands on schedule, verify empty state`, async ({ page }) => {
    const magicLink = await QueryUtil.fetchMagicLinkFromDB(
      eventId,
      attendeeEmail
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(magicLink);
    await landingPageOne.clickOnEnterNowButton();
    lobbyPage = await landingPageOne.isLobbyLoaded();
    schedulePage = (await (
      await lobbyPage.getTopNavBarComponent()
    ).switchToZone(ZoneType.SCHEDULE)) as SchedulePage;
    const expectedHeadingOfSchedule = "We're glad to have you here";
    const expectedTextForNoScheduleheading = "Schedule is not out yet";
    await test.step(`empty state validation of schedule zone`, async () => {
      await expect(
        schedulePage.emptyStateScheduleHeading,
        `expecting empty state schedule heading to have text ${expectedHeadingOfSchedule}`
      ).toContainText(expectedHeadingOfSchedule);
      await expect(
        schedulePage.noScheduleOutMessage,
        `expecting empty state schedule heading to have text ${expectedTextForNoScheduleheading}`
      ).toContainText(expectedTextForNoScheduleheading);
    });
  });
});
