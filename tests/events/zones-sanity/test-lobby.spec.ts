import {
  APIRequestContext,
  BrowserContext,
  Page,
  test,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import EventEntryType from "../../../enums/eventEntryEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { ChatComponent } from "../../../page-objects/events-pages/live-side-components/ChatComponent";
import { InteractionPanel } from "../../../page-objects/events-pages/live-side-components/InteractionPanel";
import { LobbyPage } from "../../../page-objects/events-pages/zones/LobbyPage";
import {
  createNewEvent,
  inviteAttendeeByAPI,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@zone-sanity @lobby`, async () => {
  test.describe.configure({ retries: 2 });
  let orgBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  let eventTitle: string;
  let eventId;
  let attendeeEmail: string;
  let attendeePage: Page;
  let lobbyPage: LobbyPage;
  let interactionPanel: InteractionPanel;
  let chatComponent: ChatComponent;

  test.beforeEach(async ({ context }) => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiRequestContext = await orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiRequestContext,
      event_title: eventTitle,
    });
    const eventController = new EventController(orgApiRequestContext, eventId);
    await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
      false
    );
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await inviteAttendeeByAPI(
      orgApiRequestContext,
      eventId,
      attendeeEmail,
      false
    );
    await updateEventLandingPageDetails(orgApiRequestContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });
    attendeePage = await context.newPage();
    const magicLink = await QueryUtil.fetchMagicLinkFromDB(
      eventId,
      attendeeEmail
    );
    let landingPageOne = new LandingPageOne(attendeePage);
    await landingPageOne.load(magicLink);
    await landingPageOne.clickOnEnterNowButton();
    lobbyPage = await landingPageOne.isLobbyLoaded();
    await lobbyPage.handleEditProfilePrompt(true);
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`@attendee lands on lobby, interacts with chat icon`, async () => {
    const countOfMessageBeforeTest = 0;

    await test.step(`clicking on chat icon to open chat panel and send a new message`, async () => {
      await lobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      chatComponent = lobbyPage.getChatComponent;
      await chatComponent.sendChatMessage("Hi this is from automation");
    });
    await test.step(`Validating that the new message is appearing on chat box container`, async () => {
      await expect(
        chatComponent.getCountOfTotalMessageVisibleInChatContainer(),
        `expecting count of message to be ${countOfMessageBeforeTest} + 1 after sending new message`
      ).toHaveCount(countOfMessageBeforeTest + 1);
    });
    /**
     * TO DO :
     * 1) validation of sender name
     * 2) validation of time
     * 3) validation of message content
     * 4) validation of sender label
     */
    await test.step(`Validating message properties of just sent messsage`, async () => {});

    await test.step(`Close the interaction panel`, async () => {
      await lobbyPage.closeInteractionPanel();
      await lobbyPage.verifyIneractionPanelIsClosed();
    });
  });

  test(`@attendee lands on lobby, interacts with people icon`, async () => {
    await test.step(`clicking on people icon to open people section`, async () => {
      await lobbyPage.clickOnPeopleIconOfClosedInteractionPanelState();
    });
    await test.step(`Close the interaction panel`, async () => {
      await lobbyPage.closeInteractionPanel();
      await lobbyPage.verifyIneractionPanelIsClosed();
    });
  });

  test(`@attendee lands on lobby, open interaction panel and switches tabs`, async () => {
    await test.step(`clicking on chat icon to open people section`, async () => {
      await lobbyPage.clickOnChatIconOfClosedInteractionPanelState();
    });
    await test.step(`switch to people tab`, async () => {
      await lobbyPage.clickOnPeopleTabOnInteractionPanel();
    });
    await test.step(`switch back to  chat tab`, async () => {
      await lobbyPage.clickOnChatTabOnInteractionPanel();
    });
    await test.step(`close the interaction panel`, async () => {
      await lobbyPage.closeInteractionPanel();
    });
  });
});
