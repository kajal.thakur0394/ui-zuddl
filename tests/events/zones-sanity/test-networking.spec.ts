import {
  APIRequestContext,
  BrowserContext,
  Page,
  test,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import EventEntryType from "../../../enums/eventEntryEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { LobbyPage } from "../../../page-objects/events-pages/zones/LobbyPage";
import { NetworkingPage } from "../../../page-objects/events-pages/zones/NetworkingPage";
import {
  createNewEvent,
  inviteAttendeeByAPI,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import ZoneType from "../../../enums/ZoneTypeEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@zone-sanity @networking`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  let eventTitle: string;
  let eventId;
  let attendeeEmail: string;
  let lobbyPage: LobbyPage;
  let networkingPage: NetworkingPage;
  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiRequestContext = await orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiRequestContext,
      event_title: eventTitle,
    });
    const eventController = new EventController(orgApiRequestContext, eventId);
    await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
      false
    );
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await inviteAttendeeByAPI(
      orgApiRequestContext,
      eventId,
      attendeeEmail,
      false
    );
    await updateEventLandingPageDetails(orgApiRequestContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`@attendee lands on networking page, verify essential components`, async ({
    page,
  }) => {
    const magicLink = await QueryUtil.fetchMagicLinkFromDB(
      eventId,
      attendeeEmail
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(magicLink);
    await landingPageOne.clickOnEnterNowButton();
    lobbyPage = await landingPageOne.isLobbyLoaded();
    networkingPage = (await (
      await lobbyPage.getTopNavBarComponent()
    ).switchToZone(ZoneType.NETWORKING)) as NetworkingPage;
    await test.step(`verify presence of recommended profile section`, async () => {
      await expect(networkingPage.recommendedProfileContainer).toBeVisible();
    });
    await test.step(`verify presence of start networking button`, async () => {
      await expect(networkingPage.startNetworkingButton).toBeVisible();
    });
    await test.step(`verify presence of browse all attendees section`, async () => {
      await expect(networkingPage.browseAllAttendeeSection).toBeVisible();
    });
    await test.step(`verify presence of your connection section`, async () => {
      await expect(networkingPage.yourConnectionsSection).toBeVisible();
    });
  });
});
