import {
  APIRequestContext,
  BrowserContext,
  Page,
  test,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import EventEntryType from "../../../enums/eventEntryEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { ExpoPage } from "../../../page-objects/events-pages/zones/ExpoPage";
import { LobbyPage } from "../../../page-objects/events-pages/zones/LobbyPage";
import {
  createNewEvent,
  inviteAttendeeByAPI,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import ZoneType from "../../../enums/ZoneTypeEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@zone-sanity @expo`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  let eventTitle: string;
  let eventId;
  let attendeeEmail: string;
  let attendeePage: Page;
  let lobbyPage: LobbyPage;
  let expoPage: ExpoPage;
  test.beforeEach(async ({ context }) => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiRequestContext = await orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiRequestContext,
      event_title: eventTitle,
    });
    const eventController = new EventController(orgApiRequestContext, eventId);
    await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
      false
    );
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await inviteAttendeeByAPI(
      orgApiRequestContext,
      eventId,
      attendeeEmail,
      false
    );
    await updateEventLandingPageDetails(orgApiRequestContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });
    attendeePage = await context.newPage();
    const magicLink = await QueryUtil.fetchMagicLinkFromDB(
      eventId,
      attendeeEmail
    );
    let landingPageOne = new LandingPageOne(attendeePage);
    await landingPageOne.load(magicLink);
    await landingPageOne.clickOnEnterNowButton();
    lobbyPage = await landingPageOne.isLobbyLoaded();
    expoPage = (await (
      await lobbyPage.getTopNavBarComponent()
    ).switchToZone(ZoneType.EXPO)) as ExpoPage;
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`@attendee lands on expo, verify empty state`, async () => {
    const expectedHeadingOfExpo = "We're glad to have you here";
    const expectedTextForNoOpenExpos = "Expos are not open yet";
    await test.step(`Empty state validation of expo zone`, async () => {
      await expect(
        expoPage.emptyStateExpoHeading,
        `expecting Empty state Expo heading to have text ${expectedHeadingOfExpo}`
      ).toContainText(expectedHeadingOfExpo);
      await expect(
        expoPage.noExpoOpenMessage,
        `expecting Empty state Expo heading to have text ${expectedTextForNoOpenExpos}`
      ).toContainText(expectedTextForNoOpenExpos);
    });
  });
});
