import {
  APIRequestContext,
  BrowserContext,
  Page,
  test,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import EventEntryType from "../../../enums/eventEntryEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { LobbyPage } from "../../../page-objects/events-pages/zones/LobbyPage";
import { RoomsListingPage } from "../../../page-objects/events-pages/zones/RoomsPage";

import {
  createNewEvent,
  inviteAttendeeByAPI,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { AVModal } from "../../../page-objects/events-pages/live-side-components/AvModal";
import ZoneType from "../../../enums/ZoneTypeEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel(`@zone-sanity @rooms`, async () => {
  test.describe.configure({ retries: 2 });

  let orgBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  let eventTitle: string;
  let eventId;
  let attendeeEmail: string;
  let lobbyPage: LobbyPage;
  let roomsListingPage: RoomsListingPage;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiRequestContext = await orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiRequestContext,
      event_title: eventTitle,
    });
    const eventController = new EventController(orgApiRequestContext, eventId);
    await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
      false
    );
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await inviteAttendeeByAPI(
      orgApiRequestContext,
      eventId,
      attendeeEmail,
      false
    );
    await updateEventLandingPageDetails(orgApiRequestContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`TC001: @attendee lands on rooms, verify empty state`, async ({
    page,
  }) => {
    const magicLink = await QueryUtil.fetchMagicLinkFromDB(
      eventId,
      attendeeEmail
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(magicLink);
    await landingPageOne.clickOnEnterNowButton();
    lobbyPage = await landingPageOne.isLobbyLoaded();
    const expectedHeadingOfRooms = "We're glad to have you here";
    const expectedTextForNoOpenRooms = "Rooms are not open yet";
    await test.step(`attendee clicks on room tab to switch to rooms zone`, async () => {
      roomsListingPage = (await (
        await lobbyPage.getTopNavBarComponent()
      ).switchToZone(ZoneType.ROOMS)) as RoomsListingPage;
    });
    await test.step(`empty state validation of rooms zone`, async () => {
      await expect(
        roomsListingPage.emptyStateRoomsHeading,
        `expecting empty state rooms heading to have text ${expectedHeadingOfRooms}`
      ).toContainText(expectedHeadingOfRooms);
      await expect(
        roomsListingPage.noRoomsOpenMessage,
        `expecting empty state schedule heading to have text ${expectedTextForNoOpenRooms}`
      ).toContainText(expectedTextForNoOpenRooms);
    });
  });

  test(`TC002: @attendee if one room present, clicking on room zone should open AV pop up to enter inside the room`, async ({
    page,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-93/in-case-of-one-room-present-clicking-on-room-zone-should-open-av-pop",
    });
    const magicLink = await QueryUtil.fetchMagicLinkFromDB(
      eventId,
      attendeeEmail
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(magicLink);
    await landingPageOne.clickOnEnterNowButton();
    lobbyPage = await landingPageOne.isLobbyLoaded();
    const roomsToAdd = 1;
    await test.step(`Adding 1 public rooms to the event`, async () => {
      const roomsController = new EventController(orgApiRequestContext, eventId)
        .getRoomsController;
      await roomsController.addRoomsToEvent({
        numberOfRooms: roomsToAdd,
        seatsPerRoom: 1,
        roomCategory: "PUBLIC",
      });
    });
    await test.step(`attendee clicks on room tab to switch to rooms zone`, async () => {
      await page.reload();
      roomsListingPage = (await (
        await lobbyPage.getTopNavBarComponent()
      ).switchToZone(ZoneType.ROOMS)) as RoomsListingPage;
    });
    await test.step(`verify presence of AV popup asking permission to enter inside the room`, async () => {
      let avModal = new AVModal(page);
      await avModal.isAvModalVisible();
    });
  });

  test(`TC003: @attendee verify presence of rooms in room listing page`, async ({
    page,
  }) => {
    const magicLink = await QueryUtil.fetchMagicLinkFromDB(
      eventId,
      attendeeEmail
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(magicLink);
    await landingPageOne.clickOnEnterNowButton();
    lobbyPage = await landingPageOne.isLobbyLoaded();
    const roomsToAdd = 2;
    await test.step(`Adding 2 public rooms to the event`, async () => {
      const roomsController = new EventController(orgApiRequestContext, eventId)
        .getRoomsController;
      await roomsController.addRoomsToEvent({
        numberOfRooms: roomsToAdd,
        seatsPerRoom: 2,
        roomCategory: "PUBLIC",
      });
    });
    await test.step(`attendee clicks on room tab to switch to rooms zone`, async () => {
      await page.reload();
      roomsListingPage = (await (
        await lobbyPage.getTopNavBarComponent()
      ).switchToZone(ZoneType.ROOMS)) as RoomsListingPage;
    });
    await test.step(`verify presence of 2 rooms in room listing page`, async () => {
      await expect(
        roomsListingPage.roomsLocator,
        `expecting rooms count to be ${roomsToAdd} on the rooms listing page`
      ).toHaveCount(2, { timeout: 20000 });
    });
  });
});
