import {
  APIRequestContext,
  BrowserContext,
  Page,
  test,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { ExpoPage } from "../../../page-objects/events-pages/zones/ExpoPage";
import { LobbyPage } from "../../../page-objects/events-pages/zones/LobbyPage";
import {
  inviteAttendeeByAPI,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import ZoneType from "../../../enums/ZoneTypeEnum";
import BrowserName from "../../../enums/BrowserEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@zone-sanity @expo`, async () => {
  if (process.env.run_env !== "pre-prod") {
    return;
  }
    test.describe.configure({ retries: 2 });
    let orgBrowserContext: BrowserContext;
    let orgApiRequestContext: APIRequestContext;
    let eventTitle: string;
    let eventId;
    let attendeeEmail: string;
    let attendeePage: Page;
    let landingPageOne: LandingPageOne;
    let lobbyPage: LobbyPage;
    let eventInfoDto: EventInfoDTO;
    let attendeeContext: BrowserContext;
    let expoPage: ExpoPage;
    let lobbyPageUrl: string;

    test.beforeAll(async () => {
      test.setTimeout(600000); //10 min timeout for this suite
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: true,
      });
      orgApiRequestContext = await orgBrowserContext.request;
      eventTitle = Date.now().toString() + "_automationevent";
      eventId =
        DataUtl.getApplicationTestDataObj()["eventIdForExpoWidgetTesting"];
      const eventController = new EventController(
        orgApiRequestContext,
        eventId
      );
      await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
        false
      );

      // update the event start and end time
      await eventController.changeEventStartAndEndDateTime({
        deltaByHoursInStartTime: -1,
        deltaByHoursInEndTime: 3,
      });

      await updateEventLandingPageDetails(orgApiRequestContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
      });
      eventInfoDto = new EventInfoDTO(
        eventId,
        "lobby-widgets-donottouch",
        true
      ); // true to denote magic link enabled
      attendeeContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
        browserName: BrowserName.CHROME,
      });
      attendeeEmail = DataUtl.getRandomAttendeeEmail();
      await inviteAttendeeByAPI(
        orgApiRequestContext,
        eventId,
        attendeeEmail,
        false
      );
      attendeePage = await attendeeContext.newPage();
      const magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      landingPageOne = new LandingPageOne(attendeePage);
      await landingPageOne.load(magicLink);
      // await attendeePage.waitForLoadState("networkidle");
      await landingPageOne.clickOnEnterNowButton();
      lobbyPageUrl = landingPageOne.page.url();
      lobbyPage = await landingPageOne.isLobbyLoaded();
      console.log(lobbyPageUrl);
      await lobbyPage.handleEditProfilePrompt(true);
      await lobbyPage.handleTourPrompt(true);
    });

    test.beforeEach(async ({}, testInfo) => {
      expoPage = (await (
        await lobbyPage.getTopNavBarComponent()
      ).switchToZone(ZoneType.EXPO)) as ExpoPage;
      await test.step(`Title validation of expo zone`, async () => {
        const expectedTitleExpo = "Expo-widgets";
        await expect(
          expoPage.expoTitle,
          `expecting Empty state Expo heading to have text ${expectedTitleExpo}`
        ).toContainText(expectedTitleExpo, { timeout: 20000 });
      });
      await test.step(`Entering into expo booths`, async () => {
        let boothname = "Booth1";
        if (testInfo.title.includes("immersive")) {
          boothname = "Booth 2";
        }
        console.log(
          "The title of test is:",
          testInfo.title,
          "Boothname is: ",
          boothname
        );
        await expoPage.enterZone("Expo-widgets");
        await expoPage.enterBooth(boothname);
      });
    });

    test.afterAll(async () => {
      await orgBrowserContext?.close();
    });

    test(`TC001: @attendee lands on expo, verify coupon code widgets`, async () => {
      // Verify Coupon code widget
      await test.step(`Validating  Coupon code widget`, async () => {
        let couponCode = "Code";
        let couponExpiryDate = "Expires : 24th Mar 2023";
        await expoPage.verifyCouponCodeWidget(couponCode, couponExpiryDate);
      });
    });

    test(`TC002: @attendee lands on expo, verify text widgets`, async () => {
      // Validating text  widget
      await test.step(`Validating text widget`, async () => {
        let textToVerify =
          "Some text tha is going to be displayed and validated using automation. Thanks for reading.";
        console.log("textToVerify:", textToVerify);
        await expoPage.verifyTextWidget(textToVerify);
      });
    });

    test(`TC003: @attendee lands on expo, verify file download widgets`, async () => {
      // Verify File download widget
      await test.step(`Validating  File download widget`, async () => {
        let redirectedHref =
          "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf";
        await expoPage.verifyFileDownloadWidget(redirectedHref);
      });
    });

    test(`TC004: @attendee lands on expo, verify iframe widgets`, async () => {
      // Verify iframe widget
      await test.step(`Validating  Iframe widget`, async () => {
        let srcIframe = "https://en.wikipedia.org/wiki/Main_Page";
        await expoPage.verifyIframeWidget(srcIframe);
      });
    });

    test(`TC005: @attendee lands on expo, verify Hotspot widgets`, async () => {
      // Validating Hotspot button
      await test.step(`Validating  Hotspot widget`, async () => {
        let redirectedHref = `${
          DataUtl.getApplicationTestDataObj()["baseUrl"]
        }/l/event/${eventId}/lobby`;
        await expoPage.verifyHotspotWidget(redirectedHref);
      });
    });

    test(`TC006: @attendee lands on expo, verify contact us widgets`, async () => {
      // Validating Contact us widget
      await test.step(`Validating Contact us widget`, async () => {
        let contactText = "Ctct- mkt@zuddl.com";
        await expoPage.verifyContactusWidget(contactText);
      });
    });

    test(`TC007: @attendee lands on expo, verify Go to widgets`, async () => {
      // Validating Go to button
      await test.step(`Validating Go to widget`, async () => {
        let redirectedHref = `${
          DataUtl.getApplicationTestDataObj()["baseUrl"]
        }/l/event/${eventId}/networking`;
        await expoPage.verifyGoToWidget(redirectedHref);
      });
    });

    test(`TC008: @attendee lands on expo, verify tciker widgets`, async () => {
      // Ticker widget
      await test.step(`Validating Ticker widget and its content`, async () => {
        let tickercontent =
          "This is ticker widget to test how it works and whether it works fine.";
        await expoPage.verifyTickerWidget(tickercontent);
      });
    });

    test(`TC009: @attendee lands on expo, verify External image carousel widgets`, async () => {
      //Verifying External image carousel
      await test.step(`Validating External Image Carousel widget`, async () => {
        let redirectedHref = `https://www.youtube.com/`;
        await expoPage.verifyExternalImageCarouselWidget(1, redirectedHref);
      });
    });

    test(`TC010: @attendee lands on expo, verify Internal image carousel widgets`, async () => {
      // Verifying Internal Image carousel
      await test.step(`Validating Internal Image Carousel widget`, async () => {
        let redirectedHref = `${
          DataUtl.getApplicationTestDataObj()["baseUrl"]
        }/l/event/${eventId}/expo`;
        await expoPage.verifyInternalImageCarouselWidget(redirectedHref, 0);
      });
    });

    test(`TC011: @attendee lands on expo, verify Video widgets`, async () => {
      // Validating Video widget
      await test.step(`Validating Video widget`, async () => {
        await expoPage.verifyVideoWidget();
      });
    });

    test(`TC012: @attendee lands on expo, verify Image widgets`, async () => {
      // example.spec.ts

      // validating Image widgets
      await test.step(`Validating Image widget`, async () => {
        let imageAlt = "uploadedImage";
        let redirectedHref = "https://www.google.com/";
        await expoPage.verifyImageWidget(imageAlt, redirectedHref);
      });
    });

    test(`TC013: @attendee lands on expo, verify immersive text widgets`, async () => {
      test.info().annotations.push({
        type: "P0",
        description:
          "https://linear.app/zuddl/issue/QAT-137/issue-with-text-widget-p0",
      });
      // Validating immersive text  widget
      await test.step(`Validating text widget`, async () => {
        let textToVerify =
          "Some text tha is going to be displayed and validated using automation. Thanks for reading.";
        console.log("textToVerify:", textToVerify);
        await expoPage.verifyTextWidget(textToVerify);
      });
    });
});
