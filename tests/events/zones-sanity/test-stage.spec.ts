import {
  APIRequestContext,
  BrowserContext,
  Page,
  test,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import EventEntryType from "../../../enums/eventEntryEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { LobbyPage } from "../../../page-objects/events-pages/zones/LobbyPage";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import {
  createNewEvent,
  inviteAttendeeByAPI,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import ZoneType from "../../../enums/ZoneTypeEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@zone-sanity @stage`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  let eventTitle: string;
  let eventId;
  let attendeeEmail: string;
  let attendeePage: Page;
  let lobbyPage: LobbyPage;
  let stagePage: StagePage;
  test.beforeEach(async ({ context }) => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiRequestContext = await orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiRequestContext,
      event_title: eventTitle,
    });
    const eventController = new EventController(orgApiRequestContext, eventId);
    await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
      false
    );
    attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await inviteAttendeeByAPI(
      orgApiRequestContext,
      eventId,
      attendeeEmail,
      false
    );
    await updateEventLandingPageDetails(orgApiRequestContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });
    attendeePage = await context.newPage();
    const magicLink = await QueryUtil.fetchMagicLinkFromDB(
      eventId,
      attendeeEmail
    );
    let landingPageOne = new LandingPageOne(attendeePage);
    await landingPageOne.load(magicLink);
    await landingPageOne.clickOnEnterNowButton();
    lobbyPage = await landingPageOne.isLobbyLoaded();
    stagePage = (await (
      await lobbyPage.getTopNavBarComponent()
    ).switchToZone(ZoneType.STAGE)) as StagePage;
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`@attendee lands on stage, verify empty state`, async () => {
    const expectedHeadingOfStage = "Thank you for joining";
    await test.step(`empty state validation of schedule zone`, async () => {
      await expect(
        stagePage.emptyStageHeading,
        `expecting empty state stage heading to have text ${expectedHeadingOfStage}`
      ).toContainText(expectedHeadingOfStage);
    });
    await test.step(`verify the interaction panel is open by default`, async () => {
      await expect(
        lobbyPage.chatTabOnOpenSidePanel,
        "expecting chat tab to be visible on open state of interaction panel"
      ).toBeVisible();
    });
  });
});
