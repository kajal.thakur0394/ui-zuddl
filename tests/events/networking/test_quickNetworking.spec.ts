import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { AVModal } from "../../../page-objects/events-pages/live-side-components/AvModal";
import { RoomModule } from "../../../page-objects/events-pages/site-modules/Rooms";
import {
  createNewEvent,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@networking @quick-networking  @AV`, async () => {
  let eventId;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let attendeeOneBrowserContext: BrowserContext;
  let attendeeTwoBrowserContext: BrowserContext;
  let eventController: EventController;
  let networkingPageUrl: string;
  let attendeeLandingPage: string;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = orgBrowserContext.request;
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: "room automation",
    });
    attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    attendeeTwoBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });

    eventController = new EventController(orgApiContext, eventId);

    let eventInfoDto = new EventInfoDTO(eventId, "room automation", true);
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
    networkingPageUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${eventId}/networking`;
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
    await attendeeOneBrowserContext?.close();
    await attendeeTwoBrowserContext?.close();
  });

  test("Organiser and attendee start netowrking and wait for match", async () => {
    /**
     * Might fail on staging due to DNS cache issue, known issue
     * If it fails on staging then better to skip it based on which env we are running
     */
    let attendeeOnePage = await attendeeOneBrowserContext.newPage();
    let organiserPage = await orgBrowserContext.newPage();

    const attendeeOneFirstName = "prateekAttendeeOne";
    const attendeeOneLastName = "QA";

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

    const organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];
    ("Automation");

    let attendeeOneRoom: RoomModule;
    let attendeeTwoRoom: RoomModule;
    let organiserRoom: RoomModule;

    let attendeeEmailOne = DataUtl.getRandomAttendeeEmail();
    let attendeeEmailTwo = DataUtl.getRandomAttendeeEmail();

    await test.step(`organiser invites attendee  1 to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeOneFirstName,
        lastName: attendeeOneLastName,
        phoneNumber: "1234567899",
        email: attendeeEmailOne,
      });
    });

    await eventController.disableOnboardingChecksForAttendee();
    await test.step(`attendee 1 logs into the event and go to networking page`, async () => {
      let magicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmailOne
      );
      await attendeeOnePage.goto(magicLink);
      await attendeeOnePage.click(`text=Enter Now`);
      await attendeeOnePage.goto(networkingPageUrl);

      // click on start netwroking button
      await attendeeOnePage.click("text=Start Networking");
      // wait for av modal to load and click on start networking on av modal
      const attendeeOneAvModal = new AVModal(attendeeOnePage);
      await attendeeOneAvModal.isVideoStreamLoadedOnAVModal();
      await attendeeOneAvModal.clickOnJoinButtonOnAvModal();
      await expect(
        attendeeOnePage.locator("video.agora_video_player")
      ).toHaveCount(1);
    });

    await test.step(`organiser logs into the event and go to networking page`, async () => {
      await organiserPage.goto(attendeeLandingPage);
      await organiserPage.click(`text=Enter Now`);
      await organiserPage.goto(networkingPageUrl);

      // click on start netwroking button
      await organiserPage.click("text=Start Networking");
      // wait for av modal to load and click on start networking on av modal
      const organiserAvModal = new AVModal(organiserPage);
      await organiserAvModal.isVideoStreamLoadedOnAVModal();
      await organiserAvModal.clickOnJoinButtonOnAvModal();
      await expect(
        organiserPage.locator("video.agora_video_player")
      ).toHaveCount(1);
    });

    await test.step(`verify organiser sees attendee 1 and his own stream inside networking session`, async () => {
      await expect(
        attendeeOnePage.locator("video.agora_video_player")
      ).toHaveCount(2, { timeout: 50000 });
    });
    await test.step(`verify attendee  sees organiser and his own stream inside networking session`, async () => {
      await expect(
        organiserPage.locator("video.agora_video_player")
      ).toHaveCount(2, { timeout: 50000 });
    });
    await test.step(`attendee 1 mutes his video and audio`, async () => {});
    await test.step(`verify organiser gets a message to nudge attendee 1 to turn on his video`, async () => {});
    await test.step(`attendee 1 unmutes his video and audio`, async () => {});
    await test.step(`verify organiser now sees attendee 1 video and his own video as well`, async () => {});
    await test.step(`now attendee exits the networking session`, async () => {
      await attendeeOnePage
        .locator("div[data-testid='exit-networking']")
        .click();
      await attendeeOnePage.getByRole("button", { name: "Yes, Leave" }).click();
      await attendeeOnePage.waitForURL(networkingPageUrl);
    });
    await test.step(`now organiser exits the networking session`, async () => {
      await organiserPage.locator("div[data-testid='exit-networking']").click();
      await organiserPage.getByRole("button", { name: "Yes, Leave" }).click();
      await organiserPage.waitForURL(networkingPageUrl);
    });

    await test.step(`verify attendee 1 is able to see his matched connection name in Your Connection section`, async () => {
      // go to your connection section
      const profileCardsUnderYourConnection = attendeeOnePage.locator(
        "div[class^='styles-module__orderThreeWrapper'] div[class*='styles-module__profileCard']"
      );
      await expect(
        profileCardsUnderYourConnection,
        "expecting 1 connection to appear for attendee in your connection section"
      ).toHaveCount(1);
    });

    await test.step(`verify organiser is able to see his matched connection name in Your Connection section`, async () => {
      // go to your connection section
      const profileCardsUnderYourConnection = organiserPage.locator(
        "div[class^='styles-module__orderThreeWrapper'] div[class*='styles-module__profileCard']"
      );
      await expect(
        profileCardsUnderYourConnection,
        "expecting 1 connection to appear for organiser in your connection section"
      ).toHaveCount(1);
    });

    await test.step(`verify attendee sees that 1 user has matched with you on matchers wrapper`, async () => {
      const matchersWrapper = attendeeOnePage.locator(
        "div[class^='styles-module__matchesWrapper']"
      );
      await expect(matchersWrapper).toContainText("1 Attendee");
    });
    await test.step(`verify organiser sees that 1 user has matched with you on matchers wrapper`, async () => {
      const matchersWrapper = organiserPage.locator(
        "div[class^='styles-module__matchesWrapper']"
      );
      await expect(matchersWrapper).toContainText("1 Attendee");
    });
  });
});
