import {
  APIRequestContext,
  BrowserContext,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import EventRole from "../../../enums/eventRoleEnum";
import { DudaLandingPage } from "../../../page-objects/events-pages/duda-website/dudaLandingPage";
import { RegistrationWidgetIframe } from "../../../page-objects/events-pages/duda-website/registrationIframe";
import { TopNavBarComponent } from "../../../page-objects/events-pages/live-side-components/TopNavBarComponent";
import { AdvancedSettingsPage } from "../../../page-objects/new-org-side-pages/AdvancedTabComponent";
import { SpeakerData } from "../../../test-data/speakerData";
import {
  deleteSpeakerFromEvent,
  inviteSpeakerByApi,
  updateEventLandingPageDetails,
  deleteAllSpeakers,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel(`@duda @registration @eventover @speaker`, async () => {
  test.skip(
    process.env.run_env === "staging",
    "Skipping duda registration tests on staging as it can only run on preprod"
  );
  test.describe.configure({ retries: 2 });
  let dudaPublishedAttendeeLandingPage: string;
  let dudaPublishedSpeakerLandingPage: string;
  let dudaPublishedEventId;
  let orgBrowserContext: BrowserContext;
  let organiserApiContext: APIRequestContext;
  let eventController: EventController;
  let speakerEmail: string;
  let speakerDeleted: boolean = false;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    organiserApiContext = orgBrowserContext.request;
    dudaPublishedEventId =
      DataUtl.getApplicationTestDataObj()[
        "dudaPublishedEventIdForSpeakerPageTesting"
      ];
    dudaPublishedAttendeeLandingPage =
      DataUtl.getApplicationTestDataObj()[
        "duaPublishedTemplateForSpeakerRegistration"
      ];
    dudaPublishedSpeakerLandingPage = `${dudaPublishedAttendeeLandingPage}?r=s`;
    eventController = new EventController(
      organiserApiContext,
      dudaPublishedEventId
    );
    // update the event start and end time
    await eventController.changeEventStartAndEndDateTime({
      deltaByHoursInStartTime: 1,
      deltaByHoursInEndTime: 3,
    });
  });

  test.afterEach(async () => {
    if (!speakerDeleted) {
      await deleteSpeakerFromEvent(
        organiserApiContext,
        dudaPublishedEventId,
        speakerEmail
      );
    }
    await orgBrowserContext?.close();
  });

  test(`TC001: @speaker get invited, gets magic link and open magic link and lands on stage`, async ({
    page,
  }) => {
    /**
     * If speaker opens magic link in same tab
     * expected behaviour is that, he wont have to click on enter now button
     * he will directly land on stage
     */
    let speakerMagicLink: string;
    await test.step(`Updating landing page details to make event Reg Based and Enabling speaker magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isSpeakermagicLinkEnabled: true,
        }
      );
    });
    // Creating Speaker Data and inviting speaker to event
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", false);
    let speakerUser = new SpeakerData(
      DataUtl.getRandomAttendeeEmail(),
      "Prateek"
    );
    speakerEmail = speakerUser.speakerEmail;
    await test.step(`Inviting speaker to the event`, async () => {
      console.log(`Speaker data to add is ${speakerUser}`);
      await inviteSpeakerByApi(
        organiserApiContext,
        speakerUser.getSpeakerDataObject(),
        eventInfoDto.eventId,
        true
      ); // true denote to send email
    });

    await test.step(`fetching the speaker magic link`, async () => {
      speakerMagicLink = await QueryUtil.fetchDudaMagicLinkForSpeakerFromDB(
        dudaPublishedEventId,
        speakerUser.speakerEmail,
        dudaPublishedSpeakerLandingPage
      );
    });

    await test.step(`Navigating using speaker magic link and verify user sees enter now button and lands on stage after entering event`, async () => {
      let landingPageOne = new DudaLandingPage(page);
      await landingPageOne.load(speakerMagicLink);
      // await landingPageOne.clickOnEnterNowButton();
      await landingPageOne.isStageLoaded();
    });
  });

  test(`TC002: Uninvited user gets restricted access page`, async ({
    page,
  }) => {
    /**
     * If speaker opens magic link in same tab
     * expected behaviour is that, he wont have to click on enter now button
     * he will directly land on stage
     */
    let dudaSpeakerLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;
    await test.step(`Updating landing page details to make event Reg Based and Enabling speaker magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isSpeakermagicLinkEnabled: true,
        }
      );
    });
    // Creating Speaker Data
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", false);
    let speakerUser = new SpeakerData(
      DataUtl.getRandomAttendeeEmail(),
      "Prateek"
    );
    speakerDeleted = true; // speaker was never added
    speakerEmail = speakerUser.speakerEmail;

    let userInfoDto = new UserInfoDTO(
      speakerUser.speakerEmail,
      "",
      "Prateek",
      "Automation",
      true
    );
    let userEventRoleDto = new UserEventRoleDto(
      userInfoDto,
      eventInfoDto,
      EventRole.SPEAKER
    );

    await test.step(`Loading the duda speaker landing page and click on register button to get registration iframe`, async () => {
      await page.goto(dudaPublishedSpeakerLandingPage);
      dudaSpeakerLandingPage = new DudaLandingPage(page);
      registrationWidgetIframe =
        await dudaSpeakerLandingPage.clickOnRegisterButton();
    });

    await test.step(`user try to login with otp, he should be redirected to registration form again`, async () => {
      await expect(
        registrationWidgetIframe.getLoginOptionsComponent.emailInputBox,
        "Expecting email input box to be visible"
      ).toBeVisible({ timeout: 15000 });
      await registrationWidgetIframe.getLoginOptionsComponent.emailInputBox.fill(
        speakerUser.speakerEmail
      );
      await registrationWidgetIframe.getLoginOptionsComponent.submitButton.click();
      await dudaSpeakerLandingPage.getRestrictedSpeakerAccessPage.isVisible();
    });
  });

  test(`TC003: @speaker logs out and lands on speaker landing page`, async ({
    page,
  }) => {
    /**
     * If speaker opens magic link in same tab
     * expected behaviour is that, he wont have to click on enter now button
     * he will directly land on stage
     */
    let speakerMagicLink: string;
    await test.step(`Updating landing page details to make event Reg Based and Enabling speaker magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
          isSpeakermagicLinkEnabled: true,
        }
      );
    });
    // Creating Speaker Data and inviting speaker to event
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", false);
    let speakerUser = new SpeakerData(
      DataUtl.getRandomAttendeeEmail(),
      "Prateek"
    );
    speakerEmail = speakerUser.speakerEmail;

    // Disabling Onboarding checks for speaker
    await test.step(`Disabling Onboarding Checks`, async () => {
      await new EventController(
        organiserApiContext,
        dudaPublishedEventId
      ).getAccessGroupController.handleOnboardingChecksForSpeaker(false);
    });
    await test.step(`Inviting speaker to the event`, async () => {
      console.log(`Speaker data to add is ${speakerUser}`);
      await inviteSpeakerByApi(
        organiserApiContext,
        speakerUser.getSpeakerDataObject(),
        eventInfoDto.eventId,
        true
      ); // true denote to send email
    });

    await test.step(`fetching the speaker magic link`, async () => {
      speakerMagicLink = await QueryUtil.fetchDudaMagicLinkForSpeakerFromDB(
        dudaPublishedEventId,
        speakerUser.speakerEmail,
        dudaPublishedSpeakerLandingPage
      );
    });

    await test.step(`Navigating using speaker magic link and verify user sees enter now button and lands on stage after entering event`, async () => {
      let landingPageOne = new DudaLandingPage(page);
      await landingPageOne.load(speakerMagicLink);
      await landingPageOne.isStageLoaded();
    });

    await test.step(`User logs out of the event and lands on speaker landing page`, async () => {
      await page.locator("button:has-text('VIEW AS ATTENDEE')").click();
      await new TopNavBarComponent(page).logoutFromEvent(false);
      await page.waitForURL(dudaPublishedSpeakerLandingPage);
    });
  });

  test.fixme(
    `TC004: Email domain restriction has no impact on speaker landing page`,
    async ({ page }) => {
      /**
       * If speaker opens magic link in same tab
       * expected behaviour is that, he wont have to click on enter now button
       * he will directly land on stage
       */
      await test.step(`Updating landing page details to make event Reg Based and Enabling speaker magic link`, async () => {
        await updateEventLandingPageDetails(
          organiserApiContext,
          dudaPublishedEventId,
          {
            eventEntryType: EventEntryType.REG_BASED,
            isSpeakermagicLinkEnabled: true,
          }
        );
      });
      // Creating Speaker Data and inviting speaker to event
      const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", false);
      let speakerUser = new SpeakerData(
        DataUtl.getRandomAttendeeEmail(),
        "Prateek"
      );
      // Disabling Onboarding checks for speaker
      await test.step(`Disabling Onboarding Checks`, async () => {
        await new EventController(
          organiserApiContext,
          dudaPublishedEventId
        ).getAccessGroupController.handleOnboardingChecksForSpeaker(false);
      });
      await test.step(`Inviting speaker to the event`, async () => {
        console.log(`Speaker data to add is ${speakerUser}`);
        await inviteSpeakerByApi(
          organiserApiContext,
          speakerUser.getSpeakerDataObject(),
          eventInfoDto.eventId,
          true
        ); // true denote to send email
      });

      let baseUrl = DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl;
      let advacnedSettingsUrl = `${baseUrl}/event/${dudaPublishedEventId}/advanced-settings`;
      let organiserPage = await orgBrowserContext.newPage();
      let advancedSettingsPage = new AdvancedSettingsPage(organiserPage);
      await organiserPage.goto(advacnedSettingsUrl);
      let emailRestrictionComp =
        await advancedSettingsPage.getEmailDomainRestrictionComponent();
    }
  );

  test(`TC005: @speaker get invited, log in using OTP`, async ({ page }) => {
    /**
     * If speaker opens magic link in same tab
     * expected behaviour is that, he wont have to click on enter now button
     * he will directly land on stage
     */
    let dudaSpeakerLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;

    await test.step(`Updating landing page details to make event Reg Based and Disabling speaker magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isSpeakermagicLinkEnabled: false,
        }
      );
    });
    // Creating Speaker Data and inviting speaker to event
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", false);
    let speakerUser = new SpeakerData(
      DataUtl.getRandomAttendeeEmail(),
      "Prateek"
    );
    speakerEmail = speakerUser.speakerEmail;

    await test.step(`Inviting speaker to the event`, async () => {
      console.log(`Speaker data to add is ${speakerUser}`);
      await inviteSpeakerByApi(
        organiserApiContext,
        speakerUser.getSpeakerDataObject(),
        eventInfoDto.eventId,
        true
      ); // true denote to send email
    });

    await test.step(`Loading the duda speaker landing page and click on register button to get registration iframe`, async () => {
      await page.goto(dudaPublishedSpeakerLandingPage);
      dudaSpeakerLandingPage = new DudaLandingPage(page);
      registrationWidgetIframe =
        await dudaSpeakerLandingPage.clickOnRegisterButton();
    });

    await test.step(`user log in using OTP`, async () => {
      await expect(
        registrationWidgetIframe.getLoginOptionsComponent.emailInputBox,
        "expecting email input box to be visible"
      ).toBeVisible();
      await registrationWidgetIframe.getLoginOptionsComponent.emailInputBox.fill(
        speakerUser.speakerEmail
      );
      await test.step(`should see submit button to trigger otp, click on it`, async () => {
        await registrationWidgetIframe.getLoginOptionsComponent.submitButton.click();
      });
      await test.step(`get the otp from db and enter into the otp box`, async () => {
        const otpFetched = await QueryUtil.fetchLoginOTPForEvent(
          speakerUser.speakerEmail,
          dudaPublishedEventId
        );
        console.log(`otp fetched is ${otpFetched}`);
        await registrationWidgetIframe.getLoginOptionsComponent.enterAndSubmitOtp(
          otpFetched
        );
        await registrationWidgetIframe.clickOnEnterNowButton();
        await dudaSpeakerLandingPage.isStageLoaded();
      });
    });
  });

  test(`TC006: @speaker get invited, log in using Password`, async ({
    page,
  }) => {
    /**
     * If speaker opens magic link in same tab
     * expected behaviour is that, he wont have to click on enter now button
     * he will directly land on stage
     */
    let dudaSpeakerLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;

    await test.step(`Updating landing page details to make event Reg Based and Disabling speaker magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isSpeakermagicLinkEnabled: false,
        }
      );
    });
    // Creating Speaker Data and inviting speaker to event
    const existingSpeakerEmail =
      DataUtl.getApplicationTestDataObj().attendeeEmailWithAccountEntry;
    const existingSpeakerPassword =
      DataUtl.getApplicationTestDataObj().attendeePasswordWithAccountEntry;
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", false);
    let speakerUser = new SpeakerData(existingSpeakerEmail, "Prateek");
    speakerEmail = speakerUser.speakerEmail;

    await test.step(`Inviting speaker to the event`, async () => {
      console.log(`Speaker data to add is ${speakerUser}`);
      await inviteSpeakerByApi(
        organiserApiContext,
        speakerUser.getSpeakerDataObject(),
        eventInfoDto.eventId,
        true
      ); // true denote to send email
    });

    await test.step(`Loading the duda speaker landing page and click on register button to get registration iframe`, async () => {
      await page.goto(dudaPublishedSpeakerLandingPage);
      dudaSpeakerLandingPage = new DudaLandingPage(page);
      registrationWidgetIframe =
        await dudaSpeakerLandingPage.clickOnRegisterButton();
    });

    await test.step(`user log in using password`, async () => {
      await expect(
        registrationWidgetIframe.getLoginOptionsComponent
          .loginViaPasswordButton,
        "expecting login via password button to be visible"
      ).toBeVisible({ timeout: 15000 });
      await registrationWidgetIframe.getLoginOptionsComponent.loginViaPasswordButton.click();
      await expect(
        registrationWidgetIframe.getLoginOptionsComponent
          .loginViaPasswordButton,
        "expecting it to move to take password screen"
      ).not.toBeVisible();

      await registrationWidgetIframe.getLoginOptionsComponent.emailInputBox.fill(
        speakerUser.speakerEmail
      );
      await registrationWidgetIframe.getLoginOptionsComponent.signInButton.click();
      await test.step(`Filling the  password`, async () => {
        await registrationWidgetIframe.getLoginOptionsComponent.enterPasswordBox.fill(
          existingSpeakerPassword
        );
        await registrationWidgetIframe.getLoginOptionsComponent.continueButton.click();
      });
      await registrationWidgetIframe.clickOnEnterNowButton();
      await dudaSpeakerLandingPage.isStageLoaded();
    });
  });

  test(`TC007: Deleted @speaker validation`, async ({ page }) => {
    /**
     * If speaker opens magic link in same tab
     * expected behaviour is that, he wont have to click on enter now button
     * he will directly land on stage
     */
    let dudaSpeakerLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;

    await test.step(`Updating landing page details to make event Reg Based and Disabling speaker magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isSpeakermagicLinkEnabled: false,
        }
      );
    });
    // Creating Speaker Data and inviting speaker to event
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", false);
    const speakerEmail = DataUtl.getRandomSpeakerEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    await test.step(`Inviting speaker to the event`, async () => {
      console.log(`Speaker data to add is ${speakerUser}`);
      await inviteSpeakerByApi(
        organiserApiContext,
        speakerUser.getSpeakerDataObject(),
        eventInfoDto.eventId,
        true
      ); // true denote to send email
    });
    console.log(`Speaker data to add is ${speakerUser}`);
    console.log(`Event id is ${eventInfoDto.eventId}`);
    console.log(`Duda published event id is ${dudaPublishedEventId}`);
    await test.step(`Verify this user entry has been made as speaker in DB`, async () => {
      await QueryUtil.verifyUserEmailIsPresentInSpeakerTableForAnEvent(
        speakerEmail,
        eventInfoDto.eventId
      );
    });

    await test.step(`Now Deleting speaker from the event`, async () => {
      console.log(`Speaker data to add is ${speakerUser}`);
      await deleteSpeakerFromEvent(
        organiserApiContext,
        dudaPublishedEventId,
        speakerUser.speakerEmail
      );
      speakerDeleted = true;
    });
    await test.step(`Verify this user entry has been deleted from speaker table in DB`, async () => {
      const isSpeakerDeleted =
        await QueryUtil.verifyUserEmailIsNotPresentInSpeakerTableForAnEvent(
          speakerEmail,
          eventInfoDto.eventId
        );

      expect(isSpeakerDeleted, `expecting speaker deleted to be true`).toBe(
        true
      );
    });

    await test.step(`Loading the duda speaker landing page and click on register button to get registration iframe`, async () => {
      await page.goto(dudaPublishedSpeakerLandingPage);
      dudaSpeakerLandingPage = new DudaLandingPage(page);
      registrationWidgetIframe =
        await dudaSpeakerLandingPage.clickOnRegisterButton();
    });

    await test.step(`user try to login with otp, he should be redirected to registration form again`, async () => {
      await registrationWidgetIframe.getLoginOptionsComponent.emailInputBox.fill(
        speakerUser.speakerEmail
      );
      await registrationWidgetIframe.getLoginOptionsComponent.submitButton.click();
      await dudaSpeakerLandingPage.getRestrictedSpeakerAccessPage.isVisible();
    });
  });

  test(`TC008: @speaker get invited, gets magic link and open magic link and lands on stage for future event`, async ({
    page,
  }) => {
    /**
     * If speaker opens magic link in same tab
     * expected behaviour is that, he wont have to click on enter now button
     * he will directly land on stage
     */
    let speakerMagicLink: string;
    await test.step(`Updating landing page details to make event Reg Based and Enabling speaker magic link and making future event`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isSpeakermagicLinkEnabled: true,
        }
      );
      await eventController.changeEventStartAndEndDateTime({
        deltaByHoursInStartTime: 5,
        deltaByHoursInEndTime: 7,
      });
    });
    // Creating Speaker Data and inviting speaker to event
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", false);
    let speakerUser = new SpeakerData(
      DataUtl.getRandomAttendeeEmail(),
      "Prateek"
    );
    speakerEmail = speakerUser.speakerEmail;

    await test.step(`Inviting speaker to the event`, async () => {
      console.log(`Speaker data to add is ${speakerUser}`);
      await inviteSpeakerByApi(
        organiserApiContext,
        speakerUser.getSpeakerDataObject(),
        eventInfoDto.eventId,
        true
      ); // true denote to send email
    });

    await test.step(`fetching the speaker magic link`, async () => {
      speakerMagicLink = await QueryUtil.fetchDudaMagicLinkForSpeakerFromDB(
        dudaPublishedEventId,
        speakerUser.speakerEmail,
        dudaPublishedSpeakerLandingPage
      );
    });

    await test.step(`Navigating using speaker magic link and verify user sees enter now button and lands on stage after entering event`, async () => {
      let landingPageOne = new DudaLandingPage(page);
      await landingPageOne.load(speakerMagicLink);
      await landingPageOne.clickOnEnterNowButton();
      await landingPageOne.isStageLoaded();
    });
  });
});
