import { test } from "@playwright/test";
import { DudaLandingPage } from "../../../page-objects/events-pages/duda-website/dudaLandingPage";
import { DataUtl } from "../../../util/dataUtil";

test.describe(`@duda @registration @eventover`, async () => {
  test.skip(
    process.env.run_env === "staging",
    "Skipping duda registration tests on staging as it can only run on preprod"
  );
  test.describe.configure({ retries: 2 });
  let dudaPublishedAttendeeLandingPageForEventOver: string;
  let dudaPublishedSpeakerLandingPageForEventOver: string;

  test.beforeEach(async () => {
    dudaPublishedAttendeeLandingPageForEventOver =
      DataUtl.getApplicationTestDataObj()["dudaEventEndedTestCase"];
    dudaPublishedSpeakerLandingPageForEventOver = `${dudaPublishedAttendeeLandingPageForEventOver}?r=s`;
  });

  test(`TC001: @attendee on clicking registration widget, the event is over text should be visible to attendee`, async ({
    page,
  }) => {
    await page.goto(dudaPublishedAttendeeLandingPageForEventOver);
    let dudaAttendeeLandingPage = new DudaLandingPage(page);
    await (
      await dudaAttendeeLandingPage.clickOnRegisterButton()
    ).verifyEventIsEndedTextIsVisible();
  });

  test(`TC002: @speaker on clicking registration widget, the event is over text should be visible to speaker`, async ({
    page,
  }) => {
    await page.goto(dudaPublishedSpeakerLandingPageForEventOver);
    let dudaAttendeeLandingPage = new DudaLandingPage(page);
    await (
      await dudaAttendeeLandingPage.clickOnRegisterButton()
    ).verifyEventIsEndedTextIsVisible();
  });
});
