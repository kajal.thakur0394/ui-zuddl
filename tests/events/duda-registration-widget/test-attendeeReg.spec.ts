import {
  APIRequestContext,
  BrowserContext,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import EventRole from "../../../enums/eventRoleEnum";
import createEventPayloadData from "../../../util/create_event_payload.json";
import { DudaLandingPage } from "../../../page-objects/events-pages/duda-website/dudaLandingPage";
import { RegistrationWidgetIframe } from "../../../page-objects/events-pages/duda-website/registrationIframe";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import {
  deleteAttendeeFromEvent,
  inviteAttendeeByAPI,
  registerUserToEventbyApi,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { validateDudaFutureIcsData } from "../../../util/validation-util";
import { deleteFile, parseICS } from "../../../util/commonUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@duda @registration`, async () => {
  test.skip(
    process.env.run_env === "staging",
    "Skipping duda registration tests on staging as it can only run on preprod"
  );
  let dudaPublishedAttendeeLandingPage: string;
  let dudaPublishedSpeakerLandingPage: string;
  let dudaPublishedEventId;
  let orgBrowserContext: BrowserContext;
  let organiserApiContext: APIRequestContext;
  let eventController: EventController;
  let appleInviteFileName;
  let outlookInviteFileName;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    organiserApiContext = orgBrowserContext.request;
    dudaPublishedEventId =
      DataUtl.getApplicationTestDataObj()[
        "dudaPublishedEventIdForAttendeePageTesting"
      ];
    dudaPublishedAttendeeLandingPage =
      DataUtl.getApplicationTestDataObj()[
        "duaPublishedTemplateForAttendeeRegistration"
      ];
    dudaPublishedSpeakerLandingPage = `${dudaPublishedAttendeeLandingPage}?r=s`;
    eventController = new EventController(
      organiserApiContext,
      dudaPublishedEventId
    );
    // update the event start and end time
    await eventController.changeEventStartAndEndDateTime({
      deltaByHoursInStartTime: 1,
      deltaByHoursInEndTime: 3,
    });
  });

  test.afterEach(async () => {
    await deleteFile(appleInviteFileName);
    await deleteFile(outlookInviteFileName);
    await orgBrowserContext?.close();
  });

  test(`TC001: @attendee new attendee registers through widget, gets magic link and open magic link in same tab which he used for reg`, async ({
    page,
  }) => {
    /**
     * If attendee registers and opens magic link in same tab
     * expected behaviour is that, he wont have to click on enter now button
     * he will directly land on lobby
     */
    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;
    let attendeeMagicLink: string;

    await test.step(`Updating landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
        }
      );
    });
    const attendeeEmail = DataUtl.getRandomAttendeeTestJoynEmail();
    const attendeeInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "automation",
      false
    );
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", false);
    const eventRoleDto = new UserEventRoleDto(
      attendeeInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      false
    );
    await test.step(`Loading the duda attendee landing page and click on register button to get registration iframe`, async () => {
      await page.goto(dudaPublishedAttendeeLandingPage);
      dudaAttendeeLandingPage = new DudaLandingPage(page);
      registrationWidgetIframe =
        await dudaAttendeeLandingPage.clickOnRegisterButton();
    });

    await test.step(`fill up the registration form`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.fillUpTheRegistrationForm(
        {
          firstName: "prateek",
          lastName: "automation",
          email: attendeeEmail,
        }
      );
    });

    await test.step(`verify that successfull registration message is displayed`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.verifySuccessFullRegistrationMessageIsVisible();
    });

    await test.step(`fetching the attendee magic link after user registration`, async () => {
      attendeeMagicLink = await QueryUtil.fetchDudaAttendeeMagicLinkFromDB(
        dudaPublishedEventId,
        attendeeEmail,
        dudaPublishedAttendeeLandingPage
      );
    });

    await test.step(`Navigating using magic link and verify user sees enter now button and lands on Lobby after entering event`, async () => {
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendeeMagicLink);
      await landingPageOne.isLobbyLoaded();
    });
  });

  test(`TC002: @attendee new attendee registers through widget, gets magic link and open magic link in different tab which he used for reg`, async ({
    context,
  }) => {
    let page1 = await context.newPage();
    let page2 = await context.newPage();
    let dudaAttendeeLandingPage: DudaLandingPage;
    let dudaAttendeeLandingPage2: DudaLandingPage;

    let registrationWidgetIframe: RegistrationWidgetIframe;
    let attendeeMagicLink: string;

    await test.step(`Updaing landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
        }
      );
    });
    const attendeeEmail = DataUtl.getRandomAttendeeTestJoynEmail();
    const attendeeInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "automation",
      false
    );
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", false);
    const eventRoleDto = new UserEventRoleDto(
      attendeeInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      false
    );
    await test.step(`Loading the duda attendee landing page and click on register button to get registration iframe`, async () => {
      await page1.goto(dudaPublishedAttendeeLandingPage);
      dudaAttendeeLandingPage = new DudaLandingPage(page1);
      registrationWidgetIframe =
        await dudaAttendeeLandingPage.clickOnRegisterButton();
    });

    await test.step(`fill up the registration form`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.fillUpTheRegistrationForm(
        {
          firstName: "prateek",
          lastName: "automation",
          email: attendeeEmail,
        }
      );
    });

    await test.step(`verify that successfull registration message is displayed`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.verifySuccessFullRegistrationMessageIsVisible();
    });

    await test.step(`fetching the attendee magic link after user registration`, async () => {
      attendeeMagicLink = await QueryUtil.fetchDudaAttendeeMagicLinkFromDB(
        dudaPublishedEventId,
        attendeeEmail,
        dudaPublishedAttendeeLandingPage
      );
    });

    await test.step(`Navigating using magic link and verify user sees enter now button and lands on Lobby after entering event`, async () => {
      // await context.clearCookies();
      dudaAttendeeLandingPage2 = new DudaLandingPage(page2);
      await dudaAttendeeLandingPage2.load(attendeeMagicLink);
      // await dudaAttendeeLandingPage2.clickOnEnterNowButton();
      await dudaAttendeeLandingPage2.isLobbyLoaded();
    });
  });

  test(`TC003: @attendee new attendee try to skip registration and try to login via password`, async ({
    page,
  }) => {
    /**
     * Attendee lands on duda landing page
     * click on already registered
     * click on login via password
     * enter email for password login
     * since he is new user, he will be redirected to registration form
     * where email will be prefilled
     * then he fills remaining field and complete registration
     * and verifies he gets the magic link and login through it
     */
    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;
    let attendeeMagicLink: string;

    await test.step(`Updating landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
        }
      );
    });
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
    const attendeeInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "automation",
      false
    );
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", true);
    const eventRoleDto = new UserEventRoleDto(
      attendeeInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await test.step(`Loading the duda attendee landing page and click on register button to get registration iframe`, async () => {
      await page.goto(dudaPublishedAttendeeLandingPage);
      dudaAttendeeLandingPage = new DudaLandingPage(page);
      registrationWidgetIframe =
        await dudaAttendeeLandingPage.clickOnRegisterButton();
    });

    await test.step(`user click on already registered button`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.clickOnAlreadyRegisteredButton();
    });

    await test.step(`user try to login with password, he should be redirected to registration form again`, async () => {
      await registrationWidgetIframe.getLoginOptionsComponent.loginViaPasswordButton.click();
      await registrationWidgetIframe.getLoginOptionsComponent.emailInputBox.fill(
        attendeeEmail
      );
      await registrationWidgetIframe.getLoginOptionsComponent.signInButton.click();
      await registrationWidgetIframe.getLoginOptionsComponent.submitButton.click();
      await registrationWidgetIframe.getRegistrationFormComponent.isRegistrationFormVisible();
      await test.step(`expecting email to be prefilled in registration form`, async () => {
        await registrationWidgetIframe.getRegistrationFormComponent.verifyEmailFieldIsPreFilledWith(
          attendeeEmail
        );
      });
    });

    await test.step(`fill up the registration form`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.fillUpTheRegistrationForm(
        {
          firstName: "prateek",
          lastName: "automation",
          email: attendeeEmail,
        }
      );
    });

    await test.step(`verify that successfull registration message is displayed`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.verifySuccessFullRegistrationMessageIsVisible();
    });

    await test.step(`fetching the attendee magic link after user registration`, async () => {
      attendeeMagicLink = await QueryUtil.fetchDudaAttendeeMagicLinkFromDB(
        dudaPublishedEventId,
        attendeeEmail,
        dudaPublishedAttendeeLandingPage
      );
    });

    await test.step(`Navigating using magic link and verify user sees enter now button and lands on Lobby after entering event`, async () => {
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendeeMagicLink);
      await landingPageOne.isLobbyLoaded();
    });
  });

  test(`TC004: @attendee new attendee try to skip registration and try to login via email to trigger magic link`, async ({
    page,
  }) => {
    /**
     * Attendee lands on duda landing page
     * click on already registered
     * click on login via password
     * enter email for magic link login
     * since he is new user, he will be redirected to registration form
     * where email will be prefilled
     * then he fills remaining field and complete registration
     * and verifies he gets the magic link and login through it
     */
    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;
    let attendeeMagicLink: string;

    await test.step(`Updaing landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
        }
      );
    });
    const attendeeEmail = DataUtl.getRandomAttendeeTestJoynEmail();
    const attendeeInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "automation",
      false
    );
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", true);
    const eventRoleDto = new UserEventRoleDto(
      attendeeInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    await test.step(`Loading the duda attendee landing page and click on register button to get registration iframe`, async () => {
      await page.goto(dudaPublishedAttendeeLandingPage);
      dudaAttendeeLandingPage = new DudaLandingPage(page);
      registrationWidgetIframe =
        await dudaAttendeeLandingPage.clickOnRegisterButton();
    });

    await test.step(`user click on already registered button`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.isRegistrationFormVisible();
      await registrationWidgetIframe.getRegistrationFormComponent.clickOnAlreadyRegisteredButton();
    });

    await test.step(`user enters email in email input box to trigger magic link but since he is new, he will get redirected to reg form`, async () => {
      await registrationWidgetIframe.getLoginOptionsComponent.emailInputBoxForOTP.fill(
        attendeeEmail
      );
      await page.waitForTimeout(1000);
      await registrationWidgetIframe.getLoginOptionsComponent.submitButton.click();
      await registrationWidgetIframe.getRegistrationFormComponent.isRegistrationFormVisible();
      await test.step(`expecting email to be prefilled in registration form`, async () => {
        await registrationWidgetIframe.getRegistrationFormComponent.verifyEmailFieldIsPreFilledWith(
          attendeeEmail
        );
      });
    });

    await test.step(`fill up the registration form`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.fillUpTheRegistrationForm(
        {
          firstName: "prateek",
          lastName: "automation",
          email: attendeeEmail,
        }
      );
    });

    await test.step(`verify that successfull registration message is displayed`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.verifySuccessFullRegistrationMessageIsVisible();
    });

    await test.step(`fetching the attendee magic link after user registration`, async () => {
      attendeeMagicLink = await QueryUtil.fetchDudaAttendeeMagicLinkFromDB(
        dudaPublishedEventId,
        attendeeEmail,
        dudaPublishedAttendeeLandingPage
      );
    });

    await test.step(`Navigating using magic link and verify user sees enter now button and lands on Lobby after entering event`, async () => {
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendeeMagicLink);
      await landingPageOne.isLobbyLoaded();
    });
  });

  test(`TC005: @attendee new attendee already registered to magic link event try to login via password instead`, async ({
    page,
  }) => {
    /**
     * Attendee lands on duda landing page
     * click on already registered
     * click on login via password
     * enter email for magic link login
     * since he is new user, he will be redirected to registration form
     * where email will be prefilled
     * then he fills remaining field and complete registration
     * and verifies he gets the magic link and login through it
     */
    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;
    let attendeeMagicLink: string;

    await test.step(`Updaing landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
        }
      );
    });
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
    const attendeeInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "automation",
      false
    );
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", true);
    const eventRoleDto = new UserEventRoleDto(
      attendeeInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );

    await test.step(`Registering user via API to this event`, async () => {
      await registerUserToEventbyApi(
        organiserApiContext,
        dudaPublishedEventId,
        attendeeEmail
      );
    });
    await test.step(`Loading the duda attendee landing page and click on register button to get registration iframe`, async () => {
      await page.goto(dudaPublishedAttendeeLandingPage);
      dudaAttendeeLandingPage = new DudaLandingPage(page);
      registrationWidgetIframe =
        await dudaAttendeeLandingPage.clickOnRegisterButton();
    });

    await test.step(`user click on already registered button`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.isRegistrationFormVisible();

      await registrationWidgetIframe.getRegistrationFormComponent.clickOnAlreadyRegisteredButton();
    });

    await test.step(`user select password to login and enter email to trigger password but since user is new, it will trigger OTP flow`, async () => {
      await registrationWidgetIframe.getLoginOptionsComponent.loginViaPasswordButton.click();
      await registrationWidgetIframe.getLoginOptionsComponent.emailInputBox.fill(
        attendeeEmail
      );
      await registrationWidgetIframe.getLoginOptionsComponent.signInButton.click();
      await test.step(`should see submit button to trigger otp, click on it`, async () => {
        await registrationWidgetIframe.getLoginOptionsComponent.submitButton.click();
      });
      await test.step(`get the otp from db and enter into the otp box`, async () => {
        const otpFetched = await QueryUtil.fetchLoginOTPForEvent(
          attendeeEmail,
          dudaPublishedEventId
        );
        console.log(`otp fetched is ${otpFetched}`);
        await registrationWidgetIframe.getLoginOptionsComponent.enterAndSubmitOtp(
          otpFetched
        );
      });
    });

    await test.step(`user sees enter now button, clickin on it takes user to lobby`, async () => {
      await registrationWidgetIframe.clickOnEnterNowButton();
      await dudaAttendeeLandingPage.isLobbyLoaded();
    });
  });

  test(`TC006: @attendee new attendee already registered to magic link event try to login via entering email for magic link`, async ({
    page,
  }) => {
    /**
     * Attendee lands on duda landing page
     * click on already registered
     * click on login via password
     * enter email for magic link login
     * since he is new user, he will be redirected to registration form
     * where email will be prefilled
     * then he fills remaining field and complete registration
     * and verifies he gets the magic link and login through it
     */
    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;
    let attendeeMagicLink: string;

    await test.step(`Updaing landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
        }
      );
    });
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
    const attendeeInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "automation",
      false
    );
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", true);
    const eventRoleDto = new UserEventRoleDto(
      attendeeInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );

    await test.step(`Registering user via API to this event`, async () => {
      await registerUserToEventbyApi(
        organiserApiContext,
        dudaPublishedEventId,
        attendeeEmail
      );
    });
    await test.step(`Loading the duda attendee landing page and click on register button to get registration iframe`, async () => {
      await page.goto(dudaPublishedAttendeeLandingPage);
      dudaAttendeeLandingPage = new DudaLandingPage(page);
      registrationWidgetIframe =
        await dudaAttendeeLandingPage.clickOnRegisterButton();
    });

    await test.step(`user click on already registered button`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.isRegistrationFormVisible();
      await registrationWidgetIframe.getRegistrationFormComponent.clickOnAlreadyRegisteredButton();
    });

    await test.step(`user enter his email which will trigger magic link for him`, async () => {
      await registrationWidgetIframe.getLoginOptionsComponent.emailInputBoxForOTP.fill(
        attendeeEmail
      );
      await registrationWidgetIframe.getLoginOptionsComponent.submitButton.click();
    });

    await test.step(`get the magic link from db and load it should take user to lobby`, async () => {
      attendeeMagicLink = await QueryUtil.fetchDudaAttendeeMagicLinkFromDB(
        dudaPublishedEventId,
        attendeeEmail,
        dudaPublishedAttendeeLandingPage
      );
      await test.step(`Navigating using magic link and verify user sees enter now button and lands on Lobby after entering event`, async () => {
        let landingPageOne = new LandingPageOne(page);
        await landingPageOne.load(attendeeMagicLink);
        await landingPageOne.isLobbyLoaded();
      });
    });
  });

  test(`TC007: @attendee existing attendee after registration can choose to login using his existing password`, async ({
    page,
  }) => {
    /**
     * Existing attendee in test data should already be registered to this event, if not do it manually
     * Attendee lands on attendee landing page
     * Attendee clicks on register now buttton
     * Attendee gets reg iframe
     * click on already reg button
     * choose login via password
     * enter email and password, click on continue
     * click on enter now button, should land on lobby
     */
    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;
    let attendeeMagicLink: string;

    await test.step(`Updating landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
        }
      );
    });
    const existingAttendeeEmail =
      DataUtl.getApplicationTestDataObj().attendeeEmailWithAccountEntry;
    const existingAttendeePassword =
      DataUtl.getApplicationTestDataObj().attendeePasswordWithAccountEntry;

    const attendeeInfoDto = new UserInfoDTO(
      existingAttendeeEmail,
      existingAttendeePassword,
      "prateek",
      "automation",
      true
    );
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", true);

    await test.step(`Loading the duda attendee landing page and click on register button to get registration iframe`, async () => {
      await page.goto(dudaPublishedAttendeeLandingPage);
      dudaAttendeeLandingPage = new DudaLandingPage(page);
      registrationWidgetIframe =
        await dudaAttendeeLandingPage.clickOnRegisterButton();
    });

    await test.step(`user click on already registered button`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.isRegistrationFormVisible();

      await registrationWidgetIframe.getRegistrationFormComponent.clickOnAlreadyRegisteredButton();
    });

    await test.step(`attendee choose to login via password he already have with him`, async () => {
      await registrationWidgetIframe.getLoginOptionsComponent.loginViaPasswordButton.click();
    });

    await test.step(`attendee enters his email and password`, async () => {
      await registrationWidgetIframe.getLoginOptionsComponent.emailInputBox.fill(
        existingAttendeeEmail
      );
      await registrationWidgetIframe.getLoginOptionsComponent.signInButton.click();
      await registrationWidgetIframe.getLoginOptionsComponent.enterPasswordBox.fill(
        existingAttendeePassword
      );
      await registrationWidgetIframe.getLoginOptionsComponent.continueButton.click();
    });

    await test.step(`expecting attendee to see enter now button which if click user should go to lobby`, async () => {
      await expect(
        registrationWidgetIframe.enterNowButtonOnIframe
      ).toBeVisible();
      await page.waitForLoadState("networkidle");
      await registrationWidgetIframe.clickOnEnterNowButton();
      await dudaAttendeeLandingPage.isLobbyLoaded();
    });
  });

  test(`TC009: @attendee if guest login is enabled, user can login through it`, async ({
    page,
  }) => {
    /**
     * Attendee lands on duda landing page
     * click on login as guest button in iframe
     * fill up the guest form
     * click on continue button
     * should see enter now button, if clicked
     * should take to the lobby
     */
    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;
    let attendeeMagicLink: string;

    await test.step(`Updaing landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
          isGuestAuthEnabled: true,
        }
      );
    });
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
    const attendeeInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "automation",
      false
    );
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", true);
    const eventRoleDto = new UserEventRoleDto(
      attendeeInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );

    await test.step(`Loading the duda attendee landing page and click on register button to get registration iframe`, async () => {
      await page.goto(dudaPublishedAttendeeLandingPage);
      dudaAttendeeLandingPage = new DudaLandingPage(page);
      registrationWidgetIframe =
        await dudaAttendeeLandingPage.clickOnRegisterButton();
    });

    await test.step(`user click on login as guest and fill up the guest form`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.isRegistrationFormVisible();
      await (
        await registrationWidgetIframe.getRegistrationFormComponent.clickOnLoginAsGuestButton()
      ).fillGuestAuthForm({
        firstName: "prateek",
        lastName: "automation",
        email: attendeeEmail,
      });
    });

    await test.step(`user should see enter now button, clickin on it should take to lobby`, async () => {
      await registrationWidgetIframe.clickOnEnterNowButton();
      await dudaAttendeeLandingPage.isLobbyLoaded();
    });
  });

  test(`TC010: @attendee if deleted attendee try to reg again, he should be blocked`, async ({
    page,
  }) => {
    /**
     * register a user via api
     * verify he has registered by getting his reg id
     * delete the attendee via api
     * now attendee loads the duda landing page
     * click on register button shoud open reg iframe
     * now if user tries using same email id for reg
     * an error should be thrown
     */
    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;
    let attendeeMagicLink: string;
    let userRegistrationId;

    await test.step(`Updaing landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
        }
      );
    });
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
    const attendeeInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "automation",
      false
    );
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", true);
    const eventRoleDto = new UserEventRoleDto(
      attendeeInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );

    await test.step(`Registering user via API to this event`, async () => {
      await registerUserToEventbyApi(
        organiserApiContext,
        dudaPublishedEventId,
        attendeeEmail
      );
    });

    await test.step(`fetching user registration id from db`, async () => {
      const userRegRecord =
        await QueryUtil.fetchUserDataFromEventRegAccountAndEventRoleTable(
          dudaPublishedEventId,
          attendeeEmail
        );
      userRegistrationId = userRegRecord["registration_id"];
      expect(
        userRegistrationId,
        `expecting ${userRegistrationId} to be not undefined`
      ).not.toBeUndefined();
    });

    await test.step(`delete the user via api`, async () => {
      await deleteAttendeeFromEvent(
        organiserApiContext,
        dudaPublishedEventId,
        attendeeEmail
      );
    });

    await test.step(`attendee use his deleted email to fill up the reg form should trigger blocked notification`, async () => {
      dudaAttendeeLandingPage = new DudaLandingPage(page);
      await dudaAttendeeLandingPage.load(dudaPublishedAttendeeLandingPage);
      registrationWidgetIframe =
        await dudaAttendeeLandingPage.clickOnRegisterButton();
      await registrationWidgetIframe.getRegistrationFormComponent.isRegistrationFormVisible();
      await registrationWidgetIframe.getRegistrationFormComponent.fillUpTheRegistrationForm(
        {
          firstName: "prateek",
          lastName: "automation",
          email: attendeeEmail,
        }
      );
      await registrationWidgetIframe.verifyNotificationIsSeenWithMessage(
        "You have been removed from the event"
      );
    });
  });

  test(`TC011: @attendee attendee magic link should not work if he is deleted from the event`, async ({
    page,
  }) => {
    /**
     * register a user via api
     * verify he has registered by getting his reg id
     * attendee gets the magic link
     * but we delete the attendee via api
     * now attendee loads the attendee magic link
     * attendee should not see enter now button
     * attendee should see register button as the magic link is no more valid
     */
    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;
    let attendeeMagicLink: string;
    let userRegistrationId;

    await test.step(`Updaing landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
        }
      );
    });
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
    const attendeeInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "automation",
      false
    );
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", true);
    const eventRoleDto = new UserEventRoleDto(
      attendeeInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );

    await test.step(`Registering user via API to this event`, async () => {
      await registerUserToEventbyApi(
        organiserApiContext,
        dudaPublishedEventId,
        attendeeEmail
      );
    });

    await test.step(`fetching user registration id from db`, async () => {
      const userRegRecord =
        await QueryUtil.fetchUserDataFromEventRegAccountAndEventRoleTable(
          dudaPublishedEventId,
          attendeeEmail
        );
      userRegistrationId = userRegRecord["registration_id"];
      expect(
        userRegistrationId,
        `expecting ${userRegistrationId} to be not undefined`
      ).not.toBeUndefined();
    });

    await test.step(`fetching magic link from the db for user`, async () => {
      attendeeMagicLink = await QueryUtil.fetchDudaAttendeeMagicLinkFromDB(
        dudaPublishedEventId,
        attendeeEmail,
        dudaPublishedAttendeeLandingPage
      );
    });

    await test.step(`delete the user via api`, async () => {
      await deleteAttendeeFromEvent(
        organiserApiContext,
        dudaPublishedEventId,
        attendeeEmail
      );
    });

    await test.step(`attendee tries to use his old magic link and it should not work since he is deleted`, async () => {
      dudaAttendeeLandingPage = new DudaLandingPage(page);
      await dudaAttendeeLandingPage.load(attendeeMagicLink);
      await expect(
        dudaAttendeeLandingPage.enterNowButton,
        "expecting enter now button to not be visibl since magic link is no more valid"
      ).not.toBeVisible();
      await expect(
        dudaAttendeeLandingPage.registerButton,
        "expecting register now button to be visible since magic link will not work"
      ).toBeVisible();
    });
  });

  test(`TC012: @attendee invited attendee can login to event with the recieved magic link`, async ({
    page,
  }) => {
    /**
     * register a user via api
     * verify he has registered by getting his reg id
     * attendee gets the magic link
     * but we delete the attendee via api
     * now attendee loads the attendee magic link
     * attendee should not see enter now button
     * attendee should see register button as the magic link is no more valid
     */
    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;
    let attendeeMagicLink: string;
    let userRegistrationId;

    await test.step(`Updaing landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.INVITE_ONLY,
          isMagicLinkEnabled: true,
        }
      );
    });
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
    const attendeeInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "automation",
      false
    );
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", true);
    const eventRoleDto = new UserEventRoleDto(
      attendeeInfoDto,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );

    await test.step(`Invite user via API to this event`, async () => {
      await inviteAttendeeByAPI(
        organiserApiContext,
        dudaPublishedEventId,
        attendeeEmail,
        false
      );
    });

    await test.step(`fetching magic link from the db for user`, async () => {
      attendeeMagicLink = await QueryUtil.fetchDudaAttendeeMagicLinkFromDB(
        dudaPublishedEventId,
        attendeeEmail,
        dudaPublishedAttendeeLandingPage
      );
    });

    await test.step(`verify if user uses his magic link he should be able to enter into invite only event`, async () => {
      dudaAttendeeLandingPage = new DudaLandingPage(page);
      await dudaAttendeeLandingPage.load(attendeeMagicLink);
      // await dudaAttendeeLandingPage.clickOnEnterNowButton();
      await dudaAttendeeLandingPage.isLobbyLoaded();
    });
  });

  test(`TC013: @attendee not invited user should not be able to login to the invite only event`, async ({
    page,
  }) => {
    /**
     * in an invite only event
     * verify if an uninvited user tries to login
     * he sees restricted entry screen for attendee
     */
    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;

    await test.step(`Updaing landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.INVITE_ONLY,
          isMagicLinkEnabled: true,
        }
      );
    });
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();

    await test.step(`Attendee loads the landing page and click on register button to open iframe`, async () => {
      dudaAttendeeLandingPage = new DudaLandingPage(page);
      await dudaAttendeeLandingPage.load(dudaPublishedAttendeeLandingPage);
      registrationWidgetIframe =
        await dudaAttendeeLandingPage.clickOnRegisterButton();
    });
    await test.step(`attendee clicks on already registered button as he thinks he is invited`, async () => {
      await expect(
        registrationWidgetIframe.getRegistrationFormComponent
          .alreadyRegisteredButtonContainer,
        "expecting already registered button to be visible"
      ).toBeVisible();
      await registrationWidgetIframe.getRegistrationFormComponent.clickOnAlreadyRegisteredButton();
      await registrationWidgetIframe.getLoginOptionsComponent.emailInputBoxForOTP.fill(
        attendeeEmail
      );
      await registrationWidgetIframe.getLoginOptionsComponent.submitButton.click();
    });

    await test.step(`verify attendee sees the attendee restricted page to tell he has no access`, async () => {
      await registrationWidgetIframe.verifyInviteOnlyRestrictedScreenIsVisibleForAttendee();
    });
  });

  test(`TC014: @organiser if already logged in visit Duda published landing page, he should be treated as logged in`, async () => {
    /**
     * in an invite only event
     * verify if an uninvited user tries to login
     * he sees restricted entry screen for attendee
     */
    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;
    let orgPage = await orgBrowserContext.newPage();
    await test.step(`Updaing landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
        }
      );
    });

    await test.step(`orgainser goes to visit the duda published landing page, should see enter now button`, async () => {
      dudaAttendeeLandingPage = new DudaLandingPage(orgPage);
      await dudaAttendeeLandingPage.load(dudaPublishedAttendeeLandingPage);
      await dudaAttendeeLandingPage.clickOnEnterNowButton();
    });

    await test.step(`organiser if clicks on enter now button should be landed on stage`, async () => {
      await dudaAttendeeLandingPage.isStageLoaded();
    });
  });

  test(`TC015: @attendee if event is outside check in window, he should see add to calendar screen `, async ({
    page,
  }) => {
    /**
     * in an invite only event
     * verify if an uninvited user tries to login
     * he sees restricted entry screen for attendee
     */

    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;
    let attendeeMagicLink: string;
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();

    await test.step(`Updating event start time to start after 3 hours from now so that it is outside of checkin window`, async () => {
      await eventController.changeEventStartAndEndDateTime({
        deltaByHoursInStartTime: 5,
        deltaByHoursInEndTime: 8,
      });
    });

    await test.step(`register user via api to the event`, async () => {
      await registerUserToEventbyApi(
        organiserApiContext,
        dudaPublishedEventId,
        attendeeEmail
      );
    });
    await test.step(`Updating landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
        }
      );
    });

    await test.step(`Fetching magic link from DB for the registered user`, async () => {
      attendeeMagicLink = await QueryUtil.fetchDudaAttendeeMagicLinkFromDB(
        dudaPublishedEventId,
        attendeeEmail,
        dudaPublishedAttendeeLandingPage
      );
    });

    await test.step(`Attendee visits the landing page using magic link, should show that event is in future`, async () => {
      dudaAttendeeLandingPage = new DudaLandingPage(page);
      await dudaAttendeeLandingPage.load(attendeeMagicLink);
      await dudaAttendeeLandingPage.clickOnRegisterButton();
      await dudaAttendeeLandingPage.getRegistrationIframeWidget.verifyEventIsInFutureScreenIsVisible();
      await dudaAttendeeLandingPage.getRegistrationIframeWidget.verifyAddToCalendarIsVisible();
    });

    await test.step(`Validating Add to calendar links`, async () => {
      const eventTitleForDudaPage =
        DataUtl.getApplicationTestDataObj()[
          "eventTitleForAttendeeRegOnDudaPage"
        ];
      let calendarLinks =
        await dudaAttendeeLandingPage.getRegistrationIframeWidget.fetchCalendarLinksFromAddToCalendarButton();
      appleInviteFileName = calendarLinks[0];
      outlookInviteFileName = calendarLinks[1];
      let eventInfoDto = new EventInfoDTO(
        dudaPublishedEventId,
        eventTitleForDudaPage,
        true
      ); // false to denote magic link disabled
      eventInfoDto.startDateTime = createEventPayloadData.startDateTime;
      eventInfoDto.setDudaPublishedLandingPage =
        dudaPublishedAttendeeLandingPage;
      await test.step("Verifying Outlook ICS files link and text from email recieved after registering", async () => {
        await validateDudaFutureIcsData(
          parseICS(outlookInviteFileName),
          eventInfoDto,
          dudaPublishedAttendeeLandingPage
        );
      });

      await test.step("Verifying Apple ICS files link and text from email recieved after registering", async () => {
        await validateDudaFutureIcsData(
          parseICS(appleInviteFileName),
          eventInfoDto,
          dudaPublishedAttendeeLandingPage
        );
      });
    });
  });

  test.skip(`TC016: @attendee validating that attendee from a different timezone sees is able to enter event `, async ({
    page,
  }) => {
    /**
     * in an invite only event
     * verify if an uninvited user tries to login
     * he sees restricted entry screen for attendee
     */
    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;
    let eventController: EventController;
    let attendeeMagicLink: string;
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();

    await test.step(`Updating event start time to start after 3 hours from now so that it is outside of checkin window`, async () => {
      let eventStartTime = new Date();
      eventStartTime.setDate(eventStartTime.getDate() + 7);
      console.log("New event Start Date time", eventStartTime.toISOString());
      let eventEndTime = new Date();
      eventEndTime.setDate(eventEndTime.getDate() + 8);
      console.log("New event end Date time", eventEndTime.toISOString());
      eventController = new EventController(
        organiserApiContext,
        dudaPublishedEventId
      );
      await eventController.updateEventEntryTime(
        eventStartTime.toISOString(),
        eventEndTime.toISOString()
      );
    });

    await test.step(`register user via api to the event`, async () => {
      await registerUserToEventbyApi(
        organiserApiContext,
        dudaPublishedEventId,
        attendeeEmail
      );
    });
    await test.step(`Updating landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
        }
      );
    });

    await test.step(`Fetching magic link from DB for the registered user`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        dudaPublishedEventId,
        attendeeEmail
      );
    });

    await test.step(`Attendee visits the landing page using magic link, should show that event is in future`, async () => {
      dudaAttendeeLandingPage = new DudaLandingPage(page);
      await dudaAttendeeLandingPage.load(attendeeMagicLink);
      await dudaAttendeeLandingPage.clickOnRegisterButton();
      await dudaAttendeeLandingPage.getRegistrationIframeWidget.verifyEventIsInFutureScreenIsVisible();
      await dudaAttendeeLandingPage.getRegistrationIframeWidget.verifyAddToCalendarIsVisible();
    });
  });

  test.skip(`TC017: @attendee if event is in future, then attendee joining from different timezone sees the same  `, async ({
    page,
  }) => {
    /**
     * in an invite only event
     * verify if an uninvited user tries to login
     * he sees restricted entry screen for attendee
     */
    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;
    let eventController: EventController;
    let attendeeMagicLink: string;
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();

    await test.step(`Updating event start time to start after 3 hours from now so that it is outside of checkin window`, async () => {
      let eventStartTime = new Date();
      eventStartTime.setDate(eventStartTime.getDate() + 7);
      console.log("New event Start Date time", eventStartTime.toISOString());
      let eventEndTime = new Date();
      eventEndTime.setDate(eventEndTime.getDate() + 8);
      console.log("New event end Date time", eventEndTime.toISOString());
      eventController = new EventController(
        organiserApiContext,
        dudaPublishedEventId
      );
      await eventController.updateEventEntryTime(
        eventStartTime.toISOString(),
        eventEndTime.toISOString()
      );
    });

    await test.step(`register user via api to the event`, async () => {
      await registerUserToEventbyApi(
        organiserApiContext,
        dudaPublishedEventId,
        attendeeEmail
      );
    });
    await test.step(`Updating landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
        }
      );
    });

    await test.step(`Fetching magic link from DB for the registered user`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        dudaPublishedEventId,
        attendeeEmail
      );
    });

    await test.step(`Attendee visits the landing page using magic link, should show that event is in future`, async () => {
      dudaAttendeeLandingPage = new DudaLandingPage(page);
      await dudaAttendeeLandingPage.load(attendeeMagicLink);
      await dudaAttendeeLandingPage.clickOnRegisterButton();
      await dudaAttendeeLandingPage.getRegistrationIframeWidget.verifyEventIsInFutureScreenIsVisible();
      await dudaAttendeeLandingPage.getRegistrationIframeWidget.verifyAddToCalendarIsVisible();
    });
  });

  test(`TC018: @attendee if email restriction is applied, same should reflect on duda registration widget `, async ({
    page,
  }) => {
    /**
     * in an invite only event
     * verify if an uninvited user tries to login
     * he sees restricted entry screen for attendee
     */
    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;
    const attendeeEmail = "prateek@gmail.com";

    await test.step(`Apply email restriction to not allow @gmail.com to this event`, async () => {
      await eventController.applyEmailRestrictionToNotAllowBusinessEmails();
    });

    await test.step(`Attendee open duda landing page`, async () => {
      dudaAttendeeLandingPage = new DudaLandingPage(page);
      await dudaAttendeeLandingPage.load(dudaPublishedAttendeeLandingPage);
    });

    await test.step(`Attendee try to register with email ${attendeeEmail} which is restricted and hence it should be blocked`, async () => {
      registrationWidgetIframe =
        await dudaAttendeeLandingPage.clickOnRegisterButton();
      await registrationWidgetIframe.getRegistrationFormComponent.fillUpTheRegistrationForm(
        {
          firstName: "prateek",
          lastName: "automation",
          email: "prateek@gmail.com",
        }
      );
      await registrationWidgetIframe.verifyRestrictedDomainFieldValidationErrorIsVisible();
    });
  });

  test(`@utm-params TC019: @attendee if user uses duda landing page link appended with utm params for reg, those should get recorded in event_registration table for this entry`, async ({
    page,
  }) => {
    /**
     * If attendee registers and opens magic link in same tab
     * expected behaviour is that, he wont have to click on enter now button
     * he will directly land on lobby
     */
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-237/validation-for-duda",
    });
    let dudaAttendeeLandingPage: DudaLandingPage;
    let registrationWidgetIframe: RegistrationWidgetIframe;
    let attendeeMagicLink: string;
    const utm_medium = "playwright";
    const utm_source = "automation";
    let dudaPublishedAttendeeLandingPageWithUtmParams: string;
    await test.step(`Appending the duda published attendee landing page with utm_params`, async () => {
      dudaPublishedAttendeeLandingPageWithUtmParams = `${dudaPublishedAttendeeLandingPage}?utm_source=${utm_source}&utm_medium=${utm_medium}`;
      console.log(
        `Attendee landing page with utm params appended is ${dudaPublishedAttendeeLandingPageWithUtmParams}`
      );
    });
    await test.step(`Updating landing page details to make event Reg Based and Enabling attendee magic link`, async () => {
      await updateEventLandingPageDetails(
        organiserApiContext,
        dudaPublishedEventId,
        {
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
        }
      );
    });
    const attendeeEmail = DataUtl.getRandomAttendeeTestJoynEmail();
    const attendeeInfoDto = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "automation",
      false
    );
    const eventInfoDto = new EventInfoDTO(dudaPublishedEventId, "", false);
    const eventRoleDto = new UserEventRoleDto(
      attendeeInfoDto,
      eventInfoDto,
      EventRole.ATTENDEE,
      false
    );
    await test.step(`Loading the duda attendee landing page appended with utm param and click on register button to get registration iframe`, async () => {
      await page.goto(dudaPublishedAttendeeLandingPageWithUtmParams);
      dudaAttendeeLandingPage = new DudaLandingPage(page);
      registrationWidgetIframe =
        await dudaAttendeeLandingPage.clickOnRegisterButton();
    });

    await test.step(`fill up the registration form`, async () => {
      await registrationWidgetIframe.getRegistrationFormComponent.fillUpTheRegistrationForm(
        {
          firstName: "prateek",
          lastName: "automation",
          email: attendeeEmail,
        }
      );
    });

    await test.step(`Now fetching the user entry from event_registration table and validate the utm params`, async () => {
      //verify in DB event_registration table, the utm param exists as expected
      const userRegDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          dudaPublishedEventId,
          attendeeEmail
        );
      //fetch the utm param from db for registration record
      const utmParamsFromDb = userRegDataFromDb["utm"];
      //validate the utm params
      //const utmParamJson = JSON.parse(utmParamsFromDb);
      //validate the key and value
      await test.step(`validating the utm_medium param from db entry`, async () => {
        expect(
          utmParamsFromDb["utm_medium"],
          `expecting utm_medium to be ${utm_medium}`
        ).toBe(utm_medium);
      });

      await test.step(`validating the utm_source param from db entry`, async () => {
        expect(
          utmParamsFromDb["utm_source"],
          `expecting utm_source to be ${utm_source}`
        ).toBe(utm_source);
      });
    });
  });
});
