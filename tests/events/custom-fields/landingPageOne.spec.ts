import {
  test,
  BrowserContext,
  APIRequestContext,
  expect,
  Page,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import {
  getEventDetails,
  addCustomFieldsToEvent,
  createNewEvent,
  inviteAttendeeByAPI,
  enableMagicLinkInEvent,
} from "../../../util/apiUtil";
import { DataUtl } from "../../../util/dataUtil";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventRole from "../../../enums/eventRoleEnum";
import EventEntryType from "../../../enums/eventEntryEnum";
import { fetchAttendeeInviteMagicLink } from "../../../util/emailUtil";
import { EventCustomFieldFormObj } from "../../../test-data/userRegForm";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";

test.describe.parallel("Events with custom fields | Landing Page 1", () => {
  let org_browser_context: BrowserContext;
  let organiserApiContext: APIRequestContext;
  let eventTitle: string;
  let eventId;
  let csvFileToAddDataIn: string; //filename
  let eventInfoDto: EventInfoDTO;
  let attendee_landing_page: string;
  let speaker_landing_page: string;
  let landingPageId;
  let userInfoDTO: UserInfoDTO;
  let userEventRoleDto: UserEventRoleDto;
  let page: Page;

  test.beforeEach(async ({ context }) => {
    org_browser_context = await BrowserFactory.getBrowserContext({});
    organiserApiContext = await org_browser_context.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: organiserApiContext,
      event_title: eventTitle,
      isMagicLinkEnabledAttendee: false,
    });
    csvFileToAddDataIn = DataUtl.getRandomCsvName();
    // adding event dto
    eventInfoDto = new EventInfoDTO(eventId, eventTitle, false); // true to denote magic link enabled
    eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
    const event_details_api_resp = await (
      await getEventDetails(organiserApiContext, eventId)
    ).json();
    landingPageId = event_details_api_resp.eventLandingPageId;
    eventInfoDto.landingPageId = landingPageId;
    // Updating event dto
    attendee_landing_page = eventInfoDto.attendeeLandingPage;
    speaker_landing_page = eventInfoDto.speakerLandingPage;

    //enable registration emails for speaker
    await enableDisableEmailTrigger(
      organiserApiContext,
      eventId,
      EventSettingID.EventInvitationFromEmailFromPeopleSection,
      true,
      EventRole.SPEAKER,
      true
    );

    //enable registration emails for attendee
    await enableDisableEmailTrigger(
      organiserApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );

    page = await context.newPage();
  });

  test.afterEach(async () => {
    await org_browser_context?.close();
  });

  test("TC001 Attendee is able to register in Reg Based Event by filling all custom fields", async () => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await addCustomFieldsToEvent(
      organiserApiContext,
      eventId,
      landingPageId,
      new EventCustomFieldFormObj(eventId, landingPageId).customFieldObj
    );
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendee_landing_page);
    // update the default reg fields to custom feilds

    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData.userCustomFieldFormData
    );
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    await expect(
      page.locator("div[class^='styles-module__logoHeaderBody']")
    ).toContainText("You've successfully registered!");
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC002 Attendee is able to register in Reg Based Event just by filling mandatory custom fields", async () => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await addCustomFieldsToEvent(
      organiserApiContext,
      eventId,
      landingPageId,
      new EventCustomFieldFormObj(eventId, landingPageId).customFieldObj
    );
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendee_landing_page);
    // update the default reg fields to custom feilds
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData.userCustomFieldFormData,
      {
        fillMandatoryFields: true,
        fillOptionalFields: false,
      }
    );
    userEventRoleDto.hasUserRegisteredToThisEvent = true;
    await expect(
      page.locator("div[class^='styles-module__logoHeaderBody']")
    ).toContainText("You've successfully registered!");
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).clickOnAlreadyRegisteredButton();
    await (
      await landingPageOne.getLoginOptionsComponent()
    ).submitEmailForOtpVerification(userEventRoleDto);
    await landingPageOne.clickOnEnterNowButton();
    await landingPageOne.isLobbyLoaded();
  });

  test("TC003 Attendee registration is blocked by error if any mandatory field is unfilled", async () => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await addCustomFieldsToEvent(
      organiserApiContext,
      eventId,
      landingPageId,
      new EventCustomFieldFormObj(eventId, landingPageId).customFieldObj
    );
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );
    let landingPageOne = new LandingPageOne(page);
    await landingPageOne.load(attendee_landing_page);
    // update the default reg fields to custom feilds
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      userInfoDTO.userRegFormData.userCustomFieldFormData,
      {
        fillMandatoryFields: false,
        fillOptionalFields: true,
      },
      false
    );
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).verifyThisFieldIsRequiredErrorMessageIsDisplayed();
  });

  test("TC004 Impact of adding mandatory custom field after attendee has registered", async ({
    context,
  }) => {
    /*
            1. add an event with custom field
            2. new attendee registers to the event by filling custom fields
            3. now organiser adds 1 mandatory field
            4. now verify if user checks in again to same event, prompt to fill mandatory field should be shown
        */
    // let context1 = await browser.newContext();
    // let context2 = await browser.newContext();
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let newCustomFieldObject = new EventCustomFieldFormObj(
      eventId,
      landingPageId
    ).getAnExtraMandatoryCustomFieldObj();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );

    await test.step("user registering to the event", async () => {
      await addCustomFieldsToEvent(
        organiserApiContext,
        eventId,
        landingPageId,
        new EventCustomFieldFormObj(eventId, landingPageId).customFieldObj
      );
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendee_landing_page);
      // update the default reg fields to custom feilds
      // update the default reg fields to custom feilds
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData.userCustomFieldFormData
      );
      userEventRoleDto.hasUserRegisteredToThisEvent = true;
      await expect(
        page.locator("div[class^='styles-module__logoHeaderBody']")
      ).toContainText("You've successfully registered!");
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).clickOnAlreadyRegisteredButton();
      await (
        await landingPageOne.getLoginOptionsComponent()
      ).submitEmailForOtpVerification(userEventRoleDto);
      await landingPageOne.clickOnEnterNowButton();
      await landingPageOne.isLobbyLoaded();
    });

    await test.step("Adding new mandatory custom field", async () => {
      await addCustomFieldsToEvent(
        organiserApiContext,
        eventId,
        landingPageId,
        newCustomFieldObject
      );
    });

    await test.step("Now user in a seperate browser instance try to login again to event", async () => {
      await context.clearCookies();
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendee_landing_page);
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).clickOnAlreadyRegisteredButton();
      const eventRoleApiPromise = page.waitForResponse(
        (response) =>
          response.url().includes(`/event/${eventId}/role`) &&
          response.status() === 200
      );
      await (
        await landingPageOne.getLoginOptionsComponent()
      ).submitEmailForOtpVerification(userEventRoleDto);
      // fill data in the new mandatory field which is visible
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).isRegistrationFormVisible();
      const getEventRoleResponse = await eventRoleApiPromise;
      expect(
        getEventRoleResponse.status(),
        "expecting event role api respoinse to be 200"
      ).toBe(200);

      await (
        await landingPageOne.getRegistrationFormComponent()
      ).enterDataInRegFormField(
        newCustomFieldObject[0]["labelName"], //since its a list of object
        newCustomFieldObject[0]["fieldType"].toLowerCase(),
        "chooseme"
      );
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).clickOnContinueButtonOnRegisterationForm();
      await landingPageOne.clickOnEnterNowButton();
      await landingPageOne.isLobbyLoaded();
    });
  });

  test("TC005 User even already registered should not be allowed to skip registration prompt \
        to fill new mandatory field if added after his registration", async ({
    context,
  }) => {
    /*
            1. add an event with custom field
            2. new attendee registers to the event by filling custom fields
            3. now organiser adds 1 mandatory field
            4. now verify if user checks in again to same event, prompt to fill mandatory field should be shown
        */
    // let context1 = await browser.newContext();
    // let context2 = await browser.newContext();
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let newCustomFieldObject = new EventCustomFieldFormObj(
      eventId,
      landingPageId
    ).getAnExtraMandatoryCustomFieldObj();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );

    await test.step("user registering to the event", async () => {
      await addCustomFieldsToEvent(
        organiserApiContext,
        eventId,
        landingPageId,
        new EventCustomFieldFormObj(eventId, landingPageId).customFieldObj
      );
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendee_landing_page);
      // update the default reg fields to custom feilds
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData.userCustomFieldFormData
      );
      userEventRoleDto.hasUserRegisteredToThisEvent = true;
      await expect(
        page.locator("div[class^='styles-module__logoHeaderBody']")
      ).toContainText("You've successfully registered!");
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).clickOnAlreadyRegisteredButton();
      await (
        await landingPageOne.getLoginOptionsComponent()
      ).submitEmailForOtpVerification(userEventRoleDto);
      await landingPageOne.clickOnEnterNowButton();
      await landingPageOne.isLobbyLoaded();
    });

    await test.step("Adding new mandatory custom field", async () => {
      await addCustomFieldsToEvent(
        organiserApiContext,
        eventId,
        landingPageId,
        newCustomFieldObject
      );
    });

    await test.step("Now user in a seperate browser instance try to login again to event and try to click \
        on continue button without filling registration form which comes after completing auth", async () => {
      await context.clearCookies();
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendee_landing_page);
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).clickOnAlreadyRegisteredButton();
      const eventRoleApiPromise = page.waitForResponse(
        (response) =>
          response.url().includes(`/event/${eventId}/role`) &&
          response.status() === 200
      );
      await (
        await landingPageOne.getLoginOptionsComponent()
      ).submitEmailForOtpVerification(userEventRoleDto);
      const getEventRoleResponse = await eventRoleApiPromise;
      expect(
        getEventRoleResponse.status(),
        "expecting event role api respoinse to be 200"
      ).toBe(200);
      // fill data in the new mandatory field which is visible
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).isRegistrationFormVisible();
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).clickOnContinueButtonOnRegisterationForm();
      // error should be thrown
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).verifyThisFieldIsRequiredErrorMessageIsDisplayed();
    });
  });

  test("TC006 Impact of adding only optional custom field after attendee has registered", async ({
    context,
  }) => {
    /*
            1. add an event with custom field
            2. new attendee registers to the event by filling custom fields
            3. now organiser adds 1 optional field
            4. now verify if user checks in again to same event, registration from compo  popup should not be shown
        */
    // let context1 = await browser.newContext();
    // let context2 = await browser.newContext();
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let newOptionalCustomFieldObject = new EventCustomFieldFormObj(
      eventId,
      landingPageId
    ).getAnExtraOptionalCustomFieldObj();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );

    await test.step("user registering to the event", async () => {
      await addCustomFieldsToEvent(
        organiserApiContext,
        eventId,
        landingPageId,
        new EventCustomFieldFormObj(eventId, landingPageId).customFieldObj
      );
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendee_landing_page);
      // update the default reg fields to custom feilds
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData.userCustomFieldFormData
      );
      userEventRoleDto.hasUserRegisteredToThisEvent = true;
      await expect(
        page.locator("div[class^='styles-module__logoHeaderBody']")
      ).toContainText("You've successfully registered!");
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).clickOnAlreadyRegisteredButton();
      await (
        await landingPageOne.getLoginOptionsComponent()
      ).submitEmailForOtpVerification(userEventRoleDto);
      await landingPageOne.clickOnEnterNowButton();
      await landingPageOne.isLobbyLoaded();
    });

    await test.step("Adding new mandatory custom field", async () => {
      await addCustomFieldsToEvent(
        organiserApiContext,
        eventId,
        landingPageId,
        newOptionalCustomFieldObject
      );
    });

    await test.step("Now after user auth, registration component should not be popped up", async () => {
      await context.clearCookies();
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendee_landing_page);

      await (
        await landingPageOne.getRegistrationFormComponent()
      ).clickOnAlreadyRegisteredButton();
      const eventRoleApiPromise = page.waitForResponse(
        (response) =>
          response.url().includes(`/event/${eventId}/role`) &&
          response.status() === 200
      );
      await (
        await landingPageOne.getLoginOptionsComponent()
      ).submitEmailForOtpVerification(userEventRoleDto);
      const getEventRoleResponse = await eventRoleApiPromise;
      expect(
        getEventRoleResponse.status(),
        "expecting event role api respoinse to be 200"
      ).toBe(200);
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).isNotVisible();
      await landingPageOne.clickOnEnterNowButton();
      await landingPageOne.isLobbyLoaded();
    });
  });

  test("TC007 Invited attendee by email should also get prompt to fill in new mandatory field", async () => {
    /*
            1. add an event with custom field
            2. new attendee registers to the event by filling custom fields
            3. now organiser adds 1 mandatory field
            4. now verify if user checks in again to same event, prompt to fill mandatory field should be shown
        */

    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await inviteAttendeeByAPI(organiserApiContext, eventId, attendeeEmail);
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );

    await test.step("user registering to the event", async () => {
      await addCustomFieldsToEvent(
        organiserApiContext,
        eventId,
        landingPageId,
        new EventCustomFieldFormObj(eventId, landingPageId).customFieldObj
      );
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendee_landing_page);
      // update the default reg fields to custom feilds
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).clickOnAlreadyRegisteredButton();
      const eventRoleApiPromise = page.waitForResponse(
        (response) =>
          response.url().includes(`/event/${eventId}/role`) &&
          response.status() === 200
      );
      await (
        await landingPageOne.getLoginOptionsComponent()
      ).submitEmailForOtpVerification(userEventRoleDto);
      const getEventRoleResponse = await eventRoleApiPromise;
      expect(
        getEventRoleResponse.status(),
        "expecting event role api respoinse to be 200"
      ).toBe(200);
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData.userCustomFieldFormData,
        {
          fillMandatoryFields: true,
          fillOptionalFields: true,
          clickOnContinueButton: true,
          fieldsToSkip: ["Email"],
        }
      );
      await landingPageOne.clickOnEnterNowButton();
      await landingPageOne.isLobbyLoaded();
    });
  });

  // not applicable for now
  test.skip("TC008 Impact of adding mandatory field after attendee has magic link for reg based event", async ({
    context,
  }) => {
    /*
            1. add an event with custom field
            2. new attendee registers to the event by filling custom fields
            3. now organiser adds 1 mandatory field
            4. now verify if user checks in again to same event, prompt to fill mandatory field should be shown
        */

    // let context1 = await browser.newContext();
    // let context2 = await browser.newContext();
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let attendeeMagicLink: string;
    await enableMagicLinkInEvent(
      organiserApiContext,
      eventId,
      landingPageId,
      EventEntryType.REG_BASED
    );
    let newCustomFieldObject = new EventCustomFieldFormObj(
      eventId,
      landingPageId
    ).getAnExtraMandatoryCustomFieldObj();
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      false
    );

    await test.step("user registering to the event", async () => {
      await addCustomFieldsToEvent(
        organiserApiContext,
        eventId,
        landingPageId,
        new EventCustomFieldFormObj(eventId, landingPageId).customFieldObj
      );
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendee_landing_page);
      // update the default reg fields to custom feilds
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData.userCustomFieldFormData
      );
      userEventRoleDto.hasUserRegisteredToThisEvent = true;
      await expect(
        page.locator("div[class^='styles-module__logoHeaderBody']")
      ).toContainText("You've successfully registered!");
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).clickOnAlreadyRegisteredButton();
      await (
        await landingPageOne.getLoginOptionsComponent()
      ).submitEmailForMagicLink(userEventRoleDto);
    });

    await test.step("Attendee checking his email to get the magic link", async () => {
      attendeeMagicLink = await fetchAttendeeInviteMagicLink(
        attendeeEmail,
        eventInfoDto.registrationConfirmationEmailSubj
      );
    });
    await test.step("Adding new mandatory custom field", async () => {
      await addCustomFieldsToEvent(
        organiserApiContext,
        eventId,
        landingPageId,
        newCustomFieldObject
      );
    });

    await test.step("Now user in a seperate browser instance attende loads magic link", async () => {
      await context.clearCookies();
      let landingPageOne = new LandingPageOne(page);
      const eventRoleApiPromise = page.waitForResponse(
        (response) =>
          response.url().includes(`/event/${eventId}/role`) &&
          response.status() === 200
      );
      await landingPageOne.load(attendeeMagicLink);

      const getEventRoleResponse = await eventRoleApiPromise;
      expect(
        getEventRoleResponse.status(),
        "expecting event role api respoinse to be 200"
      ).toBe(200);
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).enterDataInRegFormField(
        newCustomFieldObject[0]["labelName"], //since its a list of object
        newCustomFieldObject[0]["fieldType"].toLowerCase(),
        "chooseme"
      );
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).clickOnContinueButtonOnRegisterationForm();
      await landingPageOne.clickOnEnterNowButton();
      await landingPageOne.isLobbyLoaded();
    });
  });
  // not applicable for now
  test.skip("TC009 Impact of adding mandatory field after attendee has magic link for invite base event", async () => {
    /*
            1. add an event with custom field
            2. new attendee registers to the event by filling custom fields
            3. now organiser adds 1 mandatory field
            4. now verify if user checks in again to same event, prompt to fill mandatory field should be shown
        */
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let attendeeMagicLink: string;
    await enableMagicLinkInEvent(
      organiserApiContext,
      eventId,
      landingPageId,
      EventEntryType.INVITE_ONLY
    );
    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      false
    );
    userEventRoleDto = new UserEventRoleDto(
      userInfoDTO,
      eventInfoDto,
      EventRole.NOTEXISTS,
      true
    );

    await test.step("Inviting attendee to the event", async () => {
      await inviteAttendeeByAPI(organiserApiContext, eventId, attendeeEmail);
    });

    await test.step(`adding custom fields to the event`, async () => {
      await addCustomFieldsToEvent(
        organiserApiContext,
        eventId,
        landingPageId,
        new EventCustomFieldFormObj(eventId, landingPageId).customFieldObj
      );
    });

    await test.step("Attendee checking his email to get the magic link", async () => {
      attendeeMagicLink = await fetchAttendeeInviteMagicLink(
        attendeeEmail,
        eventInfoDto.getInviteEmailSubj()
      );
    });

    await test.step("Now user loads magic link , he should be prompted to fill the registration field", async () => {
      let landingPageOne = new LandingPageOne(page);
      const eventRoleApiPromise = page.waitForResponse(
        (response) =>
          response.url().includes(`/event/${eventId}/role`) &&
          response.status() === 200
      );
      await landingPageOne.load(attendeeMagicLink);

      const getEventRoleResponse = await eventRoleApiPromise;
      expect(
        getEventRoleResponse.status(),
        "expecting event role api respoinse to be 200"
      ).toBe(200);
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData.userCustomFieldFormData,
        {
          fillMandatoryFields: true,
          fillOptionalFields: true,
          clickOnContinueButton: true,
          fieldsToSkip: ["Email"],
        }
      );
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).clickOnContinueButtonOnRegisterationForm();
      await landingPageOne.clickOnEnterNowButton();
      await landingPageOne.isLobbyLoaded();
    });
  });
});
