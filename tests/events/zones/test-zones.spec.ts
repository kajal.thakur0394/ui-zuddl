import {
  APIRequestContext,
  BrowserContext,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { updateEventLandingPageDetails } from "../../../util/apiUtil";
import EventEntryType from "../../../enums/eventEntryEnum";
import { VenueController } from "../../../controller/VenueController";
import { VenuePage } from "../../../page-objects/events-pages/zones/VenuePage";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { TopNavBarComponent } from "../../../page-objects/events-pages/live-side-components/TopNavBarComponent";
import { AccessComponent } from "../../../page-objects/events-pages/live-side-components/AccessComponent";
import ZoneType from "../../../enums/ZoneTypeEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@zones @zone-enable/disable`, async () => {
  let eventId;
  let event_title;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventController: EventController;
  let venueController: VenueController;
  let attendeeEmail: string;
  let speakerEmail: string;
  let organiserLink: string;
  let venuePage: VenuePage;
  let orgNavPage: TopNavBarComponent;
  let attendeeNavPage: TopNavBarComponent;
  let speakerNavPage: TopNavBarComponent;
  let speakerBrowserContext: BrowserContext;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });

    speakerBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    orgApiContext = orgBrowserContext.request;
    event_title = DataUtl.getRandomEventTitle();
    await test.step(`Create a new event`, async () => {
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: event_title,
      });
      console.log(`event id is ${eventId}`);
    });
    eventController = new EventController(orgApiContext, eventId);
    venueController = new VenueController(orgApiContext, eventId);

    await test.step(`Enable magic link for this event`, async () => {
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
      });
    });
    await test.step(`Invite new attendee to the event`, async () => {
      attendeeEmail = DataUtl.getRandomAttendeeEmail();
      await eventController.inviteAttendeeToTheEvent(attendeeEmail);
    });
    await test.step(`Invite new speaker to the event`, async () => {
      speakerEmail = DataUtl.getRandomSpeakerEmail();
      await eventController.inviteSpeakerToTheEventByApi(
        speakerEmail,
        "speakerName",
        "speakerLastName"
      );
    });
    await test.step(`Invite new organiser to the event`, async () => {
      organiserLink = new EventInfoDTO(eventId, event_title)
        .attendeeLandingPage;
    });

    await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
      false
    );
  });

  test.afterEach(async () => {
    await eventController.cancelAnEvent();
    await orgBrowserContext?.close();
    await speakerBrowserContext?.close();
  });

  test(`TC001: Verify organiser is able to disable the zone from organiser's side`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-262/verify-organiser-is-able-to-disable-the-zone-from-org-side",
    });
    let orgPage = await orgBrowserContext.newPage();
    await test.step(`verify Organiser is able to disable ROOMS,EXPO from API`, async () => {
      let zonestoUpdate = new Map<ZoneType, boolean>();
      zonestoUpdate.set(ZoneType.ROOMS, false);
      zonestoUpdate.set(ZoneType.SCHEDULE, false);
      await venueController.updateVenueZoneStatus(zonestoUpdate);
      await venueController.verifyZoneUpdationViaAPI(zonestoUpdate);
    });

    await test.step(`verify Organiser is able to enable ROOMS,EXPO from API`, async () => {
      let zonestoUpdate = new Map<ZoneType, boolean>();
      zonestoUpdate.set(ZoneType.ROOMS, true);
      zonestoUpdate.set(ZoneType.SCHEDULE, true);
      await venueController.updateVenueZoneStatus(zonestoUpdate);
      await venueController.verifyZoneUpdationViaAPI(zonestoUpdate);
    });

    await test.step(`verify Organiser is able to disable lobby zone from UI`, async () => {
      let venueURL = `${
        DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
      }/event/${eventId}/venue-setup/venuesettings`;
      await orgPage.goto(venueURL);
      venuePage = new VenuePage(orgPage);
      await venuePage.toggleZone(ZoneType.LOBBY);
      await venuePage.verifyZoneisDisabledFromUI(ZoneType.LOBBY);
    });
    await test.step(`verify Organiser is able to enable lobby zone from UI`, async () => {
      await venuePage.toggleZone(ZoneType.LOBBY);
      await venuePage.verifyZoneisEnabledFromUI(ZoneType.LOBBY);
    });
  });

  test(`TC002: verify if a zone is disabled, same is reflected on the live event site`, async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-263/verify-if-a-zone-is-disabled-same-is-reflected-on-the-live-event-site",
    });
    let orgPage = await orgBrowserContext.newPage();
    let speakerPage = await speakerBrowserContext.newPage();

    await test.step(`Organiser disables the NETWORKING from API`, async () => {
      let zonestoUpdate = new Map<ZoneType, boolean>();
      zonestoUpdate.set(ZoneType.NETWORKING, false);
      await venueController.updateVenueZoneStatus(zonestoUpdate);
      await venueController.verifyZoneUpdationViaAPI(zonestoUpdate);
    });

    await test.step(`Attendee fetches attendee magic link and logs into the event`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await page.goto(attendeeMagicLink);
      await page.click("text=Enter Now");
    });

    await test.step(`Organiser logs into the event`, async () => {
      await orgPage.goto(organiserLink);
      await orgPage.locator("text=Enter Now").click();
      await orgPage
        .locator("button[class^='styles-module__modalClose___']")
        .click();
    });

    await test.step(`Speaker fetches Speaker magic link and logs into the event`, async () => {
      const speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );
      await speakerPage.goto(speakerMagicLink);
      const enterNowButton = speakerPage.locator("text=Enter Now");
      await enterNowButton.click();
      await speakerPage
        .locator("button[class^='styles-module__modalClose___']")
        .click();
    });

    await test.step(`Organiser/Attendee/Speaker should not be able to see NETWORKING in live event page`, async () => {
      orgNavPage = new TopNavBarComponent(orgPage);
      await orgNavPage.verifyZoneDisabledonLiveEvent(ZoneType.NETWORKING);
      await orgNavPage.verifyZoneDisabledonLiveEvent(ZoneType.NETWORKING);
      speakerNavPage = new TopNavBarComponent(speakerPage);
      await orgNavPage.verifyZoneDisabledonLiveEvent(ZoneType.NETWORKING);
      attendeeNavPage = new TopNavBarComponent(page);
      await attendeeNavPage.verifyZoneDisabledonLiveEvent(ZoneType.NETWORKING);
    });

    await test.step(`Organiser again enables NETWORKING zone via API`, async () => {
      let zonestoUpdate = new Map<ZoneType, boolean>();
      zonestoUpdate.set(ZoneType.NETWORKING, true);
      await venueController.updateVenueZoneStatus(zonestoUpdate);
      await venueController.verifyZoneUpdationViaAPI(zonestoUpdate);
    });

    await test.step(`Organiser/Attendee/Speaker should now be able to see NETWORKING in live event page`, async () => {
      await orgPage.reload();
      await orgPage
        .locator("button[class^='styles-module__modalClose___']")
        .click();
      await orgNavPage.verifyZoneEnabledeonLiveEvent(ZoneType.NETWORKING);
      await speakerPage.reload();
      await speakerPage
        .locator("button[class^='styles-module__modalClose___']")
        .click();
      await speakerNavPage.verifyZoneEnabledeonLiveEvent(ZoneType.NETWORKING);
      await page.reload();
      await attendeeNavPage.verifyZoneEnabledeonLiveEvent(ZoneType.NETWORKING);
    });
  });

  test(`TC003: verify if a zone is disabled and user try to navigate by url, we show no access page for user`, async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-264/verify-if-a-zone-is-disabled-and-user-try-to-navigate-by-url-we-show",
    });
    let orgPage = await orgBrowserContext.newPage();
    let speakerPage = await speakerBrowserContext.newPage();
    let orgAccessPage: AccessComponent;
    let speakerAccessPage: AccessComponent;
    let attendeeAccessPage: AccessComponent;
    let eventURls = new Map<ZoneType, string>();
    let stageId: string;
    let baseEventUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/`;
    eventURls.set(ZoneType.LOBBY, baseEventUrl + `${eventId}/lobby`);
    eventURls.set(ZoneType.SCHEDULE, baseEventUrl + `${eventId}/schedule`);
    eventURls.set(ZoneType.STAGE, baseEventUrl + `${eventId}/stages/`);
    eventURls.set(
      ZoneType.ROOMS,
      baseEventUrl + `${eventId}/discussions?redirect=true`
    );
    eventURls.set(ZoneType.EXPO, baseEventUrl + `${eventId}/expo`);
    eventURls.set(ZoneType.NETWORKING, baseEventUrl + `${eventId}/networking`);

    await test.step(`Organiser disables the EXPO from API`, async () => {
      let zonestoUpdate = new Map<ZoneType, boolean>();
      zonestoUpdate.set(ZoneType.EXPO, false);
      await venueController.updateVenueZoneStatus(zonestoUpdate);
      await venueController.verifyZoneUpdationViaAPI(zonestoUpdate);
    });

    await test.step(`Attendee fetches attendee magic link and logs into the event`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await page.goto(attendeeMagicLink);
      await page.click("text=Enter Now");
    });

    await test.step(`Organiser logs into the event`, async () => {
      await orgPage.goto(organiserLink);
      await orgPage.locator("text=Enter Now").click();
      await orgPage
        .locator("button[class^='styles-module__modalClose___']")
        .click();
    });

    await test.step(`Speaker fetches Speaker magic link and logs into the event`, async () => {
      const speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );
      await speakerPage.goto(speakerMagicLink);
      const enterNowButton = speakerPage.locator("text=Enter Now");
      await enterNowButton.click();
      await speakerPage
        .locator("button[class^='styles-module__modalClose___']")
        .click();
    });

    await test.step(`Organiser/Attendee/Speaker should not be able to see EXPO in live event page`, async () => {
      orgNavPage = new TopNavBarComponent(orgPage);
      await orgNavPage.verifyZoneDisabledonLiveEvent(ZoneType.EXPO);
      speakerNavPage = new TopNavBarComponent(speakerPage);
      await orgNavPage.verifyZoneDisabledonLiveEvent(ZoneType.EXPO);
      attendeeNavPage = new TopNavBarComponent(page);
      await attendeeNavPage.verifyZoneDisabledonLiveEvent(ZoneType.EXPO);
    });

    await test.step(`Organiser/Attendee/Speaker try to navigate via EXPO-URL in live event page`, async () => {
      orgAccessPage = new AccessComponent(orgPage);
      attendeeAccessPage = new AccessComponent(page);
      speakerAccessPage = new AccessComponent(speakerPage);
      const destinationUrl = eventURls.get(ZoneType.EXPO) || "";
      if (destinationUrl != "") {
        await orgPage.goto(destinationUrl);
        await orgAccessPage.verifyNoAccessMessageVisible();
        await page.goto(destinationUrl);
        await attendeeAccessPage.verifyNoAccessMessageVisible();
        await speakerPage.goto(destinationUrl);
        await speakerAccessPage.verifyNoAccessMessageVisible();
      }
    });
  });

  test(`TC004: Verify if lobby is disabled, then attendee after login should land on STAGE`, async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-361/if-lobby-is-disbled-then-attendee-after-login-should-land-on-stage",
    });

    await test.step(`Organiser disables the LOBBY from API`, async () => {
      let zonestoUpdate = new Map<ZoneType, boolean>();
      zonestoUpdate.set(ZoneType.LOBBY, false);
      await venueController.updateVenueZoneStatus(zonestoUpdate);
      await venueController.verifyZoneUpdationViaAPI(zonestoUpdate);
    });

    await test.step(`Attendee fetches attendee magic link and logs into the event`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await page.goto(attendeeMagicLink);
      await page.click("text=Enter Now");
    });

    await test.step(`Verify as an attendee, they should be redirected to STAGE`, async () => {
      await page.waitForURL(/stages/);
      expect(page.url()).toContain("stages");
    });
  });

  test(`TC005: Verify if STAGE is disabled, then speaker/organiser should land on SCHEDULE`, async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-362/if-stage-is-disable-the-speakerorganiser-should-land-on-schedule",
    });
    let orgPage = await orgBrowserContext.newPage();
    let speakerPage = await speakerBrowserContext.newPage();

    await test.step(`Organiser disables the STAGE from API`, async () => {
      let zonestoUpdate = new Map<ZoneType, boolean>();
      zonestoUpdate.set(ZoneType.STAGE, false);
      await venueController.updateVenueZoneStatus(zonestoUpdate);
      await venueController.verifyZoneUpdationViaAPI(zonestoUpdate);
    });

    await test.step(`Organiser logs into the event`, async () => {
      await orgPage.goto(organiserLink);
      await orgPage.locator("text=Enter Now").click();
    });

    await test.step(`Speaker fetches Speaker magic link and logs into the event`, async () => {
      const speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        eventId,
        speakerEmail
      );
      await speakerPage.goto(speakerMagicLink);
      const enterNowButton = speakerPage.locator("text=Enter Now");
      await enterNowButton.click();
    });

    await test.step(`Verify as an Organiser, they should be redirected to SCHEDULE`, async () => {
      await orgPage.waitForURL(/schedule/);
      expect(orgPage.url()).toContain("schedule");
    });

    await test.step(`Verify as a Speaker, they should be redirected to SCHEDULE`, async () => {
      await speakerPage.waitForURL(/schedule/);
      expect(speakerPage.url()).toContain("schedule");
    });
  });
});
