import {
  APIRequestContext,
  Page,
  expect,
  test,
  BrowserContext,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { EventController } from "../../../controller/EventController";
import {
  updateEventLandingPageDetails,
  updateLandingPageType,
} from "../../../util/apiUtil";
import { DataUtl } from "../../../util/dataUtil";
import EventEntryType from "../../../enums/eventEntryEnum";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import LandingPageType from "../../../enums/landingPageEnum";
import { LandingPageTwo } from "../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(
  "@registration @utm-params",
  {
    tag: "@smoke",
  },
  async () => {
    let orgBrowserContext: BrowserContext;
    let orgApiContext: APIRequestContext;
    let eventController: EventController;
    let eventTitle: string;
    let eventId;
    let attendeeLandingPage: string;
    let attendeeLandingPageWithUtmParams: string;
    let utm_source: string;
    let utm_medium: string;

    test.beforeEach(async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({});
      orgApiContext = orgBrowserContext.request;
      //create new event
      eventTitle = DataUtl.getRandomEventTitle();
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: eventTitle,
      });
      //creating event controller to create a new event
      eventController = new EventController(orgApiContext, eventId);

      //enable magic link to the event
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isSpeakermagicLinkEnabled: true,
        isMagicLinkEnabled: true,
      });

      //get the attendee landing page
      attendeeLandingPage = new EventInfoDTO(eventId, eventTitle, true)
        .attendeeLandingPage;
      //append this landing page with UTM sources
      utm_source = "automation";
      utm_medium = "playwright";
      attendeeLandingPageWithUtmParams = `${attendeeLandingPage.replace(
        "/m",
        ""
      )}?utm_source=${utm_source}&utm_medium=${utm_medium}`;
      console.log(
        `Attendee landing page with utm params appended is ${attendeeLandingPageWithUtmParams}`
      );
    });

    test.afterEach(async () => {
      await orgBrowserContext?.close();
    });

    test("@LP1 @incognito TC001 verify registration payload carries correct utm params if appended with landing page url in incognito mode", async ({
      browser,
    }) => {
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-238/validation-for-lp1",
      });
      //new incognito page
      let incognitoContext = await browser.newContext();
      let page = await incognitoContext.newPage();
      let userEmail = DataUtl.getRandomAttendeeEmail();
      //open the event landing page with
      let landingPageOne = new LandingPageOne(page);
      await test.step(`Loading the landing page appended with utm params ${attendeeLandingPageWithUtmParams}`, async () => {
        await landingPageOne.load(attendeeLandingPageWithUtmParams);
      });

      await test.step(`Filling up the registration form with valid reg data for user ${userEmail}`, async () => {
        //fill up the registration form
        let userRegistrationFormObj = new UserInfoDTO(
          userEmail,
          "",
          "prateek",
          "automation",
          false
        );
        //fill up the registration form
        await landingPageOne.registrationFormComponent.fillUpTheRegistrationForm(
          userRegistrationFormObj.userRegFormData
            .userRegFormDataForDefaultFields
        );
      });

      await test.step(`Now fetching the user entry from event_registration table and validate the utm params`, async () => {
        //verify in DB event_registration table, the utm param exists as expected
        const userRegDataFromDb =
          await QueryUtil.fetchUserEventRegistrationDataFromDb(
            eventId,
            userEmail
          );
        console.log(userRegDataFromDb);
        //fetch the utm param from db for registration record
        const utmParamsFromDb = userRegDataFromDb["utm"];
        //validate the utm params
        //const utmParamJson = JSON.parse(utmParamsFromDb);
        //validate the key and value
        await test.step(`validating the utm_medium param from db entry`, async () => {
          expect(
            utmParamsFromDb["utm_medium"],
            `expecting utm_medium to be ${utm_medium}`
          ).toBe(utm_medium);
        });

        await test.step(`validating the utm_source param from db entry`, async () => {
          expect(
            utmParamsFromDb["utm_source"],
            `expecting utm_source to be ${utm_source}`
          ).toBe(utm_source);
        });
      });
    });

    test("@LP2 @incognito TC002 verify registration payload carries correct utm params if appended with landing page url in incognito mode", async ({
      browser,
    }) => {
      test.info().annotations.push({
        type: "TC",
        description:
          "https://linear.app/zuddl/issue/QAT-239/validation-for-lp2",
      });
      await test.step(`Update event landing page template to landing page 2`, async () => {
        const landingPageId = await eventController.getEventLandingPageId();
        await updateLandingPageType(
          orgApiContext,
          eventId,
          landingPageId,
          LandingPageType.STYLE2,
          "true",
          EventEntryType.REG_BASED
        );
      });
      //new incognito page
      let incognitoContext = await browser.newContext();
      let page = await incognitoContext.newPage();
      let userEmail = DataUtl.getRandomAttendeeEmail();
      //open the event landing page with
      let landingPageTwo = new LandingPageTwo(page);
      await test.step(`Loading the landing page appended with utm params ${attendeeLandingPageWithUtmParams}`, async () => {
        await landingPageTwo.load(attendeeLandingPageWithUtmParams);
      });

      await test.step(`Filling up the registration form with valid reg data for user ${userEmail}`, async () => {
        //fill up the registration form
        let userRegistrationFormObj = new UserInfoDTO(
          userEmail,
          "",
          "prateek",
          "automation",
          false
        );
        //fill up the registration form
        await landingPageTwo.registrationFormComponent.fillUpTheRegistrationForm(
          userRegistrationFormObj.userRegFormData
            .userRegFormDataForDefaultFields
        );
      });

      await test.step(`Now fetching the user entry from event_registration table and validate the utm params`, async () => {
        //verify in DB event_registration table, the utm param exists as expected
        const userRegDataFromDb =
          await QueryUtil.fetchUserEventRegistrationDataFromDb(
            eventId,
            userEmail
          );
        //fetch the utm param from db for registration record
        const utmParamsFromDb = userRegDataFromDb["utm"];
        //validate the utm params
        //const utmParamJson = JSON.parse(utmParamsFromDb);
        //validate the key and value
        await test.step(`validating the utm_medium param from db entry`, async () => {
          expect(
            utmParamsFromDb["utm_medium"],
            `expecting utm_medium to be ${utm_medium}`
          ).toBe(utm_medium);
        });

        await test.step(`validating the utm_source param from db entry`, async () => {
          expect(
            utmParamsFromDb["utm_source"],
            `expecting utm_source to be ${utm_source}`
          ).toBe(utm_source);
        });
      });
    });
  }
);
