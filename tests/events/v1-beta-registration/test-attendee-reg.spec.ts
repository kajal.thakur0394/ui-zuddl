import {
  APIRequestContext,
  BrowserContext,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import EventRole from "../../../enums/eventRoleEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { SpeakerData } from "../../../test-data/speakerData";
import {
  createNewEvent,
  getRegistrationId,
  updateEventLandingPageDetails,
  checkInAttendee,
  deleteAttendeeFromEvent,
  inviteSpeakerByApi,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel(`@v1beta @registration @attendee`, async () => {
  test.describe.configure({ retries: 2 });
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventTitle: string;
  let eventId;
  let eventController: EventController;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiContext = orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: eventTitle,
    });
    eventController = new EventController(orgApiContext, eventId);
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`TC001: End to End flow of registering a new attendee with v1 beta api to reg based event where magic link is enabled`, async ({
    page,
  }) => {
    const attendeeEmail = DataUtl.getRandomAttendeeTestJoynEmail();
    const attendeeFirstName = "AutomationFirstName";
    const attendeeLastName = "AutomationLastName";
    const attendeeCompany = "Zuddl";
    const attendeeDesignation = "QA";
    const attendeePhoneNumber = "1234567890";
    let eventRoleIdFromV1BetaApiResp;

    await test.step(`Making event entry type to Reg based and Enabling magic link to the event`, async () => {
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
      });
    });

    await test.step(`Hitting v1 API call with payload:  in order to register ${attendeeEmail} to event`, async () => {
      const v1BetaApiResp =
        await eventController.registerUserToEventWithV1BetaAPI({
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          email: attendeeEmail,
          company: attendeeCompany,
          designation: attendeeDesignation,
          phoneNumber: attendeePhoneNumber,
        });
      expect(
        v1BetaApiResp.status(),
        "expecting v1 api status code to be 200"
      ).toBe(200);
      const v1BetaApiRespJson = await v1BetaApiResp.json();
      const uniqueLinkFromResp = v1BetaApiRespJson["unique_link"];
      console.log(
        `unique link from v1 beta api resp to be ${uniqueLinkFromResp}`
      );
      expect(
        uniqueLinkFromResp,
        `expecting unique link in resp ${uniqueLinkFromResp} to have event id : ${eventId}`
      ).toContain(eventId);
      eventRoleIdFromV1BetaApiResp = uniqueLinkFromResp.split("?r=")[1];
      expect(
        eventRoleIdFromV1BetaApiResp,
        `expecting event role  id in unique link ${eventRoleIdFromV1BetaApiResp} not to be undefined`
      ).not.toBeUndefined();
    });

    await test.step(`Fetching registered user data from DB which contains registration, account and event role data`, async () => {
      const userRegDataFroDb =
        await QueryUtil.fetchUserDataForV1BetaFlowValidation(
          eventId,
          attendeeEmail
        );
      expect(userRegDataFroDb).not.toBeNull();
      await test.step(`validating the db entry in account, event_registration and event role for key columns`, async () => {
        expect(userRegDataFroDb["email"]).toBe(attendeeEmail);
        expect(userRegDataFroDb["event_id"]).toBe(eventId);
        expect(userRegDataFroDb["reg_first_name"]).toBe(attendeeFirstName);
        expect(userRegDataFroDb["reg_last_name"]).toBe(attendeeLastName);
        expect(userRegDataFroDb["reg_designation"]).toBe(attendeeDesignation);
        expect(userRegDataFroDb["reg_company"]).toBe(attendeeCompany);
        expect(userRegDataFroDb["reg_phone_number"]).toBe(attendeePhoneNumber);
        expect(userRegDataFroDb["account_first_name"]).toBe(attendeeFirstName);
        expect(userRegDataFroDb["account_last_name"]).toBe(attendeeLastName);
        expect(userRegDataFroDb["account_designation"]).toBe(
          attendeeDesignation
        );
        expect(userRegDataFroDb["account_company"]).toBe(attendeeCompany);
        expect(userRegDataFroDb["account_phone_number"]).toBe(
          attendeePhoneNumber
        );
        expect(userRegDataFroDb["role"]).toBe("ATTENDEE");
        expect(userRegDataFroDb["status"]).toBe("REGISTERED");
        expect(userRegDataFroDb["event_role_id"]).toBe(
          eventRoleIdFromV1BetaApiResp
        );
      });
    });

    await test.step(`fetching magic link from DB for the ${attendeeEmail}`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await page.goto(attendeeMagicLink);
      await page.click("text=Enter Now");
    });
  });

  test(`TC002: End to End flow of registering a new attendee with v1 beta api to invite based event where magic link is enabled`, async ({
    page,
  }) => {
    const attendeeEmail = DataUtl.getRandomAttendeeTestJoynEmail();
    const attendeeFirstName = "AutomationFirstName";
    const attendeeLastName = "AutomationLastName";
    const attendeeCompany = "Zuddl";
    const attendeeDesignation = "QA";
    const attendeePhoneNumber = "1234567890";

    await test.step(`Making event entry type to Reg based and Enabling magic link to the event`, async () => {
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.INVITE_ONLY,
        isMagicLinkEnabled: true,
      });
    });

    await test.step(`Hitting v1 API call with payload:  in order to register ${attendeeEmail} to event`, async () => {
      const v1BetaApiResp =
        await eventController.registerUserToEventWithV1BetaAPI({
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          email: attendeeEmail,
          company: attendeeCompany,
          designation: attendeeDesignation,
          phoneNumber: attendeePhoneNumber,
        });
      expect(
        v1BetaApiResp.status(),
        "expecting v1 api status code to be 200"
      ).toBe(200);
      const v1BetaApiRespJson = await v1BetaApiResp.json();
      const uniqueLinkFromResp = v1BetaApiRespJson["unique_link"];
      console.log(
        `unique link from v1 beta api resp to be ${uniqueLinkFromResp}`
      );
      expect(
        uniqueLinkFromResp,
        `expecting unique link in resp ${uniqueLinkFromResp} to have event id : ${eventId}`
      ).toContain(eventId);
      const registrationId = uniqueLinkFromResp.split("?r=")[1];
      expect(
        registrationId,
        `expecting registration id in unique link ${registrationId} not to be undefined`
      ).not.toBeUndefined();
    });

    await test.step(`Fetching registered user data from DB which contains registration, account and event role data`, async () => {
      const userRegDataFroDb =
        await QueryUtil.fetchUserDataForV1BetaFlowValidation(
          eventId,
          attendeeEmail
        );
      expect(userRegDataFroDb).not.toBeNull();
      await test.step(`validating the db entry in account, event_registration and event role for key columns`, async () => {
        expect(userRegDataFroDb["email"]).toBe(attendeeEmail);
        expect(userRegDataFroDb["event_id"]).toBe(eventId);
        expect(userRegDataFroDb["reg_first_name"]).toBe(attendeeFirstName);
        expect(userRegDataFroDb["reg_last_name"]).toBe(attendeeLastName);
        expect(userRegDataFroDb["reg_designation"]).toBe(attendeeDesignation);
        expect(userRegDataFroDb["reg_company"]).toBe(attendeeCompany);
        expect(userRegDataFroDb["reg_phone_number"]).toBe(attendeePhoneNumber);
        expect(userRegDataFroDb["account_first_name"]).toBe(attendeeFirstName);
        expect(userRegDataFroDb["account_last_name"]).toBe(attendeeLastName);
        expect(userRegDataFroDb["account_designation"]).toBe(
          attendeeDesignation
        );
        expect(userRegDataFroDb["account_company"]).toBe(attendeeCompany);
        expect(userRegDataFroDb["account_phone_number"]).toBe(
          attendeePhoneNumber
        );
        expect(userRegDataFroDb["role"]).toBe("ATTENDEE");
        expect(userRegDataFroDb["status"]).toBe("REGISTERED");
      });
    });

    await test.step(`fetching magic link from DB for the ${attendeeEmail}`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      await page.goto(attendeeMagicLink);
      await page.click("text=Enter Now");
    });
  });

  test(`TC003: End to End flow of registering a new attendee with v1 beta api to invite only event where magic link is disabled`, async ({
    page,
  }) => {
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
    const attendeeFirstName = "AutomationFirstName";
    const attendeeLastName = "AutomationLastName";
    const attendeeCompany = "Zuddl";
    const attendeeDesignation = "QA";
    const attendeePhoneNumber = "1234567890";
    let uniqueLinkFromResp: string;

    await test.step(`Making event entry type to Reg based and Enabling magic link to the event`, async () => {
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.INVITE_ONLY,
        isMagicLinkEnabled: false,
      });
    });

    await test.step(`Hitting v1 API call with payload:  in order to register ${attendeeEmail} to event`, async () => {
      const v1BetaApiResp =
        await eventController.registerUserToEventWithV1BetaAPI({
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          email: attendeeEmail,
          company: attendeeCompany,
          designation: attendeeDesignation,
          phoneNumber: attendeePhoneNumber,
        });
      expect(
        v1BetaApiResp.status(),
        "expecting v1 api status code to be 200"
      ).toBe(200);
      const v1BetaApiRespJson = await v1BetaApiResp.json();
      uniqueLinkFromResp = v1BetaApiRespJson["unique_link"];
      console.log(
        `unique link from v1 beta api resp to be ${uniqueLinkFromResp}`
      );
      expect(
        uniqueLinkFromResp,
        `expecting unique link in resp ${uniqueLinkFromResp} to have event id : ${eventId}`
      ).toContain(eventId);
      const registrationId = uniqueLinkFromResp.split("?r=")[1];
      expect(
        registrationId,
        `expecting registration id in unique link ${registrationId} not to be undefined`
      ).not.toBeUndefined();
    });

    await test.step(`Fetching registered user data from DB which contains registration, account and event role data`, async () => {
      const userRegDataFroDb =
        await QueryUtil.fetchUserDataForV1BetaFlowValidation(
          eventId,
          attendeeEmail
        );
      expect(userRegDataFroDb).not.toBeNull();
      await test.step(`validating the db entry in account, event_registration and event role for key columns`, async () => {
        expect(userRegDataFroDb["email"]).toBe(attendeeEmail);
        expect(userRegDataFroDb["event_id"]).toBe(eventId);
        expect(userRegDataFroDb["reg_first_name"]).toBe(attendeeFirstName);
        expect(userRegDataFroDb["reg_last_name"]).toBe(attendeeLastName);
        expect(userRegDataFroDb["reg_designation"]).toBe(attendeeDesignation);
        expect(userRegDataFroDb["reg_company"]).toBe(attendeeCompany);
        expect(userRegDataFroDb["reg_phone_number"]).toBe(attendeePhoneNumber);
        expect(userRegDataFroDb["account_first_name"]).toBe(attendeeFirstName);
        expect(userRegDataFroDb["account_last_name"]).toBe(attendeeLastName);
        expect(userRegDataFroDb["account_designation"]).toBe(
          attendeeDesignation
        );
        expect(userRegDataFroDb["account_company"]).toBe(attendeeCompany);
        expect(userRegDataFroDb["account_phone_number"]).toBe(
          attendeePhoneNumber
        );
        expect(userRegDataFroDb["role"]).toBe("ATTENDEE");
        expect(userRegDataFroDb["status"]).toBe("REGISTERED");
      });
    });

    await test.step(`Attendee logs in via otp from landing page`, async () => {
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(uniqueLinkFromResp);
      let userInfoDto = new UserInfoDTO(
        attendeeEmail,
        " ",
        attendeeFirstName,
        attendeeLastName,
        true
      );
      let eventInfoDto = new EventInfoDTO(eventId, eventTitle, true);
      let userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.ATTENDEE,
        true
      );
      await (
        await landingPageOne.clickOnLoginButton()
      ).submitEmailForOtpVerification(userEventRoleDto);
      await landingPageOne.clickOnEnterNowButton();
      await landingPageOne.isLobbyLoaded();
    });
  });

  test(`TC004: End to End flow of registering a new attendee with v1 beta api to reg based  event where magic link is disabled`, async ({
    page,
  }) => {
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
    const attendeeFirstName = "AutomationFirstName";
    const attendeeLastName = "AutomationLastName";
    const attendeeCompany = "Zuddl";
    const attendeeDesignation = "QA";
    const attendeePhoneNumber = "1234567890";
    let uniqueLinkFromResp: string;

    await test.step(`Making event entry type to Reg based and Enabling magic link to the event`, async () => {
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: false,
      });
    });

    await test.step(`Hitting v1 API call with payload:  in order to register ${attendeeEmail} to event`, async () => {
      const v1BetaApiResp =
        await eventController.registerUserToEventWithV1BetaAPI({
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          email: attendeeEmail,
          company: attendeeCompany,
          designation: attendeeDesignation,
          phoneNumber: attendeePhoneNumber,
        });
      expect(
        v1BetaApiResp.status(),
        "expecting v1 api status code to be 200"
      ).toBe(200);
      const v1BetaApiRespJson = await v1BetaApiResp.json();
      uniqueLinkFromResp = v1BetaApiRespJson["unique_link"];
      console.log(
        `unique link from v1 beta api resp to be ${uniqueLinkFromResp}`
      );
      expect(
        uniqueLinkFromResp,
        `expecting unique link in resp ${uniqueLinkFromResp} to have event id : ${eventId}`
      ).toContain(eventId);
      const registrationId = uniqueLinkFromResp.split("?r=")[1];
      expect(
        registrationId,
        `expecting registration id in unique link ${registrationId} not to be undefined`
      ).not.toBeUndefined();
    });

    await test.step(`Fetching registered user data from DB which contains registration, account and event role data`, async () => {
      const userRegDataFroDb =
        await QueryUtil.fetchUserDataForV1BetaFlowValidation(
          eventId,
          attendeeEmail
        );
      expect(userRegDataFroDb).not.toBeNull();
      await test.step(`validating the db entry in account, event_registration and event role for key columns`, async () => {
        expect(userRegDataFroDb["email"]).toBe(attendeeEmail);
        expect(userRegDataFroDb["event_id"]).toBe(eventId);
        expect(userRegDataFroDb["reg_first_name"]).toBe(attendeeFirstName);
        expect(userRegDataFroDb["reg_last_name"]).toBe(attendeeLastName);
        expect(userRegDataFroDb["reg_designation"]).toBe(attendeeDesignation);
        expect(userRegDataFroDb["reg_company"]).toBe(attendeeCompany);
        expect(userRegDataFroDb["reg_phone_number"]).toBe(attendeePhoneNumber);
        expect(userRegDataFroDb["account_first_name"]).toBe(attendeeFirstName);
        expect(userRegDataFroDb["account_last_name"]).toBe(attendeeLastName);
        expect(userRegDataFroDb["account_designation"]).toBe(
          attendeeDesignation
        );
        expect(userRegDataFroDb["account_company"]).toBe(attendeeCompany);
        expect(userRegDataFroDb["account_phone_number"]).toBe(
          attendeePhoneNumber
        );
        expect(userRegDataFroDb["role"]).toBe("ATTENDEE");
        expect(userRegDataFroDb["status"]).toBe("REGISTERED");
      });
    });

    await test.step(`Attendee logs in via otp from landing page`, async () => {
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(uniqueLinkFromResp);
      let userInfoDto = new UserInfoDTO(
        attendeeEmail,
        " ",
        attendeeFirstName,
        attendeeLastName,
        true
      );
      let eventInfoDto = new EventInfoDTO(eventId, eventTitle, true);
      let userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.ATTENDEE,
        true
      );
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).clickOnAlreadyRegisteredButton();
      await (
        await landingPageOne.getLoginOptionsComponent()
      ).submitEmailForOtpVerification(userEventRoleDto);
      await landingPageOne.clickOnEnterNowButton();
      await landingPageOne.isLobbyLoaded();
    });
  });

  test(`TC005: v1 beta api call with an email which is already registered but has not checked in to the event`, async ({
    page,
  }) => {
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
    const attendeeFirstName = "AutomationFirstName";
    const attendeeLastName = "AutomationLastName";
    const attendeeCompany = "Zuddl";
    const attendeeDesignation = "QA";
    const attendeePhoneNumber = "1234567890";
    let uniqueLinkFromResp: string;
    let registrationIdBeforeRegistration;
    let eventRoleIdFromUniqueLinnk;

    await test.step(`Register user via event API to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeFirstName,
        email: attendeeEmail,
        lastName: attendeeLastName,
        phoneNumber: attendeePhoneNumber,
        designation: attendeeDesignation,
        company: attendeeCompany,
      });
      const registrationId = await getRegistrationId(
        orgApiContext,
        eventId,
        attendeeEmail
      );
    });

    await test.step(`Fetching registered user data from DB which contains registration, account and event role data`, async () => {
      let userRegDataBeforeV1Call =
        await QueryUtil.fetchUserDataFromEventRegAccountAndEventRoleTable(
          eventId,
          attendeeEmail
        );
      expect(userRegDataBeforeV1Call).not.toBeNull();
      registrationIdBeforeRegistration =
        userRegDataBeforeV1Call["registration_id"];
      await test.step(`validating the data in registration table`, async () => {
        expect(userRegDataBeforeV1Call["email"]).toBe(attendeeEmail);
        expect(userRegDataBeforeV1Call["event_id"]).toBe(eventId);
      });
      await test.step(`validating that there is no entry in account table, since user has not checked in `, async () => {
        expect(userRegDataBeforeV1Call["account_id"]).toBeNull();
      });
      await test.step(`validating that there is no entry in EVENT ROLE table, since user has not checked in `, async () => {
        expect(userRegDataBeforeV1Call["role"]).toBeNull();
        expect(userRegDataBeforeV1Call["event_role_id"]).toBeNull();
      });
    });
    await test.step(`Hitting v1 API call with payload:  in order to register ${attendeeEmail} to event`, async () => {
      const v1BetaApiResp =
        await eventController.registerUserToEventWithV1BetaAPI({
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          email: attendeeEmail,
          company: attendeeCompany,
          designation: attendeeDesignation,
          phoneNumber: attendeePhoneNumber,
        });
      expect(
        v1BetaApiResp.status(),
        "expecting v1 api status code to be 200"
      ).toBe(200);
      const v1BetaApiRespJson = await v1BetaApiResp.json();
      uniqueLinkFromResp = v1BetaApiRespJson["unique_link"];
      console.log(
        `unique link from v1 beta api resp to be ${uniqueLinkFromResp}`
      );
      expect(
        uniqueLinkFromResp,
        `expecting unique link in resp ${uniqueLinkFromResp} to have event id : ${eventId}`
      ).toContain(eventId);
      eventRoleIdFromUniqueLinnk = uniqueLinkFromResp.split("?r=")[1];
      expect(
        eventRoleIdFromUniqueLinnk,
        `expecting event role id in unique link ${eventRoleIdFromUniqueLinnk} not to be undefined`
      ).not.toBeUndefined();
    });
    await test.step(`Fetching registered user data from DB which contains registration, account and event role data`, async () => {
      const userRegDataAfterV1Call =
        await QueryUtil.fetchUserDataForV1BetaFlowValidation(
          eventId,
          attendeeEmail
        );
      expect(userRegDataAfterV1Call).not.toBeNull();
      await test.step(`validating the db entry in account, event_registration and event role for key columns`, async () => {
        expect(userRegDataAfterV1Call["email"]).toBe(attendeeEmail);
        expect(userRegDataAfterV1Call["event_id"]).toBe(eventId);
        expect(userRegDataAfterV1Call["reg_first_name"]).toBe(
          attendeeFirstName
        );
        expect(userRegDataAfterV1Call["reg_last_name"]).toBe(attendeeLastName);
        expect(userRegDataAfterV1Call["reg_designation"]).toBe(
          attendeeDesignation
        );
        expect(userRegDataAfterV1Call["reg_company"]).toBe(attendeeCompany);
        expect(userRegDataAfterV1Call["reg_phone_number"]).toBe(
          attendeePhoneNumber
        );
        expect(userRegDataAfterV1Call["account_first_name"]).toBe(
          attendeeFirstName
        );
        expect(userRegDataAfterV1Call["account_last_name"]).toBe(
          attendeeLastName
        );
        expect(userRegDataAfterV1Call["account_designation"]).toBe(
          attendeeDesignation
        );
        expect(userRegDataAfterV1Call["account_company"]).toBe(attendeeCompany);
        expect(userRegDataAfterV1Call["account_phone_number"]).toBe(
          attendeePhoneNumber
        );
        expect(userRegDataAfterV1Call["role"]).toBe("ATTENDEE");
        expect(userRegDataAfterV1Call["status"]).toBe("REGISTERED");
        expect(userRegDataAfterV1Call["registration_id"]).toBe(
          registrationIdBeforeRegistration
        );
        expect(userRegDataAfterV1Call["event_role_id"]).toBe(
          eventRoleIdFromUniqueLinnk
        );
      });
    });
  });

  test.fixme(
    `TC006: v1 beta api call with an email which is already registered and has also checked in to the event`,
    async ({ page }) => {
      const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
      const attendeeFirstName = "AutomationFirstName";
      const attendeeLastName = "AutomationLastName";
      const attendeeCompany = "Zuddl";
      const attendeeDesignation = "QA";
      const attendeePhoneNumber = "1234567890";
      let uniqueLinkFromResp: string;
      let registrationIdBeforeRegistration;
      let eventRoleIdFromUniqueLink;
      let eventRoleIdBeforeV1BetaApiCall;
      let eventRoleBeforeV1BetaApiCall;

      await test.step(`Register user via event API to the event and then check him in via API`, async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeFirstName,
          email: attendeeEmail,
          lastName: attendeeLastName,
          phoneNumber: attendeePhoneNumber,
          designation: attendeeDesignation,
          company: attendeeCompany,
        });
        const registrationId = await getRegistrationId(
          orgApiContext,
          eventId,
          attendeeEmail
        );
        await test.step(`checkin via API`, async () => {
          await checkInAttendee(orgApiContext, eventId, registrationId);
        });
      });

      await test.step(`Fetching registered user data from DB which contains registration, account and event role data`, async () => {
        let userRegDataBeforeV1Call =
          await QueryUtil.fetchUserDataFromEventRegAccountAndEventRoleTable(
            eventId,
            attendeeEmail
          );
        await test.step(`expecting registration id not to be null`, async () => {
          expect(userRegDataBeforeV1Call).not.toBeNull();
          registrationIdBeforeRegistration =
            userRegDataBeforeV1Call["registration_id"];
        });

        await test.step(`validating the data in registration table`, async () => {
          expect(userRegDataBeforeV1Call["email"]).toBe(attendeeEmail);
          expect(userRegDataBeforeV1Call["event_id"]).toBe(eventId);
        });
        await test.step(`validating that there is now an entry in account table, since user has checked in  `, async () => {
          expect(userRegDataBeforeV1Call["account_id"]).not.toBeNull();
        });
        await test.step(`validating that there is an entry in EVENT ROLE table, since user has now checked in `, async () => {
          expect(userRegDataBeforeV1Call["role"]).not.toBeNull();
          expect(userRegDataBeforeV1Call["event_role_id"]).not.toBeNull();
          eventRoleBeforeV1BetaApiCall = userRegDataBeforeV1Call["role"];
          eventRoleIdBeforeV1BetaApiCall =
            userRegDataBeforeV1Call["event_role_id"];
        });
      });
      await test.step(`Hitting v1 API call with payload:  in order to register ${attendeeEmail} to event`, async () => {
        const v1BetaApiResp =
          await eventController.registerUserToEventWithV1BetaAPI({
            firstName: attendeeFirstName,
            lastName: attendeeLastName,
            email: attendeeEmail,
            company: attendeeCompany,
            designation: attendeeDesignation,
            phoneNumber: attendeePhoneNumber,
          });
        expect(
          v1BetaApiResp.status(),
          "expecting v1 api status code to be 200"
        ).toBe(200);
        const v1BetaApiRespJson = await v1BetaApiResp.json();
        uniqueLinkFromResp = v1BetaApiRespJson["unique_link"];
        console.log(
          `unique link from v1 beta api resp to be ${uniqueLinkFromResp}`
        );
        expect(
          uniqueLinkFromResp,
          `expecting unique link in resp ${uniqueLinkFromResp} to have event id : ${eventId}`
        ).toContain(eventId);
        eventRoleIdFromUniqueLink = uniqueLinkFromResp.split("?r=")[1];
        expect(
          eventRoleIdFromUniqueLink,
          `expecting event role id in unique link ${eventRoleIdFromUniqueLink} not to be undefined`
        ).not.toBeUndefined();
      });
      await test.step(`Fetching registered user data from DB which contains registration, account and event role data`, async () => {
        const userRegDataAfterV1Call =
          await QueryUtil.fetchUserDataForV1BetaFlowValidation(
            eventId,
            attendeeEmail
          );
        expect(userRegDataAfterV1Call).not.toBeNull();
        await test.step(`validating the db entry in account, event_registration and event role for key columns`, async () => {
          expect(userRegDataAfterV1Call["email"]).toBe(attendeeEmail);
          expect(userRegDataAfterV1Call["event_id"]).toBe(eventId);
          expect(userRegDataAfterV1Call["reg_first_name"]).toBe(
            attendeeFirstName
          );
          expect(userRegDataAfterV1Call["reg_last_name"]).toBe(
            attendeeLastName
          );
          expect(userRegDataAfterV1Call["reg_designation"]).toBe(
            attendeeDesignation
          );
          expect(userRegDataAfterV1Call["reg_company"]).toBe(attendeeCompany);
          expect(userRegDataAfterV1Call["reg_phone_number"]).toBe(
            attendeePhoneNumber
          );
          expect(userRegDataAfterV1Call["account_first_name"]).toBe(
            attendeeFirstName
          );
          expect(userRegDataAfterV1Call["account_last_name"]).toBe(
            attendeeLastName
          );
          expect(userRegDataAfterV1Call["account_designation"]).toBe(
            attendeeDesignation
          );
          expect(userRegDataAfterV1Call["account_company"]).toBe(
            attendeeCompany
          );
          expect(userRegDataAfterV1Call["account_phone_number"]).toBe(
            attendeePhoneNumber
          );
          expect(userRegDataAfterV1Call["role"]).toBe("ATTENDEE");
          expect(userRegDataAfterV1Call["status"]).toBe("REGISTERED");
          expect(userRegDataAfterV1Call["registration_id"]).toBe(
            registrationIdBeforeRegistration
          );
          expect(userRegDataAfterV1Call["event_role_id"]).toBe(
            eventRoleIdFromUniqueLink
          );
        });

        await test.step(`validating that event role id and event role remains same for the user after v1 beta api call`, async () => {
          expect(userRegDataAfterV1Call["event_role_id"]).toBe(
            eventRoleIdBeforeV1BetaApiCall
          );
          expect(userRegDataAfterV1Call["role"]).toBe(
            eventRoleBeforeV1BetaApiCall
          );
        });
      });
    }
  );

  test.fixme(
    `TC007: v1 beta api call with an email who is deleted from the event`,
    async ({ page }) => {
      const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
      const attendeeFirstName = "AutomationFirstName";
      const attendeeLastName = "AutomationLastName";
      const attendeeCompany = "Zuddl";
      const attendeeDesignation = "QA";
      const attendeePhoneNumber = "1234567890";

      await test.step(`Register user via event API to the event and then check him in via API`, async () => {
        await eventController.registerUserToEventWithRegistrationApi({
          firstName: attendeeFirstName,
          email: attendeeEmail,
          lastName: attendeeLastName,
          phoneNumber: attendeePhoneNumber,
          designation: attendeeDesignation,
          company: attendeeCompany,
        });
        const registrationId = await getRegistrationId(
          orgApiContext,
          eventId,
          attendeeEmail
        );
        await test.step(`checkin via API`, async () => {
          await checkInAttendee(orgApiContext, eventId, registrationId);
        });
      });

      await test.step(`Now delete the user from the event`, async () => {
        await deleteAttendeeFromEvent(orgApiContext, eventId, attendeeEmail);
      });

      await test.step(`Hitting v1 API call with payload:  in order to register ${attendeeEmail} to event`, async () => {
        const v1BetaApiResp =
          await eventController.registerUserToEventWithV1BetaAPI({
            firstName: attendeeFirstName,
            lastName: attendeeLastName,
            email: attendeeEmail,
            company: attendeeCompany,
            designation: attendeeDesignation,
            phoneNumber: attendeePhoneNumber,
          });
        expect(
          v1BetaApiResp.status(),
          "expecting v1 api status code to be 406 for deleted user registration"
        ).toBe(406);
        const v1BetaApiRespJson = await v1BetaApiResp.json();
        await test.step(`validating the response body of v1 beta api for deleted user registration`, async () => {
          expect(v1BetaApiRespJson["message"]).toContain(
            "REGISTRATION_BLOCKED"
          );
          expect(v1BetaApiRespJson["description"]).toContain(
            "ou have been removed from the event"
          );
        });
      });
      await test.step(`Fetching registered user data from DB which contains registration, account and event role data`, async () => {
        const userRegDataAfterV1Call =
          await QueryUtil.fetchUserDataForV1BetaFlowValidation(
            eventId,
            attendeeEmail
          );
        expect(userRegDataAfterV1Call).not.toBeNull();
        await test.step(`validating the db entry in account, event_registration and event role for key columns`, async () => {
          expect(userRegDataAfterV1Call["email"]).toBe(attendeeEmail);
          expect(userRegDataAfterV1Call["event_id"]).toBe(eventId);
          expect(userRegDataAfterV1Call["reg_first_name"]).toBe(
            attendeeFirstName
          );
          expect(userRegDataAfterV1Call["reg_last_name"]).toBe(
            attendeeLastName
          );
          expect(userRegDataAfterV1Call["reg_designation"]).toBe(
            attendeeDesignation
          );
          expect(userRegDataAfterV1Call["reg_company"]).toBe(attendeeCompany);
          expect(userRegDataAfterV1Call["reg_phone_number"]).toBe(
            attendeePhoneNumber
          );
          expect(userRegDataAfterV1Call["account_first_name"]).toBe(
            attendeeFirstName
          );
          expect(userRegDataAfterV1Call["account_last_name"]).toBe(
            attendeeLastName
          );
          expect(userRegDataAfterV1Call["account_designation"]).toBe(
            attendeeDesignation
          );
          expect(userRegDataAfterV1Call["account_company"]).toBe(
            attendeeCompany
          );
          expect(userRegDataAfterV1Call["account_phone_number"]).toBe(
            attendeePhoneNumber
          );
          expect(userRegDataAfterV1Call["role"]).toBe("ATTENDEE");
          await test.step(`verifying that in DB user role still remains DEACTIVATED`, async () => {
            expect(userRegDataAfterV1Call["status"]).toBe("DEACTIVATED");
          });
        });
      });
    }
  );

  test(`TC008: v1 beta api call with an email which is added as speaker to the event`, async ({
    page,
  }) => {
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
    const attendeeFirstName = "AutomationFirstName";
    const attendeeLastName = "AutomationLastName";
    const attendeeCompany = "Zuddl";
    const attendeeDesignation = "QA";
    const attendeePhoneNumber = "1234567890";
    let uniqueLinkFromResp: string;
    let registrationIdBeforeRegistration;
    let eventRoleIdFromUniqueLink;

    await test.step(`Add user ${attendeeEmail} as speaker to the event ${eventId}`, async () => {
      const speakerDataObject = new SpeakerData(
        attendeeEmail,
        attendeeFirstName,
        attendeeLastName,
        attendeeDesignation,
        attendeeCompany
      ).getSpeakerDataObject();
      await inviteSpeakerByApi(orgApiContext, speakerDataObject, eventId, true);
    });

    await test.step(`Hitting v1 API call with payload:  in order to register ${attendeeEmail} to event which is already a speaker`, async () => {
      const v1BetaApiResp =
        await eventController.registerUserToEventWithV1BetaAPI({
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          email: attendeeEmail,
          company: attendeeCompany,
          designation: attendeeDesignation,
          phoneNumber: attendeePhoneNumber,
        });
      expect(
        v1BetaApiResp.status(),
        "expecting v1 api status code to be 200"
      ).toBe(200);
      const v1BetaApiRespJson = await v1BetaApiResp.json();
      uniqueLinkFromResp = v1BetaApiRespJson["unique_link"];
      console.log(
        `unique link from v1 beta api resp to be ${uniqueLinkFromResp}`
      );
      expect(
        uniqueLinkFromResp,
        `expecting unique link in resp ${uniqueLinkFromResp} to have event id : ${eventId}`
      ).toContain(eventId);
      eventRoleIdFromUniqueLink = uniqueLinkFromResp.split("?r=")[1];
      expect(
        eventRoleIdFromUniqueLink,
        `expecting event role id in unique link ${eventRoleIdFromUniqueLink} not to be undefined`
      ).not.toBeUndefined();
    });
    await test.step(`Fetching registered user data from DB which contains registration, account and event role data`, async () => {
      const userRegDataAfterV1Call =
        await QueryUtil.fetchUserDataForV1BetaFlowValidation(
          eventId,
          attendeeEmail
        );
      expect(userRegDataAfterV1Call).not.toBeNull();
      await test.step(`validating the db entry in account, event_registration and event role for key columns`, async () => {
        expect(userRegDataAfterV1Call["email"]).toBe(attendeeEmail);
        expect(userRegDataAfterV1Call["event_id"]).toBe(eventId);
        expect(userRegDataAfterV1Call["reg_first_name"]).toBe(
          attendeeFirstName
        );
        expect(userRegDataAfterV1Call["reg_last_name"]).toBe(attendeeLastName);
        expect(userRegDataAfterV1Call["reg_designation"]).toBe(
          attendeeDesignation
        );
        expect(userRegDataAfterV1Call["reg_company"]).toBe(attendeeCompany);
        expect(userRegDataAfterV1Call["account_first_name"]).toBe(
          attendeeFirstName
        );
        expect(userRegDataAfterV1Call["account_last_name"]).toBe(
          attendeeLastName
        );
        expect(userRegDataAfterV1Call["account_designation"]).toBe(
          attendeeDesignation
        );
        expect(userRegDataAfterV1Call["account_company"]).toBe(attendeeCompany);
        expect(userRegDataAfterV1Call["role"]).toBe("SPEAKER");
        expect(userRegDataAfterV1Call["status"]).toBe("REGISTERED");
        expect(userRegDataAfterV1Call["registration_id"]).not.toBeUndefined();
        expect(userRegDataAfterV1Call["event_role_id"]).toBe(
          eventRoleIdFromUniqueLink
        );
      });
    });
  });
  test(`TC009: v1 beta api call with an email which is already registered but has not checked in to the event`, async ({
    page,
  }) => {
    const attendeeEmailLowerCase = DataUtl.getRandomAttendeeMailosaurEmail();
    const attendeeEmailUpperCase = attendeeEmailLowerCase.toUpperCase();

    const attendeeFirstName = "AutomationFirstName";
    const attendeeLastName = "AutomationLastName";
    const attendeeCompany = "Zuddl";
    const attendeeDesignation = "QA";
    const attendeePhoneNumber = "1234567890";
    let uniqueLinkFromResp: string;
    let registrationIdBeforeRegistration;
    let eventRoleIdFromUniqueLinnk;

    await test.step(`Register user with email having all lower case ${attendeeEmailLowerCase} via event API to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeFirstName,
        email: attendeeEmailLowerCase,
        lastName: attendeeLastName,
        phoneNumber: attendeePhoneNumber,
        designation: attendeeDesignation,
        company: attendeeCompany,
      });
      const registrationId = await getRegistrationId(
        orgApiContext,
        eventId,
        attendeeEmailLowerCase
      );
    });

    await test.step(`Fetching registered user data from DB which contains registration, account and event role data`, async () => {
      let userRegDataBeforeV1Call =
        await QueryUtil.fetchUserDataFromEventRegAccountAndEventRoleTable(
          eventId,
          attendeeEmailLowerCase
        );
      expect(userRegDataBeforeV1Call).not.toBeNull();
      registrationIdBeforeRegistration =
        userRegDataBeforeV1Call["registration_id"];
      await test.step(`validating the data in registration table`, async () => {
        expect(userRegDataBeforeV1Call["email"]).toBe(attendeeEmailLowerCase);
        expect(userRegDataBeforeV1Call["event_id"]).toBe(eventId);
      });
      await test.step(`validating that there is no entry in account table, since user has not checked in `, async () => {
        expect(userRegDataBeforeV1Call["account_id"]).toBeNull();
      });
      await test.step(`validating that there is no entry in EVENT ROLE table, since user has not checked in `, async () => {
        expect(userRegDataBeforeV1Call["role"]).toBeNull();
        expect(userRegDataBeforeV1Call["event_role_id"]).toBeNull();
      });
    });
    await test.step(`Hitting v1 API call with payload:  in order to register same email but in upper acse ${attendeeEmailUpperCase} to event`, async () => {
      const v1BetaApiResp =
        await eventController.registerUserToEventWithV1BetaAPI({
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          email: attendeeEmailUpperCase,
          company: attendeeCompany,
          designation: attendeeDesignation,
          phoneNumber: attendeePhoneNumber,
        });
      expect(
        v1BetaApiResp.status(),
        "expecting v1 api status code to be 200"
      ).toBe(200);
      const v1BetaApiRespJson = await v1BetaApiResp.json();
      uniqueLinkFromResp = v1BetaApiRespJson["unique_link"];
      console.log(
        `unique link from v1 beta api resp to be ${uniqueLinkFromResp}`
      );
      expect(
        uniqueLinkFromResp,
        `expecting unique link in resp ${uniqueLinkFromResp} to have event id : ${eventId}`
      ).toContain(eventId);
      eventRoleIdFromUniqueLinnk = uniqueLinkFromResp.split("?r=")[1];
      expect(
        eventRoleIdFromUniqueLinnk,
        `expecting event role id in unique link ${eventRoleIdFromUniqueLinnk} not to be undefined`
      ).not.toBeUndefined();
    });
    await test.step(`Fetching registered user data from DB which contains registration, account and event role data`, async () => {
      const userRegDataAfterV1Call =
        await QueryUtil.fetchUserDataForV1BetaFlowValidation(
          eventId,
          attendeeEmailLowerCase
        );
      expect(userRegDataAfterV1Call).not.toBeNull();
      await test.step(`validating the db entry in account, event_registration and event role for key columns`, async () => {
        expect(userRegDataAfterV1Call["email"]).toBe(attendeeEmailLowerCase);
        expect(userRegDataAfterV1Call["event_id"]).toBe(eventId);
        expect(userRegDataAfterV1Call["reg_first_name"]).toBe(
          attendeeFirstName
        );
        expect(userRegDataAfterV1Call["reg_last_name"]).toBe(attendeeLastName);
        expect(userRegDataAfterV1Call["reg_designation"]).toBe(
          attendeeDesignation
        );
        expect(userRegDataAfterV1Call["reg_company"]).toBe(attendeeCompany);
        expect(userRegDataAfterV1Call["reg_phone_number"]).toBe(
          attendeePhoneNumber
        );
        expect(userRegDataAfterV1Call["account_first_name"]).toBe(
          attendeeFirstName
        );
        expect(userRegDataAfterV1Call["account_last_name"]).toBe(
          attendeeLastName
        );
        expect(userRegDataAfterV1Call["account_designation"]).toBe(
          attendeeDesignation
        );
        expect(userRegDataAfterV1Call["account_company"]).toBe(attendeeCompany);
        expect(userRegDataAfterV1Call["account_phone_number"]).toBe(
          attendeePhoneNumber
        );
        expect(userRegDataAfterV1Call["role"]).toBe("ATTENDEE");
        expect(userRegDataAfterV1Call["status"]).toBe("REGISTERED");
        expect(userRegDataAfterV1Call["registration_id"]).toBe(
          registrationIdBeforeRegistration
        );
        expect(userRegDataAfterV1Call["event_role_id"]).toBe(
          eventRoleIdFromUniqueLinnk
        );
      });
    });
  });
});
