import test, {
  APIRequestContext,
  APIResponse,
  BrowserContext,
  expect,
  Page,
} from "@playwright/test";
import { EventInfoDTO } from "../../../../dto/eventInfoDto";
import { UserInfoDTO } from "../../../../dto/userInfoDto";
import EventEntryType from "../../../../enums/eventEntryEnum";
import { LandingPageOne } from "../../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { LoginOptionsComponent } from "../../../../page-objects/events-pages/landing-page-1/LoginOptionsComponent";
import { AttendeeListPage } from "../../../../page-objects/new-org-side-pages/AttendeeListPage";
import {
  cancelEvent,
  checkInAttendee,
  createNewEvent,
  deleteAttendeeFromEvent,
  getRegistrationId,
  registerUserToEventbyApi,
  updateEventLandingPageDetails,
} from "../../../../util/apiUtil";
import BrowserFactory from "../../../../util/BrowserFactory";
import { readCsvFileAsJson } from "../../../../util/commonUtil";
import { DataUtl } from "../../../../util/dataUtil";
import { EventController } from "../../../../controller/EventController";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel("@registration @deleteAttendee", async () => {
  let eventTitle: string;
  let eventId;
  let attendeeLandingPage: string;
  let speakerLandingpPage: string;
  let eventInfoDto: EventInfoDTO;
  let attendeePage: Page;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let organiserPage: Page;

  test.beforeEach(async ({ context }) => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = await orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: eventTitle,
    });
    eventInfoDto = new EventInfoDTO(eventId, eventTitle, true); // true to denote magic link enabled
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
    speakerLandingpPage = eventInfoDto.speakerLandingPage;
    // get driver for attendee
    attendeePage = await context.newPage();
    await test.step(`Disabling Onboarding Checks`, async () => {
      await new EventController(
        orgApiContext,
        eventId
      ).getAccessGroupController.handleOnboardingChecksForAttendee(false);
    });
  });

  test.afterEach(async () => {
    // await cancelEvent(orgApiContext, eventId);
    console.log("In teardown, closing the browser context");
    await orgBrowserContext?.close();
  });

  test("TC001 organiser should be able to delete a user who is just registered", async () => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
    await deleteAttendeeFromEvent(orgApiContext, eventId, attendeeEmail, true);
    /*
        1. verify user is deleted from the event by trying to register him again
        2. add flag to not raise excpetion if api to register does not throw 200
        3. validaitng that register api returns 406 to denote user is deleted
    */
    let registerApiResp: APIResponse = await registerUserToEventbyApi(
      orgApiContext,
      eventId,
      attendeeEmail,
      false
    );
    expect(
      registerApiResp.status(),
      "Expecting register api to return 406 for deleted user"
    ).toBe(406);
  });

  test.fixme(
    "TC002 organiser should be able to delete a user who has checked in",
    async () => {
      let attendeeEmail = DataUtl.getRandomAttendeeEmail();
      await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
      let registrationId = await getRegistrationId(
        orgApiContext,
        eventId,
        attendeeEmail
      );
      await checkInAttendee(orgApiContext, eventId, registrationId);
      await deleteAttendeeFromEvent(
        orgApiContext,
        eventId,
        attendeeEmail,
        true
      );
      /*
        1. verify user is deleted from the event by trying to register him again
        2. add flag to not raise excpetion if api to register does not throw 200
        3. validaitng that register api returns 406 to denote user is deleted
    */
      let registerApiResp: APIResponse = await registerUserToEventbyApi(
        orgApiContext,
        eventId,
        attendeeEmail,
        false
      );
      expect(
        registerApiResp.status(),
        "Expecting register api to return 406 for deleted user"
      ).toBe(406);
    }
  );

  test("TC003 deleted attendee can not register to the event again", async () => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
    await deleteAttendeeFromEvent(orgApiContext, eventId, attendeeEmail, true);

    let landingPageOne = new LandingPageOne(attendeePage);
    await landingPageOne.load(attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      new UserInfoDTO(attendeeEmail, " ", "prateek", "qa", false)
        .userRegFormData.userRegFormDataForDefaultFields,
      {
        fillMandatoryFields: true,
      },
      false
    );
    await expect(
      landingPageOne.notificationContainer,
      "expecting error notifcation message to be visible"
    ).toBeVisible();
    await expect(
      landingPageOne.notificationContainer,
      "expecting error notifcation message to be contain text "
    ).toContainText("You have been removed from the event", {
      ignoreCase: true,
    });
  });

  test("TC004: deleted attendee can not login using otp", async () => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
    await deleteAttendeeFromEvent(orgApiContext, eventId, attendeeEmail, true);

    let landingPageOne = new LandingPageOne(attendeePage);
    await landingPageOne.load(attendeeLandingPage);
    let loginOptionsComponent: LoginOptionsComponent = await (
      await landingPageOne.getRegistrationConfirmationScreen()
    ).clickOnAlreadyRegisteredButton();
    await loginOptionsComponent.emailInputBoxForOTP.fill(attendeeEmail);
    await loginOptionsComponent.submitButton.click();
    await expect(
      landingPageOne.deactivatedEmailToast,
      "Expecting deactivated email toast to be visible"
    ).toBeVisible({
      timeout: 10000,
    });
  });
  //not working due to : https://linear.app/zuddl/issue/BUG-1492/delete-feature-seems-to-be-not-working-if-attendee-is-already-inside
  // update but behaviour still broken as attendee is being redirected to speaker landing page instead of attendee landing page
  test("TC005: attendee if deleted should be thrown out of event if he is inside", async () => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });
    await test.step(`Registering attendee ${attendeeEmail} to the event`, async () => {
      await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
    });

    await test.step(`user gets magic link and loads it and enter to the lobby`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      let landingPageOne = new LandingPageOne(attendeePage);
      await landingPageOne.load(attendeeMagicLink);
      await landingPageOne.clickOnEnterNowButton();
      await landingPageOne.isLobbyLoaded();
      await landingPageOne.page.waitForTimeout(10000);
    });

    await test.step(`organiser deletes the added attendeee who is inside the event`, async () => {
      await deleteAttendeeFromEvent(
        orgApiContext,
        eventId,
        attendeeEmail,
        true
      );
    });

    await test.step(`attendee should be thrown out of the event`, async () => {
      await attendeePage.waitForTimeout(5000);
      await expect(
        attendeePage.getByText("You are being logged out."),
        "Expecting user to be logged out"
      ).toBeVisible();

      await expect(
        attendeePage.getByText("You are being logged out."),
        "Expecting user to be logged out"
      ).toBeVisible();
      await attendeePage.waitForTimeout(7000);
      attendeeLandingPage = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/p/event/${eventId}`;
      await expect(
        attendeePage,
        "Expecting user to be on the landing page"
      ).toHaveURL(attendeeLandingPage);
      // ).toHaveURL(speakerLandingpPage);
      // Currently user is being redirected to speakerLandingPage
    });
  });

  test("TC007: If attendee deleted, attendee magic link should not work for the event", async () => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let magicLink: string;
    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });
    await test.step(`Registering attendee ${attendeeEmail} to the event`, async () => {
      await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
    });
    await test.step(`user fetches magic link from the db`, async () => {
      magicLink = await QueryUtil.fetchMagicLinkFromDB(eventId, attendeeEmail);
    });
    await test.step(`Organiser deletes the attendee`, async () => {
      await deleteAttendeeFromEvent(
        orgApiContext,
        eventId,
        attendeeEmail,
        true
      );
    });
    await test.step(`Attendee loads the magic link `, async () => {
      let landingPageOne = new LandingPageOne(attendeePage);
      await landingPageOne.load(magicLink);

      attendeeLandingPage = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/p/a/event/${eventId}`;
      // verify user remains on the landing page and does not gets enter now button
      await expect(
        attendeePage,
        `expecting magic link to not work and user remains on reg form`
      ).toHaveURL(attendeeLandingPage);
      await expect(landingPageOne.enterNowButton).toBeHidden();
    });
  });

  test("TC008: export csv should show the deleted attendee record with status deleted", async () => {
    let attendeeEmail1 = DataUtl.getRandomAttendeeEmail();
    let attendeeEmail2 = DataUtl.getRandomAttendeeEmail();

    organiserPage = await orgBrowserContext.newPage();

    await test.step(`Registering attendee 1 ${attendeeEmail1} to the event`, async () => {
      await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail1);
    });

    await test.step(`Registering attendee 2 ${attendeeEmail2} to the event`, async () => {
      await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail2);
    });
    await test.step(`organiser deletes the attendee 1 from the registered user`, async () => {
      await deleteAttendeeFromEvent(
        orgApiContext,
        eventId,
        attendeeEmail1,
        true
      );
    });

    await organiserPage.goto(
      `${
        DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
      }/event/${eventId}/people/attendees`
    );

    await organiserPage.waitForTimeout(5000);
    const [download] = await Promise.all([
      organiserPage.waitForEvent("download"),
      new AttendeeListPage(organiserPage).downloadCsv(),
    ]);
    console.log(await download.path());
    await download.saveAs("test-data/downloadAttendee.csv");
    const jsonData = await readCsvFileAsJson("test-data/downloadAttendee.csv");
    await test.step(`verify the download csv contains attendee 2 data and not the attendee 1 deleted record`, async () => {
      console.log(jsonData);
      expect(jsonData, "expecting csv to contain 1 record").toHaveLength(1);
      expect(
        jsonData[0].attendee_email.toLowerCase(),
        `expecting ${attendeeEmail2} to be present in the csv`
      ).toBe(attendeeEmail2.toLowerCase());
    });
  });
});
