import test, {
  APIRequestContext,
  APIResponse,
  BrowserContext,
  expect,
  Page,
} from "@playwright/test";
import { EventInfoDTO } from "../../../../dto/eventInfoDto";
import { UserInfoDTO } from "../../../../dto/userInfoDto";
import EventEntryType from "../../../../enums/eventEntryEnum";
import { LandingPageOne } from "../../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { LoginOptionsComponent } from "../../../../page-objects/events-pages/landing-page-1/LoginOptionsComponent";
import { AttendeeListPage } from "../../../../page-objects/new-org-side-pages/AttendeeListPage";
import {
  cancelEvent,
  checkInAttendee,
  createNewEvent,
  deleteAttendeeFromEvent,
  getRegistrationId,
  registerUserToEventbyApi,
  updateEventLandingPageDetails,
} from "../../../../util/apiUtil";
import BrowserFactory from "../../../../util/BrowserFactory";
import { readCsvFileAsJson } from "../../../../util/commonUtil";
import { DataUtl } from "../../../../util/dataUtil";
import { EventController } from "../../../../controller/EventController";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel("@registration @deleteAttendee", async () => {
  let eventTitle: string;
  let eventId;
  let attendeeLandingPage: string;
  let speakerLandingpPage: string;
  let eventInfoDto: EventInfoDTO;
  let attendeePage: Page;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let organiserPage: Page;

  test.beforeEach(async ({ context }) => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({});
    orgApiContext = await orgBrowserContext.request;
    eventTitle = Date.now().toString() + "_automationevent";
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title: eventTitle,
    });
    eventInfoDto = new EventInfoDTO(eventId, eventTitle, true); // true to denote magic link enabled
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
    speakerLandingpPage = eventInfoDto.speakerLandingPage;
    // get driver for attendee
    attendeePage = await context.newPage();
    await test.step(`Disabling Onboarding Checks`, async () => {
      await new EventController(
        orgApiContext,
        eventId
      ).getAccessGroupController.handleOnboardingChecksForAttendee(false);
    });
  });

  test.afterEach(async () => {
    // await cancelEvent(orgApiContext, eventId);
    console.log("In teardown, closing the browser context");
    await orgBrowserContext?.close();
  });

  test("TC001 organiser should be able to delete a user who is just registered", async () => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
    await deleteAttendeeFromEvent(orgApiContext, eventId, attendeeEmail, false);
    /*
    verify that deleted attendee can register again, as they are not blacklisted
    */
    let registerApiResp: APIResponse = await registerUserToEventbyApi(
      orgApiContext,
      eventId,
      attendeeEmail,
      false
    );
    expect(
      registerApiResp.status(),
      "Expecting register api to return 200 since not blacklisted."
    ).toBe(200);
  });

  test.fixme(
    "TC002 organiser should be able to delete a user who has checked in",
    async () => {
      let attendeeEmail = DataUtl.getRandomAttendeeEmail();
      await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
      let registrationId = await getRegistrationId(
        orgApiContext,
        eventId,
        attendeeEmail
      );
      await checkInAttendee(orgApiContext, eventId, registrationId);
      await deleteAttendeeFromEvent(
        orgApiContext,
        eventId,
        attendeeEmail,
        false
      );
      /*
    verify that deleted attendee can register again, as they are not blacklisted
    */
      let registerApiResp: APIResponse = await registerUserToEventbyApi(
        orgApiContext,
        eventId,
        attendeeEmail,
        false
      );
      expect(
        registerApiResp.status(),
        "Expecting register api to return 200 since not blacklisted."
      ).toBe(200);
    }
  );

  test("TC003 deleted attendee can register to the event again", async () => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
    await deleteAttendeeFromEvent(orgApiContext, eventId, attendeeEmail, false);

    let landingPageOne = new LandingPageOne(attendeePage);
    await landingPageOne.load(attendeeLandingPage);
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      new UserInfoDTO(attendeeEmail, " ", "prateek", "qa", false)
        .userRegFormData.userRegFormDataForDefaultFields,
      {
        fillMandatoryFields: true,
      },
      false
    );
    await expect(
      landingPageOne.page.locator("div[class^='styles-module__logoHeaderBody']")
    ).toContainText("You've successfully registered!");
  });

  test("TC004: deleted attendee can login using otp", async () => {
    // failing due to : https://linear.app/zuddl/issue/BUG-2074/lp1-no-error-message-is-not-displaying-if-registered-user-who
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
    await deleteAttendeeFromEvent(orgApiContext, eventId, attendeeEmail, false);

    let landingPageOne = new LandingPageOne(attendeePage);
    await landingPageOne.load(attendeeLandingPage);
    let loginOptionsComponent: LoginOptionsComponent = await (
      await landingPageOne.getRegistrationConfirmationScreen()
    ).clickOnAlreadyRegisteredButton();
    await loginOptionsComponent.emailInputBoxForOTP.fill(attendeeEmail);
    await loginOptionsComponent.submitButton.click();
    //should take me back to registration form component
    await (
      await landingPageOne.getRegistrationFormComponent()
    ).fillUpTheRegistrationForm(
      new UserInfoDTO(attendeeEmail, "", "Prateek", "QA", false).userRegFormData
        .userRegFormDataForDefaultFields,
      {
        fieldsToSkip: ["email"],
        fillMandatoryFields: true,
      },
      false
    );
    await expect(
      landingPageOne.page.locator("div[class^='styles-module__logoHeaderBody']")
    ).toContainText("You've successfully registered!");
  });

  // update but behaviour still broken as attendee is being redirected to speaker landing page instead of attendee landing page
  test("TC005: attendee if deleted should be thrown out of event if he is inside", async () => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });
    await test.step(`Registering attendee ${attendeeEmail} to the event`, async () => {
      await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
    });

    await test.step(`user gets magic link and loads it and enter to the lobby`, async () => {
      const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
      let landingPageOne = new LandingPageOne(attendeePage);
      await landingPageOne.load(attendeeMagicLink);
      await landingPageOne.clickOnEnterNowButton();
      await landingPageOne.isLobbyLoaded();
      await attendeePage.waitForTimeout(5000);
    });

    await test.step(`organiser deletes the added attendeee who is inside the event`, async () => {
      await deleteAttendeeFromEvent(
        orgApiContext,
        eventId,
        attendeeEmail,
        false
      );
    });

    await test.step(`attendee should be thrown out of the event`, async () => {
      await attendeePage.waitForTimeout(5000);
      await expect(
        attendeePage.getByText("You are being logged out."),
        "Expecting user to be logged out"
      ).toBeVisible();

      await expect(
        attendeePage.getByText("You are being logged out."),
        "Expecting user to be logged out"
      ).toBeVisible();
      await attendeePage.waitForTimeout(7000);
      attendeeLandingPage = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/p/event/${eventId}`;

      await expect(
        attendeePage,
        "Expecting user to be on the landing page"
      ).toHaveURL(attendeeLandingPage);
      // ).toHaveURL(speakerLandingpPage);
      // Currently user is being redirected to speakerLandingPage
    });
  });

  test("TC007: If attendee deleted, attendee magic link should not work for the event", async () => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let magicLink: string;
    await updateEventLandingPageDetails(orgApiContext, eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: true,
    });
    await test.step(`Registering attendee ${attendeeEmail} to the event`, async () => {
      await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail);
    });
    await test.step(`user fetches magic link from the db`, async () => {
      magicLink = await QueryUtil.fetchMagicLinkFromDB(eventId, attendeeEmail);
    });
    await test.step(`Organiser deletes the attendee`, async () => {
      await deleteAttendeeFromEvent(
        orgApiContext,
        eventId,
        attendeeEmail,
        false
      );
    });
    await test.step(`Attendee loads the magic link `, async () => {
      let landingPageOne = new LandingPageOne(attendeePage);
      await landingPageOne.load(magicLink);
      attendeeLandingPage = `${
        DataUtl.getApplicationTestDataObj().baseUrl
      }/p/a/event/${eventId}`; // should be /p/a/event/${eventId}

      // verify user remains on the landing page and does not gets enter now button
      await expect(
        attendeePage,
        `expecting magic link to not work and user remains on reg form`
      ).toHaveURL(attendeeLandingPage);
      await expect(landingPageOne.enterNowButton).toBeHidden();
    });
  });

  test("TC008: export csv should show the deleted attendee record with status deleted", async () => {
    let attendeeEmail1 = DataUtl.getRandomAttendeeEmail();
    let attendeeEmail2 = DataUtl.getRandomAttendeeEmail();

    organiserPage = await orgBrowserContext.newPage();

    await test.step(`Registering attendee 1 ${attendeeEmail1} to the event`, async () => {
      await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail1);
    });

    await test.step(`Registering attendee 2 ${attendeeEmail2} to the event`, async () => {
      await registerUserToEventbyApi(orgApiContext, eventId, attendeeEmail2);
    });
    await test.step(`organiser deletes the attendee 1 from the registered user`, async () => {
      await deleteAttendeeFromEvent(
        orgApiContext,
        eventId,
        attendeeEmail1,
        false
      );
    });

    await organiserPage.goto(
      `${
        DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
      }/event/${eventId}/people/attendees`
    );
    const [download] = await Promise.all([
      organiserPage.waitForEvent("download"),
      new AttendeeListPage(organiserPage).downloadCsv(),
    ]);
    console.log(await download.path());
    await download.saveAs("test-data/downloadAttendee.csv");
    const jsonData = await readCsvFileAsJson("test-data/downloadAttendee.csv");
    await test.step(`verify the download csv contains attendee 2 data and not the attendee 1 deleted record`, async () => {
      console.log(jsonData);
      expect(jsonData, "expecting csv to contain 1 record").toHaveLength(1);
      expect(
        jsonData[0].attendee_email.toLowerCase(),
        `expecting ${attendeeEmail2} to be present in the csv`
      ).toBe(attendeeEmail2.toLowerCase());
    });
  });
});
