import { WebinarController } from "../../../controller/WebinarController";
import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { LandingPage } from "../../../page-objects/events-pages/embedded-form/LandingPage";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe("@LP2 @utm-param", async () => {
  let webinarId: any;
  let userInfoDTO: UserInfoDTO;
  let orgApiContext: APIRequestContext;
  let orgBrowserContext: BrowserContext;
  let webinarController: WebinarController;

  test.beforeEach(async () => {
    await test.step("Initialise Organiser browser context.", async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: true,
      });
    });

    await test.step("Initialise Organiser API request context.", async () => {
      orgApiContext = orgBrowserContext.request;
    });

    await test.step("Fetch existing webinarId.", async () => {
      webinarId =
        DataUtl.getApplicationTestDataObj()["webinarIdForEmbedFormTesting"];
      console.log(`Webinar ID - ${webinarId}.`);
    });

    await test.step("Initialise Webinar Controller.", async () => {
      webinarController = new WebinarController(orgApiContext, webinarId);
    });
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`@utm-params TC001: verify if website embedding reg form opened with appended utm param in url, same get recorded in event registration table for successfull registration`, async ({
    page,
  }) => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let landingPage = new LandingPage(page);

    userInfoDTO = new UserInfoDTO(
      attendeeEmail,
      "",
      "prateek",
      "parashar",
      true
    );
    let embeddableFormUrl =
      DataUtl.getApplicationTestDataObj()["webinarEmbedFormSiteUrl"];
    let embeddableFormUrlWithUtmParams: string;

    const utm_source = "automation";
    const utm_medium = "playwright";

    await test.step(`Appending the embed form url with utm params`, async () => {
      //append this landing page with UTM sources
      embeddableFormUrlWithUtmParams = `${embeddableFormUrl}?utm_source=${utm_source}&utm_medium=${utm_medium}`;
      console.log(
        `embeddableFormUrlWithUtmParams landing page with utm params appended is ${embeddableFormUrlWithUtmParams}`
      );
    });

    await test.step(`Fill up the embedded registration form`, async () => {
      console.log(`embed form site url to test is ${embeddableFormUrl}`);
      await landingPage.load(embeddableFormUrlWithUtmParams!);
      await (
        await landingPage.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData._conditionalFieldFormData1,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
          clickOnSubmitButton: true,
          clickOnDesclaimerButton: true,
        }
      );
    });

    await test.step(`Now fetching the user entry from event_registration table and validate the utm params`, async () => {
      //verify in DB event_registration table, the utm param exists as expected
      const userRegDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          webinarId,
          attendeeEmail
        );
      //fetch the utm param from db for registration record
      const utmParamsFromDb = userRegDataFromDb["utm"];
      //validate the utm params
      //validate the key and value
      await test.step(`validating the utm_medium param from db entry`, async () => {
        expect(
          utmParamsFromDb["utm_medium"],
          `expecting utm_medium to be ${utm_medium}`
        ).toBe(utm_medium);
      });

      await test.step(`validating the utm_source param from db entry`, async () => {
        expect(
          utmParamsFromDb["utm_source"],
          `expecting utm_source to be ${utm_source}`
        ).toBe(utm_source);
      });
    });
  });
});
