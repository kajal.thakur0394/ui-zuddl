import { EventController } from "../../../controller/EventController";
import { WebinarController } from "../../../controller/WebinarController";
import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import { LandingPageTwo } from "../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe("@LP2 @utm-param", async () => {
  let webinarId;
  let landingPage: string;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let eventController: EventController;

  let orgApiContext: APIRequestContext;
  let orgBrowserContext: BrowserContext;
  let webinarController: WebinarController;

  test.beforeEach(async () => {
    await test.step("Initialise Organiser browser context.", async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: true,
      });
    });

    await test.step("Initialise Organiser API request context.", async () => {
      orgApiContext = orgBrowserContext.request;
    });

    await test.step("Create new Webinar.", async () => {
      webinarId = await new WebinarController(orgApiContext).createNewWebinar({
        title: "By automation",
      });
      console.log(`Webinar ID - ${webinarId}.`);
    });

    await test.step("Initialise Webinar Controller.", async () => {
      webinarController = new WebinarController(orgApiContext, webinarId);
    });

    await test.step("Initialise Event Controller.", async () => {
      eventController = new EventController(orgApiContext, webinarId);
    });

    await test.step("Fetching attendee landing page.", async () => {
      landingPage =
        webinarController.getWebinarInfoDto.webinarAttendeeLandingPage();
    });
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`@utm-params TC001: @attendee if user uses landing page2 link appended with utm params for reg, those should get recorded in event_registration table for this entry`, async ({
    page,
  }) => {
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    const utm_medium = "playwright";
    const utm_source = "automation";
    let utmLandingPage =
      landingPage + `?utm_source=${utm_source}&utm_medium=${utm_medium}`;

    await test.step(`Creating Attendee details for webinar`, async () => {
      eventInfoDto = new EventInfoDTO(webinarId, "by automation", true); // true to denote magic link enabled
      eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
      userInfoDto = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        false
      );
    });

    await test.step(`Attendee ${attendeeEmail} lands on webinar after registering and through magic link`, async () => {
      let landingPageTwo = new LandingPageTwo(page);
      await landingPageTwo.load(utmLandingPage);
      //fill up the registration form
      await landingPageTwo.registrationFormComponent.fillUpTheRegistrationForm(
        userInfoDto.userRegFormData.userRegFormDataForDefaultFields
      );
    });

    await test.step(`Now fetching the user entry from event_registration table and validate the utm params`, async () => {
      //verify in DB event_registration table, the utm param exists as expected
      const userRegDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          webinarId,
          attendeeEmail
        );
      //fetch the utm param from db for registration record
      const utmParamsFromDb = userRegDataFromDb["utm"];

      //validate the key and value
      await test.step(`validating the utm_medium param from db entry`, async () => {
        expect(
          utmParamsFromDb["utm_medium"],
          `expecting utm_medium to be ${utm_medium}`
        ).toBe(utm_medium);
      });

      await test.step(`validating the utm_source param from db entry`, async () => {
        expect(
          utmParamsFromDb["utm_source"],
          `expecting utm_source to be ${utm_source}`
        ).toBe(utm_source);
      });
    });
  });
});
