import {
  APIRequestContext,
  BrowserContext,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { WebinarController } from "../../../controller/WebinarController";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import EventRole from "../../../enums/eventRoleEnum";
import { LoginOptionsComponent } from "../../../page-objects/events-pages/landing-page-2/LoginOptionsComponent";
import { LandingPageTwo } from "../../../page-objects/events-pages/landing-page-2/LandingPageTwo";
import {
  addAttendeeRecordInCsv,
  addSpeakerRecordInCsv,
  deleteAttendeeFromEvent,
  getSignedS3Url,
  inviteAttendeeByAPI,
  inviteSpeakerByApi,
  processAttendeeCsvOnEndPoint,
  processSpeakerCsvOnEndPoint,
  registerUserToEventbyApi,
  updateEventLandingPageDetails,
  waitForCsvFileProcessingToComplete,
} from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { getInviteLinkFromSpeakerInviteEmail } from "../../../util/emailUtil";
import { SpeakerData } from "../../../test-data/speakerData";
import { checkIfFileExists } from "../../../util/commonUtil";
import conditionalFieldsTestData from "../../../test-data/conditional-fields-testdata/createConditionalFields.json";
import { EditProfilePage } from "../../../page-objects/events-pages/live-side-components/EditProfilePage";
import { WebinarStage } from "../../../page-objects/webinar-pages/webinarStage";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel(`@sanity @webinar @stage`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  let webinarEventId;
  let webinarAttendeeLandingPage: string;
  let webinarSpeakerLandingPage: string;
  let attendeeMagicLink: string;
  let webinarController: WebinarController;
  let attendeeEmail;
  let eventInfoDto: EventInfoDTO;
  let userInfoDto: UserInfoDTO;
  let userEventRoleDto: UserEventRoleDto;
  let speakerEmail: string;
  let csvFileToAddDataIn: string;

  test.beforeEach(async () => {
    await test.step("Initialise Organiser browser context.", async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: true,
      });
    });

    await test.step("Initialise Organiser API request context.", async () => {
      orgApiRequestContext = orgBrowserContext.request;
    });

    await test.step("Create new Webinar.", async () => {
      webinarEventId = await new WebinarController(
        orgApiRequestContext
      ).createNewWebinar({
        title: "By automation",
      });
      console.log(`Webinar ID - ${webinarEventId}.`);
    });

    await test.step("Initialise Webinar Controller.", async () => {
      webinarController = new WebinarController(
        orgApiRequestContext,
        webinarEventId
      );
    });

    await test.step("Fetching attendee landing page.", async () => {
      webinarAttendeeLandingPage =
        webinarController.getWebinarInfoDto.webinarAttendeeLandingPage();
    });

    await test.step("Fetching speaker landing page.", async () => {
      webinarSpeakerLandingPage =
        webinarController.getWebinarInfoDto.webinarSpeakerLandingPage();
    });
  });

  test.afterEach(async () => {
    const fs = require("fs");
    if (checkIfFileExists(csvFileToAddDataIn)) {
      console.log(`file exists`);
      fs.unlink(csvFileToAddDataIn, (err) => {
        if (err) {
          console.error(`error ${err} occurred when deleting the file`);
        }
      });
    } else {
      console.log("file does not exists");
    }
    await orgBrowserContext?.close();
  });

  test(`TC001: Invited Attendee lands on webinar stage by OTP`, async ({
    page,
  }) => {
    // // invite attendee to webinar
    await test.step(`Invite attendee to webinar`, async () => {
      attendeeEmail = DataUtl.getRandomAttendeeEmail();
      eventInfoDto = new EventInfoDTO(webinarEventId, "by automation", false); // true to denote magic link enabled
      eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
      userInfoDto = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        false
      );
      // invite this user to this webinar event as an attendee
      await inviteAttendeeByAPI(
        orgApiRequestContext,
        webinarEventId,
        attendeeEmail
      );
      await updateEventLandingPageDetails(
        orgApiRequestContext,
        webinarEventId,
        {
          isMagicLinkEnabled: false,
        }
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.ATTENDEE,
        true
      );
    });

    /**
     * disabling onboarding checks for attendee since it consumes time for attendee in test
     * Note: the api we are using here are not exposed on the UI, instead
     * I am using event side access group api for attendee to disable onboarding check by event controller
     */
    await test.step(`Disabling Onboarding Checks`, async () => {
      await new EventController(
        orgApiRequestContext,
        webinarEventId
      ).getAccessGroupController.handleOnboardingChecksForAttendee(false);
    });
    await test.step(`Attendee ${attendeeEmail} lands on webinar by using OTP`, async () => {
      let landingPageTwo = new LandingPageTwo(page);
      await landingPageTwo.load(webinarAttendeeLandingPage);
      await (
        await landingPageTwo.getRegistrationFormComponent()
      ).clickOnAlreadyRegisteredButton();
      // now enter your email
      await (
        await landingPageTwo.getLoginOptionsComponent()
      ).submitEmailForOtpVerification(userEventRoleDto);
      await landingPageTwo.clickOnEnterNowButton();
      let webinarPage = await landingPageTwo.isStageLoaded(true);
      await webinarPage?.getInteractionPanel.isInteractionPanelOpen();
    });
  });

  test(`TC002: Attendee register and gets a magic link and then he logs in through it`, async ({
    page,
  }) => {
    // // invite attendee to webinar
    await test.step(`Creating Attendee details for webinar`, async () => {
      attendeeEmail = DataUtl.getRandomAttendeeEmail();
      eventInfoDto = new EventInfoDTO(webinarEventId, "by automation", false); // true to denote magic link enabled
      eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
      userInfoDto = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        false
      );
      // invite this user to this webinar event as an attendee
      await inviteAttendeeByAPI(
        orgApiRequestContext,
        webinarEventId,
        attendeeEmail
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.ATTENDEE,
        true
      );
    });

    /**
     * disabling onboarding checks for attendee since it consumes time for attendee in test
     * Note: the api we are using here are not exposed on the UI, instead
     * I am using event side access group api for attendee to disable onboarding check by event controller
     */
    await test.step(`Disabling Onboarding Checks`, async () => {
      await new EventController(
        orgApiRequestContext,
        webinarEventId
      ).getAccessGroupController.handleOnboardingChecksForAttendee(false);
    });
    await test.step(`Attendee ${attendeeEmail} lands on webinar after registering and through magic link`, async () => {
      let landingPageTwo = new LandingPageTwo(page);
      await landingPageTwo.load(webinarAttendeeLandingPage);
      await (
        await landingPageTwo.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDto.userRegFormData.userRegFormDataForDefaultFields
      );
      let attendeePageLinkFromEmailInvite: string =
        await QueryUtil.fetchMagicLinkFromDB(webinarEventId, attendeeEmail);
      await landingPageTwo.load(attendeePageLinkFromEmailInvite);
      await landingPageTwo.clickOnEnterNowButton();
      let webinarPage = await landingPageTwo.isStageLoaded(true);
      await webinarPage?.getInteractionPanel.isInteractionPanelOpen();
    });
  });

  test(`TC003: Deleted Attendee can not login to Webinar`, async ({ page }) => {
    // // invite attendee to webinar
    await test.step(`Registering and deleting attendee from webinar`, async () => {
      attendeeEmail = DataUtl.getRandomAttendeeEmail();
      eventInfoDto = new EventInfoDTO(webinarEventId, "by automation", false); // true to denote magic link enabled
      eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
      userInfoDto = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        false
      );
      // invite this user to this webinar event as an attendee
      await registerUserToEventbyApi(
        orgApiRequestContext,
        webinarEventId,
        attendeeEmail
      );
      await deleteAttendeeFromEvent(
        orgApiRequestContext,
        webinarEventId,
        attendeeEmail
      );
      await updateEventLandingPageDetails(
        orgApiRequestContext,
        webinarEventId,
        {
          isMagicLinkEnabled: false,
        }
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.ATTENDEE,
        true
      );
    });
    await test.step(`Attendee ${attendeeEmail} not able to login in Webinar by using OTP`, async () => {
      let landingPageTwo = new LandingPageTwo(page);
      await landingPageTwo.load(webinarAttendeeLandingPage);
      let loginOptionsComponent: LoginOptionsComponent = await (
        await landingPageTwo.getRegistrationFormComponent()
      ).clickOnAlreadyRegisteredButton();
      await loginOptionsComponent.emailInputBox.fill(attendeeEmail);
      await loginOptionsComponent.submitButton.click();
      await expect(
        landingPageTwo.page.getByText(
          "This email is deactivated from this event"
        ),
        "expecting error message to be visible"
      ).toBeVisible();
    });
  });

  test(`TC004: Attendee log in throgh magic link and then logout and lands on attendee landing page`, async ({
    page,
  }) => {
    // // invite attendee to webinar
    await test.step(`Creating Attendee details for webinar`, async () => {
      attendeeEmail = DataUtl.getRandomAttendeeEmail();
      eventInfoDto = new EventInfoDTO(webinarEventId, "by automation", false); // true to denote magic link enabled
      eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
      userInfoDto = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        false
      );
      // invite this user to this webinar event as an attendee
      await inviteAttendeeByAPI(
        orgApiRequestContext,
        webinarEventId,
        attendeeEmail
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.ATTENDEE,
        true
      );
    });

    /**
     * disabling onboarding checks for attendee since it consumes time for attendee in test
     * Note: the api we are using here are not exposed on the UI, instead
     * I am using event side access group api for attendee to disable onboarding check by event controller
     */
    await test.step(`Disabling Onboarding Checks`, async () => {
      await new EventController(
        orgApiRequestContext,
        webinarEventId
      ).getAccessGroupController.handleOnboardingChecksForAttendee(false);
    });
    await test.step(`Attendee ${attendeeEmail} lands on webinar after registering and through magic link`, async () => {
      let landingPageTwo = new LandingPageTwo(page);
      await landingPageTwo.load(webinarAttendeeLandingPage);
      await (
        await landingPageTwo.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDto.userRegFormData.userRegFormDataForDefaultFields
      );
      let attendeePageLinkFromEmailInvite: string =
        await QueryUtil.fetchMagicLinkFromDB(webinarEventId, attendeeEmail);
      await landingPageTwo.load(attendeePageLinkFromEmailInvite);
      await landingPageTwo.clickOnEnterNowButton();
      let webinarPage = await landingPageTwo.isStageLoaded(true);
      await webinarPage?.getInteractionPanel.isInteractionPanelOpen();
    });

    await test.step(`Attendee ${attendeeEmail} logs out of webinar and lands on attendee landing page`, async () => {
      await page.locator("div[class^='styles-module__logoutButton_']").click();
      await page.getByRole("button").filter({ hasText: "Exit" }).click();
      await page.waitForURL(webinarAttendeeLandingPage);
    });
  });

  test(`TC005: Invited Speaker lands on webinar stage by OTP`, async ({
    page,
  }) => {
    // // invite attendee to webinar
    await test.step(`Invite attendee to webinar`, async () => {
      speakerEmail = DataUtl.getRandomAttendeeEmail();
      eventInfoDto = new EventInfoDTO(webinarEventId, "by automation", false); // true to denote magic link enabled
      eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
      let speakerUser = new SpeakerData(speakerEmail, "Prateek");
      userInfoDto = new UserInfoDTO(
        speakerEmail,
        "",
        "prateek",
        "parashar",
        false
      );
      // invite this user to this webinar event as an speaker
      await inviteSpeakerByApi(
        orgApiRequestContext,
        speakerUser.getSpeakerDataObject(),
        eventInfoDto.eventId,
        false
      ); // true denote to send email
      await updateEventLandingPageDetails(
        orgApiRequestContext,
        webinarEventId,
        {
          isMagicLinkEnabled: false,
        }
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.SPEAKER
      );
    });

    /**
     * disabling onboarding checks for attendee since it consumes time for attendee in test
     * Note: the api we are using here are not exposed on the UI, instead
     * I am using event side access group api for attendee to disable onboarding check by event controller
     */
    await test.step(`Disabling Onboarding Checks`, async () => {
      await new EventController(
        orgApiRequestContext,
        webinarEventId
      ).getAccessGroupController.handleOnboardingChecksForAttendee(false);
    });

    await test.step(`Speaker ${speakerEmail} lands on webinar by using OTP`, async () => {
      let landingPageTwo = new LandingPageTwo(page);
      await landingPageTwo.load(webinarSpeakerLandingPage);
      await landingPageTwo.clickOnLoginButton();
      await (
        await landingPageTwo.getLoginOptionsComponent()
      ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
      await landingPageTwo.clickOnEnterNowButton();
    });
  });

  test(`TC006: Invited Speaker lands on webinar stage by magic link`, async ({
    page,
  }) => {
    // // invite attendee to webinar
    speakerEmail = DataUtl.getRandomAttendeeEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    await test.step(`Invite attendee to webinar`, async () => {
      eventInfoDto = new EventInfoDTO(webinarEventId, "by automation", false); // true to denote magic link enabled
      eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
      userInfoDto = new UserInfoDTO(
        speakerEmail,
        "",
        "prateek",
        "parashar",
        false
      );
      await updateEventLandingPageDetails(
        orgApiRequestContext,
        webinarEventId,
        {
          isSpeakermagicLinkEnabled: true,
        }
      );
      // invite this user to this webinar event as an speaker
      await inviteSpeakerByApi(
        orgApiRequestContext,
        speakerUser.getSpeakerDataObject(),
        eventInfoDto.eventId,
        true
      ); // true denote to send email

      userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.SPEAKER
      );
    });

    /**
     * disabling onboarding checks for attendee since it consumes time for attendee in test
     * Note: the api we are using here are not exposed on the UI, instead
     * I am using event side access group api for attendee to disable onboarding check by event controller
     */
    await test.step(`Disabling Onboarding Checks`, async () => {
      await new EventController(
        orgApiRequestContext,
        webinarEventId
      ).getAccessGroupController.handleOnboardingChecksForAttendee(false);
    });

    await test.step(`Speaker ${speakerEmail} lands on webinar by using magic link`, async () => {
      let landingPageTwo = new LandingPageTwo(page);
      let speakerInviteLink = await getInviteLinkFromSpeakerInviteEmail(
        speakerEmail,
        eventInfoDto.getInviteEmailSubj()
      );
      await landingPageTwo.load(speakerInviteLink);

      await landingPageTwo.isStageLoaded(true);
    });
  });

  test(`TC007: Invited Speaker lands on webinar stage by magic link and logs out from it and lands on speaker landing page`, async ({}) => {
    // // invite attendee to webinar
    speakerEmail = DataUtl.getRandomAttendeeEmail();
    let speakerUser = new SpeakerData(speakerEmail, "Prateek");
    await test.step(`Invite attendee to webinar`, async () => {
      eventInfoDto = new EventInfoDTO(webinarEventId, "by automation", false); // true to denote magic link enabled
      eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
      userInfoDto = new UserInfoDTO(
        speakerEmail,
        "",
        "prateek",
        "parashar",
        false
      );
      await updateEventLandingPageDetails(
        orgApiRequestContext,
        webinarEventId,
        {
          isSpeakermagicLinkEnabled: true,
        }
      );
      // invite this user to this webinar event as an speaker
      await inviteSpeakerByApi(
        orgApiRequestContext,
        speakerUser.getSpeakerDataObject(),
        eventInfoDto.eventId,
        true
      ); // true denote to send email

      userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.SPEAKER
      );
    });

    /**
     * disabling onboarding checks for attendee since it consumes time for attendee in test
     * Note: the api we are using here are not exposed on the UI, instead
     * I am using event side access group api for attendee to disable onboarding check by event controller
     */
    await test.step(`Disabling Onboarding Checks`, async () => {
      await new EventController(
        orgApiRequestContext,
        webinarEventId
      ).getAccessGroupController.handleOnboardingChecksForSpeaker(false);
    });

    let speakerBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    let speakerSession = await speakerBrowserContext.newPage();

    await test.step(`Speaker ${speakerEmail} lands on webinar by using magic link`, async () => {
      let landingPageTwo = new LandingPageTwo(speakerSession);
      let speakerInviteLink = await getInviteLinkFromSpeakerInviteEmail(
        speakerEmail,
        eventInfoDto.getInviteEmailSubj()
      );
      await landingPageTwo.load(speakerInviteLink);
      await landingPageTwo.isStageLoaded(true);
    });

    await test.step(`Now Speaker ${attendeeEmail} logs out of webinar and verify he lands on speaker landing page`, async () => {
      await speakerSession.click("text=Go to Backstage");
      await speakerSession.click("text=Enter Studio");
      await speakerSession.click("text=Leave studio");
      await speakerSession.locator("button:has-text('Leave')").nth(1).click();
      await speakerSession
        .locator("button:has-text('VIEW AS ATTENDEE')")
        .click();
      await speakerSession
        .locator("div[class^='styles-module__logoutButton_']")
        .click();
      await speakerSession
        .getByRole("button")
        .filter({ hasText: "Exit" })
        .click();
      await speakerSession.waitForURL(webinarSpeakerLandingPage);
    });
  });

  test(`TC008: Invited Attendee by CSV and lands on webinar stage by OTP`, async ({
    page,
  }) => {
    // // invite attendee to webinar
    await test.step(`Invite attendee to webinar by csv`, async () => {
      attendeeEmail = DataUtl.getRandomAttendeeEmail();
      eventInfoDto = new EventInfoDTO(webinarEventId, "by automation", false); // true to denote magic link enabled
      eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
      userInfoDto = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        false
      );
      // invite this user to this webinar event as an attendee
      csvFileToAddDataIn = DataUtl.getRandomCsvName();
      await addAttendeeRecordInCsv(csvFileToAddDataIn, attendeeEmail);
      let uploadedFileNameOnS3 = await getSignedS3Url(
        orgApiRequestContext,
        csvFileToAddDataIn
      );
      await processAttendeeCsvOnEndPoint(
        orgApiRequestContext,
        webinarEventId.toString(),
        uploadedFileNameOnS3
      );
      await waitForCsvFileProcessingToComplete(
        orgApiRequestContext,
        webinarEventId.toString(),
        uploadedFileNameOnS3
      );
      await updateEventLandingPageDetails(
        orgApiRequestContext,
        webinarEventId,
        {
          isMagicLinkEnabled: false,
        }
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.ATTENDEE,
        true
      );
    });

    /**
     * disabling onboarding checks for attendee since it consumes time for attendee in test
     * Note: the api we are using here are not exposed on the UI, instead
     * I am using event side access group api for attendee to disable onboarding check by event controller
     */
    await test.step(`Disabling Onboarding Checks`, async () => {
      await new EventController(
        orgApiRequestContext,
        webinarEventId
      ).getAccessGroupController.handleOnboardingChecksForAttendee(false);
    });
    await test.step(`Attendee ${attendeeEmail} lands on webinar by using OTP`, async () => {
      let landingPageTwo = new LandingPageTwo(page);
      await landingPageTwo.load(webinarAttendeeLandingPage);
      await (
        await landingPageTwo.getRegistrationFormComponent()
      ).clickOnAlreadyRegisteredButton();
      // now enter your email
      await (
        await landingPageTwo.getLoginOptionsComponent()
      ).submitEmailForOtpVerification(userEventRoleDto);
      await landingPageTwo.clickOnEnterNowButton();
      let webinarPage = await landingPageTwo.isStageLoaded(true);
      await webinarPage?.getInteractionPanel.isInteractionPanelOpen();
    });
  });

  test(`TC009: Speaker added by CSV lands on webinar stage by OTP`, async ({
    page,
  }) => {
    // // invite attendee to webinar
    await test.step(`Invite Speaker to webinar by CSV`, async () => {
      speakerEmail = DataUtl.getRandomAttendeeEmail();
      eventInfoDto = new EventInfoDTO(webinarEventId, "by automation", false); // true to denote magic link enabled
      eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
      let speakerUser = new SpeakerData(speakerEmail, "Prateek");
      userInfoDto = new UserInfoDTO(
        speakerEmail,
        "",
        "prateek",
        "parashar",
        false
      );
      // invite this user to this webinar event as an speaker
      // invite this user to this webinar event as an attendee
      csvFileToAddDataIn = DataUtl.getRandomCsvName("speaker");
      await addSpeakerRecordInCsv(csvFileToAddDataIn, speakerEmail);
      await processSpeakerCsvOnEndPoint(
        orgApiRequestContext,
        webinarEventId.toString(),
        csvFileToAddDataIn
      );
      await updateEventLandingPageDetails(
        orgApiRequestContext,
        webinarEventId,
        {
          isMagicLinkEnabled: false,
        }
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.SPEAKER
      );
    });

    /**
     * disabling onboarding checks for attendee since it consumes time for attendee in test
     * Note: the api we are using here are not exposed on the UI, instead
     * I am using event side access group api for attendee to disable onboarding check by event controller
     */
    await test.step(`Disabling Onboarding Checks`, async () => {
      await new EventController(
        orgApiRequestContext,
        webinarEventId
      ).getAccessGroupController.handleOnboardingChecksForAttendee(false);
    });

    await test.step(`Speaker ${speakerEmail} lands on webinar by using OTP`, async () => {
      let landingPageTwo = new LandingPageTwo(page);
      await landingPageTwo.load(webinarSpeakerLandingPage);
      await landingPageTwo.clickOnLoginButton();
      await (
        await landingPageTwo.getLoginOptionsComponent()
      ).submitEmailForOtpVerification(userEventRoleDto, "speakerLandingPage");
      await landingPageTwo.clickOnEnterNowButton();
    });
  });

  test(`TC010: Organiser lands on webinar stage by Password using speaker landing page`, async ({
    page,
  }) => {
    await test.step(`Update event to have password`, async () => {
      await updateEventLandingPageDetails(
        orgApiRequestContext,
        webinarEventId,
        {
          eventEntryType: EventEntryType.INVITE_ONLY,
          speakerAuthOptions: ["EMAIL"],
        }
      );
    });
    await test.step(`Organizer lands on webinar by using OTP`, async () => {
      let landingPageTwo = new LandingPageTwo(page);
      await landingPageTwo.load(webinarSpeakerLandingPage);
      await landingPageTwo.clickOnSigninButton();
      let orgEmail: string =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserEmail"];
      let orgPassword: string =
        DataUtl.getApplicationTestDataObj()["organiserPassword"];
      eventInfoDto = new EventInfoDTO(webinarEventId, "by automation", false); // true to denote magic link enabled
      eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
      userInfoDto = new UserInfoDTO(
        orgEmail,
        orgPassword,
        "prateek",
        "automation",
        true
      );
      userInfoDto.hasSetPassword = true;
      userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.ORGANISER
      );
      await (
        await landingPageTwo.getLoginOptionsComponent()
      ).submitEmailForPasswordLogin(userEventRoleDto, "speakerLandingPage");
      await landingPageTwo.clickOnEnterNowButton();
    });
  });

  test(`TC011: Verifying edit profile is not crashing in pre-defined fields case`, async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-235/case-1-crash-happening-on-click-of-edit-profile-inside-webinar",
    });
    let webinarPage: WebinarStage;
    // invite attendee to webinar
    await test.step(`Creating Attendee details for webinar`, async () => {
      let eventController: EventController = new EventController(
        orgApiRequestContext,
        webinarEventId
      );
      let ConditionalFieldsTestData =
        conditionalFieldsTestData["predefined-edit-profile-data"];
      await eventController.addCustomFieldsWithConditionToEvent(
        ConditionalFieldsTestData
      );
      attendeeEmail = DataUtl.getRandomAttendeeEmail();
      eventInfoDto = new EventInfoDTO(webinarEventId, "by automation", false); // true to denote magic link enabled
      eventInfoDto.eventEntryType = EventEntryType.REG_BASED;
      userInfoDto = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        false
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDto,
        eventInfoDto,
        EventRole.ATTENDEE,
        true
      );
    });

    /**
     * disabling onboarding checks for attendee since it consumes time for attendee in test
     * Note: the api we are using here are not exposed on the UI, instead
     * I am using event side access group api for attendee to disable onboarding check by event controller
     */
    await test.step(`Disabling Onboarding Checks`, async () => {
      await new EventController(
        orgApiRequestContext,
        webinarEventId
      ).getAccessGroupController.handleOnboardingChecksForAttendee(false);
    });
    await test.step(`Attendee ${attendeeEmail} lands on webinar after registering and through magic link`, async () => {
      let landingPageTwo = new LandingPageTwo(page);
      await landingPageTwo.load(webinarAttendeeLandingPage);
      await (
        await landingPageTwo.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDto.userRegFormData.predefinedFieldsFormDataEditProfile
      );
      let attendeePageLinkFromEmailInvite: string =
        await QueryUtil.fetchMagicLinkFromDB(webinarEventId, attendeeEmail);
      await landingPageTwo.load(attendeePageLinkFromEmailInvite);
      await landingPageTwo.clickOnEnterNowButton();
      webinarPage = await landingPageTwo.isStageLoaded(true);
      await webinarPage?.getInteractionPanel.isInteractionPanelOpen();
    });
    await test.step(`Open edit profile from settings and verify page is not crashing`, async () => {
      await page
        .locator("button[class^='styles-module__volumeButton']")
        .click();
      await page.getByText("Edit profile").click();
      let editProfilePage = new EditProfilePage(page);
      await editProfilePage.verifyEditProfileModalIsVisible();
      await expect(
        editProfilePage.firstNameInputBox,
        "expecting firstName to be prateek"
      ).toHaveValue("prateek");
    });
  });

  test(`TC012: Update webinar description and verification on webinar landing page`, async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "P0-P1",
      description:
        "https://linear.app/zuddl/issue/QAT-241/update-webinar-description-and-verification-on-webinar-landing-page",
    });

    await test.step(`Attendee lands on webinar landing page and verify old description`, async () => {
      let landingPageTwo = new LandingPageTwo(page);
      await landingPageTwo.load(webinarAttendeeLandingPage);
      await landingPageTwo.page.waitForLoadState("networkidle");
      await landingPageTwo.page.waitForLoadState("domcontentloaded");
      await landingPageTwo.page.waitForLoadState("load");
      await (
        await landingPageTwo.getAboutSectionComponents()
      ).verifyAboutText("created by automation");
    });

    await test.step(`Changing webinar description`, async () => {
      let eventController: EventController = new EventController(
        orgApiRequestContext,
        webinarEventId
      );
      await eventController.changeWebinarDescription(
        "New webinar description by automation"
      );
    });

    await test.step(`Attendee lands on webinar landing page and verify old description`, async () => {
      let landingPageTwo = new LandingPageTwo(page);
      await landingPageTwo.load(webinarAttendeeLandingPage);
      await landingPageTwo.page.waitForLoadState("networkidle");
      await landingPageTwo.page.waitForLoadState("domcontentloaded");
      await landingPageTwo.page.waitForLoadState("load");
      await (
        await landingPageTwo.getAboutSectionComponents()
      ).verifyAboutText("New webinar description by automation");
    });
  });
});
