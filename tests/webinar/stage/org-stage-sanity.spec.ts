import {
  APIRequestContext,
  BrowserContext,
  test,
  expect,
} from "@playwright/test";
import { WebinarController } from "../../../controller/WebinarController";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { WebinarStage } from "../../../page-objects/webinar-pages/webinarStage";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";

test.describe(`@webinar @stage @organiser`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  let webinarEventId;
  let webinarAttendeeLandingPage: string;
  let webinarController: WebinarController;
  let organiserName: string;
  test.beforeEach(async () => {
    //organiser browser
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiRequestContext = orgBrowserContext.request;
    await test.step("Create new Webinar.", async () => {
      webinarEventId = await new WebinarController(
        orgApiRequestContext
      ).createNewWebinar({
        title: "By automation",
      });
      console.log(`Webinar ID - ${webinarEventId}.`);
    });

    await test.step("Initialise Webinar Controller.", async () => {
      webinarController = new WebinarController(
        orgApiRequestContext,
        webinarEventId
      );
    });
    webinarAttendeeLandingPage =
      webinarController.getWebinarInfoDto.webinarAttendeeLandingPage();

    //organiser name
    let organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    let organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    organiserName = `${organiserFirstName} ${organiserLastName}`;
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`AV Off: organiser coordinating backstage->stage->backstage movements with his own stream`, async () => {
    let organiserSession = await orgBrowserContext.newPage();
    let webinarStagePage = new WebinarStage(organiserSession);
    let studioPage: StudioPage;

    await test.step(`organiser joins the backstage with his AV on`, async () => {
      await organiserSession.goto(webinarAttendeeLandingPage);
      await organiserSession.locator("text=Enter Now").click();
      studioPage = await (
        await webinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({});
    });

    await test.step(`organiser verifies his AV stream on backstage container`, async () => {
      await studioPage.getBackStageComponent.organiserVerifiesPresenceOfSpeakerStreamWithName(
        organiserName
      );
    });

    await test.step(`organiser moves himself to onstage from backstage`, async () => {
      await studioPage.getBackStageComponent.addMediaStreamToStageFromBackstage(
        organiserName
      );
      await expect(
        studioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
          organiserName
        ),
        `expecting speaker with name : ${organiserName} to be visible on main stage`
      ).toBeVisible();
      await expect(
        studioPage.getBackStageComponent.getSpeakerStreamForOnStageList("You")
      ).toBeVisible();
    });

    await test.step(`organiser removes himself from the mainstage using remove button`, async () => {
      await studioPage.getBackStageComponent.removeMediaStreamToStageFromBackstage(
        "You"
      );
    });
    await test.step(`organiser verifies his AV stream on backstage container`, async () => {
      await studioPage.getBackStageComponent.organiserVerifiesPresenceOfSpeakerStreamWithName(
        organiserName
      );
    });
  });

  test(`AV On: organiser coordinating backstage->stage->backstage movements with his own stream`, async () => {
    let organiserSession = await orgBrowserContext.newPage();
    let webinarStagePage = new WebinarStage(organiserSession);
    let studioPage: StudioPage;

    await test.step(`organiser joins the backstage with his AV on`, async () => {
      await organiserSession.goto(webinarAttendeeLandingPage);
      await organiserSession.locator("text=Enter Now").click();
      studioPage = await (
        await webinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({ muteAudio: false, muteVideo: false });
    });

    await test.step(`organiser verifies his AV stream on backstage container`, async () => {
      await studioPage.getBackStageComponent.organiserVerifiesPresenceOfSpeakerStreamWithName(
        organiserName
      );
    });

    await test.step(`organiser moves himself to onstage from backstage`, async () => {
      await studioPage.getBackStageComponent.addMediaStreamToStageFromBackstage(
        organiserName
      );
      await expect(
        studioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
          organiserName
        ),
        `expecting speaker with name : ${organiserName} to be visible on main stage`
      ).toBeVisible();
      await expect(
        studioPage.getBackStageComponent.getSpeakerStreamForOnStageList("You")
      ).toBeVisible();
    });

    await test.step(`organiser removes himself from the mainstage using remove button`, async () => {
      await studioPage.getBackStageComponent.removeMediaStreamToStageFromBackstage(
        "You"
      );
    });
    await test.step(`organiser verifies his AV stream on backstage container`, async () => {
      await studioPage.getBackStageComponent.organiserVerifiesPresenceOfSpeakerStreamWithName(
        organiserName
      );
    });
  });
});
