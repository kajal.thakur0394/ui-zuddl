import {
  APIRequestContext,
  BrowserContext,
  test,
  expect,
  Page,
} from "@playwright/test";
import { WebinarController } from "../../../controller/WebinarController";
import { InteractionPanel } from "../../../page-objects/events-pages/live-side-components/InteractionPanel";
import { InteractionPanelIframe } from "../../../page-objects/studio-pages/InteractionPanelFrame";
import { StudioJoiningAvModal } from "../../../page-objects/studio-pages/studioEntryAvModal";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { WebinarStage } from "../../../page-objects/webinar-pages/webinarStage";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import BrowserName from "../../../enums/BrowserEnum";
import { cancelEvent } from "../../../util/apiUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";
import { StageController } from "playwright-qa/controller/StageController";

test.describe(`@webinar @stage @attendee`, async () => {
  //browser contexts
  let orgBrowserContext: BrowserContext;
  let speakerBrowserContext: BrowserContext;
  let attendeeBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  // webinar details
  let webinarEventId;
  let webinarAttendeeLandingPage: string;
  let attendeeMagicLink: string;
  let organiserName: string;
  let speakerName: string;
  let speakerEmail: string;
  let attendeeEmail: string;
  let attendeeName: string;

  // objects
  let webinarController: WebinarController;
  let organiserSession: Page;
  let speakerSession: Page;
  let attendeeSession: Page;

  test.beforeEach(async () => {
    //organiser browser
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
      browserName: BrowserName.CHROME,
    });
    orgApiRequestContext = orgBrowserContext.request;

    // create new webinar
    await test.step("Create new Webinar.", async () => {
      webinarEventId = await new WebinarController(
        orgApiRequestContext
      ).createNewWebinar({
        title: "By automation",
      });
      console.log(`Webinar ID - ${webinarEventId}.`);
    });

    await test.step("Initialise Webinar Controller.", async () => {
      webinarController = new WebinarController(
        orgApiRequestContext,
        webinarEventId
      );
      await webinarController.getStudioIdForWebinar();
      let studioId = await new WebinarController(
        orgApiRequestContext,
        webinarEventId
      ).getStudioIdForWebinar();
      console.log(`Studio ID - ${studioId}`);

      let stageController = new StageController(
        orgApiRequestContext,
        webinarEventId,
        studioId
      );
      await stageController.disableRecordingForStudioAsBackstage();
    });

    // get the webinar id and webinar landing page id
    webinarEventId = webinarController.getWebinarInfoDto.webinarId;
    webinarAttendeeLandingPage =
      webinarController.getWebinarInfoDto.webinarAttendeeLandingPage();

    //organiser invites speaker to the webinar
    //organiser name
    let organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    let organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    organiserName = `${organiserFirstName} ${organiserLastName}`;

    //speaker name
    speakerName = "automation parashar";
    speakerEmail = DataUtl.getRandomSpeakerEmail();
    await webinarController.updateMagicLinkStatusForWebinar({
      enableSpeakerMagicLink: true,
      enableAttendeeMagicLink: true,
    });
    await webinarController.disableOnboardingChecksForAttendee();
    await webinarController.addNewSpeakerToWebinar(speakerEmail, true);

    //organiser invites attendee to the webinar
    attendeeName = "prateek";
    attendeeEmail = DataUtl.getRandomAttendeeTestJoynEmail();
    await webinarController.addNewAttendeeToWebinar(attendeeEmail);

    //attendee browser
    attendeeBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      browserName: BrowserName.CHROME,
    });
    //speaker browser
    speakerBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      browserName: BrowserName.CHROME,
    });

    // organiser and speaker page
    organiserSession = await orgBrowserContext.newPage();
    speakerSession = await speakerBrowserContext.newPage();
    attendeeSession = await attendeeBrowserContext.newPage();
  });

  test.afterEach(async () => {
    await cancelEvent(orgApiRequestContext, webinarEventId);
    await orgBrowserContext?.close();
    await speakerBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test(`TC001: AV OFF: Attendee viewing movement of organiser and speaker to on stage and then off stage`, async () => {
    let speakerMagicLink: string;
    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserSession
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerSession
    );
    let organiserStudioPage: StudioPage;
    let speakerStudioPage: StudioPage;
    let attendeeWebinarStagePage: WebinarStage;

    await test.step(`organiser joins the backstage with his AV ON`, async () => {
      await organiserSession.goto(webinarAttendeeLandingPage);
      await organiserSession.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({});
    });

    await test.step(`speaker logs in to the webinar with his AV off`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarEventId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerSession.goto(speakerMagicLink);
      await speakerSession.locator("text=Enter Now").click();
      speakerStudioPage = await speakerStudioJoinModal.enterInsideTheStudio({});
    });

    await test.step(`attendee logs in to the webinar`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarEventId,
        attendeeEmail
      );

      console.log(
        `fetched attendee magic link for webinar is ${attendeeMagicLink} `
      );
      await attendeeSession.goto(attendeeMagicLink);
      await attendeeSession.locator("text=Enter Now").click();

      attendeeWebinarStagePage = new WebinarStage(attendeeSession);
      await test.step(`attendee verifies that he sees the interaction panel on stage`, async () => {
        await attendeeWebinarStagePage.getInteractionPanel.isInteractionPanelOpenAndVisible();
      });
    });

    await test.step(`organiser verifies the presence of speaker stream on backstage with name ${speakerName}`, async () => {
      await organiserStudioPage.getBackStageComponent.organiserVerifiesPresenceOfSpeakerStreamWithName(
        speakerName
      );
    });

    await test.step(`speaker verifies the presence of his own stream on backstage ${speakerName}`, async () => {
      await speakerStudioPage.getBackStageComponent.organiserVerifiesPresenceOfSpeakerStreamWithName(
        speakerName
      );
    });

    await test.step(`organiser moves speaker stream from backstage to on stage`, async () => {
      await organiserStudioPage.getBackStageComponent.addMediaStreamToStageFromBackstage(
        speakerName
      );
    });

    await test.step(`organiser verifies presence of speaker stream on main stage`, async () => {
      await expect(
        organiserStudioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
          speakerName
        )
      ).toBeVisible();
    });

    await test.step(`verify that attendee is not able to see anything on stage as session is not yet ON`, async () => {
      await attendeeWebinarStagePage.verifyStagePreviewIsNotVisible();
    });

    await test.step(`organiser starts the session and speaker remains on stage`, async () => {
      await organiserStudioPage.startTheSession();
      await test.step(`organiser verifies presence of speaker stream on main stage`, async () => {
        await expect(
          organiserStudioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
            speakerName
          )
        ).toBeVisible();
      });
    });

    await test.step(`verify that attendee is able to see speaker on stage when session has started`, async () => {
      await attendeeWebinarStagePage.verifySpeakerStreamWithNameIsVisibleForAttendee(
        speakerName
      );
    });

    await test.step(`organiser ends the session and speaker remains on stage`, async () => {
      await organiserStudioPage.getMainStageComponent.endSessionOnStage();
    });

    await test.step(`verify that attendee is not able to see speaker on stage when session has ended`, async () => {
      await attendeeWebinarStagePage.verifySpeakerStreamWithNameIsNotVisibleForAttendee(
        speakerName
      );
    });

    await test.step(`organiser click on remove button on speaker stream on stage list container`, async () => {
      await organiserStudioPage.getBackStageComponent.openStageTab();
      await organiserStudioPage.getBackStageComponent.removeMediaStreamToStageFromBackstage(
        speakerName
      );
      await expect(
        organiserStudioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
          speakerName
        )
      ).not.toBeVisible();
    });
    await test.step(`organiser verifies the presence of speaker stream on backstage with name ${speakerName}`, async () => {
      await organiserStudioPage.getBackStageComponent.openBackstageTab();
      await expect(
        organiserStudioPage.getBackStageComponent.getSpeakerStreamFromBackStageList(
          speakerName
        )
      ).toBeVisible();
    });
    await test.step(`speaker verifies the presence of his own stream now on backstage with name ${speakerName}`, async () => {
      await expect(
        speakerStudioPage.getBackStageComponent.getSpeakerStreamFromBackStageList(
          speakerName
        )
      ).toBeVisible();
    });
  });

  test(`TC002: AV ON: Attendee viewing movement of organiser and speaker to on stage and then off stage`, async () => {
    let speakerMagicLink: string;
    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserSession
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerSession
    );
    let organiserStudioPage: StudioPage;
    let speakerStudioPage: StudioPage;
    let attendeeWebinarStagePage: WebinarStage;

    await test.step(`organiser joins the backstage with his AV ON`, async () => {
      await organiserSession.goto(webinarAttendeeLandingPage);
      await organiserSession.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({ muteAudio: false, muteVideo: false });
    });

    await test.step(`speaker logs in to the webinar with his AV off`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarEventId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerSession.goto(speakerMagicLink);
      await speakerSession.locator("text=Enter Now").click();
      speakerStudioPage = await speakerStudioJoinModal.enterInsideTheStudio({
        muteAudio: false,
        muteVideo: false,
      });
    });

    await test.step(`attendee logs in to the webinar`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarEventId,
        attendeeEmail
      );

      console.log(
        `fetched attendee magic link for webinar is ${attendeeMagicLink} `
      );
      await attendeeSession.goto(attendeeMagicLink);
      await attendeeSession.locator("text=Enter Now").click();

      attendeeWebinarStagePage = new WebinarStage(attendeeSession);
      await test.step(`attendee verifies that he sees the interaction panel on stage`, async () => {
        await attendeeWebinarStagePage.getInteractionPanel.isInteractionPanelOpenAndVisible();
      });
    });

    await test.step(`organiser verifies the presence of speaker stream on backstage with name ${speakerName}`, async () => {
      await organiserStudioPage.getBackStageComponent.organiserVerifiesPresenceOfSpeakerStreamWithName(
        speakerName
      );
    });

    await test.step(`speaker verifies the presence of his own stream on backstage ${speakerName}`, async () => {
      await speakerStudioPage.getBackStageComponent.organiserVerifiesPresenceOfSpeakerStreamWithName(
        speakerName
      );
    });

    await test.step(`organiser moves speaker stream from backstage to on stage`, async () => {
      await organiserStudioPage.getBackStageComponent.addMediaStreamToStageFromBackstage(
        speakerName
      );
    });

    await test.step(`organiser verifies presence of speaker stream on main stage`, async () => {
      await expect(
        organiserStudioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
          speakerName
        )
      ).toBeVisible();
    });

    await test.step(`verify that attendee is not able to see anything on stage as session is not yet ON`, async () => {
      await attendeeWebinarStagePage.verifyStagePreviewIsNotVisible();
    });

    await test.step(`organiser starts the session and speaker remains on stage`, async () => {
      await organiserStudioPage.startTheSession();
      await test.step(`organiser verifies presence of speaker stream on main stage`, async () => {
        await expect(
          organiserStudioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
            speakerName
          )
        ).toBeVisible();
      });
    });

    await test.step(`verify that attendee is able to see speaker on stage when session has started`, async () => {
      await attendeeWebinarStagePage.verifyVideoStreamIsVisibleForSpeaker(
        speakerName
      );
    });

    await test.step(`organiser ends the session and speaker remains on stage`, async () => {
      await organiserStudioPage.getMainStageComponent.endSessionOnStage();
    });

    await test.step(`verify that attendee is not able to see speaker on stage when session has started`, async () => {
      await attendeeWebinarStagePage.verifySpeakerStreamWithNameIsNotVisibleForAttendee(
        speakerName
      );
    });

    await test.step(`organiser click on remove button on speaker stream on stage list container`, async () => {
      await organiserStudioPage.getBackStageComponent.openStageTab();
      await organiserStudioPage.getBackStageComponent.removeMediaStreamToStageFromBackstage(
        speakerName
      );
      await expect(
        organiserStudioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
          speakerName
        )
      ).not.toBeVisible();
    });
    await test.step(`organiser verifies the presence of speaker stream on backstage with name ${speakerName}`, async () => {
      await organiserStudioPage.getBackStageComponent.openBackstageTab();
      await expect(
        organiserStudioPage.getBackStageComponent.getSpeakerStreamFromBackStageList(
          speakerName
        )
      ).toBeVisible();
    });
    await test.step(`speaker verifies the presence of his own stream now on backstage with name ${speakerName}`, async () => {
      await expect(
        speakerStudioPage.getBackStageComponent.getSpeakerStreamFromBackStageList(
          speakerName
        )
      ).toBeVisible();
    });
  });

  test(`TC003: Attendee, speaker and organiser are able to communicate through interaction panel - stage chat`, async () => {
    let speakerMagicLink: string;
    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserSession
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerSession
    );
    let organiserStudioPage: StudioPage;
    let speakerStudioPage: StudioPage;
    let attendeeWebinarStage: WebinarStage;
    let organiserInteractionPanel: InteractionPanelIframe;
    let speakerInteractionPanel: InteractionPanelIframe;
    let attendeeInteractionPanel: InteractionPanel;

    await test.step(`organiser joins the backstage with his AV off`, async () => {
      await organiserSession.goto(webinarAttendeeLandingPage);
      await organiserSession.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({});
    });

    await test.step(`speaker logs in to the webinar with his AV off`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarEventId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerSession.goto(speakerMagicLink);
      await speakerSession.locator("text=Enter Now").click();
      speakerStudioPage = await speakerStudioJoinModal.enterInsideTheStudio({});
    });

    await test.step(`attendee logs in to the webinar`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarEventId,
        attendeeEmail
      );

      console.log(
        `fetched attendee magic link for webinar is ${attendeeMagicLink} `
      );

      await attendeeSession.goto(attendeeMagicLink);
      await attendeeSession.locator("text=Enter Now").click();
      attendeeWebinarStage = new WebinarStage(attendeeSession);
      await test.step(`attendee verifies that he sees the interaction panel on stage`, async () => {
        await attendeeWebinarStage.getInteractionPanel.isInteractionPanelOpenAndVisible();
      });
      await attendeeWebinarStage.page.waitForTimeout(5000);
    });

    await test.step(`organiser opens the interaction panel by clicking on engage from right side control`, async () => {
      organiserInteractionPanel =
        await organiserStudioPage.getRightSideOrganiserControlPanel.clickOnEngageMenuOption();
      await organiserInteractionPanel.sendChatMessage("Hi , I am organiser");
    });

    await test.step(`for speaker, by default interaction panel -> stage chat should be opned`, async () => {
      speakerInteractionPanel = speakerStudioPage.getInteractionPanelIframe;
      await speakerInteractionPanel.sendChatMessage("Hi , I am speaker");
    });

    await test.step(`for attendee, by default stage chat should be opened`, async () => {
      attendeeInteractionPanel = attendeeWebinarStage.getInteractionPanel;
      await attendeeInteractionPanel.getChatComponent.sendChatMessage(
        "Hi, I am attendee"
      );
    });

    await test.step(`Organiser verifies he is able to see all 3 message in interaction panel`, async () => {
      await test.step(`organiser verifies he is able to see attendee message`, async () => {
        await organiserInteractionPanel.verifyCountOfMessageMatches(3);
        await organiserInteractionPanel.verifyMessageWithGivenContentIsVisible(
          "Hi , I am organiser"
        );
        await organiserInteractionPanel.verifyMessageWithGivenContentIsVisible(
          "Hi , I am speaker"
        );
        await organiserInteractionPanel.verifyMessageWithGivenContentIsVisible(
          "Hi, I am attendee"
        );
      });
    });

    await test.step(`Speaker verifies he is able to see all 3 message in interaction panel`, async () => {
      await speakerInteractionPanel.verifyCountOfMessageMatches(3);
      await speakerInteractionPanel.verifyMessageWithGivenContentIsVisible(
        "Hi , I am organiser"
      );
      await speakerInteractionPanel.verifyMessageWithGivenContentIsVisible(
        "Hi , I am speaker"
      );
      await speakerInteractionPanel.verifyMessageWithGivenContentIsVisible(
        "Hi, I am attendee"
      );
    });

    await test.step(`Attendee verifies he is able to see all 3 message in interaction panel`, async () => {
      await attendeeInteractionPanel.getChatComponent.verifyCountOfMessageMatches(
        3
      );
      await attendeeInteractionPanel.getChatComponent.verifyMessageWithGivenContentIsVisible(
        "Hi , I am organiser"
      );
      await attendeeInteractionPanel.getChatComponent.verifyMessageWithGivenContentIsVisible(
        "Hi , I am speaker"
      );
      await attendeeInteractionPanel.getChatComponent.verifyMessageWithGivenContentIsVisible(
        "Hi, I am attendee"
      );
    });
  });
});
