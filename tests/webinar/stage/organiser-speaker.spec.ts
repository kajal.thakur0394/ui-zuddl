import {
  APIRequestContext,
  BrowserContext,
  test,
  expect,
  Page,
} from "@playwright/test";
import { WebinarController } from "../../../controller/WebinarController";
import { InteractionPanelIframe } from "../../../page-objects/studio-pages/InteractionPanelFrame";
import { StudioJoiningAvModal } from "../../../page-objects/studio-pages/studioEntryAvModal";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { WebinarStage } from "../../../page-objects/webinar-pages/webinarStage";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { cancelEvent } from "../../../util/apiUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@webinar @stage @organiser`, async () => {
  let orgBrowserContext: BrowserContext;
  let speakerBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  let webinarEventId;
  let webinarAttendeeLandingPage: string;
  let attendeeMagicLink: string;
  let webinarController: WebinarController;
  let organiserSession: Page;
  let speakerSession: Page;
  let organiserName: string;
  let speakerName: string;
  let speakerEmail: string;
  test.beforeEach(async () => {
    //organiser browser
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiRequestContext = orgBrowserContext.request;

    await test.step("Create new Webinar.", async () => {
      webinarEventId = await new WebinarController(
        orgApiRequestContext
      ).createNewWebinar({
        title: "By automation",
      });
      console.log(`Webinar ID - ${webinarEventId}.`);
    });

    await test.step("Initialise Webinar Controller.", async () => {
      webinarController = new WebinarController(
        orgApiRequestContext,
        webinarEventId
      );
      await webinarController.getStudioIdForWebinar();
    });

    webinarAttendeeLandingPage =
      webinarController.getWebinarInfoDto.webinarAttendeeLandingPage();
    //organiser name
    let organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    let organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];
    organiserName = `${organiserFirstName} ${organiserLastName}`;

    //speaker name and email
    speakerName = "automation parashar";
    speakerEmail = DataUtl.getRandomSpeakerEmail();

    //enabling magic link for the webinar
    await webinarController.updateMagicLinkStatusForWebinar({
      enableSpeakerMagicLink: true,
      enableAttendeeMagicLink: true,
    });

    //inviting speaker to the webinar
    await webinarController.addNewSpeakerToWebinar(speakerEmail, true);

    //speaker browser
    speakerBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    // organiser and speaker page
    organiserSession = await orgBrowserContext.newPage();
    speakerSession = await speakerBrowserContext.newPage();
  });

  test.afterEach(async () => {
    await cancelEvent(orgApiRequestContext, webinarEventId);
    await orgBrowserContext?.close();
    await speakerBrowserContext?.close();
  });

  test(`TC001: AV OFF: organiser coordinating backstage->stage->backstage movements with speaker stream`, async () => {
    let speakerMagicLink: string;
    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserSession
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerSession
    );
    let organiserStudioPage: StudioPage;
    let speakerStudioPage: StudioPage;

    await test.step(`organiser joins the backstage with his AV on`, async () => {
      await organiserSession.goto(webinarAttendeeLandingPage);
      await organiserSession.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({});
    });

    await test.step(`speaker logs in to the webinar`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarEventId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerSession.goto(speakerMagicLink);
      await speakerSession.locator("text=Enter Now").click();
      speakerStudioPage = await speakerStudioJoinModal.enterInsideTheStudio({});
    });

    await test.step(`organiser verifies the presence of speaker stream on backstage with name ${speakerName}`, async () => {
      await organiserStudioPage.getBackStageComponent.organiserVerifiesPresenceOfSpeakerStreamWithName(
        speakerName
      );
    });

    await test.step(`speaker verifies the presence of his own stream on backstage ${speakerName}`, async () => {
      await speakerStudioPage.getBackStageComponent.organiserVerifiesPresenceOfSpeakerStreamWithName(
        speakerName
      );
      //   await expect(
      //     speakerSession
      //       .locator("p[class*='stream-video_streamUserName']")
      //       .filter({ hasText: speakerName })
      //   ).toBeVisible();
    });

    await test.step(`organiser moves speaker stream from backstage to on stage`, async () => {
      await organiserStudioPage.getBackStageComponent.addMediaStreamToStageFromBackstage(
        speakerName
      );
    });

    await test.step(`organiser verifies presence of speaker stream on main stage`, async () => {
      await expect(
        organiserStudioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
          speakerName
        )
      ).toBeVisible();
    });

    await test.step(`organiser click on remove button on speaker stream on stage list container`, async () => {
      await organiserStudioPage.getBackStageComponent.openStageTab();
      await organiserStudioPage.getBackStageComponent.removeMediaStreamToStageFromBackstage(
        speakerName
      );
      await expect(
        organiserStudioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
          speakerName
        )
      ).not.toBeVisible();
    });
    await test.step(`organiser verifies the presence of speaker stream on backstage with name ${speakerName}`, async () => {
      await organiserStudioPage.getBackStageComponent.openBackstageTab();
      await expect(
        organiserStudioPage.getBackStageComponent.getSpeakerStreamFromBackStageList(
          speakerName
        )
      ).toBeVisible();
    });
    await test.step(`speaker verifies the presence of his own stream now on backstage with name ${speakerName}`, async () => {
      //   await expect(
      //     speakerSession
      //       .locator("p[class*='stream-video_streamUserName']")
      //       .filter({ hasText: speakerName })
      //   ).toBeVisible();
      await expect(
        speakerStudioPage.getBackStageComponent.getSpeakerStreamFromBackStageList(
          speakerName
        )
      ).toBeVisible();
    });
  });

  test(`TC002: AV ON: organiser coordinating backstage->stage->backstage movements with speaker stream`, async () => {
    let speakerMagicLink: string;
    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserSession
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerSession
    );
    let organiserStudioPage: StudioPage;
    let speakerStudioPage: StudioPage;

    await test.step(`organiser joins the backstage with his AV ON`, async () => {
      await organiserSession.goto(webinarAttendeeLandingPage);
      await organiserSession.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({ muteAudio: false, muteVideo: false });
    });

    await test.step(`speaker logs in to the webinar and enter inside studio with his AV ON`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarEventId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerSession.goto(speakerMagicLink);
      await speakerSession.locator("text=Enter Now").click();
      speakerStudioPage = await speakerStudioJoinModal.enterInsideTheStudio({
        muteAudio: false,
        muteVideo: false,
      });
    });

    await test.step(`organiser verifies the presence of speaker stream on backstage with name ${speakerName}`, async () => {
      await organiserStudioPage.getBackStageComponent.organiserVerifiesPresenceOfSpeakerStreamWithName(
        speakerName
      );
    });

    await test.step(`speaker verifies the presence of his own stream on backstage ${speakerName}`, async () => {
      await speakerStudioPage.getBackStageComponent.organiserVerifiesPresenceOfSpeakerStreamWithName(
        speakerName
      );
    });

    await test.step(`organiser moves speaker stream from backstage to on stage`, async () => {
      await organiserStudioPage.getBackStageComponent.addMediaStreamToStageFromBackstage(
        speakerName
      );
    });

    await test.step(`organiser verifies presence of speaker stream on main stage`, async () => {
      await expect(
        organiserStudioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
          speakerName
        )
      ).toBeVisible();
    });

    await test.step(`organiser click on remove button on speaker stream on stage list container`, async () => {
      await organiserStudioPage.getBackStageComponent.openStageTab();
      await organiserSession.waitForTimeout(2000);
      await organiserStudioPage.getBackStageComponent.removeMediaStreamToStageFromBackstage(
        speakerName
      );
      await expect(
        organiserStudioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
          speakerName
        )
      ).not.toBeVisible();
      await organiserSession.waitForTimeout(2000);
    });
    await test.step(`speaker verifies the presence of his own stream now on backstage with name ${speakerName}`, async () => {
      await expect(
        speakerStudioPage.getBackStageComponent.getSpeakerStreamFromBackStageList(
          speakerName
        )
      ).toBeVisible();
    });
    await test.step(`organiser verifies the presence of speaker stream on backstage with name ${speakerName}`, async () => {
      await organiserStudioPage.getBackStageComponent.openBackstageTab();
      await expect(
        organiserStudioPage.getBackStageComponent.getSpeakerStreamFromBackStageList(
          speakerName
        )
      ).toBeVisible();
    });
  });

  test(`Attendee and speaker are able to communicate through interaction panel - backstage chat`, async () => {
    let speakerMagicLink: string;
    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserSession
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerSession
    );
    let organiserStudioPage: StudioPage;
    let speakerStudioPage: StudioPage;
    let organiserInteractionPanel: InteractionPanelIframe;
    let speakerInteractionPanel: InteractionPanelIframe;

    await test.step(`organiser joins the backstage with his AV off`, async () => {
      await organiserSession.goto(webinarAttendeeLandingPage);
      await organiserSession.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({});
    });

    await test.step(`speaker logs in to the webinar with his AV off`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarEventId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerSession.goto(speakerMagicLink);
      await speakerSession.locator("text=Enter Now").click();
      speakerStudioPage = await speakerStudioJoinModal.enterInsideTheStudio({});
    });

    await test.step(`organiser opens the interaction panel by clicking on engage from right side control`, async () => {
      organiserInteractionPanel =
        await organiserStudioPage.getRightSideOrganiserControlPanel.clickOnEngageMenuOption();
    });
    await test.step(`organiser open the backstage tab and sends the message`, async () => {
      await organiserInteractionPanel.openBackStageTab();
      await organiserInteractionPanel.sendChatMessage("Hi , I am organiser");
    });

    await test.step(`speaker open the backstage tab`, async () => {
      speakerInteractionPanel = speakerStudioPage.getInteractionPanelIframe;
      await speakerInteractionPanel.openBackStageTab();
      await speakerInteractionPanel.sendChatMessage("Hi , I am speaker");
    });

    await test.step(`Organiser verifies he is able to see all 3 message in backstage chat panel`, async () => {
      await test.step(`organiser verifies he is able to see attendee message`, async () => {
        await organiserInteractionPanel.verifyCountOfMessageMatches(2);
        await organiserInteractionPanel.verifyMessageWithGivenContentIsVisible(
          "Hi , I am organiser"
        );
        await organiserInteractionPanel.verifyMessageWithGivenContentIsVisible(
          "Hi , I am speaker"
        );
      });
    });

    await test.step(`Speaker verifies he is able to see all 3 message in interaction panel`, async () => {
      await speakerInteractionPanel.verifyCountOfMessageMatches(2);
      await speakerInteractionPanel.verifyMessageWithGivenContentIsVisible(
        "Hi , I am organiser"
      );
      await speakerInteractionPanel.verifyMessageWithGivenContentIsVisible(
        "Hi , I am speaker"
      );
    });
  });
});
