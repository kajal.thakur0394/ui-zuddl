import {
  APIRequestContext,
  BrowserContext,
  test,
  Page,
} from "@playwright/test";
import { WebinarController } from "../../../controller/WebinarController";
import { StudioJoiningAvModal } from "../../../page-objects/studio-pages/studioEntryAvModal";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { WebinarStage } from "../../../page-objects/webinar-pages/webinarStage";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import BrowserName from "../../../enums/BrowserEnum";
import { EventController } from "../../../controller/EventController";
import { cancelEvent } from "../../../util/apiUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";
import { StageController } from "playwright-qa/controller/StageController";

test.describe(`@webinar @stage @video @static-webinar`, async () => {
  //browser contexts
  let orgBrowserContext: BrowserContext;
  let speakerBrowserContext: BrowserContext;
  let attendeeBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  // webinar details
  let webinarEventId;
  let webinarAttendeeLandingPage: string;
  let attendeeMagicLink: string;
  let organiserName: string;
  let speakerName: string;
  let speakerEmail: string;
  let attendeeEmail: string;
  let attendeeName: string;

  // objects
  let webinarController: WebinarController;
  let organiserSession: Page;
  let speakerSession: Page;
  let attendeeSession: Page;

  test.beforeEach(async () => {
    //organiser browser
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
      browserName: BrowserName.CHROME,
    });
    orgApiRequestContext = orgBrowserContext.request;

    // create new webinar

    webinarEventId =
      DataUtl.getApplicationTestDataObj()["webinarIdForVideoPlayBackTesting"];

    let eventController = new EventController(
      orgApiRequestContext,
      webinarEventId
    );

    let duplicateEventId = await eventController.duplicateEvent({
      title: "static-webinar-duplicate",
      deltaHours: 5,
      deltaDays: 500,
      tz: Intl.DateTimeFormat().resolvedOptions().timeZone,
    });
    webinarEventId = duplicateEventId;
    console.log(`duplicate event id is ${duplicateEventId}`);
    webinarController = new WebinarController(
      orgApiRequestContext,
      webinarEventId
    );
    let studioId = await webinarController.getStudioIdForWebinar();
    await webinarController.updateWebinarStartAndEndTime({
      deltaByHoursInStartTime: 1,
      deltaByHoursInEndTime: 3,
      eventTimeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
    });

    // get the webinar id and webinar landing page id
    webinarAttendeeLandingPage =
      webinarController.getWebinarInfoDto.webinarAttendeeLandingPage();

    //organiser invites speaker to the webinar
    organiserName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    speakerName = "automation parashar";
    speakerEmail = DataUtl.getRandomSpeakerEmail();
    await webinarController.updateMagicLinkStatusForWebinar({
      enableSpeakerMagicLink: true,
      enableAttendeeMagicLink: true,
    });

    // setting up test environment of webinar

    await webinarController.disableOnboardingChecksForAttendee();
    await webinarController.addNewSpeakerToWebinar(speakerEmail, true);
    await webinarController.stopFillerImageBroadcastOnStudio();
    await webinarController.stopFillerVideoBroadcastOnStudio();
    await webinarController.stopVideoPlayBackOnStudio();

    //organiser invites attendee to the webinar
    attendeeName = "prateek";
    attendeeEmail = DataUtl.getRandomAttendeeTestJoynEmail();
    await webinarController.addNewAttendeeToWebinar(attendeeEmail);

    //attendee browser
    attendeeBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      browserName: BrowserName.CHROME,
    });
    //speaker browser
    speakerBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      browserName: BrowserName.CHROME,
    });

    // organiser and speaker page
    organiserSession = await orgBrowserContext.newPage();
    speakerSession = await speakerBrowserContext.newPage();
    attendeeSession = await attendeeBrowserContext.newPage();

    let stageController = new StageController(
      orgApiRequestContext,
      webinarEventId,
      studioId
    );
    await stageController.disableRecordingForStudioAsBackstage();
  });

  test.afterEach(async () => {
    // close the studio session if already running
    await webinarController.endLiveSessionOnStudio();
    await webinarController.stopVideoPlayBackOnStudio();
    await cancelEvent(orgApiRequestContext, webinarEventId);
    await orgBrowserContext?.close();
    await speakerBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test(`TC001: handle video play back from content panel on studio webinar`, async () => {
    test.slow();
    let speakerMagicLink: string;
    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserSession
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerSession
    );
    let organiserStudioPage: StudioPage;
    let speakerStudioPage: StudioPage;
    let attendeeWebinarStage: WebinarStage;

    console.log(`webinar link ${webinarAttendeeLandingPage}`);

    await test.step(`organiser joins the backstage with his AV ON`, async () => {
      await organiserSession.goto(webinarAttendeeLandingPage);
      await organiserSession.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({});
    });

    await test.step(`speaker logs in to the webinar with his AV off`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarEventId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerSession.goto(speakerMagicLink);
      await speakerSession.locator("text=Enter Now").click();
      speakerStudioPage = await speakerStudioJoinModal.enterInsideTheStudio({});
    });

    await test.step(`attendee logs in to the webinar`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarEventId,
        attendeeEmail
      );

      console.log(
        `fetched attendee magic link for webinar is ${attendeeMagicLink} `
      );

      await attendeeSession.goto(attendeeMagicLink);
      await attendeeSession.locator("text=Enter Now").click();
      attendeeWebinarStage = new WebinarStage(attendeeSession);
      await test.step(`attendee verifies that he sees the interaction panel on stage`, async () => {
        await attendeeWebinarStage.getInteractionPanel.isInteractionPanelOpenAndVisible();
      });
    });

    await test.step(`organiser plays the uploaded video from content section to display on stage`, async () => {
      const organiserContentUploadPage =
        await organiserStudioPage.getRightSideOrganiserControlPanel.clickOnContentMenuOption();
      await organiserSession.waitForTimeout(3000);
      await organiserContentUploadPage.playUploadedVideoOnStage();
    });

    await test.step(`verify organiser is able to see video playback on main stage`, async () => {
      // await organiserSession.bringToFront();
      await organiserStudioPage.getMainStageComponent.verifyVideoPlayBackVisibleOnMainStage();
    });

    await test.step(`verify speaker is able to see video playback being displayed on main stage`, async () => {
      // await speakerSession.bringToFront();
      await speakerStudioPage.getMainStageComponent.verifyVideoPlayBackVisibleOnMainStage();
    });

    await test.step(`organiser starts the session`, async () => {
      // await organiserSession.bringToFront();
      await organiserStudioPage.startTheSession();
    });

    await test.step(`verify organiser is able to see video playback on main stage after starting the session`, async () => {
      await organiserStudioPage.getMainStageComponent.verifyVideoPlayBackVisibleOnMainStage();
    });

    await test.step(`verify speaker is able to see video playback being displayed on main stage after start of session`, async () => {
      await speakerStudioPage.getMainStageComponent.verifyVideoPlayBackVisibleOnMainStage();
    });

    await test.step(`verify attendee is now able to see video playback being displayed on main stage`, async () => {
      await attendeeWebinarStage.verifyVideoPlayBackIsVisibleOnStage();
    });

    await test.step(`organiser stops the video playback`, async () => {
      const organiserContentUploadPage =
        await organiserStudioPage.getRightSideOrganiserControlPanel.clickOnContentMenuOption();
      await organiserSession.waitForTimeout(3000);
      await organiserContentUploadPage.stopPlayingVideoOnStage();
    });

    await test.step(`verify organiser is now not able to see video playback on main stage`, async () => {
      await organiserStudioPage.getMainStageComponent.verifyVideoPlayBackIsNotVisibleOnMainStage();
    });

    await test.step(`verify speaker is now not able to see video playback  on main stage`, async () => {
      await speakerStudioPage.getMainStageComponent.verifyVideoPlayBackIsNotVisibleOnMainStage();
    });

    await test.step(`verify attendee is now not able to see video playback being displayed on main stage`, async () => {
      await attendeeWebinarStage.verifyVideoPlayBackIsNotVisibleOnStage();
    });
  });

  test(`TC002: handle filler video broadcasting`, async () => {
    let speakerMagicLink: string;
    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserSession
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerSession
    );
    let organiserStudioPage: StudioPage;
    let speakerStudioPage: StudioPage;
    let attendeeWebinarStage: WebinarStage;

    await test.step(`organiser joins the backstage with his AV ON`, async () => {
      await organiserSession.goto(webinarAttendeeLandingPage);
      await organiserSession.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({});
    });

    await test.step(`speaker logs in to the webinar with his AV off`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarEventId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerSession.goto(speakerMagicLink);
      await speakerSession.locator("text=Enter Now").click();
      speakerStudioPage = await speakerStudioJoinModal.enterInsideTheStudio({});
    });

    await test.step(`attendee logs in to the webinar`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarEventId,
        attendeeEmail
      );

      console.log(
        `fetched attendee magic link for webinar is ${attendeeMagicLink} `
      );

      await attendeeSession.goto(attendeeMagicLink);
      await attendeeSession.locator("text=Enter Now").click();
      attendeeWebinarStage = new WebinarStage(attendeeSession);
      await test.step(`attendee verifies that he sees the interaction panel on stage`, async () => {
        await attendeeWebinarStage.getInteractionPanel.isInteractionPanelOpenAndVisible();
      });
    });

    await test.step(`organiser opens filler section to publish uploaded video`, async () => {
      const organiserFillerSection =
        await organiserStudioPage.getRightSideOrganiserControlPanel.clickOnFillerMenuOption();
      await organiserFillerSection.playFillerVideoOnStage();
    });

    await test.step(`verify attendee is able to see broadcasted video on stage`, async () => {
      await attendeeWebinarStage.verifyVideoPlayBackIsVisibleOnStage();
    });

    await test.step(`org sends speaker to on stage from backstage`, async () => {
      await organiserStudioPage.getBackStageComponent.addMediaStreamToStageFromBackstage(
        speakerName
      );
    });

    await test.step(`org verifies the presence of speaker stream on main stage`, async () => {
      await organiserStudioPage.getBackStageComponent.organiserVerifiesPresenceOfSpeakerStreamWithName(
        speakerName
      );
    });

    await test.step(`speaker verifies the presence of his own stream on main stage`, async () => {
      await organiserStudioPage.getBackStageComponent.organiserVerifiesPresenceOfSpeakerStreamWithName(
        speakerName
      );
    });

    await test.step(`verify that attendee still is not able to see speaker on stage`, async () => {
      await attendeeWebinarStage.verifySpeakerStreamWithNameIsNotVisibleForAttendee(
        speakerName
      );
    });

    await test.step(`verify that attendee is still seeing the filler video play back`, async () => {
      await attendeeWebinarStage.verifyVideoPlayBackIsVisibleOnStage();
    });

    await test.step(`organiser starts the session`, async () => {
      await organiserStudioPage.startTheSession();
    });

    await test.step(`org verifies the presence of speaker stream on main stage after starting the session`, async () => {
      await organiserStudioPage.getMainStageComponent.verifyPresenceOfSpeakerStreamOnMainStage(
        speakerName
      );
    });

    await test.step(`verify that attendee now sees the speaker stream on stage`, async () => {
      await attendeeWebinarStage.verifySpeakerStreamWithNameIsVisibleForAttendee(
        speakerName
      );
    });

    await test.step(`verify that organiser now end the session`, async () => {
      await organiserStudioPage.getMainStageComponent.endSessionOnStage();
    });

    await test.step(`verify that attendee again start seeing the video play back on stage`, async () => {
      await attendeeWebinarStage.verifyVideoPlayBackIsVisibleOnStage();
    });
  });
  test(`TC003: handle filler image broadcasting`, async () => {
    let speakerMagicLink: string;
    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserSession
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerSession
    );
    let organiserStudioPage: StudioPage;
    let speakerStudioPage: StudioPage;
    let attendeeWebinarStage: WebinarStage;

    await test.step(`organiser joins the backstage with his AV ON`, async () => {
      await organiserSession.goto(webinarAttendeeLandingPage);
      await organiserSession.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({});
    });

    await test.step(`speaker logs in to the webinar with his AV off`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarEventId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerSession.goto(speakerMagicLink);
      await speakerSession.locator("text=Enter Now").click();
      speakerStudioPage = await speakerStudioJoinModal.enterInsideTheStudio({});
    });

    await test.step(`attendee logs in to the webinar`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarEventId,
        attendeeEmail
      );

      console.log(
        `fetched attendee magic link for webinar is ${attendeeMagicLink} `
      );

      await attendeeSession.goto(attendeeMagicLink);
      await attendeeSession.locator("text=Enter Now").click();
      attendeeWebinarStage = new WebinarStage(attendeeSession);
      await test.step(`attendee verifies that he sees the interaction panel on stage`, async () => {
        await attendeeWebinarStage.getInteractionPanel.isInteractionPanelOpenAndVisible();
      });
    });

    await test.step(`organiser opens filler section to broadcast filler image`, async () => {
      const organiserFillerSection =
        await organiserStudioPage.getRightSideOrganiserControlPanel.clickOnFillerMenuOption();
      await organiserFillerSection.broadcastFillerImageOnStage();
    });

    await test.step(`verify attendee is able to see broadcasted filler image on stage`, async () => {
      await attendeeWebinarStage.verifyFillerImageBroadcastIsVisibleOnStage();
    });

    await test.step(`org sends speaker to on stage from backstage`, async () => {
      await organiserStudioPage.getBackStageComponent.addMediaStreamToStageFromBackstage(
        speakerName
      );
    });

    await test.step(`org verifies the presence of speaker stream on main stage`, async () => {
      await organiserStudioPage.getBackStageComponent.organiserVerifiesPresenceOfSpeakerStreamWithName(
        speakerName
      );
    });

    await test.step(`speaker verifies the presence of his own stream on main stage`, async () => {
      await organiserStudioPage.getBackStageComponent.organiserVerifiesPresenceOfSpeakerStreamWithName(
        speakerName
      );
    });

    await test.step(`verify that attendee still is not able to see speaker on stage`, async () => {
      await attendeeWebinarStage.verifySpeakerStreamWithNameIsNotVisibleForAttendee(
        speakerName
      );
    });

    await test.step(`verify that attendee is still seeing the broadcasted filler image`, async () => {
      await attendeeWebinarStage.verifyFillerImageBroadcastIsVisibleOnStage();
    });

    await test.step(`organiser starts the session`, async () => {
      await organiserStudioPage.startTheSession();
    });

    await test.step(`org verifies the presence of speaker stream on main stage after starting the session`, async () => {
      await organiserStudioPage.getMainStageComponent.verifyPresenceOfSpeakerStreamOnMainStage(
        speakerName
      );
    });

    await test.step(`verify that attendee now sees the speaker stream on stage`, async () => {
      await attendeeWebinarStage.verifySpeakerStreamWithNameIsVisibleForAttendee(
        speakerName
      );
    });

    await test.step(`verify that organiser now end the session`, async () => {
      await organiserStudioPage.getMainStageComponent.endSessionOnStage();
    });

    await test.step(`verify that attendee again start seeing the broadcasted filler image on stage after end session`, async () => {
      await attendeeWebinarStage.verifyFillerImageBroadcastIsNotVisibleOnStage();
    });
  });
});
