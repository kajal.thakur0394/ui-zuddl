import {
  APIRequestContext,
  BrowserContext,
  test,
  expect,
  Page,
} from "@playwright/test";
import { WebinarController } from "../../../controller/WebinarController";
import { StudioJoiningAvModal } from "../../../page-objects/studio-pages/studioEntryAvModal";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { WebinarStage } from "../../../page-objects/webinar-pages/webinarStage";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { cancelEvent } from "../../../util/apiUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";
import { StageController } from "playwright-qa/controller/StageController";

test.describe(`@webinar @stage @stage-update`, async () => {
  //browser contexts
  let orgBrowserContext: BrowserContext;
  let speakerBrowserContext: BrowserContext;
  let attendeeBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  // webinar details
  let webinarEventId;
  let webinarAttendeeLandingPage: string;
  let attendeeMagicLink: string;
  let organiserName: string;
  let speakerName: string;
  let speakerEmail: string;
  let attendeeEmail: string;
  let attendeeName: string;

  // objects
  let webinarController: WebinarController;
  let organiserSession: Page;
  let speakerSession: Page;
  let attendeeSession: Page;

  test.beforeEach(async () => {
    //organiser browser
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiRequestContext = orgBrowserContext.request;

    // create new webinar

    // create new webinar
    await test.step("Create new Webinar.", async () => {
      webinarEventId = await new WebinarController(
        orgApiRequestContext
      ).createNewWebinar({
        title: "By automation",
      });
      console.log(`Webinar ID - ${webinarEventId}.`);
    });

    await test.step("Initialise Webinar Controller.", async () => {
      webinarController = new WebinarController(
        orgApiRequestContext,
        webinarEventId
      );
      let studioId = await new WebinarController(
        orgApiRequestContext,
        webinarEventId
      ).getStudioIdForWebinar();
      console.log(`Studio ID - ${studioId}`);

      await webinarController.getStudioIdForWebinar();

      let stageController = new StageController(
        orgApiRequestContext,
        webinarEventId,
        studioId
      );
      await stageController.disableRecordingForStudioAsBackstage();
    });

    webinarAttendeeLandingPage =
      webinarController.getWebinarInfoDto.webinarAttendeeLandingPage();

    //organiser invites speaker to the webinar
    let organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    let organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    organiserName = `${organiserFirstName} ${organiserLastName}`;
    speakerName = "automation parashar";
    speakerEmail = DataUtl.getRandomSpeakerEmail();
    await webinarController.updateMagicLinkStatusForWebinar({
      enableSpeakerMagicLink: true,
      enableAttendeeMagicLink: true,
    });
    await webinarController.disableOnboardingChecksForAttendee();
    await webinarController.addNewSpeakerToWebinar(speakerEmail, true);

    //organiser invites attendee to the webinar
    attendeeName = "prateek";
    attendeeEmail = DataUtl.getRandomAttendeeTestJoynEmail();
    await webinarController.addNewAttendeeToWebinar(attendeeEmail);

    //attendee browser
    attendeeBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    //speaker browser
    speakerBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    // organiser and speaker page
    organiserSession = await orgBrowserContext.newPage();
    speakerSession = await speakerBrowserContext.newPage();
    attendeeSession = await attendeeBrowserContext.newPage();
  });

  test.afterEach(async () => {
    // close the studio session if already running
    await webinarController.endLiveSessionOnStudio();
    await webinarController.stopVideoPlayBackOnStudio();
    await cancelEvent(orgApiRequestContext, webinarEventId);
    await orgBrowserContext?.close();
    await speakerBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test(`TC001: updating stage with multiple branding options and speaker together on stage`, async () => {
    let speakerMagicLink: string;
    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserSession
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerSession
    );
    let organiserStudioPage: StudioPage;
    let speakerStudioPage: StudioPage;
    let attendeeWebinarStage: WebinarStage;

    console.log(`webinar link ${webinarAttendeeLandingPage}`);

    await test.step(`organiser joins the backstage with his AV ON`, async () => {
      await organiserSession.goto(webinarAttendeeLandingPage);
      await organiserSession.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({});
    });

    await test.step(`speaker logs in to the webinar with his AV off`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarEventId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerSession.goto(speakerMagicLink);
      await speakerSession.locator("text=Enter Now").click();
      speakerStudioPage = await speakerStudioJoinModal.enterInsideTheStudio({});
    });

    await test.step(`attendee logs in to the webinar`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarEventId,
        attendeeEmail
      );

      console.log(
        `fetched attendee magic link for webinar is ${attendeeMagicLink} `
      );

      await attendeeSession.goto(attendeeMagicLink);
      await attendeeSession.locator("text=Enter Now").click();
      attendeeWebinarStage = new WebinarStage(attendeeSession);
      await test.step(`attendee verifies that he sees the interaction panel on stage`, async () => {
        await attendeeWebinarStage.getInteractionPanel.isInteractionPanelOpenAndVisible();
      });
    });

    await test.step(`organiser selects 1 logo, 1 background and 1 overlay to update stage with`, async () => {
      const organiserBrandingPanel =
        await organiserStudioPage.getRightSideOrganiserControlPanel.clickOnBrandingMenuOption();

      await organiserBrandingPanel.selectLogoByCheckbox(2);
      await organiserBrandingPanel.selectBackgroundByCheckbox(2);
      await organiserBrandingPanel.selectOverlayByCheckbox(2);
    });

    await test.step(`organiser selects speaker stream from backstage listing container`, async () => {
      await organiserStudioPage.getBackStageComponent.selectSpeakerMediaStream(
        speakerName
      );
    });

    await test.step(`organiser selects his own stream from backstage listing container`, async () => {
      await organiserStudioPage.getBackStageComponent.selectSpeakerMediaStream(
        organiserName
      );
    });

    await test.step(`oganiser clicks on update all button to update the stage with selected changes`, async () => {
      const multiSelectApiPromise =
        organiserSession.waitForResponse(/multiselect/);
      await organiserStudioPage.getBackStageComponent.clickOnUpdateStageButton();
      const multiSelectPatchApiResponse = await multiSelectApiPromise;
      if (multiSelectPatchApiResponse.status() != 200) {
        throw new Error(
          `Patch multiselect API call to update on stage failed with ${multiSelectPatchApiResponse.status()}`
        );
      }
    });

    await test.step(`verify organiser is able to see changes made reflected on stage`, async () => {
      await test.step(`organiser verifies the presence of new logo on main stage`, async () => {
        await organiserStudioPage.getMainStageComponent.verifyIfLogoIsDisplayedOnMainStage();
      });
      await test.step(`organiser verifies the presence of new background on main stage`, async () => {
        await organiserStudioPage.getMainStageComponent.verifyBackGroundIsAppliedOnMainstage();
      });
      await test.step(`organiser verifies the presence of new overlay on main stage`, async () => {
        await organiserStudioPage.getMainStageComponent.verifyOverLayIsBeingDisplayedOnMainStage();
      });
      await test.step(`organiser is able to see speaker media on stage`, async () => {
        await expect(
          organiserStudioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
            speakerName
          )
        ).toBeVisible();
      });
      await test.step(`organiser is able to see his own media on stage`, async () => {
        await expect(
          organiserStudioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
            organiserName
          )
        ).toBeVisible();
      });
    });

    await test.step(`organiser starts the session`, async () => {
      await organiserStudioPage.startTheSession();
    });

    await test.step(`verify speaker is able to see changes made reflected on stage`, async () => {
      await test.step(`speaker verifies the presence of new logo on main stage`, async () => {
        await speakerStudioPage.getMainStageComponent.verifyIfLogoIsDisplayedOnMainStage();
      });
      await test.step(`speaker verifies the presence of new background on main stage`, async () => {
        await speakerStudioPage.getMainStageComponent.verifyBackGroundIsAppliedOnMainstage();
      });
      await test.step(`speaker verifies the presence of new overlay on main stage`, async () => {
        await speakerStudioPage.getMainStageComponent.verifyOverLayIsBeingDisplayedOnMainStage();
      });
      await test.step(`speaker is able to see speaker media on stage`, async () => {
        await expect(
          speakerStudioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
            organiserName
          )
        ).toBeVisible();
      });
      await test.step(`speaker is able to see his own media on stage`, async () => {
        await expect(
          speakerStudioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
            speakerName
          )
        ).toBeVisible();
      });
    });

    await test.step(`verify attendee is able to see changes made reflected on stage`, async () => {
      await test.step(`attendee verifies the presence of new logo on main stage`, async () => {
        await attendeeWebinarStage.verifyIfLogoIsDisplayedOnMainStage();
      });
      await test.step(`attendee verifies the presence of new background on main stage`, async () => {
        await attendeeWebinarStage.verifyBackGroundIsAppliedOnMainstage();
      });
      await test.step(`attendee verifies the presence of new overlay on main stage`, async () => {
        await attendeeWebinarStage.verifyOverLayIsBeingDisplayedOnMainStage();
      });
      await test.step(`attendee is able to see speaker stream on stage`, async () => {
        await attendeeWebinarStage.verifySpeakerStreamWithNameIsVisibleForAttendee(
          speakerName
        );
      });
      await test.step(`attendee is able to see his organiser stream on stage`, async () => {
        await attendeeWebinarStage.verifySpeakerStreamWithNameIsVisibleForAttendee(
          organiserName
        );
      });
    });
  });
});
