import {
  APIRequestContext,
  BrowserContext,
  test,
  Page,
} from "@playwright/test";
import { WebinarController } from "../../../controller/WebinarController";
import { StudioJoiningAvModal } from "../../../page-objects/studio-pages/studioEntryAvModal";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { WebinarStage } from "../../../page-objects/webinar-pages/webinarStage";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import BrowserName from "../../../enums/BrowserEnum";
import { cancelEvent } from "../../../util/apiUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";
import { StageController } from "playwright-qa/controller/StageController";

test.describe(`@webinar @stage @banners`, async () => {
  //browser contexts
  let orgBrowserContext: BrowserContext;
  let speakerBrowserContext: BrowserContext;
  let attendeeBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  // webinar details
  let webinarEventId;
  let webinarAttendeeLandingPage: string;
  let attendeeMagicLink: string;
  let organiserName: string;
  let speakerName: string;
  let speakerEmail: string;
  let attendeeEmail: string;
  let attendeeName: string;

  // objects
  let webinarController: WebinarController;
  let organiserSession: Page;
  let speakerSession: Page;
  let attendeeSession: Page;

  test.beforeEach(async () => {
    //organiser browser
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
      browserName: BrowserName.CHROME,
    });
    orgApiRequestContext = orgBrowserContext.request;
    await test.step("Create new Webinar.", async () => {
      webinarEventId = await new WebinarController(
        orgApiRequestContext
      ).createNewWebinar({
        title: "By automation",
      });
      console.log(`Webinar ID - ${webinarEventId}.`);
    });

    await test.step("Initialise Webinar Controller.", async () => {
      webinarController = new WebinarController(
        orgApiRequestContext,
        webinarEventId
      );
      await webinarController.getStudioIdForWebinar();
      let studioId = await new WebinarController(
        orgApiRequestContext,
        webinarEventId
      ).getStudioIdForWebinar();
      console.log(`Studio ID - ${studioId}`);

      let stageController = new StageController(
        orgApiRequestContext,
        webinarEventId,
        studioId
      );
      await stageController.disableRecordingForStudioAsBackstage();
    });

    webinarAttendeeLandingPage =
      webinarController.getWebinarInfoDto.webinarAttendeeLandingPage();

    //organiser invites speaker to the webinar
    let organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    let organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];

    organiserName = `${organiserFirstName} ${organiserLastName}`;
    speakerName = "automation parashar";
    speakerEmail = DataUtl.getRandomSpeakerEmail();
    await webinarController.updateMagicLinkStatusForWebinar({
      enableSpeakerMagicLink: true,
      enableAttendeeMagicLink: true,
    });
    await webinarController.disableOnboardingChecksForAttendee();
    await webinarController.addNewSpeakerToWebinar(speakerEmail, true);

    //organiser invites attendee to the webinar
    attendeeName = "prateek";
    attendeeEmail = DataUtl.getRandomAttendeeTestJoynEmail();
    await webinarController.addNewAttendeeToWebinar(attendeeEmail);

    //attendee browser
    attendeeBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      browserName: BrowserName.CHROME,
    });
    //speaker browser
    speakerBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
      browserName: BrowserName.CHROME,
    });

    // organiser and speaker page
    organiserSession = await orgBrowserContext.newPage();
    speakerSession = await speakerBrowserContext.newPage();
    attendeeSession = await attendeeBrowserContext.newPage();
  });

  test.afterEach(async () => {
    // close the studio session if already running
    await webinarController.endLiveSessionOnStudio();
    await cancelEvent(orgApiRequestContext, webinarEventId);
    await orgBrowserContext?.close();
    await speakerBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test(`TC001: Handling banners on webinar`, async () => {
    test.slow();
    let speakerMagicLink: string;
    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserSession
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerSession
    );
    let organiserStudioPage: StudioPage;
    let speakerStudioPage: StudioPage;
    let attendeeWebinarStage: WebinarStage;

    await test.step(`organiser joins the backstage with his AV ON`, async () => {
      await organiserSession.goto(webinarAttendeeLandingPage);
      await organiserSession.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({});
    });

    await test.step(`speaker logs in to the webinar with his AV off`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarEventId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerSession.goto(speakerMagicLink);
      await speakerSession.locator("text=Enter Now").click();
      speakerStudioPage = await speakerStudioJoinModal.enterInsideTheStudio({});
    });

    await test.step(`attendee logs in to the webinar`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarEventId,
        attendeeEmail
      );

      console.log(
        `fetched attendee magic link for webinar is ${attendeeMagicLink} `
      );

      await attendeeSession.goto(attendeeMagicLink);
      await attendeeSession.locator("text=Enter Now").click();
      attendeeWebinarStage = new WebinarStage(attendeeSession);
      await test.step(`attendee verifies that he sees the interaction panel on stage`, async () => {
        await attendeeWebinarStage.getInteractionPanel.isInteractionPanelOpenAndVisible();
      });
    });

    await test.step(`organiser adds a new banner and publish it to stage`, async () => {
      const organiserBannerSection =
        await organiserStudioPage.getRightSideOrganiserControlPanel.clickOnBannerMenuOption();
      await organiserBannerSection.addNewBanner(
        "Banner by automation",
        "webinar script"
      );
      await organiserBannerSection.showBannerOnStage("Banner by automation");
    });

    await test.step(`verify organiser is able to see banner on main stage`, async () => {
      await organiserStudioPage.getMainStageComponent.verifyBannerIsDisplayedOnMainStage(
        "Banner by automation"
      );
    });
    await test.step(`verify speaker is able to see banner on main stage`, async () => {
      await speakerStudioPage.getMainStageComponent.verifyBannerIsDisplayedOnMainStage(
        "Banner by automation"
      );
    });
    await test.step(`verify attendee is not able to see banner on main stage as session is not started`, async () => {
      await attendeeWebinarStage.verifyBannerIsNotDisplayedOnMainStage();
    });
    await test.step(`organiser now starts the session`, async () => {
      await organiserStudioPage.startTheSession();
    });
    await test.step(`verify org can still see banner after start of the session`, async () => {
      await organiserStudioPage.getMainStageComponent.verifyBannerIsDisplayedOnMainStage(
        "Banner by automation"
      );
    });
    await test.step(`verify speaker can still see banner after start of the session`, async () => {
      await speakerStudioPage.getMainStageComponent.verifyBannerIsDisplayedOnMainStage(
        "Banner by automation"
      );
    });
    await test.step(`verify attendee can now see banner after start of the session`, async () => {
      await attendeeWebinarStage.verifyBannerIsDisplayedOnMainStage(
        "Banner by automation"
      );
    });

    await test.step(`organiser ending the session`, async () => {
      await organiserStudioPage.getMainStageComponent.endSessionOnStage();
    });
    await test.step(`verify attendee is not able to see banner on main stage as session has ended`, async () => {
      await attendeeWebinarStage.verifyBannerIsNotDisplayedOnMainStage();
    });
  });
});
