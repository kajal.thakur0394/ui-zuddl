import {
  APIRequestContext,
  BrowserContext,
  test,
  Page,
  expect,
} from "@playwright/test";
import { WebinarController } from "../../../controller/WebinarController";
import { StudioJoiningAvModal } from "../../../page-objects/studio-pages/studioEntryAvModal";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { WebinarStage } from "../../../page-objects/webinar-pages/webinarStage";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { cancelEvent } from "../../../util/apiUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";
import { StageController } from "playwright-qa/controller/StageController";

test.describe(`@webinar @stage`, async () => {
  //browser contexts
  let orgBrowserContext: BrowserContext;
  let speakerBrowserContext: BrowserContext;
  let attendeeBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  // webinar details
  let webinarEventId;
  let webinarAttendeeLandingPage: string;
  let attendeeMagicLink: string;
  let organiserName: string;
  let speakerName: string;
  let speakerEmail: string;
  let attendeeEmail: string;
  let attendeeName: string;
  let studioId: string;

  // objects
  let webinarController: WebinarController;
  let organiserSession: Page;
  let speakerSession: Page;
  let attendeeSession: Page;

  test.beforeEach(async () => {
    //organiser browser
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiRequestContext = orgBrowserContext.request;

    // create new webinar
    await test.step("Create new Webinar.", async () => {
      webinarEventId = await new WebinarController(
        orgApiRequestContext
      ).createNewWebinar({
        title: "By automation",
      });
      console.log(`Webinar ID - ${webinarEventId}.`);
    });

    await test.step("Initialise Webinar Controller.", async () => {
      webinarController = new WebinarController(
        orgApiRequestContext,
        webinarEventId
      );
      await webinarController.getStudioIdForWebinar();
      studioId = await new WebinarController(
        orgApiRequestContext,
        webinarEventId
      ).getStudioIdForWebinar();
      console.log(`Studio ID - ${studioId}`);
      let stageController = new StageController(
        orgApiRequestContext,
        webinarEventId,
        studioId
      );
      await stageController.disableRecordingForStudioAsBackstage();
    });

    webinarAttendeeLandingPage =
      webinarController.getWebinarInfoDto.webinarAttendeeLandingPage();

    //organiser name
    let organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    let organiserLastName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserLastName"];
    organiserName = `${organiserFirstName} ${organiserLastName}`;

    //speaker name
    speakerName = "automation parashar";
    speakerEmail = DataUtl.getRandomSpeakerEmail();
    await webinarController.updateMagicLinkStatusForWebinar({
      enableSpeakerMagicLink: true,
      enableAttendeeMagicLink: true,
    });

    // organiser and speaker page
    organiserSession = await orgBrowserContext.newPage();
  });

  test.afterEach(async () => {
    // close the studio session if already running
    await webinarController.endLiveSessionOnStudio();
    await cancelEvent(orgApiRequestContext, webinarEventId);

    await orgBrowserContext?.close();
    await speakerBrowserContext?.close();
  });

  test(`@organiser-attendee-view TC001: verify org is able to see pdf in attendee view when session is running`, async () => {
    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserSession
    );
    let organiserPage2: Page;
    let organiserStudioPage: StudioPage;
    let organiserAttendeeWebinarView: WebinarStage;

    await webinarController.uploadPdfToWebinar();
    console.log(`webinar link ${webinarAttendeeLandingPage}`);

    await test.step(`organiser joins the backstage with his AV ON`, async () => {
      await organiserSession.goto(webinarAttendeeLandingPage);
      await organiserSession.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({});
    });

    await test.step(`organiser plays the pdf from content section to display on stage`, async () => {
      const organiserContentUploadPage =
        await organiserStudioPage.getRightSideOrganiserControlPanel.clickOnContentMenuOption();
      await organiserSession.waitForTimeout(3000);
      await organiserContentUploadPage.uploadPdfOnStage("sample");
      await organiserContentUploadPage.playPdfOnStage("sample");
    });

    await test.step(`verify organiser is able to see pdf being display on main stage`, async () => {
      await organiserStudioPage.getMainStageComponent.verifyIfPdfPresentationIsVisibleOnStage();
    });

    await test.step(`organiser starts the session`, async () => {
      await organiserStudioPage.startTheSession();
    });

    await test.step("verify organiser is able to see pdf displayed from inside of studio", async () => {
      await organiserStudioPage.getMainStageComponent.verifyIfPdfPresentationIsVisibleOnStage();
    });

    await test.step("Now organiser opens a new tab and go to webinar and choose attendee view", async () => {
      organiserPage2 = await orgBrowserContext.newPage();
      await organiserPage2.goto(webinarAttendeeLandingPage);
      await organiserPage2.locator("text=Enter Now").click();
      organiserAttendeeWebinarView = new WebinarStage(organiserPage2);
      await organiserAttendeeWebinarView.clickOnViewAsAttendeeButtonOnWelcomeStageModal();
    });

    await test.step(`verify organiser as attendee is able to view the presentation on stage`, async () => {
      await organiserAttendeeWebinarView.verifyPdfStreamIsVisibleForAttendee();
    });

    await test.step(`organiser changes the pdf to next slide`, async () => {
      await organiserStudioPage.getMainStageComponent.movePdfToNextPage();
    });

    await test.step(`verify organiser as attendee is still  able to see pdf being display on main stage`, async () => {
      await organiserAttendeeWebinarView.verifyPdfStreamIsVisibleForAttendee();
    });

    await test.step(`organiser now stops the pdf presentation`, async () => {
      const organiserContentUploadPage =
        await organiserStudioPage.getRightSideOrganiserControlPanel.clickOnContentMenuOption();
      await organiserSession.waitForTimeout(3000);
      await organiserContentUploadPage.stopPlayingPdfOnStage("sample");
    });

    await test.step(`verify organiser as attendee is now not able to see pdf being display on main stage`, async () => {
      await organiserAttendeeWebinarView.verifyPdfStreamIsNotVisibleForAttendee();
    });
  });

  test(`@organiser-attendee-view TC002 : verify org is able to see the speaker stream on stage in attendee view`, async () => {
    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserSession
    );
    let speakerMagicLink: string;
    let organiserPage2: Page;
    let organiserStudioPage: StudioPage;
    let organiserAttendeeWebinarView: WebinarStage;
    let speakerStudioPage: StudioPage;
    let speakerStudioJoinModal: StudioJoiningAvModal;
    await test.step(`invite new speaker to this webinar`, async () => {
      speakerName = "automation parashar";
      speakerEmail = DataUtl.getRandomSpeakerEmail();
      await webinarController.updateMagicLinkStatusForWebinar({
        enableSpeakerMagicLink: true,
        enableAttendeeMagicLink: true,
      });
      await webinarController.disableOnboardingChecksForAttendee();
      await webinarController.addNewSpeakerToWebinar(speakerEmail, true);
    });

    await test.step(`spin up a new speaker context and speakers joins webinar through it`, async () => {
      //speaker browser
      speakerBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      speakerSession = await speakerBrowserContext.newPage();
      await test.step(`speaker logs in to the webinar with his AV off`, async () => {
        speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          webinarEventId,
          speakerEmail
        );

        console.log(
          `fetched speaker magic link for webinar is ${speakerMagicLink} `
        );

        await speakerSession.goto(speakerMagicLink);
        await speakerSession.locator("text=Enter Now").click();
        speakerStudioJoinModal = new StudioJoiningAvModal(speakerSession);
        speakerStudioPage = await speakerStudioJoinModal.enterInsideTheStudio(
          {}
        );
      });
    });

    await test.step(`organiser joins the backstage with his AV ON`, async () => {
      await organiserSession.goto(webinarAttendeeLandingPage);
      await organiserSession.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({});
    });

    await test.step(`organiser verifies his AV stream on backstage container`, async () => {
      await organiserStudioPage.getBackStageComponent.organiserVerifiesPresenceOfSpeakerStreamWithName(
        organiserName
      );
    });

    await test.step(`organiser verifies presence of speaker AV stream on backstage container`, async () => {
      await organiserStudioPage.getBackStageComponent.organiserVerifiesPresenceOfSpeakerStreamWithName(
        speakerName
      );
    });

    await test.step(`organiser moves speaker to on stage`, async () => {
      await organiserStudioPage.getBackStageComponent.addMediaStreamToStageFromBackstage(
        speakerName
      );
    });

    await test.step(`organiser verifies presence of  speaker stream on main stage from inside of studio`, async () => {
      await expect(
        organiserStudioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
          speakerName
        )
      ).toBeVisible();
    });

    await test.step(`organiser starts the session`, async () => {
      await organiserStudioPage.startTheSession();
    });

    await test.step(`organiser verifies presence of  speaker stream on main stage from inside of studio after starting the session`, async () => {
      await expect(
        organiserStudioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
          speakerName
        )
      ).toBeVisible();
    });

    await test.step(`speaker verifies presence of own on main stage after starting the session`, async () => {
      await expect(
        speakerStudioPage.getMainStageComponent.getSpeakerStreamContainerFromMainStage(
          speakerName
        )
      ).toBeVisible();
    });

    await test.step("Now organiser opens a new tab and go to webinar and choose attendee view", async () => {
      organiserPage2 = await orgBrowserContext.newPage();
      await organiserPage2.goto(webinarAttendeeLandingPage);
      await organiserPage2.locator("text=Enter Now").click();
      organiserAttendeeWebinarView = new WebinarStage(organiserPage2);
      await organiserAttendeeWebinarView.clickOnViewAsAttendeeButtonOnWelcomeStageModal();
    });

    await test.step(`organiser in attendee view verifies presence of  speaker stream on main stage from inside of studio`, async () => {
      await organiserAttendeeWebinarView.verifySpeakerStreamWithNameIsVisibleForAttendee(
        speakerName
      );
    });

    await test.step(`organiser stops the session`, async () => {
      await organiserStudioPage.getMainStageComponent.endSessionOnStage();
    });

    await test.step(`organiser in attendee view should not able to see speaker on stage when session has ended`, async () => {
      await organiserAttendeeWebinarView.verifySpeakerStreamWithNameIsNotVisibleForAttendee(
        speakerName
      );
    });
  });
});
