import {
  APIRequestContext,
  BrowserContext,
  test,
  expect,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { WebinarController } from "../../../controller/WebinarController";
import { inviteAttendeeByAPI } from "../../../util/apiUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { DataUtl } from "../../../util/dataUtil";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe.parallel(`@sanity @webinar @stage`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  let webinarEventId;
  let webinarAttendeeLandingPage: string;
  let attendeeMagicLink: string;
  let webinarController: WebinarController;
  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiRequestContext = orgBrowserContext.request;
    await test.step("Create new Webinar.", async () => {
      webinarEventId = await new WebinarController(
        orgApiRequestContext
      ).createNewWebinar({
        title: "By automation",
      });
      console.log(`Webinar ID - ${webinarEventId}.`);
    });

    await test.step("Initialise Webinar Controller.", async () => {
      webinarController = new WebinarController(
        orgApiRequestContext,
        webinarEventId
      );
    });
    webinarAttendeeLandingPage =
      webinarController.getWebinarInfoDto.webinarAttendeeLandingPage();
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`@attendee TC001: Attendee lands on webinar stage by using magic link`, async ({
    page,
  }) => {
    // // invite attendee to webinar
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await inviteAttendeeByAPI(
      orgApiRequestContext,
      webinarEventId,
      attendeeEmail
    );
    /**
     * disabling onboarding checks for attendee since it consumes time for attendee in test
     * Note: the api we are using here are not exposed on the UI, instead
     * I am using event side access group api for attendee to disable onboarding check by event controller
     */
    await new EventController(
      orgApiRequestContext,
      webinarEventId
    ).getAccessGroupController.handleOnboardingChecksForAttendee(false);
    await test.step(`Attendee ${attendeeEmail} lands on webinar by using magic link ${attendeeMagicLink} and enter inside should land on stage`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarEventId,
        attendeeEmail
      );
      await page.goto(attendeeMagicLink);
      await page.click("text=Enter Now");
    });

    await test.step(`Verify the presence of interaction panel`, async () => {});

    await test.step(`verify the interaction panel is open by default`, async () => {});

    await test.step(`verify the presence of stage tab and backstage tab on interaction panel`, async () => {});

    await test.step(`verify attendee is able to send a chat message and its visible to him in the chat container`, async () => {});
  });

  test(`@organiser TC002:  Loggedin organiser visits events link should be able to enter inside webinar`, async () => {
    let organiserSession = await orgBrowserContext.newPage();
    await test.step(`Organiser visits ${webinarAttendeeLandingPage} should be able to enter inside the webinar`, async () => {
      await organiserSession.goto(webinarAttendeeLandingPage);
      await organiserSession.click("text=Enter Now");
      await organiserSession.click("button[data-testid='go_backstage']");
      await expect(
        organiserSession.locator(
          "div[class^='video-preview_userVideoContainer'] div#videoId"
        )
      ).toBeVisible();
      await organiserSession.fill("#joining-modal-title", "QA Automation");
      await organiserSession.click("text=Enter studio");
      await expect(
        organiserSession.locator("div[class^='studio-screen_container']")
      ).toBeVisible();
    });

    await test.step(`verify organiser is able to see webinar controls`, async () => {});
  });

  test(`@speaker TC003: Logged in speaker visits events link should be able to enter inside webinar`, async () => {
    let speakerBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    let speakerSession = await speakerBrowserContext.newPage();
    await webinarController.updateMagicLinkStatusForWebinar({
      enableSpeakerMagicLink: true,
      enableAttendeeMagicLink: true,
    });
    const speakerEmail = DataUtl.getRandomSpeakerEmail();
    let speakerMagicLink: string;
    await test.step(`Invite speaker ${speakerEmail}to webinar`, async () => {
      await webinarController.addNewSpeakerToWebinar(speakerEmail, true);
    });
    await test.step(`Fetch magic link for the speaker and poll the magic link`, async () => {
      /**
       * magic link takes some time to work on webinar, hence I am polling the page until enter now button is visible
       */
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarEventId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerSession.goto(speakerMagicLink);
      await speakerSession.locator("text=Enter Now").click();
      await speakerSession.click("text=Enter studio");
      await speakerSession.locator("text=Backstage").click();
    });
  });
});
