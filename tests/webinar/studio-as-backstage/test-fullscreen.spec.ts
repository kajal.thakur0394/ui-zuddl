import test, { APIRequestContext, BrowserContext } from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import BrowserName from "../../../enums/BrowserEnum";
import BrowserFactory from "../../../util/BrowserFactory";
import { WebinarController } from "../../../controller/WebinarController";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { WebinarStage } from "../../../page-objects/webinar-pages/webinarStage";
import { StudioJoiningAvModal } from "../../../page-objects/studio-pages/studioEntryAvModal";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";
import { StudioController } from "playwright-qa/controller/StudioController";
import { StageController } from "playwright-qa/controller/StageController";

test.describe.parallel(`@webinar @studio`, async () => {
  let webinarId;
  let studioId;
  let organiserPage;
  let speakerPage;
  let attendeePage;
  let landingPage: string;
  let speakerEmail: string;
  let attendeeEmail: string;
  let attendeeMagicLink: string;
  let speakerMagicLink: string;
  let organiserBackstage: StudioPage;

  let orgApiContext: APIRequestContext;
  let orgBrowserContext: BrowserContext;
  let speakerBrowserContext: BrowserContext;
  let attendeeBrowserContext: BrowserContext;
  let webinarController: WebinarController;

  test.beforeEach(async () => {
    await test.step("Initialise Organiser browser context.", async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: true,
      });
    });

    await test.step("Initialise Organiser API request context.", async () => {
      orgApiContext = orgBrowserContext.request;
    });

    await test.step("Initialise Speaker browser context.", async () => {
      speakerBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });
    });

    await test.step("Initialise Attendee browser context.", async () => {
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });
    });

    await test.step("Create new Webinar.", async () => {
      webinarId = await new WebinarController(orgApiContext).createNewWebinar({
        title: "By automation",
      });
      console.log(`Webinar ID - ${webinarId}.`);

      studioId = await new WebinarController(
        orgApiContext,
        webinarId
      ).getStudioIdForWebinar();
      console.log(`Studio ID - ${studioId}`);

      let stageController = new StageController(
        orgApiContext,
        webinarId,
        studioId
      );
      await stageController.disableRecordingForStudioAsBackstage();
    });

    await test.step("Initialise Webinar Controller.", async () => {
      webinarController = new WebinarController(
        orgApiContext,
        webinarId,
        studioId
      );
    });

    await test.step("Fetching attendee landing page.", async () => {
      landingPage =
        webinarController.getWebinarInfoDto.webinarAttendeeLandingPage();
    });

    await test.step("Initilise Speaker name and get random Speaker email.", async () => {
      speakerEmail = DataUtl.getRandomSpeakerEmail();
    });

    await test.step("Enable Magic link for Speaker and Attendee.", async () => {
      await webinarController.updateMagicLinkStatusForWebinar({
        enableSpeakerMagicLink: true,
        enableAttendeeMagicLink: true,
      });
    });

    await test.step("Disable onboarding checks for Attendee.", async () => {
      await webinarController.disableOnboardingChecksForAttendee();
    });

    await test.step("Initilise Attendee name and get random Attendee email.", async () => {
      attendeeEmail = DataUtl.getRandomAttendeeTestJoynEmail();
    });

    await test.step("Add new Speaker and Attendee to Webinar.", async () => {
      await webinarController.addNewSpeakerToWebinar(speakerEmail, true);
      await webinarController.addNewAttendeeToWebinar(attendeeEmail);
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step("Speaker browser context.", async () => {
      speakerPage = await speakerBrowserContext.newPage();
    });

    await test.step("Attendees browser context.", async () => {
      attendeePage = await attendeeBrowserContext.newPage();
    });
  });

  test.afterEach(async () => {
    await webinarController.endLiveSessionOnStudio();
    await webinarController.stopVideoPlayBackOnStudio();
    await orgBrowserContext?.close();
    await speakerBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test("TC001: Testing full screen view of main stage container.", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-470/test-fullscreen-functionality",
    });

    let organiserStudioPage: StudioPage;
    let attendeeWebinarStage: WebinarStage;

    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserPage
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerPage
    );

    await test.step(`Organiser joins the backstage with AV on.`, async () => {
      await organiserPage.goto(landingPage);
      await organiserPage.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({ muteAudio: false, muteVideo: false });
    });

    await test.step(`Speaker logs in to the webinar mid-sesison with AV on.`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerPage.goto(speakerMagicLink);
      await speakerPage.locator("text=Enter Now").click();
      await speakerStudioJoinModal.enterInsideTheStudio({
        muteAudio: false,
        muteVideo: false,
      });
    });

    await test.step(`Verify Organiser's and Speaker's stream in backstage.`, async () => {
      organiserBackstage = new StudioPage(organiserPage);
      await organiserBackstage.isBackstageVisible();
      await organiserBackstage.verifyCountOfStreamInBackstage(2);
    });

    await test.step(`Attendee logs in to the webinar.`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarId,
        attendeeEmail
      );

      console.log(
        `fetched attendee magic link for webinar is ${attendeeMagicLink} `
      );

      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.locator("text=Enter Now").click();
      attendeeWebinarStage = new WebinarStage(attendeePage);
      await test.step(`attendee verifies that he sees the interaction panel on stage`, async () => {
        await attendeeWebinarStage.getInteractionPanel.isInteractionPanelOpenAndVisible();
      });
    });

    await test.step(`Organiser starts the session`, async () => {
      await organiserStudioPage.startTheSession();
    });

    await test.step(`Organsier change to fullscreen view.`, async () => {
      await organiserBackstage.getStudioSceneComponent.changeToFullscreenView();
    });

    await test.step(`Verify the fullscreen view.`, async () => {
      await organiserBackstage.getStudioSceneComponent.verifyFullscreenView();
    });

    await test.step(`End the session.`, async () => {
      let studioController = new StudioController(orgApiContext, studioId);
      await studioController.stopSessionOnStudio();
    });
  });
});
