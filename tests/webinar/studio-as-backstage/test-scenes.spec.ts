import test, {
  APIRequestContext,
  BrowserContext,
  expect,
} from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import BrowserName from "../../../enums/BrowserEnum";
import BrowserFactory from "../../../util/BrowserFactory";
import { StudioController } from "../../../controller/StudioController";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { WebinarStage } from "../../../page-objects/webinar-pages/webinarStage";
import { WebinarController } from "../../../controller/WebinarController";
import { StudioJoiningAvModal } from "../../../page-objects/studio-pages/studioEntryAvModal";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";
import { StageController } from "playwright-qa/controller/StageController";

test.describe.parallel(`@webinar @scenes`, async () => {
  test.setTimeout(4 * 60 * 1000);
  let webinarId;
  let studioId;
  let organiserPage;
  let speakerPage;
  let attendeePage;
  let landingPage: string;
  let organiserName: string;
  let speakerName: string;
  let speakerEmail: string;
  let attendeeEmail: string;
  let attendeeMagicLink: string;
  let speakerMagicLink: string;
  let organiserBackstage: StudioPage;
  let speakerBackstage: StudioPage;

  let orgApiContext: APIRequestContext;
  let orgBrowserContext: BrowserContext;
  let speakerBrowserContext: BrowserContext;
  let attendeeBrowserContext: BrowserContext;
  let webinarController: WebinarController;
  let studioController: StudioController;

  test.beforeEach(async () => {
    await test.step("Initialise Organiser browser context.", async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: true,
      });
    });

    await test.step("Initialise Organiser API request context.", async () => {
      orgApiContext = orgBrowserContext.request;
    });

    await test.step("Initialise Speaker browser context.", async () => {
      speakerBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });
    });

    await test.step("Initialise Attendee browser context.", async () => {
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });
    });

    await test.step("Create new Webinar.", async () => {
      webinarId = await new WebinarController(orgApiContext).createNewWebinar({
        title: "By automation",
      });
      console.log(`Webinar ID - ${webinarId}.`);

      studioId = await new WebinarController(
        orgApiContext,
        webinarId
      ).getStudioIdForWebinar();
      console.log(`Studio ID - ${studioId}`);
    });

    await test.step("Initialise Webinar Controller.", async () => {
      webinarController = new WebinarController(
        orgApiContext,
        webinarId,
        studioId
      );
    });

    await test.step("Initialise Studio Controller.", async () => {
      studioController = new StudioController(orgApiContext, studioId);
    });

    await test.step("Fetching attendee landing page.", async () => {
      landingPage =
        webinarController.getWebinarInfoDto.webinarAttendeeLandingPage();
    });

    await test.step("Initilise Speaker and Organiser name and get random Speaker email.", async () => {
      speakerName = "automation parashar";
      organiserName =
        DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
      speakerEmail = DataUtl.getRandomSpeakerEmail();
    });

    await test.step("Enable Magic link for Speaker and Attendee.", async () => {
      await webinarController.updateMagicLinkStatusForWebinar({
        enableSpeakerMagicLink: true,
        enableAttendeeMagicLink: true,
      });
    });

    await test.step("Disable onboarding checks for Attendee.", async () => {
      await webinarController.disableOnboardingChecksForAttendee();
    });

    await test.step("Initilise Attendee name and get random Attendee email.", async () => {
      attendeeEmail = DataUtl.getRandomAttendeeTestJoynEmail();
    });

    await test.step("Add new Speaker and Attendee to Webinar.", async () => {
      await webinarController.addNewSpeakerToWebinar(speakerEmail, true);
      await webinarController.addNewAttendeeToWebinar(attendeeEmail);
    });
    let stageController = new StageController(
      orgApiContext,
      webinarId,
      studioId
    );
    await stageController.disableRecordingForStudioAsBackstage();
  });

  test.afterEach(async () => {
    await webinarController.endLiveSessionOnStudio();
    await webinarController.stopVideoPlayBackOnStudio();
    await orgBrowserContext?.close();
    await speakerBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test("TC001: Verify flow of saving a scene with multiple user stream and then playing it on stage.", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-476/verify-flow-of-saving-a-scene-with-multiple-user-stream-and-then",
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step("Speaker browser context.", async () => {
      speakerPage = await speakerBrowserContext.newPage();
    });

    await test.step("Attendees browser context.", async () => {
      attendeePage = await attendeeBrowserContext.newPage();
    });

    let organiserStudioPage: StudioPage;
    let attendeeWebinarStage: WebinarStage;

    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserPage
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerPage
    );

    await test.step(`Organiser joins the backstage with AV on.`, async () => {
      await organiserPage.goto(landingPage);
      await organiserPage.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({ muteAudio: false, muteVideo: false });
    });

    await test.step(`Speaker logs in to the webinar with AV on.`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerPage.goto(speakerMagicLink);
      await speakerPage.locator("text=Enter Now").click();
      await speakerStudioJoinModal.enterInsideTheStudio({
        muteAudio: false,
        muteVideo: false,
      });
    });

    await test.step(`Verify Organiser and Speaker stream in backstage.`, async () => {
      organiserBackstage = new StudioPage(organiserPage, studioController);
      await organiserBackstage.isBackstageVisible();

      speakerBackstage = new StudioPage(speakerPage);
      // await speakerBackstage.closeStudioWalkthrough();

      await test.step(`Verify Organiser view.`, async () => {
        await organiserBackstage.verifyCountOfStreamInBackstage(2);
      });
      await test.step(`Verify Speaker view.`, async () => {
        await speakerBackstage.verifyCountOfStreamInBackstage(2);
      });
    });

    await test.step(`Attendee logs in to the webinar.`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarId,
        attendeeEmail
      );

      console.log(
        `fetched attendee magic link for webinar is ${attendeeMagicLink} `
      );

      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.locator("text=Enter Now").click();
      attendeeWebinarStage = new WebinarStage(attendeePage);
      await test.step(`attendee verifies that he sees the interaction panel on stage`, async () => {
        await attendeeWebinarStage.getInteractionPanel.isInteractionPanelOpenAndVisible();
      });
    });

    await test.step(`Organiser starts the session`, async () => {
      await organiserStudioPage.startTheSession();
    });

    await test.step(`Change the brand color, theme, logo, background and overlay.`, async () => {
      await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
        1
      );
    });

    await test.step(`Organiser joins the main stage.`, async () => {
      await organiserStudioPage.addUserToStageFromBackStage(
        organiserName,
        true
      );
    });

    await test.step(`Organizer sends Speaker on stage.`, async () => {
      await organiserBackstage.addUserToStageFromBackStage(speakerName);
    });

    await test.step(`Verify Attendee's view || Organiser and Speaker stream.`, async () => {
      await attendeeWebinarStage.verifyNumberOfVisibileStreams(2);
    });

    await test.step(`Save a scene and capture the details.`, async () => {
      await organiserBackstage.getStudioSceneComponent.saveCurrentScene(
        studioId
      );
      await organiserBackstage.getStudioSceneComponent.captureExpectedData();
    });

    await test.step(`Change theme, logo, background and overlay.`, async () => {
      await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
        2
      );
      await organiserBackstage.verifyCountOfStreamInBackstage(2);
    });

    await test.step(`Play the scene.`, async () => {
      await organiserBackstage.getStudioSceneComponent.playScene();
      await organiserBackstage.verifyCountOfStreamInStage(2);
    });

    await test.step(`Validate the applied scene.`, async () => {
      await organiserBackstage.getStudioSceneComponent.validateTheScene();
    });

    await test.step(`End the session.`, async () => {
      await organiserBackstage.endTheSession();
    });
  });

  test("TC002: Verify flow of updating existing scene with new one and then verify if played updated content are visible on stage.", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-477/verify-flow-of-updating-existing-scene-with-new-one-and-then-verify-if",
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step("Speaker browser context.", async () => {
      speakerPage = await speakerBrowserContext.newPage();
    });

    await test.step("Attendees browser context.", async () => {
      attendeePage = await attendeeBrowserContext.newPage();
    });

    let organiserStudioPage: StudioPage;
    let attendeeWebinarStage: WebinarStage;

    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserPage
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerPage
    );

    await test.step(`Organiser joins the backstage with AV on.`, async () => {
      await organiserPage.goto(landingPage);
      await organiserPage.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({ muteAudio: false, muteVideo: false });
    });

    await test.step(`Speaker logs in to the webinar with AV on.`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerPage.goto(speakerMagicLink);
      await speakerPage.locator("text=Enter Now").click();
      await speakerStudioJoinModal.enterInsideTheStudio({
        muteAudio: false,
        muteVideo: false,
      });
    });

    await test.step(`Verify Organiser and Speaker stream in backstage.`, async () => {
      organiserBackstage = new StudioPage(organiserPage, studioController);
      await organiserBackstage.isBackstageVisible();

      speakerBackstage = new StudioPage(speakerPage);
      // speakerBackstage.closeStudioWalkthrough();

      await test.step(`Verify Organiser view.`, async () => {
        await organiserBackstage.verifyCountOfStreamInBackstage(2);
      });
      await test.step(`Verify Speaker view.`, async () => {
        await speakerBackstage.verifyCountOfStreamInBackstage(2);
      });
    });

    await test.step(`Attendee logs in to the webinar.`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarId,
        attendeeEmail
      );

      console.log(
        `fetched attendee magic link for webinar is ${attendeeMagicLink} `
      );

      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.locator("text=Enter Now").click();
      attendeeWebinarStage = new WebinarStage(attendeePage);
      await test.step(`attendee verifies that he sees the interaction panel on stage`, async () => {
        await attendeeWebinarStage.getInteractionPanel.isInteractionPanelOpenAndVisible();
      });
    });

    await test.step(`Organiser starts the session`, async () => {
      await organiserStudioPage.startTheSession();
    });

    await test.step(`Change the brand color, theme, logo, background and overlay.`, async () => {
      await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
        1
      );
    });

    await test.step(`Organiser joins the main stage.`, async () => {
      await organiserStudioPage.addUserToStageFromBackStage(
        organiserName,
        true
      );
    });

    await test.step(`Organizer sends Speaker on stage.`, async () => {
      await organiserBackstage.addUserToStageFromBackStage(speakerName);
    });

    await test.step(`Verify Attendee's view || Organiser and Speaker stream.`, async () => {
      await attendeeWebinarStage.verifyNumberOfVisibileStreams(2);
    });

    await test.step(`Save a scene.`, async () => {
      await organiserBackstage.getStudioSceneComponent.saveCurrentScene(
        studioId
      );
    });

    await test.step(`Change the configuration.`, async () => {
      await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
        2
      );
      await organiserBackstage.verifyCountOfStreamInBackstage(2);
    });

    await test.step(`Organizer sends Speaker on stage.`, async () => {
      await organiserBackstage.addUserToStageFromBackStage(speakerName);
      await organiserBackstage.verifyCountOfStreamInStage(1);
    });

    await test.step(`Update the scene and capture the details.`, async () => {
      await organiserBackstage.getStudioSceneComponent.updateScene();
      await organiserBackstage.getStudioSceneComponent.captureExpectedData();
    });

    await test.step(`Change the configuration.`, async () => {
      await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
        3
      );
      await organiserBackstage.verifyCountOfStreamInBackstage(2);
    });

    await test.step(`Play the scene.`, async () => {
      await organiserBackstage.getStudioSceneComponent.playScene();
      await organiserBackstage.verifyCountOfStreamInStage(1);
    });

    await test.step(`Validate the applied scene.`, async () => {
      await organiserBackstage.getStudioSceneComponent.validateTheScene();
    });

    await test.step(`End the session.`, async () => {
      await organiserBackstage.endTheSession();
    });
  });

  test("TC003: Verify organiser is able to delete the scenes.", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-478/verify-organiser-is-able-to-delete-the-scenes",
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step("Speaker browser context.", async () => {
      speakerPage = await speakerBrowserContext.newPage();
    });

    await test.step("Attendees browser context.", async () => {
      attendeePage = await attendeeBrowserContext.newPage();
    });

    let organiserStudioPage: StudioPage;
    let attendeeWebinarStage: WebinarStage;

    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserPage
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerPage
    );

    await test.step(`Organiser joins the backstage with AV on.`, async () => {
      await organiserPage.goto(landingPage);
      await organiserPage.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({ muteAudio: false, muteVideo: false });
    });

    await test.step(`Speaker logs in to the webinar with AV on.`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerPage.goto(speakerMagicLink);
      await speakerPage.locator("text=Enter Now").click();
      await speakerStudioJoinModal.enterInsideTheStudio({
        muteAudio: false,
        muteVideo: false,
      });
    });

    await test.step(`Verify Organiser and Speaker stream in backstage.`, async () => {
      organiserBackstage = new StudioPage(organiserPage, studioController);
      await organiserBackstage.isBackstageVisible();

      speakerBackstage = new StudioPage(speakerPage);
      // speakerBackstage.closeStudioWalkthrough();

      await test.step(`Verify Organiser view.`, async () => {
        await organiserBackstage.verifyCountOfStreamInBackstage(2);
      });
      await test.step(`Verify Speaker view.`, async () => {
        await speakerBackstage.verifyCountOfStreamInBackstage(2);
      });
    });

    await test.step(`Attendee logs in to the webinar.`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarId,
        attendeeEmail
      );

      console.log(
        `fetched attendee magic link for webinar is ${attendeeMagicLink} `
      );

      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.locator("text=Enter Now").click();
      attendeeWebinarStage = new WebinarStage(attendeePage);
      await test.step(`attendee verifies that he sees the interaction panel on stage`, async () => {
        await attendeeWebinarStage.getInteractionPanel.isInteractionPanelOpenAndVisible();
      });
    });

    await test.step(`Organiser starts the session`, async () => {
      await organiserStudioPage.startTheSession();
    });

    await test.step(`Change the brand color, theme, logo, background and overlay.`, async () => {
      await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
        1
      );
    });

    await test.step(`Organiser joins the main stage.`, async () => {
      await organiserStudioPage.addUserToStageFromBackStage(
        organiserName,
        true
      );
    });

    await test.step(`Organizer sends Speaker on stage.`, async () => {
      await organiserBackstage.addUserToStageFromBackStage(speakerName);
    });

    await test.step(`Verify Attendee's view || Organiser and Speaker stream.`, async () => {
      await attendeeWebinarStage.verifyNumberOfVisibileStreams(2);
    });

    await test.step(`Save a scene and Capture the details.`, async () => {
      await organiserBackstage.getStudioSceneComponent.saveCurrentScene(
        studioId
      );
      await organiserBackstage.getStudioSceneComponent.captureExpectedData();
    });

    await test.step(`Change theme, logo, background and overlay.`, async () => {
      await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
        2
      );
      await organiserBackstage.verifyCountOfStreamInBackstage(2);
    });

    await test.step(`Play the scene.`, async () => {
      await organiserBackstage.getStudioSceneComponent.playScene();
      await organiserBackstage.verifyCountOfStreamInStage(2);
    });

    await test.step(`Validate the applied scene.`, async () => {
      await organiserBackstage.getStudioSceneComponent.validateTheScene();
    });

    await test.step(`Delete the scene and verify the scene is deleted.`, async () => {
      await organiserBackstage.getStudioSceneComponent.deleteScene();
      await organiserBackstage.getStudioSceneComponent.verifySceneIsAbsent();
    });

    await test.step(`End the session.`, async () => {
      await organiserBackstage.endTheSession();
    });
  });

  test("TC004: Verify saving scene with placeholder of speakers.", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-479/verify-saving-scene-with-placeholder-of-speakers",
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step("Speaker browser context.", async () => {
      speakerPage = await speakerBrowserContext.newPage();
    });

    await test.step("Attendees browser context.", async () => {
      attendeePage = await attendeeBrowserContext.newPage();
    });

    let organiserStudioPage: StudioPage;
    let attendeeWebinarStage: WebinarStage;

    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserPage
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerPage
    );

    await test.step(`Organiser joins the backstage with AV on.`, async () => {
      await organiserPage.goto(landingPage);
      await organiserPage.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({ muteAudio: false, muteVideo: false });
    });

    await test.step(`Verify Organiser stream in backstage.`, async () => {
      organiserBackstage = new StudioPage(organiserPage, studioController);
      await organiserBackstage.isBackstageVisible();
      await organiserBackstage.verifyCountOfStreamInBackstage(1);
    });

    await test.step(`Change the brand color, theme, logo, background and overlay.`, async () => {
      await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
        1
      );
    });

    await test.step(`Add the Speaker Placeholders.`, async () => {
      await organiserBackstage.getStudioSceneComponent.addSpeakerPlaceholders([
        speakerName,
      ]);
    });

    await test.step(`Verify Placeholder count.`, async () => {
      await organiserBackstage.getStudioSceneComponent.verifyCountOfPlaceholderInStage(
        1
      );
    });

    await test.step(`Organiser joins the main stage.`, async () => {
      await organiserStudioPage.addUserToStageFromBackStage(
        organiserName,
        true
      );
    });

    await test.step(`Save a scene and capture the details.`, async () => {
      await organiserBackstage.getStudioSceneComponent.saveCurrentScene(
        studioId
      );
      await organiserBackstage.getStudioSceneComponent.captureExpectedData();
    });

    await test.step(`Attendee logs in to the webinar.`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarId,
        attendeeEmail
      );

      console.log(
        `fetched attendee magic link for webinar is ${attendeeMagicLink} `
      );

      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.locator("text=Enter Now").click();
      attendeeWebinarStage = new WebinarStage(attendeePage);
      await test.step(`attendee verifies that he sees the interaction panel on stage`, async () => {
        await attendeeWebinarStage.getInteractionPanel.isInteractionPanelOpenAndVisible();
      });
    });

    await test.step(`Speaker logs in to the webinar with AV on.`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerPage.goto(speakerMagicLink);
      await speakerPage.locator("text=Enter Now").click();
      await speakerStudioJoinModal.enterInsideTheStudio({
        muteAudio: false,
        muteVideo: false,
      });
    });

    await test.step(`Verify Speaker stream in backstage.`, async () => {
      speakerBackstage = new StudioPage(speakerPage);
      // speakerBackstage.closeStudioWalkthrough();

      await speakerBackstage.verifyCountOfStreamInBackstage(1);
    });

    await test.step(`Organiser starts the session`, async () => {
      await organiserStudioPage.startTheSession();
    });

    await test.step(`Verify Attendee's view || Organiser stream.`, async () => {
      await attendeeWebinarStage.verifyNumberOfVisibileStreams(1);
    });

    await test.step(`Change theme, logo, background and overlay.`, async () => {
      await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
        2
      );
      await organiserBackstage.verifyCountOfStreamInBackstage(2);
    });

    await test.step(`Verify Attendee's view || No stream.`, async () => {
      await attendeeWebinarStage.verifyNumberOfVisibileStreams(0);
    });

    await test.step(`Play the scene.`, async () => {
      await organiserBackstage.getStudioSceneComponent.playScene();
      await organiserBackstage.verifyCountOfStreamInStage(2);
    });

    await test.step(`Verify Attendee view || Organiser and Speaker streams.`, async () => {
      await attendeeWebinarStage.verifyNumberOfVisibileStreams(2);
    });

    await test.step(`Validate the applied scene.`, async () => {
      await organiserBackstage.getStudioSceneComponent.validateTheScene();
    });

    await test.step(`End the session.`, async () => {
      await organiserBackstage.endTheSession();
    });
  });

  test("TC005: Verify impact on scene if user stream is no more present on the backstage.", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-480/verify-impact-on-scene-if-user-stream-is-no-more-present-on-the",
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step("Attendees browser context.", async () => {
      attendeePage = await attendeeBrowserContext.newPage();
    });

    let organiserStudioPage: StudioPage;
    let attendeeWebinarStage: WebinarStage;

    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserPage
    );

    await test.step(`Organiser joins the backstage with AV on.`, async () => {
      await organiserPage.goto(landingPage);
      await organiserPage.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({ muteAudio: false, muteVideo: false });
    });

    await test.step(`Verify Organiser stream in backstage.`, async () => {
      organiserBackstage = new StudioPage(organiserPage, studioController);
      await organiserBackstage.isBackstageVisible();
      await organiserBackstage.verifyCountOfStreamInBackstage(1);
    });

    await test.step(`Change the brand color, theme, logo, background and overlay.`, async () => {
      await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
        1
      );
    });

    await test.step(`Add the Speaker Placeholders.`, async () => {
      await organiserBackstage.getStudioSceneComponent.addSpeakerPlaceholders([
        speakerName,
      ]);
    });

    await test.step(`Verify Placeholder count.`, async () => {
      await organiserBackstage.getStudioSceneComponent.verifyCountOfPlaceholderInStage(
        1
      );
    });

    await test.step(`Organiser joins the main stage.`, async () => {
      await organiserStudioPage.addUserToStageFromBackStage(
        organiserName,
        true
      );
    });

    await test.step(`Save a scene and capture the details.`, async () => {
      await organiserBackstage.getStudioSceneComponent.saveCurrentScene(
        studioId
      );
      await organiserBackstage.getStudioSceneComponent.captureExpectedData();
    });

    await test.step(`Attendee logs in to the webinar.`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarId,
        attendeeEmail
      );

      console.log(
        `fetched attendee magic link for webinar is ${attendeeMagicLink} `
      );

      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.locator("text=Enter Now").click();
      attendeeWebinarStage = new WebinarStage(attendeePage);
      await test.step(`attendee verifies that he sees the interaction panel on stage`, async () => {
        await attendeeWebinarStage.getInteractionPanel.isInteractionPanelOpenAndVisible();
      });
    });

    await test.step(`Organiser starts the session`, async () => {
      await organiserStudioPage.startTheSession();
    });

    await test.step(`Verify Attendee's view || Organiser stream.`, async () => {
      await attendeeWebinarStage.verifyNumberOfVisibileStreams(1);
    });

    await test.step(`Change theme, logo, background and overlay.`, async () => {
      await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
        2
      );
      await organiserBackstage.verifyCountOfStreamInBackstage(1);
    });

    await test.step(`Verify Attendee's view || No stream.`, async () => {
      await attendeeWebinarStage.verifyNumberOfVisibileStreams(0);
    });

    await test.step(`Play the scene.`, async () => {
      await organiserBackstage.getStudioSceneComponent.playScene();
      await organiserBackstage.verifyCountOfStreamInStage(1);
    });

    await test.step(`Verify Attendee view || Organiser stream.`, async () => {
      await attendeeWebinarStage.verifyNumberOfVisibileStreams(1);
    });

    await test.step(`Validate the applied scene.`, async () => {
      await organiserBackstage.getStudioSceneComponent.validateTheScene();
    });

    await test.step(`End the session.`, async () => {
      await organiserBackstage.endTheSession();
    });
  });

  test("TC006: Verify AV stream state had no impact on saving/playing scene. || AV On when scene is saved, AV Off when scene is played.", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-481/verify-av-stream-state-had-no-impact-on-savingplaying-scene-oror-av-on",
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step("Speaker browser context.", async () => {
      speakerPage = await speakerBrowserContext.newPage();
    });

    await test.step("Attendees browser context.", async () => {
      attendeePage = await attendeeBrowserContext.newPage();
    });

    let organiserStudioPage: StudioPage;
    let attendeeWebinarStage: WebinarStage;

    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserPage
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerPage
    );

    await test.step(`Organiser joins the backstage with AV on.`, async () => {
      await organiserPage.goto(landingPage);
      await organiserPage.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({ muteAudio: false, muteVideo: false });
    });

    await test.step(`Speaker logs in to the webinar with AV on.`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerPage.goto(speakerMagicLink);
      await speakerPage.locator("text=Enter Now").click();
      await speakerStudioJoinModal.enterInsideTheStudio({
        muteAudio: false,
        muteVideo: false,
      });
    });

    await test.step(`Verify Organiser and Speaker stream in backstage.`, async () => {
      organiserBackstage = new StudioPage(organiserPage, studioController);
      await organiserBackstage.isBackstageVisible();

      speakerBackstage = new StudioPage(speakerPage);
      // speakerBackstage.closeStudioWalkthrough();

      await test.step(`Verify Organiser view.`, async () => {
        await organiserBackstage.verifyCountOfStreamInBackstage(2);
      });
      await test.step(`Verify Speaker view.`, async () => {
        await speakerBackstage.verifyCountOfStreamInBackstage(2);
      });
    });

    await test.step(`Attendee logs in to the webinar.`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarId,
        attendeeEmail
      );

      console.log(
        `fetched attendee magic link for webinar is ${attendeeMagicLink} `
      );

      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.locator("text=Enter Now").click();
      attendeeWebinarStage = new WebinarStage(attendeePage);
      await test.step(`attendee verifies that he sees the interaction panel on stage`, async () => {
        await attendeeWebinarStage.getInteractionPanel.isInteractionPanelOpenAndVisible();
      });
    });

    await test.step(`Organiser starts the session`, async () => {
      await organiserStudioPage.startTheSession();
    });

    await test.step(`Change the brand color, theme, logo, background and overlay.`, async () => {
      await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
        1
      );
    });

    await test.step(`Organiser joins the main stage.`, async () => {
      await organiserStudioPage.addUserToStageFromBackStage(
        organiserName,
        true
      );
    });

    await test.step(`Organizer sends Speaker on stage.`, async () => {
      await organiserBackstage.addUserToStageFromBackStage(speakerName);
    });

    await test.step(`Verify streams on stage.`, async () => {
      await organiserBackstage.verifyCountOfStreamInStage(2);
    });

    await test.step(`Verify Attendee's view || Organiser and Speaker stream.`, async () => {
      await attendeeWebinarStage.verifyNumberOfVisibileStreams(2);
    });

    await test.step(`Save a scene and capture the details.`, async () => {
      await organiserBackstage.getStudioSceneComponent.saveCurrentScene(
        studioId
      );
      await organiserBackstage.getStudioSceneComponent.captureExpectedData();
    });

    await test.step(`Change theme, logo, background and overlay.`, async () => {
      await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
        2
      );
      await organiserBackstage.verifyCountOfStreamInBackstage(2);
    });

    await test.step(`Turn off Audio/Video for both Organiser and Speaker.`, async () => {
      await organiserBackstage.getStudioAvModal.turnAudioOff();
      await organiserBackstage.getStudioAvModal.turnVideoOff();
      await speakerBackstage.getStudioAvModal.turnAudioOff();
      await speakerBackstage.getStudioAvModal.turnVideoOff();
    });

    await test.step(`Play the scene.`, async () => {
      await organiserBackstage.getStudioSceneComponent.playScene();
      await organiserBackstage.verifyCountOfStreamInStage(2);
    });

    await test.step(`Validate the applied scene.`, async () => {
      await organiserBackstage.getStudioSceneComponent.validateTheScene();
    });

    await test.step(`Verify Audio/Video state.`, async () => {
      await organiserBackstage.getStudioAvModal.verifyAudioState(false);
      await organiserBackstage.getStudioAvModal.verifyVideoState(false);
      await speakerBackstage.getStudioAvModal.verifyAudioState(false);
      await speakerBackstage.getStudioAvModal.verifyVideoState(false);
    });

    await test.step(`End the session.`, async () => {
      await organiserBackstage.endTheSession();
    });
  });

  test("TC007: Verify Audio/Video stream state had no impact on saving/playing scene. || AV Off when scene is saved, AV On when scene is played.", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-482/verify-audiovideo-stream-state-had-no-impact-on-savingplaying-scene",
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step("Speaker browser context.", async () => {
      speakerPage = await speakerBrowserContext.newPage();
    });

    await test.step("Attendees browser context.", async () => {
      attendeePage = await attendeeBrowserContext.newPage();
    });

    let organiserStudioPage: StudioPage;
    let attendeeWebinarStage: WebinarStage;

    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserPage
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerPage
    );

    await test.step(`Organiser joins the backstage with AV off.`, async () => {
      await organiserPage.goto(landingPage);
      await organiserPage.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({});
    });

    await test.step(`Speaker logs in to the webinar with AV off.`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerPage.goto(speakerMagicLink);
      await speakerPage.locator("text=Enter Now").click();
      await speakerStudioJoinModal.enterInsideTheStudio({});
    });

    await test.step(`Verify Organiser and Speaker stream in backstage.`, async () => {
      organiserBackstage = new StudioPage(organiserPage, studioController);
      await organiserBackstage.isBackstageVisible();

      speakerBackstage = new StudioPage(speakerPage);
      // speakerBackstage.closeStudioWalkthrough();

      await test.step(`Verify Organiser view.`, async () => {
        await organiserBackstage.verifyCountOfStreamInBackstage(2);
      });
      await test.step(`Verify Speaker view.`, async () => {
        await speakerBackstage.verifyCountOfStreamInBackstage(2);
      });
    });

    await test.step(`Attendee logs in to the webinar.`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarId,
        attendeeEmail
      );

      console.log(
        `fetched attendee magic link for webinar is ${attendeeMagicLink} `
      );

      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.locator("text=Enter Now").click();
      attendeeWebinarStage = new WebinarStage(attendeePage);
      await test.step(`attendee verifies that he sees the interaction panel on stage`, async () => {
        await attendeeWebinarStage.getInteractionPanel.isInteractionPanelOpenAndVisible();
      });
    });

    await test.step(`Organiser starts the session`, async () => {
      await organiserStudioPage.startTheSession();
    });

    await test.step(`Change the brand color, theme, logo, background and overlay.`, async () => {
      await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
        1
      );
    });

    await test.step(`Organiser joins the main stage.`, async () => {
      await organiserStudioPage.addUserToStageFromBackStage(
        organiserName,
        true
      );
    });

    await test.step(`Organizer sends Speaker on stage.`, async () => {
      await organiserBackstage.addUserToStageFromBackStage(speakerName);
    });

    await test.step(`Verify streams on stage.`, async () => {
      await organiserBackstage.verifyCountOfStreamInStage(2);
    });

    await test.step(`Verify Attendee view || 2 streams.`, async () => {
      await attendeeWebinarStage.verifyNumberOfVisibileStreams(2);
    });

    await test.step(`Save a scene and capture the details.`, async () => {
      await organiserBackstage.getStudioSceneComponent.saveCurrentScene(
        studioId
      );
      await organiserBackstage.getStudioSceneComponent.captureExpectedData();
    });

    await test.step(`Change theme, logo, background and overlay.`, async () => {
      await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
        2
      );
      await organiserBackstage.verifyCountOfStreamInBackstage(2);
    });

    await test.step(`Turn on Audio/Video for both Organiser and Speaker.`, async () => {
      await organiserBackstage.getStudioAvModal.turnAudioOn();
      await organiserBackstage.getStudioAvModal.turnVideoOn();
      await speakerBackstage.getStudioAvModal.turnAudioOn();
      await speakerBackstage.getStudioAvModal.turnVideoOn();
    });

    await test.step(`Play the scene.`, async () => {
      await organiserBackstage.getStudioSceneComponent.playScene();
      await organiserBackstage.verifyCountOfStreamInStage(2);
    });

    await test.step(`Validate the applied scene.`, async () => {
      await organiserBackstage.getStudioSceneComponent.validateTheScene();
    });

    await test.step(`Verify Audio/Video state.`, async () => {
      await organiserBackstage.getStudioAvModal.verifyAudioState(true);
      await organiserBackstage.getStudioAvModal.verifyVideoState(true);
      await speakerBackstage.getStudioAvModal.verifyAudioState(true);
      await speakerBackstage.getStudioAvModal.verifyVideoState(true);
    });

    await test.step(`End the session.`, async () => {
      await organiserBackstage.endTheSession();
    });
  });

  test("TC008: Verify scene can be saved with pdf presentation along with slide number and when played, it starts broadcasting pdf from that slide.", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-483/verify-scene-can-be-saved-with-pdf-presentation-along-with-slide",
    });

    let beforeSlideNumber;
    let afterSlideNumber;

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step("Speaker browser context.", async () => {
      speakerPage = await speakerBrowserContext.newPage();
    });

    await test.step("Attendees browser context.", async () => {
      attendeePage = await attendeeBrowserContext.newPage();
    });

    let organiserStudioPage: StudioPage;
    let attendeeWebinarStage: WebinarStage;

    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserPage
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerPage
    );

    await test.step(`Organiser joins the backstage with AV on.`, async () => {
      await organiserPage.goto(landingPage);
      await organiserPage.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({ muteAudio: false, muteVideo: false });
    });

    await test.step(`Speaker logs in to the webinar with AV on.`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerPage.goto(speakerMagicLink);
      await speakerPage.locator("text=Enter Now").click();
      await speakerStudioJoinModal.enterInsideTheStudio({
        muteAudio: false,
        muteVideo: false,
      });
    });

    await test.step(`Verify Organiser and Speaker stream in backstage.`, async () => {
      organiserBackstage = new StudioPage(organiserPage, studioController);
      await organiserBackstage.isBackstageVisible();

      speakerBackstage = new StudioPage(speakerPage);
      // speakerBackstage.closeStudioWalkthrough();

      await test.step(`Verify Organiser view.`, async () => {
        await organiserBackstage.verifyCountOfStreamInBackstage(2);
      });
      await test.step(`Verify Speaker view.`, async () => {
        await speakerBackstage.verifyCountOfStreamInBackstage(2);
      });
    });

    await test.step(`Attendee logs in to the webinar.`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarId,
        attendeeEmail
      );

      console.log(
        `fetched attendee magic link for webinar is ${attendeeMagicLink} `
      );

      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.locator("text=Enter Now").click();
      attendeeWebinarStage = new WebinarStage(attendeePage);
      await test.step(`attendee verifies that he sees the interaction panel on stage`, async () => {
        await attendeeWebinarStage.getInteractionPanel.isInteractionPanelOpenAndVisible();
      });
    });

    await test.step(`Organiser starts the session`, async () => {
      await organiserStudioPage.startTheSession();
    });

    await test.step(`Change the brand color, theme, logo, background and overlay.`, async () => {
      await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
        1
      );
    });

    await test.step(`Organiser joins the main stage.`, async () => {
      await organiserStudioPage.addUserToStageFromBackStage(
        organiserName,
        true
      );
    });

    await test.step(`Organizer sends Speaker on stage.`, async () => {
      await organiserBackstage.addUserToStageFromBackStage(speakerName);
    });

    await test.step(`Verify streams on stage.`, async () => {
      await organiserBackstage.verifyCountOfStreamInStage(2);
    });

    await test.step(`Organiser uploades a PDF and present it on stage.`, async () => {
      await organiserBackstage.getContentPanel.uploadPdfOnStage("sample");
      await organiserBackstage.getContentPanel.playPdfOnStage("sample");
    });

    await test.step(`Turn few pages of PDF.`, async () => {
      beforeSlideNumber = await organiserBackstage.getContentPanel.turnNextPage(
        5
      );
    });

    await test.step(`Verify Attendee view || 2 streams and PDF.`, async () => {
      await attendeeWebinarStage.verifyNumberOfVisibileStreams(2);
      await attendeeWebinarStage.verifyPresentationIsVisible(true);
    });

    await test.step(`Save a scene and capture the details.`, async () => {
      await organiserBackstage.getStudioSceneComponent.saveCurrentScene(
        studioId
      );
      await organiserBackstage.getStudioSceneComponent.captureExpectedData();
    });

    await test.step(`Change theme, logo, background and overlay.`, async () => {
      await organiserBackstage.getStudioSceneComponent.changeStageConfiguration(
        2
      );
      await organiserBackstage.verifyCountOfStreamInBackstage(2);
    });

    await test.step(`Play the scene.`, async () => {
      await organiserBackstage.getStudioSceneComponent.playScene();
      await organiserBackstage.verifyCountOfStreamInStage(3);
      afterSlideNumber =
        await organiserBackstage.getContentPanel.getCurrentSlideNumber();
    });

    await test.step(`Validate the applied scene.`, async () => {
      await organiserBackstage.getStudioSceneComponent.validateTheScene();

      await test.step(`Validate the Slide number.`, async () => {
        expect(beforeSlideNumber).toBe(afterSlideNumber);
      });
    });

    await test.step(`End the session.`, async () => {
      await organiserBackstage.endTheSession();
    });
  });
});
