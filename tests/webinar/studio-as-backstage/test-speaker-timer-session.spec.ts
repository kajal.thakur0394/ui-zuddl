import test, { APIRequestContext, BrowserContext } from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import BrowserName from "../../../enums/BrowserEnum";
import BrowserFactory from "../../../util/BrowserFactory";
import { WebinarController } from "../../../controller/WebinarController";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { WebinarStage } from "../../../page-objects/webinar-pages/webinarStage";
import { StudioJoiningAvModal } from "../../../page-objects/studio-pages/studioEntryAvModal";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";
import { StageController } from "playwright-qa/controller/StageController";

test.describe.parallel(`@webinar @studio`, async () => {
  test.slow();
  let webinarId;
  let studioId;
  let organiserPage;
  let speakerPage;
  let attendeePage;
  let landingPage: string;
  let speakerEmail: string;
  let attendeeEmail: string;
  let attendeeMagicLink: string;
  let speakerMagicLink: string;
  let organiserBackstage: StudioPage;
  let speakerBackstage: StudioPage;
  let organiserStudioPage: StudioPage;
  let attendeeWebinarStage: WebinarStage;

  let orgApiContext: APIRequestContext;
  let orgBrowserContext: BrowserContext;
  let speakerBrowserContext: BrowserContext;
  let attendeeBrowserContext: BrowserContext;
  let webinarController: WebinarController;

  test.beforeEach(async () => {
    await test.step("Initialise Organiser browser context.", async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: true,
      });
    });

    await test.step("Initialise Organiser API request context.", async () => {
      orgApiContext = orgBrowserContext.request;
    });

    await test.step("Initialise Speaker browser context.", async () => {
      speakerBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });
    });

    await test.step("Initialise Attendee browser context.", async () => {
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: false,
      });
    });

    await test.step("Create new Webinar.", async () => {
      webinarId = await new WebinarController(orgApiContext).createNewWebinar({
        title: "By automation",
      });
      console.log(`Webinar ID - ${webinarId}.`);

      studioId = await new WebinarController(
        orgApiContext,
        webinarId
      ).getStudioIdForWebinar();
      console.log(`Studio ID - ${studioId}`);

      let stageController = new StageController(
        orgApiContext,
        webinarId,
        studioId
      );
      await stageController.disableRecordingForStudioAsBackstage();
    });

    await test.step("Initialise Webinar Controller.", async () => {
      webinarController = new WebinarController(
        orgApiContext,
        webinarId,
        studioId
      );
    });

    await test.step("Fetching attendee landing page.", async () => {
      landingPage =
        webinarController.getWebinarInfoDto.webinarAttendeeLandingPage();
    });

    await test.step("Initilise Speaker name and get random Speaker email.", async () => {
      speakerEmail = DataUtl.getRandomSpeakerEmail();
    });

    await test.step("Enable Magic link for Speaker and Attendee.", async () => {
      await webinarController.updateMagicLinkStatusForWebinar({
        enableSpeakerMagicLink: true,
        enableAttendeeMagicLink: true,
      });
    });

    await test.step("Disable onboarding checks for Attendee.", async () => {
      await webinarController.disableOnboardingChecksForAttendee();
    });

    await test.step("Initilise Attendee name and get random Attendee email.", async () => {
      attendeeEmail = DataUtl.getRandomAttendeeTestJoynEmail();
    });

    await test.step("Add new Speaker and Attendee to Webinar.", async () => {
      await webinarController.addNewSpeakerToWebinar(speakerEmail, true);
      await webinarController.addNewAttendeeToWebinar(attendeeEmail);
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    await test.step("Speaker browser context.", async () => {
      speakerPage = await speakerBrowserContext.newPage();
    });

    await test.step("Attendees browser context.", async () => {
      attendeePage = await attendeeBrowserContext.newPage();
    });
  });

  test.afterEach(async () => {
    await webinarController.endLiveSessionOnStudio();
    await webinarController.stopVideoPlayBackOnStudio();
    await orgBrowserContext?.close();
    await speakerBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test("TC001: Speaker timer.", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-475/speaker-timer",
    });

    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserPage
    );
    let speakerStudioJoinModal: StudioJoiningAvModal = new StudioJoiningAvModal(
      speakerPage
    );

    await test.step(`Organiser joins the backstage with AV on.`, async () => {
      await organiserPage.goto(landingPage);
      await organiserPage.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({ muteAudio: false, muteVideo: false });
    });

    await test.step(`Speaker logs in to the webinar mid-sesison with AV on.`, async () => {
      speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
        webinarId,
        speakerEmail
      );

      console.log(
        `fetched speaker magic link for webinar is ${speakerMagicLink} `
      );

      await speakerPage.goto(speakerMagicLink);
      await speakerPage.locator("text=Enter Now").click();
      await speakerStudioJoinModal.enterInsideTheStudio({
        muteAudio: false,
        muteVideo: false,
      });
    });

    await test.step(`Verify Organiser and Spreaker stream in backstage.`, async () => {
      organiserBackstage = new StudioPage(organiserPage);
      await organiserBackstage.isBackstageVisible();

      speakerBackstage = new StudioPage(speakerPage);
      // speakerBackstage.closeStudioWalkthrough();

      await test.step(`Verify Organiser view.`, async () => {
        await organiserBackstage.verifyCountOfStreamInBackstage(2);
      });
      await test.step(`Verify Speaker view.`, async () => {
        await speakerBackstage.verifyCountOfStreamInBackstage(2);
      });
    });

    await test.step(`Attendee logs in to the webinar.`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        webinarId,
        attendeeEmail
      );

      console.log(
        `fetched attendee magic link for webinar is ${attendeeMagicLink} `
      );

      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.locator("text=Enter Now").click();
      attendeeWebinarStage = new WebinarStage(attendeePage);
      await test.step(`attendee verifies that he sees the interaction panel on stage`, async () => {
        await attendeeWebinarStage.getInteractionPanel.isInteractionPanelOpenAndVisible();
      });
    });

    await test.step(`Organiser starts the session.`, async () => {
      await organiserStudioPage.startTheSession();
    });

    await test.step(`Organiser joins the main stage.`, async () => {
      await organiserStudioPage.addUserToStageFromBackStage("You");
    });

    await test.step(`Organizer sends Speaker on stage.`, async () => {
      await organiserBackstage.addUserToStageFromBackStage(
        "automation parashar"
      );
    });

    await test.step(`Verify Attendee's view || Organiser and Speaker stream.`, async () => {
      await attendeeWebinarStage.verifyNumberOfVisibileStreams(2);
    });

    await test.step("Organiser start the speaker-timer.", async () => {
      await organiserBackstage.clickOnExtraTab();
      await organiserBackstage.startTheTimer("50");
    });

    await test.step("Time Validation.", async () => {
      await test.step("Timer is visibile to Organiser.", async () => {
        await organiserBackstage.verifyTimerVisibility(true);
      });

      await test.step("Timer is visibile to Speaker.", async () => {
        await speakerBackstage.verifyTimerVisibility(true);
      });

      await test.step("Timer is visibile to Attendee.", async () => {
        await attendeeWebinarStage.verifyTimerVisibility(true);
      });
    });
    await test.step(`Organizer joins backstage.`, async () => {
      await organiserBackstage.removeUserFromStage("You");
    });

    await test.step(`Organizer send Speaker to backstage.`, async () => {
      await organiserBackstage.removeUserFromStage("automation parashar");
    });

    await test.step(`Verify Attendee's view || No stream.`, async () => {
      await attendeeWebinarStage.verifyNumberOfVisibileStreams(0);
    });

    await test.step("Verify timer visibilty.", async () => {
      await test.step("Timer is visibile to Organiser.", async () => {
        await organiserBackstage.verifyTimerVisibility(true);
      });

      await test.step("Timer is visibile to Speaker.", async () => {
        await speakerBackstage.verifyTimerVisibility(true);
      });

      await test.step("Timer is visibile to Attendee.", async () => {
        await attendeeWebinarStage.verifyTimerVisibility(true);
      });
    });

    await test.step("Organiser stops the timer.", async () => {
      await organiserBackstage.clickOnExtraTab();
      await organiserBackstage.stopTheTimer();
    });

    await test.step("Verify timer visibility.", async () => {
      await test.step("Timer is not visibile to Organiser.", async () => {
        await organiserBackstage.verifyTimerVisibility(false);
      });

      await test.step("Timer is not visibile to Organiser.", async () => {
        await speakerBackstage.verifyTimerVisibility(false);
      });

      await test.step("Timer is not visibile to Attendee.", async () => {
        await attendeeWebinarStage.verifyTimerVisibility(false);
      });
    });

    await test.step(`Backstage validation.`, async () => {
      await test.step(`Verify Organiser's view || Organiser and Speaker stream in green room.`, async () => {
        await organiserBackstage.verifyCountOfStreamInBackstage(2);
      });

      await test.step(`Verify Speaker's view || Organiser and Speaker stream in green room.`, async () => {
        await speakerBackstage.verifyCountOfStreamInBackstage(2);
      });

      await test.step(`Verify Speaker's view || Organiser and Speaker stream in green room.`, async () => {
        await attendeeWebinarStage.verifyNumberOfVisibileStreams(0);
      });
    });
  });
});
