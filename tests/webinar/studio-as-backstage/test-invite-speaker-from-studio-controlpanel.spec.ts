import test, { APIRequestContext, BrowserContext } from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import BrowserName from "../../../enums/BrowserEnum";
import BrowserFactory from "../../../util/BrowserFactory";
import { WebinarController } from "../../../controller/WebinarController";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { WebinarStage } from "../../../page-objects/webinar-pages/webinarStage";

test.describe.parallel(`@webinar @studio`, async () => {
  let webinarId;
  let studioId;
  let organiserPage;
  let landingPage: string;
  let expectedPeoplePageUrl: string;
  let organiserBackstage: StudioPage;

  let orgApiContext: APIRequestContext;
  let orgBrowserContext: BrowserContext;
  let webinarController: WebinarController;

  test.beforeEach(async () => {
    await test.step("Initialise Organiser browser context.", async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: true,
      });
    });

    await test.step("Initialise Organiser API request context.", async () => {
      orgApiContext = orgBrowserContext.request;
    });

    await test.step("Create new Webinar.", async () => {
      webinarId = await new WebinarController(orgApiContext).createNewWebinar({
        title: "By automation",
      });
      console.log(`Webinar ID - ${webinarId}.`);

      studioId = await new WebinarController(
        orgApiContext,
        webinarId
      ).getStudioIdForWebinar();
      console.log(`Studio ID - ${studioId}`);
    });

    await test.step("Initialise Webinar Controller.", async () => {
      webinarController = new WebinarController(
        orgApiContext,
        webinarId,
        studioId
      );
    });

    await test.step("Fetching attendee landing page.", async () => {
      landingPage =
        webinarController.getWebinarInfoDto.webinarAttendeeLandingPage();
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });

    expectedPeoplePageUrl = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/webinar/${webinarId}/people/speakers`;
  });

  test.afterEach(async () => {
    await webinarController.endLiveSessionOnStudio();
    await webinarController.stopVideoPlayBackOnStudio();
    await orgBrowserContext?.close();
  });

  test("TC001: Invite speaker flow from inside of studio control panel, should navigate to events people section.", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-471/invite-speaker-from-studio-control-panel",
    });

    let organiserStudioPage: StudioPage;

    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserPage
    );

    await test.step(`Organiser joins the backstage with AV on.`, async () => {
      await organiserPage.goto(landingPage);
      await organiserPage.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({ muteAudio: false, muteVideo: false });
    });

    await test.step(`Verify Organiser's stream in backstage.`, async () => {
      organiserBackstage = new StudioPage(organiserPage);
      await organiserBackstage.isBackstageVisible();
      await organiserBackstage.verifyCountOfStreamInBackstage(1);
    });

    await test.step(`Organiser clicks on "Invite a speaker" in Studio control panel.`, async () => {
      await organiserBackstage.clickAddSpeakerButton();
    });

    await test.step(`Click on invite speaker button.`, async () => {
      await organiserBackstage.clickInviteSpeakerButtonAndVerifyPage();
    });

    await test.step(`Verify the button redirects to "Event people section" page.`, async () => {
      await organiserBackstage.verifyPageUrl(
        orgBrowserContext,
        expectedPeoplePageUrl
      );
    });
  });
});
