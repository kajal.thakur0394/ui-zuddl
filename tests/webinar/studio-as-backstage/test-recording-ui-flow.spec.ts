import test, { APIRequestContext, BrowserContext } from "@playwright/test";
import BrowserName from "../../../enums/BrowserEnum";
import BrowserFactory from "../../../util/BrowserFactory";
import { EventController } from "../../../controller/EventController";
import { StudioPage } from "../../../page-objects/studio-pages/StudioPage";
import { StudioController } from "../../../controller/StudioController";
import { WebinarController } from "../../../controller/WebinarController";
import { WebinarStage } from "../../../page-objects/webinar-pages/webinarStage";
import { DataUtl } from "../../../util/dataUtil";
import { RecordingController } from "../../../controller/RecordingController";
import { setTimeOut } from "../../../util/commonUtil";

test.describe.parallel(`@webinar @studio`, async () => {
  let webinarId;
  let stageId;
  let studioId;
  let organiserPage;
  let landingPage: string;
  let studioController: StudioController;
  let eventController: EventController;

  let orgApiContext: APIRequestContext;
  let orgBrowserContext: BrowserContext;
  let webinarController: WebinarController;

  test.beforeEach(async () => {
    await test.step("Initialise Organiser browser context.", async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        browserName: BrowserName.CHROME,
        laodOrganiserCookies: true,
      });
    });

    await test.step("Initialise Organiser API request context.", async () => {
      orgApiContext = orgBrowserContext.request;
    });

    await test.step("Create new Webinar.", async () => {
      webinarId = await new WebinarController(orgApiContext).createNewWebinar({
        title: "By automation",
      });
      console.log(`Webinar ID - ${webinarId}.`);

      studioId = await new WebinarController(
        orgApiContext,
        webinarId
      ).getStudioIdForWebinar();
      console.log(`Studio ID - ${studioId}`);
    });

    await test.step("Initialise Webinar Controller.", async () => {
      webinarController = new WebinarController(
        orgApiContext,
        webinarId,
        studioId
      );
    });

    await test.step("Initialise Studio Controller.", async () => {
      studioController = new StudioController(orgApiContext, studioId, stageId);
    });

    await test.step("Initialise Event Controller.", async () => {
      eventController = new EventController(orgApiContext, webinarId);
    });

    await test.step("Fetching deafult stage id.", async () => {
      stageId = await eventController.getDefaultStageId();
    });

    await test.step("Fetching landing page.", async () => {
      landingPage =
        webinarController.getWebinarInfoDto.webinarAttendeeLandingPage();
    });

    await test.step("Organiser browser context.", async () => {
      organiserPage = await orgBrowserContext.newPage();
    });
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test("TC001: Disable recording and then verify it, then Enable it again and verify it.", async () => {
    test.info().annotations.push({
      type: "TC",
      description: "https://linear.app/zuddl/issue/QAT-472/recording-ui-flow",
    });

    let organiserStudioPage: StudioPage;
    let organiserBackstage: StudioPage;

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];

    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserPage
    );

    await test.step(`Organiser joins the backstage with AV on.`, async () => {
      await organiserPage.goto(landingPage);
      await organiserPage.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({ muteAudio: false, muteVideo: false });
    });

    await test.step(`Verify Organiser's stream in backstage.`, async () => {
      organiserBackstage = new StudioPage(organiserPage);
      await organiserBackstage.isBackstageVisible();
      await organiserBackstage.verifyCountOfStreamInBackstage(1);
    });

    await test.step("Disable dry-run.", async () => {
      await studioController.disableDryRunForWebinar();
    });

    await test.step("Disable the recording.", async () => {
      await organiserBackstage.changeRecordingStatus(false);
    });

    await test.step(`Organizer initiates the session and joins the stage.`, async () => {
      await organiserBackstage.addUserToStageFromBackStage(
        organiserFirstName,
        true
      );
      await organiserBackstage.startTheSession();
    });

    await test.step(`Verify recording is diabled during session.`, async () => {
      await organiserBackstage.verifyRecordingStatus(false);
    });

    await test.step(`End the session.`, async () => {
      await organiserBackstage.endTheSession();
    });

    await test.step(`Enable the recording.`, async () => {
      await organiserBackstage.changeRecordingStatus(true);
    });

    await test.step(`Organizer initiates the session.`, async () => {
      await organiserBackstage.startTheSession();
    });

    await test.step(`Verify recording is enabled during session.`, async () => {
      await organiserBackstage.verifyRecordingStatus(true);
    });

    await test.step(`End the session.`, async () => {
      await organiserBackstage.endTheSession();
    });
  });
  test("TC002: Verify recording video is getting listed on organizer side.", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-923/verify-recording-video-is-coming-on-organizer-side",
    });
    test.skip(
      process.env.run_env !== "pre-prod",
      "Skipping recording as it can only run on preprod"
    );
    let organiserStudioPage: StudioPage;
    let organiserBackstage: StudioPage;
    let recordingController: RecordingController;

    const organiserFirstName =
      DataUtl.getApplicationTestDataObj()["eventsOrganiserFirstName"];
    recordingController = new RecordingController(orgApiContext, webinarId);

    let organiserWebinarStagePage: WebinarStage = new WebinarStage(
      organiserPage
    );

    await test.step(`Organiser joins the backstage with AV on.`, async () => {
      await organiserPage.goto(landingPage);
      await organiserPage.locator("text=Enter Now").click();
      organiserStudioPage = await (
        await organiserWebinarStagePage.clickOnGoBackstageOnWelcomeStageModal()
      ).enterInsideTheStudio({ muteAudio: false, muteVideo: false });
    });

    await test.step(`Verify Organiser's stream in backstage.`, async () => {
      organiserBackstage = new StudioPage(organiserPage);
      await organiserBackstage.isBackstageVisible();
      await organiserBackstage.verifyCountOfStreamInBackstage(1);
    });

    await test.step("Disable dry-run.", async () => {
      await studioController.disableDryRunForWebinar();
    });

    await test.step(`Enable the recording.`, async () => {
      await organiserBackstage.changeRecordingStatus(true);
    });

    await test.step(`Organizer initiates the session.`, async () => {
      await organiserBackstage.startTheSession();
    });

    await test.step(`Verify recording is enabled during session.`, async () => {
      await organiserBackstage.verifyRecordingStatus(true);
    });

    await test.step(`End the session.`, async () => {
      await setTimeOut(6000);
      await organiserBackstage.endTheSession();
    });
    await test.step(`Verify Recording duration to be Non-Zero`, async () => {
      await recordingController.verifyIfDurationIsNonZero();
    });

    await test.step(`Verify Recording Size to be Non-Zero`, async () => {
      await recordingController.verifyIfSizeByteIsNonZero();
    });
  });
});
