import {
  APIRequestContext,
  BrowserContext,
  expect,
  test,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { EventController } from "../../../controller/EventController";
import { HubspotIntegrationController } from "../../../controller/HubspotIntegrationController";
import { DataUtl } from "../../../util/dataUtil";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import integrationType from "../../../enums/integrationTYpeEnum";
import hubspotRegistrationMappingTestdata from "../../../test-data/hubspotintegration-testdata/registration-fields-n-mapping-data.json";
import { HubspotIntegrationDirectionType } from "../../../enums/SalesforceIntegrationDirectionEnum";
import EventRole from "../../../enums/eventRoleEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { IntegrationController } from "../../../controller/IntegrationController";
import { fieldMappingPayLoad } from "../../../interfaces/IntegrationControllerInterface";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe(`@integrations @hubspot @export`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId;
  let eventTitle: string;
  let eventController: EventController;
  let hubspotController: HubspotIntegrationController;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiContext = orgBrowserContext.request;

    await test.step(`Create a new event`, async () => {
      eventTitle = DataUtl.getRandomEventTitle();
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: eventTitle,
      });
      eventController = new EventController(orgApiContext, eventId);
      console.log(
        `Event created succesfully with eventTitle->${eventTitle} and eventId->${eventId}`
      );
    });

    await test.step(`Initialise the hubspot integration controller`, async () => {
      hubspotController = new HubspotIntegrationController(
        orgApiContext,
        eventId
      );
    });
    //get field list, if it contains more than passed in payload, remove it.
    let integrationController = new IntegrationController(orgApiContext);
    let payload: fieldMappingPayLoad = {
      organizationFieldMapping: {
        lastName: "lastname",
        firstName: "firstname",
        email: "email",
      },
      organizationPolicyMapping: {
        lastname: "ALWAYS_UPDATE",
        firstname: "ALWAYS_UPDATE",
        email: "DO_NOT_UPDATE",
      },
    };
    await test.step(`Now, add as the pre-defined field at org level via API`, async () => {
      await integrationController.addOrganisationMappingFields(
        integrationType.HUBSPOT,
        payload
      );
    });
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test("TC001: Import: Verify attendee gets synced to zuddl in an event having pre-defined fields", async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-672/import-verify-attendee-gets-synced-to-zuddl-in-an-event",
    });
    let staticEventId: any;
    let userRecordFromTray: JSON;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let userInfoDTO: UserInfoDTO;
    let eventInfoDto: EventInfoDTO = new EventInfoDTO(
      eventId,
      eventTitle,
      false
    );
    let userEventRoleDto: UserEventRoleDto;
    let registrationDataFromDb: any;
    let integrationController = new IntegrationController(orgApiContext);
    let payload: fieldMappingPayLoad = {
      organizationFieldMapping: {
        lastName: "lastname",
        firstName: "firstname",
        country: "country",
        phoneNumber: "phone",
        company: "company",
        email: "email",
      },
      organizationPolicyMapping: {
        lastname: "ALWAYS_UPDATE",
        firstname: "ALWAYS_UPDATE",
        country: "ALWAYS_UPDATE",
        phone: "ALWAYS_UPDATE",
        company: "ALWAYS_UPDATE",
        email: "DO_NOT_UPDATE",
      },
    };
    await test.step(`Now, add as the pre-defined field at org level via API`, async () => {
      await integrationController.addOrganisationMappingFields(
        integrationType.HUBSPOT,
        payload
      );
    });
    await test.step(`Add pre-defined fields for an event`, async () => {
      const customFieldsData =
        hubspotRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["registration-form-data-with-only-predefined-fields"];
      await eventController.addCustomFieldsWithConditionToEvent(
        customFieldsData
      );
    });

    await test.step(`Enable hubspot integration for the event ${eventId}`, async () => {
      await hubspotController.enableHubspotIntegrationForThisEvent({
        integrationType: integrationType.HUBSPOT,
        status: true,
        integrationDirectionType: HubspotIntegrationDirectionType.PUSH,
        shouldSyncData: true,
      });
      console.log(`Successfully enabled hubspot Integration`);
    });

    await test.step(`Update mapping of zuddl fields with hubspot fields`, async () => {
      const customFieldMapping =
        hubspotRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customFieldMapping"];
      const customFieldPolicyMapping =
        hubspotRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customPolicyMapping"];

      await hubspotController.mapZuddlFieldsWithHubspotFields(
        {
          customFieldMapping: customFieldMapping,
          customPolicyMapping: customFieldPolicyMapping,
        },
        integrationType.HUBSPOT
      );
    });

    await test.step(`Register a new user to event`, async () => {
      let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
        .attendeeLandingPage;
      userInfoDTO = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateekimport",
        "parashar",
        true
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDTO,
        eventInfoDto,
        EventRole.NOTEXISTS,
        false
      );
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendeeLandingPage);
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData.predefinedFieldsFormDataForHubspotPushCase,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
        }
      );
    });

    await test.step(`Fetch data from tray for event ${eventId} to verify registration data is synced to tray for Hubspot`, async () => {
      await page.waitForTimeout(2 * 60000);
      userRecordFromTray =
        await hubspotController.getHubspotRegisterUserDataFromTray(
          attendeeEmail
        );
    });

    await test.step(`Update static event data time`, async () => {
      staticEventId =
        DataUtl.getApplicationTestDataObj()["hubspotImportEventId"];
      let staticEventController = new EventController(
        orgApiContext,
        staticEventId
      );
      await staticEventController.changeEventStartAndEndDateTime({
        deltaByDayInStartDate: 1,
        deltaByDayInEndDate: 3,
        deltaByHoursInStartTime: -8,
        deltaByHoursInEndTime: -4,
      });
    });

    await test.step(`Update mapping of zuddl fields with hubspot fields`, async () => {
      let staticHubspotController = new HubspotIntegrationController(
        orgApiContext,
        staticEventId
      );
      const customFieldMapping =
        hubspotRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customFieldMapping"];
      const customFieldPolicyMapping =
        hubspotRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customPolicyMapping"];

      await staticHubspotController.mapZuddlFieldsWithHubspotFields(
        {
          customFieldMapping: customFieldMapping,
          customPolicyMapping: customFieldPolicyMapping,
        },
        integrationType.HUBSPOT
      );
    });

    await test.step(`Wait for 2 min until data is synced to hubspot workflow`, async () => {
      await page.waitForTimeout(2 * 20000);
    });

    await test.step(`Now verify in event registration table, registration records are pulled from previous event`, async () => {
      await test.step(`Verifying if registration record exists for email ${attendeeEmail} for event ${staticEventId}`, async () => {
        registrationDataFromDb =
          await QueryUtil.fetchUserEventRegistrationDataFromDb(
            staticEventId,
            attendeeEmail
          );
      });
      await test.step(`Verify registration id`, async () => {
        let registrationId = registrationDataFromDb["registration_id"];
        expect(
          registrationId,
          `expecting ${registrationId} to not be null`
        ).not.toBeNull();
      });
      await test.step(`Verify Company Name`, async () => {
        let companyName = registrationDataFromDb["company"];
        expect(
          companyName,
          `expecting companyName to be ${userInfoDTO.userRegFormData.predefinedFieldsFormDataForHubspotPushCase["company"]?.dataToEnter}`
        ).toBe(
          userInfoDTO.userRegFormData
            .predefinedFieldsFormDataForHubspotPushCase["company"]?.dataToEnter
        );
      });
      await test.step(`Verify Country`, async () => {
        let country = registrationDataFromDb["country"];
        expect(
          country,
          `expecting country to be ${userInfoDTO.userRegFormData.predefinedFieldsFormDataForHubspotPushCase["country"]?.dataToEnter}`
        ).toBe(
          userInfoDTO.userRegFormData
            .predefinedFieldsFormDataForHubspotPushCase["country"]?.dataToEnter
        );
      });
      await test.step(`Verify Phone Number`, async () => {
        let phoneNumber = registrationDataFromDb["phone_number"];
        expect(
          phoneNumber,
          `expecting phoneNumber to be ${userInfoDTO.userRegFormData.predefinedFieldsFormDataForHubspotPushCase["phoneNumber"]?.dataToEnter}`
        ).toBe(
          userInfoDTO.userRegFormData
            .predefinedFieldsFormDataForHubspotPushCase["phoneNumber"]
            ?.dataToEnter
        );
      });
    });
  });
});
