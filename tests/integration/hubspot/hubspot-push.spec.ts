import {
  APIRequestContext,
  BrowserContext,
  expect,
  test,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { EventController } from "../../../controller/EventController";
import { DataUtl } from "../../../util/dataUtil";
import { HubspotIntegrationController } from "../../../controller/HubspotIntegrationController";
import { HubspotIntegrationDirectionType } from "../../../enums/SalesforceIntegrationDirectionEnum";
import integrationType from "../../../enums/integrationTYpeEnum";
import { IntegrationValidationUtil } from "../../../util/IntegrationValidationUtil";
import hubspotRegistrationMappingTestdata from "../../../test-data/hubspotintegration-testdata/registration-fields-n-mapping-data.json";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import EventRole from "../../../enums/eventRoleEnum";
import { IntegrationController } from "../../../controller/IntegrationController";
import { fieldMappingPayLoad } from "../../../interfaces/IntegrationControllerInterface";

test.describe(`@integrations @hubspot @export`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let eventId;
  let eventTitle: string;
  let eventController: EventController;
  let hubspotController: HubspotIntegrationController;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiContext = orgBrowserContext.request;

    await test.step(`Create a new event`, async () => {
      eventTitle = DataUtl.getRandomEventTitle();
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: eventTitle,
      });
      eventController = new EventController(orgApiContext, eventId);
      console.log(
        `Event created succesfully with eventTitle->${eventTitle} and eventId->${eventId}`
      );
      //get field list, if it contains more than passed in payload, remove it.
      let integrationController = new IntegrationController(orgApiContext);
      let payload: fieldMappingPayLoad = {
        organizationFieldMapping: {
          lastName: "lastname",
          firstName: "firstname",
          email: "email",
        },
        organizationPolicyMapping: {
          lastname: "ALWAYS_UPDATE",
          firstname: "ALWAYS_UPDATE",
          email: "DO_NOT_UPDATE",
        },
      };
      await test.step(`Now, add as the pre-defined field at org level via API`, async () => {
        await integrationController.addOrganisationMappingFields(
          integrationType.HUBSPOT,
          payload
        );
      });
    });

    await test.step(`Initialise the hubspot integration controller`, async () => {
      hubspotController = new HubspotIntegrationController(
        orgApiContext,
        eventId
      );
    });
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test("TC001: Export: Verify when existing record is present in an event, sync should happen post enablement of integration", async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-646/export-verify-when-existing-record-is-present-in-an-event-sync-should",
    });

    let attendeeDetails = {
      email: DataUtl.getRandomAttendeeEmail(),
      firstName: "Prateek",
      lastName: "AutomationHubSpot",
    };
    let userRecordFromTray: JSON;
    await test.step(`Register a new user to event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi(
        attendeeDetails
      );
      console.log(`successfully registered user to the event`);
    });

    await test.step(`Enable hubspot integration for the event ${eventId}`, async () => {
      await hubspotController.enableHubspotIntegrationForThisEvent({
        integrationType: integrationType.HUBSPOT,
        status: true,
        integrationDirectionType: HubspotIntegrationDirectionType.PUSH,
        shouldSyncData: true,
      });
      console.log(`Successfully enabled hubspot Integration`);
    });

    await test.step(`Fetch data from tray for event ${eventId} to verify registration data is synced to tray for SF`, async () => {
      await page.waitForTimeout(2 * 60000);
      userRecordFromTray =
        await hubspotController.getHubspotRegisterUserDataFromTray(
          attendeeDetails.email
        );
    });

    await test.step(`Now verify the record properties recieved from tray`, async () => {
      await IntegrationValidationUtil.validateHubspotDataFetchedFromTray(
        {
          email: attendeeDetails.email,
          firstname: attendeeDetails.firstName,
          lastname: attendeeDetails.lastName,
        },
        userRecordFromTray
      );
    });
  });

  test("TC002: Export: Verify when integration is enabled, new user sync should happen", async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-647/export-verify-when-integration-is-enabled-new-user-sync-should-happen",
    });

    let attendeeDetails = {
      email: DataUtl.getRandomAttendeeEmail(),
      firstName: "PrateekNew",
      lastName: "AutomationHubSpot",
    };
    let userRecordFromTray: JSON;
    await test.step(`Enable hubspot integration for the event ${eventId}`, async () => {
      await hubspotController.enableHubspotIntegrationForThisEvent({
        integrationType: integrationType.HUBSPOT,
        status: true,
        integrationDirectionType: HubspotIntegrationDirectionType.PUSH,
        shouldSyncData: true,
      });
      console.log(`Successfully enabled hubspot Integration`);
    });
    await test.step(`Register a new user to event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi(
        attendeeDetails
      );
      console.log(`successfully registered user to the event`);
    });
    await test.step(`Fetch data from tray for event ${eventId} to verify registration data is synced to tray for Hubspot`, async () => {
      await page.waitForTimeout(2 * 60000);
      userRecordFromTray =
        await hubspotController.getHubspotRegisterUserDataFromTray(
          attendeeDetails.email
        );
    });

    await test.step(`Now verify the record properties recieved from tray`, async () => {
      await IntegrationValidationUtil.validateHubspotDataFetchedFromTray(
        {
          email: attendeeDetails.email,
          firstname: attendeeDetails.firstName,
          lastname: attendeeDetails.lastName,
        },
        userRecordFromTray
      );
    });
  });

  test("TC003: Export: Verify when integration is enabled, new user sync should happen with pre-defined fields", async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-649/export-verify-when-integration-is-enabled-new-user-sync-should-happen",
    });
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let userInfoDTO: UserInfoDTO;
    let eventInfoDto: EventInfoDTO = new EventInfoDTO(
      eventId,
      eventTitle,
      false
    );
    let userRecordFromTray: JSON;
    let userEventRoleDto: UserEventRoleDto;

    await test.step(`Add pre-defined fields for an event`, async () => {
      const customFieldsData =
        hubspotRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["registration-form-data-with-only-predefined-fields"];
      await eventController.addCustomFieldsWithConditionToEvent(
        customFieldsData
      );
    });

    await test.step(`Enable hubspot integration for the event ${eventId}`, async () => {
      await hubspotController.enableHubspotIntegrationForThisEvent({
        integrationType: integrationType.HUBSPOT,
        status: true,
        integrationDirectionType: HubspotIntegrationDirectionType.PUSH,
        shouldSyncData: true,
      });
      console.log(`Successfully enabled hubspot Integration`);
    });

    await test.step(`Update mapping of zuddl fields with hubspot fields`, async () => {
      const customFieldMapping =
        hubspotRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customFieldMapping"];
      const customFieldPolicyMapping =
        hubspotRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customPolicyMapping"];

      await hubspotController.mapZuddlFieldsWithHubspotFields(
        {
          customFieldMapping: customFieldMapping,
          customPolicyMapping: customFieldPolicyMapping,
        },
        integrationType.HUBSPOT
      );
    });

    await test.step(`Register a new user to event`, async () => {
      let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
        .attendeeLandingPage;
      userInfoDTO = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        true
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDTO,
        eventInfoDto,
        EventRole.NOTEXISTS,
        false
      );
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendeeLandingPage);
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData.predefinedFieldsFormDataForHubspotPushCase,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
        }
      );
    });

    await test.step(`Fetch data from tray for event ${eventId} to verify registration data is synced to tray for Hubspot`, async () => {
      await page.waitForTimeout(2 * 60000);
      userRecordFromTray =
        await hubspotController.getHubspotRegisterUserDataFromTray(
          attendeeEmail
        );
      console.log("here");
    });

    await test.step(`Now verify the record pre-defined properties recieved from tray`, async () => {
      await IntegrationValidationUtil.validateHubspotPredefinedDataFetchedFromTray(
        {
          email: attendeeEmail,
          country:
            userInfoDTO.userRegFormData
              .predefinedFieldsFormDataForHubspotPushCase["country"]
              ?.dataToEnter,
          company:
            userInfoDTO.userRegFormData
              .predefinedFieldsFormDataForHubspotPushCase["company"]
              ?.dataToEnter,
          mobilephone:
            userInfoDTO.userRegFormData
              .predefinedFieldsFormDataForHubspotPushCase["phoneNumber"]
              ?.dataToEnter,
        },
        userRecordFromTray
      );
    });
  });

  test("TC004: Export: Verify when existing record is present in an event, sync should happen post enablement of integration with pre-defined fields", async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-648/export-verify-when-existing-record-is-present-in-an-event-sync-should",
    });

    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let userInfoDTO: UserInfoDTO;
    let eventInfoDto: EventInfoDTO = new EventInfoDTO(
      eventId,
      eventTitle,
      false
    );
    let userRecordFromTray: JSON;
    let userEventRoleDto: UserEventRoleDto;

    await test.step(`Update event data time`, async () => {
      await eventController.changeEventStartAndEndDateTime({
        deltaByDayInStartDate: 1,
        deltaByDayInEndDate: 3,
        deltaByHoursInStartTime: -8,
        deltaByHoursInEndTime: -4,
      });
    });

    await test.step(`Add pre-defined fields for an event`, async () => {
      const customFieldsData =
        hubspotRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["registration-form-data-with-only-predefined-fields"];
      await eventController.addCustomFieldsWithConditionToEvent(
        customFieldsData
      );
    });

    await test.step(`Register a new user to event`, async () => {
      let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
        .attendeeLandingPage;
      userInfoDTO = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        true
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDTO,
        eventInfoDto,
        EventRole.NOTEXISTS,
        false
      );
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendeeLandingPage);
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData.predefinedFieldsFormDataForHubspotPushCase,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
        }
      );
    });

    await test.step(`Enable hubspot integration for the event ${eventId}`, async () => {
      await hubspotController.enableHubspotIntegrationForThisEvent({
        integrationType: integrationType.HUBSPOT,
        status: true,
        integrationDirectionType: HubspotIntegrationDirectionType.PUSH,
        shouldSyncData: true,
      });
      await hubspotController.syncExistingRecords(integrationType.HUBSPOT);
      console.log(`Successfully enabled hubspot Integration`);
    });

    await test.step(`Update mapping of zuddl fields with hubspot fields`, async () => {
      const customFieldMapping =
        hubspotRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customFieldMapping"];
      const customFieldPolicyMapping =
        hubspotRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customPolicyMapping"];

      await hubspotController.mapZuddlFieldsWithHubspotFields(
        {
          customFieldMapping: customFieldMapping,
          customPolicyMapping: customFieldPolicyMapping,
        },
        integrationType.HUBSPOT
      );
    });

    await test.step(`Fetch data from tray for event ${eventId} to verify registration data is synced to tray for Hubspot`, async () => {
      await page.waitForTimeout(2 * 60000);
      userRecordFromTray =
        await hubspotController.getHubspotRegisterUserDataFromTray(
          attendeeEmail
        );
    });

    await test.step(`Now verify the record pre-defined properties recieved from tray`, async () => {
      await IntegrationValidationUtil.validateHubspotPredefinedDataFetchedFromTray(
        {
          email: attendeeEmail,
          country:
            userInfoDTO.userRegFormData
              .predefinedFieldsFormDataForHubspotPushCase["country"]
              ?.dataToEnter,
          company:
            userInfoDTO.userRegFormData
              .predefinedFieldsFormDataForHubspotPushCase["company"]
              ?.dataToEnter,
          mobilephone:
            userInfoDTO.userRegFormData
              .predefinedFieldsFormDataForHubspotPushCase["phoneNumber"]
              ?.dataToEnter,
        },
        userRecordFromTray
      );
    });
  });

  test("TC005: Export: Verify when integration is enabled, new user sync should happen with pre-defined fields", async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://https://linear.app/zuddl/issue/QAT-649/export-verify-when-integration-is-enabled-new-user-sync-should-happen",
    });

    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let userInfoDTO: UserInfoDTO;
    let eventInfoDto: EventInfoDTO = new EventInfoDTO(
      eventId,
      eventTitle,
      false
    );
    let userRecordFromTray: JSON;
    let userEventRoleDto: UserEventRoleDto;

    await test.step(`Add pre-defined fields for an event`, async () => {
      const customFieldsData =
        hubspotRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["registration-form-data-with-only-predefined-fields"];
      await eventController.addCustomFieldsWithConditionToEvent(
        customFieldsData
      );
    });

    await test.step(`Enable hubspot integration for the event ${eventId}`, async () => {
      await hubspotController.enableHubspotIntegrationForThisEvent({
        integrationType: integrationType.HUBSPOT,
        status: true,
        integrationDirectionType: HubspotIntegrationDirectionType.PUSH,
        shouldSyncData: true,
      });
      console.log(`Successfully enabled hubspot Integration`);
    });

    await test.step(`Register a new user to event`, async () => {
      let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
        .attendeeLandingPage;
      userInfoDTO = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        true
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDTO,
        eventInfoDto,
        EventRole.NOTEXISTS,
        false
      );
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendeeLandingPage);
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData.predefinedFieldsFormDataForHubspotPushCase,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
        }
      );
    });

    await test.step(`Update mapping of zuddl fields with hubspot fields`, async () => {
      const customFieldMapping =
        hubspotRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customFieldMapping"];
      const customFieldPolicyMapping =
        hubspotRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customPolicyMapping"];

      await hubspotController.mapZuddlFieldsWithHubspotFields(
        {
          customFieldMapping: customFieldMapping,
          customPolicyMapping: customFieldPolicyMapping,
        },
        integrationType.HUBSPOT
      );
    });

    await test.step(`Fetch data from tray for event ${eventId} to verify registration data is synced to tray for Hubspot`, async () => {
      await page.waitForTimeout(2 * 60000);
      userRecordFromTray =
        await hubspotController.getHubspotRegisterUserDataFromTray(
          attendeeEmail
        );
    });

    await test.step(`Now verify the record pre-defined properties recieved from tray`, async () => {
      await IntegrationValidationUtil.validateHubspotPredefinedDataFetchedFromTray(
        {
          email: attendeeEmail,
          country:
            userInfoDTO.userRegFormData
              .predefinedFieldsFormDataForHubspotPushCase["country"]
              ?.dataToEnter,
          company:
            userInfoDTO.userRegFormData
              .predefinedFieldsFormDataForHubspotPushCase["company"]
              ?.dataToEnter,
          mobilephone:
            userInfoDTO.userRegFormData
              .predefinedFieldsFormDataForHubspotPushCase["phoneNumber"]
              ?.dataToEnter,
        },
        userRecordFromTray
      );
    });
  });

  test("TC006: Export: Verify deletion of fields", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-650/export-verify-deletion-of-fields",
    });

    await test.step(`Add pre-defined fields for an event`, async () => {
      const customFieldsData =
        hubspotRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["registration-form-data-with-only-predefined-fields"];
      await eventController.addCustomFieldsWithConditionToEvent(
        customFieldsData
      );
    });

    await test.step(`Enable hubspot integration for the event ${eventId}`, async () => {
      await hubspotController.enableHubspotIntegrationForThisEvent({
        integrationType: integrationType.HUBSPOT,
        status: true,
        integrationDirectionType: HubspotIntegrationDirectionType.PUSH,
        shouldSyncData: true,
      });
      console.log(`Successfully enabled hubspot Integration`);
    });

    await test.step(`Update mapping of zuddl fields with hubspot fields`, async () => {
      const customFieldMapping =
        hubspotRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customFieldMapping"];
      const customFieldPolicyMapping =
        hubspotRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customPolicyMapping"];

      await hubspotController.mapZuddlFieldsWithHubspotFields(
        {
          customFieldMapping: customFieldMapping,
          customPolicyMapping: customFieldPolicyMapping,
        },
        integrationType.HUBSPOT
      );
    });

    await test.step(`Now delete the custom field PhoneNumber via API`, async () => {
      const customFieldMapping = {
        country: "country",
        company: "company",
      };
      const customPolicyMapping = {
        country: "DO_NOT_UPDATE",
        company: "DO_NOT_UPDATE",
      };
      await hubspotController.mapZuddlFieldsWithHubspotFields(
        {
          customFieldMapping: customFieldMapping,
          customPolicyMapping: customPolicyMapping,
        },
        integrationType.HUBSPOT
      );
    });

    await test.step(`Verify PhoneNumber custom field is not mapped in hubspot via API`, async () => {
      expect(
        await hubspotController.verifyFieldPresentInCustomFieldMapping(
          "phoneNumber",
          integrationType.HUBSPOT
        )
      ).toBeFalsy();
      console.log("PhoneNumber removed successfully");
    });
  });
});
