import {
  APIRequestContext,
  BrowserContext,
  expect,
  test,
} from "@playwright/test";
import BrowserFactory from "../../../util/BrowserFactory";
import { HubspotPage } from "../../../page-objects/integrations/hubspotPage";
import { EventController } from "../../../controller/EventController";
import { DataUtl } from "../../../util/dataUtil";
import { getCampainIDVerification } from "../../../util/apiUtil";

test.describe(`@integrations @hubspot`, async () => {
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiContext = orgBrowserContext.request;
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test("TC001: Verify e2e flow of install/uninstall app", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-639/verify-flow-of-installuninstall-app",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-639/verify-flow-of-installuninstall-app",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-640/verify-when-organiser-installs-the-app-the-uninstall-button-is-visible",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-642/verify-when-organiser-uninstalls-the-app-the-install-button-is-visible",
    });

    let orgPage = await orgBrowserContext.newPage();
    let hubspotPage = new HubspotPage(orgPage);
    await orgPage.setViewportSize({ width: 1400, height: 700 });
    await test.step(`Organiser opens the Hubspot View Details page`, async () => {
      await hubspotPage.goToDashboard();
      await hubspotPage.openIntegrationListPage();
      await hubspotPage.openViewDetailsPage();
    });

    await test.step(`Organiser verifies 'Uninstall app' button to be visible`, async () => {
      await hubspotPage.verifyPresenceOfButton("Uninstall app");
    });

    await test.step(`Organiser verifies 'Install app' button to be visible`, async () => {
      await hubspotPage.clickOnInstallOrUninstallAppButton();
      await hubspotPage.verifyPresenceOfButton("Install app");
    });

    await test.step(`Finally, Organiser installs the app`, async () => {
      await hubspotPage.clickOnInstallOrUninstallAppButton();
      hubspotPage = new HubspotPage(orgPage);
      await hubspotPage.clickOnNextButton();
      await hubspotPage.clickOnFinishButton();
      await orgPage.getByRole("button", { name: "Finish" }).click();
      await hubspotPage.verifyPresenceOfButton("Uninstall app");
    });
  });

  test("TC002: Verify organiser is able to edit the App", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-643/verify-organiser-is-able-to-edit-the-app",
    });
    let orgPage = await orgBrowserContext.newPage();
    let hubspotPage = new HubspotPage(orgPage);

    await test.step(`Organiser opens the Hubspot View Details page`, async () => {
      await hubspotPage.goToDashboard();
      await hubspotPage.openIntegrationListPage();
      await hubspotPage.openViewDetailsPage();
    });

    await test.step(`Organiser verifies 'Uninstall app' button to be visible`, async () => {
      await hubspotPage.verifyPresenceOfButton("Uninstall app");
    });

    await test.step(`Organiser now clicks on Edit Button`, async () => {
      await hubspotPage.clickOnEditButton();
      hubspotPage = new HubspotPage(orgPage);
    });

    await test.step(`Organiser verifies Authentication name`, async () => {
      await hubspotPage.verifyAccountName(`Automation Org's HubSpot account`);
    });
  });

  test("TC003: Verify enable/disable flow of export and import data toggle button", async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-644/verify-enabledisable-flow-of-export-data-toggle-button",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-645/verify-enabledisable-flow-of-import-data-toggle-button",
    });
    let eventTitle: string;
    let eventId: any;
    let pushURL: string;
    let eventController: EventController;
    let orgPage = await orgBrowserContext.newPage();
    let hubspotPage = new HubspotPage(orgPage);

    await test.step(`Create a new event`, async () => {
      eventTitle = DataUtl.getRandomEventTitle();
      eventId = await EventController.generateNewEvent(orgApiContext, {
        event_title: eventTitle,
      });
      eventController = new EventController(orgApiContext, eventId);
      console.log(
        `Event created succesfully with eventTitle->${eventTitle} and eventId->${eventId}`
      );
    });

    await test.step(`Organiser goes to the event integration page and enable the hubspot integration`, async () => {
      await hubspotPage.goToDashboard();
      await hubspotPage.openEventIntegrationPage(eventId);
      await hubspotPage.enableToggle();
    });

    await test.step(`Organiser verifies enabled-status to be true via API`, async () => {
      pushURL = `${
        DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
      }/api/integration/${eventId}/integration-setting/PUSH`;
      await orgPage.waitForTimeout(2000);
      let pushData = await getCampainIDVerification(orgApiContext, pushURL);
      expect(await pushData[0].status).toBeTruthy();
    });

    await test.step(`Organiser now disables the toggle`, async () => {
      await hubspotPage.disableToggle();
      await orgPage.waitForTimeout(2000);
      let pushData = await getCampainIDVerification(orgApiContext, pushURL);
      expect(await pushData[0].status).toBeFalsy();
    });
  });
});
