import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { DataUtl } from "../../../util/dataUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { IntegrationController } from "../../../controller/IntegrationController";
import integrationType from "../../../enums/integrationTYpeEnum";
import { fieldMappingPayLoad } from "../../../interfaces/IntegrationControllerInterface";
import { HubspotIntegrationDirectionType } from "../../../enums/SalesforceIntegrationDirectionEnum";
import { IntegrationValidationUtil } from "../../../util/IntegrationValidationUtil";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventRole from "../../../enums/eventRoleEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import { HubspotPage } from "../../../page-objects/integrations/hubspotPage";
import { HubspotIntegrationController } from "../../../controller/HubspotIntegrationController";
import hubspotRegistrationMappingTestdata from "../../../test-data/hubspotintegration-testdata/registration-fields-n-mapping-data.json";

test.describe("@integration @hubspot @export @fieldmapping", async () => {
  let eventId;
  let eventTitle: string;
  let eventController: EventController;
  let orgBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  let hubspotIntegrationController: HubspotIntegrationController;
  let dashboardUrl: string;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiRequestContext = orgBrowserContext.request;
    dashboardUrl = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/dashboard`;
  });

  test.afterEach(async () => {
    //get field list, if it contains more than passed in payload, remove it.
    let integrationController = new IntegrationController(orgApiRequestContext);
    let payload: fieldMappingPayLoad = {
      organizationFieldMapping: {
        lastName: "lastname",
        firstName: "firstname",
        email: "email",
      },
      organizationPolicyMapping: {
        lastname: "ALWAYS_UPDATE",
        firstname: "ALWAYS_UPDATE",
        email: "DO_NOT_UPDATE",
      },
    };
    await test.step(`Now, add as the pre-defined field at org level via API`, async () => {
      await integrationController.addOrganisationMappingFields(
        integrationType.HUBSPOT,
        payload
      );
    });
    await orgBrowserContext?.close();
  });

  test(`TC001: To verify FirstName, LastName, and Email are the default fields present when app is installed`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-757/to-verify-email-is-the-default-fields-present-when-app-is-installed",
    });
    let orgPage = await orgBrowserContext.newPage();
    let hubspotPage = new HubspotPage(orgPage);
    orgPage.setViewportSize({ width: 1400, height: 700 });

    await test.step(`Open hubspot Integration and go to mapping field page`, async () => {
      await hubspotPage.IntegrateApp(dashboardUrl, integrationType.HUBSPOT);
    });

    await test.step(`Verify FirstName, LastName, and Email as the default field being present`, async () => {
      await hubspotPage.verifyPresenceOfDefaultFields();
    });
  });

  test(`TC002: To verify user is able to add and delete new field when app is installed`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-758/to-verify-user-is-able-to-add-and-delete-new-field-when-app-is",
    });
    let orgPage = await orgBrowserContext.newPage();
    let hubspotPage = new HubspotPage(orgPage);
    orgPage.setViewportSize({ width: 1400, height: 700 });
    let integrationController = new IntegrationController(orgApiRequestContext);
    let payload: fieldMappingPayLoad = {
      organizationFieldMapping: {
        firstName: "FirstName",
        lastName: "LastName",
        email: "Email",
        company: "Company",
      },
      organizationPolicyMapping: {
        FirstName: "ALWAYS_UPDATE",
        LastName: "ALWAYS_UPDATE",
        Email: "DO_NOT_UPDATE",
        Company: "ALWAYS_UPDATE",
      },
    };
    const deleteField = ["company"];

    await test.step(`Open hubspot Integration and go to mapping field page`, async () => {
      await hubspotPage.IntegrateApp(dashboardUrl, integrationType.HUBSPOT);
    });

    await test.step(`Now, add ${deleteField} as the pre-defined field at org level via API`, async () => {
      await integrationController.addOrganisationMappingFields(
        integrationType.HUBSPOT,
        payload
      );
    });

    await test.step(`Verify ${deleteField} field successfully gets added via API`, async () => {
      let response =
        await integrationController.getOrganizationFieldMappingObject(
          integrationType.HUBSPOT
        );
      expect(response).toEqual(payload.organizationFieldMapping);
    });
  });

  test(`TC003: Lead custom fields: Verify when pre-defined field added in both org and event level, data should get synced in hubspot succesfully`, async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-759/verify-when-pre-defined-field-added-in-both-org-and-event-level-data",
    });

    let integrationController = new IntegrationController(orgApiRequestContext);
    let payload: fieldMappingPayLoad = {
      organizationFieldMapping: {
        lastName: "lastname",
        firstName: "firstname",
        company: "company",
        email: "email",
      },
      organizationPolicyMapping: {
        lastname: "ALWAYS_UPDATE",
        firstname: "ALWAYS_UPDATE",
        company: "ALWAYS_UPDATE",
        email: "DO_NOT_UPDATE",
      },
    };
    const attendeeFirstName = "prateekAttendeeOne";
    const attendeeLastName = "QA";
    const attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let leadDataFromTray: any;

    await test.step(`Creating new event to test hubspot export integration`, async () => {
      eventTitle = DataUtl.getRandomEventTitle();
      eventId = await EventController.generateNewEvent(orgApiRequestContext, {
        event_title: eventTitle,
      });
      eventController = new EventController(orgApiRequestContext, eventId);
      hubspotIntegrationController = new HubspotIntegrationController(
        orgApiRequestContext,
        eventId
      );
    });

    await test.step(`Now, add Company as the pre-defined field at org level via API`, async () => {
      await integrationController.addOrganisationMappingFields(
        integrationType.HUBSPOT,
        payload
      );
    });

    await test.step(`Verify Company field successfully gets added at org level via API`, async () => {
      let response =
        await integrationController.getOrganizationFieldMappingObject(
          integrationType.HUBSPOT
        );
      expect(response).toEqual(payload.organizationFieldMapping);
    });

    await test.step(`Enable hubspot integration for the event ${eventId}`, async () => {
      await hubspotIntegrationController.enableHubspotIntegrationForThisEvent({
        integrationType: integrationType.HUBSPOT,
        status: true,
        integrationDirectionType: HubspotIntegrationDirectionType.PUSH,
        shouldSyncData: true,
      });
      console.log(`Successfully enabled hubspot Integration`);
    });

    await test.step(`Now Register a new user to the event via API`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeFirstName,
        lastName: attendeeLastName,
        email: attendeeEmail,
      });
    });

    await test.step(`Fetch lead data from tray`, async () => {
      await page.waitForTimeout(2 * 60000);
      leadDataFromTray =
        await hubspotIntegrationController.getHubspotRegisterUserDataFromTray(
          attendeeEmail
        );
    });

    await test.step(`Verifying the lead record fields recieved from tray`, async () => {
      console.log(JSON.stringify(leadDataFromTray));
      IntegrationValidationUtil.validateHubspotDataFetchedFromTray(
        {
          firstname: attendeeFirstName,
          lastname: attendeeLastName,
          email: attendeeEmail,
        },
        leadDataFromTray
      );
    });
  });

  test(`TC004: Lead custom fields: Verify existing user's pre-defined field should get updated in hubspot succesfully`, async ({
    page,
  }) => {
    /*
        1. Add user in husbpot 
        2. In new event, update the same users data
        3. Verify in tray data should be updated
        */
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-760/verify-existing-users-pre-defined-field-should-get-updated-in-hubspot",
    });

    let integrationController = new IntegrationController(orgApiRequestContext);
    let payload: fieldMappingPayLoad = {
      organizationFieldMapping: {
        lastName: "lastname",
        firstName: "firstname",
        company: "company",
        email: "email",
      },
      organizationPolicyMapping: {
        lastname: "ALWAYS_UPDATE",
        firstname: "ALWAYS_UPDATE",
        company: "ALWAYS_UPDATE",
        email: "DO_NOT_UPDATE",
      },
    };
    const attendeeFirstName = "prateekAttendeeOne";
    const attendeeLastName = "QA";
    const updatedAttendeeFirstName = "prateekAttendeeOneUpdated";
    const updatedAttendeeLastName = "QAUpdated";
    const attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let leadDataFromTray: any;
    let eventId2: any;
    let eventTitle2: string;
    let eventController2: EventController;

    await test.step(`Create new event1`, async () => {
      eventTitle = DataUtl.getRandomEventTitle();
      eventId = await EventController.generateNewEvent(orgApiRequestContext, {
        event_title: eventTitle,
      });
      eventController = new EventController(orgApiRequestContext, eventId);
      hubspotIntegrationController = new HubspotIntegrationController(
        orgApiRequestContext,
        eventId
      );
    });

    await test.step(`Now, add Company as the pre-defined field at org level via API`, async () => {
      await integrationController.addOrganisationMappingFields(
        integrationType.HUBSPOT,
        payload
      );
    });

    await test.step(`Verify Company field successfully gets added at org level via API`, async () => {
      let response =
        await integrationController.getOrganizationFieldMappingObject(
          integrationType.HUBSPOT
        );
      expect(response).toEqual(payload.organizationFieldMapping);
    });

    await test.step(`Enable hubspot integration for the event ${eventId}`, async () => {
      await hubspotIntegrationController.enableHubspotIntegrationForThisEvent({
        integrationType: integrationType.HUBSPOT,
        status: true,
        integrationDirectionType: HubspotIntegrationDirectionType.PUSH,
        shouldSyncData: true,
      });
      console.log(`Successfully enabled hubspot Integration`);
    });

    await test.step(`Now Register a new user to the event via API`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeFirstName,
        lastName: attendeeLastName,
        email: attendeeEmail,
      });
    });

    await test.step(`Fetch lead data from tray`, async () => {
      await page.waitForTimeout(2 * 60000);
      leadDataFromTray =
        await hubspotIntegrationController.getHubspotRegisterUserDataFromTray(
          attendeeEmail
        );
    });

    await test.step(`Verifying the lead record fields recieved from tray`, async () => {
      console.log(JSON.stringify(leadDataFromTray));
      IntegrationValidationUtil.validateHubspotDataFetchedFromTray(
        {
          firstname: attendeeFirstName,
          lastname: attendeeLastName,
          email: attendeeEmail,
        },
        leadDataFromTray
      );
    });

    await test.step(`Now, create another event2`, async () => {
      eventTitle2 = DataUtl.getRandomEventTitle();
      eventId2 = await EventController.generateNewEvent(orgApiRequestContext, {
        event_title: eventTitle2,
      });
      eventController2 = new EventController(orgApiRequestContext, eventId2);
    });

    await test.step(`Enable Hubspot Integration`, async () => {
      hubspotIntegrationController = new HubspotIntegrationController(
        orgApiRequestContext,
        eventId2
      );

      hubspotIntegrationController.enableHubspotIntegrationForThisEvent({
        integrationType: integrationType.HUBSPOT,
        status: true,
        integrationDirectionType: HubspotIntegrationDirectionType.PUSH,
        shouldSyncData: true,
      });
    });

    await test.step(`Now, Register the same existing user of hubspot ${attendeeEmail} to this new event via API`, async () => {
      await eventController2.registerUserToEventWithRegistrationApi({
        firstName: updatedAttendeeFirstName,
        lastName: updatedAttendeeLastName,
        email: attendeeEmail,
      });
    });

    await test.step(`Fetch lead data from tray`, async () => {
      await page.waitForTimeout(60000);
      leadDataFromTray =
        await hubspotIntegrationController.getHubspotRegisterUserDataFromTray(
          attendeeEmail
        );
    });

    await test.step(`Verifying the lead record fields recieved from tray has updated details`, async () => {
      console.log(JSON.stringify(leadDataFromTray));
      IntegrationValidationUtil.validateHubspotDataFetchedFromTray(
        {
          firstname: updatedAttendeeFirstName,
          lastname: updatedAttendeeLastName,
          email: attendeeEmail,
        },
        leadDataFromTray
      );
    });
  });

  test(`TC005: Lead custom fields: Verify when custom field is added in an event level, data should get synced in hubspot succesfully, also verify deletion of custom field`, async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-761/verify-when-custom-field-is-added-in-an-event-level-data-should-get",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-762/verify-deletion-of-lead-custom-fields-via-ui",
    });
    /*
        1. Create event with custom field
        2. Map custom field with any pre-defined field
        3. enable integration
        4. Register user to event
        5. Verify tray data
        */
    let integrationController = new IntegrationController(orgApiRequestContext);
    let hubspotController: HubspotIntegrationController;
    let payload: fieldMappingPayLoad = {
      organizationFieldMapping: {
        lastName: "lastname",
        firstName: "firstname",
        company: "company",
        email: "email",
      },
      organizationPolicyMapping: {
        lastname: "ALWAYS_UPDATE",
        firstname: "ALWAYS_UPDATE",
        company: "ALWAYS_UPDATE",
        email: "DO_NOT_UPDATE",
      },
    };
    const attendeeFirstName = "PrateekAttendeeOne";
    const attendeeLastName = "AutomationQA";
    const attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let leadDataFromTray: any;
    let userInfoDTO: UserInfoDTO;
    let userEventRoleDto: UserEventRoleDto;
    let eventInfoDto: EventInfoDTO = new EventInfoDTO(eventId, "", false);
    const fieldToDeleteList = ["firstName", "lastName", "company"];

    await test.step(`Now, add as the pre-defined field at org level via API`, async () => {
      await integrationController.addOrganisationMappingFields(
        integrationType.HUBSPOT,
        payload
      );
    });
    await test.step(`Create new event`, async () => {
      eventTitle = DataUtl.getRandomEventTitle();
      eventId = await EventController.generateNewEvent(orgApiRequestContext, {
        event_title: eventTitle,
      });
      eventController = new EventController(orgApiRequestContext, eventId);
      hubspotController = new HubspotIntegrationController(
        orgApiRequestContext,
        eventId
      );
    });

    await test.step(`Add custom fields for an event`, async () => {
      const customFieldsData =
        hubspotRegistrationMappingTestdata[
          "TC2_PUSH_ZUDDL_PREDEFINED_N_CUSTOMFIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["registrationCustomFields"];
      await eventController.addCustomFieldsWithConditionToEvent(
        customFieldsData
      );
    });

    await test.step(`Map Custom field to Pre-defined field in event Integration`, async () => {
      //mapping custom field 'myfield' to pre-defined field 'company'
      const customFieldMapping =
        hubspotRegistrationMappingTestdata[
          "TC2_PUSH_ZUDDL_PREDEFINED_N_CUSTOMFIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customFieldMapping"];
      const customFieldPolicyMapping =
        hubspotRegistrationMappingTestdata[
          "TC2_PUSH_ZUDDL_PREDEFINED_N_CUSTOMFIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customPolicyMapping"];

      await test.step(`Enable hubspot integration for the event ${eventId}`, async () => {
        await hubspotController.enableHubspotIntegrationForThisEvent({
          integrationType: integrationType.HUBSPOT,
          status: true,
          integrationDirectionType: HubspotIntegrationDirectionType.PUSH,
          shouldSyncData: true,
        });
        console.log(`Successfully enabled hubspot Integration`);
      });

      await hubspotController.mapZuddlFieldsWithHubspotFields(
        {
          customFieldMapping: customFieldMapping,
          customPolicyMapping: customFieldPolicyMapping,
        },
        integrationType.HUBSPOT
      );
    });

    await test.step(`Register a new user to event`, async () => {
      let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
        .attendeeLandingPage;
      userInfoDTO = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        true
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDTO,
        eventInfoDto,
        EventRole.NOTEXISTS,
        false
      );
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendeeLandingPage);
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData
          .predefinedAndCustomFieldsFormDataForSalesforcePushCase,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
        }
      );
    });

    await test.step(`Fetch lead data from tray`, async () => {
      await page.waitForTimeout(2 * 60000);
      leadDataFromTray =
        await hubspotController.getHubspotRegisterUserDataFromTray(
          attendeeEmail
        );
    });

    await test.step(`Verifying the lead record fields recieved from tray has updated details`, async () => {
      console.log(JSON.stringify(leadDataFromTray));
      IntegrationValidationUtil.validateCustomLeadHubspotFieldsFetchedFromTray(
        {
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          email: attendeeEmail,
          company: "custom field",
        },
        leadDataFromTray
      );
    });

    await test.step(`Verify custom field gets succesfully deleted from event via UI`, async () => {
      let orgPage = await orgBrowserContext.newPage();
      let integrationPage = new HubspotPage(orgPage);
      const fieldDelete = "MyField (string)";
      let integrationPageURL = `${
        DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
      }/event/${eventId}/event-integration`;
      await orgPage.goto(integrationPageURL);
      console.log("here");
      await integrationPage.openCustomFieldTab();
      await integrationPage.verifyFieldToBeDeleted(fieldDelete);
    });
  });
});
