import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { DataUtl } from "../../../util/dataUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { SalesforceEventIntegrationController } from "../../../controller/SalesforceIntegrationController";
import { registerUserToEventbyApi } from "../../../util/apiUtil";
import { SalesforceIntegrationDirectionType } from "../../../enums/SalesforceIntegrationDirectionEnum";
import salesforceRegistrationMappingTestdata from "../../../test-data/salesforceintegration-testdata/registration-fields-n-mapping-data.json";
import { SalesforcePage } from "../../../page-objects/integrations/salesforcePage";
import { QueryUtil } from "../../../DB/QueryUtil";

test.describe("@integration @salesforce @import", async () => {
  let eventId;
  let eventTitle;
  let eventController: EventController;
  let orgBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  let salesforceIntegrationController: SalesforceEventIntegrationController;
  let integrationPageURL: string;
  let page: Page;

  test.beforeEach(async () => {
    test.setTimeout(6 * 80000);
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiRequestContext = orgBrowserContext.request;
    page = await orgBrowserContext.newPage();

    await test.step(`Creating new event to test salesforce export integration`, async () => {
      eventTitle = DataUtl.getRandomEventTitle();
      eventId = await EventController.generateNewEvent(orgApiRequestContext, {
        event_title: eventTitle,
      });
      eventController = new EventController(orgApiRequestContext, eventId);
    });

    await test.step(`initialising the salesforce integration controller`, async () => {
      salesforceIntegrationController =
        new SalesforceEventIntegrationController(orgApiRequestContext, eventId);
    });
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`TC001: Verify we are able to pull registration records from a campaign to zuddl registration records `, async () => {
    // linear ticket info
    let eventIdToPullRegData;
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-123/verify-new-user-if-registers-his-data-should-be-properly-sent-to",
    });
    const campaignId = "7015j0000011XlbAAE";
    let listOfRegisteredRecordFromTray: [JSON];
    let isUserFoundInTray = false;
    let userRecordFromTray: JSON;
    const listOfNewRandomAttendeeEmail =
      DataUtl.getListOfRandomAttendeeEmail(5);

    await test.step(`Enable Campaign for PUSH integration for event ${eventId}`, async () => {
      await salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId,
        SalesforceIntegrationDirectionType.PUSH
      );
    });

    await test.step(`Register ${listOfNewRandomAttendeeEmail.length} users to this event ${eventId} so that this data get pushed to salesforce campaign`, async () => {
      for (let eachAttendeeEmail of listOfNewRandomAttendeeEmail) {
        await test.step(`Register user with ${eachAttendeeEmail} to event`, async () => {
          await registerUserToEventbyApi(
            orgApiRequestContext,
            eventId,
            eachAttendeeEmail
          );
        });
      }
    });

    await test.step(`Fetch data from tray for event ${eventId} to verify registration data is synced to tray for SF`, async () => {
      await page.waitForTimeout(2 * 60000);
      listOfRegisteredRecordFromTray =
        await salesforceIntegrationController.getRegisterUserDataFromTray(
          campaignId
        );
    });

    await test.step(`Verify each user from list of attendee got pushed to salesforce`, async () => {
      for (const eachAttendeeEmail of listOfNewRandomAttendeeEmail) {
        await test.step(`Verify the records fetched from Tray contains the attendee email registered for this event before enabling sync`, async () => {
          userRecordFromTray =
            await salesforceIntegrationController.verifyRecordsFromTray(
              listOfRegisteredRecordFromTray,
              eachAttendeeEmail,
              isUserFoundInTray,
              userRecordFromTray
            );
        });
      }
    });

    await test.step(`Now create a new event to pull the registration data from campaign ${campaignId} which was created for previous event`, async () => {
      eventIdToPullRegData = await EventController.generateNewEvent(
        orgApiRequestContext,
        {
          event_title: DataUtl.getRandomEventTitle(),
        }
      );
      console.log(`event title->` + eventTitle);
    });

    await test.step(`Enable PULL Integration for event : ${eventIdToPullRegData} to fetch data from campaign`, async () => {
      await new SalesforceEventIntegrationController(
        orgApiRequestContext,
        eventIdToPullRegData
      ).enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId,
        SalesforceIntegrationDirectionType.PULL
      );
    });

    await test.step(`Wait for 2 min until data gets pulled to zuddl event reg table`, async () => {
      await page.waitForTimeout(2 * 60000);
    });

    await test.step(`Now verify in event registration table, registration records are pulled from previous event`, async () => {
      for (const eachAttendeeEmail of listOfNewRandomAttendeeEmail) {
        await test.step(`Verifying if registration record exists for email ${eachAttendeeEmail} for event ${eventIdToPullRegData}`, async () => {
          let registrationDataFromDb =
            await QueryUtil.fetchUserEventRegistrationDataFromDb(
              eventIdToPullRegData,
              eachAttendeeEmail
            );
          let registrationId = registrationDataFromDb["registration_id"];
          expect(
            registrationId,
            `expecting ${registrationId} to not be null`
          ).not.toBeNull();
        });
      }
    });
  });

  test(`TC002: Verify registration custom fields if mapped to salesforce lead fields are being synced correctly in pull`, async () => {
    // linear ticket info
    let eventIdToPullRegData;
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-126/import-lead-custom-field-mapping",
    });
    const campaignId = "7015j0000011XlbAAE";
    let listOfRegisteredRecordFromTray: [JSON];
    let isUserFoundInTray = false;
    let userRecordFromTray: JSON;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();

    await test.step(`Enable Campaign for PUSH integration for event ${eventId}`, async () => {
      await salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId,
        SalesforceIntegrationDirectionType.PUSH
      );
    });

    await test.step(`Register ${attendeeEmail} users to this event ${eventId} so that this data get pushed to salesforce campaign`, async () => {
      await test.step(`Register user with ${attendeeEmail} to event`, async () => {
        await registerUserToEventbyApi(
          orgApiRequestContext,
          eventId,
          attendeeEmail
        );
      });
    });

    await test.step(`Fetch data from tray for event ${eventId} to verify registration data is synced to tray for SF`, async () => {
      await page.waitForTimeout(2 * 60000);
      listOfRegisteredRecordFromTray =
        await salesforceIntegrationController.getRegisterUserDataFromTray(
          campaignId
        );
    });

    await test.step(`Verify each user from list of attendee got pushed to salesforce`, async () => {
      await test.step(`Verify the records fetched from Tray contains the attendee email registered for this event before enabling sync`, async () => {
        userRecordFromTray =
          await salesforceIntegrationController.verifyRecordsFromTray(
            listOfRegisteredRecordFromTray,
            attendeeEmail,
            isUserFoundInTray,
            userRecordFromTray
          );
      });
    });

    await test.step(`Now create a new event to pull the registration data from campaign ${campaignId} which was created for previous event`, async () => {
      eventIdToPullRegData = await EventController.generateNewEvent(
        orgApiRequestContext,
        {
          event_title: DataUtl.getRandomEventTitle(),
        }
      );
    });

    await test.step(`Add pre-defined fields for an event`, async () => {
      eventController = new EventController(
        orgApiRequestContext,
        eventIdToPullRegData
      );
      salesforceIntegrationController =
        new SalesforceEventIntegrationController(
          orgApiRequestContext,
          eventIdToPullRegData
        );
      const customFieldsData =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["registration-form-data-with-only-predefined-fields"];
      await eventController.addCustomFieldsWithConditionToEvent(
        customFieldsData
      );
    });

    await test.step(`Update mapping of zuddl fields with salesforce fields`, async () => {
      const customFieldMapping =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customFieldMapping"];
      const customFieldPolicyMapping =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customPolicyMapping"];

      await salesforceIntegrationController.mapZuddlFieldsWithSalesforceFields({
        customFieldMapping: customFieldMapping,
        customPolicyMapping: customFieldPolicyMapping,
      });
    });

    await test.step(`Create new campaign for salesforce integration for this event`, async () => {
      await salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId,
        SalesforceIntegrationDirectionType.PULL
      );
    });

    await test.step(`Wait for 2 min until data gets pulled to zuddl event reg table`, async () => {
      await page.waitForTimeout(2 * 60000);
    });

    await test.step(`Now verify in event registration table, registration records are pulled from previous event`, async () => {
      await test.step(`Verifying if registration record exists for email ${attendeeEmail} for event ${eventIdToPullRegData}`, async () => {
        let registrationDataFromDb =
          await QueryUtil.fetchUserEventRegistrationDataFromDb(
            eventIdToPullRegData,
            attendeeEmail
          );
        let registrationId = registrationDataFromDb["registration_id"];
        expect(
          registrationId,
          `expecting ${registrationId} to not be null`
        ).not.toBeNull();
      });
    });
  });

  test(`TC004: Verify registration custom fields if mapped to salesforce contact fields are being synced correctly in pull`, async () => {
    // linear ticket info
    let eventIdToPullRegData;
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-127/import-contact-custom-field-mapping",
    });
    const campaignId = "7015j0000011XlbAAE";
    let listOfRegisteredRecordFromTray: [JSON];
    let isUserFoundInTray = false;
    let userRecordFromTray: JSON;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();

    await test.step(`Enable Campaign for PUSH integration for event ${eventId}`, async () => {
      await salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId,
        SalesforceIntegrationDirectionType.PUSH
      );
    });

    await test.step(`Register ${attendeeEmail} users to this event ${eventId} so that this data get pushed to salesforce campaign`, async () => {
      await test.step(`Register user with ${attendeeEmail} to event`, async () => {
        await registerUserToEventbyApi(
          orgApiRequestContext,
          eventId,
          attendeeEmail
        );
      });
    });

    await test.step(`Fetch data from tray for event ${eventId} to verify registration data is synced to tray for SF`, async () => {
      await page.waitForTimeout(2 * 60000);
      listOfRegisteredRecordFromTray =
        await salesforceIntegrationController.getRegisterUserDataFromTray(
          campaignId
        );
    });

    await test.step(`Verify each user from list of attendee got pushed to salesforce`, async () => {
      await test.step(`Verify the records fetched from Tray contains the attendee email registered for this event before enabling sync`, async () => {
        userRecordFromTray =
          await salesforceIntegrationController.verifyRecordsFromTray(
            listOfRegisteredRecordFromTray,
            attendeeEmail,
            isUserFoundInTray,
            userRecordFromTray
          );
      });
    });

    await test.step(`Now create a new event to pull the registration data from campaign ${campaignId} which was created for previous event`, async () => {
      eventIdToPullRegData = await EventController.generateNewEvent(
        orgApiRequestContext,
        {
          event_title: DataUtl.getRandomEventTitle(),
        }
      );
    });

    await test.step(`Add pre-defined fields for an event`, async () => {
      eventController = new EventController(
        orgApiRequestContext,
        eventIdToPullRegData
      );
      salesforceIntegrationController =
        new SalesforceEventIntegrationController(
          orgApiRequestContext,
          eventIdToPullRegData
        );
      const customFieldsData =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ][
          "registration-form-data-with-only-predefined-fields-secondaryCustomFieldMapping"
        ];
      await eventController.addCustomFieldsWithConditionToEvent(
        customFieldsData
      );
    });

    await test.step(`Update mapping of zuddl fields with salesforce fields`, async () => {
      const secondaryCustomFieldMapping =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["secondaryCustomFieldMapping"];
      const secondaryCustomPolicyMapping =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["secondaryCustomPolicyMapping"];

      await salesforceIntegrationController.mapZuddlFieldsWithSalesforceFields({
        secondaryCustomFieldMapping: secondaryCustomFieldMapping,
        secondaryCustomPolicyMapping: secondaryCustomPolicyMapping,
      });
    });

    await test.step(`Create new campaign for salesforce integration for this event`, async () => {
      await salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId,
        SalesforceIntegrationDirectionType.PULL
      );
    });

    await test.step(`Wait for 2 min until data gets pulled to zuddl event reg table`, async () => {
      await page.waitForTimeout(2 * 60000);
    });

    await test.step(`Now verify in event registration table, registration records are pulled from previous event`, async () => {
      await test.step(`Verifying if registration record exists for email ${attendeeEmail} for event ${eventIdToPullRegData}`, async () => {
        let registrationDataFromDb =
          await QueryUtil.fetchUserEventRegistrationDataFromDb(
            eventIdToPullRegData,
            attendeeEmail
          );
        let registrationId = registrationDataFromDb["registration_id"];
        expect(
          registrationId,
          `expecting ${registrationId} to not be null`
        ).not.toBeNull();
      });
    });
  });

  test(`TC005: Contact Custom field: deletion of fields in pull`, async () => {
    // linear ticket info
    let eventIdToPullRegData;
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-579/import-contact-custom-field-deletion-of-fields",
    });
    const campaignId = "7015j0000011XlbAAE";
    let listOfRegisteredRecordFromTray: [JSON];
    let isUserFoundInTray = false;
    let userRecordFromTray: JSON;
    let attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let integrationPage = new SalesforcePage(page);

    await test.step(`Enable Campaign for PUSH integration for event ${eventId}`, async () => {
      await salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId,
        SalesforceIntegrationDirectionType.PUSH
      );
    });

    await test.step(`Register ${attendeeEmail} users to this event ${eventId} so that this data get pushed to salesforce campaign`, async () => {
      await test.step(`Register user with ${attendeeEmail} to event`, async () => {
        await registerUserToEventbyApi(
          orgApiRequestContext,
          eventId,
          attendeeEmail
        );
      });
    });

    await test.step(`Fetch data from tray for event ${eventId} to verify registration data is synced to tray for SF`, async () => {
      await page.waitForTimeout(2 * 60000);
      listOfRegisteredRecordFromTray =
        await salesforceIntegrationController.getRegisterUserDataFromTray(
          campaignId
        );
    });

    await test.step(`Verify each user from list of attendee got pushed to salesforce`, async () => {
      await test.step(`Verify the records fetched from Tray contains the attendee email registered for this event before enabling sync`, async () => {
        userRecordFromTray =
          await salesforceIntegrationController.verifyRecordsFromTray(
            listOfRegisteredRecordFromTray,
            attendeeEmail,
            isUserFoundInTray,
            userRecordFromTray
          );
      });
    });

    await test.step(`Now create a new event to pull the registration data from campaign ${campaignId} which was created for previous event`, async () => {
      eventIdToPullRegData = await EventController.generateNewEvent(
        orgApiRequestContext,
        {
          event_title: DataUtl.getRandomEventTitle(),
        }
      );
    });

    await test.step(`Add pre-defined fields for an event`, async () => {
      eventController = new EventController(
        orgApiRequestContext,
        eventIdToPullRegData
      );
      salesforceIntegrationController =
        new SalesforceEventIntegrationController(
          orgApiRequestContext,
          eventIdToPullRegData
        );
      const customFieldsData =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ][
          "registration-form-data-with-only-predefined-fields-secondaryCustomFieldMapping"
        ];
      await eventController.addCustomFieldsWithConditionToEvent(
        customFieldsData
      );
    });

    await test.step(`Update mapping of zuddl fields with salesforce fields`, async () => {
      const secondaryCustomFieldMapping =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["secondaryCustomFieldMapping"];
      const secondaryCustomPolicyMapping =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["secondaryCustomPolicyMapping"];

      await salesforceIntegrationController.mapZuddlFieldsWithSalesforceFields({
        secondaryCustomFieldMapping: secondaryCustomFieldMapping,
        secondaryCustomPolicyMapping: secondaryCustomPolicyMapping,
      });
    });

    await test.step(`Create new campaign for salesforce integration for this event`, async () => {
      await salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId,
        SalesforceIntegrationDirectionType.PULL
      );
    });

    await test.step("Verify Custom Field can be deleted via UI", async () => {
      integrationPageURL = `${
        DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
      }/event/${eventIdToPullRegData}/event-integration`;
      await integrationPage.load(integrationPageURL);
      await integrationPage.gotoLeadCustomFieldTabForPull(2);
      await page.waitForTimeout(5000);
      await integrationPage.deleteOneCustomFieldForPull();
      await salesforceIntegrationController.verifyCustomFieldDeletion(2);
    });
  });
});
