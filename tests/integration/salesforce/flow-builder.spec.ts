import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { DataUtl } from "../../../util/dataUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { SalesforceEventIntegrationController } from "../../../controller/SalesforceIntegrationController";
import { IntegrationController } from "../../../controller/IntegrationController";
import integrationType from "../../../enums/integrationTYpeEnum";
import { fieldMappingPayLoad } from "../../../interfaces/IntegrationControllerInterface";
import { SalesforceIntegrationDirectionType } from "../../../enums/SalesforceIntegrationDirectionEnum";
import { IntegrationValidationUtil } from "../../../util/IntegrationValidationUtil";
import salesforceRegistrationMappingTestdata from "../../../test-data/salesforceintegration-testdata/registration-fields-n-mapping-data.json";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventRole from "../../../enums/eventRoleEnum";
import {
  createNewEvent,
  updateEventLandingPageDetails,
  updateEventType,
} from "../../../util/apiUtil";
import EventType from "../../../enums/eventTypeEnum";
import { enableDisableEmailTrigger } from "../../../util/email-validation-api-util";
import EventSettingID from "../../../enums/EventSettingEnum";
import { FlowBuilderPage } from "../../../page-objects/flow-builder/flow-ticketing";
import { FlowBuilderController } from "../../../controller/FlowBuilderController";
import { publishDataPayload } from "../../../interfaces/FlowBuilerControllerInterface";
import { FlowStatus, FlowType } from "../../../enums/FlowTypeEnum";
test.describe("@integration @salesforce @flowbuilder", async () => {
  let eventController: EventController;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let salesforceIntegrationController: SalesforceEventIntegrationController;
  let eventId: any;
  let attendeeBrowserContext: BrowserContext;
  let flowBuilderPage: FlowBuilderPage;
  let flowBuilderController: FlowBuilderController;
  let publishdataPayLoad: publishDataPayload;
  let attendeePage: Page;

  let eventInfoDto: EventInfoDTO;

  test.beforeAll(async () => {
    await test.step(`setup browser contexts`, async () => {
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: true,
      });
      orgApiContext = orgBrowserContext.request;
      attendeeBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: false,
      });
      attendeePage = await attendeeBrowserContext.newPage();
    });

    await test.step(`create an event with FLEX as resgistration type`, async () => {
      eventId = await createNewEvent({
        api_request_context: orgApiContext,
        event_title: "flow-without-ticket automation",
        isFlex: true,
      });
      console.log(`event id ${eventId}`);
    });

    await test.step(`update an event with HYBRID as event type`, async () => {
      await updateEventType(orgApiContext, eventId, EventType.HYBRID);
      await updateEventLandingPageDetails(orgApiContext, eventId, {
        isMagicLinkEnabled: true,
      });
    });

    await test.step(`Initialise event controller`, async () => {
      eventController = new EventController(orgApiContext, eventId);
    });

    await test.step(`Get event details`, async () => {
      let eventInfo = await (await eventController.getEventInfo()).json();
      console.log("Event Info: ", eventInfo);
      eventInfoDto = new EventInfoDTO(eventInfo.eventId, eventInfo.title);
      eventInfoDto.startDateTime = eventInfo.startDateTime;
    });

    await enableDisableEmailTrigger(
      orgApiContext,
      eventId,
      EventSettingID.EmailOnRegistration,
      true,
      EventRole.ATTENDEE,
      true
    );

    await test.step(`Initialise flowbuilder controller`, async () => {
      flowBuilderController = new FlowBuilderController(orgApiContext, eventId);
    });

    await test.step(`make publish flow test data`, async () => {
      let flowDraftId = await flowBuilderController.getFlowId(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
      publishdataPayLoad = {
        flowId: "",
        flowDraftId: flowDraftId,
        clientFlowDraftVersion: 2,
      };
    });

    await test.step(`update default flow steps with existing expected steps`, async () => {
      await flowBuilderController.generateFlowWithIntegration(
        FlowType.REGISTRATION,
        FlowStatus.DRAFT
      );
    });

    await test.step(`now, publish the flow`, async () => {
      await flowBuilderController.publishFlowData(publishdataPayLoad);
      await attendeePage.waitForTimeout(5000);
    });

    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
  });

  test.beforeEach(async () => {
    await test.step(`set content to publish site`, async () => {
      await attendeePage.goto("https://example.com");
      await attendeePage.setContent(
        await flowBuilderController.generateEmbedHtml()
      );
      flowBuilderPage = new FlowBuilderPage(attendeePage);
    });
    await test.step(`Initialising the salesforce integration controller`, async () => {
      salesforceIntegrationController =
        new SalesforceEventIntegrationController(orgApiContext, eventId);
    });
  });

  test.afterEach(async () => {
    //get field list, if it contains more than passed in payload, remove it.
    let integrationController = new IntegrationController(orgApiContext);
    let payload: fieldMappingPayLoad = {
      organizationFieldMapping: {
        firstName: "FirstName",
        lastName: "LastName",
        email: "Email",
      },
      organizationPolicyMapping: {
        FirstName: "ALWAYS_UPDATE",
        LastName: "ALWAYS_UPDATE",
        Email: "DO_NOT_UPDATE",
      },
    };
    await test.step(`Now, add as the pre-defined field at org level via API`, async () => {
      await integrationController.addOrganisationMappingFields(
        integrationType.SALESFORCE,
        payload
      );
    });
    await attendeePage.reload();
    // clear the cookies
    await attendeePage.context().clearCookies();
    await attendeePage.evaluate(() => {
      localStorage.clear();
    });
  });

  test.afterAll(async () => {
    await orgBrowserContext?.close();
    await attendeeBrowserContext?.close();
  });

  test(`TC001: Lead custom fields: Verify when custom field is added in an event level, data should get synced in salesforce succesfully: FLEX event`, async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-770/flow-builder-in-integration",
    });

    /*
          1. Create event with custom field
          2. Map custom field with any pre-defined field
          3. enable integration
          4. Register user to event
          5. Verify tray data
          */
    let xpage = await orgBrowserContext.newPage();
    let integrationController = new IntegrationController(orgApiContext);
    let salesForceController: SalesforceEventIntegrationController;
    let payload: fieldMappingPayLoad = {
      organizationFieldMapping: {
        firstName: "FirstName",
        lastName: "LastName",
        company: "Company",
        email: "Email",
      },
      organizationPolicyMapping: {
        FirstName: "ALWAYS_UPDATE",
        LastName: "ALWAYS_UPDATE",
        Company: "ALWAYS_UPDATE",
        Email: "DO_NOT_UPDATE",
      },
    };
    const campaignId1 = "7015j0000011XlbAAE";
    const attendeeFirstName = "Jxtin";
    const attendeeLastName = "zuddl";
    const attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let leadDataFromTray: any;
    await test.step(`Initialise salesforce controller`, async () => {
      salesForceController = new SalesforceEventIntegrationController(
        orgApiContext,
        eventId
      );
    });

    await test.step(`Now, add FirstName, LastName, and Email as the pre-defined field at org level via API`, async () => {
      await integrationController.addOrganisationMappingFields(
        integrationType.SALESFORCE,
        payload
      );
    });

    await test.step(`Verify FirstName, LastName, and Email field successfully gets added at org level via API`, async () => {
      let response =
        await integrationController.getOrganizationFieldMappingObject(
          integrationType.SALESFORCE
        );
      expect(response).toEqual(payload.organizationFieldMapping);
    });
    await test.step(`Map Custom field to Pre-defined field in event Integration`, async () => {
      //mapping custom field 'myfield' to pre-defined field 'company'
      const customFieldMapping =
        salesforceRegistrationMappingTestdata[
          "TC2_PUSH_ZUDDL_PREDEFINED_N_CUSTOMFIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customFieldMapping"];
      const customFieldPolicyMapping =
        salesforceRegistrationMappingTestdata[
          "TC2_PUSH_ZUDDL_PREDEFINED_N_CUSTOMFIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customPolicyMapping"];

      await test.step(`Enable Salesforce Integration`, async () => {
        salesForceController.enableSalesforceIntegrationForThisEventWithCampaign(
          campaignId1,
          SalesforceIntegrationDirectionType.PUSH
        );
      });

      await salesForceController.mapZuddlFieldsWithSalesforceFields({
        customFieldMapping: customFieldMapping,
        customPolicyMapping: customFieldPolicyMapping,
      });
    });

    await test.step(`Fill the registration form`, async () => {
      await flowBuilderPage.fillRegistrationForm({
        userFirstName: attendeeFirstName,
        userLastName: attendeeLastName,
        expectedFormHeading: "Form - Mandatory-Integration",
        userEmail: attendeeEmail,
      });
    });

    await test.step(`Fetch lead data from tray`, async () => {
      await page.waitForTimeout(2 * 60000);
      leadDataFromTray =
        await salesForceController.getLeadDataRegisteredFromTray(attendeeEmail);
    });

    await test.step(`Verifying the lead record fields recieved from tray has updated details`, async () => {
      console.log(JSON.stringify(leadDataFromTray));
      IntegrationValidationUtil.validateCustomLeadFieldsFetchedFromTray(
        {
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          email: attendeeEmail,
          company: "my custom field text",
        },
        leadDataFromTray
      );
    });
  });
});
