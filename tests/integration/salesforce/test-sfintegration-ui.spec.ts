import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import {
  createNewEvent,
  getResponseFromIntegrationURL,
  updateStatusFromIntegration,
  getRegisterUserData,
  getMappingData,
  getSalesforceInstallationData,
  getCampainIDVerification,
} from "../../../util/apiUtil";
import { DataUtl } from "../../../util/dataUtil";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import BrowserFactory from "../../../util/BrowserFactory";
import { SalesforcePage } from "../../../page-objects/integrations/salesforcePage";

test.describe.parallel(`@integrations`, async () => {
  //test.describe.configure({ retries: 2 });
  let eventId: number;
  let event_title: string;
  let orgBrowserContext: BrowserContext;
  let orgApiContext: APIRequestContext;
  let dashboardUrl: string;
  let eventInfoDto: EventInfoDTO;
  let integrationPageUrl: string;
  let eventURL: string;
  let formURL: string;
  let integrationURL: string;
  let orgSideAttendePageURL: string;
  let attendeeLandingPage: string;
  let campainIDPushURL: string;
  let orgPage: Page;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiContext = orgBrowserContext.request;
    orgPage = await orgBrowserContext.newPage();
    orgPage.setViewportSize({ width: 1400, height: 700 });
    event_title = DataUtl.getRandomOrganisationName();
    eventId = await createNewEvent({
      api_request_context: orgApiContext,
      event_title,
    });
    eventURL = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/event/${eventId}/registration/list`;
    formURL = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/event/${eventId}/registration/form`;
    dashboardUrl = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/dashboard`;
    integrationPageUrl = await getResponseFromIntegrationURL(orgApiContext);
    integrationURL = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/event/${eventId}/event-integration`;
    orgSideAttendePageURL = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/event/${eventId}/people/attendees`;
    campainIDPushURL = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/api/integration/${eventId}/integration-setting/PUSH`;
    eventInfoDto = new EventInfoDTO(
      eventId,
      DataUtl.getRandomOrganisationName(),
      true
    );
    attendeeLandingPage = eventInfoDto.attendeeLandingPage;
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test("TC001: to verify organiser able to install and setup salesforce at org level and also at event level by adding campaign id ", async () => {
    /*
        going to integration page at org level and checking if organiser can install salesforce integation and setup autantication or not
        and then organiser will go inside the event , will go to registraion form page and add a custom field and check if field has added successfully or not
        and then will go to integration tab insid eent and enable saledforce, then will add a campaign id and save it, and move to lead and contact custom field mapping tab 
         */
    //let orgPage = await orgBrowserContext.newPage();
    let integrationPage = new SalesforcePage(orgPage);
    const campaignId = "7015j0000011XlbAAE";
    await test.step(`Salesforce installation and setup at org Level`, async () => {
      await integrationPage.salesForceInstallation(
        dashboardUrl,
        integrationPageUrl,
        orgApiContext
      );
    });
    //going to registration form page and adding one custom field and then checking the success massege if field saved or not

    await test.step("Adding fields to the add Field options", async () => {
      await integrationPage.load(formURL);
      await orgPage.waitForTimeout(2000);
      await integrationPage.clickOnRegPageContinueBtn();
      await orgPage.waitForTimeout(5000);
      await integrationPage.addFieldBtnClick();
      await orgPage.selectOption("#pre-defined", { label: "Bio" });
      await integrationPage.load(integrationURL);
    });

    await test.step("Adding campainId and Saving it", async () => {
      await integrationPage.pushDataLocatorClick();
      let campainId = '7015j0000011XlbAAE';
      await integrationPage.addingCampainIDAndSaving(campaignId);
    });

    await orgPage.reload();
    let attendeeContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    let attendePage = await attendeeContext.newPage();
    let integrationPage_new = new SalesforcePage(attendePage);
    await integrationPage_new.load(attendeeLandingPage);
    await integrationPage_new.registeringAttende(
      "namrataz",
      "tiwari",
      "namarta1233@gmail.com",
      "bio"
    );

    await integrationPage.load(orgSideAttendePageURL);
    await orgPage.waitForTimeout(90000);
    let trayData = await getRegisterUserData(orgApiContext);
    let verfyingData = await integrationPage.verifyTrayData(
      trayData,
      "namarta1233@gmail.com"
    );
    expect(
      verfyingData,
      "Expecting emailID to be present in tray data"
    ).toEqual(true);
    await integrationPage.load(dashboardUrl);
    await integrationPage.clickOnIntegrationButton();
    await integrationPage.selectIntegrationTab();
    let installationCheck = await integrationPage.getSalesforceInstallStatus();
    if (installationCheck === "Uninstall app") {
      await integrationPage.uninstallSalesforceOnBtnClick();
    }
  });

  test("TC002: to verify status of salesforce installation", async () => {
    /*
     */
    // let orgPage = await orgBrowserContext.newPage();
    let integrationPage = new SalesforcePage(orgPage);
    await test.step(`Salesforce installation and setup at org Level`, async () => {
      await integrationPage.salesForceInstallation(
        dashboardUrl,
        integrationPageUrl,
        orgApiContext
      );
    });
    let salesforceInstallationData = await getSalesforceInstallationData(
      orgApiContext
    );
    await test.step(`Salesforce status check`, async () => {
      let salesforceStatus =
        await integrationPage.getVerificationSalesforceStatus(
          salesforceInstallationData
        );
      await integrationPage.verifySalesforceInstallation(salesforceStatus);
    });
  });

  test("TC003: Edit function check ", async () => {
    let integrationPage = new SalesforcePage(orgPage);
    await test.step(`Salesforce installation and setup at org Level`, async () => {
      await integrationPage.salesForceInstallation(
        dashboardUrl,
        integrationPageUrl,
        orgApiContext
      );
    });
    await orgPage.getByText("Edit").click();
    let textCheck = await integrationPage.getSalesForceAccountText();
    expect(textCheck).toEqual("Automation Org's Salesforce account 1");
  });

  test("TC004: verify able to enter campaign id ", async () => {
    // let orgPage = await orgBrowserContext.newPage();
    let integrationPage = new SalesforcePage(orgPage);
    await test.step(`Salesforce installation and setup at org Level`, async () => {
      await integrationPage.salesForceInstallation(
        dashboardUrl,
        integrationPageUrl,
        orgApiContext
      );
    });
    //going to registration form page and adding one custom field and then checking the success massege if field saved or not
    await test.step("Adding fields to the add Field options", async () => {
      await integrationPage.load(formURL);
      await orgPage.waitForTimeout(2000);
      await integrationPage.clickOnRegPageContinueBtn();
      await orgPage.waitForTimeout(5000);
      await integrationPage.addFieldBtnClick();
      await orgPage.waitForTimeout(2000);
      await orgPage.selectOption("#pre-defined", { label: "Bio" });
      await integrationPage.load(integrationURL);
    });
    await test.step("Adding campainId and Saving it", async () => {
      await integrationPage.pushDataLocatorClick();
      let campainId = '7015j0000011XlbAAE';
      await integrationPage.addingCampainIDAndSaving(campainId);
    });
    await orgPage.waitForTimeout(2000);
    await test.step("CampainId Verification using API", async () => {
      let pushData = await getCampainIDVerification(
        orgApiContext,
        campainIDPushURL
      );
      let verifingStatus = await integrationPage.verifyCampainID(pushData);
      expect(verifingStatus, "Same ID").toEqual(true);
    });
  });

  test("TC005: verify disabling of integration from setup side from UI ", async () => {
    let integrationPage = new SalesforcePage(orgPage);
    await test.step(`Salesforce installation and setup at org Level`, async () => {
      await integrationPage.salesForceInstallation(
        dashboardUrl,
        integrationPageUrl,
        orgApiContext
      );
    });
    //going to registration form page and adding one custom field and then checking the success massege if field saved or not

    await test.step("Adding fields to the add Field options", async () => {
      await integrationPage.load(formURL);
      await orgPage.waitForTimeout(5000);
      await integrationPage.clickOnRegPageContinueBtn();
      await orgPage.waitForTimeout(3000);
      await integrationPage.addFieldBtnClick();
      await orgPage.waitForTimeout(5000);
      await orgPage.selectOption("#pre-defined", { label: "Bio" });
      await integrationPage.load(integrationURL);
    });

    await test.step("Adding campainId and Saving it", async () => {
      await integrationPage.pushDataLocatorClick();
      let campainId = '7015j0000011XlbAAE';
      await integrationPage.addingCampainIDAndSaving(campainId);
    });
    await orgPage.waitForTimeout(5000);
    await orgPage.locator("#integrationExportData label").click();
    await integrationPage.confirmDisableImportData.click();
    await orgPage.waitForTimeout(20000);
    let pushData = await getCampainIDVerification(
      orgApiContext,
      campainIDPushURL
    );
    expect(
      await integrationPage.verifyDisableStatus(pushData),
      "Status integration"
    ).toBeTruthy();
  });

  test("TC006: verify enabling/disable of import data toggle button", async () => {
    let integrationPage = new SalesforcePage(orgPage);
    await test.step(`Salesforce installation and setup at org Level`, async () => {
      await integrationPage.salesForceInstallation(
        dashboardUrl,
        integrationPageUrl,
        orgApiContext
      );
    });
    //going to registration form page and adding one custom field and then checking the success massege if field saved or not

    await test.step("Adding fields to the add Field options", async () => {
      await integrationPage.load(formURL);
      await orgPage.waitForTimeout(5000);
      await integrationPage.clickOnRegPageContinueBtn();
      await orgPage.waitForTimeout(2000);
      await integrationPage.addFieldBtnClick();
      await orgPage.waitForTimeout(5000);
      await orgPage.selectOption("#pre-defined", { label: "Bio" });
      await integrationPage.load(integrationURL);
    });

    await integrationPage.clickOnImportDataTab();
  });
});
