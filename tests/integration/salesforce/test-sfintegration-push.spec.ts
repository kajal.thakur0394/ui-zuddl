import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { DataUtl } from "../../../util/dataUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { SalesforceEventIntegrationController } from "../../../controller/SalesforceIntegrationController";
import {
  getEndDateFromToday,
  getStartDateFromToday,
  registerUserToEventbyApi,
  updateEventLandingPageDetails,
} from "../../../util/apiUtil";
import { SalesforceIntegrationDirectionType } from "../../../enums/SalesforceIntegrationDirectionEnum";
import EventEntryType from "../../../enums/eventEntryEnum";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import EventRole from "../../../enums/eventRoleEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";
import salesforceRegistrationMappingTestdata from "../../../test-data/salesforceintegration-testdata/registration-fields-n-mapping-data.json";
import { async } from "node-ical";
import { formatdateString } from "../../../util/commonUtil";
import { StagePage } from "../../../page-objects/events-pages/zones/StagePage";
import { PollsOrQuizController } from "../../../controller/PollsController";
import { PollsComponent } from "../../../page-objects/events-pages/live-side-components/PollsComponent";
import { RoomsListingPage } from "../../../page-objects/events-pages/zones/RoomsPage";
import { AVModal } from "../../../page-objects/events-pages/live-side-components/AvModal";
import { ExpoPage } from "../../../page-objects/events-pages/zones/ExpoPage";
import ZoneType from "../../../enums/ZoneTypeEnum";
import { TopNavBarComponent } from "../../../page-objects/events-pages/live-side-components/TopNavBarComponent";
import { QnAController } from "../../../controller/QnAController";
import { RoomModule } from "../../../page-objects/events-pages/site-modules/Rooms";
import { LobbyPage } from "../../../page-objects/events-pages/zones/LobbyPage";
import { SalesforcePage } from "../../../page-objects/integrations/salesforcePage";
import { IntegrationValidationUtil } from "../../../util/IntegrationValidationUtil";
import { log } from "console";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

test.describe("@integration @salesforce @export", async () => {
  let eventId;
  let eventTitle: string;
  let eventController: EventController;
  let orgBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  let salesforceIntegrationController: SalesforceEventIntegrationController;
  let integrationPageURL: string;
  test.beforeEach(async () => {
    test.setTimeout(6 * 60000);
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiRequestContext = orgBrowserContext.request;

    await test.step(`Creating new event to test salesforce export integration`, async () => {
      eventTitle = DataUtl.getRandomEventTitle();
      eventId = await EventController.generateNewEvent(orgApiRequestContext, {
        event_title: eventTitle,
      });
      eventController = new EventController(orgApiRequestContext, eventId);
    });

    await test.step(`Initialising the salesforce integration controller`, async () => {
      salesforceIntegrationController =
        new SalesforceEventIntegrationController(orgApiRequestContext, eventId);
    });
  });

  test.afterEach(async () => {
    await orgBrowserContext?.close();
  });

  test(`TC001: Verify when event is created, salesforce is present in integrations page`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-576/when-event-is-created-verify-salesforce-is-present-in-integrations",
    });
    let page = await orgBrowserContext.newPage();
    let integrationPage = new SalesforcePage(page);
    integrationPageURL = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/event/${eventId}/event-integration`;
    await test.step("Verify Salesforce tab is present in integration page", async () => {
      await integrationPage.load(integrationPageURL);
      await integrationPage.veirfySalesforceTabPresent();
    });
  });

  test(`TC002: Pushing existing records to salesforce by creating campaign with default fields for an event and trigger sync`, async ({}) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-122/verify-sync-action-for-[existing-registration-should-be-pushed-to",
    });
    const attendeeBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    const page = await orgBrowserContext.newPage();
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
    const attendeeFirstName = "Prateekzzz";
    const attendeeLastName = "QAAutomation";
    const attendeePhoneNumber = "1234567890";
    const attendeeDesignation = "SQA";
    const attendeeCompany = "Zuddl";

    const campaignId = "7015j0000011XlbAAE";
    let listOfRegisteredRecordFromTray: [JSON];
    let isUserFoundInTray = false;
    let userRecordFromTray: JSON;

    await test.step(`Register a new user to event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        email: attendeeEmail,
        firstName: attendeeFirstName,
        lastName: attendeeLastName,
        phoneNumber: attendeePhoneNumber,
        designation: attendeeDesignation,
        company: attendeeCompany,
      });
      console.log(`successfully registered user to the event`);
    });

    await test.step(`Enable salesforce integration for this event via campaignId`, async () => {
      await salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId,
        SalesforceIntegrationDirectionType.PUSH
      );
      console.log(`Successfully enabled salesforce Integration`);
    });

    await test.step(`Trigger the sync for the PUSH Integration for campaign ${campaignId}`, async () => {
      //await salesforceIntegrationController.syncExistingRecords();
      console.log(`Existing records synced successfully`);
    });

    await test.step(`Fetch data from tray for event ${eventId} to verify registration data is synced to tray for SF`, async () => {
      await page.waitForTimeout(2 * 60000);
      listOfRegisteredRecordFromTray =
        await salesforceIntegrationController.getRegisterUserDataFromTray(
          campaignId
        );
    });

    await test.step(`Verify the records fetched from Tray contains the attendee email registered for this event before enabling sync`, async () => {
      userRecordFromTray =
        await salesforceIntegrationController.verifyRecordsFromTray(
          listOfRegisteredRecordFromTray,
          attendeeEmail,
          isUserFoundInTray,
          userRecordFromTray
        );
    });

    await test.step(`verifying the record properties received from tray`, async () => {
      IntegrationValidationUtil.validateLeadDataFetchedFromTray(
        {
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          company: attendeeCompany,
          email: attendeeEmail,
        },
        userRecordFromTray,
        "Registered"
      );
    });
  });

  test(`TC003: Verify new registrations are being exported to salesforce , if the push integration is active for an event`, async ({
    page,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-123/verify-new-user-if-registers-his-data-should-be-properly-sent-to",
    });
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
    const attendeeFirstName = "prateek";
    const attendeeLastName = "test";
    const attendeeCompany = "Unknown";
    const campaignId = "7015j0000011XlbAAE";
    let listOfRegisteredRecordFromTray: [JSON];
    let isUserFoundInTray = false;
    let userRecordFromTray: JSON;

    await test.step(`Create new campaign for salesforce integration for this event`, async () => {
      await salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId,
        SalesforceIntegrationDirectionType.PUSH
      );
    });

    await test.step(`Register a new user to event`, async () => {
      await registerUserToEventbyApi(
        orgApiRequestContext,
        eventId,
        attendeeEmail
      );
    });

    await test.step(`Fetch data from tray for event ${eventId} to verify registration data is synced to tray for SF`, async () => {
      await page.waitForTimeout(2 * 60000);
      listOfRegisteredRecordFromTray =
        await salesforceIntegrationController.getRegisterUserDataFromTray(
          campaignId
        );
    });

    await test.step(`Verify the records fetched from Tray contains the attendee email registered for this event before enabling sync`, async () => {
      userRecordFromTray =
        await salesforceIntegrationController.verifyRecordsFromTray(
          listOfRegisteredRecordFromTray,
          attendeeEmail,
          isUserFoundInTray,
          userRecordFromTray
        );
    });

    await test.step(`verifying the record properties recieved from tray`, async () => {
      console.log(userRecordFromTray);
      console.log(JSON.stringify(userRecordFromTray));
      IntegrationValidationUtil.validateLeadDataFetchedFromTray2(
        {
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          company: attendeeCompany,
          email: attendeeEmail,
        },
        userRecordFromTray,
        "Registered"
      );
    });
  });

  test(`TC004: Verify if user has attended the event, same data is being shown when record is synced to tray with push integration`, async ({
    page,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-123/verify-new-user-if-registers-his-data-should-be-properly-sent-to",
    });
    const campaignId = "7015j0000011XlbAAE";
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
    const attendeeFirstName = "Prateek";
    const attendeeLastName = "Attended";
    const attendeeName = `${attendeeFirstName} ${attendeeLastName}`;
    const attendeeBio = "Testing";
    const attendeePhoneNumber = "1234567890";
    const attendeeDesignation = "SQA";
    const attendeeCompany = "Zuddl";
    let registrationId;
    let listOfRegisteredRecordFromTray: [JSON];
    let isUserFoundInTray = false;
    let userRecordFromTray: JSON;
    let attendeeMagicLink: string;

    await test.step(`Create new campaign for salesforce integration for this event`, async () => {
      await salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId,
        SalesforceIntegrationDirectionType.PUSH
      );
    });

    await test.step(`Register a new user to event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        email: attendeeEmail,
        firstName: attendeeFirstName,
        lastName: attendeeLastName,
        phoneNumber: attendeePhoneNumber,
        designation: attendeeDesignation,
        company: attendeeCompany,
      });
    });

    await test.step(`Fetch the registration Id of this user`, async () => {
      const registrationDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          eventId,
          attendeeEmail
        );
      registrationId = registrationDataFromDb["registration_id"];
    });

    await test.step(`Fetch the magic link for this attendee`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
    });

    await test.step(`Enabling magic link for this event`, async () => {
      await updateEventLandingPageDetails(orgApiRequestContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
      });
    });

    await test.step(`Attendee visits attendee magic link and enter inside the event until lobby`, async () => {
      await page.goto(attendeeMagicLink);
      await page.click("text=Enter Now");
      await page.waitForURL(/lobby/);
    });

    await test.step(`Fetch data from tray for event ${eventId} to verify registration data is synced to tray for SF`, async () => {
      await page.waitForTimeout(3 * 60000);
      listOfRegisteredRecordFromTray =
        await salesforceIntegrationController.getRegisterUserDataFromTray(
          campaignId
        );
    });

    await test.step(`Verify the records fetched from Tray contains the attendee email registered for this event before enabling sync`, async () => {
      userRecordFromTray =
        await salesforceIntegrationController.verifyRecordsFromTray(
          listOfRegisteredRecordFromTray,
          attendeeEmail,
          isUserFoundInTray,
          userRecordFromTray
        );
    });

    await test.step(`verifying the record properties recieved from tray`, async () => {
      console.log(userRecordFromTray);
      console.log(JSON.stringify(userRecordFromTray));
      IntegrationValidationUtil.validateLeadDataFetchedFromTray(
        {
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          company: attendeeCompany,
          email: attendeeEmail,
        },
        userRecordFromTray,
        "Registered"
      );
    });
  });

  test(`TC005: verify new registrations are not being exported to salesforce, if push integration is disabled for an event`, async ({
    page,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-255/verify-if-push-integration-is-disabled-attendee-registration-data-do",
    });

    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
    const campaignId = "7015j0000011XlbAAE";
    let listOfRegisteredRecordFromTray: [JSON];
    let isUserFoundInTray = false;
    let userRecordFromTray: JSON;

    await test.step(`Create new campaign and enable salesforce integration for this event`, async () => {
      await salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId,
        SalesforceIntegrationDirectionType.PUSH
      );
    });

    await test.step(`Disable the salesforce integration for this event for the given campaign`, async () => {
      await salesforceIntegrationController.disableSalesforceIntegrationForThisEventWithCampaign(
        campaignId,
        SalesforceIntegrationDirectionType.PUSH
      );
    });

    await test.step(`Now Register a new user to event`, async () => {
      await registerUserToEventbyApi(
        orgApiRequestContext,
        eventId,
        attendeeEmail
      );
    });

    await test.step(`Fetch data from tray for event ${eventId} to verify registration data is synced to tray for SF`, async () => {
      await page.waitForTimeout(2 * 60000);
      listOfRegisteredRecordFromTray =
        await salesforceIntegrationController.getRegisterUserDataFromTray(
          campaignId
        );
    });

    await test.step(`Verify the records fetched from Tray should not contain the  email registered for this event when sync is disabled`, async () => {
      for (const eachRecord of listOfRegisteredRecordFromTray) {
        const userEmail = eachRecord["Email"];
        if (userEmail == attendeeEmail) {
          console.log(`Record found for ${userEmail} in tray`);
          isUserFoundInTray = true;
          userRecordFromTray = eachRecord;
          break;
        }
      }
      expect(
        isUserFoundInTray,
        `expecting user email ${attendeeEmail} to not be exported in tray `
      ).toBe(false);
    });
  });

  test(`TC006: Verify registration pre-defined fields if mapped to salesforce fields are being synced correctly`, async ({
    page,
  }) => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-123/verify-new-user-if-registers-his-data-should-be-properly-sent-to",
    });
    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
    const campaignId = "7015j0000011XlbAAE";
    let listOfRegisteredRecordFromTray: [JSON];
    let isUserFoundInTray = false;
    let userRecordFromTray: JSON;
    let userInfoDTO: UserInfoDTO;
    let userEventRoleDto: UserEventRoleDto;
    let eventInfoDto: EventInfoDTO = new EventInfoDTO(eventId, "", false);

    await test.step(`Add pre-defined fields for an event`, async () => {
      const customFieldsData =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["registration-form-data-with-only-predefined-fields"];
      await eventController.addCustomFieldsWithConditionToEvent(
        customFieldsData
      );
    });

    await test.step(`Create new campaign for salesforce integration for this event`, async () => {
      await salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId,
        SalesforceIntegrationDirectionType.PUSH
      );
    });

    await test.step(`Update mapping of zuddl fields with salesforce fields`, async () => {
      const customFieldMapping =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customFieldMapping"];
      const customFieldPolicyMapping =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customPolicyMapping"];

      await salesforceIntegrationController.mapZuddlFieldsWithSalesforceFields({
        customFieldMapping: customFieldMapping,
        customPolicyMapping: customFieldPolicyMapping,
      });
    });

    await test.step(`Register a new user to event`, async () => {
      let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
        .attendeeLandingPage;
      userInfoDTO = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        true
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDTO,
        eventInfoDto,
        EventRole.NOTEXISTS,
        false
      );
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendeeLandingPage);
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData
          .predefinedFieldsFormDataForSalesforcePushCase,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
        }
      );
      // test.step("verify succesfully registered message visible.", async () => {
      //   await (
      //     await landingPageOne.getRegistrationConfirmationScreen()
      //   ).isRegistrationConfirmationMessageVisible(true);
      // });
    });

    await test.step(`Fetch data from tray for event ${eventId} to verify registration data is synced to tray for SF`, async () => {
      await page.waitForTimeout(2 * 60000);
      listOfRegisteredRecordFromTray =
        await salesforceIntegrationController.getRegisterUserDataFromTray(
          campaignId
        );
    });

    await test.step(`Verify the records fetched from Tray contains the attendee email registered for this event before enabling sync`, async () => {
      userRecordFromTray =
        await salesforceIntegrationController.verifyRecordsFromTray(
          listOfRegisteredRecordFromTray,
          attendeeEmail,
          isUserFoundInTray,
          userRecordFromTray
        );
    });

    await test.step(`verifying the record properties recieved from tray`, async () => {
      IntegrationValidationUtil.validateLeadDataFetchedFromTray(
        {
          firstName:
            userInfoDTO.userRegFormData
              .predefinedFieldsFormDataForSalesforcePushCase["firstName"]
              ?.dataToEnter,
          lastName:
            userInfoDTO.userRegFormData
              .predefinedFieldsFormDataForSalesforcePushCase["lastName"]
              ?.dataToEnter,
          company:
            userInfoDTO.userRegFormData
              .predefinedFieldsFormDataForSalesforcePushCase["company"]
              ?.dataToEnter,
          email:
            userInfoDTO.userRegFormData
              .predefinedFieldsFormDataForSalesforcePushCase["email"]
              ?.dataToEnter,
        },
        userRecordFromTray,
        "Registered"
      );
    });
  });

  test(`TC007: Lead Custom field: deletion of fields`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-578/exportlead-custom-field-deletion-of-fields",
    });
    const campaignId = "7015j0000011XlbAAE";
    let page = await orgBrowserContext.newPage();
    let integrationPage = new SalesforcePage(page);
    integrationPageURL = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/event/${eventId}/event-integration`;
    await test.step(`Add pre-defined fields for an event`, async () => {
      const customFieldsData =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["registration-form-data-with-only-predefined-fields"];
      await eventController.addCustomFieldsWithConditionToEvent(
        customFieldsData
      );
    });

    await test.step(`Create new campaign for salesforce integration for this event`, async () => {
      await salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId,
        SalesforceIntegrationDirectionType.PUSH
      );
    });

    await test.step(`Update mapping of zuddl fields with salesforce fields`, async () => {
      const customFieldMapping =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customFieldMapping"];
      const customFieldPolicyMapping =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customPolicyMapping"];

      await salesforceIntegrationController.mapZuddlFieldsWithSalesforceFields({
        customFieldMapping: customFieldMapping,
        customPolicyMapping: customFieldPolicyMapping,
      });
    });

    await test.step("Verify Custom Field can be deleted using API", async () => {
      await integrationPage.load(integrationPageURL);
      await integrationPage.gotoLeadCustomFieldTabForPush(1);
      await page.waitForTimeout(5000);
      await integrationPage.deleteOneCustomFieldForPush();
      await salesforceIntegrationController.verifyCustomFieldDeletion(1);
    });
  });

  test(`TC008: Verify registration pre-defined fields if mapped to salesforce fields are being synced correctly for secondary field`, async () => {
    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-604/export-contact-custom-field",
    });
    let page = await orgBrowserContext.newPage();
    let integrationPage = new SalesforcePage(page);
    integrationPageURL = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/event/${eventId}/event-integration`;

    const attendeeEmail = DataUtl.getRandomAttendeeMailosaurEmail();
    const campaignId = "7015j0000011XlbAAE";
    let listOfRegisteredRecordFromTray: [JSON];
    let isUserFoundInTray = false;
    let userRecordFromTray: JSON;
    // let customFieldsData =
    //   conditionalFieldsTestData["predefined-fields-regform"];
    let userInfoDTO: UserInfoDTO;
    let userEventRoleDto: UserEventRoleDto;
    let eventInfoDto: EventInfoDTO = new EventInfoDTO(eventId, "", false);

    await test.step(`Add pre-defined fields for an event`, async () => {
      const customFieldsData =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ][
          "registration-form-data-with-only-predefined-fields-secondaryCustomFieldMapping"
        ];
      await eventController.addCustomFieldsWithConditionToEvent(
        customFieldsData
      );
    });

    await test.step(`Create new campaign for salesforce integration for this event`, async () => {
      await salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId,
        SalesforceIntegrationDirectionType.PUSH
      );
    });

    await test.step(`Update mapping of zuddl fields with salesforce fields`, async () => {
      const secondaryCustomFieldMapping =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["secondaryCustomFieldMapping"];
      const secondaryCustomPolicyMapping =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["secondaryCustomPolicyMapping"];

      await salesforceIntegrationController.mapZuddlFieldsWithSalesforceFields({
        secondaryCustomFieldMapping: secondaryCustomFieldMapping,
        secondaryCustomPolicyMapping: secondaryCustomPolicyMapping,
      });
    });

    await test.step(`Register a new user to event`, async () => {
      let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
        .attendeeLandingPage;
      userInfoDTO = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        true
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDTO,
        eventInfoDto,
        EventRole.NOTEXISTS,
        false
      );
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendeeLandingPage);
      await landingPageOne.clickSignout();
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData
          .predefinedFieldsFormDataForSalesforcePushCase,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
        }
      );
    });
    await test.step(`Fetch data from tray for event ${eventId} to verify registration data is synced to tray for SF`, async () => {
      await page.waitForTimeout(2 * 60000);
      listOfRegisteredRecordFromTray =
        await salesforceIntegrationController.getRegisterUserDataFromTray(
          campaignId
        );
    });
  });

  test(`TC009: Contact Custom field: deletion of fields`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-580/export-contact-member-flow-deletion-of-fields",
    });
    const campaignId = "7015j0000011XlbAAE";
    let page = await orgBrowserContext.newPage();
    let integrationPage = new SalesforcePage(page);
    integrationPageURL = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/event/${eventId}/event-integration`;
    await test.step(`Add pre-defined fields for an event`, async () => {
      const customFieldsData =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ][
          "registration-form-data-with-only-predefined-fields-secondaryCustomFieldMapping"
        ];
      await eventController.addCustomFieldsWithConditionToEvent(
        customFieldsData
      );
    });

    await test.step(`Create new campaign for salesforce integration for this event`, async () => {
      await salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId,
        SalesforceIntegrationDirectionType.PUSH
      );
    });

    await test.step(`Update mapping of zuddl fields with salesforce fields`, async () => {
      const secondaryCustomFieldMapping =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["secondaryCustomFieldMapping"];
      const secondaryCustomPolicyMapping =
        salesforceRegistrationMappingTestdata[
          "TC_PUSH_ZUDDL_PREDEFINED_FIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["secondaryCustomPolicyMapping"];

      await salesforceIntegrationController.mapZuddlFieldsWithSalesforceFields({
        secondaryCustomFieldMapping: secondaryCustomFieldMapping,
        secondaryCustomPolicyMapping: secondaryCustomPolicyMapping,
      });
    });

    await test.step("Verify Custom Field can be deleted using UI", async () => {
      await integrationPage.load(integrationPageURL);
      await integrationPage.gotoLeadCustomFieldTabForPush(2);
      await page.waitForTimeout(5000);
      await integrationPage.deleteOneCustomFieldForPush();
      await salesforceIntegrationController.verifyCustomFieldDeletion(2);
    });
  });

  test(`TC0010: Verify agregrated data flow into salesforce when attendee has attended the event`, async ({}) => {
    test.setTimeout(7 * 120000);

    // Event Polls Answered
    // Event Question Asked
    // Event Question Upvoted
    // Note: It goes only after event has ended

    // linear ticket info
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-125/verify-aggregated-data-from-salesforce-data",
    });

    const attendeeOneBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    let attendeePage = await attendeeOneBrowserContext.newPage();

    const attendeeEmail = DataUtl.getRandomAttendeeEmail();
    const campaignId = "7015j0000011XlbAAE";
    const attendeeFirstName = "PrateeQA";
    const attendeeLastName = "Attended";
    const attendeePhoneNumber = "1234567890";
    const attendeeDesignation = "SQA";
    const attendeeCompany = "Zuddl";
    let zoneName: string;
    let zoneId: string;
    let boothId: string;
    let boothChannelId: string;
    let registrationId;
    let attendeeMagicLink: string;
    let defaultStageId: string;
    let defaultStageChannelId: string;
    let stageUrl: string;
    let roomsUrl: string;
    let roomId: any;
    let roomChannelId: any;
    let attendeePollComponent: PollsComponent;
    let pollController: PollsOrQuizController;
    let attendeeNavBar: TopNavBarComponent;
    let attendeeExpoPage: ExpoPage;
    let orgStagePage: StagePage;
    let attendeeStagePage: StagePage;
    let orgRoomPage: RoomsListingPage;
    let attendeeRoomPage: RoomsListingPage;
    let attendeeLandingPage;
    let orgAvModal: AVModal;
    let attendeeAvModal: AVModal;
    let qnaController: QnAController;
    let attendeeRoom: RoomModule;
    let attendeeLobbyPage: LobbyPage;
    let listOfRegisteredRecordFromTray: [JSON];
    let isUserFoundInTray = false;
    let userRecordFromTray: JSON;

    const pollTitle = "Poll 1";
    const pollOptions = [{ text: "option1" }, { text: "option2" }];
    const questionToAsk = "Where is Mumbai located";
    const eventChatMessage = "Event chat message";
    let orgPage = await orgBrowserContext.newPage();
    qnaController = new QnAController(
      attendeeOneBrowserContext.request,
      eventId,
      roomChannelId
    );

    await test.step(`Creating new event to test salesforce export integration`, async () => {
      eventTitle = DataUtl.getRandomEventTitle();
      eventId = await EventController.generateNewEvent(orgApiRequestContext, {
        event_title: eventTitle,
        default_settings: false,
        add_start_date_from_today: 0,
        add_end_date_from_today: 0,
        addMinutesToEndDate: 10,
      });
      console.log("eventTitle-->" + eventTitle);

      eventController = new EventController(orgApiRequestContext, eventId);
      attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
        .attendeeLandingPage;
      salesforceIntegrationController =
        new SalesforceEventIntegrationController(orgApiRequestContext, eventId);

      await test.step(`Enable salesforce integration for this event`, async () => {
        await salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
          campaignId,
          SalesforceIntegrationDirectionType.PUSH
        );
      });

      await test.step("Create a Room.", async () => {
        await eventController.getRoomsController.addRoomsToEvent({
          seatsPerRoom: 10,
          numberOfRooms: 1,
          roomCategory: "PUBLIC",
        });
      });

      await test.step("Create a Zone inside Expo.", async () => {
        let zoneCreationResponse =
          await eventController.getExpoController.createZone();
        zoneId = zoneCreationResponse["boothZoneId"];
        zoneName = zoneCreationResponse["title"];
        console.log("zoneName" + zoneName);
      });

      await test.step("Create a Booth inside Zone.", async () => {
        let boothName = DataUtl.generateRandomBoothName();
        let boothCreationResponse =
          await eventController.getExpoController.createSingleBoothWithinZone(
            zoneId,
            boothName
          );
        boothId = boothCreationResponse.get(boothName);
        boothChannelId = await eventController.getChannelId(boothId);
      });

      await test.step(`Get default stage Id for this event`, async () => {
        defaultStageId = await eventController.getDefaultStageId();
      });

      await test.step(`Get default stage channel Id to post polls`, async () => {
        defaultStageChannelId = await eventController.getChannelId(
          defaultStageId
        );
      });

      await test.step(`Add session To Schedule`, async () => {
        await eventController.getScheduleController.addSessionToScheduleEvent({
          title: "TestSession",
          description: "Session Description",
          type: "NONE",
          startDateTime: await formatdateString(
            (await getStartDateFromToday()).toString()
          ),
          endDateTime: await formatdateString(
            (await getEndDateFromToday()).toString()
          ),
          eventId: eventId,
        });
      });
    });

    await test.step(`Test data`, async () => {
      pollController = new PollsOrQuizController(orgApiRequestContext, eventId);
      stageUrl = `${
        DataUtl.getApplicationTestDataObj()["baseUrl"]
      }/l/event/${eventId}/stages/${defaultStageId}`;
      roomsUrl = `${
        DataUtl.getApplicationTestDataObj()["baseUrl"]
      }/l/event/${eventId}/discussions/`;
    });

    await test.step(`Organiser invites attendee to the event`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeFirstName,
        lastName: attendeeLastName,
        email: attendeeEmail,
        phoneNumber: attendeePhoneNumber,
        designation: attendeeDesignation,
        company: attendeeCompany,
      });
    });

    await test.step(`Fetch the registration Id of this user`, async () => {
      let registrationDataFromDb =
        await QueryUtil.fetchUserEventRegistrationDataFromDb(
          eventId,
          attendeeEmail
        );
      registrationId = registrationDataFromDb["registration_id"];
    });

    await test.step(`Fetch the magic link for this attendee`, async () => {
      attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
        eventId,
        attendeeEmail
      );
    });

    await test.step(`Organiser enables magic link for attendee for this event`, async () => {
      await updateEventLandingPageDetails(orgApiRequestContext, eventId, {
        eventEntryType: EventEntryType.REG_BASED,
        isMagicLinkEnabled: true,
      });
    });

    await test.step(`Attendee logs into the STAGE`, async () => {
      await attendeePage.goto(attendeeMagicLink);
      await attendeePage.click(`text=Enter Now`);
      await attendeePage.goto(stageUrl);
    });

    await test.step(`Organiser goes to attendee landing page and enter into the event and then goes to STAGE`, async () => {
      await orgPage.goto(attendeeLandingPage);
      await orgPage.click("text=Enter Now");
      await orgPage.goto(stageUrl);
      orgStagePage = new StagePage(orgPage);
      await orgStagePage.closeWelcomeStagePopUp();
    });

    await test.step(`Create new poll by api with title ${pollTitle} in published status in STAGE`, async () => {
      const pollCreatedInPublishResp =
        await pollController.createNewPollInPublish(defaultStageChannelId, {
          prompt: pollTitle,
          options: pollOptions,
        });
    });

    await test.step(`Attendee votes on option 1 in STAGE`, async () => {
      attendeeStagePage = new StagePage(attendeePage);
      attendeePollComponent = await attendeeStagePage.openPollsTab();
      await attendeePollComponent.selectOptionToVoteOnPoll(
        pollTitle,
        pollOptions[0]["text"]
      );
    });

    await test.step(`Organiser now goes to ROOMS`, async () => {
      await orgPage.goto(roomsUrl);
      orgRoomPage = new RoomsListingPage(orgPage);
      orgAvModal = new AVModal(orgPage);
      await orgRoomPage.enterIntoRoom();
      await orgAvModal.muteAudioVideoStreams();
      await orgAvModal.clickOnJoinButtonOnAvModal();
    });

    await test.step(`Get room channel Id to post polls`, async () => {
      await test.step(`Get room Id for this event`, async () => {
        roomId = await eventController.getRoomId(eventId);
      });
      roomChannelId = await eventController.getChannelId(roomId);
      qnaController = new QnAController(
        orgApiRequestContext,
        eventId,
        roomChannelId
      );
    });

    await test.step(`Organiser creates a new poll by api with title ${pollTitle} in publish status in ROOMS`, async () => {
      const pollCreatedInPublishResp =
        await pollController.createNewPollInPublish(roomChannelId, {
          prompt: pollTitle,
          options: pollOptions,
        });
    });

    await test.step(`Attendee now goes to ROOMS`, async () => {
      attendeeRoomPage = new RoomsListingPage(attendeePage);
      await attendeePage.goto(roomsUrl);
      attendeeAvModal = new AVModal(attendeePage);
      await attendeeRoomPage.enterIntoRoom();
      await attendeePage.waitForTimeout(2000);
      await attendeeAvModal.clickOnJoinButtonOnAvModal();
      await attendeePage.locator("button:has-text('Skip')").click();
    });

    await test.step(`Attendee votes on option 2 in ROOMS`, async () => {
      attendeePollComponent = await attendeeRoomPage.openPollsTab();
      await attendeePollComponent.selectOptionToVoteOnPoll(
        pollTitle,
        pollOptions[1]["text"]
      );
    });
    //QNA-Upvote by attendee in room
    await test.step(`Now, Attendee posts the question via API`, async () => {
      await qnaController.postAQuestion(questionToAsk);
    });

    await test.step(`Attendee opens QnA section`, async () => {
      attendeeRoom = new RoomModule(attendeePage);
      await attendeeRoom.getInteractionPanelComponent.clickOnQnATabOnInteractionPanel();
    });

    await test.step(`Attendee upvotes the question`, async () => {
      await attendeeRoom.getInteractionPanelComponent.getqNaComponent.clickOnUpvoteButton();
      await attendeeRoomPage.exitTheRoom();
    });

    await test.step(`Organiser creates a new poll by api with title ${pollTitle} in publish status in BOOTH`, async () => {
      const pollCreatedInPublishResp =
        await pollController.createNewPollInPublish(boothChannelId, {
          prompt: pollTitle,
          options: pollOptions,
        });
    });

    await test.step(`Attendee now goes to BOOTH`, async () => {
      attendeeNavBar = new TopNavBarComponent(attendeePage);
      attendeeExpoPage = new ExpoPage(attendeePage);
      await attendeeNavBar.switchToZone(ZoneType.EXPO);
      await attendeeExpoPage.clickOnEnterZoneButton();
      await attendeeExpoPage.enterIntoBooth();
    });

    await test.step(`Attendee votes on option 1 in BOOTH`, async () => {
      attendeePollComponent = await attendeeExpoPage.openPollsTab();
      await attendeePollComponent.selectOptionToVoteOnPoll(
        pollTitle,
        pollOptions[0]["text"]
      );
    });

    //Attendee sends the chat in Lobby
    await test.step(`Attendee goes to the lobby`, async () => {
      await attendeeNavBar.switchToZone(ZoneType.LOBBY);
      attendeeLobbyPage = new LobbyPage(attendeePage);
      await attendeeLobbyPage.clickOnChatIconOfClosedInteractionPanelState();
      await attendeeLobbyPage.getChatComponent.sendChatMessage(
        eventChatMessage
      );
    });

    await test.step(`Fetch data from tray for event ${eventId} to verify registration data is synced to tray for SF`, async () => {
      await orgPage.waitForTimeout(12 * 60000);
      listOfRegisteredRecordFromTray =
        await salesforceIntegrationController.getRegisterUserDataFromTray(
          campaignId
        );
    });

    await test.step(`Verify the records fetched from Tray contains the attendee email registered for this event before enabling sync`, async () => {
      userRecordFromTray =
        await salesforceIntegrationController.verifyRecordsFromTray(
          listOfRegisteredRecordFromTray,
          attendeeEmail,
          isUserFoundInTray,
          userRecordFromTray
        );
    });

    await test.step(`verifying the record properties recieved from tray`, async () => {
      console.log(JSON.stringify(userRecordFromTray));
      IntegrationValidationUtil.validateAggregateDataFetchedFromTray(
        {
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          company: attendeeCompany,
          email: attendeeEmail,
        },
        userRecordFromTray,
        "Attended"
      );
    });
  });
});
