import {
  APIRequestContext,
  BrowserContext,
  Page,
  expect,
  test,
} from "@playwright/test";
import { EventController } from "../../../controller/EventController";
import { DataUtl } from "../../../util/dataUtil";
import BrowserFactory from "../../../util/BrowserFactory";
import { SalesforceEventIntegrationController } from "../../../controller/SalesforceIntegrationController";
import { SalesforcePage } from "../../../page-objects/integrations/salesforcePage";
import { IntegrationController } from "../../../controller/IntegrationController";
import integrationType from "../../../enums/integrationTYpeEnum";
import { fieldMappingPayLoad } from "../../../interfaces/IntegrationControllerInterface";
import { SalesforceIntegrationDirectionType } from "../../../enums/SalesforceIntegrationDirectionEnum";
import { IntegrationValidationUtil } from "../../../util/IntegrationValidationUtil";
import salesforceRegistrationMappingTestdata from "../../../test-data/salesforceintegration-testdata/registration-fields-n-mapping-data.json";
import { UserInfoDTO } from "../../../dto/userInfoDto";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import { EventInfoDTO } from "../../../dto/eventInfoDto";
import EventRole from "../../../enums/eventRoleEnum";
import { LandingPageOne } from "../../../page-objects/events-pages/landing-page-1/LandingPageOne";

test.describe("@integration @salesforce @export @fieldmapping", async () => {
  let eventId;
  let eventTitle: string;
  let eventController: EventController;
  let orgBrowserContext: BrowserContext;
  let orgApiRequestContext: APIRequestContext;
  let salesforceIntegrationController: SalesforceEventIntegrationController;
  let dashboardUrl: string;

  test.beforeEach(async () => {
    orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    orgApiRequestContext = orgBrowserContext.request;
    dashboardUrl = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/dashboard`;
  });

  test.afterEach(async () => {
    //get field list, if it contains more than passed in payload, remove it.
    let integrationController = new IntegrationController(orgApiRequestContext);
    let payload: fieldMappingPayLoad = {
      organizationFieldMapping: {
        firstName: "FirstName",
        lastName: "LastName",
        email: "Email",
      },
      organizationPolicyMapping: {
        FirstName: "ALWAYS_UPDATE",
        LastName: "ALWAYS_UPDATE",
        Email: "DO_NOT_UPDATE",
      },
    };
    await test.step(`Now, add as the pre-defined field at org level via API`, async () => {
      await integrationController.addOrganisationMappingFields(
        integrationType.SALESFORCE,
        payload
      );
    });
    await orgBrowserContext?.close();
  });

  test(`TC001: To verify FirstName, LastName, and Email are the default fields present when app is installed`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-729/to-verify-email-is-the-default-fields-present-when-app-is-installed",
    });
    let orgPage = await orgBrowserContext.newPage();
    let salesforcePage = new SalesforcePage(orgPage);
    orgPage.setViewportSize({ width: 1400, height: 700 });

    await test.step(`Open salesforce Integration and go to mapping field page`, async () => {
      await salesforcePage.IntegrateApp(
        dashboardUrl,
        integrationType.SALESFORCE
      );
    });

    await test.step(`Verify FirstName, LastName, and Email as the default field being present`, async () => {
      await salesforcePage.verifyPresenceOfDefaultFields();
    });
  });

  test(`TC002: To verify user is able to add and delete new field when app is installed`, async () => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-730/to-verify-user-is-able-to-add-and-delete-new-field-when-app-is",
    });
    let orgPage = await orgBrowserContext.newPage();
    let salesforcePage = new SalesforcePage(orgPage);
    orgPage.setViewportSize({ width: 1400, height: 700 });
    let integrationController = new IntegrationController(orgApiRequestContext);
    let payload: fieldMappingPayLoad = {
      organizationFieldMapping: {
        firstName: "FirstName",
        lastName: "LastName",
        email: "Email",
        company: "Company",
      },
      organizationPolicyMapping: {
        FirstName: "ALWAYS_UPDATE",
        LastName: "ALWAYS_UPDATE",
        Email: "DO_NOT_UPDATE",
        Company: "ALWAYS_UPDATE",
      },
    };
    const deleteField = ["company"];

    await test.step(`Open salesforce Integration and go to mapping field page`, async () => {
      await salesforcePage.IntegrateApp(
        dashboardUrl,
        integrationType.SALESFORCE
      );
    });

    await test.step(`Now, add ${deleteField} as the pre-defined field at org level via API`, async () => {
      await integrationController.addOrganisationMappingFields(
        integrationType.SALESFORCE,
        payload
      );
    });

    await test.step(`Verify ${deleteField} field successfully gets added via API`, async () => {
      let response =
        await integrationController.getOrganizationFieldMappingObject(
          integrationType.SALESFORCE
        );
      expect(response).toEqual(payload.organizationFieldMapping);
    });

    // await test.step(`Now, delete ${deleteField} as the pre-defined field at org level via API`, async () => {
    //     await integrationController.deleteOrganisationMappingFields(payload, deleteField, integrationType.SALESFORCE);
    // });

    // await test.step(`Verify ${deleteField} field successfully gets deleted via API`, async () => {
    //     let deletedResponse = await integrationController.getOrganizationFieldMappingObject(integrationType.SALESFORCE);
    //     expect(deletedResponse).toEqual(payload.organizationFieldMapping);
    // });
  });

  test(`TC003: Lead custom fields: Verify when pre-defined field added in both org and event level, data should get synced in salesforce succesfully`, async ({
    // page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-731/verify-when-pre-defined-field-added-in-both-org-and-event-level-data",
    });

    const attendeeBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    const page = await attendeeBrowserContext.newPage();
    let xx = await orgBrowserContext.newPage();
    let integrationController = new IntegrationController(orgApiRequestContext);
    let payload: fieldMappingPayLoad = {
      organizationFieldMapping: {
        firstName: "FirstName",
        lastName: "LastName",
        company: "Company",
        email: "Email",
      },
      organizationPolicyMapping: {
        FirstName: "ALWAYS_UPDATE",
        LastName: "ALWAYS_UPDATE",
        Company: "ALWAYS_UPDATE",
        Email: "DO_NOT_UPDATE",
      },
    };
    const campaignId = "7015j0000011XlbAAE";
    const attendeeFirstName = "prateekAttendeeOne";
    const attendeeLastName = "QA";
    const attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let leadDataFromTray: any;
    const fieldToDeleteList = ["firstName", "lastName", "company"];

    await test.step(`Creating new event to test salesforce export integration`, async () => {
      eventTitle = DataUtl.getRandomEventTitle();
      eventId = await EventController.generateNewEvent(orgApiRequestContext, {
        event_title: eventTitle,
      });
      eventController = new EventController(orgApiRequestContext, eventId);
    });

    await test.step(`Now, add Company as the pre-defined field at org level via API`, async () => {
      await integrationController.addOrganisationMappingFields(
        integrationType.SALESFORCE,
        payload
      );
    });

    await test.step(`Verify Company field successfully gets added at org level via API`, async () => {
      let response =
        await integrationController.getOrganizationFieldMappingObject(
          integrationType.SALESFORCE
        );
      expect(response).toEqual(payload.organizationFieldMapping);
    });

    await test.step(`Enable Salesforce Integration`, async () => {
      salesforceIntegrationController =
        new SalesforceEventIntegrationController(orgApiRequestContext, eventId);
      salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId,
        SalesforceIntegrationDirectionType.PUSH
      );
    });

    await test.step(`Now Register a new user to the event via API`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeFirstName,
        lastName: attendeeLastName,
        email: attendeeEmail,
      });
    });

    await test.step(`Fetch lead data from tray`, async () => {
      await page.waitForTimeout(2 * 60000);
      leadDataFromTray =
        await salesforceIntegrationController.getLeadDataRegisteredFromTray(
          attendeeEmail
        );
    });

    await test.step(`Verifying the lead record fields recieved from tray`, async () => {
      console.log(JSON.stringify(leadDataFromTray));
      IntegrationValidationUtil.validateLeadFieldsFetchedFromTray(
        {
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          email: attendeeEmail,
        },
        leadDataFromTray
      );
    });

    // await test.step(`Now, delete  ${fieldToDeleteList} as the pre-defined field at org level via API`, async () => {
    //     await integrationController.deleteOrganisationMappingFields(payload, fieldToDeleteList, integrationType.SALESFORCE);
    // });

    // await test.step(`Verify ${fieldToDeleteList} field successfully gets deleted via API`, async () => {
    //     let deletedResponse = await integrationController.getOrganizationFieldMappingObject(integrationType.SALESFORCE);
    //     expect(deletedResponse).toEqual(payload.organizationFieldMapping);
    // });
  });

  test(`TC004: Lead custom fields: Verify existing user's pre-defined field should get updated in salesforce succesfully`, async ({
    page,
  }) => {
    /*
        1. Add user in salesforce 
        2. In new event, update the same users data
        3. Verify in tray data should be updated
        */
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-732/verify-existing-users-pre-defined-field-should-get-updated-in",
    });
    let integrationController = new IntegrationController(orgApiRequestContext);
    let payload: fieldMappingPayLoad = {
      organizationFieldMapping: {
        firstName: "FirstName",
        lastName: "LastName",
        company: "Company",
        email: "Email",
      },
      organizationPolicyMapping: {
        FirstName: "ALWAYS_UPDATE",
        LastName: "ALWAYS_UPDATE",
        Company: "ALWAYS_UPDATE",
        Email: "DO_NOT_UPDATE",
      },
    };
    const campaignId1 = "7015j0000011XlbAAE";
    const campaignId2 = "7015j000000ku01AAA";
    const attendeeFirstName = "prateekAttendeeOne";
    const attendeeLastName = "QA";
    const updatedAttendeeFirstName = "prateekAttendeeOneUpdated";
    const updatedAttendeeLastName = "QAUpdated";
    const attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let leadDataFromTray: any;
    let eventId2: any;
    let eventTitle2: string;
    let eventController2: EventController;
    const fieldToDeleteList = ["firstName", "lastName", "company"];

    await test.step(`Create new event1`, async () => {
      eventTitle = DataUtl.getRandomEventTitle();
      eventId = await EventController.generateNewEvent(orgApiRequestContext, {
        event_title: eventTitle,
      });
      eventController = new EventController(orgApiRequestContext, eventId);
    });

    await test.step(`Now, add Company as the pre-defined field at org level via API`, async () => {
      await integrationController.addOrganisationMappingFields(
        integrationType.SALESFORCE,
        payload
      );
    });

    await test.step(`Verify Company field successfully gets added at org level via API`, async () => {
      let response =
        await integrationController.getOrganizationFieldMappingObject(
          integrationType.SALESFORCE
        );
      expect(response).toEqual(payload.organizationFieldMapping);
    });

    await test.step(`Enable Salesforce Integration`, async () => {
      salesforceIntegrationController =
        new SalesforceEventIntegrationController(orgApiRequestContext, eventId);
      salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId1,
        SalesforceIntegrationDirectionType.PUSH
      );
    });

    await test.step(`Now Register a new user to the event via API`, async () => {
      await eventController.registerUserToEventWithRegistrationApi({
        firstName: attendeeFirstName,
        lastName: attendeeLastName,
        email: attendeeEmail,
      });
    });

    await test.step(`Fetch lead data from tray`, async () => {
      await page.waitForTimeout(2 * 60000);
      leadDataFromTray =
        await salesforceIntegrationController.getLeadDataRegisteredFromTray(
          attendeeEmail
        );
    });

    await test.step(`Verifying the lead record fields recieved from tray`, async () => {
      console.log(JSON.stringify(leadDataFromTray));
      IntegrationValidationUtil.validateLeadFieldsFetchedFromTray(
        {
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          email: attendeeEmail,
        },
        leadDataFromTray
      );
    });

    await test.step(`Now, create another event2`, async () => {
      eventTitle2 = DataUtl.getRandomEventTitle();
      eventId2 = await EventController.generateNewEvent(orgApiRequestContext, {
        event_title: eventTitle2,
      });
      eventController2 = new EventController(orgApiRequestContext, eventId2);
    });

    await test.step(`Enable Salesforce Integration`, async () => {
      salesforceIntegrationController =
        new SalesforceEventIntegrationController(
          orgApiRequestContext,
          eventId2
        );
      salesforceIntegrationController.enableSalesforceIntegrationForThisEventWithCampaign(
        campaignId2,
        SalesforceIntegrationDirectionType.PUSH
      );
    });

    await test.step(`Now, Register the same existing user of salesforce ${attendeeEmail} to this new event via API`, async () => {
      await eventController2.registerUserToEventWithRegistrationApi({
        firstName: updatedAttendeeFirstName,
        lastName: updatedAttendeeLastName,
        email: attendeeEmail,
      });
    });

    await test.step(`Fetch lead data from tray`, async () => {
      await page.waitForTimeout(60000);
      leadDataFromTray =
        await salesforceIntegrationController.getLeadDataRegisteredFromTray(
          attendeeEmail
        );
    });

    await test.step(`Verifying the lead record fields recieved from tray has updated details`, async () => {
      console.log(JSON.stringify(leadDataFromTray));
      IntegrationValidationUtil.validateLeadFieldsFetchedFromTray(
        {
          firstName: updatedAttendeeFirstName,
          lastName: updatedAttendeeLastName,
          email: attendeeEmail,
        },
        leadDataFromTray
      );
    });

    // await test.step(`Now, delete  ${fieldToDeleteList} as the pre-defined field at org level via API`, async () => {
    //     await integrationController.deleteOrganisationMappingFields(payload, fieldToDeleteList, integrationType.SALESFORCE);
    // });

    // await test.step(`Verify ${fieldToDeleteList} field successfully gets deleted via API`, async () => {
    //     let deletedResponse = await integrationController.getOrganizationFieldMappingObject(integrationType.SALESFORCE);
    //     expect(deletedResponse).toEqual(payload.organizationFieldMapping);
    // });
  });

  test(`TC005: Lead custom fields: Verify when custom field is added in an event level, data should get synced in salesforce succesfully, also verify deletion of custom field`, async ({
    page,
  }) => {
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-733/verify-when-custom-field-is-added-in-an-event-level-data-should-get",
    });
    test.info().annotations.push({
      type: "TC",
      description:
        "https://linear.app/zuddl/issue/QAT-734/verify-deletion-of-lead-custom-fields-via-ui",
    });
    /*
        1. Create event with custom field
        2. Map custom field with any pre-defined field
        3. enable integration
        4. Register user to event
        5. Verify tray data
        */

    let integrationController = new IntegrationController(orgApiRequestContext);
    let salesForceController: SalesforceEventIntegrationController;
    let payload: fieldMappingPayLoad = {
      organizationFieldMapping: {
        firstName: "FirstName",
        lastName: "LastName",
        company: "Company",
        email: "Email",
      },
      organizationPolicyMapping: {
        FirstName: "ALWAYS_UPDATE",
        LastName: "ALWAYS_UPDATE",
        Company: "ALWAYS_UPDATE",
        Email: "DO_NOT_UPDATE",
      },
    };
    const campaignId1 = "7015j0000011XlbAAE";
    const attendeeFirstName = "PrateekAttendeeOne";
    const attendeeLastName = "AutomationQA";
    const attendeeEmail = DataUtl.getRandomAttendeeEmail();
    let leadDataFromTray: any;
    let userInfoDTO: UserInfoDTO;
    let userEventRoleDto: UserEventRoleDto;
    let eventInfoDto: EventInfoDTO = new EventInfoDTO(eventId, "", false);
    const fieldToDeleteList = ["firstName", "lastName", "company"];
    await test.step(`Create new event`, async () => {
      eventTitle = DataUtl.getRandomEventTitle();
      eventId = await EventController.generateNewEvent(orgApiRequestContext, {
        event_title: eventTitle,
      });
      eventController = new EventController(orgApiRequestContext, eventId);
      salesForceController = new SalesforceEventIntegrationController(
        orgApiRequestContext,
        eventId
      );
    });

    await test.step(`Now, add FirstName, LastName, and Email as the pre-defined field at org level via API`, async () => {
      await integrationController.addOrganisationMappingFields(
        integrationType.SALESFORCE,
        payload
      );
    });

    await test.step(`Verify FirstName, LastName, and Email field successfully gets added at org level via API`, async () => {
      let response =
        await integrationController.getOrganizationFieldMappingObject(
          integrationType.SALESFORCE
        );
      expect(response).toEqual(payload.organizationFieldMapping);
    });

    await test.step(`Add custom fields for an event`, async () => {
      const customFieldsData =
        salesforceRegistrationMappingTestdata[
          "TC2_PUSH_ZUDDL_PREDEFINED_N_CUSTOMFIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["registrationCustomFields"];
      await eventController.addCustomFieldsWithConditionToEvent(
        customFieldsData
      );
    });

    await test.step(`Map Custom field to Pre-defined field in event Integration`, async () => {
      //mapping custom field 'myfield' to pre-defined field 'company'
      const customFieldMapping =
        salesforceRegistrationMappingTestdata[
          "TC2_PUSH_ZUDDL_PREDEFINED_N_CUSTOMFIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customFieldMapping"];
      const customFieldPolicyMapping =
        salesforceRegistrationMappingTestdata[
          "TC2_PUSH_ZUDDL_PREDEFINED_N_CUSTOMFIELDS_WITH_ALWAYS_UPDATE_POLICY"
        ]["customPolicyMapping"];

      await test.step(`Enable Salesforce Integration`, async () => {
        salesForceController.enableSalesforceIntegrationForThisEventWithCampaign(
          campaignId1,
          SalesforceIntegrationDirectionType.PUSH
        );
      });

      await salesForceController.mapZuddlFieldsWithSalesforceFields({
        customFieldMapping: customFieldMapping,
        customPolicyMapping: customFieldPolicyMapping,
      });
    });

    await test.step(`Register a new user to event`, async () => {
      let attendeeLandingPage = new EventInfoDTO(eventId, eventTitle)
        .attendeeLandingPage;
      userInfoDTO = new UserInfoDTO(
        attendeeEmail,
        "",
        "prateek",
        "parashar",
        true
      );
      userEventRoleDto = new UserEventRoleDto(
        userInfoDTO,
        eventInfoDto,
        EventRole.NOTEXISTS,
        false
      );
      let landingPageOne = new LandingPageOne(page);
      await landingPageOne.load(attendeeLandingPage);
      await (
        await landingPageOne.getRegistrationFormComponent()
      ).fillUpTheRegistrationForm(
        userInfoDTO.userRegFormData
          .predefinedAndCustomFieldsFormDataForSalesforcePushCase,
        {
          fillOptionalFields: true,
          fillMandatoryFields: true,
        }
      );
    });

    await test.step(`Fetch lead data from tray`, async () => {
      await page.waitForTimeout(2 * 60000);
      leadDataFromTray =
        await salesForceController.getLeadDataRegisteredFromTray(attendeeEmail);
    });

    await test.step(`Verifying the lead record fields recieved from tray has updated details`, async () => {
      console.log(JSON.stringify(leadDataFromTray));
      IntegrationValidationUtil.validateCustomLeadFieldsFetchedFromTray(
        {
          firstName: attendeeFirstName,
          lastName: attendeeLastName,
          email: attendeeEmail,
          company: "custom field",
        },
        leadDataFromTray
      );
    });

    await test.step(`Verify custom field gets succesfully deleted from event via UI`, async () => {
      let orgPage = await orgBrowserContext.newPage();
      let integrationPage = new SalesforcePage(orgPage);
      const fieldDelete = "MyField (string)";
      let integrationPageURL = `${
        DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
      }/event/${eventId}/event-integration`;
      await orgPage.goto(integrationPageURL);
      await integrationPage.gotoLeadCustomFieldTabForPush(1);
      await integrationPage.verifyFieldToBeDeleted(fieldDelete);
    });

    // await test.step(`Now, delete ${fieldToDeleteList} as the pre-defined field at org level via API`, async () => {
    //     await integrationController.deleteOrganisationMappingFields(payload, fieldToDeleteList, integrationType.SALESFORCE);
    // });

    // await test.step(`Verify ${fieldToDeleteList} field successfully gets deleted via API`, async () => {
    //     let deletedResponse = await integrationController.getOrganizationFieldMappingObject(integrationType.SALESFORCE);
    //     expect(deletedResponse).toEqual(payload.organizationFieldMapping);
    // });
  });
});
