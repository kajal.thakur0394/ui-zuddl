import {
    APIRequestContext,
    BrowserContext,
    Page,
    expect,
    test,
  } from "@playwright/test";
  import { EventController } from "../../../controller/EventController";
  import { DataUtl } from "../../../util/dataUtil";
  import BrowserFactory from "../../../util/BrowserFactory";
  import { SalesforceEventIntegrationController } from "../../../controller/SalesforceIntegrationController";
  import {
    updateEventLandingPageDetails,
    updateEventType,
  } from "../../../util/apiUtil";
  import EventType from "../../../enums/eventTypeEnum";
  import { SalesforcePage } from "../../../page-objects/integrations/salesforcePage";
  
  test.describe("@integration @salesforce @export", async () => {
    let eventId;
    let eventTitle: string;
    let eventController: EventController;
    let orgBrowserContext: BrowserContext;
    let orgApiRequestContext: APIRequestContext;
    let salesforceIntegrationController: SalesforceEventIntegrationController;
    let integrationPageURL: string;
    let campainIDPushURL: string;
    test.beforeEach(async () => {
      test.setTimeout(6 * 60000);
      orgBrowserContext = await BrowserFactory.getBrowserContext({
        laodOrganiserCookies: true,
      });
      orgApiRequestContext = orgBrowserContext.request;
  
      await test.step(`Creating new event to test salesforce export integration`, async () => {
        eventTitle = DataUtl.getRandomEventTitle();
        eventId = await EventController.generateNewEvent(orgApiRequestContext, {
          event_title: eventTitle,
        });
        eventController = new EventController(orgApiRequestContext, eventId);
      });

      await test.step(`update an event with HYBRID as event type`, async () => {
        await updateEventType(orgApiRequestContext, eventId, EventType.HYBRID);
        await updateEventLandingPageDetails(orgApiRequestContext, eventId, {
          isMagicLinkEnabled: true,
        });
      });
  
      await test.step(`Initialising the salesforce integration controller`, async () => {
        salesforceIntegrationController =
          new SalesforceEventIntegrationController(orgApiRequestContext, eventId);
      });
      campainIDPushURL = `${
        DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
      }/api/integration/${eventId}/integration-setting/PUSH`;
    });
  
    test.afterEach(async () => {
      await orgBrowserContext?.close();
    });
  
    test(`TC001: Verify when event is created, salesforce is present in integrations page`, async () => {
      test.info().annotations.push({
        type: "TC",
        description:
          "",
      });
      let page = await orgBrowserContext.newPage();
      let integrationPage = new SalesforcePage(page);
      integrationPageURL = `${
        DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
      }/event/${eventId}/event-integration`;
      await test.step("Verify Salesforce tab is present in integration page", async () => {
          await integrationPage.load(integrationPageURL);
        await integrationPage.veirfySalesforceTabPresent();
      });
    });


    test(`TC002: Verify Lead capture field mapping can be done with custom lead capture field`, async () => {
      test.info().annotations.push({
        type: "TC",
        description:
          "",
      });
      let page = await orgBrowserContext.newPage();
      let integrationPage = new SalesforcePage(page);
      integrationPageURL = `${
        DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
      }/event/${eventId}/event-integration`;
      await test.step("Verify Salesforce tab is present in integration page", async () => {
          await integrationPage.load(integrationPageURL);
        //   await page.pause()
        await integrationPage.veirfySalesforceTabPresent();
      });
      await test.step("Adding campainId and Saving it", async () => {
        await integrationPage.pushDataLocatorClick();
        let campainId = '7015j0000011XlbAAE';
        await integrationPage.addingCampainID(campainId);
      });

      await test.step("Now goto lead custom field", async () => {
        await integrationPage.savingCustomActivities();
        await integrationPage.savingleadCustomRegField();
      });
      await test.step("Verify that the button redirect to correct URL", async () => {
        await integrationPage.gotoToLeadCaptureFieldsURL();
        await integrationPage.verifyLeadCaptureURL();
      });
    });
    test(`TC003: Verify adding new Lead capture custome field`, async () => {
      test.info().annotations.push({
        type: "TC",
        description:
          "",
      });
      let page = await orgBrowserContext.newPage();
      let integrationPage = new SalesforcePage(page);
      integrationPageURL = `${
        DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
      }/event/${eventId}/event-integration`;
      await test.step("Verify Salesforce tab is present in integration page", async () => {
          await integrationPage.load(integrationPageURL);
        //   await page.pause()
        await integrationPage.veirfySalesforceTabPresent();
      });
      await test.step("Adding campainId and Saving it", async () => {
        await integrationPage.pushDataLocatorClick();
        let campainId = '7015j0000011XlbAAE';
        await integrationPage.addingCampainID(campainId);
      });

      await test.step("Now goto lead custom field", async () => {
        await integrationPage.savingCustomActivities();
        await integrationPage.savingleadCustomRegField();
      });
      await test.step("Goto lead capture page and add new custom lead capture field", async () => {
        await integrationPage.gotoToLeadCaptureFieldsURL();
        await integrationPage.addCustomeLeadCaptureField();
        await integrationPage.load(integrationPageURL);
      });
      await test.step("Verify Salesforce tab is present in integration page", async () => {
        await integrationPage.load(integrationPageURL);
      });
      await test.step("Verify new custom lead capture field added in the dropdown menu", async () => {
        await page.waitForTimeout(3000);
        await integrationPage.pushDataLocatorClick();
        let campainId = '7015j0000011XlbAAE';
        await integrationPage.addingCampainID(campainId);
        await integrationPage.savingCustomActivities();
        await integrationPage.savingleadCustomRegField();
        await integrationPage.verifyCustomDropDown();
      });

    });


});