export enum AudienceTypeEnum {
  ATTENDEE = "Attendee",
  SPEAKER = "Speaker",
  ORGANISER = "Organiser",
  MODERATOR = "Moderator",
  SPONSER = "Sponser",
  ATTENDED = "Attended",
  NOT_ATTENDED = "Not Attended",
}

export enum TriggerTypeEnum {
  ATTENDEE_REGISTERED = "Attendee registered",
  TICKET_PURCHASED = "Ticket purchased",
  EVENT_CANCELED = "Event canceled",
  ATTENDEE_CHECKIN = "Attendee checked-in",
}

export enum CustomCommTypeEnum {
  PRE_EVENT = "Pre-event",
  LIVE_EVENT = "Live event",
  POST_EVENT = "Post-event",
}
