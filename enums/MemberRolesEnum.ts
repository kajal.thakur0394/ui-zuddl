export enum MemberRolesEnum {
    OWNER = "OWNER",
    ADMIN = "ADMIN",
    ORGANIZER = "ORGANIZER",
    MEMBER = "MEMBER",
    MODERATOR = "MODERATOR",
    ATTENDEE = "ATTENDEE"
}
export enum MemberRolesCapitaliseEnum {
    OWNER = "Owner",
    ADMIN = "Admin",
    ORGANIZER = "Organizer",
    MEMBER = "Member",
    MODERATOR = "Moderator",
    ATTENDEE = "Attendee"
}
export enum MemberRolesSmallCaseEnum {
    OWNER = "owner",
    ADMIN = "admin",
    ORGANIZER = "organizer",
    MEMBER = "member",
    MODERATOR = "moderator",
    ATTENDEE = "attendee"
}


