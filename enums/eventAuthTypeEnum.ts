enum EventAuthType {
  EMAIL = "EMAIL",
  EMAIL_OTP = "EMAIL_OTP",
  SSO = "SSO",
  FACEBOOK = "FACEBOOK",
  LINKEDIN = "LINKEDIN",
  GOOGLE = "GOOGLE",
}

export default EventAuthType;
