enum StageSize {
    SMALL = "SMALL",
    MEDIUM = "REGULAR",
    LARGE = "LARGE"
}

export default StageSize