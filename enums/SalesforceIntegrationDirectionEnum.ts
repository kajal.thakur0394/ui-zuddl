export enum SalesforceIntegrationDirectionType {
  PUSH = "PUSH",
  PULL = "PULL",
}
export enum HubspotIntegrationDirectionType {
  PUSH = "PUSH",
  PULL = "PULL",
}
