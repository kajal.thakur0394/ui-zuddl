enum integrationType {
    HUBSPOT = "HUBSPOT",
    SALESFORCE = "SALESFORCE"
}
export default integrationType;