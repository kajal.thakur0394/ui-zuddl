enum onboardingCheckMessage{
    BROWSERCHECKMESSAGE = 'Checking your browser',
    CONNECTIONCHECKMESSAGE = 'Checking your connection',
    NETWORKCHECKMESSAGE = 'Network strength : Good',
    JOINMESSAGE = "You'll be able to join in just a moment",
}
export default onboardingCheckMessage