enum EventSettingID {
  EmailOnRegistration = "REGISTRATION_CONFIRMATION",
  EventInvitationFromEmailFromPeopleSection = "EVENT_INVITATION",
  EmailReminderBefore7Day = "EVENT_REMINDER_SEVEN_DAYS_TO_GO",
  EmailReminderBefore1Day = "EVENT_REMINDER_ONE_DAY_TO_GO",
  EmailReminderBefore1Hour = "EVENT_REMINDER_ONE_HOUR_TO_GO",
  EmailReminderBefore5Minute = "EVENT_REMINDER_FIVE_MINUTES_TO_GO",
  EmailThankYouForComing = "THANK_YOU_FOR_COMING",
  EmailTicketBuyerReminderForAddingAttendeeDetails = "TICKET_BUYER_REMINDER_EVENT_ONE_DAY_TO_GO",
  EventCanceled = "EVENT_CANCELED",
  WeMissedYouAtTheEventForAtteendee = "WE_MISSED_YOU_AT_THE_EVENT",
  RegistrationPendingForAttendee = "REGISTRATION_PENDING_APPROVAL",
  RegistrationRejectedForAttendee = "REGISTRATION_REJECTED",
}

/*
Attendee Notification type-
............................
"REGISTRATION_CONFIRMATION",
"EVENT_INVITATION",
"EVENT_REMINDER_ONE_DAY_TO_GO",
"EVENT_REMINDER_ONE_HOUR_TO_GO",
"THANK_YOU_FOR_COMING",
"EVENT_REMINDER_FIVE_MINUTES_TO_GO",
"TICKET_BUYER_REMINDER_EVENT_ONE_DAY_TO_GO",
"EVENT_CANCELED", 
"WE_MISSED_YOU_AT_THE_EVENT",
"REGISTRATION_PENDING_APPROVAL",
"REGISTRATION_REJECTED"

Speaker Notification type-
..........................
"EVENT_INVITATION",
"EVENT_REMINDER_ONE_DAY_TO_GO",
"EVENT_REMINDER_ONE_HOUR_TO_GO",
"THANK_YOU_FOR_COMING",
"EVENT_CANCELED", 
*/

export default EventSettingID;
