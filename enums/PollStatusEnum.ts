enum PollStatus {
  SCHEDULED = "SCHEDULED",
  LIVE = "LIVE",
  CLOSED="CLOSED"
}
export default PollStatus;
