enum ProductType {
  EVENT = "EVENT",
  STUDIO = "STUDIO",
  WEBINAR = "WEBINAR"
}

export default ProductType;
