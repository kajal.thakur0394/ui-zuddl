enum EmailRestrictionType {
  DENY = "DENY",
  ALLOW = "ALLOW",
  NONE = "NONE",
}

export default EmailRestrictionType;
