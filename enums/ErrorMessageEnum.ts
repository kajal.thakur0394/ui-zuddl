enum ErrorMessage {
    NO_ACCESS = "Looks like you do not have access to the page you are looking for",
    NO_VALID_QUESTION = "Please ask a valid question."
}

export default ErrorMessage