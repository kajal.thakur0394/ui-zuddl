enum ZoneType {
  STAGE = "STAGE",
  EXPO = "EXPO",
  LOBBY = "LOBBY",
  SCHEDULE = "SCHEDULE",
  NETWORKING = "NETWORKING",
  ROOMS = "TABLE",
}

export default ZoneType;
