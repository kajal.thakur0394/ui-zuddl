enum EventReminderHeader {
  EmailReminderBefore1Day = "Just 1 day to go!",
  EmailReminderBefore7Days = "7 days to go!",
  EmailReminderBefore1Hour = "Just an hour to go!",
  EmailReminderBefore5Minute = "Just 5 minutes to go!",
  EmailThankYouForComing = "It was great to see you",
}

export default EventReminderHeader;
