enum EventType{
    VIRTUAL="VIRTUAL",
    HYBRID="HYBRID",
    IN_PERSON="IN_PERSON"
}

export default EventType
