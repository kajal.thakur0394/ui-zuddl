enum TextMessage {
  UPVOTE_MESSAGE = "You’ve got 1 upvote for your question",
  PUBLISH_QUESTION = "Published Questions which are visible to attendees appear here",
}

export default TextMessage;
