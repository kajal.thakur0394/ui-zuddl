export enum VenueOptions {
  LEGACYBACKSTAGE = "LegacyBackstage",
  STAGE = "Stage",
  ROOMS = "Room",
}

export default VenueOptions;
