export enum Filters {
  EMAIL = "Email",
  TITLE = "Title",
  COMPANY = "Company",
  COUNTRY = "Country",
  INDUSTRY = "Industry",
  IP_STATE = "IP State",
  IP_CITY = "IP City",
  IP_COUNTRY = "IP Country",
  SOURCE = "Source",
  REGISTRATION_DATE_AND_TIME = "Registration date and time",
  IN_PERSON_CHECK_IN_TIME = "In-Person Check-In Time",
  STATUS = "Status",
  DEVICE_TYPE = "Device Type",
}

export enum FilterOperators {
  IS = "EQUAL_TO",
  IS_NOT = "NOT_EQUAL_TO",
  CONTAINS = "CONTAINS",
  NOT_CONTAINS = "NOT_CONTAINS",
  TRUE = "IS_TRUE",
  FALSE = "IS_FALSE",
  EARLIER_THAN = "SMALLER_THAN_OR_EQUAL",
  LATER_THAN = "GREATER_THAN_OR_EQUAL",
  ANY_OF = "ANY_OF",
}

export enum FilterOperatorsUI {
  IS = "is",
  IS_NOT = "is not",
  CONTAINS = "Contains",
  NOT_CONTAINS = "Does not contain",
  TRUE = "True",
  FALSE = "False",
  EARLIER_THAN = "earlier than",
  LATER_THAN = "later than",
  ANY_OF = "any of",
}

export type FiltersUserInput = {
  filterName: Filters;
  compareValue: string | DeviceType[];
  filterOperator: FilterOperators;
};

export enum DeviceType {
  DESKTOP = "DESKTOP",
  MOBILE = "MOBILE",
  TABLET = "TABLET",
  UNKNOWN = "UNKNOWN",
}

export enum FilterLogic {
  AND = "AND",
  OR = "OR",
}
