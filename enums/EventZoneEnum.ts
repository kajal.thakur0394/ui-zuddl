export enum EventZones {
  STAGE = "stage",
  LOBBY = "lobby",
  EXPO = "expo",
  ROOMS = "rooms",
  NETWORKING = "networking",
}
export enum EventZonesUpperCase {
  STAGE = "STAGE",
  LOBBY = "LOBBY",
  EXPO = "EXPO",
  ROOMS = "TABLE",
  NETWORKING = "NETWORKING",
  SCHEDULE = "SCHEDULE",
}

export enum AnnouncementDefaultZones {
  STAGE = "Stage",
  LOBBY = "Lobby",
  EXPO = "Expo",
  ROOMS = "Rooms",
  NETWORKING = "Networking",
  SCHEDULE = "Schedule",
}
