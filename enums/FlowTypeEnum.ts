export enum FlowType {
  REGISTRATION = "REGISTRATION",
}

export enum FlowStatus {
  DRAFT = "draft",
  FLOW = "flow",
}

export enum DiscountType {
  AMOUNT = "AMOUNT",
  PERCENTAGE = "PERCENTAGE",
}
