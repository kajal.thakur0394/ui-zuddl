export enum QuestionsSortingEnum {
  UPVOTES = "Upvotes",
  LATEST = "Latest",
}

export enum AnswersViewingSortingEnum {
  ALL = "All",
  ANSWERED_WITH_TEXT = "Answered with text",
  MY_QUESTIONS = "My Questions",
}

export enum OrganisersAnswersViewingSortingEnum {
  ALL = "All",
  ANSWERED = "Answered",
  UNANSWERED = "Unanswered",
}
