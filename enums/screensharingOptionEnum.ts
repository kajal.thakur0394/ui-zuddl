export enum ScreenSharingOptions {
  YOUR_SCREEN = "Your Screen",
  WEBSITE = "Website",
  MIRO_WHITEBOARD = "Miro Whiteboard",
  PDF_FILE = "PDF File",
}
