export enum TeamMemberRoleEnum {
  ORGANIZER = "ORGANIZER",
  MODERATOR = "MODERATOR",
}
