enum PlanType {
  ATTENDEE_CUSTOM = "ATTENDEE_CUSTOM",
  CUSTOM = "CUSTOM"
}
export default PlanType;
