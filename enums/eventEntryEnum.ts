enum EventEntryType {
  REG_BASED = "REGISTRATION",
  TICKET_BASED = "TICKETING",
  INVITE_ONLY = "INVITE_ONLY",
  //FLOW=""
}

export default EventEntryType;
