enum BrowserName {
  CHROMIUM = "chromium",
  CHROME = "chrome",
}

export default BrowserName;
