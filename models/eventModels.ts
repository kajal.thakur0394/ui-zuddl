import { EventZonesUpperCase } from "../enums/EventZoneEnum";
import { DataUtl } from "../util/dataUtil";

export class EventInfoModel {
  constructor(
    public readonly eventId: number,
    public readonly eventTitle: string,
    public eventTimeZone?: string
  ) {
    this.eventId = eventId;
    this.eventTitle = eventTitle;
    this.eventTimeZone = eventTimeZone;
  }
  public getSpeakerLandingPageLink() {
    return `${DataUtl.getApplicationTestDataObj()["baseUrl"]}/p/event/${
      this.eventId
    }`;
  }

  public getAttendeeLandingPageLink() {
    return `${DataUtl.getApplicationTestDataObj()["baseUrl"]}/p/a/event/${
      this.eventId
    }`;
  }

  public getZoneUrl(zoneName: EventZonesUpperCase) {
    let zoneUrl: string;
    const baseUrl = `${DataUtl.getApplicationTestDataObj()["baseUrl"]}/l/event`;
    switch (zoneName) {
      case EventZonesUpperCase.LOBBY:
        zoneUrl = `${baseUrl}/${this.eventId}/lobby`;
        break;
      case EventZonesUpperCase.STAGE:
        zoneUrl = `${baseUrl}/${this.eventId}/stages`;
        break;
      case EventZonesUpperCase.EXPO:
        zoneUrl = `${baseUrl}/${this.eventId}/expo`;
        break;
      case EventZonesUpperCase.SCHEDULE:
        zoneUrl = `${baseUrl}/${this.eventId}/schedule`;
        break;
      case EventZonesUpperCase.ROOMS:
        zoneUrl = `${baseUrl}/${this.eventId}/discussions`;
        break;
      case EventZonesUpperCase.NETWORKING:
        zoneUrl = `${baseUrl}/${this.eventId}/networking`;
        break;
      default:
        throw new Error(
          `Invalid zone name ${zoneName} was passed to get the zone url`
        );
    }
    return zoneUrl;
  }
}
