import { EventZonesUpperCase } from "../enums/EventZoneEnum";

export interface AnnouncementPayload {
  announcementNote: string;
  externalUrl?: string | null;
  isRedirectEnabled: boolean;
  redirectTo?: "INTERNAL" | "EXTERNAL" | null;
  redirectionZone: EventZonesUpperCase | null;
  redirectionSubZone?: "" | string;
  targetZones?: Array<EventZonesUpperCase>;
}

export class AnnouncementModel {
  announcementNote: string;
  externalUrl: string | null;
  isRedirectEnabled: boolean;
  redirectionZone: string | EventZonesUpperCase;
  redirectTo: "INTERNAL" | "EXTERNAL" | null;
  redirectionSubZone: string | "";
  targetZones: Array<EventZonesUpperCase>;

  constructor({
    announcementNote,
    externalUrl = null,
    isRedirectEnabled = false,
    redirectionZone = EventZonesUpperCase.LOBBY,
    redirectionSubZone = "",
    redirectTo = "INTERNAL",
    targetZones,
  }: AnnouncementPayload) {
    this.announcementNote = announcementNote;
    this.externalUrl = externalUrl;
    this.isRedirectEnabled = isRedirectEnabled;
    this.redirectionZone = redirectionZone;
    this.redirectTo = redirectTo;
    this.redirectionSubZone = redirectionSubZone;
    this.targetZones = targetZones;
  }

  toJSON() {
    return {
      announcementNote: this.announcementNote,
      externalUrl: this.externalUrl,
      isRedirectEnabled: this.isRedirectEnabled,
      redirectionZone: this.redirectionZone,
      redirectionSubZone: this.redirectionSubZone,
      redirectTo: this.redirectTo,
      targetZones: this.targetZones,
    };
  }
}
