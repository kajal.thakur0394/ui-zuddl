import { AnnouncementModel } from "../models/announcementModel";
import { EventZonesUpperCase } from "../enums/EventZoneEnum";
import { expect, APIRequestContext } from "@playwright/test";

export interface AnnouncementPayload {
  announcementNote: string;
  externalUrl?: string | null;
  isRedirectEnabled: boolean;
  redirectTo?: "INTERNAL" | "EXTERNAL" | null;
  redirectionZone: EventZonesUpperCase | null;
  redirectionSubZone?: "" | string;
  targetZones?: Array<EventZonesUpperCase>;
}

export class AnnouncementController {
  readonly orgApiContext: APIRequestContext;
  readonly eventId: string;
  constructor(orgApiContext: APIRequestContext, eventId: string) {
    this.orgApiContext = orgApiContext;
    this.eventId = eventId;
  }

  async publishAnAnnouncement({
    announcementNote,
    externalUrl,
    isRedirectEnabled,
    redirectionZone,
    redirectionSubZone,
    redirectTo,
    targetZones,
  }: AnnouncementPayload) {
    const announcementModel = new AnnouncementModel({
      announcementNote: announcementNote,
      externalUrl: externalUrl,
      isRedirectEnabled: isRedirectEnabled,
      redirectionZone: redirectionZone,
      redirectionSubZone: redirectionSubZone,
      redirectTo: redirectTo,
      targetZones: targetZones,
    });
    const createAnnouncementApiResp = await this.orgApiContext.post(
      `/api/event/${this.eventId}/announcement`,
      {
        data: announcementModel.toJSON(),
      }
    );
    expect(
      createAnnouncementApiResp.status(),
      "expecting announcement API call to return with 200 status code"
    ).toBe(200);
    return createAnnouncementApiResp;
  }
  async publishAnInternalRedirectionAnnouncement(
    announcementText: string,
    redirectToZone: EventZonesUpperCase = EventZonesUpperCase.LOBBY,
    redirectToSubZone = "",
    targetZones: Array<EventZonesUpperCase> = [
      EventZonesUpperCase.LOBBY,
      EventZonesUpperCase.EXPO,
      EventZonesUpperCase.SCHEDULE,
      EventZonesUpperCase.STAGE,
      EventZonesUpperCase.ROOMS,
      EventZonesUpperCase.NETWORKING,
    ]
  ) {
    const publishAnnouncementApiResp = await this.publishAnAnnouncement({
      announcementNote: announcementText,
      redirectionZone: redirectToZone,
      redirectionSubZone: redirectToSubZone,
      isRedirectEnabled: true,
      redirectTo: "INTERNAL",
      targetZones: targetZones,
      externalUrl: null,
    });
    return publishAnnouncementApiResp;
  }

  async publishAnExtenralRedirectionAnnouncement(
    announcementText: string,
    externalUrl,
    targetZones: Array<EventZonesUpperCase> = [
      EventZonesUpperCase.LOBBY,
      EventZonesUpperCase.EXPO,
      EventZonesUpperCase.SCHEDULE,
      EventZonesUpperCase.STAGE,
      EventZonesUpperCase.ROOMS,
      EventZonesUpperCase.NETWORKING,
    ]
  ) {
    const publishAnnouncementApiResp = await this.publishAnAnnouncement({
      announcementNote: announcementText,
      redirectionZone: null,
      redirectionSubZone: "",
      isRedirectEnabled: true,
      redirectTo: "EXTERNAL",
      targetZones: targetZones,
      externalUrl: externalUrl,
    });
    return publishAnnouncementApiResp;
  }

  async publishAnAnnouncementWithoutRedirection(
    announcementText: string,
    targetZones: Array<EventZonesUpperCase> = [
      EventZonesUpperCase.LOBBY,
      EventZonesUpperCase.EXPO,
      EventZonesUpperCase.SCHEDULE,
      EventZonesUpperCase.STAGE,
      EventZonesUpperCase.ROOMS,
      EventZonesUpperCase.NETWORKING,
    ]
  ) {
    const publishAnnouncementApiResp = await this.publishAnAnnouncement({
      announcementNote: announcementText,
      redirectionZone: null,
      redirectionSubZone: "",
      isRedirectEnabled: false,
      redirectTo: null,
      targetZones: targetZones,
      externalUrl: null,
    });
    return publishAnnouncementApiResp;
  }
}
