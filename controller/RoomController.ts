import { APIRequestContext, APIResponse, test } from "@playwright/test";

export class RoomController {
  constructor(
    readonly orgApiContext: APIRequestContext,
    readonly eventId,
    readonly roomId
  ) {
    this.orgApiContext = orgApiContext;
    this.eventId = eventId;
    this.roomId = roomId;
  }

  async getChannelResponse(): Promise<APIResponse> {
    let getChannelIdApiResp: APIResponse;
    await test.step(`Fetching the channel response for ${this.roomId}`, async () => {
      getChannelIdApiResp = await this.orgApiContext.get(
        `/api/channel?refId=${this.roomId}`
      );
      if (getChannelIdApiResp.status() != 200) {
        throw new Error(
          `API to fetch channelId for room failed with ${getChannelIdApiResp.status()}`
        );
      }
    });
    return getChannelIdApiResp;
  }
  async getChannelId() {
    const getChannelApiResp = await this.getChannelResponse();
    const getChannelIdRespJson = await getChannelApiResp.json();
    const thisRoomChannelId = getChannelIdRespJson["channelId"];
    console.log(
      `The channel id fetched for room ${this.roomId} is ${thisRoomChannelId}`
    );
    return thisRoomChannelId;
  }

  async updateRoomEngaementSettingsForThisRoom({
    enableChat = true,
    enableModeratedChat = false,
    hasPoll = true,
    hasQuestion = true,
    hasQuestionModerate = false,
  }) {
    const getChannelApiResp = await this.getChannelResponse();
    const getChannelApiRespJson = await getChannelApiResp.json();
    const settingsOrder = getChannelApiRespJson["settingsOrder"];
    const channelId = getChannelApiRespJson["channelId"];
    console.log(`setting order fetched is ${settingsOrder}`);
    console.log(`channel id  fetched is ${channelId}`);
    await test.step(`Hitting settings api for this channel to enable chat: ${enableChat} and enable moderated chat ${enableModeratedChat}`, async () => {
      const updateRoomEngagementSettingsApiResp = await this.orgApiContext.post(
        `/api/channel/${channelId}/settings`,
        {
          data: {
            channelId: channelId,
            hasChat: enableChat,
            hasChatModerate: enableModeratedChat,
            hasPoll: hasPoll,
            hasQuestion: hasQuestion,
            hasQuestionModerate: hasQuestionModerate,
            settingsOrder: settingsOrder,
          },
        }
      );

      const updateApiStatus = updateRoomEngagementSettingsApiResp.status();
      if (updateApiStatus != 200) {
        throw new Error(
          `API call to udpate room engagement settings failed with ${updateApiStatus}`
        );
      }
    });
  }
}
