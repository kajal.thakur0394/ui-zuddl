import { APIRequestContext } from "@playwright/test";

export class UserController {
  userRequestContext: APIRequestContext;
  constructor(requestContext: APIRequestContext) {
    this.userRequestContext = requestContext;
  }

  async getUserProfileViewData() {
    /*
    This API call only returns default fields data in the field
    */
    const profileViewResp = await this.userRequestContext.get(
      "/api/account/profile/view"
    );
    if (profileViewResp.status() != 200) {
      throw new Error(
        `API to fetch profile view data failed with ${profileViewResp.status()}`
      );
    }
    return profileViewResp;
  }

  async validateUserProfileViewData(listOfDefaultFieldsDataObj) {
    /*
    This functions calls profile view api and validate only default feilds data with it
    This function needs default feilds test data object to fetch the expected data and compare
    */
    //fetch the profiel view data in an object
    const profileViewDataAsJson = await (
      await this.getUserProfileViewData()
    ).json();
    for (const eachFieldObject of listOfDefaultFieldsDataObj) {
      const fieldName = eachFieldObject.fieldName;
      const fieldType = eachFieldObject.fieldType;
      let expectedData;
      let actualData;
      if (fieldType == "text" || fieldType == "numeric") {
        expectedData = eachFieldObject.dataToEnter;
      } else if (fieldType == "dropdown") {
        if (fieldName == "industry") {
          expectedData = await this.fetchIndustryId(
            eachFieldObject.dropDownOptionToSelect
          );
        } else {
          expectedData = eachFieldObject.dropDownOptionToSelect;
        }
      }
      console.log(`expected  data for ${fieldName} is ${expectedData}`);
      if (!eachFieldObject.toSkip) {
        console.log(`comparing data for ${fieldName}`);
        if (
          fieldName == "twitter" ||
          fieldName == "facebook" ||
          fieldName == "linkedin" ||
          fieldName == "website"
        ) {
          const socialLinkData = profileViewDataAsJson.socialLinks;
          for (const eachSocialLinkData of socialLinkData) {
            if (eachSocialLinkData.type.toLowerCase() == fieldName) {
              actualData = eachSocialLinkData.url;
              break;
            }
          }
        } else {
          actualData = profileViewDataAsJson[fieldName];
        }
        if (actualData.toLowerCase() != expectedData.toLowerCase()) {
          throw new Error(
            `Edit profile data not matched for ${fieldName}, expected = ${expectedData} and actual = ${actualData}`
          );
        }
      }
    }
  }

  async fetchIndustryId(industryName: string) {
    let industryId = undefined;
    let industriesResp = await this.userRequestContext.get(
      "/api/account/profile/industries"
    );
    if (industriesResp.status() != 200) {
      throw new Error(
        `API to fetch industries id failed with ${industriesResp.status()}`
      );
    }
    let industriesRespJson = await industriesResp.json();
    for (const industryObj of industriesRespJson) {
      if (industryObj.name.toLowerCase() == industryName.toLowerCase()) {
        industryId = industryObj.industryId;
        break;
      }
    }
    return industryId;
  }
}
