import { APIRequestContext } from "@playwright/test";
import { DataUtl } from "../util/dataUtil";
import PollStatus from "../enums/PollStatusEnum";

export class PollsOrQuizController {
  readonly orgApiContext: APIRequestContext;
  readonly eventId;

  constructor(orgApiContext: APIRequestContext, eventId) {
    this.orgApiContext = orgApiContext;
    this.eventId = eventId;
  }

  async createNewPollOrQuiz(
    channelId: string,
    isQuiz: boolean,
    options: Array<PollOptions>,
    pollTitle: string,
    publishedStatus: PollStatus,
    correctAnswerOption?: string
  ) {
    const pollCreateApiResp = await this.orgApiContext.post(
      `/api/channel/${channelId}/poll`,
      {
        data: {
          isQuiz: isQuiz,
          options: options,
          prompt: pollTitle,
          status: publishedStatus,
        },
      }
    );
    if (pollCreateApiResp.status() != 200) {
      throw new Error(
        `API call to create new Poll failed with ${pollCreateApiResp.status()}`
      );
    }
    return pollCreateApiResp;
  }
  async createNewPollInDraft(
    channelId: string,
    {
      isQuiz = false,
      options = [{ text: "option1" }, { text: "option2" }],
      prompt = `Poll ${DataUtl.getRandomName()}`,
      status = PollStatus.SCHEDULED,
    }: PollPayload
  ) {
    const createPollInDraftResp = await this.createNewPollOrQuiz(
      channelId,
      isQuiz,
      options,
      prompt,
      status
    );
    return createPollInDraftResp;
  }

  async createNewPollInPublish(
    channelId: string,
    {
      isQuiz = false,
      options = [{ text: "option1" }, { text: "option2" }],
      prompt = `Poll ${DataUtl.getRandomName()}`,
      status = PollStatus.LIVE,
    }: PollPayload
  ) {
    const createPollInPublishResp = await this.createNewPollOrQuiz(
      channelId,
      isQuiz,
      options,
      prompt,
      status
    );
    let publisResponseJson = await createPollInPublishResp.json();
    return publisResponseJson;
  }

  async createNewQuizInDraft(
    channelId: string,
    {
      isQuiz = true,
      options = [{ text: "option1" }, { text: "option2" }],
      prompt = `Poll ${DataUtl.getRandomName()}`,
      status = PollStatus.SCHEDULED,
      correctAnswerOption = "option1",
    }: PollPayload
  ) {
    const createQuizInDraftResp = await this.createNewPollOrQuiz(
      channelId,
      isQuiz,
      options,
      prompt,
      status,
      correctAnswerOption
    );
    return createQuizInDraftResp;
  }

  async createNewQuizInPublish(
    channelId: string,
    {
      isQuiz = true,
      options = [{ text: "option1" }, { text: "option2" }],
      prompt = `Poll ${DataUtl.getRandomName()}`,
      status = PollStatus.LIVE,
      correctAnswerOption = "option1",
    }: PollPayload
  ) {
    const createQuizInPublishResp = await this.createNewPollOrQuiz(
      channelId,
      isQuiz,
      options,
      prompt,
      status,
      correctAnswerOption
    );
    return createQuizInPublishResp;
  }

  async publishADraftedPollOrQuiz(pollOrQuizId: string) {
    const publishDraftedPollOrQuizApiResp = await this.orgApiContext.patch(
      `/api/poll/${pollOrQuizId}/publish`
    );
    if (publishDraftedPollOrQuizApiResp.status() != 200) {
      throw new Error(
        `Publishing quiz or poll with id ${pollOrQuizId} failed with ${publishDraftedPollOrQuizApiResp.status()}`
      );
    }
  }

  async closePollOrQuiz(pollOrQuizId: string) {
    const closePollOrQuizApiResp = await this.orgApiContext.post(
      `/api/poll/${pollOrQuizId}/close`
    );
    if (closePollOrQuizApiResp.status() != 200) {
      throw new Error(
        `Closing quiz or poll with id ${pollOrQuizId} failed with ${closePollOrQuizApiResp.status()}`
      );
    }
  }

  async closeNhidePollOrQuiz(pollOrQuizId: string) {
    const closeNhidePollOrQuizApiResp = await this.orgApiContext.post(
      `/api/poll/${pollOrQuizId}/close-n-hide`
    );
    if (closeNhidePollOrQuizApiResp.status() != 200) {
      throw new Error(
        `Closing and hiding quiz or poll with id ${pollOrQuizId} failed with ${closeNhidePollOrQuizApiResp.status()}`
      );
    }
  }

  async deleteDraftedPollOrQuiz(pollOrQuizId: string) {
    const deletedPollOrQuizApiResp = await this.orgApiContext.delete(
      `/api/poll/${pollOrQuizId}`
    );
    if (deletedPollOrQuizApiResp.status() != 200) {
      throw new Error(
        `Deleting quiz or poll with id ${pollOrQuizId} failed with ${deletedPollOrQuizApiResp.status()}`
      );
    }
  }

  async updateADraftedPollOrQuiz(
    pollOrQuizId: string,
    { isQuiz, options, prompt, status }: PollPayload
  ) {
    const editPollOrQuizApiResp = await this.orgApiContext.post(
      `/api/poll/${pollOrQuizId}`,
      {
        data: {
          isQuiz: isQuiz,
          options: options,
          prompt: prompt,
          status: status,
        },
      }
    );
    let responseJson = await editPollOrQuizApiResp.json();
    console.log(responseJson);

    if (editPollOrQuizApiResp.status() != 200) {
      throw new Error(
        `API call to update drafted poll or quiz failed with ${editPollOrQuizApiResp.status()}`
      );
    }
  }

  async getListOfPollOrQuizzes(channelId) {
    const listOfPollsResp = await this.orgApiContext.get(
      `/api/channel/${channelId}/poll/list`
    );
    if (listOfPollsResp.status() != 200) {
      throw new Error(
        `API call to get list of polls/quizzes failed with status ${listOfPollsResp.status()}`
      );
    }
    return listOfPollsResp;
  }
  async getPollFromList(channelId: string, pollTitle: string) {
    const listOfPollsResp = await this.getListOfPollOrQuizzes(channelId);
    const listOfPollsRespJson = await listOfPollsResp.json();
    let pollDetailsJson;
    for (const eachPollObject of listOfPollsRespJson) {
      if (eachPollObject["prompt"] === pollTitle) {
        pollDetailsJson = await eachPollObject;
        return pollDetailsJson;
      }
    }
  }

  async getCountOfPollsAndQuizzes(channelId) {
    const pollNQuizzCountContainer = new Map<String, number>([
      ["countOfPollsNQuizzes", 0],
      ["countOfPolls", 0],
      ["countOfQuizzes", 0],
    ]);
    const listOfPollsResp = await this.getListOfPollOrQuizzes(channelId);
    const listOfPollsRespJson = await listOfPollsResp.json();
    pollNQuizzCountContainer["countOfPollsNQuizzes"] =
      listOfPollsRespJson.length();
    for (const eachPollObject of listOfPollsRespJson) {
      if (!eachPollObject["isQuiz"]) {
        pollNQuizzCountContainer["countOfPolls"] =
          pollNQuizzCountContainer["countOfPolls"] + 1;
      } else {
        pollNQuizzCountContainer["countOfPolls"] =
          pollNQuizzCountContainer["countOfQuizzes"] + 1;
      }
    }
    return pollNQuizzCountContainer;
  }

  async getCountOfVotedPoll(channelId: string, pollTitle: string) {
    let totalVotes = 0;
    const listOfPollsResp = await this.getListOfPollOrQuizzes(channelId);
    const listOfPollsRespJson = await listOfPollsResp.json();
    for (const eachPollObject of listOfPollsRespJson) {
      if (eachPollObject["prompt"] === pollTitle) {
        for (const eachOption of eachPollObject["options"]) {
          totalVotes += eachOption["count"];
        }
      }
    }
    return totalVotes;
  }

  async getPollId(channelId: string, pollTitle: string) {
    let pollId: string = "";
    const listOfPollsResp = await this.getListOfPollOrQuizzes(channelId);
    const listOfPollsRespJson = await listOfPollsResp.json();
    for (const eachPollObject of listOfPollsRespJson) {
      if (eachPollObject["prompt"] === pollTitle) {
        pollId = eachPollObject["pollId"];
      }
    }
    return pollId;
  }

  async getOptions(channelId: string, pollTitle: string) {
    let option: PollOptions[] = [];
    const listOfPollsResp = await this.getListOfPollOrQuizzes(channelId);
    const listOfPollsRespJson = await listOfPollsResp.json();
    for (const optionObject of listOfPollsRespJson) {
      if (optionObject["prompt"] === pollTitle) {
        for (const opt of optionObject["options"]) {
          option.push(opt);
        }
      }
    }
    console.log(option);
    return option;
  }

  async updateExistingOption(
    channelId: string,
    pollTitle: string,
    optionName: string,
    optionToAdd: PollOptions
  ) {
    let options = await this.getOptions(channelId, pollTitle);
    let updatedOptionList: PollOptions[] = [];
    for (let option of options) {
      if (option["text"] == optionName) {
        // option.pollOptionId = option["pollOptionId"];
        option.text =
          optionToAdd.text !== undefined ? optionToAdd.text : option.text;
        option.count =
          optionToAdd.count !== undefined ? optionToAdd.count : option.count;
        option.hasVoted =
          optionToAdd.hasVoted !== undefined
            ? optionToAdd.hasVoted
            : option.hasVoted;
      }
      updatedOptionList.push(option);
    }
    console.log(updatedOptionList);
    return updatedOptionList;
  }

  async addNewOption(
    channelId: string,
    pollTitle: string,
    optionToAdd: PollOptions
  ) {
    let options = await this.getOptions(channelId, pollTitle);
    options.push(optionToAdd);
    return options;
  }

  async getPollDetails(channelId: string, pollTitle: string) {
    const listOfPollsResp = await this.getListOfPollOrQuizzes(channelId);
    const listOfPollsRespJson = await listOfPollsResp.json();
    for (const optionObject of listOfPollsRespJson) {
      if (optionObject["prompt"] === pollTitle) {
        console.log(optionObject);
        return optionObject;
      }
    }
  }

  async closePoll(pollId: string) {
    const listOfPollsResp = await this.orgApiContext.post(
      `api/poll/${pollId}/close`
    );
    if (listOfPollsResp.status() != 200) {
      throw new Error(
        `API call to close the poll failed with status ${listOfPollsResp.status()}`
      );
    }
  }
}

interface PollOptions {
  pollOptionId?: number;
  text: string;
  count?: number;
  hasVoted?: boolean;
}

interface PollPayload {
  pollId?: string;
  isQuiz?: boolean;
  options?: Array<PollOptions>;
  prompt?: string;
  status?: PollStatus;
  correctAnswerOption?: string;
}
