import { APIRequestContext, APIResponse, Page, expect } from "@playwright/test";
import EventType from "../enums/eventTypeEnum";
import { FlowBuilderController } from "./FlowBuilderController";
import {
  CouponCreationPayload,
  TicketCreationPayload,
} from "../interfaces/FlowBuilerControllerInterface";
import { DiscountType } from "../enums/FlowTypeEnum";
import { QueryUtil } from "./../DB/QueryUtil";
import { randomUUID } from "crypto";

export class TicketingController extends FlowBuilderController {
  constructor(orgApiRequestContext: APIRequestContext, eventId: any) {
    super(orgApiRequestContext, eventId);
    this.orgApiRequest = orgApiRequestContext;
    this.eventId = eventId;
  }

  async getGateways() {
    const response = await this.orgApiRequest.get(
      `/api/event/${this.eventId}/payments/gateways`
    );
    if (response.status() !== 200) {
      throw new Error(
        `Failed to get gateways for event ${
          this.eventId
        }. Response: ${response.status()}`
      );
    }

    const gatewaysJson = await response.json();
    return gatewaysJson;
  }

  async selectGateway(gatewayId?: string) {
    if (!gatewayId) {
      const gateways = await this.getGateways();
      // gatewayId = gateways[0].paymentGatewayId;
      gatewayId = gateways[gateways.length - 1].paymentGatewayId;
    }
    const response = await this.orgApiRequest.post(
      `/api/event/${this.eventId}/payments/gateways/${gatewayId}`
    );
    if (response.status() !== 200) {
      throw new Error(
        `Failed to select gateway ${gatewayId} for event ${
          this.eventId
        }. Response: ${response.status()}`
      );
    }
  }

  async createTicket({
    name = "ticket1",
    description = null,
    autoGenerateCode = false,
    code = null,
    numberOfTickets = 3,
    pricePerTicket = 0,
    minTicketsPerOrder = 1,
    maxTicketsPerOrder = 1,
    isBulkPurchaseEnabled = false,
    isHide = false,
    isLocked = false,
    salesStartDate = new Date().toISOString().split(".")[0] + "Z",
    salesEndDate = new Date(new Date().setDate(new Date().getDate() + 1))
      .toISOString()
      .split(".")[0] + "Z",
    active = true,
    isAvailableForAll = true,
    isInvitedList = false,
    isInvitedListForAll = false,
    isDomainRestriction = false,
    isPaidTicket = false,
    domainRestrictionCsvFileName = "",
    audienceGroupIds = [],
    domains = null,
    ticketTagType = "IN_PERSON",
    linkedTicketTypeIds = [],
  }) {
    const ticketCreationPayload: TicketCreationPayload = {
      name,
      description,
      autoGenerateCode,
      code,
      numberOfTickets,
      pricePerTicket,
      minTicketsPerOrder,
      maxTicketsPerOrder,
      isBulkPurchaseEnabled,
      isHide,
      isLocked,
      salesStartDate,
      salesEndDate,
      active,
      isAvailableForAll,
      isInvitedList,
      isInvitedListForAll,
      isDomainRestriction,
      domainRestrictionCsvFileName,
      audienceGroupIds,
      domains,
      ticketTagType,
      isPaidTicket,
      linkedTicketTypeIds,
    };

    console.log("ticketCreationPayload", ticketCreationPayload);
    const response = await this.orgApiRequest.post(
      `/api/event/${this.eventId}/ticket-type/v2`,
      {
        data: ticketCreationPayload,
      }
    );
    if (response.status() !== 200) {
      throw new Error(
        `Failed to create ticket for event ${
          this.eventId
        }. Response: ${response.status()}`
      );
    }
    const ticket = await response.json();
    return ticket;
  }

  async getAllTickets() {
    const response = await this.orgApiRequest.get(
      `/api/event/${this.eventId}/ticket-type/v2`
    );
    if (response.status() !== 200) {
      throw new Error(
        `Failed to get all tickets for event ${
          this.eventId
        }. Response: ${response.status()}`
      );
    }
    const tickets = await response.json();
    return tickets;
  }

  async getTicketInfo(ticketName: string) {
    let tickets = await this.getAllTickets();
    for (let ticket of tickets) {
      if (ticket.name === ticketName) {
        return ticket;
      }
    }
  }

  async addMoreTickets(ticketName: string, numberOfTickets: number) {
    let ticketInfo = await this.getTicketInfo(ticketName);
    ticketInfo.numberOfTickets += numberOfTickets;
    const response = await this.orgApiRequest.patch(
      `/api/event/${this.eventId}/ticket-type/v2`,
      {
        data: ticketInfo,
      }
    );
    if (response.status() !== 200) {
      throw new Error(
        `Failed to add more tickets for event ${
          this.eventId
        }. Response: ${response.status()}`
      );
    }
  }

  async verifyVivenuTransactionDetails(
    email: string,
    eventId: string,
    attendeePage: Page,
    transactionDetails: any = {
      total_tickets: 3,
      city: "Bhopal",
      company: null,
      currency: "USD",
      email: "attendee_1702884941756_845@1gb4osi2.mailosaur.net",
      postal: "111",
      regular_price: 0,
      last_name: "zuddl",
      applied_coupons: null,
      first_name: "Jxtin",
      street: "Random Home",
      price: 0,
    }
  ) {
    // let transactionData = await new DBUtil(
    //   attendeePage
    // ).fetchVivenuTransactionRecord(eventId, email);

    let transactionData = await QueryUtil.fetchVivenuTransactionRecord(
      eventId,
      email
    );
    console.log(`transactionData ${JSON.stringify(transactionData)}`);
    expect(transactionData).toBeDefined();
    console.log(`transactionData ${JSON.stringify(transactionData)}`);
    console.log(`transactionDetails ${JSON.stringify(transactionDetails)}`);

    if (transactionDetails.price === 0) {
      // remove street, city, postal from both the objects
      delete transactionDetails.street;
      delete transactionDetails.city;
      delete transactionDetails.postal;

      delete transactionData.street;
      delete transactionData.city;
      delete transactionData.postal;
    }
    // deep compare
    for (let key in transactionDetails) {
      // null and empty string are treated as same
      if (transactionDetails[key] === null) {
        transactionDetails[key] = "";
      }
      if (transactionData[key] === null) {
        transactionData[key] = "";
      }
      expect(transactionData[key]).toEqual(transactionDetails[key]);
    }
  }

  async getTicketTypeId(ticketName: string): Promise<string[]> {
    let ticket = await this.getTicketInfo(ticketName);
    return ticket["ticketTypeId"];
  }

  async addCoupons({
    couponId = null,
    couponCode = "100OFF",
    description = "coupon description",
    active = false,
    discountType = DiscountType.AMOUNT,
    singleUsage = false,
    discountValue = 10,
    maxDiscount = null,
    maxUsageCount = 5,
    maxTickets = 5, //could be string
    ticketTypeIds = [],
    isAvailableForAllTickets = false,
    isAvailableForEveryone = false,
    isInvitedList = false,
    isInvitedListForAll = false,
    isDomainRestriction = false,
    domainRestrictionCsvFileName = "",
    audienceGroupIds = [],
    domains = null,
  }) {
    const couponCreationPayload: CouponCreationPayload = {
      couponId,
      couponCode,
      description,
      active,
      discountType,
      singleUsage,
      discountValue,
      maxDiscount,
      maxUsageCount,
      maxTickets,
      ticketTypeIds,
      isAvailableForAllTickets,
      isAvailableForEveryone,
      isInvitedList,
      isInvitedListForAll,
      isDomainRestriction,
      domainRestrictionCsvFileName,
      audienceGroupIds,
      domains,
    };
    // // Add a 5-second timeout
    // await new Promise((resolve) => setTimeout(resolve, 5000));
    // console.log("here");
    const response = await this.orgApiRequest.post(
      `/api/event/${this.eventId}/coupon`,
      {
        data: couponCreationPayload,
      }
    );
    if (response.status() !== 200) {
      throw new Error(
        `Failed to create coupon for event ${
          this.eventId
        }. Response: ${response.status()}`
      );
    }
    const coupon = await response.json();
    return coupon;
  }

  async getAllTransactions() {
    const response = await this.orgApiRequest.get(
      `/api/event/${this.eventId}/transactions`
    );
    if (response.status() !== 200) {
      throw new Error(
        `Failed to get all transactions for event ${
          this.eventId
        }. Response: ${response.status()}`
      );
    }
    const transactions = await response.json();
    return transactions;
  }

  async cancelTransaction(transactionId: string, refund: boolean = false) {
    const response = await this.orgApiRequest.post(
      `/api/event/${this.eventId}/transactions/${transactionId}/cancel`,
      {
        data: {
          refundFunds: refund,
        },
      }
    );
    if (response.status() !== 200) {
      throw new Error(
        `Failed to cancel transaction for event ${
          this.eventId
        }. Response: ${response.status()}`
      );
    }
  }

  async cancelTransactionFromEmail(email: string, refund: boolean = false) {
    let transactions = await this.getAllTransactions();
    for (let transaction of transactions) {
      if (transaction.email === email) {
        // verify if the transaction is not already cancelled
        expect(
          transaction.vivenuStatus,
          "Expecting status to not be cancelled already"
        ).not.toBe("CANCELED");
        await this.cancelTransaction(transaction.vivenuTransactionId, refund);
        break;
      }
    }
    transactions = await this.getAllTransactions();
    for (let transaction of transactions) {
      if (transaction.email === email) {
        expect(
          transaction.vivenuStatus,
          "Expecting status to be cancelled"
        ).toBe("CANCELED");
        return;
      }
    }
  }

  async getTransactionDetails(transactionId: string) {
    const response = await this.orgApiRequest.get(
      `/api/event/${this.eventId}/transactions/${transactionId}/details`
    );
    if (response.status() !== 200) {
      throw new Error(
        `Failed to get transaction details for event ${
          this.eventId
        }. Response: ${response.status()}`
      );
    }
    const transaction = await response.json();
    return transaction;
  }

  async confirmTransaction(
    transactionId: string,
    {
      paymentGatewayType = "BANK_TRANSFER",
      paymentStatus = "RECEIVED",
      note = "",
      paidAt = new Date().toISOString().split(".")[0] + "Z",
    }
  ) {
    // let payload = {
    //   paidAt: "2024-03-14T18:00:00+05:30",
    //   paymentStatus: "RECEIVED",
    //   note: "",
    //   paymentGatewayType: "BANK_TRANSFER",
    //   bankTransferDetails: { bankName: "bankname", referenceNumber: "refnum" },
    // };

    // let payload = {
    //   paidAt: "2024-03-14T19:00:00+05:30",
    //   paymentStatus: "RECEIVED",
    //   note: "",
    //   paymentGatewayType: "CASH",
    // };

    let payload = {};
    if (paymentGatewayType === "BANK_TRANSFER") {
      payload = {
        paidAt,
        paymentStatus,
        note,
        paymentGatewayType,
        bankTransferDetails: {
          bankName: "bankname",
          referenceNumber: "refnum",
        },
      };
    } else if (paymentGatewayType === "CASH") {
      payload = {
        paidAt,
        paymentStatus,
        note,
        paymentGatewayType,
      };
    }

    const response = await this.orgApiRequest.patch(
      `/api/event/${this.eventId}/transactions/${transactionId}/payment`,
      {
        data: payload,
      }
    );

    if (response.status() !== 200) {
      throw new Error(
        `Failed to confirm transaction for event ${
          this.eventId
        }. Response: ${response.status()}`
      );
    }
  }

  async setupLinearTicketConditioning(
    namedTicketName: string,
    emailTicketName: string,
    luckyTicketName: string,
    countryTicketName: string,
    hiddenTicketName: string,
    lockedTicketName: string,
    flowName: string = "Basic registration flow"
  ) {
    let tickets = await this.getAllTickets();

    // find the ticket ids
    let namedTicket = tickets.find((ticket) => ticket.name === namedTicketName);
    let emailTicket = tickets.find((ticket) => ticket.name === emailTicketName);
    let luckyTicket = tickets.find((ticket) => ticket.name === luckyTicketName);
    let countryTicket = tickets.find(
      (ticket) => ticket.name === countryTicketName
    );
    let hiddenTicket = tickets.find(
      (ticket) => ticket.name === hiddenTicketName
    );
    let lockedTicket = tickets.find(
      (ticket) => ticket.name === lockedTicketName
    );

    let flowDataResponse = await this.orgApiRequest.get(
      `/api/event/${this.eventId}/flow/published/list/data`
    );
    if (flowDataResponse.status() !== 200) {
      throw new Error(
        `Failed to get flow data for event ${
          this.eventId
        }. Response: ${flowDataResponse.status()}`
      );
    }

    let flowData = await flowDataResponse.json();

    flowData = flowData.filter((flow) => flow.name === flowName);
    if (flowData.length === 0) {
      throw new Error(`Flow with name ${flowName} not found`);
    }

    let flowId = flowData[0].flowId;

    let referenceStepIds = {};
    console.log(flowData[0].flowJson);
    console.log(flowData[0].flowJson.steps);
    referenceStepIds["TKT by name"] = flowData[0].flowJson.steps.find(
      (step) => step.name === "Hello"
    ).id;

    let firstNameFieldId = flowData[0].flowJson.steps
      .find((step) => step.name === "Hello")
      .config.form.fields.find((field) => field.label === "First name").fieldId;

    let lastNameFieldId = flowData[0].flowJson.steps
      .find((step) => step.name === "Hello")
      .config.form.fields.find((field) => field.label === "Last name").fieldId;

    let emailFieldId = flowData[0].flowJson.steps
      .find((step) => step.name === "Hello")
      .config.form.fields.find(
        (field) => field.label === "Email address"
      ).fieldId;

    referenceStepIds["TKT by email"] = flowData[0].flowJson.steps.find(
      (step) => step.name === "Hello"
    ).id;

    referenceStepIds["Lucky TKT"] = flowData[0].flowJson.steps.find(
      (step) => step.name === "Are you lucky?"
    ).id;

    let luckyNumberFieldId = flowData[0].flowJson.steps
      .find((step) => step.name === "Are you lucky?")
      .config.form.fields.find(
        (field) => field.label === "Roll a dice"
      ).fieldId;

    referenceStepIds["Country TKT"] = flowData[0].flowJson.steps.find(
      (step) => step.name === "Nationality Check"
    ).id;

    let countryFieldId = flowData[0].flowJson.steps
      .find((step) => step.name === "Nationality Check")
      .config.form.fields.find((field) => field.label === "Country").fieldId;

    namedTicket.isAvailableForAll = false;
    namedTicket.isFlowConditions = true;
    namedTicket.flowConditionsConfig = {
      flowConditions: [
        {
          order: 1,
          flowConditionId: randomUUID(), //generate uuid,
          flowConditionName: "Flow condition 1",
          conditionOperator: "AND",
          flowId: flowId,
          conditions: [
            {
              id: firstNameFieldId,
              referenceStepId: referenceStepIds["TKT by name"], // need to get this from the flow
              type: "STRING",
              operator: "EQUAL_TO",
              conditionId: randomUUID(),
              conditionType: "STANDARD",
              compareValue: "jxtin", // some standard value
              customConditionData: null,
            },
            {
              conditionId: randomUUID(),
              conditionType: "STANDARD",
              referenceStepId: referenceStepIds["TKT by name"],
              id: lastNameFieldId,
              type: "STRING",
              operator: "EQUAL_TO",
              compareValue: "me", // some standard value
            },
          ],
        },
      ],
    };

    emailTicket.isAvailableForAll = false;
    emailTicket.isFlowConditions = true;
    emailTicket.flowConditionsConfig = {
      flowConditions: [
        {
          flowConditionId: randomUUID(),
          flowConditionName: "Flow condition 1",
          conditionOperator: "OR",
          conditions: [
            {
              conditionId: randomUUID(),
              conditionType: "STANDARD",
              referenceStepId: referenceStepIds["TKT by email"],
              id: emailFieldId,
              type: "STRING",
              operator: "CONTAINS",
              compareValue: "zuddl.com",
            },
            {
              conditionId: randomUUID(),
              conditionType: "STANDARD",
              referenceStepId: referenceStepIds["TKT by email"],
              id: emailFieldId,
              type: "STRING",
              operator: "CONTAINS",
              compareValue: "zudddl.com", // extra d
            },
            {
              conditionId: randomUUID(),
              conditionType: "STANDARD",
              referenceStepId: referenceStepIds["Lucky TKT"],
              id: luckyNumberFieldId,
              type: "NUMBER",
              operator: "EQUAL_TO",
              compareValue: 2,
            },
          ],
          order: 1,
          flowId: flowId,
        },
      ],
    };

    luckyTicket.isAvailableForAll = false;
    luckyTicket.isFlowConditions = true;
    luckyTicket.flowConditionsConfig = {
      flowConditions: [
        {
          flowConditionId: randomUUID(),
          flowConditionName: "Flow condition 1",
          conditionOperator: "AND",
          conditions: [
            {
              conditionId: randomUUID(),
              conditionType: "STANDARD",
              referenceStepId: referenceStepIds["Lucky TKT"],
              id: luckyNumberFieldId,
              type: "NUMBER",
              operator: "EQUAL_TO",
              compareValue: 4,
            },
          ],
          order: 1,
          flowId: flowId,
        },
      ],
    };

    countryTicket.isAvailableForAll = false;
    countryTicket.isFlowConditions = true;
    countryTicket.flowConditionsConfig = {
      flowConditions: [
        {
          flowConditionId: randomUUID(),
          flowConditionName: "Flow condition 1",
          conditionOperator: "AND",
          conditions: [
            {
              conditionId: randomUUID(),
              conditionType: "STANDARD",
              referenceStepId: referenceStepIds["Country TKT"],
              id: countryFieldId,
              type: "STRING",
              operator: "CONTAINS",
              compareValue: "india",
            },
          ],
          order: 1,
          flowId: flowId,
        },
      ],
    };

    hiddenTicket.isAvailableForAll = false;
    hiddenTicket.isFlowConditions = true;
    hiddenTicket.flowConditionsConfig = {
      flowConditions: [
        {
          flowConditionId: randomUUID(),
          flowConditionName: "Flow condition 1",
          conditionOperator: "AND",
          conditions: [
            {
              conditionId: randomUUID(),
              conditionType: "STANDARD",
              referenceStepId: referenceStepIds["Country TKT"],
              id: countryFieldId,
              type: "STRING",
              operator: "CONTAINS",
              compareValue: "russia",
            },
          ],
          order: 1,
          flowId: flowId,
        },
      ],
    };

    lockedTicket.isAvailableForAll = false;
    lockedTicket.isFlowConditions = true;
    lockedTicket.flowConditionsConfig = {
      flowConditions: [
        {
          flowConditionId: randomUUID(),
          flowConditionName: "Flow condition 1",
          conditionOperator: "AND",
          conditions: [
            {
              conditionId: randomUUID(),
              conditionType: "STANDARD",
              referenceStepId: referenceStepIds["Country TKT"],
              id: countryFieldId,
              type: "STRING",
              operator: "CONTAINS",
              compareValue: "china",
            },
          ],
          order: 1,
          flowId: flowId,
        },
      ],
    };

    // patch the tickets

    let response = await this.orgApiRequest.patch(
      `/api/event/${this.eventId}/ticket-type/v2`,
      {
        data: namedTicket,
      }
    );

    if (response.status() !== 200) {
      throw new Error(
        `Failed to patch named ticket for event ${
          this.eventId
        }. Response: ${response.status()}`
      );
    }

    response = await this.orgApiRequest.patch(
      `/api/event/${this.eventId}/ticket-type/v2`,
      {
        data: emailTicket,
      }
    );

    if (response.status() !== 200) {
      throw new Error(
        `Failed to patch email ticket for event ${
          this.eventId
        }. Response: ${response.status()}`
      );
    }

    response = await this.orgApiRequest.patch(
      `/api/event/${this.eventId}/ticket-type/v2`,
      {
        data: luckyTicket,
      }
    );

    if (response.status() !== 200) {
      throw new Error(
        `Failed to patch lucky ticket for event ${
          this.eventId
        }. Response: ${response.status()}`
      );
    }

    response = await this.orgApiRequest.patch(
      `/api/event/${this.eventId}/ticket-type/v2`,
      {
        data: countryTicket,
      }
    );
  }
}
