import { APIRequestContext, APIResponse, test } from "@playwright/test";
import { QnAControllerInterface } from "../interfaces/QnAControllerInterface";

export class QnAController implements QnAControllerInterface {
    readonly orgApiRequest: APIRequestContext;
    readonly eventId;
    readonly channelId: number;

    constructor(orgApiRequest: APIRequestContext, eventId, channelId) {
        this.orgApiRequest = orgApiRequest;
        this.eventId = eventId;
        this.channelId = channelId;
    }


    async postAQuestion(questionText: string): Promise<APIResponse> {
        let addquestion = await this.orgApiRequest.post(`/api/channel/${this.channelId}/question`,
            {
                data: {
                    text: questionText
                }
            }
        );

        const addquestionStatus = addquestion.status();
        if (addquestionStatus != 200) {
            throw new Error(
                `API call to add question failed with ${addquestionStatus}`
            );
        }
        let questionResponseJson = await addquestion.json();
        return addquestion;
    }

    async getQuestionList(): Promise<APIResponse> {
        let questionList = await this.orgApiRequest.get(`/api/channel/${this.channelId}/questions`);
        if (questionList.status() != 200) {
            throw new Error(
                `API to fetch questionList for qna failed with ${questionList.status()}`
            );
        }
        let questionListResponse = await questionList.json();
        return questionList;
    }

    async getUpvoteCount(questionText: string) {
        let questionList = await (await this.getQuestionList()).json();
        for (const question of questionList) {
            if (question['question'] == questionText) {
                return question['upvotes'];
                break;
            }
        }
    }

    async getQuestionId(questionText: string) {
        let questionListResponse = await this.getQuestionList();
        let questionListResponseJson = await questionListResponse.json();
        for (const question of questionListResponseJson) {
            if (question.question === questionText) {
                return question['questionId'];
            }
        }
    }

    async deleteAQuestion(questionText: string): Promise<void> {
        let questionId = await this.getQuestionId(questionText);
        let deleteQuestion = await this.orgApiRequest.delete(`/api/question/${questionId}/delete?isPinned=false`);
        let deleteStatus = deleteQuestion.status();
        if (deleteStatus != 200) {
            throw new Error(
                `API to delete queston for qna failed with ${deleteStatus}`
            );
        }
    }

    async upvoteAQuestion(questionText: string): Promise<string> {
        let questionId = await this.getQuestionId(questionText);
        let upvoteQuestion = await this.orgApiRequest.post(`/api/question/${questionId}/upvote`);

        const upvoteQuestionStatus = upvoteQuestion.status();
        if (upvoteQuestionStatus != 200) {
            throw new Error(
                `API call to upvote question failed with ${upvoteQuestionStatus}`
            );
        }
        let questionResponseJson = await upvoteQuestion.json();
        return questionResponseJson['updatedAt'];

    }

    async replyAQuestion(questionText: string, replyText: string) {
        let questionId = await this.getQuestionId(questionText);
        let addquestion = await this.orgApiRequest.post(`/api/question/${questionId}/answered`,
            {
                data: {
                    answer: replyText
                }
            }
        );

        const addquestionStatus = addquestion.status();
        if (addquestionStatus != 200) {
            throw new Error(
                `API call to add question failed with ${addquestionStatus}`
            );
        }
        let questionResponseJson = await addquestion.json();
        return addquestion;
    }

    async approveOrRejectQuestions(questionIdList: string[], status: string) {
        let approveQuestions = await this.orgApiRequest.post(`api/channel/${this.channelId}/questions/bulk?moderateQuestionStatus=${status}`,
            {
                data: questionIdList
            }
        );

        const approveQuestionStatus = approveQuestions.status();
        if (approveQuestionStatus != 200) {
            throw new Error(
                `API call to approve questions failed with ${approveQuestionStatus}`
            );
        }
    }

    async verifyQuestionModeratedStatuse(questiontext: string, status: string) {
        let questionList = await this.getQuestionList();
        let questionListResponseJson = await questionList.json();
        for (const question of questionListResponseJson) {
            if (question['question'] === questiontext) {
                return question['moderateQuestionStatus']
            }
        }
    }
}