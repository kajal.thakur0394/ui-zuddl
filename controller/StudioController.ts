import { DataUtl } from "../util/dataUtil";
import { APIRequestContext, expect } from "@playwright/test";

export class StudioController {
  readonly orgApiRequestContext: APIRequestContext;
  readonly studioId;
  readonly stageId;

  constructor(orgApiRequestContext: APIRequestContext, studioId, stageId?) {
    this.orgApiRequestContext = orgApiRequestContext;
    this.studioId = studioId;
    this.stageId = stageId;
  }

  async stopSessionOnStudio() {
    const endSessionUrl = `${
      DataUtl.getApplicationTestDataObj()["studioBaseUrl"]
    }/api/studio/${this.studioId}/live/end`;
    const endStudioSessionResp = await this.orgApiRequestContext.post(
      endSessionUrl
    );
    if (endStudioSessionResp.status() == 500) {
      console.log(
        `API to end studio session gave 500, so it might already be ended`
      );
    }
  }

  async selectSessionForStage(sessionId: string) {
    const studioSettingUri = `${
      DataUtl.getApplicationTestDataObj()["studioBaseUrl"]
    }/api/studio/${this.studioId}/settings/`;

    const response = await this.orgApiRequestContext.patch(studioSettingUri, {
      data: { selectedSegmentId: sessionId },
    });

    if (!response.ok()) {
      throw new Error(
        `API call to change session on Stage failed with ${response.status()}`
      );
    }
  }

  async startSessionOnStudio(sessionId = "", dryRun = false) {
    //Select Scheduled session to run on Stage || "" - means No-session
    await this.selectSessionForStage(sessionId);

    if (dryRun) await this.enableDryRun();
    else await this.disableDryRun();

    const startSessionUrl = `${
      DataUtl.getApplicationTestDataObj()["studioBaseUrl"]
    }/api/studio/${this.studioId}/live/`;

    const startStudioSessionResp = await this.orgApiRequestContext.post(
      startSessionUrl
    );
    if (startStudioSessionResp.status() != 200) {
      throw new Error(
        `API call to start studio session failed with ${startStudioSessionResp.status()}`
      );
    }

    return startStudioSessionResp;
  }

  async stopVideoPlayBackOnStudio() {
    const endVideoPlayBackUrl = `${
      DataUtl.getApplicationTestDataObj()["studioBaseUrl"]
    }/api/studio/${this.studioId}/snapshot`;
    const endVideoPlayBackResp = await this.orgApiRequestContext.patch(
      endVideoPlayBackUrl,
      {
        data: {
          studioId: this.studioId,
          videoContent: { mediaId: "", currentPlayingSeconds: 0 },
        },
      }
    );
    if (endVideoPlayBackResp.status() != 200) {
      throw new Error(
        `API call to end video playback on studio failed with ${endVideoPlayBackResp.status()}`
      );
    }
  }

  async stopFillerImageBroadcastOnStudio() {
    const endImageFillerBroadcastUrl = `${
      DataUtl.getApplicationTestDataObj()["studioBaseUrl"]
    }/api/studio/${this.studioId}/snapshot`;
    const endImageFillerBroadcastResp = await this.orgApiRequestContext.patch(
      endImageFillerBroadcastUrl,
      {
        data: {
          studioId: this.studioId,
          imageFiller: "",
        },
      }
    );
    if (endImageFillerBroadcastResp.status() != 200) {
      throw new Error(
        `API call to end filler  image broadcast on studio failed with ${endImageFillerBroadcastResp.status()}`
      );
    }
  }

  async stopFillerVideoBroadcastOnStudio() {
    const endVideoFillerBroadcastUrl = `${
      DataUtl.getApplicationTestDataObj()["studioBaseUrl"]
    }/api/studio/${this.studioId}/snapshot`;
    const endVideoFillerBroadcastResp = await this.orgApiRequestContext.patch(
      endVideoFillerBroadcastUrl,
      {
        data: {
          studioId: this.studioId,
          videoFiller: "",
        },
      }
    );
    if (endVideoFillerBroadcastResp.status() != 200) {
      throw new Error(
        `API call to end filler video playback on studio failed with ${endVideoFillerBroadcastResp.status()}`
      );
    }
  }

  async stopBrandingVideoClipBroadcast() {
    const endBrandingVideoClipUrl = `${
      DataUtl.getApplicationTestDataObj()["studioBaseUrl"]
    }/api/studio/${this.studioId}/snapshot`;
    const endBrandingVideoClipResp = await this.orgApiRequestContext.patch(
      endBrandingVideoClipUrl,
      {
        data: {
          studioId: this.studioId,
          videoClip: "",
        },
      }
    );
    if (endBrandingVideoClipResp.status() != 200) {
      throw new Error(
        `API call to end branding video playback on studio failed with ${endBrandingVideoClipResp.status()}`
      );
    }
  }

  async updateRecordingSettingForStudio(enableRecording = false) {
    const settingsUrlEndpoint = `${
      DataUtl.getApplicationTestDataObj()["studioBaseUrl"]
    }/api/studio/${this.studioId}/settings`;
    const apiResponse = await this.orgApiRequestContext.patch(
      settingsUrlEndpoint,
      {
        data: { isRecordingEnabled: enableRecording },
      }
    );
    if (apiResponse.status() != 200) {
      throw new Error(
        `API call to update recording settings on studio failed with ${apiResponse.status()}`
      );
    }
  }

  async updateBrandColor(colorCode: string) {
    const updateBrandColorEndpoint = `${
      DataUtl.getApplicationTestDataObj()["studioBaseUrl"]
    }/api/studio/${this.studioId}/snapshot`;
    const apiResponse = await this.orgApiRequestContext.patch(
      updateBrandColorEndpoint,
      {
        data: {
          studioId: this.studioId,
          hexColor: colorCode,
        },
      }
    );
    if (apiResponse.status() != 200) {
      throw new Error(
        `API call to update color on studio failed with ${apiResponse.status()}`
      );
    }
  }

  async enableDryRun() {
    const apiResponse = await this.orgApiRequestContext.post(
      `${DataUtl.getApplicationTestDataObj()["studioBaseUrl"]}/api/stage/${
        this.stageId
      }/dry_run/update/true`
    );

    if (apiResponse.status() != 200) {
      throw new Error(
        `API call to enable dry-run failed with ${apiResponse.status()}`
      );
    }
  }

  async disableDryRun() {
    const apiResponse = await this.orgApiRequestContext.post(
      `${DataUtl.getApplicationTestDataObj()["studioBaseUrl"]}/api/stage/${
        this.stageId
      }/dry_run/update/false`
    );

    if (apiResponse.status() != 200) {
      throw new Error(
        `API call to disable dry-run failed with ${apiResponse.status()}`
      );
    }
  }

  async enableDryRunForWebinar() {
    const apiResponse = await this.orgApiRequestContext.patch(
      `${DataUtl.getApplicationTestDataObj()["studioBaseUrl"]}/api/studio/${
        this.studioId
      }/settings`,
      {
        data: { isRecordingEnabled: true },
      }
    );

    if (apiResponse.status() != 200) {
      throw new Error(
        `API call to enable dry-run in Webinar failed with ${apiResponse.status()}`
      );
    }
  }

  async disableDryRunForWebinar() {
    const apiResponse = await this.orgApiRequestContext.patch(
      `${DataUtl.getApplicationTestDataObj()["studioBaseUrl"]}/api/studio/${
        this.studioId
      }/settings`,
      {
        data: { isRecordingEnabled: false },
      }
    );

    if (apiResponse.status() != 200) {
      throw new Error(
        `API call to enable dry-run in Webinar failed with ${apiResponse.status()}`
      );
    }
  }
}
