import { APIRequestContext, expect } from "@playwright/test";
import { DataUtl } from "../util/dataUtil";

export class BrandingController {
  protected orgApiRequestContext: APIRequestContext;
  protected eventId: string;
  protected eventInfoEndpoint: string;
  protected eventDataInstance;

  constructor(orgApiRequestContext: APIRequestContext, eventId) {
    this.orgApiRequestContext = orgApiRequestContext;
    this.eventId = eventId;
    this.eventInfoEndpoint = `/api/event/${this.eventId}`;
  }

  async getCurrentBrandingConfig() {
    return (await this.fetchAllEventInfo()).json();
  }

  async fetchAllEventInfo() {
    const eventInfoRespApi = await this.orgApiRequestContext.get(
      this.eventInfoEndpoint
    );
    if (eventInfoRespApi.status() != 200) {
      throw new Error(
        `API call to get event info failed with ${eventInfoRespApi.status()}`
      );
    }
    return eventInfoRespApi;
  }

  async updateColorTheme(
    colorTheme: string,
    primaryColor: string,
    updatedDataInstance = null
  ) {
    if (updatedDataInstance == null) {
      updatedDataInstance = await this.getCurrentBrandingConfig();
    }
    updatedDataInstance.colorTheme = colorTheme;
    updatedDataInstance.primaryColor = primaryColor;
    console.log(updatedDataInstance);

    const updateColorThemeRespApi = await this.orgApiRequestContext.post(
      this.eventInfoEndpoint,
      {
        data: updatedDataInstance,
      }
    );

    console.log(updateColorThemeRespApi.status());
  }

  async updateNavBarTheme(
    navbarColor: string,
    navbarPrimaryColor: string,
    updatedDataInstance = null
  ) {
    if (updatedDataInstance == null) {
      updatedDataInstance = await this.getCurrentBrandingConfig();
    }
    updatedDataInstance.navbarColor = navbarColor;
    updatedDataInstance.navbarPrimaryColor = navbarPrimaryColor;
    console.log(updatedDataInstance);

    const updateNavBarResp = await this.orgApiRequestContext.post(
      this.eventInfoEndpoint,
      {
        data: updatedDataInstance,
      }
    );

    console.log(updateNavBarResp.status());
  }

  async updateBackgroundImageUrl(
    backgroundImageIndex: number,
    updatedDataInstance = null
  ) {
    if (updatedDataInstance == null) {
      updatedDataInstance = await this.getCurrentBrandingConfig();
    }

    console.log(backgroundImageIndex);

    const imageUrl = `https://phoenixlive.imgix.net/website/bg-common-${backgroundImageIndex}.png`;

    updatedDataInstance.backgroundImageUrl = imageUrl;
    console.log(updatedDataInstance);

    const updateBackgroundImageResp = await this.orgApiRequestContext.post(
      this.eventInfoEndpoint,
      {
        data: updatedDataInstance,
      }
    );

    console.log(updateBackgroundImageResp.status());
    const updateLobbyBackgroundImageRespApi =
      await this.orgApiRequestContext.post(
        `${this.eventInfoEndpoint}/lobby/layout`,
        {
          data: {
            bgUrl: `website/bg-common-${backgroundImageIndex}.png`,
          },
        }
      );

    console.log(updateLobbyBackgroundImageRespApi.status());
    console.log(updateBackgroundImageResp.status());
  }

  async updateLogo(imageIndex: number, updatedDataInstance = null) {
    if (updatedDataInstance == null) {
      updatedDataInstance = await this.getCurrentBrandingConfig();
    }
    updatedDataInstance.logoUrl = `https://phoenixlive.imgix.net/website/bg-common-${imageIndex}.png`;
    console.log(updatedDataInstance);

    const updateColorThemeRespApi = await this.orgApiRequestContext.post(
      this.eventInfoEndpoint,
      {
        data: updatedDataInstance,
      }
    );
  }

  async randomiseBranding() {
    let updatedDataInstance = await this.getCurrentBrandingConfig();

    updatedDataInstance.colorTheme = Math.random() < 0.5 ? "CUSTOM" : "DARK";
    updatedDataInstance.primaryColor = DataUtl.getRandomColorCode(true);
    updatedDataInstance.secondaryColor = DataUtl.getRandomColorCode(true);
    updatedDataInstance.navbarColor = DataUtl.getRandomColorCode(true);
    updatedDataInstance.navbarPrimaryColor = DataUtl.getRandomColorCode(true);
    updatedDataInstance.backgroundImageUrl = DataUtl.getRandomColorCode(true);
    updatedDataInstance.logoImageUrl = DataUtl.getRandomColorCode(true);
    // split at .net/
    let lobbyLayoutBgUrl =
      updatedDataInstance.backgroundImageUrl.split(".net/")[1];

    const updateBrandingCompleteApi = await this.orgApiRequestContext.post(
      this.eventInfoEndpoint,
      {
        data: updatedDataInstance,
      }
    );

    const updateLobbyBackgroundImageRespApi =
      await this.orgApiRequestContext.post(
        `${this.eventInfoEndpoint}/lobby/layout`,
        {
          data: {
            bgUrl: lobbyLayoutBgUrl,
          },
        }
      );

    console.log(updateBrandingCompleteApi.status());
    console.log(updateLobbyBackgroundImageRespApi.status());
  }
}
