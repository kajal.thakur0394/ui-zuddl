import { APIRequestContext, APIResponse, expect, test } from "@playwright/test";
import {
  boothWithinZonePayloadGenerator,
  boothWithoutZonePayloadGenerator,
  zonePayloadGenerator,
  CreateBoothWithinZonePayload,
  CreateBoothWithoutZonePayload,
  CreateWidgetPayload,
  widgetPayloadGenerator,
} from "../util/jsonPayloadGenerator";
import { AccessGroupController } from "./AccessGroupController";
import { RegistrationFormController } from "./RegistrationFormController";
import { RoomsController } from "./RoomsController";
import { ModeratedStageSettingPaylod } from "./StageController";
import { DataUtl } from "../util/dataUtil";

export class CustomFieldObject {
  registrationCustomFieldId;
  fieldName: string;
  fieldType;
  localFieldType;
  dataType;
  labelName;
  dropdownValues;
  isRequired;
  eventId: string;
  choiceTypeJson;
  isPredefinedField: boolean;
  uniqueId: string;
  customFieldConditions: any;
  orderIdx: number;
  eventLandingPageId: string;
}

export class ExpoController {
  protected orgApiRequest: APIRequestContext;
  protected registrationFormController: RegistrationFormController;
  protected lobbyController;
  protected accessGroupController: AccessGroupController;
  protected eventId;
  protected boothId;
  protected roomsController: RoomsController;

  constructor(orgApiRequestContext: APIRequestContext, eventId) {
    this.orgApiRequest = orgApiRequestContext;
    this.eventId = eventId;

    this.registrationFormController = new RegistrationFormController(
      orgApiRequestContext,
      eventId
    );
    this.accessGroupController = new AccessGroupController(
      orgApiRequestContext,
      eventId
    );
    this.roomsController = new RoomsController(orgApiRequestContext, eventId);
  }

  async getEventInfo() {
    const eventInfoResp = await this.orgApiRequest.get(
      `/api/event/${this.eventId}`
    );
    if (eventInfoResp.status() != 200) {
      throw new Error(
        `API call to get event info failed with ${eventInfoResp.status()}`
      );
    }
    return eventInfoResp;
  }

  async createZone() {
    let createZoneResponse: APIResponse;

    await test.step("Creating a Zone in Expo.", async () => {
      createZoneResponse = await this.orgApiRequest.post(
        `api/event/${this.eventId}/booth_zone`,
        {
          data: zonePayloadGenerator(),
        }
      );

      if (createZoneResponse.status() != 200) {
        throw new Error(
          `API call to create new Zone failed with response ${createZoneResponse.status()}`
        );
      }
    });

    return createZoneResponse!.json();
  }

  async createBoothWithinZone(jsonPayload: CreateBoothWithinZonePayload) {
    let createBoothResponse: APIResponse;

    await test.step("Creating a Booth in Expo.", async () => {
      createBoothResponse = await this.orgApiRequest.post(
        `/api/event/${this.eventId}/booth`,
        {
          data: jsonPayload,
        }
      );

      if (createBoothResponse.status() != 200) {
        throw new Error(
          `API call to add new booth failed with response ${createBoothResponse.status()}`
        );
      }
    });

    return await createBoothResponse!.json();
  }

  async createSingleBoothWithinZone(zoneId: string, boothName?: string) {
    let response = new Map();
    boothName =
      boothName != null ? boothName : DataUtl.generateRandomBoothName();
    let jsonResponse: JSON;

    await test.step("Creating a booth.", async () => {
      let jsonPayload: CreateBoothWithinZonePayload =
        boothWithinZonePayloadGenerator(this.eventId, zoneId, boothName);

      jsonResponse = await this.createBoothWithinZone(jsonPayload);

      console.log(jsonResponse);

      const boothId = jsonResponse["boothId"];
      boothName = jsonResponse["name"];

      response.set(boothName, boothId);
    });

    return response;
  }

  async getLayoutId(boothId: string) {
    let getLayoutIdResponse: APIResponse;

    await test.step("Creating a Booth in Expo.", async () => {
      getLayoutIdResponse = await this.orgApiRequest.get(
        `/api/booth/${boothId}/layout`
      );

      if (getLayoutIdResponse.status() != 200) {
        throw new Error(
          `API call to add new booth failed with response ${getLayoutIdResponse.status()}`
        );
      }
    });

    return await getLayoutIdResponse!.json();
  }

  async addTextWidget(layoutId: string) {
    let addWidgetResponse: APIResponse;
    let jsonPayload: CreateWidgetPayload = widgetPayloadGenerator(layoutId);

    await test.step("Adding widget to booth.", async () => {
      addWidgetResponse = await this.orgApiRequest.post(
        `/api/event/${this.eventId}/layout/${layoutId}/widget`,
        {
          data: jsonPayload,
        }
      );

      if (addWidgetResponse.status() != 200) {
        throw new Error(
          `API call to add new widget failed with response ${addWidgetResponse.status()}`
        );
      }
    });
  }

  async createBoothWithoutZone(jsonPayload: CreateBoothWithoutZonePayload) {
    let createBoothResponse: APIResponse;

    await test.step("Creating a Booth in Expo.", async () => {
      createBoothResponse = await this.orgApiRequest.post(
        `/api/event/${this.eventId}/booth`,
        {
          data: jsonPayload,
        }
      );

      if (createBoothResponse.status() != 200) {
        throw new Error(
          `API call to add new booth failed with response ${createBoothResponse.status()}`
        );
      }
    });

    return await createBoothResponse!.json();
  }

  async createSingleBoothWithoutZone() {
    let response = new Map();
    let boothName: string;
    let jsonResponse: JSON;

    await test.step("Creating a booth.", async () => {
      let jsonPayload: CreateBoothWithoutZonePayload =
        boothWithoutZonePayloadGenerator(this.eventId, boothName);

      jsonResponse = await this.createBoothWithoutZone(jsonPayload);

      console.log(jsonResponse);
      this.boothId = jsonResponse["boothId"];
      boothName = jsonResponse["name"];
      response.set(this.boothId, boothName);
    });
    return response;
  }

  async createMultipleBoothsWithoutZone(numberOfBooths: number) {
    let response = new Map();
    let iterator: number = 1;

    await test.step("Creating multiple booths one by one.", async () => {
      while (iterator <= numberOfBooths) {
        let boothCreationResponse = await this.createSingleBoothWithoutZone();

        for (let [key, value] of boothCreationResponse) {
          console.log("Single booth response -> " + boothCreationResponse);
          console.log(key + " -> " + value);

          response.set(key, value);
        }

        iterator++;
      }
    });

    return response;
  }

  async deleteBooth(boothId: string) {
    let deleteBoothResponse = await this.orgApiRequest.delete(
      `/api/booth/${boothId}`
    );

    if (deleteBoothResponse.status() != 200) {
      throw new Error(
        `API call to delete booth failed with response ${deleteBoothResponse.status()}`
      );
    }
  }

  async getChannelResponse(): Promise<APIResponse> {
    let getChannelIdApiResp: APIResponse;
    await test.step(`Fetching the channel response for ${this.boothId}`, async () => {
      getChannelIdApiResp = await this.orgApiRequest.get(
        `/api/channel?refId=${this.boothId}`
      );
      if (getChannelIdApiResp.status() != 200) {
        throw new Error(
          `API to fetch channelId for room failed with ${getChannelIdApiResp.status()}`
        );
      }
    });
    return getChannelIdApiResp;
  }

  async updateBoothEngagementSettingsForThisBooth(
    moderatedStageSettingPaylod: ModeratedStageSettingPaylod
  ) {
    const getChannelApiResp = await this.getChannelResponse();
    const getChannelApiRespJson = await getChannelApiResp.json();
    const channelId = getChannelApiRespJson["channelId"];
    moderatedStageSettingPaylod.channelId = channelId;
    console.log(`channel id  fetched is ${channelId}`);
    await test.step(`Hitting settings api for this channel to enable chat: ${moderatedStageSettingPaylod.hasChat} and enable moderated chat ${moderatedStageSettingPaylod.hasChatModerate}`, async () => {
      const updateStageEngagementSettingsApiResp =
        await this.orgApiRequest.post(`/api/channel/${channelId}/settings`, {
          data: moderatedStageSettingPaylod,
        });

      const updateApiStatus = updateStageEngagementSettingsApiResp.status();
      if (updateApiStatus != 200) {
        throw new Error(
          `API call to udpate stage engagement settings failed with ${updateApiStatus}`
        );
      }
    });
  }

  async getAllBooths() {
    const getAllBoothsApiResp = await this.orgApiRequest.get(
      `/api/booth/${this.eventId}/o/booth/list?showHidden=true`
    );

    const getAllBoothsApiStatus = getAllBoothsApiResp.status();
    if (getAllBoothsApiStatus != 200) {
      throw new Error(
        `API call to get all booths failed with ${getAllBoothsApiStatus}`
      );
    }

    const getAllBoothsApiRespJson = await getAllBoothsApiResp.json();
    // console.log(
    //   `All booths response is ${JSON.stringify(getAllBoothsApiRespJson)}`
    // );
    return getAllBoothsApiRespJson;
  }

  async getAllZones() {
    const getAllZonesApiResp = await this.orgApiRequest.get(
      `/api/booth/${this.eventId}/o/booth_zone/list`
    );

    const getAllZonesApiStatus = getAllZonesApiResp.status();
    if (getAllZonesApiStatus != 200) {
      throw new Error(
        `API call to get all zones failed with ${getAllZonesApiStatus}`
      );
    }

    const getAllZonesApiRespJson = await getAllZonesApiResp.json();
    // console.log(
    //   `All zones response is ${JSON.stringify(getAllZonesApiRespJson)}`
    // );
    return getAllZonesApiRespJson;
  }

  async getAllWidgets(boothId: string) {
    const layoutIdResp = await this.getLayoutId(boothId);
    const layoutId = layoutIdResp["layoutId"];
    const getAllWidgetsApiResp = await this.orgApiRequest.get(
      `/api/event/${this.eventId}/widget/${layoutId}`
    );

    const getAllWidgetsApiStatus = getAllWidgetsApiResp.status();
    if (getAllWidgetsApiStatus != 200) {
      throw new Error(
        `API call to get all widgets failed with ${getAllWidgetsApiStatus}`
      );
    }

    const getAllWidgetsApiRespJson = await getAllWidgetsApiResp.json();

    return getAllWidgetsApiRespJson;
  }

  async addBoothOwnerInsideBooth(boothId: string, accountId: string) {
    const addboothOwner = await this.orgApiRequest.post(
      `/api/booth/${boothId}/${this.eventId}/owner`,
      {
        data: {
          accountId: accountId,
        },
      }
    );

    if (addboothOwner.status() != 200) {
      throw new Error(
        `API call to add new booth owner failed with response ${addboothOwner.status()}`
      );
    }
  }
}
