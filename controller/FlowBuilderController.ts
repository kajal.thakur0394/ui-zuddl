import { APIRequestContext, APIResponse } from "@playwright/test";
import { FlowStatus, FlowType } from "../enums/FlowTypeEnum";
import {
  FlowBuilderControllerInterface,
  publishDataPayload,
} from "../interfaces/FlowBuilerControllerInterface";
import * as fs from "fs";
import * as uuid from "uuid";
import * as path from "path";
import { TicketingController } from "./TicketingController";
import { DataUtl } from "../util/dataUtil";

export class FlowBuilderController implements FlowBuilderControllerInterface {
  protected orgApiRequest: APIRequestContext;
  protected eventId: any;
  protected initialFlowSteps: any;
  protected updatedFlowSteps: any;
  protected flowBuilderThankYouPageConfig: any;
  constructor(orgApiRequestContext: APIRequestContext, eventId: any) {
    this.orgApiRequest = orgApiRequestContext;
    this.eventId = eventId;
    this.flowBuilderThankYouPageConfig =
      DataUtl.getApplicationTestDataObj()["flowBuilderThankYouPageConfig"];
  }

  async getFlowDraftList() {
    const flowDraftListResponse = await this.orgApiRequest.get(
      `api/event/${this.eventId}/flow/draft/list`
    );
    if (flowDraftListResponse.status() != 200) {
      throw new Error(
        `API to fetch flow draft list failed with ${flowDraftListResponse.status()}`
      );
    }
    let flowDraftList = await flowDraftListResponse.json();
    console.log(flowDraftList);
    return flowDraftList;
  }

  async getFlowData(
    flowType: FlowType,
    flowStatus: FlowStatus,
    defaultFlow: boolean = true
  ): Promise<APIResponse> {
    let flowId = await this.getFlowId(flowType, flowStatus);
    if (defaultFlow) {
      let flowList = await this.getFlowDraftList();
      if (flowList.length > 0) {
        flowId = flowList[0].flowId;
      }
    }
    let flowDataEndPoint = `api/event/${this.eventId}/flow/draft/list`;
    let flowResponse = await this.orgApiRequest.get(flowDataEndPoint);

    if (flowResponse.status() != 200) {
      throw new Error(
        `API to fetch flow data response failed with ${flowResponse.status()}`
      );
    }
    let flowResponseJson = await flowResponse.json();
    console.log(`Response for ${flowType} ${flowStatus}:`);
    console.log(flowResponseJson);
    return flowResponseJson;
  }

  async updateFlowData(
    flowType: FlowType,
    flowStatus: FlowStatus
  ): Promise<APIResponse> {
    let flowId = await this.getFlowId(flowType, flowStatus);
    let dataPayLoad = await this.getUpdateFlowBodyJSON(flowType, flowStatus);
    // set custom templates
    const customTemplates = await this.getCustomTemplates(flowId);
    dataPayLoad["flow"]["customTemplates"] = customTemplates;
    const updateFlowDataResponse = await this.orgApiRequest.post(
      `api/event/${this.eventId}/flow/${flowId}/${flowStatus}`,
      {
        data: dataPayLoad,
      }
    );

    if (updateFlowDataResponse.status() != 200) {
      throw new Error(
        `API to add update flow json data failed with ${updateFlowDataResponse.status()}`
      );
    }
    return await updateFlowDataResponse.json();
  }

  async publishFlowData(dataPayLoad: publishDataPayload): Promise<APIResponse> {
    dataPayLoad["flowId"] = await this.getPublishedFlowId(
      dataPayLoad.flowDraftId
    );
    const publishFlowResponse = await this.orgApiRequest.post(
      `api/event/${this.eventId}/flow/draft/publish`,
      {
        data: dataPayLoad,
      }
    );

    if (publishFlowResponse.status() != 200) {
      throw new Error(
        `API to publish the flow failed with ${publishFlowResponse.status()}`
      );
    }
    return await publishFlowResponse.json();
  }

  async getFlowId(flowType: FlowType, flowStatus: FlowStatus) {
    let flowResponseJson = (await await this.getFlowDraftList())[0];
    console.log(flowResponseJson);
    return flowResponseJson["flowId"];
  }

  async getPublishedFlowId(flowId: string) {
    const flowDraftList = await this.getFlowDraftList();
    let flowData = flowDraftList.find((flow: any) => flow.flowId === flowId);
    return flowData["publishedFlowId"];
  }

  async getCustomTemplates(flowId: string) {
    const flowDraftList = await this.getFlowDraftList();
    let flowData = flowDraftList.find((flow: any) => flow.flowId === flowId);
    console.log(flowData);
    const customTemplates = flowData["flowJson"]["customTemplates"];
    console.log(customTemplates);
    return customTemplates;
  }

  async setCustomTemplates(flowId: string) {
    const customTemplates = await this.getCustomTemplates(flowId);
    let flowData = await this.getFlowData(
      FlowType.REGISTRATION,
      FlowStatus.DRAFT
    );
    console.log(JSON.stringify(flowData, null, 2));
    let dataPayLoad = flowData[0]["flowJson"];
    console.log(dataPayLoad);
    dataPayLoad["customTemplates"] = customTemplates;
    const updateFlowDataResponse = await this.orgApiRequest.post(
      `api/event/${this.eventId}/flow/${flowData[0]["flowId"]}/draft`,
      {
        data: { flow: dataPayLoad },
      }
    );
    if (updateFlowDataResponse.status() != 200) {
      throw new Error(
        `API to add update flow json data failed with ${updateFlowDataResponse.status()}`
      );
    }
  }

  async getVersionId(flowType: FlowType, flowStatus: FlowStatus) {
    let flowResponseJson = await this.getFlowData(flowType, flowStatus);
    return flowResponseJson[0]["flowJson"]["version"];
  }

  async getScehmaVersionId(flowType: FlowType, flowStatus: FlowStatus) {
    let flowResponseJson = await this.getFlowData(flowType, flowStatus);
    return flowResponseJson[0]["flowJson"].schemaVersion;
  }

  async getFlowBasicDetails(flowType: FlowType, flowStatus: FlowStatus) {
    const resultMap = new Map();
    const flowId = await this.getFlowId(flowType, flowStatus);
    const version = await this.getVersionId(flowType, flowStatus);
    const schemaVersion = await this.getScehmaVersionId(flowType, flowStatus);
    resultMap.set("version", version);
    resultMap.set("id", flowId);
    resultMap.set("schemaVersion", schemaVersion);
    resultMap.set("eventId", this.eventId);
    resultMap.set("type", flowType);
    return resultMap;
  }

  async getFlowStepsJSON() {
    const placeholderMappingFile = path.resolve(
      "test-data/",
      "placeholder_mapping.json"
    );
    const placeholderStepsFile = path.resolve(
      "test-data/",
      "steps_with_placeholder.json"
    );

    const updatedStepsJSON: any = JSON.parse(
      fs.readFileSync(placeholderStepsFile, "utf-8")
    );
    const placeholderMapping: Record<string, string> = JSON.parse(
      fs.readFileSync(placeholderMappingFile, "utf-8")
    );

    let updatedJSONData = this.replaceUUIDs(
      updatedStepsJSON,
      placeholderMapping
    );

    updatedJSONData["steps"] = updatedJSONData["steps"].map((step: any) => {
      if (step["type"] === "THANK_YOU") {
        step["config"]["defaultConfig"] = this.flowBuilderThankYouPageConfig;
      }
      return step;
    });
    console.log("Final JSON:");
    console.log(JSON.stringify(updatedJSONData, null, 2));
    this.initialFlowSteps = updatedJSONData;
    return updatedJSONData;
  }

  async getUpdateFlowBodyJSON(flowType: FlowType, flowStatus: FlowStatus) {
    const placeholderMappingFile = path.resolve(
      "test-data/",
      "flowdataupdate.json"
    );
    const updatedStepsJSON: any = JSON.parse(
      fs.readFileSync(placeholderMappingFile, "utf-8")
    );
    let resultMap = await this.getFlowBasicDetails(flowType, flowStatus);
    console.log(await await this.getFlowStepsJSON()["steps"]);
    let x = await this.getFlowStepsJSON();
    resultMap.set("steps", x["steps"]);
    let updatedJSONData = await this.replacePlaceholders(
      updatedStepsJSON,
      resultMap
    );
    console.log(JSON.stringify(updatedJSONData, null, 2));
    return updatedJSONData;
  }

  async replacePlaceholders(
    jsonData: any,
    placeholderMap: Map<string, any>
  ): Promise<any> {
    if (typeof jsonData === "object") {
      if (Array.isArray(jsonData)) {
        return Promise.all(
          jsonData.map(
            async (item) => await this.replacePlaceholders(item, placeholderMap)
          )
        );
      } else {
        const updatedObject: any = {};
        for (const key in jsonData) {
          // eslint-disable-next-line no-prototype-builtins
          if (jsonData.hasOwnProperty(key)) {
            updatedObject[key] = await this.replacePlaceholders(
              jsonData[key],
              placeholderMap
            );
          }
        }
        return updatedObject;
      }
    } else if (typeof jsonData === "string") {
      if (jsonData.startsWith("{{") && jsonData.endsWith("}}")) {
        const placeholderKey = jsonData.slice(2, -2);
        if (placeholderMap.has(placeholderKey)) {
          return placeholderMap.get(placeholderKey);
        } else {
          console.warn(`Placeholder key not found: ${placeholderKey}`);
          return jsonData;
        }
      } else {
        return jsonData;
      }
    } else {
      return jsonData;
    }
  }

  async insertFormToExistingFlow(
    flowType: FlowType,
    flowStatus: FlowStatus,
    insertAfter: string
  ) {
    const placeholderStepsFile = path.resolve(
      "test-data/",
      "additionalFlowStep.json"
    );

    let newStepsText = fs.readFileSync(placeholderStepsFile, "utf-8");
    // get the existing flow data

    let flowData = await this.getFlowData(flowType, flowStatus);
    // get THANK_YOU step id
    let thankYouStepId = flowData[0]["flowJson"]["steps"].find(
      (step: any) => step["type"] === "THANK_YOU"
    )["id"];
    newStepsText = newStepsText.replace("{{thankYouID}}", thankYouStepId);
    const newStepId = this.generateNewUUID();
    newStepsText = newStepsText.replace("{{newId}}", newStepId);
    newStepsText = newStepsText.replace(
      "{{newFieldId}}",
      this.generateNewUUID()
    );
    newStepsText = newStepsText.replace(
      "{{newFormId}}",
      this.generateNewUUID()
    );
    newStepsText = newStepsText.replace(
      "{{newBranchId}}",
      this.generateNewUUID()
    );
    // append the new step to the flow
    let newSteps = JSON.parse(newStepsText);
    this.updatedFlowSteps = newSteps;
    flowData[0]["flowJson"]["steps"].push(newSteps);
    // find the step with name insertAfter and replace step["branches"][0]["destinationId"] with newStepId
    let insertAfterStep = flowData[0]["flowJson"]["steps"].find(
      (step: any) => step["name"] === insertAfter
    );
    insertAfterStep["branches"][0]["destinationId"] = newStepId;
    // update the flow data
    let dataPayLoad = flowData[0]["flowJson"];

    dataPayLoad["steps"] = dataPayLoad["steps"].map((step: any) => {
      if (step["type"] === "THANK_YOU") {
        step["config"]["defaultConfig"] = this.flowBuilderThankYouPageConfig;
      }
      return step;
    });

    console.log(JSON.stringify(dataPayLoad, null, 2));

    const customTemplates = await this.getCustomTemplates(
      flowData[0]["flowId"]
    );

    dataPayLoad["customTemplates"] = customTemplates;
    const updateFlowDataResponse = await this.orgApiRequest.post(
      `api/event/${this.eventId}/flow/${flowData[0]["flowId"]}/draft`,
      {
        data: { flow: dataPayLoad },
      }
    );
    if (updateFlowDataResponse.status() != 200) {
      throw new Error(
        `API to add update flow json data failed with ${updateFlowDataResponse.status()}`
      );
    }

    const flowDraftData = (
      await this.getFlowData(flowType, FlowStatus.DRAFT)
    )[0];
    console.log(flowDraftData);
    const flowPublishedData = (
      await this.getFlowData(flowType, FlowStatus.FLOW)
    )[0];

    console.log(flowData);

    const publishDataPayload: publishDataPayload = {
      flowId: flowPublishedData["publishedFlowId"],
      flowDraftId: flowDraftData["flowId"],
      clientFlowDraftVersion: flowDraftData["version"],
    };
    console.log(publishDataPayload);
    // publish the flow
    const publishFlowResponse = await this.orgApiRequest.post(
      `api/event/${this.eventId}/flow/draft/publish`,
      {
        data: publishDataPayload,
      }
    );
    if (publishFlowResponse.status() != 200) {
      throw new Error(
        `API to publish the flow failed with ${publishFlowResponse.status()}`
      );
    }
    return await publishFlowResponse.json();
  }

  async deleteNewStepFromFlow(flowType: FlowType, flowStatus: FlowStatus) {
    let flowData = await this.getFlowData(flowType, flowStatus);
    console.log(JSON.stringify(flowData, null, 2));
    flowData[0]["flowJson"]["steps"] = this.initialFlowSteps["steps"];
    let dataPayLoad = flowData[0]["flowJson"];
    console.log(dataPayLoad);
    const customTemplates = await this.getCustomTemplates(
      flowData[0]["flowId"]
    );

    dataPayLoad["customTemplates"] = customTemplates;

    const updateFlowDataResponse = await this.orgApiRequest.post(
      `api/event/${this.eventId}/flow/${flowData[0]["flowId"]}/draft`,
      {
        data: { flow: dataPayLoad },
      }
    );
    if (updateFlowDataResponse.status() != 200) {
      throw new Error(
        `API to add update flow json data failed with ${updateFlowDataResponse.status()}`
      );
    }

    const flowDraftData = (
      await this.getFlowData(flowType, FlowStatus.DRAFT)
    )[0];
    console.log(flowDraftData);
    const flowPublishedData = (
      await this.getFlowData(flowType, FlowStatus.FLOW)
    )[0];

    console.log(flowData);

    const publishDataPayload: publishDataPayload = {
      flowId: flowPublishedData["publishedFlowId"],
      flowDraftId: flowDraftData["flowId"],
      clientFlowDraftVersion: flowDraftData["version"],
    };
    console.log(publishDataPayload);
    // publish the flow
    const publishFlowResponse = await this.orgApiRequest.post(
      `api/event/${this.eventId}/flow/draft/publish`,
      {
        data: publishDataPayload,
      }
    );
    if (publishFlowResponse.status() != 200) {
      throw new Error(
        `API to publish the flow failed with ${publishFlowResponse.status()}`
      );
    }
    return await publishFlowResponse.json();
  }

  generateNewUUID(): string {
    return uuid.v4();
  }

  replaceUUIDs(jsonData: any, uuidMapping: Record<string, string>): any {
    let jsonString: string = JSON.stringify(jsonData);

    for (const [placeholder, oldUUID] of Object.entries(uuidMapping)) {
      const newUUID = this.generateNewUUID();
      jsonString = jsonString.split(placeholder).join(newUUID);
    }

    return JSON.parse(jsonString);
  }

  async generateEmbedHtml(): Promise<string> {
    const placeholderEmbedFile = path.resolve("test-data/", "flow-embed.html");
    const embedHtml: string = fs.readFileSync(placeholderEmbedFile, "utf-8");
    const flowId = await this.getFlowId(FlowType.REGISTRATION, FlowStatus.FLOW);
    let finalHtmlEmbed = embedHtml.replace("{{eventId}}", this.eventId);
    finalHtmlEmbed = finalHtmlEmbed.replace("{{flowId}}", flowId);
    finalHtmlEmbed = finalHtmlEmbed.replace(/{{env}}/g, process.env.run_env);
    console.log(finalHtmlEmbed);
    return finalHtmlEmbed;
  }

  async generateTicketingFirstStep(flowType: FlowType, flowStatus: FlowStatus) {
    const placeholderMappingFile = path.resolve(
      "test-data/",
      "placeholder_mapping_taf.json"
    );
    const placeholderStepsFile = path.resolve(
      "test-data/",
      "ticketing_first_step.json"
    );

    const updatedStepsJSON: any = JSON.parse(
      fs.readFileSync(placeholderStepsFile, "utf-8")
    );
    const placeholderMapping: Record<string, string> = JSON.parse(
      fs.readFileSync(placeholderMappingFile, "utf-8")
    );

    let updatedJSONData = this.replaceUUIDs(
      updatedStepsJSON,
      placeholderMapping
    );
    updatedJSONData["steps"] = updatedJSONData["steps"].map((step: any) => {
      if (step["type"] === "THANK_YOU") {
        step["config"]["defaultConfig"] = this.flowBuilderThankYouPageConfig;
      }
      return step;
    });
    console.log("Final JSON:");
    console.log(JSON.stringify(updatedJSONData, null, 2));

    let resultMap = await this.getFlowBasicDetails(flowType, flowStatus);
    resultMap.set("steps", updatedJSONData["steps"]);
    console.log(JSON.stringify(updatedJSONData, null, 2));

    let flowId = await this.getFlowId(flowType, flowStatus);
    let dataPayLoad = Object.fromEntries(resultMap);
    const customTemplates = await this.getCustomTemplates(flowId);

    dataPayLoad["customTemplates"] = customTemplates;

    console.log(dataPayLoad);
    const updateFlowDataResponse = await this.orgApiRequest.post(
      `api/event/${this.eventId}/flow/${flowId}/draft`,
      {
        data: { flow: dataPayLoad },
      }
    );

    if (updateFlowDataResponse.status() != 200) {
      throw new Error(
        `API to add update flow json data failed with ${updateFlowDataResponse.status()}`
      );
    }
  }

  async generateFlowWithBranching(flowType: FlowType, flowStatus: FlowStatus) {
    const placeholderMappingFile = path.resolve(
      "test-data/",
      "placeholder_mapping_with_branch.json"
    );
    const placeholderStepsFile = path.resolve(
      "test-data/",
      "updated_steps_with_branch.json"
    );

    const updatedStepsJSON: any = JSON.parse(
      fs.readFileSync(placeholderStepsFile, "utf-8")
    );
    const placeholderMapping: Record<string, string> = JSON.parse(
      fs.readFileSync(placeholderMappingFile, "utf-8")
    );

    let updatedJSONData = this.replaceUUIDs(
      updatedStepsJSON,
      placeholderMapping
    );
    updatedJSONData["steps"] = updatedJSONData["steps"].map((step: any) => {
      if (step["type"] === "THANK_YOU") {
        step["config"]["defaultConfig"] = this.flowBuilderThankYouPageConfig;
      }
      return step;
    });
    console.log("Final JSON:");
    console.log(JSON.stringify(updatedJSONData, null, 2));

    let resultMap = await this.getFlowBasicDetails(flowType, flowStatus);
    resultMap.set("steps", updatedJSONData["steps"]);
    console.log(JSON.stringify(updatedJSONData, null, 2));

    let flowId = await this.getFlowId(flowType, flowStatus);
    let dataPayLoad = Object.fromEntries(resultMap);
    const customTemplates = await this.getCustomTemplates(flowId);

    dataPayLoad["customTemplates"] = customTemplates;

    console.log(dataPayLoad);
    const updateFlowDataResponse = await this.orgApiRequest.post(
      `api/event/${this.eventId}/flow/${flowId}/draft`,
      {
        data: { flow: dataPayLoad },
      }
    );

    if (updateFlowDataResponse.status() != 200) {
      throw new Error(
        `API to add update flow json data failed with ${updateFlowDataResponse.status()}`
      );
    }
  }

  async generateFlowWithIntegration(
    flowType: FlowType,
    flowStatus: FlowStatus
  ) {
    const placeholderMappingFile = path.resolve(
      "test-data/",
      "placeholder_mapping_integration.json"
    );
    const placeholderStepsFile = path.resolve(
      "test-data/",
      "updated_steps_integration.json"
    );

    const updatedStepsJSON: any = JSON.parse(
      fs.readFileSync(placeholderStepsFile, "utf-8")
    );
    const placeholderMapping: Record<string, string> = JSON.parse(
      fs.readFileSync(placeholderMappingFile, "utf-8")
    );

    let updatedJSONData = this.replaceUUIDs(
      updatedStepsJSON,
      placeholderMapping
    );
    updatedJSONData["steps"] = updatedJSONData["steps"].map((step: any) => {
      if (step["type"] === "THANK_YOU") {
        step["config"]["defaultConfig"] = this.flowBuilderThankYouPageConfig;
      }
      return step;
    });
    console.log("Final JSON:");
    console.log(JSON.stringify(updatedJSONData, null, 2));

    let resultMap = await this.getFlowBasicDetails(flowType, flowStatus);
    resultMap.set("steps", updatedJSONData["steps"]);
    console.log(JSON.stringify(updatedJSONData, null, 2));

    let flowId = await this.getFlowId(flowType, flowStatus);
    let dataPayLoad = Object.fromEntries(resultMap);
    const customTemplates = await this.getCustomTemplates(flowId);

    dataPayLoad["customTemplates"] = customTemplates;

    console.log(dataPayLoad);
    const updateFlowDataResponse = await this.orgApiRequest.post(
      `api/event/${this.eventId}/flow/${flowId}/draft`,
      {
        data: { flow: dataPayLoad },
      }
    );

    if (updateFlowDataResponse.status() != 200) {
      throw new Error(
        `API to add update flow json data failed with ${updateFlowDataResponse.status()}`
      );
    }
  }

  async generateSimpleTicketingFlow(
    flowType: FlowType,
    flowStatus: FlowStatus
  ) {
    const placeholderMappingFile = path.resolve(
      "test-data/",
      "placeholder_mapping_without_branch_with_ticket.json"
    );
    const placeholderStepsFile = path.resolve(
      "test-data/",
      "updated_steps_without_branch_with_ticket.json"
    );

    const updatedStepsJSON: any = JSON.parse(
      fs.readFileSync(placeholderStepsFile, "utf-8")
    );
    const placeholderMapping: Record<string, string> = JSON.parse(
      fs.readFileSync(placeholderMappingFile, "utf-8")
    );

    let updatedJSONData = this.replaceUUIDs(
      updatedStepsJSON,
      placeholderMapping
    );

    console.log("Final JSON:", JSON.stringify(updatedJSONData, null, 2));

    updatedJSONData["flowJson"]["steps"] = updatedJSONData["flowJson"][
      "steps"
    ].map((step: any) => {
      if (step["type"] === "THANK_YOU") {
        step["config"]["defaultConfig"] = this.flowBuilderThankYouPageConfig;
      }
      return step;
    });
    console.log("Final JSON:");
    console.log(JSON.stringify(updatedJSONData, null, 2));

    let resultMap = await this.getFlowBasicDetails(flowType, flowStatus);
    resultMap.set("steps", updatedJSONData["flowJson"]["steps"]);
    console.log(JSON.stringify(updatedJSONData, null, 2));

    let flowId = await this.getFlowId(flowType, flowStatus);
    let dataPayLoad = Object.fromEntries(resultMap);
    const customTemplates = await this.getCustomTemplates(flowId);

    dataPayLoad["customTemplates"] = customTemplates;

    console.log(dataPayLoad);
    const updateFlowDataResponse = await this.orgApiRequest.post(
      `api/event/${this.eventId}/flow/${flowId}/draft`,
      {
        data: { flow: dataPayLoad },
      }
    );

    if (updateFlowDataResponse.status() != 200) {
      throw new Error(
        `API to add update flow json data failed with ${updateFlowDataResponse.status()}`
      );
    }
  }

  async generateFlowWithTicketBranching(
    flowType: FlowType,
    flowStatus: FlowStatus,
    ticketId: string
  ) {
    // steps:
    // 1. create tickets
    // 2. open json with ticketing
    // 3. replace placeholders
    // 4. add ticketId to placeholder mapping

    const placeholderMappingFile = path.resolve(
      "test-data/",
      "placeholder_mapping_with_branch_with_ticket.json"
    );
    const placeholderStepsFile = path.resolve(
      "test-data/",
      "updated_steps_with_branch_with_ticket.json"
    );

    const updatedStepsJSON: any = JSON.parse(
      fs.readFileSync(placeholderStepsFile, "utf-8")
    );
    const placeholderMapping: Record<string, string> = JSON.parse(
      fs.readFileSync(placeholderMappingFile, "utf-8")
    );

    let updatedJSONData = this.replaceUUIDs(
      updatedStepsJSON,
      placeholderMapping
    );

    console.log("Final JSON:", JSON.stringify(updatedJSONData, null, 2));

    updatedJSONData["flowJson"]["steps"][1]["branches"][1]["conditions"][0][
      "compareValue"
    ] = [ticketId];
    updatedJSONData["flowJson"]["steps"] = updatedJSONData["flowJson"][
      "steps"
    ].map((step: any) => {
      if (step["type"] === "THANK_YOU") {
        step["config"]["defaultConfig"] = this.flowBuilderThankYouPageConfig;
      }
      return step;
    });
    console.log("Final JSON:");
    console.log(JSON.stringify(updatedJSONData, null, 2));

    let resultMap = await this.getFlowBasicDetails(flowType, flowStatus);
    resultMap.set("steps", updatedJSONData["flowJson"]["steps"]);
    console.log(JSON.stringify(updatedJSONData, null, 2));

    let flowId = await this.getFlowId(flowType, flowStatus);
    let dataPayLoad = Object.fromEntries(resultMap);
    const customTemplates = await this.getCustomTemplates(flowId);

    dataPayLoad["customTemplates"] = customTemplates;

    console.log(dataPayLoad);
    const updateFlowDataResponse = await this.orgApiRequest.post(
      `api/event/${this.eventId}/flow/${flowId}/draft`,
      {
        data: { flow: dataPayLoad },
      }
    );

    if (updateFlowDataResponse.status() != 200) {
      throw new Error(
        `API to add update flow json data failed with ${updateFlowDataResponse.status()}`
      );
    }
  }

  async generateFlowWithTAFBranching(
    flowType: FlowType,
    flowStatus: FlowStatus,
    inPersonTicketId: string,
    virtualTicketId: string
  ) {
    const placeholderMappingFile = path.resolve(
      "test-data/",
      "placeholder_mapping_with_branch_with_taf.json"
    );
    const placeholderStepsFile = path.resolve(
      "test-data/",
      "updated_steps_with_branch_with_taf.json"
    );

    const updatedStepsJSON: any = JSON.parse(
      fs.readFileSync(placeholderStepsFile, "utf-8")
    );
    const placeholderMapping: Record<string, string> = JSON.parse(
      fs.readFileSync(placeholderMappingFile, "utf-8")
    );

    let updatedJSONData = this.replaceUUIDs(
      updatedStepsJSON,
      placeholderMapping
    );
    updatedJSONData["flowJson"]["steps"] = updatedJSONData["flowJson"][
      "steps"
    ].map((step: any) => {
      if (step["type"] === "THANK_YOU") {
        step["config"]["defaultConfig"] = this.flowBuilderThankYouPageConfig;
      }
      return step;
    });

    console.log("Final JSON:", JSON.stringify(updatedJSONData, null, 2));

    updatedJSONData["flowJson"]["steps"][0]["branches"][1]["conditions"][0][
      "compareValue"
    ] = [virtualTicketId];

    updatedJSONData["flowJson"]["steps"][0]["branches"][2]["conditions"][0][
      "compareValue"
    ] = [virtualTicketId, inPersonTicketId];
    console.log("Final JSON:");
    console.log(JSON.stringify(updatedJSONData, null, 2));

    let resultMap = await this.getFlowBasicDetails(flowType, flowStatus);
    resultMap.set("steps", updatedJSONData["flowJson"]["steps"]);
    console.log(JSON.stringify(updatedJSONData, null, 2));

    let flowId = await this.getFlowId(flowType, flowStatus);
    let dataPayLoad = Object.fromEntries(resultMap);
    const customTemplates = await this.getCustomTemplates(flowId);

    dataPayLoad["customTemplates"] = customTemplates;

    console.log(dataPayLoad);
    const updateFlowDataResponse = await this.orgApiRequest.post(
      `api/event/${this.eventId}/flow/${flowId}/draft`,
      {
        data: { flow: dataPayLoad },
      }
    );

    if (updateFlowDataResponse.status() != 200) {
      throw new Error(
        `API to add update flow json data failed with ${updateFlowDataResponse.status()}`
      );
    }
  }

  async generateLinearTktConditoningFlow(
    flowType: FlowType,
    flowStatus: FlowStatus
  ) {
    const placeholderMappingFile = path.resolve(
      "test-data/",
      "placeholder_mapping_ticket_conditioning_flow_linear.json"
    );
    const placeholderStepsFile = path.resolve(
      "test-data/",
      "ticket_conditioning_flow_linear.json"
    );

    const updatedStepsJSON: any = JSON.parse(
      fs.readFileSync(placeholderStepsFile, "utf-8")
    );
    const placeholderMapping: Record<string, string> = JSON.parse(
      fs.readFileSync(placeholderMappingFile, "utf-8")
    );

    let updatedJSONData = this.replaceUUIDs(
      updatedStepsJSON,
      placeholderMapping
    );
    updatedJSONData["flowJson"]["steps"] = updatedJSONData["flowJson"][
      "steps"
    ].map((step: any) => {
      if (step["type"] === "THANK_YOU") {
        step["config"]["defaultConfig"] = this.flowBuilderThankYouPageConfig;
      }
      return step;
    });

    console.log("Final JSON:");
    console.log(JSON.stringify(updatedJSONData, null, 2));

    let resultMap = await this.getFlowBasicDetails(flowType, flowStatus);
    resultMap.set("steps", updatedJSONData["flowJson"]["steps"]);
    console.log(JSON.stringify(updatedJSONData, null, 2));

    let flowId = await this.getFlowId(flowType, flowStatus);
    let dataPayLoad = Object.fromEntries(resultMap);
    const customTemplates = await this.getCustomTemplates(flowId);

    dataPayLoad["customTemplates"] = customTemplates;

    console.log(dataPayLoad);
    const updateFlowDataResponse = await this.orgApiRequest.post(
      `api/event/${this.eventId}/flow/${flowId}/draft`,
      {
        data: { flow: dataPayLoad },
      }
    );

    if (updateFlowDataResponse.status() != 200) {
      throw new Error(
        `API to add update flow json data failed with ${updateFlowDataResponse.status()}`
      );
    }
  }

  async addNewFlow(flowType: FlowType, flowStatus: FlowStatus) {
    const createNewFlowResponse = await this.orgApiRequest.post(
      `api/event/${this.eventId}/flow/draft`,
      {
        data: { name: "new-flow", duplicatingDraftFlowId: null },
      }
    );

    if (createNewFlowResponse.status() != 201) {
      throw new Error(
        `API to add new flow failed with ${createNewFlowResponse.status()}`
      );
    }
    return await createNewFlowResponse.json();
  }

  async addLinearFlowWithoutTicketing() {
    let createNewFlowResponse = this.addNewFlow(
      FlowType.REGISTRATION,
      FlowStatus.DRAFT
    );

    let newFlowData = await createNewFlowResponse;
    let flowId = newFlowData["flowId"];

    let dataPayLoad = await this.getUpdateFlowBodyJSON(
      FlowType.REGISTRATION,
      FlowStatus.DRAFT
    );
    console.log("-----------------------");
    console.log(dataPayLoad);
    console.log("-----------------------");

    const customTemplates = await this.getCustomTemplates(flowId);
    dataPayLoad["flow"]["customTemplates"] = customTemplates;
    dataPayLoad["flow"]["version"] = 1;
    dataPayLoad["flow"]["id"] = flowId;
    const updateFlowDataResponse = await this.orgApiRequest.post(
      `api/event/${this.eventId}/flow/${flowId}/${FlowStatus.DRAFT}`,
      {
        data: dataPayLoad,
      }
    );

    if (updateFlowDataResponse.status() != 200) {
      throw new Error(
        `API to add update flow json data failed with ${updateFlowDataResponse.status()}`
      );
    }
    let updatedResponseData = await updateFlowDataResponse.json();

    console.log(updatedResponseData);

    await this.publishFlow(updatedResponseData["flow"]["id"]);
    return updatedResponseData;
  }

  async publishFlow(flowDraftId: string) {
    let publishdataPayLoad = {
      flowId: "",
      flowDraftId: flowDraftId,
      clientFlowDraftVersion: 2,
      isDefaultFlow: true,
    };
    const publishFlowResponse = await this.orgApiRequest.post(
      `api/event/${this.eventId}/flow/draft/publish`,
      {
        data: publishdataPayLoad,
      }
    );

    if (publishFlowResponse.status() != 200) {
      throw new Error(
        `API to publish the flow failed with ${publishFlowResponse.status()}`
      );
    }
    return await publishFlowResponse.json();
  }

  async generateMultiflowWithoutTicketing() {
    let flowDraftId = await this.getFlowId(
      FlowType.REGISTRATION,
      FlowStatus.DRAFT
    );
    let publishdataPayLoad = {
      flowId: "",
      flowDraftId: flowDraftId,
      clientFlowDraftVersion: 2,
    };
    await this.generateFlowWithBranching(
      FlowType.REGISTRATION,
      FlowStatus.DRAFT
    );
    await this.publishFlowData(publishdataPayLoad);

    let linearFlowResponse = await this.addLinearFlowWithoutTicketing();
  }

  async getPublishedFlowIds() {
    let flowDraftListResponse = await this.orgApiRequest.get(
      `api/event/${this.eventId}/flow/draft/list`
    );
    if (flowDraftListResponse.status() != 200) {
      throw new Error(
        `API to fetch flow draft list failed with ${flowDraftListResponse.status()}`
      );
    }
    let flowDraftList = await flowDraftListResponse.json();
    console.log(flowDraftList);
    let publishedFlowIds = flowDraftList.map((flow: any) => {
      return flow.publishedFlowId;
    });
    console.log(publishedFlowIds);
    return publishedFlowIds;
  }
  async getHtmlForFlow(publishedFlowId: string): Promise<string> {
    const placeholderEmbedFile = path.resolve("test-data/", "flow-embed.html");
    const embedHtml: string = fs.readFileSync(placeholderEmbedFile, "utf-8");
    let finalHtmlEmbed = embedHtml.replace("{{eventId}}", this.eventId);
    finalHtmlEmbed = finalHtmlEmbed.replace("{{flowId}}", publishedFlowId);
    finalHtmlEmbed = finalHtmlEmbed.replace(/{{env}}/g, process.env.run_env);
    console.log(finalHtmlEmbed);
    return finalHtmlEmbed;
  }
}
