import { APIRequestContext } from "@playwright/test";
import { inspect } from "util";

interface sessionObject {
  day?: string;
  eventId;
  hiddenSegment?: boolean;
  refId?: string;
  description: string;
  startDateTime;
  endDateTime;
  speakers?;
  title;
  type;
  segmentFormat?: string;
  eventInPersonLocationId?: null;
  maxAvailableSpots?: null;
}

interface sessionObjectNew {
  title: string;
  description: string;
  hiddenSegment?: boolean;
  speakers?: string[];
  eventTagIds?: string[];
  type: string;
  startDateTime;
  endDateTime;
  refId?;
  eventId: string;
  day?: string;
}

interface AddSessionPayload {
  title: string;
  description: string;
  hiddenSegment?: boolean;
  speakers?: string[];
  type?: string;
  startDateTime: Date;
  endDateTime: Date;
  refId?;
  eventId: string;
  day?: string;
  segmentFormat?: string;
  eventInPersonLocationId?: null;
  maxAvailableSpots?: null;
}

export class ScheduleController {
  orgApiRequestContext: APIRequestContext;
  eventId;

  constructor(orgApiRequestContext: APIRequestContext, eventId) {
    this.orgApiRequestContext = orgApiRequestContext;
    this.eventId = eventId;
  }

  async addSessionToScheduleEvent(sessionObj: sessionObject) {
    sessionObj.speakers = sessionObj.speakers || [];
    sessionObj.refId = sessionObj.refId || "";
    sessionObj.day = sessionObj.day || "";
    sessionObj.hiddenSegment = sessionObj.hiddenSegment || false;

    const addSessionEndPoint = `/api/event/${this.eventId}/segment`;
    const addSessionApiResp = await this.orgApiRequestContext.post(
      addSessionEndPoint,
      {
        data: sessionObj,
      }
    );
    // validate the add session api call
    if (addSessionApiResp.status() != 200) {
      throw new Error(
        `API call to add new session failed with ${addSessionApiResp.status()}`
      );
    }
    let sessionResponse = await addSessionApiResp.json();
    console.log("session schedule response:");
    console.log(sessionResponse);
  }

  async addSession(sessionObj: sessionObject): Promise<any> {
    const addSessionEndPoint = `/api/event/${this.eventId}/segment`;
    const addSessionApiResp = await this.orgApiRequestContext.post(
      addSessionEndPoint,
      {
        data: sessionObj,
      }
    );

    // validate the add session api call
    if (addSessionApiResp.status() != 200) {
      throw new Error(
        `API call to add new session failed with ${addSessionApiResp.status()} ${await addSessionApiResp.body()}`
      );
    }

    let sessionResponse = await addSessionApiResp.json();
    console.log("session schedule response:");
    console.log(sessionResponse);

    return sessionResponse["segmentId"];
  }

  async addSessionInEvent({
    title = "Test Session",
    description = "Test Description",
    startDay = 0,
    startHour = 0,
    startMinute = 0,
    endDay = 0,
    endHour = 2,
    endMinute = 0,
    type = "NONE",
    refId = "",
    speakers = [],
    hiddenSegment = false,
    segmentFormat = "VIRTUAL",
  }): Promise<any> {
    let addSessionJson: AddSessionPayload;

    let currentDate = new Date();
    console.log(currentDate);

    let startDate = currentDate;
    startDate.setDate(currentDate.getDate() + startDay);
    startDate.setHours(currentDate.getHours() + startHour);
    startDate.setMinutes(currentDate.getMinutes() + startMinute);
    let sd = startDate.toISOString().split(".")[0] + "Z";
    console.log(`Session start date is ${sd}`);

    let endDate = currentDate;
    endDate.setDate(currentDate.getDate() + endDay);
    endDate.setHours(currentDate.getHours() + endHour);
    endDate.setMinutes(currentDate.getMinutes() + endMinute);
    let ed = endDate.toISOString().split(".")[0] + "Z";
    console.log(`Session end date is ${ed}`);

    console.log(title);
    addSessionJson.title = title;
    console.log(description);
    addSessionJson.description = description;
    addSessionJson.hiddenSegment = hiddenSegment;
    addSessionJson.speakers = speakers;
    addSessionJson.type = type;
    addSessionJson.startDateTime = startDate;
    addSessionJson.endDateTime = endDate;
    addSessionJson.refId = refId;
    addSessionJson.eventId = this.eventId;
    addSessionJson.day = "";
    addSessionJson.segmentFormat = segmentFormat;
    addSessionJson.eventInPersonLocationId = null;
    addSessionJson.maxAvailableSpots = null;

    const addSessionUri = `/api/segment/${this.eventId}/v2`;
    const response = await this.orgApiRequestContext.post(addSessionUri, {
      data: addSessionJson,
    });

    if (!response.ok()) {
      throw new Error(
        `API call to add Session failed with ${response.status()} ${await response.body()}`
      );
    }

    let addSessionResponse = await response.json();
    console.log(
      "Session created with response -> ",
      inspect(addSessionResponse, { depth: null })
    );

    return addSessionResponse["segmentId"];
  }

  async deleteSession(sessionId) {
    const deleteSessionEndPoint = `/api/segment/${this.eventId}/${sessionId}`;

    const deleteSessionApiResp = await this.orgApiRequestContext.delete(
      deleteSessionEndPoint
    );

    // validate the add session api call
    if (deleteSessionApiResp.status() != 200) {
      throw new Error(
        `API call to add new session failed with ${deleteSessionApiResp.status()}`
      );
    }
    return deleteSessionApiResp;
  }

  async updateSession(
    sessionId,
    updatedSessionObj: {
      title: string;
      description: string;
      segmentId: string;
      eventId: string;
      refId: string;
      startDateTime?: string;
      endDateTime?: string;
      speakers?: string[];
      type: string;
    }
  ) {
    const updateSessionEndPoint = `/api/segment/${sessionId}`;

    const updateSessionApiResp = await this.orgApiRequestContext.patch(
      updateSessionEndPoint,
      {
        data: updatedSessionObj,
      }
    );

    // validate the add session api call
    if (updateSessionApiResp.status() != 200) {
      throw new Error(
        `API call to add new session failed with ${updateSessionApiResp.status()}`
      );
    }
    return updateSessionApiResp;
  }
}
