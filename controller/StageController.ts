import { APIRequestContext, APIResponse, test, expect } from "@playwright/test";
import { DataUtl } from "../util/dataUtil";
import { StudioController } from "./StudioController";
import StageSize from "../enums/StageSizeEnum";

export class StageController {
  orgApiRequest: APIRequestContext;
  stageId;
  eventId;
  constructor(orgApiRequest: APIRequestContext, eventId, stageId?) {
    this.orgApiRequest = orgApiRequest;
    this.stageId = stageId;
    this.eventId = eventId;
  }

  async addImageOverLay(name: string) {
    const channelId = await this.getStageChannelId(this.stageId);
    //TODO: currently adding a static s3 url to reuse , later will make it dynamic
    const addImageOverlayPayload = {
      name: name,
      url: "https://phoenixlive.imgix.net/957284b8-b3dd-4250-a624-21d7eabcb1df.jpeg",
      channelImageType: "OVERLAY",
    };
    const addImageOverlayResp = await this.orgApiRequest.post(
      `/api/channel/${channelId}/image/add`,
      {
        data: addImageOverlayPayload,
      }
    );
    if (addImageOverlayResp.status() != 200) {
      throw new Error(
        `API call to add image overlay for ${
          this.stageId
        } failed with ${addImageOverlayResp.status()} `
      );
    }
  }

  // for video broadcasting
  async addSessionVideosToStage(videoName: string) {
    const channelId = await this.getStageChannelId(this.stageId);
    //TODO: currently adding a static s3 url to reuse , later will make it dynamic
    const addSessionVideoPayload = {
      videoLink:
        "https://stream.mux.com/sxBH01SZxqtS1Na008grPFMGqp4yvquf5aPTimuSsdv3M.m3u8",
      videoName: videoName,
    };
    const addSessionVideoResp = await this.orgApiRequest.post(
      `/api/channel/${channelId}/video`,
      {
        data: addSessionVideoPayload,
      }
    );
    if (addSessionVideoResp.status() != 200) {
      throw new Error(
        `API call to add session video on stage  for ${
          this.stageId
        } failed with ${addSessionVideoResp.status()} `
      );
    }
  }

  async getStageChannelId(stageId) {
    const getChannelDetailsResp = await this.orgApiRequest.get(
      `/api/channel?refId=${stageId}`
    );
    if (getChannelDetailsResp.status() != 200) {
      throw new Error(
        `API call to fetch channel details for ${stageId} failed with ${getChannelDetailsResp.status()} `
      );
    }

    const channelDetailRespJson = await getChannelDetailsResp.json();
    return channelDetailRespJson["channelId"];
  }

  async createNewSegmentOnStage(
    {
      title = DataUtl.getRandomName(),
      description = DataUtl.getRandomName(),
      hiddenSegment = false,
      speakers = [],
      eventTagIds = [],
      type = "STAGE",
      startDateTime,
      endDateTime,
      refId = this.stageId,
      eventId = this.eventId,
      day = "",
    }: StageSegmentPayload,
    defaultSetting = true,
    startHourFromNow?,
    endHourFromNow?
  ) {
    // if default settings
    let startDateTimeSegment: string;
    let endDateTimeSegment: string;
    let currentDateTime = new Date();
    //calculate time for default setting
    if (defaultSetting) {
      startHourFromNow = 1;
      endHourFromNow = 4;
      startDateTimeSegment = new Date(
        currentDateTime.setHours(currentDateTime.getHours() + startHourFromNow)
      ).toISOString();
      endDateTimeSegment = new Date(
        currentDateTime.setHours(currentDateTime.getHours() + endHourFromNow)
      ).toISOString();
    }

    const addSegmentApiResp = await this.orgApiRequest.post(
      `/api/event/${this.eventId}/segment`,
      {
        data: {
          title: title,
          description: description,
          hiddenSegment: false,
          speakers: [],
          eventTagIds: [],
          type: type,
          startDateTime: startDateTimeSegment!,
          endDateTime: endDateTimeSegment!,
          refId: refId,
          eventId: eventId,
          day: day,
        },
      }
    );
    if (addSegmentApiResp.status() != 200) {
      throw new Error(
        `API call to add new segment failed with ${addSegmentApiResp.status()}`
      );
    }
    return addSegmentApiResp;
  }

  async deleteSegmentFromStage(segmentId) {
    const deleteSegmentApiRes = await this.orgApiRequest.delete(
      `/api/segment/${this.eventId}/${segmentId}`
    );
    if (deleteSegmentApiRes.status() != 200) {
      throw new Error(
        `API call to delete segment failed with ${deleteSegmentApiRes.status()}`
      );
    }
    return deleteSegmentApiRes;
  }

  async getAllSegmentsForThisStage() {
    const getAllSegmentResp = await this.orgApiRequest.get(
      `/api/segment/${this.eventId}/list/all?r=`
    );
    if (getAllSegmentResp.status() != 200) {
      throw new Error(
        `API call to get all segment failed with statud code ${getAllSegmentResp.status()}`
      );
    }
    //filter all segments for this stage
    const getAllSegmentRespJson = await getAllSegmentResp.json();
    let listOfSegmentsForThisStage: string[] = [];
    for (const segmentObject of getAllSegmentRespJson) {
      //get ref id
      let refId = segmentObject["refId"];
      console.log(`checking if ref id : ${refId} is equal to ${this.stageId}`);
      if (refId === this.stageId) {
        listOfSegmentsForThisStage.push(segmentObject["segmentId"]);
      }
    }
    return listOfSegmentsForThisStage;
  }

  async enableBreakoutRoomsForThisStage(
    maxRoomSize: number,
    breakoutType = "AUTOMATIC"
  ) {
    const enableBreakourRoomConfigResp = await this.orgApiRequest.post(
      `/api/${this.eventId}/breakout/config?refId=${this.stageId}&refType=STAGE`,
      {
        data: {
          breakoutType: breakoutType,
          maxRoomSize: maxRoomSize,
          stageId: this.stageId,
        },
      }
    );
    if (enableBreakourRoomConfigResp.status() != 200) {
      throw new Error(
        `API call to update breakout room config failed with ${enableBreakourRoomConfigResp.status()}`
      );
    }
    return enableBreakourRoomConfigResp;
  }

  async getStudioIdForEventStage() {
    const listWebinarStageRes = await this.orgApiRequest.get(
      `/api/stage/${this.eventId}/list`
    );
    if (listWebinarStageRes.status() != 200) {
      throw new Error(
        `API call to fetch list of stages for webinar failed with ${listWebinarStageRes.status()}`
      );
    }
    const listOfWebinarStageResJson = await listWebinarStageRes.json();
    const defaultStageObject = listOfWebinarStageResJson[0];
    const studioId = defaultStageObject["studioId"];
    console.log(`studio Id fetched for webinar ${this.eventId} is ${studioId}`);
    if (!studioId) {
      throw new Error(
        `Studio id not found in list of stage response for webinar ${this.eventId}`
      );
    }
    return studioId;
  }

  async disableRecordingForStudioAsBackstage() {
    let studioId = await this.getStudioIdForEventStage();
    let studioController = new StudioController(this.orgApiRequest, studioId);
    await studioController.updateRecordingSettingForStudio(false);
  }

  async addNewStageToEvent(stageObject: StageCreationPayLoad) {
    stageObject.name = stageObject.name || "New Stage";
    stageObject.size = stageObject.size || StageSize.SMALL;

    const addStageEndPoint = `/api/stage/${this.eventId}/create`;
    const addStageApiResp = await this.orgApiRequest.post(addStageEndPoint, {
      data: stageObject,
    });

    // validate the add stage api call
    if (!addStageApiResp.ok()) {
      throw new Error(
        `API call to add new stage failed with ${addStageApiResp.status()}`
      );
    }

    let stageResponseJson = await addStageApiResp.json();

    return stageResponseJson["stageId"];
  }

  async getChannelResponse(): Promise<APIResponse> {
    let getChannelIdApiResp: APIResponse;
    await test.step(`Fetching the channel response for ${this.stageId}`, async () => {
      getChannelIdApiResp = await this.orgApiRequest.get(
        `/api/channel?refId=${this.stageId}`
      );
      if (getChannelIdApiResp.status() != 200) {
        throw new Error(
          `API to fetch channelId for room failed with ${getChannelIdApiResp.status()}`
        );
      }
    });
    return getChannelIdApiResp;
  }

  async getChannelId() {
    const getChannelApiResp = await this.getChannelResponse();
    const getChannelIdRespJson = await getChannelApiResp.json();
    const thisStageChannelId = getChannelIdRespJson["channelId"];
    console.log(
      `The channel id fetched for stage ${this.stageId} is ${thisStageChannelId}`
    );
    return thisStageChannelId;
  }

  async updateStageEngagementSettingsForThisStage(
    moderatedStageSettingPaylod: ModeratedStageSettingPaylod
  ) {
    const getChannelApiResp = await this.getChannelResponse();
    const getChannelApiRespJson = await getChannelApiResp.json();
    const channelId = getChannelApiRespJson["channelId"];
    moderatedStageSettingPaylod.channelId = channelId;
    console.log(`channel id  fetched is ${channelId}`);
    await test.step(`Hitting settings api for this channel to enable chat: ${moderatedStageSettingPaylod.hasChat} and enable moderated chat ${moderatedStageSettingPaylod.hasChatModerate}`, async () => {
      const updateStageEngagementSettingsApiResp =
        await this.orgApiRequest.post(`/api/channel/${channelId}/settings`, {
          data: moderatedStageSettingPaylod,
        });

      const updateApiStatus = updateStageEngagementSettingsApiResp.status();
      if (updateApiStatus != 200) {
        throw new Error(
          `API call to udpate stage engagement settings failed with ${updateApiStatus}`
        );
      }
    });
  }

  async updateRandomiseStageSettings() {
    const getChannelApiResp = await this.getChannelResponse();
    const getChannelApiRespJson = await getChannelApiResp.json();
    const channelId = getChannelApiRespJson["channelId"];
    const settingsOrder = getChannelApiRespJson["settingsOrder"];
    console.log(`channel id  fetched is ${channelId}`);
    console.log(`settings order fetched is ${settingsOrder}`);
    console.log(getChannelApiRespJson);

    let settingsPayload = {
      hasChat: Math.random() < 0.5,
      hasChatModerate: Math.random() < 0.5,
      hasQuestion: Math.random() < 0.5,
      hasQuestionModerate: Math.random() < 0.5,
      hasPoll: Math.random() < 0.5,
      hasFile: Math.random() < 0.5,
      hasPeople: Math.random() < 0.5,
      hasRequestToJoin: Math.random() < 0.5,
      hasAudienceGallery: Math.random() < 0.5,
      hasContributorView: Math.random() < 0.5,
      channelId: channelId,
      settingsOrder: settingsOrder,
    };

    settingsPayload.hasChatModerate = settingsPayload.hasChat
      ? settingsPayload.hasChatModerate
      : false;
    settingsPayload.hasQuestionModerate = settingsPayload.hasQuestion
      ? settingsPayload.hasQuestionModerate
      : false;

    await test.step(`Updating the settings`, async () => {
      const updateStageEngagementSettingsApiResp =
        await this.orgApiRequest.post(`/api/channel/${channelId}/settings`, {
          data: settingsPayload,
        });

      const updateApiStatus = updateStageEngagementSettingsApiResp.status();
      if (updateApiStatus != 200) {
        throw new Error(
          `API call to update stage engagement settings failed with ${updateApiStatus}`
        );
      }

      const updatedSettingsApiResp = await this.orgApiRequest.get(
        `/api/channel/${channelId}/settings`
      );
      const updatedSettingsApiRespJson = await updatedSettingsApiResp.json();
      console.log(updatedSettingsApiRespJson);

      expect(updatedSettingsApiRespJson["hasChat"]).toBe(
        settingsPayload.hasChat
      );
      expect(updatedSettingsApiRespJson["hasChatModerate"]).toBe(
        settingsPayload.hasChatModerate
      );
      expect(updatedSettingsApiRespJson["hasQuestion"]).toBe(
        settingsPayload.hasQuestion
      );
      expect(updatedSettingsApiRespJson["hasQuestionModerate"]).toBe(
        settingsPayload.hasQuestionModerate
      );
      expect(updatedSettingsApiRespJson["hasPoll"]).toBe(
        settingsPayload.hasPoll
      );
      expect(updatedSettingsApiRespJson["hasFile"]).toBe(
        settingsPayload.hasFile
      );
      expect(updatedSettingsApiRespJson["hasPeople"]).toBe(
        settingsPayload.hasPeople
      );
      expect(updatedSettingsApiRespJson["hasRequestToJoin"]).toBe(
        settingsPayload.hasRequestToJoin
      );
    });

    await test.step(`Update the background as well`, async () => {
      const updateBackgroundApi = await this.orgApiRequest.post(
        `/api/event/${this.eventId}/stage/${this.stageId}/layout`,
        {
          data: {
            bgUrl: `website/bg-common-${DataUtl.randomIntFromInterval(
              1,
              8
            )}.png`,
          },
        }
      );
      expect(updateBackgroundApi.status()).toBe(200);
    });

    await test.step(`Update the stage name as well`, async () => {
      const updateStageNameApi = await this.orgApiRequest.post(
        `/api/stage/${this.stageId}/update`,
        {
          data: { stageId: this.stageId, name: DataUtl.getRandomName() },
        }
      );

      expect(updateStageNameApi.status()).toBe(200);
    });

    let randomStage;

    await test.step(`Get the stage details`, async () => {
      const getStageDetailsApi = await this.orgApiRequest.get(
        `/api/stage/${this.stageId}/details`
      );

      expect(getStageDetailsApi.status()).toBe(200);

      const getStageDetailsApiJson = await getStageDetailsApi.json();
      randomStage = {
        stageId: getStageDetailsApiJson["stageId"],
        stageName: getStageDetailsApiJson["name"],
        stageSize: getStageDetailsApiJson["size"],
      };

      console.log(getStageDetailsApiJson);
    });

    return randomStage;
  }

  async getAllStages() {
    const getAllStagesApiResp = await this.orgApiRequest.get(
      `/api/stage/${this.eventId}/list`
    );
    if (getAllStagesApiResp.status() != 200) {
      throw new Error(
        `API call to get all stages failed with ${getAllStagesApiResp.status()}`
      );
    }
    const getAllStagesApiRespJson = await getAllStagesApiResp.json();
    return getAllStagesApiRespJson;
  }

  async getStageLayout() {
    const layoutApiReq = await this.orgApiRequest.get(
      `/api/event/${this.eventId}/stage/${this.stageId}/layout`
    );

    if (layoutApiReq.status() != 200) {
      throw new Error(
        `API call to get stage layout failed with ${layoutApiReq.status()}`
      );
    }
    const layoutApiReqJson = await layoutApiReq.json();
    return layoutApiReqJson;
  }

  async getEngagementSettings() {
    const getChannelApiResp = await this.getChannelResponse();
    const getChannelApiRespJson = await getChannelApiResp.json();
    const channelId = getChannelApiRespJson["channelId"];

    const getEngagementSettingsApiResp = await this.orgApiRequest.get(
      `/api/channel/${channelId}/settings`
    );

    const getEngagementSettingsApiRespJson =
      await getEngagementSettingsApiResp.json();
    console.log(
      `Engagement settings for stage ${this.stageId} is ${JSON.stringify(
        getEngagementSettingsApiRespJson
      )}`
    );
    // ["hasChat"]
    // ["hasChatModerate"]
    // ["hasQuestion"]
    // ["hasQuestionModerate"]
    // ["hasPoll"]
    // ["hasFile"]
    // ["hasPeople"]
    // ["hasRequestToJoin"]
    // only return these

    const minimisedEngagementSettings = {
      hasChat: getEngagementSettingsApiRespJson["hasChat"],
      hasChatModerate: getEngagementSettingsApiRespJson["hasChatModerate"],
      hasQuestion: getEngagementSettingsApiRespJson["hasQuestion"],
      hasQuestionModerate:
        getEngagementSettingsApiRespJson["hasQuestionModerate"],
      hasPoll: getEngagementSettingsApiRespJson["hasPoll"],
      hasFile: getEngagementSettingsApiRespJson["hasFile"],
      hasPeople: getEngagementSettingsApiRespJson["hasPeople"],
      hasRequestToJoin: getEngagementSettingsApiRespJson["hasRequestToJoin"],
    };

    return minimisedEngagementSettings;
  }
}

export interface ModeratedStageSettingPaylod {
  hasChat?: boolean;
  hasChatModerate?: boolean;
  hasQuestion?: boolean;
  hasQuestionModerate?: boolean;
  hasPoll?: boolean;
  hasFile?: boolean;
  hasPeople?: boolean;
  hasRequestToJoin?: boolean;
  hasAudienceGallery?: boolean;
  hasContributorView?: boolean;
  channelId?: number;
  settingsOrder?: string;
}
export interface StageCreationPayLoad {
  name: string;
  size: StageSize;
}

export interface StageSegmentPayload {
  title?: string;
  description?: string;
  hiddenSegment?: false;
  speakers?: [];
  eventTagIds?: [];
  type?: string;
  startDateTime?: string;
  endDateTime?: string;
  refId?: string;
  eventId?: string;
  day?: string;
}
