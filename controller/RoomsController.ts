import { APIRequestContext, expect } from "@playwright/test";
import { DataUtl } from "../util/dataUtil";
import {
  RoomsControllerInterface,
  roomDetailsToBeUpdatedPayload,
} from "../interfaces/RoomsControllerInterface";
import { inspect } from "util";
import { count } from "console";

interface AddNewRoomsPayLoad {
  seatsPerRoom: number;
  numberOfRooms: number;
  roomCategory: "PUBLIC" | "PRIVATE";
  isLargeRooms?: boolean;
}

export class RoomsController implements RoomsControllerInterface {
  readonly orgApiRequestContext: APIRequestContext;
  readonly eventId;

  constructor(orgApiRequestContext: APIRequestContext, eventId) {
    this.orgApiRequestContext = orgApiRequestContext;
    this.eventId = eventId;
  }

  async addRoomsToEvent({
    seatsPerRoom,
    numberOfRooms,
    roomCategory,
  }: AddNewRoomsPayLoad) {
    const countOfExistingRooms = await this.getCountOfCurrentRooms();

    const addRoomsEndpoint = `/api/event/${this.eventId}/discussion_room`;
    const addRoomsApiResp = await this.orgApiRequestContext.post(
      addRoomsEndpoint,
      {
        data: {
          tableCapacity: seatsPerRoom,
          tableCount: numberOfRooms,
          isLargeRoom: false,
          roomCategory: roomCategory,
        },
      }
    );
    // validate the add rooms api call
    if (addRoomsApiResp.status() != 200) {
      throw new Error(
        `API call to add new room failed with ${addRoomsApiResp.status()}`
      );
    }
    const totalCountOfRooms = countOfExistingRooms + numberOfRooms;
    await expect(async () => {
      const getListOfRoomsResp = await this.getListOfRooms();
      const getListOfRoomsJson = await getListOfRoomsResp.json();
      expect(
        getListOfRoomsJson,
        `expecting list of rooms api to return ${totalCountOfRooms}`
      ).toHaveLength(totalCountOfRooms);
    }, `polling list apis until, the list of rooms returns as  ${totalCountOfRooms}`).toPass(
      { timeout: 5000 }
    );
  }

  async getCountOfCurrentRooms() {
    const getListOfRoomsResp = await this.getListOfRooms();
    const getListOfRoomsJson = await getListOfRoomsResp.json();
    const numberOfRooms = getListOfRoomsJson.length;

    return numberOfRooms;
  }

  async getListOfRooms() {
    const getListOfRoomsApi = `/api/discussion/list/${this.eventId}`;
    const getListOfRoomsApiResp = await this.orgApiRequestContext.get(
      getListOfRoomsApi
    );
    // validate the get rooms api call
    if (getListOfRoomsApiResp.status() != 200) {
      throw new Error(
        `API call to get list of rooms failed with ${getListOfRoomsApiResp.status()}`
      );
    }
    let roomsListJson = await getListOfRoomsApiResp.json();
    console.log("Rooms List -> ", inspect(roomsListJson, { depth: null }));
    return getListOfRoomsApiResp;
  }

  async getRoomId(roomName: string) {
    let roomDetails = await this.getListOfRooms();
    let roomDetailsJsonList = await roomDetails.json();

    let roomId;
    for (var i = 0; i < roomDetailsJsonList.length; i++) {
      if (roomDetailsJsonList[i]["name"] === roomName) {
        roomId = roomDetailsJsonList[i]["discussionTableId"];
      }
    }

    return roomId;
  }

  async getRoomDetails(roomId: string) {
    const getRoomDetailsApi = `/api/discussion/${roomId}`;
    const getRoomDetailsApiResp = await this.orgApiRequestContext.get(
      getRoomDetailsApi
    );
    // validate the get rooms api call

    if (getRoomDetailsApiResp.status() != 200) {
      throw new Error(
        `API call to get room details failed with ${getRoomDetailsApiResp.status()}`
      );
    }
    let roomDetails = await getRoomDetailsApiResp.json();

    return roomDetails;
  }

  async updateRoomCapacity(roomId: string, capacity: number) {
    console.log(`Updating room capacity to ${capacity}`);
    let roomDetails = await this.getRoomDetails(roomId);
    console.log(`Room capacity is ${roomDetails.capacity}`);

    if (roomDetails.capacity == capacity) {
      console.log(`Room capacity is already ${capacity}`);
      return;
    }

    roomDetails.capacity = capacity;

    console.log(`Updating room capacity to ${capacity}`);

    const updateRoomCapacityApi = `/api/discussion/${roomId}/update`;

    const updateRoomCapacityApiResp = await this.orgApiRequestContext.post(
      updateRoomCapacityApi,
      {
        data: roomDetails,
      }
    );
    // validate the get rooms api call
    if (updateRoomCapacityApiResp.status() != 200) {
      throw new Error(
        `API call to update room capacity failed with ${updateRoomCapacityApiResp.status()}`
      );
    }
    const updatedRoomDetails = await updateRoomCapacityApiResp.json();
    return updatedRoomDetails;
  }

  async updateRoomDetails(
    roomId: string,
    detailsToUpdate: roomDetailsToBeUpdatedPayload
  ) {
    const updateRoomApi = `/api/discussion/${roomId}/update`;
    const updateRoomApiResp = await this.orgApiRequestContext.post(
      updateRoomApi,
      {
        data: detailsToUpdate,
      }
    );
    // validate the get rooms api call
    if (updateRoomApiResp.status() != 200) {
      throw new Error(
        `API call to update room name failed with ${updateRoomApiResp.status()}`
      );
    }
    const updatedRoomDetails = await updateRoomApiResp.json();
    return updatedRoomDetails;
  }

  async addTagsToRoom(roomId: string, tags) {
    let roomDetails = await this.getRoomDetails(roomId);

    roomDetails.eventTagIds = tags.map((tag) => tag.tagId);

    const updateRoomCapacityApi = `/api/discussion/${roomId}/update`;

    const updateRoomCapacityApiResp = await this.orgApiRequestContext.post(
      updateRoomCapacityApi,
      {
        data: roomDetails,
      }
    );

    // validate the get rooms api call

    if (updateRoomCapacityApiResp.status() != 200) {
      throw new Error(
        `API call to update room capacity failed with ${updateRoomCapacityApiResp.status()}`
      );
    }

    const updatedRoomDetails = await updateRoomCapacityApiResp.json();

    return updatedRoomDetails;
  }

  async randomizeRoomDetails(roomId: string) {
    let roomDetails = await this.getRoomDetails(roomId);
    // name, capacity, description, primaryColour, logo
    roomDetails.name = DataUtl.generateRandomBoothName();
    roomDetails.capacity = DataUtl.randomIntFromInterval(1, 10);
    roomDetails.description = `<p>Randomized description ${roomDetails.name}</p>`;
    roomDetails.primaryColour = DataUtl.getRandomColorCode(true);
    roomDetails.logo = DataUtl.getRandomImageUrl();

    const updateRoomCapacityApi = `/api/discussion/${roomId}/update`;

    const updateRoomCapacityApiResp = await this.orgApiRequestContext.post(
      updateRoomCapacityApi,
      {
        data: roomDetails,
      }
    );

    // validate the get rooms api call

    if (updateRoomCapacityApiResp.status() != 200) {
      throw new Error(
        `API call to update room capacity failed with ${updateRoomCapacityApiResp.status()}`
      );
    }

    const updatedRoomDetails = await updateRoomCapacityApiResp.json();

    roomDetails = await this.getRoomDetails(roomId);
    expect(roomDetails.name).toEqual(updatedRoomDetails.name);
    expect(roomDetails.capacity).toEqual(updatedRoomDetails.capacity);
    expect(roomDetails.description).toEqual(updatedRoomDetails.description);
    expect(roomDetails.primaryColour).toEqual(updatedRoomDetails.primaryColour);
    expect(roomDetails.logo).toEqual(updatedRoomDetails.logo);

    return roomDetails;
  }
}
