import { APIRequestContext, Page } from "@playwright/test";
import { EventController } from "./EventController";

export class ChatController {
  readonly orgApiContext: APIRequestContext;
  readonly eventId: string;
  constructor(orgApiContext: APIRequestContext, eventId: string) {
    this.orgApiContext = orgApiContext;
    this.eventId = eventId;
  }

  async postMessageInEventChat(contentToPost: string, postAsHost = false) {
    const eventChannelId = await new EventController(
      this.orgApiContext,
      this.eventId
    ).getEventChannelId();
    await this.postMessageInGivenChannel(
      eventChannelId,
      contentToPost,
      postAsHost
    );
  }

  async postMessageInGivenChannel(
    channelId: string,
    contentToPost: string,
    postAsHost = false
  ) {
    const postMessageApiResp = await this.orgApiContext.post(
      `/api/channel/${channelId}/message?host=${postAsHost}`,
      {
        data: {
          content: contentToPost,
          mediaType: "text",
        },
      }
    );
    if (postMessageApiResp.status() != 200) {
      throw new Error(
        `API call to post new message in channel id : ${channelId} failed with ${postMessageApiResp.status()}`
      );
    }
  }

  //pin chat features
  async triggerApiToPinChatMessage(messageId: string) {
    const pinChatApiResp = await this.orgApiContext.put(
      `/api/message/${messageId}/pin/true`
    );
    if (pinChatApiResp.status() != 200) {
      throw new Error(
        `API call to pin chat message id ${messageId} failed with status ${pinChatApiResp.status()}`
      );
    }
  }
  async triggerApiToUnPinChatMessage(messageId: string) {
    const unpinChatApiResp = await this.orgApiContext.put(
      `/api/message/${messageId}/pin/false`
    );
    if (unpinChatApiResp.status() != 200) {
      throw new Error(
        `API call to unpin chat message id ${messageId} failed with status ${unpinChatApiResp.status()}`
      );
    }
  }
  async triggerApiToDeleteChatMessage(messageId: string) {
    const deleteChatApiResp = await this.orgApiContext.delete(
      `/api/message/${messageId}`
    );
    if (deleteChatApiResp.status() != 200) {
      throw new Error(
        `API call to delete chat message id ${messageId} failed with status ${deleteChatApiResp.status()}`
      );
    }
  }

  async getListOfChatMessages(
    channelId: string,
    forHost = false,
    isPinned = false
  ) {
    const listOfChatmessagesApiResp = await this.orgApiContext.get(
      `/api/channel/${channelId}/message/list?host=${forHost}&isPinned=${isPinned}`
    );
    if (listOfChatmessagesApiResp.status() != 200) {
      throw new Error(
        `API call to get list of chat messages  failed with status ${listOfChatmessagesApiResp.status()}`
      );
    }
  }
}
