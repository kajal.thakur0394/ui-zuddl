import { APIRequestContext, expect } from "@playwright/test";
import { DataUtl } from "../util/dataUtil";

export class RegistrationFormController {
  protected orgApiRequestContext: APIRequestContext;
  protected eventId;

  constructor(orgApiRequestContext: APIRequestContext, eventId) {
    this.orgApiRequestContext = orgApiRequestContext;
    this.eventId = eventId;
  }

  async addCustomFieldsWithConditionToEvent(listOfCustomFieldsToAdd) {
    /*
    1. Make a copy of the test data object passed to this functon
    2. Process the test data object and replace event id and landing page id of this event
    3. get the information tab response and fetch the reg custom feield object from it
    4. now iterate through each reg feield obj from test data and see if it has the condition fields object
    5. itreate through each condition, fetch corresponding custom field id for the label
    6. hit add conditions endpoint

    */
    //    1. Make a copy of the test data object passed to this functon
    const testDataObject = JSON.parse(JSON.stringify(listOfCustomFieldsToAdd));

    console.log(`#### Fetching the event information tab response #### `);
    let informationTabViewRespJson = await (
      await this.getInformationTabView()
    ).json();

    console.log(`#### Extracting the landing page id #### `);
    let landingPageId = await this.getEventLandingPageId();

    //modify the data to add event id and landing page id for this event
    console.log(
      "#### modifying the test data and replace the event id and landing page id #### "
    );
    //2. Process the test data object and remove conditional fields object
    for (const eachCustomFieldObj of testDataObject) {
      eachCustomFieldObj.eventId = this.eventId;
      eachCustomFieldObj.eventLandingPageId = landingPageId;
    }

    //3. Now replace the reg custom field object of fetched information tab response with the test data obj
    informationTabViewRespJson.registrationCustomFields = testDataObject;

    //4. Now post the information tab response with updated payload with custom fields
    await this.updateInformationTabView(informationTabViewRespJson);

    //5. Now wait until the get call to information tab starts showing the custom fields
    let informationTabResponseWithCustomFields;
    await expect(async () => {
      const informationTabRespWithCustomFields =
        await this.getInformationTabView();
      const informationTabRespWithCustomFieldsJson =
        await informationTabRespWithCustomFields.json();
      const listOfcustomFieldsObject =
        informationTabRespWithCustomFieldsJson.registrationCustomFields;
      expect(
        listOfcustomFieldsObject.length,
        "expecting length of list of custom fields object to be > 0"
      ).not.toBe(0);
      informationTabResponseWithCustomFields =
        informationTabRespWithCustomFieldsJson;
    }, "waiting for information tab response to show updated custom fields").toPass();

    //6. Now apply conditions to the fields who have condition object in test data
    const listOfFetchedcustomFieldObjectWithIds =
      informationTabResponseWithCustomFields["registrationCustomFields"];

    for (const eachCustomFieldObj of testDataObject) {
      let thisFieldName = eachCustomFieldObj["fieldName"];
      let thisFieldLabel = eachCustomFieldObj["labelName"];
      //fetch the reg custom feild id of this field

      let thisFieldCustomFieldId =
        await this.fetchRegistrationCustomFieldIdWithLabel(
          thisFieldLabel,
          informationTabResponseWithCustomFields
        );

      //checking if this field is conditional (if len conditional obj > 0)
      let listOfConditionalFieldsObject =
        eachCustomFieldObj["customFieldConditions"];
      if (listOfConditionalFieldsObject.length > 0) {
        console.log(`this field ${thisFieldName} have conditional objects`);

        // now apply conditional to this field
        for (const eachConditionObject of listOfConditionalFieldsObject) {
          const dependentFieldLabel =
            eachConditionObject["conditionCustomFieldId"];
          // finding the dependent field custom id
          let dependentFieldCustomFieldId =
            await this.fetchRegistrationCustomFieldIdWithLabel(
              dependentFieldLabel,
              informationTabResponseWithCustomFields
            );
          // now replace the label with id in conditional field object
          eachConditionObject["conditionCustomFieldId"] =
            dependentFieldCustomFieldId;
        }
        // now you have the updated list of conditionalFields Object
        await this.hitPostApiToApplyConditions(
          listOfConditionalFieldsObject,
          thisFieldCustomFieldId
        );
      }
    }
  }

  async hitPostApiToApplyConditions(
    listOfConditionsObject,
    registrationFieldCustomId
  ) {
    const applyConditionEndpoint = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/api/event/landing_page/${
      this.eventId
    }/${registrationFieldCustomId}/field_conditions`;

    const applyConditionApiResp = await this.orgApiRequestContext.post(
      applyConditionEndpoint,
      {
        data: listOfConditionsObject,
      }
    );
    if (applyConditionApiResp.status() != 200) {
      throw new Error(`Post API call to apply condition on field ${registrationFieldCustomId} failed with 
      ${applyConditionApiResp.status()}`);
    }
  }

  async addDiscalimerFieldsToEvent(listOfDiscalimerFieldsToAdd) {
    const testDataObjectOfDiscalimerFields = JSON.parse(
      JSON.stringify(listOfDiscalimerFieldsToAdd)
    );
    console.log(`#### Fetching the event information tab response #### `);
    let informationTabViewRespJson = await (
      await this.getInformationTabView()
    ).json();

    console.log(`#### Extracting the landing page id #### `);
    let landingPageId = await this.getEventLandingPageId();
    //modify the data to add event id and landing page id for this event

    console.log(
      "#### modifying the test data and replace the event id and landing page id #### "
    );
    //2. Process the test data object and remove conditional fields object
    for (const eachDiscalimerFieldObject of testDataObjectOfDiscalimerFields) {
      eachDiscalimerFieldObject.eventLandingPageId = landingPageId;
    }

    //3. Now replace the reg custom field object of fetched information tab response with the test data obj
    informationTabViewRespJson.eventLandingPageDisclaimers =
      testDataObjectOfDiscalimerFields;

    //4. now update the information tab with discalimer fields
    await this.updateInformationTabView(informationTabViewRespJson);
  }

  async fetchRegistrationCustomFieldIdWithLabel(
    fieldLabel,
    informationTabResponseJson
  ) {
    const listOfRegistrationFields =
      informationTabResponseJson.registrationCustomFields;
    if (listOfRegistrationFields.length == 0) {
      throw new Error(
        `recieved informatin tab response does not have any custom field`
      );
    }
    for (const eachRegistrationCustomFieldObj of listOfRegistrationFields) {
      const currentFieldLabel =
        eachRegistrationCustomFieldObj["labelName"].toLowerCase();
      if (currentFieldLabel == fieldLabel.toLowerCase()) {
        let registrationCustomFieldId =
          eachRegistrationCustomFieldObj["registrationCustomFieldId"];
        console.log(
          `field with label ${fieldLabel} found in the informaiton tab response with id : ${registrationCustomFieldId}`
        );
        return registrationCustomFieldId;
      }
    }
  }

  async getEventLandingPageId() {
    const eventLandingPageResp = await this.orgApiRequestContext.get(
      `/api/event/landing_page/${this.eventId}`
    );

    if (eventLandingPageResp.status() != 200) {
      throw new Error(
        `API to fetch event landing page details failed with ${eventLandingPageResp.status()}`
      );
    }

    let eventLandingPageResJson = await eventLandingPageResp.json();
    let eventLandingPageId = eventLandingPageResJson.eventLandingPageId;
    console.log(`the event landing page id is ${eventLandingPageId}`);

    return eventLandingPageId;
  }

  async getInformationTabView() {
    let landingPageId = await this.getEventLandingPageId();
    let getInformationTabViewResp = await this.orgApiRequestContext.get(
      `/api/event/landing_page/${this.eventId}/${landingPageId}/information_tab_view`
    );
    if (getInformationTabViewResp.status() != 200) {
      throw new Error(
        `API to fetch information tab view failed with ${getInformationTabViewResp.status()}`
      );
    }
    return getInformationTabViewResp;
  }

  async updateInformationTabView(payLoadToUpdateAsJson) {
    let landingPageId = await this.getEventLandingPageId();
    let postInformationTabViewResp = await this.orgApiRequestContext.post(
      `/api/event/landing_page/${this.eventId}/${landingPageId}/information_tab_view`,
      {
        data: payLoadToUpdateAsJson,
      }
    );
    if (postInformationTabViewResp.status() != 200) {
      throw new Error(
        `API to add custom fields in information tab view failed with ${postInformationTabViewResp.status()}`
      );
    }
  }

  async writeEmbeddedFormHtml() {
    const fs = require("fs");

    const emdeddedFormHtml = `<div id="zuddl-embeddable-form" data-zuddl-event-id=${this.eventId} style="width:100%;height:100%;overflow-y:scroll;"></div>
   <link rel="stylesheet" href="https://static.zuddl.com/embed/reg-form.css">
   <script>const global = globalThis;</script>
   <script src="https://static.zuddl.com/staging/embed/reg-form.js"></script>`;

    await fs.writeFileSync("newFile.html", emdeddedFormHtml);
  }
}
