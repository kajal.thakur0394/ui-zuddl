import { APIRequestContext } from "@playwright/test";
import { NewOrgDataFields } from "../enums/newOrganizationEnum";
import { fetchOtpFromEmail } from "../util/emailUtil";

export class NewOrgCreationController {
  readonly orgApiRequestContext: APIRequestContext;
  constructor(orgApiRequestContext) {
    this.orgApiRequestContext = orgApiRequestContext;
  }

  async createNewOrganization(
    organizationName: string,
    firstName: string,
    lastName: string,
    email: string,
    productType: NewOrgDataFields = NewOrgDataFields.PRODUCT_TYPE,
    planType: NewOrgDataFields = NewOrgDataFields.PLAN_TYPE,
    billingPeriodUnit = "Monthly",
    planStartDate = "2022-08-15T00:48:54.347556Z",
    planEndDate = "2023-12-26T00:48:54.347556Z",
    teamSize = 5,
    cloudStorageSizeInGb = 6,
    noOfAttendees = 23,
    perEventLengthInHours = 48,
    isFieldEventEnabled = false,
    isStandardEventEnabled = true
  ) {
    const createNewOrg = await this.orgApiRequestContext.post(
      `https://setup.staging.zuddl.io/api/core/organization/re-tool/create`,
      {
        headers: {
          "retool-token": "UsomURposTpwJQDPZudC",
          "Content-Type": "application/json",
        },
        data: {
          organizationName,
          firstName,
          lastName,
          email,
          licenseDetailsDto: {
            productType,
            planType,
            billingPeriodUnit,
            eventLicenseDetailsDto: {
              planStartDate,
              planEndDate,
              teamSize,
              cloudStorageSizeInGb,
              noOfAttendees,
              perEventLengthInHours,
              isFieldEventEnabled,
              isStandardEventEnabled,
            },
          },
        },
      }
    );
    if (createNewOrg.status() != 200) {
      throw new Error(
        `API call to create new org failed with ${createNewOrg.status()}`
      );
    }
    console.log("-=-=-=->", await createNewOrg.json());
    return await createNewOrg.json();
  }

  async loginToDashboard(email: string) {
    const loginInitiate = await this.orgApiRequestContext.post(
      `https://setup.staging.zuddl.io/api/zuddlotp/initiate?authFor=STUDIO`,
      {
        data: {
          email,
        },
      }
    );
    if (loginInitiate.status() != 200) {
      throw new Error(
        `API call to create new org failed with ${loginInitiate.status()}`
      );
    }

    const getOtp = await fetchOtpFromEmail(email, "Zuddl");
    console.log(getOtp);
  }
}
