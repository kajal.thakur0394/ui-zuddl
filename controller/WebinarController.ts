import { APIRequestContext, expect } from "@playwright/test";
import { readFileSync } from "fs";
import { WebinarInfoDto } from "../dto/webinarInfoDto";
import webinarPayLoadData from "../test-data/createWebinarPayload.json";
import {
  getDefaultTeamForThisOrganiser,
  inviteAttendeeByAPI,
} from "../util/apiUtil";
import { DataUtl } from "../util/dataUtil";
import { StudioController } from "./StudioController";
import {
  EventController,
  UpdateEventStartAndEndDateTime,
} from "./EventController";

interface WebinarPaylod {
  title?: string;
  tz?: string;
  startDateTime?;
  endDateTime?;
  isMagicLinkEnabled?: boolean;
  isGuestModeEnabled?: boolean;
  description?: string;
  authTypes?: [];
  teamId?: string | null;
  seriesEventId?: string | null;
}

export class WebinarController {
  private orgApiRequestContext: APIRequestContext;
  private webinarId;
  private studioId;
  private studioController: StudioController;
  private webinarInfoDto: WebinarInfoDto;

  constructor(orgApiRequestContext: APIRequestContext, webinarId?, studioId?) {
    this.orgApiRequestContext = orgApiRequestContext;

    // this.studioId = (async () => {
    //   // Call your async function using await
    //   await this.getStudioIdForWebinar();
    // })();

    if (webinarId) {
      this.webinarId = webinarId;
      this.webinarInfoDto = new WebinarInfoDto(this.webinarId);
      if (studioId) {
        this.studioId = studioId;
        this.studioController = new StudioController(
          this.orgApiRequestContext,
          this.studioId
        );
      }
      console.log(`Webinar ID - ${this.webinarId}`);
      console.log(`Studio ID - ${this.studioId}`);
    }
  }

  get getWebinarInfoDto() {
    if (this.webinarInfoDto) {
      return this.webinarInfoDto;
    } else {
      throw new Error(`Webinar info is undefined, hence cant be returned`);
    }
  }

  get getWebinarId() {
    if (this.webinarId) {
      return this.webinarId;
    } else {
      throw new Error(`Webinar info is undefined, hence cant be returned`);
    }
  }

  async createNewWebinar({
    title,
    tz,
    startDateTime,
    endDateTime,
    authTypes,
    isMagicLinkEnabled = true,
    isGuestModeEnabled = true,
    description = "created by automation",
    teamId = null,
    seriesEventId = null,
  }: WebinarPaylod) {
    let currentDate = new Date();
    webinarPayLoadData.title = title != undefined ? title : "Webinar";
    webinarPayLoadData.tz = "Asia/Kolkata";

    //set start date of webinar
    if (!startDateTime) {
      startDateTime = currentDate.toISOString().split(".")[0] + "Z";
    }
    webinarPayLoadData.startDateTime = startDateTime;

    // set end date of webianr
    if (!endDateTime) {
      endDateTime =
        new Date(currentDate.setHours(currentDate.getHours() + 2))
          .toISOString()
          .split(".")[0] + "Z";
    }
    webinarPayLoadData.endDateTime = endDateTime;

    webinarPayLoadData.isMagicLinkEnabled = isMagicLinkEnabled;
    webinarPayLoadData.isGuestModeEnabled = isGuestModeEnabled;
    webinarPayLoadData.description = description;
    webinarPayLoadData.teamId =
      teamId != null
        ? teamId
        : await getDefaultTeamForThisOrganiser(this.orgApiRequestContext);
    webinarPayLoadData.seriesEventId = seriesEventId;

    // make the post request
    const addWebinarApiResp = await this.orgApiRequestContext.post(
      `/api/event/webinar`,
      {
        data: webinarPayLoadData,
      }
    );

    if (addWebinarApiResp.status() != 200) {
      throw new Error(
        `API call to add new webinar failed with status ${addWebinarApiResp.status()}`
      );
    }

    const addWebinarApiRespJson = await addWebinarApiResp.json();
    this.webinarId = addWebinarApiRespJson.eventId;
    console.log(`Webinar ID - ${this.webinarId}`);
    this.webinarInfoDto = new WebinarInfoDto(
      addWebinarApiRespJson.eventId,
      addWebinarApiRespJson.title,
      isMagicLinkEnabled,
      isGuestModeEnabled,
      webinarPayLoadData.authTypes
    );

    console.log(`Webinar info dto is ${JSON.stringify(this.webinarInfoDto)}`);
    return this.webinarId;
  }

  async addNewSpeakerToWebinar(speakerEmailId: string, canSendEmail = false) {
    const payload = {
      accessGroupMappings: [],
      accessGroups: [],
      bio: "QA",
      canSendConfirmationMail: false,
      company: "Zuddl",
      designation: "QA",
      email: speakerEmailId,
      firstName: "automation",
      lastName: "parashar",
      organization: "Zuddl",
      picUrl: "",
      ticketTagType: "VIRTUAL",
    };

    payload.email = speakerEmailId;
    const addSpeakerResp = await this.orgApiRequestContext.post(
      `/api/speaker/${this.webinarId}?canSendMail=${canSendEmail}`,
      {
        data: payload,
      }
    );
    if (addSpeakerResp.status() != 200) {
      throw new Error(
        `Api call to add new speaker failed with ${addSpeakerResp.status()}`
      );
    }
    console.log(
      `API call to add speaker ${speakerEmailId} passed for event ${this.webinarId}`
    );
  }

  async disableOnboardingChecksForAttendee() {
    await new EventController(
      this.orgApiRequestContext,
      this.webinarId
    ).getAccessGroupController.handleOnboardingChecksForAttendee(false);
  }

  async endLiveSessionOnStudio() {
    await this.studioController.stopSessionOnStudio();
  }

  async stopVideoPlayBackOnStudio() {
    await this.studioController.stopVideoPlayBackOnStudio();
  }

  async stopFillerImageBroadcastOnStudio() {
    await this.studioController.stopFillerImageBroadcastOnStudio();
  }

  async stopBrandingVideoClipBroadcastOnStudio() {
    await this.studioController.stopBrandingVideoClipBroadcast();
  }
  async stopFillerVideoBroadcastOnStudio() {
    await this.studioController.stopFillerVideoBroadcastOnStudio();
  }

  async addNewAttendeeToWebinar(attendeeEmail: string) {
    await inviteAttendeeByAPI(
      this.orgApiRequestContext,
      this.webinarId,
      attendeeEmail,
      true
    );
  }

  async getWebinarLandingPageDetails() {
    const getLandingPageEndpoint = `/api/event/landing_page/${this.webinarId}`;
    const getLandingPageResp = await this.orgApiRequestContext.get(
      getLandingPageEndpoint
    );
    if (getLandingPageResp.status() != 200) {
      throw new Error(
        `API call to get webinar landing page id failed with ${getLandingPageResp.status()}`
      );
    }
    return getLandingPageResp;
  }

  // async getWebinarLandingPageDetails(webinarLandingPageId) {
  //   const getlandingPageDetailsEndpoint = `/api/event/landing_page/${this.webinarId}/${webinarLandingPageId}`;
  //   const landingPageDetailResp = await this.orgApiRequestContext.get(
  //     getlandingPageDetailsEndpoint
  //   );
  //   if (landingPageDetailResp.status() != 200) {
  //     throw new Error(
  //       `API call to get webinar landing page details for landing page ${webinarLandingPageId} failed with ${landingPageDetailResp.status()}`
  //     );
  //   }
  //   return landingPageDetailResp;
  // }
  async updateMagicLinkStatusForWebinar({
    enableSpeakerMagicLink = true,
    enableAttendeeMagicLink = true,
  }) {
    const landingPageDetailRespJson = await (
      await this.getWebinarLandingPageDetails()
    ).json();
    const landingPageId = landingPageDetailRespJson["eventLandingPageId"];
    landingPageDetailRespJson.speakerMagicLinkEnabled = enableSpeakerMagicLink;
    landingPageDetailRespJson.magicLinkEnabled = enableAttendeeMagicLink;
    const updateLandingPageResp = await this.orgApiRequestContext.patch(
      `/api/event/landing_page/${this.webinarId}/${landingPageId}`,
      {
        data: landingPageDetailRespJson,
      }
    );
    if (updateLandingPageResp.status() != 200) {
      throw new Error(
        `API call to update magic link status for webinar ${
          this.webinarId
        } failed with ${updateLandingPageResp.status()}`
      );
    }
    // verify that the magic link changes which you did are reflecting in the api call
    await expect(async () => {
      const updatedlandingPageDetailRespJson = await (
        await this.getWebinarLandingPageDetails()
      ).json();
      expect(updatedlandingPageDetailRespJson).toHaveProperty(
        "speakerMagicLinkEnabled",
        enableSpeakerMagicLink
      );
      expect(updatedlandingPageDetailRespJson).toHaveProperty(
        "magicLinkEnabled",
        enableAttendeeMagicLink
      );
    }, "expecting magic link changes to reflect in get event landing page api").toPass(
      { timeout: 5000 }
    );
  }

  async uploadPdfToWebinar() {
    // we are using default pdf file

    const pdfName = "test-data/upload-pdf-webinar/webinarpdf.pdf";
    const studioId = await this.getStudioIdForWebinar();
    const signedUrlEndpoint = `${
      DataUtl.getApplicationTestDataObj()["studioBaseUrl"]
    }/api/file-upload/signed_put`;
    const randomFileName = DataUtl.getRandomS3FileName();
    const signeds3urlresp = await this.orgApiRequestContext.post(
      signedUrlEndpoint,
      {
        data: randomFileName,
      }
    );

    if (signeds3urlresp.status() != 200) {
      throw new Error(
        `API call to get signed s3 url failed with ${signeds3urlresp.status()}`
      );
    }

    const signeds3urlrespJson = await signeds3urlresp.json();
    let preSignedUrl = signeds3urlrespJson["presignedPutUrl"];
    let renderUrl = signeds3urlrespJson["renderUrl"];
    console.log(
      `presigned url is ${preSignedUrl} and render url is ${renderUrl}`
    );
    const pdfFile = readFileSync(pdfName);
    if (pdfFile) {
      console.log(`pdf file is read`);
    }

    let uploadPdfToS3Endpoint = await this.orgApiRequestContext.put(
      preSignedUrl,
      {
        data: pdfFile,
      }
    );
    console.log(uploadPdfToS3Endpoint.status());

    // upload the pdf
    const addMediaEndpoint = `${
      DataUtl.getApplicationTestDataObj()["studioBaseUrl"]
    }/api/studio/media/add/studio/${studioId}`;

    const pdfMetaData = {
      totalPages: 4,
      thumbnail: `${renderUrl}?page=1`,
      type: "PDF_DOCUMENT",
      studioId: studioId,
    };
    const mediaPayloadToAddPdf = {
      name: "webinarpdf",
      url: renderUrl,
      metadata: JSON.stringify(pdfMetaData),
      type: "PDF_DOCUMENT",
      studioId: studioId,
    };
    const addMediaResp = await this.orgApiRequestContext.post(
      addMediaEndpoint,
      {
        data: mediaPayloadToAddPdf,
      }
    );
    if (addMediaResp.status() != 200) {
      throw new Error(
        `API call to add pdf failed with ${addMediaResp.status()}`
      );
    }
  }

  async getStudioIdForWebinar() {
    const listWebinarStageRes = await this.orgApiRequestContext.get(
      `/api/stage/${this.webinarId}/list`
    );
    if (listWebinarStageRes.status() != 200) {
      throw new Error(
        `API call to fetch list of stages for webinar failed with ${listWebinarStageRes.status()}`
      );
    }
    const listOfWebinarStageResJson = await listWebinarStageRes.json();
    const defaultStageObject = listOfWebinarStageResJson[0];
    const studioId = defaultStageObject["studioId"];
    console.log(
      `studio Id fetched for webinar ${this.webinarId} is ${studioId}`
    );
    if (!studioId) {
      throw new Error(
        `Studio id not found in list of stage response for webinar ${this.webinarId}`
      );
    }
    this.studioId = studioId;
    this.studioController = new StudioController(
      this.orgApiRequestContext,
      this.studioId
    );

    return studioId;
  }

  async updateWebinarStartAndEndTime({
    deltaByDayInStartDate,
    deltaByDayInEndDate,
    deltaByHoursInStartTime,
    deltaByHoursInEndTime,
    deltaByMinuteInStartTime,
    deltaByMinuteInEndTime,
    deltaBySecondsInStartTime,
    deltaBySecondsInEndTime,
    eventTimeZone = "Asia/Kolkata",
  }: UpdateEventStartAndEndDateTime) {
    let eventController = new EventController(
      this.orgApiRequestContext,
      this.webinarId
    );
    await eventController.changeEventStartAndEndDateTime({
      deltaByDayInStartDate,
      deltaByDayInEndDate,
      deltaByHoursInStartTime,
      deltaByHoursInEndTime,
      deltaByMinuteInStartTime,
      deltaByMinuteInEndTime,
      deltaBySecondsInStartTime,
      deltaBySecondsInEndTime,
      eventTimeZone,
    });
  }
}
