import { APIRequestContext } from "@playwright/test";
import {
  CustomFieldMappingPayload,
  HubspotEnableIntegrationPayload,
  IntegrationControllerInterface,
  salesforceFieldMappingPaylod,
} from "../interfaces/IntegrationControllerInterface";
import integrationType from "../enums/integrationTYpeEnum";
import { DataUtl } from "../util/dataUtil";

export class HubspotIntegrationController
  implements IntegrationControllerInterface
{
  readonly eventId;
  readonly orgApiRequestContext: APIRequestContext;

  constructor(orgApiRequestContext, eventId) {
    this.eventId = eventId;
    this.orgApiRequestContext = orgApiRequestContext;
  }

  async enableHubspotIntegrationForThisEvent(
    hubspotPayload: HubspotEnableIntegrationPayload
  ) {
    const updateSettingResp = await this.orgApiRequestContext.post(
      `/api/integration/${this.eventId}/update-integration-setting`,
      {
        data: hubspotPayload,
      }
    );
    if (updateSettingResp.status() != 200) {
      throw new Error(
        `API call to update hubspot integration setting for event Id ${
          this.eventId
        } failed with ${updateSettingResp.status()}`
      );
    }
  }

  async syncExistingRecords(integrationType: integrationType) {
    const syncExistingRecordApiResp2 = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/api/integration/${this.eventId}/sync-existing-detail/${integrationType}`;
    const syncExistingRecordApiResp = await this.orgApiRequestContext.post(
      syncExistingRecordApiResp2
    );

    if (syncExistingRecordApiResp.status() != 200) {
      throw new Error(
        `API call to trigger existing records sync failed with ${syncExistingRecordApiResp.status()}`
      );
    }
  }

  async getHubspotRegisterUserDataFromTray(emailId: string): Promise<JSON> {
    const getRegisterUserDataFromTrayResp =
      await this.orgApiRequestContext.post(
        `https://47195630-8f1b-45f0-98ec-7bd20b42c071.trayapp.io`,
        {
          data: {
            email: emailId,
          },
        }
      );
    if (getRegisterUserDataFromTrayResp.status() != 200) {
      throw new Error(
        `API call to fetch registered user data failed with ${getRegisterUserDataFromTrayResp.status()}`
      );
    }
    //fetching the result object from the response object
    const registerUserDataFromTrayRespJson =
      await getRegisterUserDataFromTrayResp.json();
    return registerUserDataFromTrayRespJson["result"];
  }

  async mapZuddlFieldsWithHubspotFields(
    {
      customFieldMapping,
      customPolicyMapping,
      secondaryCustomFieldMapping,
      secondaryCustomPolicyMapping,
      tertiaryCustomFieldMapping,
      tertiaryCustomPolicyMapping,
    }: salesforceFieldMappingPaylod,
    integrationName: integrationType
  ) {
    const updateIntegrationMappingResp2 = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/api/core/integration/${integrationName}/mapping/fields`;
    const updateIntegrationMappingResp = await this.orgApiRequestContext.post(
      updateIntegrationMappingResp2,
      {
        data: {
          eventId: this.eventId,
          customFieldMapping: customFieldMapping,
          customPolicyMapping: customPolicyMapping,
          secondaryCustomFieldMapping: secondaryCustomFieldMapping,
          secondaryCustomPolicyMapping: secondaryCustomPolicyMapping,
          tertiaryCustomFieldMapping: tertiaryCustomFieldMapping,
          tertiaryCustomPolicyMapping: tertiaryCustomPolicyMapping,
        },
      }
    );
    if (updateIntegrationMappingResp.status() != 200) {
      throw new Error(
        `API call to update integration mapping failed with status ${updateIntegrationMappingResp.status()}`
      );
    }
  }

  async getAllFieldMappings(integrationType: integrationType) {
    const getMappingResponse = await this.orgApiRequestContext.get(
      `api/core/integration/${integrationType}/mapping/fields?eventId=${this.eventId}`
    );

    if (getMappingResponse.status() != 200) {
      throw new Error(
        `API call to fetch custom field apping ${
          this.eventId
        } failed with ${getMappingResponse.status()}`
      );
    }
    return await getMappingResponse.json();
  }
  async verifyFieldPresentInCustomFieldMapping(
    fieldName: string,
    integrationType: integrationType
  ): Promise<boolean> {
    let response = await this.getAllFieldMappings(integrationType);
    let customMappingData = response["customFieldMapping"];
    // Fetch all properties from customFieldMapping object
    const allProperties: string[] = [];
    for (const key in customMappingData) {
      allProperties.push(key);
    }
    // Verify if 'fieldName' key exists
    const fieldNameExists = allProperties.includes(fieldName);
    return fieldNameExists;
  }
}
