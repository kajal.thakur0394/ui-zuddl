import { APIRequestContext, APIResponse, Page, test } from "@playwright/test";
import { DataUtl } from "../util/dataUtil";
import { AddMemberInOrganisation, CreateOrganisationParams, CreateTeamParams, OrganisationControllerInterface, UpdateOrganisationPlanParams } from "../interfaces/OrganisationControllerInterface";
import { MemberRolesEnum } from "../enums/MemberRolesEnum";


export class OrganisationController implements OrganisationControllerInterface {
  readonly apiRequestContext: APIRequestContext;

  constructor(apiRequestContext: APIRequestContext) {
    this.apiRequestContext = apiRequestContext;
  }

  async createNewOrganization(
    createOrgParams: CreateOrganisationParams): Promise<APIResponse> {
    const createOrgEndpoint = `${DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
      }/api/core/organization/re-tool/create`;
    const retoolToken = process.env.RETOOL_TOKEN;
    if (retoolToken == null || retoolToken == undefined) {
      throw new Error(
        `Retool token not found in env variable with name RETOOL_TOKEN`
      );
    }
    const headers = {
      "retool-token": retoolToken,
    };
    const createNewOrgApiResp = await this.apiRequestContext.post(
      createOrgEndpoint,
      {
        data: createOrgParams,
        headers: headers,
      }
    );

    if (createNewOrgApiResp.status() != 200) {
      throw new Error(
        `API call to create new organisation failed with ${createNewOrgApiResp.status()}`
      );
    }
    return createNewOrgApiResp;
  }

  async renewPlanInOrganization(updateOrganisationPlanParams: UpdateOrganisationPlanParams): Promise<APIResponse> {
    const updateOrgEndpoint = `${DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
      }/api/core/organization/re-tool/renew`;
    const retoolToken = process.env.RETOOL_TOKEN;
    if (retoolToken == null || retoolToken == undefined) {
      throw new Error(
        `Retool token not found in env variable with name RETOOL_TOKEN`
      );
    }
    const headers = {
      "retool-token": retoolToken,
    };
    const updateOrgApiResp = await this.apiRequestContext.post(
      updateOrgEndpoint,
      {
        data: updateOrganisationPlanParams,
        headers: headers,
      }
    );

    if (updateOrgApiResp.status() != 200) {
      throw new Error(
        `API call to update organisation plan failed with ${updateOrgApiResp.status()}`
      );
    }
    return updateOrgApiResp;
  }

  async addNewMemberToOrganisation(
    memberDetailsObject: AddMemberInOrganisation
  ): Promise<APIResponse> {
    let addMemberResp: any;
    await test.step(`Adding new member with email ${memberDetailsObject.email} to organisation`, async () => {
      addMemberResp = await this.apiRequestContext.post(
        "api/core/organization/add-member",
        {
          data: memberDetailsObject,
        }
      );
      if (addMemberResp.status() != 200) {
        throw new Error(
          `API to add new member to events team failed with ${addMemberResp.status()}`
        );
      }

    });
    return addMemberResp;
  }

  async addMultipleMembersToOrganisation(memberDetailsObject: AddMemberInOrganisation, noOfUsers: number, isOrganiser: boolean) {
    let email: string;
    let productRole: MemberRolesEnum

    for (let i = 0; i < noOfUsers; i++) {
      if (isOrganiser) {
        email = DataUtl.getRandomOrganiserEmail();
        productRole = MemberRolesEnum.ORGANIZER
      } else {
        email = DataUtl.getRandomModeratorEmail();
        productRole = MemberRolesEnum.MODERATOR
      }
      await this.addNewMemberToOrganisation({
        firstName: memberDetailsObject.firstName + i,
        lastName: memberDetailsObject.lastName + i,
        email: email,
        productRole: productRole,
        myTeamList: memberDetailsObject.myTeamList
      });
      console.log(`successfully added member ${email} having product role as ${productRole}`);
    }
  }

  async createATeam(createTeamParams: CreateTeamParams): Promise<void> {
    const createTeamResponse = await this.apiRequestContext.post(
      `api/core/organization/team/add`,
      {
        data: createTeamParams,
      }
    );

    if (createTeamResponse.status() != 200) {
      throw new Error(
        `API to add new Team failed with ${createTeamResponse.status()}`
      );
    }
  }

  async deleteMemberFromOrganisation(orgMemberId: string): Promise<void> {
    const deleteMemberResponse = await this.apiRequestContext.delete(`/api/core/organization/member/${orgMemberId}`);
    if (deleteMemberResponse.status() != 200) {
      throw new Error(
        `API to delete member from organisation failed with ${deleteMemberResponse.status()}`
      );
    }
  }

  async isMemberPartOfThisOrganisation(memberEmail: string): Promise<boolean> {
    const getAllTeamsAssignedToMemberResp = await this.apiRequestContext.get(
      `/api/core/organization/member`
    );
    if (getAllTeamsAssignedToMemberResp.status() != 200) {
      throw new Error(
        `API call to get all teams list assigned to a member failed with status ${getAllTeamsAssignedToMemberResp.status()}`
      );
    }
    const getAllTeamsAssignedToMemberRespJson = await getAllTeamsAssignedToMemberResp.json();
    let memberList = getAllTeamsAssignedToMemberRespJson["memberDtoList"];
    for (const member of memberList) {
      if (member.email == memberEmail) {
        return true;
      }
    }
    return false;
  }

  async fetchAccountIdOfMemberInOrganization(memberEmail: string) {
    const getAllTeamsAssignedToMemberResp = await this.apiRequestContext.get(
      `/api/core/organization/member`
    );
    if (getAllTeamsAssignedToMemberResp.status() != 200) {
      throw new Error(
        `API call to get all teams list assigned to a member failed with status ${getAllTeamsAssignedToMemberResp.status()}`
      );
    }
    const getAllTeamsAssignedToMemberRespJson = await getAllTeamsAssignedToMemberResp.json();
    let memberList = getAllTeamsAssignedToMemberRespJson["memberDtoList"];
    for (const member of memberList) {
      if (member.email == memberEmail) {
        return member["accountId"];
      }
    }

  }

}


