import { APIRequestContext, expect } from "@playwright/test";

export class DefaultAccessGroupController {
  private apiRequestContext: APIRequestContext;
  private eventId;

  constructor(apiRequestContext: APIRequestContext, eventId) {
    this.apiRequestContext = apiRequestContext;
    this.eventId = eventId;
  }
}
