import { APIRequestContext, expect } from "@playwright/test";
import { Features } from "../../enums/AccessControlEnum";
import { inspect } from "util";
import { StageController } from "../StageController";
import { RoomsController } from "../RoomsController";
import { json } from "stream/consumers";

export class AccessControlController {
  private apiRequestContext: APIRequestContext;
  private eventId;
  private groupId;
  private stageController: StageController;
  private roomsController: RoomsController;

  constructor(apiRequestContext: APIRequestContext, eventId, groupId) {
    this.apiRequestContext = apiRequestContext;
    this.eventId = eventId;
    this.groupId = groupId;
    this.stageController = new StageController(apiRequestContext, eventId);
    this.roomsController = new RoomsController(apiRequestContext, eventId);
  }

  get getStageController() {
    return this.stageController;
  }

  get getRoomsController() {
    return this.roomsController;
  }

  private async getUserId(userName) {
    const getUserIdUri = `/api/access_groups/${this.eventId}/groups`;
    let userGroupsResponse = await this.apiRequestContext.get(getUserIdUri);

    if (!userGroupsResponse.ok()) {
      throw new Error(
        `API to get user group details failed with response -> ${userGroupsResponse.status()}`
      );
    }

    const userGroupList = await userGroupsResponse.json();
    let userId;
    for (const userGroup of userGroupList) {
      if (userName === userGroup.groupName) {
        userId = userGroup.groupId;
        break;
      }
    }

    if (userId) {
      return userId;
    } else {
      throw new Error(`User -> ${userName} not found.`);
    }
  }

  private async getFeaturesJson() {
    const getFeaturesUri = `/api/access_groups/${this.eventId}/${this.groupId}`;
    let response = await this.apiRequestContext.get(getFeaturesUri);

    if (!response.ok()) {
      throw new Error(
        `API to get list of features failed with response -> ${response.status()}`
      );
    }

    const responseJson = await response.json();
    const featureList = responseJson["features"];
    return featureList;
  }

  private async getFeatureId(featureName: Features) {
    const featuresList = await this.getFeaturesJson();

    let featureId;
    for (const feature of featuresList) {
      if (feature["featureName"] === featureName) {
        featureId = feature["id"];
      }
    }

    if (featureId) {
      return featureId;
    } else {
      throw new Error("Feature not found");
    }
  }

  async fetchAccessControlSettings(userName) {
    const userId = await this.getUserId(userName);
    const getSettingsUri = `/api/access_groups/${this.eventId}/${userId}`;

    let getAccessControlSettingsResponse = await this.apiRequestContext.get(
      getSettingsUri
    );

    if (!getAccessControlSettingsResponse.ok()) {
      throw new Error(
        `API call to fetch the control settings failed with error -> ${getAccessControlSettingsResponse.status()}`
      );
    }
  }

  private async getSubfeaturesJson(featureName: Features) {
    const featuresList = await this.getFeaturesJson();

    let subFeatureJson;
    for (const feature of featuresList) {
      if (feature["featureName"] === featureName) {
        subFeatureJson = feature["subFeatures"];
      }
    }

    console.log(
      `${featureName} Subfeatures json ...... `,
      inspect(subFeatureJson, { depth: null })
    );

    return subFeatureJson;
  }

  private async getDayId(dayNumber: number) {
    const featuresList = await this.getFeaturesJson();
    let daysList = await this.getSubfeaturesJson(Features.DAY);

    let dayId;
    if (daysList.length < dayNumber || dayNumber <= 0) {
      throw new Error("Day is outside the event timeline.");
    } else {
      let day = daysList[dayNumber - 1];
      dayId = day["id"];
    }

    return dayId;
  }

  private async getSubStageId(stageName: string) {
    const featuresList = await this.getFeaturesJson();

    let stageList = await this.getSubfeaturesJson(Features.STAGE);

    let daySubFeatureId;

    for (const stage of stageList) {
      if (
        stage["dynSubFeatureName"].toLowerCase() === stageName.toLowerCase()
      ) {
        daySubFeatureId = stage["id"];
      }
    }

    return daySubFeatureId;
  }

  private async getRoomId(roomType: string) {
    const featuresList = await this.getFeaturesJson();

    let roomId;
    if (roomType === "ROOMS") {
      roomId = await this.getFeatureId(Features.ROOM);
    } else {
      let roomSubFeatureJson = await this.getSubfeaturesJson(Features.ROOM);

      for (const stage of roomSubFeatureJson) {
        if (stage["subFeatureName"].toLowerCase() === roomType.toLowerCase()) {
          roomId = stage["id"];
        }
      }
    }

    return roomId;
  }

  async toggleDayAccess(dayNumber: number, toggle: boolean) {
    const dayId = await this.getDayId(dayNumber);

    const dayToggleUri = `/api/access_groups/${this.eventId}/day_access/${this.groupId}`;
    const response = await this.apiRequestContext.patch(dayToggleUri, {
      data: [
        {
          id: dayId,
          isActive: toggle,
        },
      ],
    });

    if (!response.ok()) {
      throw new Error(
        `API to toggle day access failed with response -> ${response.status()}`
      );
    }
  }

  async toggleAccessForFeature(featureName: Features, toggle: boolean) {
    const featureId = await this.getFeatureId(featureName);

    const toggleFeatureUri = `/api/access_groups/${this.eventId}/${this.groupId}`;
    const response = await this.apiRequestContext.patch(toggleFeatureUri, {
      data: [
        {
          id: featureId,
          isActive: toggle,
          isStageBreakoutRoomActive: null,
        },
      ],
    });

    if (!response.ok()) {
      throw new Error(
        `API to toggle feature access failed with response -> ${response.status()}`
      );
    }
  }

  async toggleStageAccess(stageName: string, toggle: boolean) {
    const subStageId = await this.getSubStageId(stageName);

    const stageToggleUri = `/api/access_groups/${this.eventId}/${this.groupId}`;
    const jsonPayload = {
      id: subStageId,
      isActive: toggle,
      isStageBreakoutRoomActive: null,
    };
    console.log(
      "Json payload to toggle stage access -> ",
      inspect(jsonPayload, { depth: null })
    );
    const response = await this.apiRequestContext.patch(stageToggleUri, {
      data: [jsonPayload],
    });

    if (!response.ok()) {
      throw new Error(
        `API to toggle Stage Access failed with -> ${response.status()}`
      );
    }

    console.log(`API response ->`, inspect(response, { depth: null }));
  }

  async toggleRoomAccess(
    roomType: "ROOM" | "ACCESS_PVT_ROOMS" | "SHOW_RESERVED_ROOMS_ONLY",
    toggle: boolean
  ) {
    let roomId = await this.getRoomId(roomType);
    const roomToggleUri = `/api/access_groups/${this.eventId}/${this.groupId}`;
    const response = await this.apiRequestContext.patch(roomToggleUri, {
      data: [
        {
          id: roomId,
          isActive: toggle,
          isStageBreakoutRoomActive: null,
        },
      ],
    });

    console.log(`API response ->`, inspect(response, { depth: null }));
    if (!response.ok()) {
      throw new Error(
        `API to toggle Room Access failed with -> ${response.status()}`
      );
    }
  }

  async toggleDefaultStageAccess(stageName: string, toggle: boolean) {
    const subStageId = await this.getSubStageId(stageName);
    const stageId = await this.getFeatureId(Features.STAGE);

    const stageToggleUri = `/api/access_groups/${this.eventId}/${this.groupId}`;
    const response = await this.apiRequestContext.patch(stageToggleUri, {
      data: [
        {
          id: stageId,
          isActive: toggle,
          isStageBreakoutRoomActive: null,
        },
        {
          id: subStageId,
          isActive: toggle,
        },
      ],
    });

    console.log(`API response ->`, inspect(response, { depth: null }));
    if (!response.ok()) {
      throw new Error(
        `API to toggle Stage Access failed with -> ${response.status()}`
      );
    }
  }

  async toggleEventEngagement(
    featureName: Features,
    stageName: string,
    toggle: boolean,
    enableStageBreakout: true
  ) {
    const featureId = await this.getFeatureId(featureName);
    const stageId = await this.getSubStageId(stageName);

    const stageToggleUri = `/api/access_groups/${this.eventId}/${featureId}`;
    const response = await this.apiRequestContext.patch(stageToggleUri, {
      data: [
        {
          id: stageId,
          isActive: toggle,
          isStageBreakoutRoomActive: enableStageBreakout,
        },
      ],
    });

    if (!response.ok()) {
      throw new Error(
        `API to toggle event engagement failed with -> ${response.status()}`
      );
    }
  }
}
