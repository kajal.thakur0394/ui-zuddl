import { APIRequestContext } from "@playwright/test";
import {
  FilterLogic,
  FilterOperators,
  Filters,
  FiltersUserInput,
} from "../../enums/AccessGroupEnum";
import { inspect } from "util";
import { json } from "stream/consumers";

export class CustomAccessGroupController {
  private apiRequestContext: APIRequestContext;
  private eventId;

  constructor(orgApiRquestContext: APIRequestContext, eventId) {
    this.apiRequestContext = orgApiRquestContext;
    this.eventId = eventId;
  }

  private async fetchFilters() {
    let getFiltersUri = `/api/access_groups/${this.eventId}/filter/fields`;
    let filtersApiResponse = await this.apiRequestContext.get(getFiltersUri);

    if (!filtersApiResponse.ok()) {
      throw new Error(
        `API to fetch list of filters failed with ${filtersApiResponse.status()}`
      );
    }
    const filtersJson = await filtersApiResponse.json();
    // console.log(
    //   `List of filters ....... \n`,
    //   inspect(filtersJson, { depth: null }),
    //   "\n"
    // );

    const labelNameToPayloadMap = new Map<string, FilterItem>();
    filtersJson.forEach(async (item) => {
      // console.log(inspect(item, { depth: null }), "\n");
      labelNameToPayloadMap.set(await item.labelName, item);
    });

    return labelNameToPayloadMap;
  }

  private async createGroup(createGroupPayload) {
    let createGroupUri = `/api/audience/${this.eventId}/custom/group`;

    const createGroupResponse = await this.apiRequestContext.post(
      createGroupUri,
      {
        data: createGroupPayload,
      }
    );

    if (!createGroupResponse.ok()) {
      throw new Error(
        `API to create new group failed with ${createGroupResponse.status()}`
      );
    }

    console.log(
      "Create Group Response body",
      inspect(await createGroupResponse.json(), { depth: null }),
      "\n"
    );

    return createGroupResponse;
  }

  async createCustomGroup(
    groupName: string,
    groupType: "STATIC" | "ACTIVE",
    accessControl: boolean,
    filters: Array<FiltersUserInput>,
    filterLogic: Array<FilterLogicPair>,
    accessGroupFiltersLogic: FilterLogic
  ) {
    const draftGroupPayload = {
      groupName: groupName,
      isAccessControlEnabled: accessControl,
      groupUpdateType: groupType,
      isActive: false,
      status: "DRAFT",
    };
    let draftGroupResponse = await this.createGroup(draftGroupPayload);
    const draftGroupId = (await draftGroupResponse.json())["audienceGroupId"];
    console.log("Draft group created || Group ID ->", draftGroupId);

    const filtersPayload = await this.createFiltersPayload(
      filters,
      filterLogic
    );

    const createGroupPayload = {
      isActive: true,
      audienceGroupId: draftGroupId,
      groupUpdateType: groupType,
      filterMapping: {
        operationType: accessGroupFiltersLogic,
        filterConditionsDTOList: filtersPayload,
        isPassAudienceGroupId: false,
      },
      isAccessControlEnabled: accessControl,
      groupName: groupName,
      status: "PUBLISHED",
    };
    console.log(
      "############# Create Custom Group Payload ###############\n",
      inspect(createGroupPayload, { depth: null }),
      "\n"
    );
    const response = await this.createGroup(createGroupPayload);
    const groupId = (await response.json())["audienceGroupId"];
    console.log("Group id -> ", groupId);
    return groupId;
  }

  private async createFiltersPayload(
    filtersToApply: Array<FiltersUserInput>,
    filterLogic: Array<FilterLogicPair>
  ) {
    let filtersMap: Map<string, FilterItem> = await this.fetchFilters();
    console.log("Filters Json:");
    console.log("------------------------");
    filtersMap.forEach((value, key) => {
      console.log(`Key: ${key}, Value: `, value);
    });

    let filtersPayload = [];
    let filterJsonPayloadMap: Map<Filters, Array<JSON>> = new Map<
      Filters,
      Array<JSON>
    >();

    console.log("Filters to apply:");
    console.log("------------------------");
    for (const filter of filtersToApply) {
      const filterName = filter.filterName;
      var compareValue = filter.compareValue;
      var filterOperator = filter.filterOperator;
      console.log("Filter Name:", filterName);
      console.log("Compare Value:", compareValue);
      console.log("Filter Operator:", filterOperator);
      console.log("------------------------");

      var filterId;
      var fieldName;

      if (filtersMap.has(filterName.toString())) {
        const filterJson = filtersMap.get(filterName.toString());
        if (filterJson) {
          filterId = filterJson.filterFieldId;
          fieldName = filterJson.fieldName;
          console.log("Filter ID:", filterId);
          console.log("Field Name:", fieldName);
          console.log("------------------------");
        } else {
          console.log(
            "FilterJson is null...",
            inspect(filterJson, { depth: null })
          );
        }
      }

      let filterJsonPayload;

      if (
        filterName === Filters.REGISTRATION_DATE_AND_TIME ||
        filterName === Filters.IN_PERSON_CHECK_IN_TIME
      ) {
        if (typeof compareValue === "string") {
          filterJsonPayload = {
            filterFieldId: filterId,
            compareValue: compareValue,
            operator: filterOperator,
            fieldName: fieldName,
            filterType: "STANDARD",
            type: "DATE",
          };
        } else {
          throw new Error("Compare value is invalid");
        }
      } else if (filterName === Filters.STATUS) {
        if (
          filterOperator === FilterOperators.TRUE ||
          filterOperator === FilterOperators.FALSE
        ) {
          filterJsonPayload = {
            filterFieldId: filterId,
            compareValue: "None",
            operator: filterOperator,
            fieldName: fieldName,
            filterType: "STANDARD",
            type: "BOOLEAN",
          };
        } else {
          throw new Error("Operator is invalid.");
        }
      } else if (filterName === Filters.DEVICE_TYPE) {
        if (typeof compareValue === "object") {
          filterJsonPayload = {
            filterFieldId: filterId,
            compareValue: compareValue,
            operator: FilterOperators.ANY_OF,
            fieldName: fieldName,
            filterType: "STANDARD",
            type: "LIST",
          };
        }
      } else {
        filterJsonPayload = {
          filterFieldId: filterId,
          compareValue: compareValue,
          operator: filterOperator,
          fieldName: fieldName,
          filterType: "STANDARD",
          type: "STRING",
        };
      }

      console.log(
        `Payload for ${fieldName} is ....`,
        inspect(filterJsonPayload, { depth: null }),
        "\n"
      );
      console.log(".........................");
      if (filterJsonPayload !== undefined) {
        if (filterJsonPayloadMap.has(filterName)) {
          const existingData = filterJsonPayloadMap.get(filterName);
          if (existingData) {
            existingData.push(filterJsonPayload);
          }
        } else {
          const filterJsonArray: JSON[] = [];
          filterJsonArray.push(filterJsonPayload);
          filterJsonPayloadMap.set(filterName, filterJsonArray);
        }
      } else {
        throw new Error("Payload is undefined.");
      }
    }

    console.log("Filter Payload Json Map ...", filterJsonPayloadMap);

    for (const [filter, filterJson] of filterJsonPayloadMap.entries()) {
      console.log(`Filter: ${filter}`);
      console.log("JSON Array:", inspect(filterJson, { depth: null }));

      let conditionOperator: FilterLogic;

      const index = filterLogic.findIndex(
        (item) => Object.keys(item)[0] === filter
      );
      if (index !== -1) {
        conditionOperator = FilterLogic.AND;
      } else {
        conditionOperator = filterLogic[filter];
      }

      let conditionJson = {
        conditionOperator: conditionOperator,
        conditions: filterJson,
      };
      filtersPayload.push(conditionJson);
    }

    return filtersPayload;
  }

  async fetchAudienceList(groupId, filterLogic: FilterLogic) {
    let audienceList = [];

    let getAudienceListEndpoint = `/api/audience/custom/${this.eventId}/list/filter?offset=0&audienceGroupId=${groupId}`;

    const response = await this.apiRequestContext.post(
      getAudienceListEndpoint,
      {
        data: {
          operationType: filterLogic,
          filterConditionsDTOList: null,
          isPassAudienceGroupId: true,
        },
      }
    );

    if (!response.ok()) {
      throw new Error(
        `API to fetch attendee list failed with ${response.status()}`
      );
    }

    const responseJson = await response.json();
    console.log("Response ->", inspect(responseJson, { depth: null }));

    console.log("Emails -> \n");
    responseJson.forEach(async (userEmailJson) => {
      const email = await userEmailJson["email"];
      console.log(inspect(userEmailJson, { depth: null }), "\n");
      audienceList.push(email);
    });

    return audienceList;
  }
}
interface FilterItem {
  filterFieldId: string;
  fieldName: string;
  labelName: string;
  type: string;
  subType: string;
  filterGroupType: string;
  filterType: string;
  predefinedValues: any[] | null;
}

export interface FilterLogicPair {
  key: Filters;
  value: FilterLogic;
}
