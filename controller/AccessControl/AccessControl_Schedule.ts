import { APIRequestContext } from "@playwright/test";
import { json } from "stream/consumers";
import { inspect } from "util";

export class ScheduleAccessControl {
  private apiRequestContext: APIRequestContext;
  private eventId;
  private groupId;

  constructor(apiRequestContext: APIRequestContext, eventId, groupId) {
    this.apiRequestContext = apiRequestContext;
    this.eventId = eventId;
    this.groupId = groupId;
  }

  // async toggleSessionAccess(sessionId, toggle) {
  //   const sessionControlUri = `/api/access_groups/${this.eventId}/${this.groupId}`;
  //   let response = await this.apiRequestContext.patch(sessionControlUri, {
  //     data: [{ id: sessionId, isActive: toggle }],
  //   });

  //   if (!response.ok()) {
  //     throw new Error(
  //       `API to update session control failed with -> ${response.status()}`
  //     );
  //   }
  // }

  async fetchSessions() {
    const fetchSessionDetailsUri = `/api/segment/${this.eventId}/${this.groupId}/list/search?q=`;

    let response = await this.apiRequestContext.post(fetchSessionDetailsUri, {
      data: {},
    });

    if (!response.ok()) {
      throw new Error(
        `API to fetch session details for user failed with -> ${response.status()}`
      );
    }

    let jsonResponse = await response.json();
    console.log("Session details -> ", inspect(jsonResponse, { depth: null }));
    return jsonResponse;
  }

  private async getSessionGroupMappingId(sessionId: string | number) {
    let responseJsonString = await this.fetchSessions();

    let sessionGroupId;
    for (var i = 0; i < responseJsonString.length; i++) {
      if (sessionId === responseJsonString[i]["segmentId"]) {
        sessionGroupId = responseJsonString[i]["accessGroupMappingId"];
      }
    }

    console.log(
      `Accessgroup Mapping id for Segment -> ${sessionId} is ${sessionGroupId}`
    );

    return sessionGroupId;
  }

  async toggleSessionAccess(sessionId: string | number, toggle: boolean) {
    const sessionAccessUri = `/api/access_groups/${this.eventId}/${this.groupId}`;

    const sessionGroupId = await this.getSessionGroupMappingId(sessionId);

    let response = await this.apiRequestContext.patch(sessionAccessUri, {
      data: [{ id: sessionGroupId, isActive: toggle }],
    });

    if (!response.ok()) {
      throw new Error(
        `API to toggle session access failed with -> ${response.status()}`
      );
    }
  }
}
