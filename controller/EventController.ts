import { APIRequestContext, APIResponse, expect, test } from "@playwright/test";
import EmailRestrictionType from "../enums/emailRestrictionTypeEnum";
import RestrictionEmailDomainType from "../enums/restrictedEmailDomainType";
import * as path from "path";
const csvWriter = require("csv-writer");

import {
  createNewEvent,
  inviteAttendeeByAPI,
  inviteSpeakerByApi,
  updateEmailRestrictionSettings,
  getStartDateFromToday,
  getEndDateFromToday,
  eventLandingPageOptions,
  cancelEvent,
} from "../util/apiUtil";
import { DataUtl } from "../util/dataUtil";
import { formatdateString } from "../util/commonUtil";
import { AccessGroupController } from "./AccessGroupController";
import { RegistrationFormController } from "./RegistrationFormController";
import { RoomsController } from "./RoomsController";
import createEventPayloadData from "../util/create_event_payload.json";
import { SpeakerData } from "../test-data/speakerData";
import { ExpoController } from "./ExpoController";
import { ScheduleController } from "./ScheduleController";
import EventEntryType from "../enums/eventEntryEnum";
import { AnnouncementController } from "./AnnouncementController";
import { StageController } from "./StageController";
import StageSize from "../enums/StageSizeEnum";
import sessionDataPayload from "../test-data/sessionDataPayload.json";
import { deprecate, inspect } from "util";
import BrowserFactory from "playwright-qa/util/BrowserFactory";

export class CustomFieldObject {
  registrationCustomFieldId;
  fieldName: string;
  fieldType;
  localFieldType;
  dataType;
  labelName;
  dropdownValues;
  isRequired;
  eventId: string;
  choiceTypeJson;
  isPredefinedField: boolean;
  uniqueId: string;
  customFieldConditions: any;
  orderIdx: number;
  eventLandingPageId: string;
}

export class EventController {
  protected orgApiRequest: APIRequestContext;
  protected registrationFormController: RegistrationFormController;
  protected lobbyController;
  protected accessGroupController: AccessGroupController;
  protected eventId;
  protected roomsController: RoomsController;
  protected expoController: ExpoController;
  protected scheduleController: ScheduleController;
  protected announcementController: AnnouncementController;
  protected stageController: StageController;

  constructor(orgApiRequestContext: APIRequestContext, eventId) {
    this.orgApiRequest = orgApiRequestContext;
    this.eventId = eventId;
    this.registrationFormController = new RegistrationFormController(
      orgApiRequestContext,
      eventId
    );
    this.accessGroupController = new AccessGroupController(
      orgApiRequestContext,
      eventId
    );
    this.roomsController = new RoomsController(orgApiRequestContext, eventId);
    this.scheduleController = new ScheduleController(
      orgApiRequestContext,
      eventId
    );
    this.expoController = new ExpoController(orgApiRequestContext, eventId);
    this.announcementController = new AnnouncementController(
      orgApiRequestContext,
      eventId
    );
    this.stageController = new StageController(
      orgApiRequestContext,
      eventId,
      this.getDefaultStageId()
    );
  }

  get getRegistrationFormController() {
    return this.registrationFormController;
  }

  get getAccessGroupController() {
    return this.accessGroupController;
  }

  get getAnnouncementController() {
    return this.announcementController;
  }
  get getRoomsController() {
    return this.roomsController;
  }
  get getExpoController() {
    return this.expoController;
  }

  get getScheduleController() {
    return this.scheduleController;
  }

  async disableOnboardingChecksForAttendee() {
    await this.accessGroupController.handleOnboardingChecksForAttendee(false);
  }

  async disableOnboardingChecksForSpeaker() {
    await this.accessGroupController.handleOnboardingChecksForSpeaker(false);
  }

  public static async generateNewEvent(
    orgApiRequest: APIRequestContext,
    {
      event_title,
      eventDescription = "This is a test event",
      teamId = null,
      default_settings = true, // start_date = cur date and end date = cur date + 3 days
      sameDayEvent = false,
      add_start_date_from_today = 1, // can be in + to show future date and - to create past events
      add_end_date_from_today = 3, // can be in + to show future date and - to create past events
      timezone = "Asia/Kolkata",
      add_hours_to_end_date = 0,
      add_hours_to_start_date = 0,
      addMinutesToEndDate = 0,
      addMinutesToSatrtDate = 0,
      addSecondsToEndDate = 0,
      addSecondsToStartDate = 0,
      isMagicLinkEnabledAttendee = true,
      isMagicLinkEnabledSpeaker = true,
      isPasswordEnabledAttendee = false,
      isPasswordEnabledSpeaker = false,
    }
  ): Promise<number> {
    let event_id;
    await test.step(`creating a new event with title ${event_title}`, async () => {
      event_id = await createNewEvent({
        api_request_context: orgApiRequest,
        event_title: event_title,
        eventDescription: eventDescription,
        teamId: teamId,
        default_settings: default_settings,
        sameDayEvent: sameDayEvent,
        add_start_date_from_today: add_start_date_from_today,
        timezone: timezone,
        add_hours_to_end_date: add_hours_to_end_date,
        add_hours_to_start_date: add_hours_to_start_date,
        addMinutesToEndDate: addMinutesToEndDate,
        addMinutesToSatrtDate: addMinutesToSatrtDate,
        addSecondsToEndDate: addSecondsToEndDate,
        addSecondsToStartDate: addSecondsToStartDate,
        isMagicLinkEnabledAttendee: isMagicLinkEnabledAttendee,
        isMagicLinkEnabledSpeaker: isMagicLinkEnabledSpeaker,
        isPasswordEnabledAttendee: isPasswordEnabledAttendee,
        isPasswordEnabledSpeaker: isPasswordEnabledSpeaker,
      });
    });
    console.log(event_id);
    return event_id;
  }

  async cancelAnEvent(eventId = this.eventId) {
    await cancelEvent(this.orgApiRequest, eventId);
  }

  async getInformationTabView() {
    console.log("####### getting information tab response #######");
    let landingPageId = await this.getEventLandingPageId();
    console.log(`####### landing page id ${landingPageId} #######`);
    console.log(`####### event id ${this.eventId} #######`);
    let informationTabEndpoint = `/api/event/landing_page/${this.eventId}/${landingPageId}/information_tab_view`;
    let getInformationTabViewResp = await this.orgApiRequest.get(
      informationTabEndpoint
    );

    if (getInformationTabViewResp.status() != 200) {
      throw new Error(
        `API to fetch information tab view failed with ${getInformationTabViewResp.status()}`
      );
    }
    return getInformationTabViewResp;
  }

  async updateInformationTabView(payLoadToUpdateAsJson) {
    let landingPageId = await this.getEventLandingPageId();
    let postInformationTabViewResp = await this.orgApiRequest.post(
      `/api/event/landing_page/${this.eventId}/${landingPageId}/information_tab_view`,
      {
        data: payLoadToUpdateAsJson,
      }
    );
    if (postInformationTabViewResp.status() != 200) {
      throw new Error(
        `API to add custom fields in information tab view failed with ${postInformationTabViewResp.status()}`
      );
    }
  }

  async addCustomFieldsWithConditionToEvent(listOfCustomFieldsToAdd) {
    /*
    1. Make a copy of the test data object passed to this functon
    2. Process the test data object and replace event id and landing page id of this event
    3. get the information tab response and fetch the reg custom feield object from it
    4. now iterate through each reg feield obj from test data and see if it has the condition fields object
    5. itreate through each condition, fetch corresponding custom field id for the label
    6. hit add conditions endpoint

    */
    //    1. Make a copy of the test data object passed to this functon
    const testDataObject = JSON.parse(JSON.stringify(listOfCustomFieldsToAdd));

    console.log(`#### Fetching the event information tab response #### `);
    let informationTabViewRespJson = await (
      await this.getInformationTabView()
    ).json();

    console.log(`#### Extracting the landing page id #### `);
    let landingPageId = await this.getEventLandingPageId();
    //modify the data to add event id and landing page id for this event

    console.log(
      "#### modifying the test data and replace the event id and landing page id #### "
    );
    //2. Process the test data object and remove conditional fields object
    for (const eachCustomFieldObj of testDataObject) {
      eachCustomFieldObj.eventId = this.eventId;
      eachCustomFieldObj.eventLandingPageId = landingPageId;
    }

    //3. Now replace the reg custom field object of fetched information tab response with the test data obj
    informationTabViewRespJson.registrationCustomFields = testDataObject;

    //4. Now post the information tab response with updated payload with custom fields
    await this.updateInformationTabView(informationTabViewRespJson);

    //5. Now wait until the get call to information tab starts showing the custom fields
    let informationTabResponseWithCustomFields;
    await expect(async () => {
      const informationTabRespWithCustomFields =
        await this.getInformationTabView();
      const informationTabRespWithCustomFieldsJson =
        await informationTabRespWithCustomFields.json();
      const listOfcustomFieldsObject =
        informationTabRespWithCustomFieldsJson.registrationCustomFields;
      expect(
        listOfcustomFieldsObject.length,
        "expecting length of list of custom fields object to be > 0"
      ).not.toBe(0);
      informationTabResponseWithCustomFields =
        informationTabRespWithCustomFieldsJson;
    }, "waiting for information tab response to show updated custom fields").toPass();

    //6. Now apply conditions to the fields who have condition object in test data
    const listOfFetchedcustomFieldObjectWithIds =
      informationTabResponseWithCustomFields["registrationCustomFields"];

    for (const eachCustomFieldObj of testDataObject) {
      let thisFieldName = eachCustomFieldObj["fieldName"];
      let thisFieldLabel = eachCustomFieldObj["labelName"];
      //fetch the reg custom feild id of this field

      let thisFieldCustomFieldId =
        await this.fetchRegistrationCustomFieldIdWithLabel(
          thisFieldLabel,
          informationTabResponseWithCustomFields
        );

      //checking if this field is conditional (if len conditional obj > 0)
      let listOfConditionalFieldsObject =
        eachCustomFieldObj["customFieldConditions"];
      if (listOfConditionalFieldsObject.length > 0) {
        console.log(`this field ${thisFieldName} have conditional objects`);

        // now apply conditional to this field
        for (const eachConditionObject of listOfConditionalFieldsObject) {
          const dependentFieldLabel =
            eachConditionObject["conditionCustomFieldId"];
          // finding the dependent field custom id
          let dependentFieldCustomFieldId =
            await this.fetchRegistrationCustomFieldIdWithLabel(
              dependentFieldLabel,
              informationTabResponseWithCustomFields
            );
          // now replace the label with id in conditional field object
          eachConditionObject["conditionCustomFieldId"] =
            dependentFieldCustomFieldId;
        }
        // now you have the updated list of conditionalFields Object
        await this.hitPostApiToApplyConditions(
          listOfConditionalFieldsObject,
          thisFieldCustomFieldId
        );
      }
    }
  }

  async addDiscalimerFieldsToEvent(listOfDiscalimerFieldsToAdd) {
    const testDataObjectOfDiscalimerFields = JSON.parse(
      JSON.stringify(listOfDiscalimerFieldsToAdd)
    );
    console.log(`#### Fetching the event information tab response #### `);
    let informationTabViewRespJson = await (
      await this.getInformationTabView()
    ).json();

    console.log(`#### Extracting the landing page id #### `);
    let landingPageId = await this.getEventLandingPageId();
    //modify the data to add event id and landing page id for this event

    console.log(
      "#### modifying the test data and replace the event id and landing page id #### "
    );
    //2. Process the test data object and remove conditional fields object
    for (const eachDiscalimerFieldObject of testDataObjectOfDiscalimerFields) {
      eachDiscalimerFieldObject.eventLandingPageId = landingPageId;
    }

    //3. Now replace the reg custom field object of fetched information tab response with the test data obj
    informationTabViewRespJson.eventLandingPageDisclaimers =
      testDataObjectOfDiscalimerFields;

    //4. now update the information tab with discalimer fields
    await this.updateInformationTabView(informationTabViewRespJson);
  }

  async hitPostApiToApplyConditions(
    listOfConditionsObject,
    registrationFieldCustomId
  ) {
    const applyConditionEndpoint = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/api/event/landing_page/${
      this.eventId
    }/${registrationFieldCustomId}/field_conditions`;

    const applyConditionApiResp = await this.orgApiRequest.post(
      applyConditionEndpoint,
      {
        data: listOfConditionsObject,
      }
    );
    if (applyConditionApiResp.status() != 200) {
      throw new Error(`Post API call to apply condition on field ${registrationFieldCustomId} failed with 
      ${applyConditionApiResp.status()}`);
    }
  }

  async fetchRegistrationCustomFieldIdWithLabel(
    fieldLabel,
    informationTabResponseJson
  ) {
    const listOfRegistrationFields =
      informationTabResponseJson.registrationCustomFields;
    if (listOfRegistrationFields.length == 0) {
      throw new Error(
        `recieved informatin tab response does not have any custom field`
      );
    }
    for (const eachRegistrationCustomFieldObj of listOfRegistrationFields) {
      const currentFieldLabel =
        eachRegistrationCustomFieldObj["labelName"].toLowerCase();
      if (currentFieldLabel == fieldLabel.toLowerCase()) {
        let registrationCustomFieldId =
          eachRegistrationCustomFieldObj["registrationCustomFieldId"];
        console.log(
          `field with label ${fieldLabel} found in the informaiton tab response with id : ${registrationCustomFieldId}`
        );
        return registrationCustomFieldId;
      }
    }
  }

  async getEventLandingPageDetails() {
    console.log(
      `getting event landing page details by API for event id ${this.eventId}`
    );
    const eventLandingPageResp = await this.orgApiRequest.get(
      `/api/event/landing_page/${this.eventId}`
    );

    if (eventLandingPageResp.status() != 200) {
      throw new Error(
        `API to fetch event landing page details failed with ${eventLandingPageResp.status()}`
      );
    }
    return eventLandingPageResp;
  }

  async getEventLandingPageId() {
    console.log(
      `getting event landing page details by API for event id ${this.eventId}`
    );
    let eventLandingPageResJson = await (
      await this.getEventLandingPageDetails()
    ).json();
    let eventLandingPageId = eventLandingPageResJson.eventLandingPageId;
    console.log(`the event landing page id is ${eventLandingPageId}`);
    return eventLandingPageId;
  }

  async addInviteListToEvent(
    s3SignedUrl: string,
    name: string = "l4",
    description: string = "l4"
  ) {
    let inviteListApiResp = await this.orgApiRequest.post(
      `/api/audience/${this.eventId}/invite-list/import`,
      {
        data: {
          s3FileUrl: s3SignedUrl,
          eventId: this.eventId,
          fileExtension: "csv",
          audienceInviteListName: name,
          audienceInviteListDescription: description,
        },
      }
    );

    console.log(`invite list api response is ${inviteListApiResp.status()}`);
    console.log(`invite list api response is ${inviteListApiResp.text()}`);

    if (inviteListApiResp.status() != 200) {
      throw new Error(
        `API to add invite list to event failed with ${inviteListApiResp.status()}`
      );
    }
  }

  async getInviteListAudienceGroupId(groupName: string = "l4") {
    let audienceGroups = await this.orgApiRequest.get(
      `/api/audience/audience-group/${this.eventId}`
    );
    if (audienceGroups.status() != 200) {
      throw new Error(
        `API to get audience group list to event failed with ${audienceGroups.status()}`
      );
    }
    // invitedGroupList
    let invitedGroupList = (await audienceGroups.json())["invitedGroupList"];
    console.log(invitedGroupList);
    // iterate over invitedGroupList to find item with name == groupName
    let audienceGroupId: string;
    for (let group of invitedGroupList) {
      if (group.name === groupName) {
        audienceGroupId = group.audienceGroupId;
        return audienceGroupId;
      }
    }
    throw new Error(`No audience group found with name ${groupName}`);
  }

  async registerUserToEventWithV1BetaAPI({
    firstName,
    lastName,
    email,
    phoneNumber,
    designation,
    company,
  }: RegistrationPayload) {
    const v1Payload = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      phoneNumber: phoneNumber,
      company: company,
      designation: designation,
    };
    let jwtTokenForV1BetaApi =
      DataUtl.getApplicationTestDataObj()["jwtTokenV1BetaApi"];
    const headers = {
      Authorization: `Bearer ${jwtTokenForV1BetaApi}`,
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/x-www-form-urlencoded",
      "User-Agent":
        "Zuddl, Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko)",
    };
    let emptyBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    let emptyApiRequestContext = emptyBrowserContext.request;

    const v1BetaRegApiRes = await emptyApiRequestContext.post(
      `/api/v1beta/event/${this.eventId}/register`,
      {
        form: v1Payload,
        headers: headers,
      }
    );
    return v1BetaRegApiRes;
  }

  async registerUserToEventWithRegistrationApi(
    {
      firstName,
      lastName,
      email,
      phoneNumber = null,
      designation = null,
      company = null,
      country = null,
      customField = '{"disclaimers":[]}',
      bio = null,
    }: RegistrationPayload,
    raiseError = true
  ): Promise<APIResponse> {
    let registrationEndPoint = `/api/event/user_registration`;
    console.log(`Registering ${email} to ${this.eventId} by API `);
    const addUserByRegApiResp = await this.orgApiRequest.post(
      registrationEndPoint,
      {
        data: {
          eventId: this.eventId,
          firstName: firstName,
          lastName: lastName,
          email: email,
          bio: bio,
          designation: designation,
          company: company,
          phoneNumber: phoneNumber,
          country: country,
          customField: customField,
        },
      }
    );
    if (addUserByRegApiResp.status() != 200 && raiseError) {
      throw new Error(
        `API to add registration via api failed with ${addUserByRegApiResp.status()}`
      );
    }
    return addUserByRegApiResp;
  }

  async updateEventEntryTime(
    startDateTime?,
    endDateTime?,
    timeZone = "Asia/Kolkata"
  ) {
    const eventInfoRespBeforeUpdate = await this.getEventInfo();
    const eventInfoRespJsonBeforeUpdate =
      await eventInfoRespBeforeUpdate.json();
    const eventStartTimeBeforeUpdate =
      eventInfoRespJsonBeforeUpdate["startDateTime"];
    const eventEndTimeBeforeUpdate =
      eventInfoRespJsonBeforeUpdate["endDateTime"];
    //see if we have to update event start date time
    if (startDateTime) {
      console.log(
        `updating event start date time to ${startDateTime} from ${eventStartTimeBeforeUpdate}`
      );
      eventInfoRespJsonBeforeUpdate["startDateTime"] = startDateTime;
    }

    //see if we have to update event end date time
    if (endDateTime) {
      console.log(
        `updating event end date time to ${endDateTime} from ${eventEndTimeBeforeUpdate}`
      );
      eventInfoRespJsonBeforeUpdate["endDateTime"] = endDateTime;
    }

    // adding the timezone
    eventInfoRespJsonBeforeUpdate["tz"] = timeZone;

    // hit post request to update the landing page entry time and timezone
    const updateLandingPageRes = await this.orgApiRequest.post(
      `/api/event/${this.eventId}`,
      {
        data: eventInfoRespJsonBeforeUpdate,
      }
    );

    if (updateLandingPageRes.status() != 200) {
      throw new Error(
        `Update API call to event start date and end date failed with ${updateLandingPageRes.status()}`
      );
    }
    // asserting if after event update, everything is fine
    const eventInfoRespAfterUpdate = await this.getEventInfo();
    const eventInfoRespJsonAfterUpdate = await eventInfoRespAfterUpdate.json();
    const eventStartTimeAfterUpdate =
      eventInfoRespJsonAfterUpdate["startDateTime"];
    const eventEndTimeAfterUpdate = eventInfoRespJsonAfterUpdate["endDateTime"];

    // if (startDateTime != null) {
    //   expect(eventStartTimeAfterUpdate).toBe(startDateTime);
    // }

    // if (endDateTime != null) {
    //   expect(eventEndTimeAfterUpdate).toBe(endDateTime);
    // }
  }

  async getEventInfo() {
    const eventInfoResp = await this.orgApiRequest.get(
      `/api/event/${this.eventId}`
    );
    if (eventInfoResp.status() != 200) {
      throw new Error(
        `API call to get event info failed with ${eventInfoResp.status()}`
      );
    }
    return eventInfoResp;
  }

  async changeEventStartAndEndDateTime({
    deltaByDayInStartDate,
    deltaByDayInEndDate,
    deltaByHoursInStartTime,
    deltaByHoursInEndTime,
    deltaByMinuteInStartTime,
    deltaByMinuteInEndTime,
    deltaBySecondsInStartTime,
    deltaBySecondsInEndTime,
    eventTimeZone = "Asia/Kolkata",
  }: UpdateEventStartAndEndDateTime) {
    /*
     *since reference point is current date
     * we will initalise start date as cur date and end date as cur date\
     * to calculate the delta by days, hours and minuts in
     * both start and end date of the event
     */
    const startDate = new Date();

    const endDate = new Date();

    //calculating the start date time first
    if (deltaByDayInStartDate) {
      startDate.setDate(startDate.getDate() + deltaByDayInStartDate);
    }
    if (deltaByHoursInStartTime) {
      startDate.setHours(startDate.getHours() + deltaByHoursInStartTime);
    }
    if (deltaByMinuteInStartTime) {
      startDate.setMinutes(startDate.getMinutes() + deltaByMinuteInStartTime);
    }
    if (deltaBySecondsInStartTime) {
      startDate.setSeconds(startDate.getSeconds() + deltaBySecondsInStartTime);
    }

    //calculating the end datetime now
    if (deltaByDayInEndDate) {
      endDate.setDate(endDate.getDate() + deltaByDayInEndDate);
    }
    if (deltaByHoursInEndTime) {
      endDate.setHours(endDate.getHours() + deltaByHoursInEndTime);
    }
    if (deltaByMinuteInEndTime) {
      endDate.setMinutes(endDate.getMinutes() + deltaByMinuteInEndTime);
    }
    if (deltaBySecondsInEndTime) {
      endDate.setSeconds(endDate.getSeconds() + deltaBySecondsInEndTime);
    }

    // convert the start date time and end date time into ISO string
    // removing the milli second string as its not handled by our client
    const startDateTimeIsoString = startDate.toISOString().split(".")[0] + "Z";
    const endDateTimeIsoString = endDate.toISOString().split(".")[0] + "Z";

    console.log(
      `After the processing, start date time is ${startDateTimeIsoString}`
    );
    console.log(
      `After the processing, end date time is ${endDateTimeIsoString}`
    );
    await this.updateEventEntryTime(
      startDateTimeIsoString,
      endDateTimeIsoString,
      eventTimeZone
    );
    createEventPayloadData.endDateTime = endDateTimeIsoString;
    createEventPayloadData.startDateTime = startDateTimeIsoString;
  }

  async applyEmailRestrictionToNotAllowBusinessEmails() {
    const landingPageId = await this.getEventLandingPageId();
    await updateEmailRestrictionSettings(
      this.orgApiRequest,
      this.eventId,
      landingPageId,
      null,
      EmailRestrictionType.DENY,
      [RestrictionEmailDomainType.PREDEFINED]
    );
  }

  async getDefaultStageData() {
    const listOfStageResp = await this.getStageListData();
    const listOfStageRespJson = await listOfStageResp.json();
    const defaultStageData = listOfStageRespJson[0]; //0th index stage is always default stage
    return defaultStageData;
  }

  async getDefaultStageId() {
    const defaultStageData = await this.getDefaultStageData();
    if (defaultStageData === undefined) return null;
    const stageId = defaultStageData["stageId"];
    console.log(`stage Id is ${stageId}`);
    return stageId;
  }

  async enableStudioAsBackstage(stageId: string) {
    console.log(`enabling StudioAsBackstage for stage ${stageId}`);
    const enableStudioAsBackstageResp = await this.orgApiRequest.post(
      `/api/stage/${stageId}/studio/enable`
    );

    if (enableStudioAsBackstageResp.status() != 200) {
      throw new Error(
        `API call to enable StudioAsBackstage failed with ${enableStudioAsBackstageResp.status()}`
      );
    }

    return enableStudioAsBackstageResp.text();
  }

  async disableStudioAsBackstage(stageId: string) {
    const disableStudioAsBackstageResp = await this.orgApiRequest.post(
      `/api/stage/${stageId}/studio/disable`
    );
    if (disableStudioAsBackstageResp.status() != 200) {
      throw new Error(
        `API call to disable StudioAsBackstage failed with ${disableStudioAsBackstageResp.status()}`
      );
    }
  }

  async getStageListData() {
    const listOfStageResp = await this.orgApiRequest.get(
      `/api/stage/${this.eventId}/list`
    );
    if (listOfStageResp.status() != 200) {
      throw new Error(
        `API call to fetch stage list failed with ${listOfStageResp.status()}`
      );
    }
    return listOfStageResp;
  }

  async inviteSpeakerToTheEventByApi(
    speakerEmail: string,
    firstName: string,
    lastName: string
  ) {
    await test.step(`Inviting speaker ${speakerEmail} to the event ${this.eventId}`, async () => {
      await inviteSpeakerByApi(
        this.orgApiRequest,
        new SpeakerData(
          speakerEmail,
          firstName,
          lastName
        ).getSpeakerDataObject(),
        this.eventId,
        true
      );
    });
  }

  async inviteAttendeeToTheEvent(userEmailToAdd: string) {
    let addAttendeeApiResp: APIResponse;
    await test.step(`Inviting Attendee with email ${userEmailToAdd} to the event ${this.eventId}`, async () => {
      addAttendeeApiResp = await inviteAttendeeByAPI(
        this.orgApiRequest,
        this.eventId,
        userEmailToAdd,
        true
      );
    });
    return addAttendeeApiResp!;
  }

  async triggerDuplicateEventApiForThisEvent() {
    //get event details
    // const eventDetailsResp = await this.getEventInfo();
    // const eventDetailsRespJson = await eventDetailsResp.json();
    const duplicatEventApiResp = await this.orgApiRequest.post(
      `/api/event/${this.eventId}/duplicate`
    );
    if (duplicatEventApiResp.status() != 200) {
      throw new Error(
        `API to duplicate event ${this.eventId} failed with duplicatEventApiResp.status()`
      );
    }
    // get the duplicate eventId
    const duplicatEventApiRespJson = await duplicatEventApiResp.json();
    const duplicateEventId = duplicatEventApiRespJson["eventId"];
    console.log(`Duplicate event Id is ${duplicateEventId}`);
    return duplicateEventId;
  }
  async changeWebinarDescription(description: string) {
    let changeWebinarDescriptionResponse: APIResponse;
    await test.step(`Changing webinar description to ${description} of the event ${this.eventId}`, async () => {
      // /api/event/eb207074-b148-4bfe-a81c-13dfa5f54de7
      let getWebinarPayLoadData = await this.orgApiRequest.get(
        `/api/event/${this.eventId}`
      );
      let webinarPayLoad: JSON = await getWebinarPayLoadData.json();
      webinarPayLoad["description"] = description;
      changeWebinarDescriptionResponse = await this.orgApiRequest.post(
        `/api/event/${this.eventId}`,
        {
          data: webinarPayLoad,
        }
      );
      if (changeWebinarDescriptionResponse.status() != 200) {
        throw new Error(
          `API call to add new webinar description failed with status ${changeWebinarDescriptionResponse.status()}`
        );
      }
    });
    return changeWebinarDescriptionResponse!;
  }
  async getEventChannelId() {
    return await this.getChannelId(this.eventId);
  }

  async postEventChatMessage(contentToPost: string, postAsHost = false) {
    const channelId = await this.getEventChannelId();
    console.log(`channel id is ${channelId}}`);
    const postMessageApiResp = await this.orgApiRequest.post(
      `/api/channel/${channelId}/message?host=${postAsHost}`,
      {
        data: {
          content: contentToPost,
          mediaType: "text",
        },
      }
    );
    if (postMessageApiResp.status() != 200) {
      throw new Error(
        `API call to post new message failed with ${postMessageApiResp.status()}`
      );
    }
    return postMessageApiResp;
  }

  async postEventChatMessageAndGetMessageId(
    contentToPost: string,
    postAsHost = false
  ) {
    const postChatMessageApiResp = await this.postEventChatMessage(
      contentToPost,
      postAsHost
    );
    const postChatMessageApiRespJson = await postChatMessageApiResp.json();
    const messageId = postChatMessageApiRespJson["messageId"];
    return messageId;
  }
  async postGifInEventChat(contentToPost: string, postAsHost = false) {
    const channelId = await this.getEventChannelId();
    console.log(`channel id is ${channelId}}`);
    const postMessageApiResp = await this.orgApiRequest.post(
      `/api/channel/${channelId}/message?host=${postAsHost}`,
      {
        data: {
          content: contentToPost,
          mediaType: "gif",
        },
      }
    );
    if (postMessageApiResp.status() != 200) {
      throw new Error(
        `API call to post new gif message failed with ${postMessageApiResp.status()}`
      );
    }
  }

  async validateEventStartTimeFromApi(
    eventHourStartTime: string,
    eventMinutesStartTime: string,
    eventStartTimeMeridian: string,
    timeZone = "Asia/Kolkata"
  ) {
    const eventInfoRespBeforeUpdate = await this.getEventInfo();
    const eventInfoRespJsonBeforeUpdate =
      await eventInfoRespBeforeUpdate.json();
    let eventStartTimeValue = eventInfoRespJsonBeforeUpdate["startDateTime"];
    eventStartTimeValue = new Date(eventStartTimeValue).toLocaleTimeString(
      "en-US",
      {
        timeZone: timeZone,
      }
    );
    console.log(`the event start time value fetched is ${eventStartTimeValue}`);
    expect(
      eventStartTimeValue,
      `Selected event time in hour to contain hour ${eventHourStartTime}`
    ).toContain(eventHourStartTime);
    expect(
      eventStartTimeValue,
      `Selected event time in hour to contain minute ${eventMinutesStartTime}`
    ).toContain(eventMinutesStartTime);
    expect(
      eventStartTimeValue,
      `Selected event time in hour to contain meridain ${eventStartTimeMeridian}`
    ).toContain(eventStartTimeMeridian);
  }

  async eventCreationWithMultipleVenueSetup(
    options: optionalParamsForEventCreation = {}
  ) {
    const { noOfRooms = 1, noOfBooths = 1 } = options;
    await this.roomsController.addRoomsToEvent({
      seatsPerRoom: 3,
      numberOfRooms: noOfRooms,
      roomCategory: "PUBLIC",
    });

    await this.expoController.createMultipleBoothsWithoutZone(noOfBooths);
    await this.scheduleController.addSessionToScheduleEvent({
      title: "TestSession",
      description: "Session Description",
      type: "NONE",
      startDateTime: await formatdateString(
        (await getStartDateFromToday()).toString()
      ),
      endDateTime: await formatdateString(
        (await getEndDateFromToday()).toString()
      ),
      eventId: this.eventId,
    });
    await this.stageController.addNewStageToEvent({
      name: "My Large Stage",
      size: StageSize.LARGE,
    });
  }

  async createNewStage(name: string, size: StageSize = StageSize.LARGE) {
    let response = await this.stageController.addNewStageToEvent({
      name: name,
      size: size,
    });

    let stageId = response;
    console.log(`Stage id - ${stageId}`);
    return stageId;
  }

  async createSession(
    sessionDataId: number = 1,
    refId: string,
    daysFromStartDate: number = 0,
    daysFromEndDate: number = 0,
    hoursFromStartDate: number = 0,
    hoursFromEndDate: number = 1,
    minutesFromStartDate: number = 0,
    minutesFromEndDate: number = 0,
    venue: string = "STAGE",
    speakers: {
      speakerId: string;
      speakerEmail: string;
      firstName: string;
      lastName: string;
    }[] = [],
    tags: string[] = [],
    sessionName: string = ""
  ) {
    let speakerObjs = [];
    for (let speaker of speakers) {
      let speakerObj = new SpeakerData(
        speaker.speakerEmail,
        speaker.firstName,
        speaker.lastName
      );
      console.log(`${speaker.speakerId}`);
      speakerObj.speakerId = speaker.speakerId;
      speakerObjs.push(speakerObj.getSpeakerDataObject());
    }
    console.log(`Speaker Objs - ${JSON.stringify(speakerObjs)}`);

    let sessionTitle;
    if (sessionName === "") {
      sessionTitle = sessionDataPayload[sessionDataId]["title"];
    } else {
      sessionTitle = sessionName;
    }

    let sessionId = await this.scheduleController.addSession({
      title: sessionTitle,
      description: sessionDataPayload[sessionDataId]["description"],
      hiddenSegment: sessionDataPayload[sessionDataId]["hiddenSegment"],
      speakers: speakerObjs,
      type: venue,
      startDateTime: await this.modifyDateTime(
        daysFromStartDate,
        hoursFromStartDate,
        minutesFromStartDate
      ),
      endDateTime: await this.modifyDateTime(
        daysFromEndDate,
        hoursFromEndDate,
        minutesFromEndDate
      ),
      refId: refId,
      eventId: this.eventId,
      day: "",
      segmentFormat: "VIRTUAL",
      eventInPersonLocationId: null,
      maxAvailableSpots: null,
    });
    console.log(`Session ID - ${sessionId}`);
    return sessionId;
  }

  async getEventStartDatetime() {
    let eventInfo = await this.getEventInfo();
    let eventInfoJson = await eventInfo.json();

    return eventInfoJson["startDateTime"];
  }

  async getEventTitle() {
    let eventInfo = await this.getEventInfo();
    let eventInfoJson = await eventInfo.json();
    let eventTitle = eventInfoJson.title;

    console.log("Event title ->", eventTitle);
    return eventTitle;
  }

  async modifyDateTime(
    deltaDays: number = 0,
    deltaHours: number = 0,
    deltaMinutes: number = 0,
    curDate: boolean = true
  ) {
    let modifiedDate;
    if (curDate) {
      modifiedDate = new Date();
    } else {
      let eventStartDateTime = await this.getEventStartDatetime();
      modifiedDate = new Date(eventStartDateTime);
    }

    console.log(`Current date time - ${modifiedDate.toISOString()}`);

    if (deltaDays) {
      modifiedDate.setDate(modifiedDate.getDate() + deltaDays);
    }
    if (deltaHours) {
      modifiedDate.setHours(modifiedDate.getHours() + deltaHours);
    }
    if (deltaMinutes) {
      modifiedDate.setMinutes(modifiedDate.getMinutes() + deltaMinutes);
    }

    const dateTimeIsoString = modifiedDate.toISOString().split(".")[0] + "Z";
    console.log(`After processing, curent date time - ${dateTimeIsoString}`);

    return dateTimeIsoString;
  }

  async deleteSession(sessionId) {
    let deleteSessionResponse = await this.scheduleController.deleteSession(
      sessionId
    );
    console.log(`Session deleted successfully - ${deleteSessionResponse}`);
  }

  async tranferScene(studioId: string, sceneId: string, sessionId: string) {
    let transferSceneResponse = await this.orgApiRequest.post(
      `/api/studio/${studioId}/shot/${sceneId}/segment/${sessionId}`
    );

    if (transferSceneResponse.status() != 200) {
      throw new Error(
        `API call to transfer scene failed with status ${transferSceneResponse.status()}`
      );
    }
  }

  async changeSession(studioId: string, sessionId: string) {
    let changeSessionData: ChangeSessionPayload = {
      selectedSegmentId: sessionId,
    };

    let changeSessionResponse = await this.orgApiRequest.patch(
      `/api/studio/${studioId}/settings`,
      { data: changeSessionData }
    );

    if (changeSessionResponse.status() != 200) {
      throw new Error(
        `API call to change session failed with status ${changeSessionResponse.status()}`
      );
    }
  }

  async getAllSessions(eventId: string) {
    let getAllSessionsResponse = await this.orgApiRequest.get(
      `/api/segment/${eventId}/list/all?r=`
    );

    if (getAllSessionsResponse.status() != 200) {
      throw new Error(
        `API call to get all sessions failed with status ${getAllSessionsResponse.status()}`
      );
    }

    return getAllSessionsResponse.json();
  }

  async getRequiredInfo(eventId: string, sessionId: string) {
    const data = await this.getAllSessions(eventId);
    const session = data.find((session) => session.segmentId === sessionId);
    console.log(session.startDateTime);
    console.log(session.endDateTime);

    return session;
  }

  async createTag(tagName: string) {
    const createTagResponse = await this.orgApiRequest.post(
      `/api/event/${this.eventId}/tags`,
      {
        data: {
          name: tagName,
          eventId: this.eventId,
          description: tagName,
        },
      }
    );

    if (createTagResponse.status() != 200) {
      throw new Error(
        `API call to create tag failed with status ${createTagResponse.status()}`
      );
    }

    console.log(`Tag created successfully - ${tagName}`);
    let allTags = await this.getAllTags();
    console.log(`All tags - ${JSON.stringify(allTags)}`);

    const createTagResponseJson = await createTagResponse.json();
    const tagId = createTagResponseJson["tagId"];
    return tagId;
  }

  async getAllTags() {
    const getAllTagsResponse = await this.orgApiRequest.get(
      `/api/event/${this.eventId}/tags/`
    );

    if (getAllTagsResponse.status() != 200) {
      throw new Error(
        `API call to get all tags failed with status ${getAllTagsResponse.status()}`
      );
    }

    let tagList = await getAllTagsResponse.json();
    return tagList;
  }

  async getChannelId(refId: string) {
    const getChannelConfigResp = await this.orgApiRequest.get(
      `/api/channel/config?refId=${refId}`
    );
    if (getChannelConfigResp.status() != 200) {
      throw new Error(
        `API call to get  channel id for refid ${refId} failed with status ${getChannelConfigResp.status()}`
      );
    }
    const getChannelConfigRespJson = await getChannelConfigResp.json();
    const channelId = getChannelConfigRespJson["channelId"];
    return channelId;
  }

  async updateEventLandingPageSettings({
    eventEntryType = EventEntryType.REG_BASED,
    isMagicLinkEnabled = true,
    isSpeakermagicLinkEnabled = true,
    isGuestAuthEnabled = true,
    speakerAuthOptions = [
      "EMAIL",
      "EMAIL_OTP",
      "SSO",
      "FACEBOOK",
      "LINKEDIN",
      "GOOGLE",
    ],
    attendeeAuthOptions = [
      "EMAIL",
      "EMAIL_OTP",
      "SSO",
      "FACEBOOK",
      "LINKEDIN",
      "GOOGLE",
    ],
  }: eventLandingPageOptions) {
    let landingPageRes = await this.getEventLandingPageDetails();
    let landingPageResJson = await landingPageRes.json();
    let landingPageId = landingPageResJson["eventLandingPageId"];
    //update the landing page res json

    landingPageResJson["eventEntryType"] = eventEntryType;
    landingPageResJson["guestModeEnabled"] = isGuestAuthEnabled;
    landingPageResJson["magicLinkEnabled"] = isMagicLinkEnabled;
    landingPageResJson["attendeeAuthType"] = attendeeAuthOptions;
    landingPageResJson["speakerAuthType"] = speakerAuthOptions;
    landingPageResJson["speakerMagicLinkEnabled"] = isSpeakermagicLinkEnabled;

    // hit patch api call
    const updated_landing_page_res = await this.orgApiRequest.patch(
      `/api/event/landing_page/${this.eventId}/${landingPageId}`,
      {
        data: landingPageResJson,
      }
    );
    if (updated_landing_page_res.status() != 200) {
      throw new Error(
        `patch api call to update landing page failed with ${updated_landing_page_res.status()}, with message ${await updated_landing_page_res.body()}`
      );
    }
  }
  async getRoomId(eventId: any) {
    const getRoomResponse = await this.orgApiRequest.get(
      `api/event/${eventId}/discussion_tables/list`
    );
    if (getRoomResponse.status() != 200) {
      throw new Error(
        `API call to get room id for refid failed with status ${getRoomResponse.status()}`
      );
    }
    const getRoomConfigRespJson = await getRoomResponse.json();
    const roomId = getRoomConfigRespJson[0]["discussionTableId"];
    console.log(roomId);
    return roomId;
  }

  async getBoothId(eventId: any) {
    const getBoothResponse = await this.orgApiRequest.get(
      `api/event/${eventId}/booth/list?showHidden=false`
    );
    if (getBoothResponse.status() != 200) {
      throw new Error(
        `API call to get booth id for refid failed with status ${getBoothResponse.status()}`
      );
    }
    const getBoothConfigRespJson = await getBoothResponse.json();
    const boothId = getBoothConfigRespJson[0]["boothId"];
    console.log(boothId);
    return boothId;
  }

  async fetchRoomIdFromListOfRoomsByRoomName(expRoomName: string) {
    let discussionTableId: string;
    const getRoomResponse = await this.orgApiRequest.get(
      `api/event/${this.eventId}/discussion_tables/list`
    );
    if (getRoomResponse.status() != 200) {
      throw new Error(
        `API call to get room id for refid failed with status ${getRoomResponse.status()}`
      );
    }
    const getRoomConfigRespJson = await getRoomResponse.json();
    for (const eachRoomObj of getRoomConfigRespJson) {
      const roomName = eachRoomObj["name"];
      if (roomName === expRoomName) {
        discussionTableId = eachRoomObj["discussionTableId"];
        break;
      }
    }
    return discussionTableId;
  }

  async toggleScheduleSync(enable: boolean) {
    const toggleScheduleSyncEndpoint = `/api/event/${this.eventId}/scheduleSyncActiveState?active=${enable}`;
    const toggleScheduleSyncApiResp = await this.orgApiRequest.post(
      toggleScheduleSyncEndpoint
    );
    if (toggleScheduleSyncApiResp.status() != 200) {
      throw new Error(
        `API call to toggle schedule sync failed with ${toggleScheduleSyncApiResp.status()}`
      );
    } else {
      console.log(`Schedule sync toggled to ${enable}`);
    }
  }

  async GenerateCSV(
    fileName = "schedule.csv",
    sessionName = "Test Session",
    timeZone = "Asia/Kolkata",
    aboutSession = "This is test discription.",
    duration = "30",
    venue = "",
    subVenue = "",
    registeredSpeaker = "",
    eventTags = "",
    hiddenSession = ""
  ) {
    const writer = csvWriter.createObjectCsvWriter({
      path: path.resolve("test-data", fileName),
      header: [
        { id: "sessionName", title: "Session Name (Max 100)" },
        { id: "aboutSession", title: "About the session (Max 200 characters)" },
        { id: "date", title: "Date (dd/mm/yyyy)" },
        { id: "startTime", title: "Start time (Event TZ)(24 hr)" },
        { id: "duration", title: "Duration (Mins)" },
        { id: "venue", title: "Venue" },
        { id: "subVenue", title: "Sub venue" },
        { id: "registeredSpeaker", title: "Registered Speaker " },
        { id: "eventTags", title: "Event tags" },
        { id: "hiddenSession", title: "Hidden session" },
      ],
    });

    let date = new Date();
    let startTime = new Date();

    const formattedDate = date.toLocaleDateString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
      timeZone: timeZone,
    });

    const formattedStartTime = startTime.toLocaleTimeString("en-GB", {
      hour: "2-digit",
      minute: "2-digit",
      hour12: false,
      timeZone: timeZone,
    });

    console.log(formattedDate);
    console.log(formattedStartTime);

    let sessionData = [
      {
        sessionName: sessionName,
        aboutSession: aboutSession,
        date: formattedDate,
        startTime: formattedStartTime,
        duration: duration,
        venue: venue,
        subVenue: subVenue,
        registeredSpeaker: registeredSpeaker,
        eventTags: eventTags,
        hiddenSession: hiddenSession,
      },
    ];

    console.log(sessionData);

    writer.writeRecords(sessionData).then(() => {
      console.log("CSV Generated!");
    });
  }

  async getAllStages() {
    const getAllStagesApiResp = await this.getStageListData();
    console.log(
      `All stages are ...`,
      inspect(getAllStagesApiResp, { depth: null })
    );
    const getAllStagesApiRespJson = await getAllStagesApiResp.json();
    return getAllStagesApiRespJson;
  }

  async duplicateEvent({
    title = "Automation duplicate event",
    deltaHours = 0,
    deltaDays = 0,
    isEventSetup = true,
    isRegistration = true,
    isVenueSetup = true,
    isAccessGroups = true,
    isCommunication = true,
    tz = "Asia/Kolkata",
  }: EventSettings) {
    let eventDuplPayload = {
      title: title,
      startDateTime: await this.modifyDateTime(deltaDays, deltaHours, 0, false),
      isEventSetup: isEventSetup,
      isRegistration: isRegistration,
      isVenueSetup: isVenueSetup,
      isAccessGroups: isAccessGroups,
      isCommunication: isCommunication,
      tz: tz,
    };

    console.log(
      `Duplicate event payload is ...`,
      inspect(eventDuplPayload, { depth: null })
    );

    const duplicateEventApiResp = await this.orgApiRequest.post(
      `/api/event/${this.eventId}/duplicate/v2`,
      {
        data: eventDuplPayload,
      }
    );

    if (duplicateEventApiResp.status() != 200) {
      throw new Error(
        `API call to duplicate event failed with ${duplicateEventApiResp.status()}`
      );
    }

    const duplicateEventApiRespJson = await duplicateEventApiResp.json();
    const duplicateEventId = duplicateEventApiRespJson["eventId"];
    console.log(`Duplicate event Id is ${duplicateEventId}`);
    console.log(
      `Duplicate event details are ${JSON.stringify(duplicateEventApiRespJson)}`
    );

    return duplicateEventId;
  }
}

export interface ChangeSessionPayload {
  selectedSegmentId: string;
}

export interface RegistrationPayload {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber?: string | null;
  designation?: string | null;
  company?: string | null;
  country?: string | null;
  customField?: any;
  bio?: string | null;
}

export interface UpdateEventStartAndEndDateTime {
  deltaByDayInStartDate?; //give value in minus if you want to minus the days
  deltaByDayInEndDate?;
  deltaByHoursInStartTime?; //give valie in minus if you want to minus the hours
  deltaByHoursInEndTime?;
  deltaByMinuteInStartTime?;
  deltaByMinuteInEndTime?;
  deltaBySecondsInStartTime?;
  deltaBySecondsInEndTime?;
  eventTimeZone?;
}
export interface optionalParamsForEventCreation {
  noOfRooms?: number;
  noOfBooths?: number;
}

export interface EventSettings {
  title: string;
  deltaHours?: number;
  deltaDays?: number;
  isEventSetup?: boolean;
  isRegistration?: boolean;
  isVenueSetup?: boolean;
  isAccessGroups?: boolean;
  isCommunication?: boolean;
  tz?: string;
}
