import { APIRequestContext } from "@playwright/test";
import { UserProfileControllerInterface, profileDetailsPayload } from "../interfaces/UserProfileControllerInterface";

export class UserProfileController implements UserProfileControllerInterface {
    profileRequestContext: APIRequestContext;
    readonly eventId: any;

    constructor(profileRequestContext: APIRequestContext, eventId) {
        this.profileRequestContext = profileRequestContext;
        this.eventId = eventId;
    }


    async editProfileDetailsSettings(userProfileDetails: profileDetailsPayload): Promise<void> {
        const editProfileSettings = await this.profileRequestContext.post(`/api/user_privacy_setting/${this.eventId}/`,
            {
                data: userProfileDetails
            }
        );

        if (editProfileSettings.status() != 200) {
            throw new Error(
                `API call to edit profile settings failed with response ${editProfileSettings.status()}`
            );
        }
    }
}