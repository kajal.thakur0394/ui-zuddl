import test, { APIRequestContext, Page, expect } from "@playwright/test";
import * as path from "path";
import fs from "fs";
import { DataUtl } from "../util/dataUtil";
import EventRole from "../enums/eventRoleEnum";
import {
  getAllRegistrationIdOfEvent,
  inviteAttendeeByAPI,
  deleteAttendeeFromEvent,
} from "../util/apiUtil";
import { fetchMailasaurEmailObject } from "../util/emailUtil";
import { emailTemplateValidation } from "../util/emailTemplateValidation";
import { enableDisableEmailTrigger } from "../util/email-validation-api-util";
import EventSeriesCommunicationEnum from "../enums/EventSeriesCommunicationEnum";
import { error } from "console";
import { RegistrationFormController } from "./RegistrationFormController";

export class EventSeriesController {
  protected orgApiRequest: APIRequestContext;
  protected seriesId: string;

  constructor(orgApiRequestContext: APIRequestContext, seriesId: string) {
    this.orgApiRequest = orgApiRequestContext;
    this.seriesId = seriesId;
  }

  private async uploadThumbnail(thumbnailFilePath) {
    let thumbnailUploadEndpoint = `/api/file-upload/signed-put`;
    const file = path.resolve("test-data/", thumbnailFilePath);
    const image = fs.readFileSync(file);

    const response = await this.orgApiRequest.post(thumbnailUploadEndpoint, {
      headers: {
        Accept: "*/*",
        ContentType: "multipart/form-data",
      },
      multipart: {
        file: {
          name: file,
          mimeType: "image/jpeg",
          buffer: image,
        },
      },
    });
    const body = await response.json();

    if (response.status() != 200) {
      throw new Error(
        `API to upload thumbnail photo failed with ${response.status()}`
      );
    }

    let imageUrl = body["renderUrl"];

    console.log(`Image url -> ${imageUrl}`);

    return imageUrl;
  }

  async createSeries(seriesName: string, seriesThumbnailPath: string) {
    let createSeriesEndpoint = `/api/series/create`;

    const imageUrl = await this.uploadThumbnail(seriesThumbnailPath);

    let payload = {
      name: seriesName,
      thumbnailImageUrl: imageUrl,
      teamId: "",
    };

    const response = await this.orgApiRequest.post(createSeriesEndpoint, {
      data: payload,
    });

    const body = await response.json();

    if (response.status() != 200) {
      throw new Error(`API to create Series failed with ${response.status()}`);
    }

    let seriesId = body["eventId"];

    console.log(`Series Id -> ${seriesId}`);

    return seriesId;
  }

  async updateSeries(
    seriesName: string,
    seriesThumbnailPath: string,
    eventSeriesId: string
  ) {
    let createSeriesEndpoint = `/api/series/update`;

    const imageUrl = await this.uploadThumbnail(seriesThumbnailPath);

    let payload = {
      name: seriesName,
      thumbnailImageUrl: imageUrl,
      eventSeriesId: eventSeriesId,
    };

    const response = await this.orgApiRequest.patch(createSeriesEndpoint, {
      data: payload,
    });

    const body = await response.json();

    if (response.status() != 200) {
      throw new Error(`API to update Series failed with ${response.status()}`);
    }
  }

  async addAttendeesToSeries(listOfAttendeeEmail: Array<string>) {
    let totalAttendeeCount = listOfAttendeeEmail.length;

    for (let i = 0; i < totalAttendeeCount; i++) {
      await inviteAttendeeByAPI(
        this.orgApiRequest,
        this.seriesId,
        listOfAttendeeEmail[i],
        false,
        "Automation",
        `TestA${i + 1}`
      );
    }
  }

  async addAttendeeToEvent(eventId, emailId) {
    await inviteAttendeeByAPI(
      this.orgApiRequest,
      eventId,
      emailId,
      true,
      "Automation",
      `TestAttendee`
    );
  }

  async removeAttendeeFromSeries(attendeeEmailId: string) {
    await deleteAttendeeFromEvent(
      this.orgApiRequest,
      this.seriesId,
      attendeeEmailId
    );
  }

  async generateListOfRandomAttendees(
    numberOfAttendee: number
  ): Promise<Array<string>> {
    let emailPostfix = DataUtl.getApplicationTestDataObj()["mailosaurDomain"];

    let listOfAttendees = new Array<string>();

    for (let i = 0; i < numberOfAttendee; i++) {
      const currentDate = Date.now();
      const finalEmailAddress: string = `attendee${currentDate}${i}@${emailPostfix}`;

      listOfAttendees.push(finalEmailAddress);
    }

    console.log("List of email address generated ->\n");
    listOfAttendees.forEach((emailAddress) => {
      console.log(`${emailAddress} \n`);
    });

    return listOfAttendees;
  }

  async getRandomAttendeeEmail(): Promise<string> {
    return DataUtl.getRandomAttendeeMailosaurEmail();
  }

  async getRandomSpeakerEmail(): Promise<string> {
    return DataUtl.getRandomSpeakerMailosaurEmail();
  }

  async verifyAddEventEmailReceivedByUser(listOfEmailIds: Array<string>) {
    let emailTitle = "Event added to AutomationSeries";
    await this.verifyAddEmail(listOfEmailIds, emailTitle);
  }

  async verifyAddWebinarEmailReceivedByUser(listOfEmailIds: Array<string>) {
    let emailTitle = "Webinar added to AutomationSeries";
    await this.verifyAddEmail(listOfEmailIds, emailTitle);
  }

  private async verifyAddEmail(
    listOfEmailIds: Array<string>,
    emailTitle: string
  ) {
    let listSize = listOfEmailIds.length;

    for (var i = 0; i < listSize; i++) {
      let fetched_email = await fetchMailasaurEmailObject(
        listOfEmailIds[i],
        emailTitle
      );

      let emailValidationObject = new emailTemplateValidation(fetched_email);
      console.log(await emailValidationObject.fetchEventTitle());
    }
  }

  async verifyCalendarBlocksAreAttachedWithEmail(
    listOfEmailIds: Array<string>,
    expectedAttachmentName: any,
    webinar = false
  ) {
    console.log("Verfying caleder blocks");
    let emailTitle;

    if (webinar) emailTitle = "Webinar added to AutomationSeries";
    else emailTitle = "Event added to AutomationSeries";

    let emailListSize = listOfEmailIds.length;
    for (var i = 0; i < emailListSize; i++) {
      let fetched_email = await fetchMailasaurEmailObject(
        listOfEmailIds[i],
        emailTitle
      );

      let emailValidationObject = new emailTemplateValidation(fetched_email);
      let emailAttachments =
        await emailValidationObject.fetchEmailAttachments();

      let attachmentListSize = emailAttachments.length;
      let invitationAttachmentPresent = false;

      for (var j = 0; j < attachmentListSize; j++) {
        if (emailAttachments[j].fileName === expectedAttachmentName) {
          invitationAttachmentPresent = true;
          break;
        }
      }

      expect(invitationAttachmentPresent).toBe(true);
    }
  }

  // verifyLiveEventSideIsAccessibleForTheUser;
  async verifyLiveEventSideIsAccessibleForAttendee(
    magicLink,
    userPage: Page,
    eventTypeWebinar: boolean = false
  ) {
    await userPage.goto(magicLink);

    if (eventTypeWebinar === true) {
      let webinarLoadedLocator = userPage
        .frameLocator('iframe[title="Stage Studio"]')
        .locator("html");
      // .getByTestId("chatBoxContainer")
      // .locator("div")
      // .filter({ hasText: "Your Chatappears here" })
      // .nth(1);
      await expect(webinarLoadedLocator).toBeVisible({ timeout: 50000 });
    } else {
      let stageNavBar = userPage.getByTestId("navbar-menu-stage");
      await expect(stageNavBar).toBeVisible({ timeout: 50000 });
    }
  }

  async verifyLiveEventSideIsAccessibleForSpeaker(
    magicLink,
    userPage: Page,
    eventTypeWebinar: boolean = false
  ) {
    await userPage.goto(magicLink);

    if (eventTypeWebinar === true) {
      let webinarLoadedLocator = userPage.getByText(
        "Camera and mic checkFull nameTitle (Optional)Enter studio"
      );
      await expect(webinarLoadedLocator).toBeVisible({ timeout: 30000 });
    } else {
      let stageNavBar = userPage.getByTestId("navbar-menu-stage");
      await expect(stageNavBar).toBeVisible({ timeout: 15000 });
    }
  }

  async verifyMagicLinkNavigatesToRegistrationPage(magicLink, userPage) {
    let registrationButtonLocator = userPage.getByRole("button", {
      name: "Register",
    });

    await userPage.goto(magicLink);
    await expect(registrationButtonLocator).toBeVisible({ timeout: 8000 });
  }

  async getMagicLinkFromEventInvitationEmail(userEmailId) {
    let emailTitle = "Event Invite";

    let fetched_email = await fetchMailasaurEmailObject(
      userEmailId,
      emailTitle
    );

    let emailValidationObject = new emailTemplateValidation(fetched_email);
    let links = await emailValidationObject.fetchAllLinks();
    let magicLink: string = links[4];

    console.log(`Fetched magic link -> ${magicLink}`);

    return magicLink;
  }

  async getMagicLinkFromEventWebinarAddedEmail(
    userEmailId,
    isWebinar: boolean = false
  ) {
    let emailTitle = "Event added to";
    if (isWebinar === true) {
      emailTitle = "Webinar added to";
    }

    let fetched_email = await fetchMailasaurEmailObject(
      userEmailId,
      emailTitle
    );

    let emailValidationObject = new emailTemplateValidation(fetched_email);
    let links = await emailValidationObject.fetchAllLinks();
    let magicLink: string = links[0];

    console.log(`Fetched magic link -> ${magicLink}`);

    return magicLink;
  }

  async getMagicLinkFromSeriesInvitationEmail(
    userEmailId,
    isWebinar: boolean = false
  ) {
    let emailTitle = "Registration Invitation";

    let fetched_email = await fetchMailasaurEmailObject(
      userEmailId,
      emailTitle
    );

    let emailValidationObject = new emailTemplateValidation(fetched_email);
    let links = await emailValidationObject.fetchAllLinks();
    let magicLink: string = links[0];

    console.log(`Fetched magic link -> ${magicLink}`);

    return magicLink;
  }

  async verifyEventInviteEmailForAttendee(
    userEmailId: string,
    userPage: Page,
    eventTypeWebinar: boolean = false
  ) {
    let emailTitle = "Event Invite";

    let fetched_email = await fetchMailasaurEmailObject(
      userEmailId,
      emailTitle
    );

    let emailValidationObject = new emailTemplateValidation(fetched_email);
    let links = await emailValidationObject.fetchAllLinks();
    let magicLink = links[4];

    console.log(`Fetched magic link -> ${magicLink}`);

    await userPage.goto(magicLink);

    if (eventTypeWebinar === true) {
      let webinarLoadedLocator = userPage.getByText(
        "Camera and mic checkFull nameTitle (Optional)Enter studio"
      );
      await expect(webinarLoadedLocator).toBeVisible({ timeout: 8000 });
    } else {
      let stageNavBar = userPage.getByTestId("navbar-menu-stage");
      await expect(stageNavBar).toBeVisible({ timeout: 8000 });
    }
  }

  async verifyRegistrationConfirmationEmail(
    userEmailId: string,
    userPage: Page
  ) {
    let emailTitle = "Registration Confirmed";

    await fetchMailasaurEmailObject(userEmailId, emailTitle);
  }

  async changeCommunicationSettings(
    settingsToChange: EventSeriesCommunicationEnum,
    enableSettings: boolean
  ) {
    await enableDisableEmailTrigger(
      this.orgApiRequest,
      this.seriesId,
      settingsToChange,
      true,
      EventRole.ATTENDEE,
      enableSettings
    );
  }

  async verifySeriesAttendeesAreAdded(seriesId: string, eventId: string) {
    let seriesEmailIds = await this.getAttendeeList(seriesId);
    let eventEmailIds = await this.getAttendeeList(eventId);

    console.log("Series email ids ......");
    for (var i = 0; i < seriesEmailIds.length; i++) {
      console.log(`${seriesEmailIds[i]} \n`);
    }

    console.log("Event email ids ......");
    for (var i = 0; i < eventEmailIds.length; i++) {
      console.log(`${eventEmailIds[i]} \n`);
    }

    for (var i = 0; i < seriesEmailIds.length; i++) {
      if (!eventEmailIds.includes(seriesEmailIds[i])) {
        throw error(
          `Attendee - ${seriesEmailIds[i]} not added in Event attendees.`
        );
      }
    }
  }

  async verifySeriesAttendeeNotAdded(eventId: string, emailId: string) {
    let eventEmailIds = await this.getAttendeeList(eventId);

    console.log("Event email-ids ......");
    for (var i = 0; i < eventEmailIds.length; i++) {
      console.log(`${eventEmailIds[i]} \n`);
    }

    for (var i = 0; i < eventEmailIds.length; i++) {
      if (emailId.includes(eventEmailIds[i])) {
        throw error(
          `Attendee - ${eventEmailIds[i]} is added in Event/Webinar attendees.`
        );
      }
    }
  }

  async getAttendeeList(eventId: string) {
    let seriesJson = await getAllRegistrationIdOfEvent(
      this.orgApiRequest,
      eventId
    );

    let listOfEmails: Array<string> = new Array<string>();
    let length = await seriesJson.length;

    for (var i = 0; i < length; i++) {
      let json = await seriesJson[i];
      const emailId = await json["email"];
      console.log(`Email id -> ${emailId} \n`);
      listOfEmails.push(emailId);
    }

    return listOfEmails;
  }

  async updateCheckInTime(
    eventId,
    chekinEnableTimeBeforeEventStartsInHours: number = 2,
    checkDisableTimeAfterEventEndsInHours: number = 2
  ) {
    let registrationFormController = new RegistrationFormController(
      this.orgApiRequest,
      eventId
    );

    let informationTabViewRespJson =
      await registrationFormController.getInformationTabView();
    let landingPageId = await this.getEventLandingPageId();

    await registrationFormController.updateInformationTabView({
      eventLandingPageId: landingPageId,
      eventId: eventId,
      checkInEnableTime: chekinEnableTimeBeforeEventStartsInHours * 60,
      checkInDisableTime: checkDisableTimeAfterEventEndsInHours * 60,
      eventLandingPageDisclaimers: [],
      registrationCustomFields: [],
      hasRegistrationsStarted: await informationTabViewRespJson[
        "hasRegistrationsStarted"
      ],
      eventEntryType: await informationTabViewRespJson["eventEntryType"],
    });
  }

  async addCustomFieldsWithConditionToEvent(listOfCustomFieldsToAdd) {
    let testDataObject;
    let informationTabViewRespJson;
    let informationTabResponseWithCustomFields;
    let landingPageId = await this.getEventLandingPageId();

    await test.step("Make a copy of the test data object passed to this functon.", async () => {
      testDataObject = JSON.parse(JSON.stringify(listOfCustomFieldsToAdd));
      console.log(`#### Fetching the event information tab response #### `);
      informationTabViewRespJson = await (
        await this.getInformationTabView()
      ).json();
    });

    await test.step("Modify payload data to add series id and landing page id for this event.", async () => {
      for (const eachCustomFieldObj of testDataObject) {
        eachCustomFieldObj.eventId = this.seriesId;
        eachCustomFieldObj.eventLandingPageId = landingPageId;
      }
    });

    await test.step("Replace the reg custom field object of fetched information tab response with the test data obj.", async () => {
      informationTabViewRespJson.registrationCustomFields = testDataObject;
    });

    await test.step("Post the information tab response with updated payload with custom fields.", async () => {
      console.log("Registration payload ->", informationTabViewRespJson);
      await this.updateInformationTabView(informationTabViewRespJson);
    });

    await test.step("Wait until the get call to information tab starts showing the custom fields.", async () => {
      await expect(async () => {
        const informationTabRespWithCustomFields =
          await this.getInformationTabView();
        const informationTabRespWithCustomFieldsJson =
          await informationTabRespWithCustomFields.json();
        const listOfcustomFieldsObject =
          informationTabRespWithCustomFieldsJson.registrationCustomFields;

        expect(
          listOfcustomFieldsObject.length,
          "expecting length of list of custom fields object to be > 0"
        ).not.toBe(0);

        informationTabResponseWithCustomFields =
          informationTabRespWithCustomFieldsJson;
      }, "waiting for information tab response to show updated custom fields").toPass();
    });

    await test.step("Apply conditions to the fields who have condition object in test data.", async () => {
      for (const eachCustomFieldObj of testDataObject) {
        let thisFieldName = eachCustomFieldObj["fieldName"];
        let thisFieldLabel = eachCustomFieldObj["labelName"];
        //fetch the reg custom feild id of this field

        let thisFieldCustomFieldId =
          await this.fetchRegistrationCustomFieldIdWithLabel(
            thisFieldLabel,
            informationTabResponseWithCustomFields
          );

        //checking if this field is conditional (if len conditional obj > 0)
        let listOfConditionalFieldsObject =
          eachCustomFieldObj["customFieldConditions"];
        if (listOfConditionalFieldsObject.length > 0) {
          console.log(`this field ${thisFieldName} have conditional objects`);

          // now apply conditional to this field
          for (const eachConditionObject of listOfConditionalFieldsObject) {
            const dependentFieldLabel =
              eachConditionObject["conditionCustomFieldId"];
            // finding the dependent field custom id
            let dependentFieldCustomFieldId =
              await this.fetchRegistrationCustomFieldIdWithLabel(
                dependentFieldLabel,
                informationTabResponseWithCustomFields
              );
            // now replace the label with id in conditional field object
            eachConditionObject["conditionCustomFieldId"] =
              dependentFieldCustomFieldId;
          }
          // now you have the updated list of conditionalFields Object
          await this.hitPostApiToApplyConditions(
            listOfConditionalFieldsObject,
            thisFieldCustomFieldId
          );
        }
      }
    });
  }

  async getInformationTabView() {
    console.log("####### getting information tab response #######");
    let landingPageId = await this.getEventLandingPageId();
    console.log(`####### landing page id ${landingPageId} #######`);
    console.log(`####### Series id ${this.seriesId} #######`);

    let getInformationTabViewResp = await this.orgApiRequest.get(
      `/api/event/landing_page/${this.seriesId}/${landingPageId}/information_tab_view`
    );

    if (getInformationTabViewResp.status() != 200) {
      throw new Error(
        `API to fetch information tab view failed with ${getInformationTabViewResp.status()}`
      );
    }
    return getInformationTabViewResp;
  }

  async updateInformationTabView(payLoadToUpdateAsJson) {
    let landingPageId = await this.getEventLandingPageId();
    let postInformationTabViewResp = await this.orgApiRequest.post(
      `/api/event/landing_page/${this.seriesId}/${landingPageId}/information_tab_view`,
      {
        data: payLoadToUpdateAsJson,
      }
    );

    if (postInformationTabViewResp.status() != 200) {
      throw new Error(
        `API to add custom fields in information tab view failed with ${postInformationTabViewResp.status()}`
      );
    }
  }

  async fetchRegistrationCustomFieldIdWithLabel(
    fieldLabel,
    informationTabResponseJson
  ) {
    const listOfRegistrationFields =
      informationTabResponseJson.registrationCustomFields;
    if (listOfRegistrationFields.length == 0) {
      throw new Error(
        `recieved informatin tab response does not have any custom field`
      );
    }
    for (const eachRegistrationCustomFieldObj of listOfRegistrationFields) {
      const currentFieldLabel =
        eachRegistrationCustomFieldObj["labelName"].toLowerCase();
      if (currentFieldLabel == fieldLabel.toLowerCase()) {
        let registrationCustomFieldId =
          eachRegistrationCustomFieldObj["registrationCustomFieldId"];
        console.log(
          `field with label ${fieldLabel} found in the informaiton tab response with id : ${registrationCustomFieldId}`
        );
        return registrationCustomFieldId;
      }
    }
  }

  async hitPostApiToApplyConditions(
    listOfConditionsObject,
    registrationFieldCustomId
  ) {
    const applyConditionEndpoint = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/api/event/landing_page/${
      this.seriesId
    }/${registrationFieldCustomId}/field_conditions`;

    const applyConditionApiResp = await this.orgApiRequest.post(
      applyConditionEndpoint,
      {
        data: listOfConditionsObject,
      }
    );
    if (applyConditionApiResp.status() != 200) {
      throw new Error(`Post API call to apply condition on field ${registrationFieldCustomId} failed with 
      ${applyConditionApiResp.status()}`);
    }
  }

  private async getEventLandingPageId() {
    console.log(
      `Getting series landing page details by API for event id ${this.seriesId}`
    );

    let eventLandingPageResJson = await (
      await this.getEventLandingPageDetails()
    ).json();

    let eventLandingPageId = eventLandingPageResJson.eventLandingPageId;
    console.log(`the series landing page id is ${eventLandingPageId}`);

    return eventLandingPageId;
  }

  private async getEventLandingPageDetails() {
    console.log(
      `Getting series landing page details by API for event id ${this.seriesId}`
    );

    const eventLandingPageResp = await this.orgApiRequest.get(
      `/api/event/landing_page/${this.seriesId}`
    );

    if (eventLandingPageResp.status() != 200) {
      throw new Error(
        `API to fetch event landing page details failed with ${eventLandingPageResp.status()}`
      );
    }

    return eventLandingPageResp;
  }
}
