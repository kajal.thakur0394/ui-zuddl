import { APIRequestContext, APIResponse, test } from "@playwright/test";
import { MemberRolesEnum } from "../enums/MemberRolesEnum";
import {
  TeamControllerInterface,
  TeamMemberInfoDto,
  addMemberInATeam,
  addMemberInMultipleTeams,
  addMemberToEventsTeam,
} from "../interfaces/TeamControllerInterface";

export class TeamController implements TeamControllerInterface {
  readonly orgApiContext: APIRequestContext;
  readonly organisationId: string;
  readonly teamId?: string;
  constructor(
    orgApiContext: APIRequestContext,
    organisationId: string,
    teamId?: string
  ) {
    this.orgApiContext = orgApiContext;
    this.organisationId = organisationId;
    this.teamId = teamId;
  }

  async addExistingOrganisationMemberToTeam(
    memberEmail: string
  ): Promise<void> {
    throw new Error("Method not implemented.");
  }

  async updateTeamName(newNameString: string) {
    throw new Error("Method not implemented.");
  }

  async getTeamDetailsByApi(): Promise<APIResponse> {
    throw new Error("Method not implemented.");
  }

  async getListOfEventsForThisTeam(): Promise<[]> {
    throw new Error("Method not implemented.");
  }

  async getListOfMembersInThisTeam(): Promise<[TeamMemberInfoDto]> {
    throw new Error("Method not implemented.");
  }

  async isMemberPartOfThisTeam(memberEmail: string): Promise<boolean> {
    throw new Error("Method not implemented.");
  }

  async getTeamMemberInfo(memberEmail: string): Promise<TeamMemberInfoDto> {
    throw new Error("Method not implemented.");
  }

  async addNewMemberToEvents({
    firstName,
    lastName,
    email,
    studioLicenseRole = "ORGANIZER",
    studioPlan = "Custom",
    eventPlan = "Custom",
    eventLicensesHeading,
    studioLicensesHeading,
    eventLicenseRole,
    eventProductRole,
  }: addMemberToEventsTeam) {
    let payload = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      studioLicenseRole: studioLicenseRole,
      studioPlan: studioPlan,
      eventPlan: eventPlan,
      eventLicensesHeading: "0 license  left",
      studioLicensesHeading: "168 licenses  left",
      eventLicenseRole: eventLicenseRole,
      eventProductRole: eventProductRole,
    };

    await test.step(`Adding new member with email ${email} to events team`, async () => {
      const addMemberResp = await this.orgApiContext.post(
        "api/core/organization/add-member",
        {
          data: payload,
        }
      );
      if (addMemberResp.status() != 200) {
        throw new Error(
          `API to add new member to events team failed with ${addMemberResp.status()}`
        );
      }
      return addMemberResp;
    });
  }

  async addNewMemberToTeam(addMemberInATeamObject: addMemberInATeam) {
    const addMemberResp = await this.orgApiContext.post(
      "api/core/organization/team-member/add",
      {
        data: addMemberInATeamObject,
      }
    );
    if (addMemberResp.status() != 200) {
      throw new Error(
        `API to add new member to team the failed with ${addMemberResp.status()}`
      );
    }
  }

  async fetchListOfTeamsAssignedToAMember(email: string) {
    let myTeamList: any;
    let teamMap = new Map<string, string>();
    const getAllTeamsAssignedToMemberResp = await this.orgApiContext.get(
      `/api/core/organization/member`
    );
    if (getAllTeamsAssignedToMemberResp.status() != 200) {
      throw new Error(
        `API call to get all teams list assigned to a member failed with status ${getAllTeamsAssignedToMemberResp.status()}`
      );
    }
    const getAllTeamsAssignedToMemberRespJson = await getAllTeamsAssignedToMemberResp.json();
    let memberList = getAllTeamsAssignedToMemberRespJson["memberDtoList"];
    for (const member of memberList) {
      if (member.email == email) {
        myTeamList = member["myTeamList"];
        break;
      }
    }
    for (const team of myTeamList) {
      teamMap.set(team.teamId, team.teamName);
    }
    return teamMap;
  }

  async verifyTeamsDetails(expectedTeamListMap: Map<string, string>, actualTeamListMap: Map<string, string>): Promise<boolean> {
    // Convert maps to arrays of key-value pairs
    const arr1 = Array.from(expectedTeamListMap.entries());
    const arr2 = Array.from(actualTeamListMap.entries());

    // If the arrays have different lengths, the maps are not equal
    if (arr1.length !== arr2.length) {
      return false;
    }

    // Sort the arrays by key to ensure consistent comparison
    arr1.sort(([key1], [key2]) => key1.localeCompare(key2));
    arr2.sort(([key1], [key2]) => key1.localeCompare(key2));

    // Compare each key-value pair in the sorted arrays
    for (let i = 0; i < arr1.length; i++) {
      const [key1, value1] = arr1[i];
      const [key2, value2] = arr2[i];

      // If any key or value is different, the maps are not equal
      if (key1 !== key2 || value1 !== value2) {
        return false;
      }
    }
    return true;
  }

  async addNewMemberToStudio() { }

  async addNewMemberToStudioAndEvents() { }

  async fetchteamListFromOrganisation() {
    const getAllTeamsResp = await this.orgApiContext.get(
      `/api/core/organization/team`
    );
    if (getAllTeamsResp.status() != 200) {
      throw new Error(
        `API call to get all teams list failed with status ${getAllTeamsResp.status()}`
      );
    }
    const getAllTeamsRespJson = await getAllTeamsResp.json();
    return getAllTeamsRespJson["myTeams"];
  }

  //fetch either general teamObject or any teamObject with particular teamId
  async fetchTeam(teamId?: string) {
    const getAllTeamsRespJson = await this.fetchteamListFromOrganisation();
    for (const team of getAllTeamsRespJson) {
      if (team["teamId"] === teamId || team["general"] === true) {
        return team;
      }
    }
  }

  async fetchGeneralTeamId() {
    const getTeamResp = await this.fetchTeam();
    //let teamResponJson = await getTeamResp.json();
    return getTeamResp["teamId"];
  }

  async fetchTeamId(teamName: string) {
    const getAllTeamsRespJson = await this.fetchteamListFromOrganisation();
    for (const team of getAllTeamsRespJson) {
      if (team.teamName === teamName) {
        return team["teamId"];
      }
    }
  }

  async fetchTeamMembersInsideATeam(teamId: string) {
    const getAllTeamMembersResponse = await this.orgApiContext.get(
      `api/core/organization/member/get-team-member/${teamId}`
    );
    if (getAllTeamMembersResponse.status() != 200) {
      throw new Error(
        `API call to get all members in ${teamId} failed with status ${getAllTeamMembersResponse.status()}`
      );
    }
    const getAllTeamMembersResponseJson =
      await getAllTeamMembersResponse.json();
    return getAllTeamMembersResponseJson;
  }

  async fetchRoleOfAMember(teamId: string, email: string, roleType: string) {
    const getAllTeamMembersResponseJson =
      await this.fetchTeamMembersInsideATeam(teamId);
    for (const member of getAllTeamMembersResponseJson) {
      if (member["email"] === email) {
        return member[roleType];
      }
    }
  }

  async fetchOrgMemberIdOfAMember(teamId: string, email: string) {
    const getAllTeamMembersResponseJson =
      await this.fetchTeamMembersInsideATeam(teamId);
    for (const member of getAllTeamMembersResponseJson) {
      if (member["email"] === email) {
        return member["orgMemberId"];
      }
    }
  }

  async addOrDeleteMemberInMultipleTeams(addMemberInMultipleTeamsObject: addMemberInMultipleTeams): Promise<void> {
    const addMemberResp = await this.orgApiContext.post(
      `/api/core/organization/team-member/bulk-remove-add`,
      {
        data: addMemberInMultipleTeamsObject
      }
    );
    if (addMemberResp.status() != 200) {
      throw new Error(
        `API call to add a member in multiple teams failed with status ${addMemberResp.status()}`
      );
    }

  }

  async updateMemberRoleInOrganization(
    teamId: string,
    email: string,
    role: MemberRolesEnum
  ) {
    const getAllTeamMembersResponseJson =
      await this.fetchTeamMembersInsideATeam(teamId);
    let orgMemberId: string;
    for (const member of getAllTeamMembersResponseJson) {
      if (member["email"] === email) {
        orgMemberId = member["orgMemberId"];
        break;
      }
    }
    const updateMemberRole = await this.orgApiContext.patch(
      `api/core/organization/member/${orgMemberId}/org/${role}`
    );
    if (updateMemberRole.status() != 200) {
      throw new Error(
        `API call to update role of ${email} to ${role} failed with status ${updateMemberRole.status()}`
      );
    }
  }

  async verifyMemberToBePresentInATeam(teamId: string, email: string): Promise<boolean> {
    const getAllTeamMembersResponse = await this.fetchTeamMembersInsideATeam(teamId);
    for (const member of getAllTeamMembersResponse) {
      if (member.email == email) {
        return true;
      }
    }
    return false;
  }

  async verifyOwnerRoleChangesToAdmin() {
    const getMemberDetails = await this.orgApiContext.get(
      `api/core/organization/member`
    );
    if (getMemberDetails.status() != 200) {
      throw new Error(
        `API call to get all member list failed with status ${getMemberDetails.status()}`
      );
    }
    const getAllMemberDetails = await getMemberDetails.json();
    const getAllMemberList = getAllMemberDetails.memberDtoList;
    for(let idx in getAllMemberList){
      if(getAllMemberList[idx].lastName=='OwnerQA' && getAllMemberList[idx].organizationRole=='ADMIN'){
        return true
      }
    }
    return false;
  }

  async changeRoleFromOrganizerToModerator() {
    let account_id:string;
    const getMemberDetails = await this.orgApiContext.get(
      `api/core/organization/member`
    );
    if (getMemberDetails.status() != 200) {
      throw new Error(
        `API call to get all member list failed with status ${getMemberDetails.status()}`
      );
    }
    const getAllMemberDetails = await getMemberDetails.json();
    const getAllMemberList = getAllMemberDetails.memberDtoList;
    for(let idx in getAllMemberList){
      if(getAllMemberList[idx].organizationRole=='OWNER'){
        account_id=getAllMemberList[idx].id;
      }
    }
    const updateMemberRole = await this.orgApiContext.patch(
      `api/core/organization/member/${account_id}/product/MODERATOR`
    );
    if (updateMemberRole.status() != 200) {
      throw new Error(
        `API call to update role failed with status ${updateMemberRole.status()}`
      );
    }
  }
}
