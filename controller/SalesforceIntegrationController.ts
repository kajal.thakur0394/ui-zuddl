import { APIRequestContext, expect } from "@playwright/test";
import { SalesforceIntegrationDirectionType } from "../enums/SalesforceIntegrationDirectionEnum";
import {
  IntegrationControllerInterface,
  salesforceFieldMappingPaylod,
} from "../interfaces/IntegrationControllerInterface";
import { DataUtl } from "../util/dataUtil";

export class SalesforceEventIntegrationController
  implements IntegrationControllerInterface
{
  readonly eventId;
  readonly orgApiRequestContext: APIRequestContext;
  constructor(orgApiRequestContext, eventId) {
    this.eventId = eventId;
    this.orgApiRequestContext = orgApiRequestContext;
  }

  //update campagin ID for event integration
  async enableSalesforceIntegrationForThisEventWithCampaign(
    campaginId: string,
    integrationDirectionType: SalesforceIntegrationDirectionType = SalesforceIntegrationDirectionType.PUSH
  ) {
    const updateSalesforceIntegrationSettingsResp2 = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/api/integration/${this.eventId}/update-integration-setting`;
    const updateSalesforceIntegrationSettingsResp =
      await this.orgApiRequestContext.post(
        updateSalesforceIntegrationSettingsResp2,
        {
          data: {
            integrationType: "SALESFORCE",
            eventIntegrationId: campaginId,
            integrationDirectionType: integrationDirectionType,
            status: true,
            shouldSyncData: true,
          },
        }
      );

    if (updateSalesforceIntegrationSettingsResp.status() != 200) {
      throw new Error(`API call to update salesforce integration setting for campaign id ${campaginId} failed with
         ${updateSalesforceIntegrationSettingsResp.status()}`);
    }
  }

  //update campagin ID for event integration
  async disableSalesforceIntegrationForThisEventWithCampaign(
    campaginId: string,
    integrationDirectionType: SalesforceIntegrationDirectionType = SalesforceIntegrationDirectionType.PUSH
  ) {
    const updateSalesforceIntegrationSettingsResp =
      await this.orgApiRequestContext.post(
        `api/integration/${this.eventId}/update-integration-setting`,
        {
          data: {
            integrationType: "SALESFORCE",
            integrationDirectionType: integrationDirectionType,
            status: false,
          },
        }
      );

    if (updateSalesforceIntegrationSettingsResp.status() != 200) {
      throw new Error(`API call to update salesforce integration setting for campaign id ${campaginId} failed with
           ${updateSalesforceIntegrationSettingsResp.status()}`);
    }
  }

  async getSalesForceEventPushIntegrationId(campaginId: string) {
    let pushIntegrationId;
    const getPushIntegrationDataResp = await this.orgApiRequestContext.get(
      `/api/integration/${this.eventId}/integration-setting/PUSH`
    );
    if (getPushIntegrationDataResp.status() != 200) {
      throw new Error(
        `API call to fetch push integration Id failed with ${getPushIntegrationDataResp.status()}`
      );
    }
    //fetch the id
    const getPushIntegrationDataRespJson =
      await getPushIntegrationDataResp.json();
    for (const integrationDataObj of getPushIntegrationDataRespJson) {
      const campaignIdFetched = integrationDataObj["eventIntegrationId"];
      if (campaignIdFetched == campaginId) {
        pushIntegrationId = integrationDataObj["id"];
        break;
      }
    }
    return pushIntegrationId!;
  }

  async getSalesForceEventPullIntegrationId(campaginId: string) {
    let pullIntegrationId;
    const getPullIntegrationDataResp = await this.orgApiRequestContext.get(
      `/api/integration/${this.eventId}/integration-setting/PULL`
    );
    if (getPullIntegrationDataResp.status() != 200) {
      throw new Error(
        `API call to fetch pull integration Id failed with ${getPullIntegrationDataResp.status()}`
      );
    }
    //fetch the id
    const getPullIntegrationDataRespJson =
      await getPullIntegrationDataResp.json();
    for (const integrationDataObj of getPullIntegrationDataRespJson) {
      const campaignIdFetched = integrationDataObj["eventIntegrationId"];
      if (campaignIdFetched == campaginId) {
        pullIntegrationId = integrationDataObj["id"];
        break;
      }
    }
    return pullIntegrationId!;
  }

  async syncExistingRecords() {
    const syncExistingRecordApiResp2 = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/api/integration/${this.eventId}/sync-existing-detail/SALESFORCE`;
    const syncExistingRecordApiResp = await this.orgApiRequestContext.post(
      syncExistingRecordApiResp2
      //   `/api/integration/${this.eventId}/sync-existing-detail/SALESFORCE`
    );
    if (syncExistingRecordApiResp.status() != 200) {
      throw new Error(
        `API call to trigger existing records sync failed with ${syncExistingRecordApiResp.status()}`
      );
    }
  }

  async getRegisterUserDataFromTray(campaginId: string): Promise<[JSON]> {
    //the token gets expired. Connect with developer to
    const getRegisterUserDataFromTrayResp =
      await this.orgApiRequestContext.post(
        `https://dba5743b-d3fa-4d05-8914-ab7518d0d7c5.trayapp.io`,
        {
          data: {
            eventIntegrationId: campaginId,
          },
        }
      );
    if (getRegisterUserDataFromTrayResp.status() != 200) {
      throw new Error(
        `API call to fetch registered users data failed with ${getRegisterUserDataFromTrayResp.status()}`
      );
    }
    //fetching the result object from the response object
    const registerUserDataFromTrayRespJson =
      await getRegisterUserDataFromTrayResp.json();
    return registerUserDataFromTrayRespJson; //return list of records
  }

  async mapZuddlFieldsWithSalesforceFields({
    customFieldMapping,
    customPolicyMapping,
    secondaryCustomFieldMapping,
    secondaryCustomPolicyMapping,
    tertiaryCustomFieldMapping,
    tertiaryCustomPolicyMapping,
  }: salesforceFieldMappingPaylod) {
    const updateIntegrationMappingResp2 = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/api/core/integration/SALESFORCE/mapping/fields`;
    const updateIntegrationMappingResp = await this.orgApiRequestContext.post(
      updateIntegrationMappingResp2,
      {
        data: {
          eventId: this.eventId,
          customFieldMapping: customFieldMapping,
          customPolicyMapping: customPolicyMapping,
          secondaryCustomFieldMapping: secondaryCustomFieldMapping,
          secondaryCustomPolicyMapping: secondaryCustomPolicyMapping,
          tertiaryCustomFieldMapping: tertiaryCustomFieldMapping,
          tertiaryCustomPolicyMapping: tertiaryCustomPolicyMapping,
        },
      }
    );
    if (updateIntegrationMappingResp.status() != 200) {
      throw new Error(
        `API call to update integration mapping failed with status ${updateIntegrationMappingResp.status()}`
      );
    }
  }

  async getLeadCustomFieldsMappingOptionsFromSalesforce() {}
  async getLeadContactCustomFieldsMappingOptionsFromSalesforce() {}

  async getCampaignMemberCustomFieldsMappingOptionsFromSalesforce() {}

  async verifyRecordsFromTray(
    listOfRegisteredRecordFromTray: [JSON],
    attendeeEmail: string,
    isUserFoundInTray: boolean,
    userRecordFromTray: JSON
  ) {
    for (const eachRecord of listOfRegisteredRecordFromTray) {
      const userEmail = eachRecord["Email"];
      if (userEmail == attendeeEmail) {
        console.log(`Record found for ${userEmail} in tray`);
        isUserFoundInTray = true;
        userRecordFromTray = eachRecord;
        break;
      }
    }
    expect(
      isUserFoundInTray,
      `expecting user email ${attendeeEmail} to be found in tray `
    ).toBe(true);
    return userRecordFromTray;
  }

  async verifyCustomFieldDeletion(tabNumber: number) {
    const getCustomFieldDataForEvent = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/api/core/integration/SALESFORCE/mapping/fields?eventId=${this.eventId}`;

    const getCustomFieldResponse = await this.orgApiRequestContext.get(
      getCustomFieldDataForEvent
    );

    if (getCustomFieldResponse.status() != 200) {
      throw new Error(
        `API call to fetch pull integration Id failed with ${getCustomFieldResponse.status()}`
      );
    }
    let customFieldObj = await getCustomFieldResponse.json();
    let customFieldData = {};
    if (tabNumber === 1) {
      customFieldData = customFieldObj.customFieldMapping;
    } else if (tabNumber === 2) {
      customFieldData = customFieldObj.secondaryCustomFieldMapping;
    }
    let length = Object.keys(customFieldData).length;
    let checkDeletedData = true;
    if (length == 4) {
      for (const key in customFieldData) {
        if (
          customFieldData.hasOwnProperty(key) &&
          key == "country" &&
          customFieldData[key] == "Country"
        ) {
          checkDeletedData = false;
        }
      }
    } else {
      checkDeletedData = false;
    }
    expect(
      checkDeletedData,
      `expecting Country field should not present in the custom Field`
    ).toBe(true);
  }

  async getLeadDataRegisteredFromTray(email: string): Promise<[JSON]> {
    //the token gets expired. Connect with developer to
    const getRegisterUserDataFromTrayResp =
      await this.orgApiRequestContext.post(
        `https://c0a96cc4-5967-44b8-901e-75ebd2db8d78.trayapp.io`,
        {
          data: {
            email: email,
          },
        }
      );
    if (getRegisterUserDataFromTrayResp.status() != 200) {
      throw new Error(
        `API call to fetch registered users data failed with ${getRegisterUserDataFromTrayResp.status()}`
      );
    }
    //fetching the result object from the response object
    return await getRegisterUserDataFromTrayResp.json();
  }
}
