import { APIRequestContext } from "@playwright/test";
import integrationTYpeEnum from "../enums/integrationTYpeEnum";
import {
  IntegrationControllerInterface,
  fieldMappingPayLoad,
} from "../interfaces/IntegrationControllerInterface";

export class IntegrationController implements IntegrationControllerInterface {
  readonly orgApiRequestContext: APIRequestContext;

  constructor(orgApiRequestContext: APIRequestContext) {
    this.orgApiRequestContext = orgApiRequestContext;
  }

  async addOrganisationMappingFields(
    integrationType: integrationTYpeEnum,
    payload: fieldMappingPayLoad
  ) {
    let addFieldsMappingToOrganisation = await this.orgApiRequestContext.post(
      `/api/core/integration/${integrationType}/organization/mapping/fields`,
      {
        data: payload,
      }
    );
    if (addFieldsMappingToOrganisation.status() != 200) {
      throw new Error(
        `API call to add fields in ${integrationType} integration failed with status ${addFieldsMappingToOrganisation.status()}`
      );
    }
  }

  async getOrganisationMappingFieldResponse(
    integrationType: integrationTYpeEnum
  ) {
    let getFieldsMapping = await this.orgApiRequestContext.get(
      `/api/core/integration/${integrationType}/organization/mapping/fields`
    );

    if (getFieldsMapping.status() != 200) {
      throw new Error(
        `API call to get field mapping in ${integrationType} integration failed with status ${getFieldsMapping.status()}`
      );
    }
    let getFieldsMappingResponseJson = await getFieldsMapping.json();
    return getFieldsMappingResponseJson;
  }

  async getOrganizationFieldMappingObject(
    integrationType: integrationTYpeEnum
  ) {
    let getFieldsMappingResponseJson =
      await this.getOrganisationMappingFieldResponse(integrationType);
    let organizationFieldMappingDataObject = await getFieldsMappingResponseJson[
      "organizationFieldMapping"
    ];
    return organizationFieldMappingDataObject;
  }

  async getOrganizationPolicyMappingObject(
    integrationType: integrationTYpeEnum
  ) {
    let getFieldsMappingResponseJson =
      await this.getOrganisationMappingFieldResponse(integrationType);
    let organizationPolicyMappingDataObject =
      await getFieldsMappingResponseJson["organizationPolicyMapping"];
    return organizationPolicyMappingDataObject;
  }

  async removeFieldKey(
    payload: fieldMappingPayLoad,
    fieldNameList: string[]
  ): Promise<fieldMappingPayLoad> {
    for (let fieldName of fieldNameList) {
      if (payload.organizationFieldMapping[fieldName]) {
        delete payload.organizationFieldMapping[fieldName];
      } else {
        //fieldName is not present
      }
      fieldName =
        fieldName.charAt(0).toUpperCase() + fieldName.slice(1).toLowerCase();
      if (payload.organizationPolicyMapping[fieldName]) {
        delete payload.organizationPolicyMapping[fieldName];
      }
    }
    return payload;
  }

  async deleteOrganisationMappingFields(
    payload: fieldMappingPayLoad,
    fieldName: string[],
    integrationType: integrationTYpeEnum
  ) {
    // Call the function with your payload
    let updatedPayload = await this.removeFieldKey(payload, fieldName);
    console.log(updatedPayload);
    await this.addOrganisationMappingFields(integrationType, updatedPayload);
  }

  async deleteOrgKeys(data) {
    for (const key in data) {
      if (
        Object.prototype.hasOwnProperty.call(data, key) &&
        key.toLowerCase().includes("email")
      ) {
        continue; // Skip the email key
      }
      delete data[key];
    }

    console.log(data);
  }
}
