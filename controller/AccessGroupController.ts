import { APIRequestContext, expect } from "@playwright/test";
import { Roles } from "../enums/AccessControlEnum";
import { inspect } from "util";

export class AccessGroupController {
  private orgApiRequestContext: APIRequestContext;
  private eventId;
  constructor(orgApiRquestContext: APIRequestContext, eventId) {
    this.orgApiRequestContext = orgApiRquestContext;
    this.eventId = eventId;
  }

  async handleOnboardingChecksForAttendee(enableOnBoardingChecks = false) {
    let attendeeGroupId = await this.fetchAccessGroupId("Attendees");
    let featureId = await this.fetchFeatureIdForGroup(
      "ON_BOARDING_CHECKS",
      attendeeGroupId
    );
    let getAccessGroupDetail = `/api/access_groups/${this.eventId}/${attendeeGroupId}`;

    const patchPayload = {
      id: featureId,
      isActive: enableOnBoardingChecks,
      isStageBreakoutRoomActive: null,
    };
    const updateFeatureStatusForAccessGroupResp =
      await this.orgApiRequestContext.patch(getAccessGroupDetail, {
        data: [patchPayload],
      });
    if (updateFeatureStatusForAccessGroupResp.status() != 200) {
      throw new Error(
        `Patch API call to make onboarding status as ${enableOnBoardingChecks} failed with ${updateFeatureStatusForAccessGroupResp.status()}`
      );
    }
  }

  async getFeaturesJson(groupId: string | number) {
    const getFeaturesUri = `/api/access_groups/${this.eventId}/${groupId}`;
    let response = await this.orgApiRequestContext.get(getFeaturesUri);

    if (!response.ok()) {
      throw new Error(
        `API to get list of features failed with response -> ${response.status()}`
      );
    }

    const responseJson = await response.json();
    const featureList = responseJson["features"];
    return featureList;
  }

  async handleOnboardingChecksForSpeaker(enableOnBoardingChecks = false) {
    let speakerGroupId = await this.fetchAccessGroupId("Speakers");
    let featureId = await this.fetchFeatureIdForGroup(
      "ON_BOARDING_CHECKS",
      speakerGroupId
    );
    let getAccessGroupDetail = `/api/access_groups/${this.eventId}/${speakerGroupId}`;

    const patchPayload = {
      id: featureId,
      isActive: enableOnBoardingChecks,
      isStageBreakoutRoomActive: null,
    };
    const updateFeatureStatusForAccessGroupResp =
      await this.orgApiRequestContext.patch(getAccessGroupDetail, {
        data: [patchPayload],
      });
    if (updateFeatureStatusForAccessGroupResp.status() != 200) {
      throw new Error(
        `Patch API call to make onboarding status as ${enableOnBoardingChecks} failed with ${updateFeatureStatusForAccessGroupResp.status()}`
      );
    }
  }

  async fetchFeatureIdForGroup(featureName: string, groupId) {
    let featureId;
    let getAccessGroupDetail = `/api/access_groups/${this.eventId}/${groupId}`;
    let accessGroupDetailResp = await this.orgApiRequestContext.get(
      getAccessGroupDetail
    );
    if (accessGroupDetailResp.status() != 200) {
      throw new Error(
        `API to fetch access group details of group id ${groupId} failed with ${accessGroupDetailResp.status()}`
      );
    }
    const accessGroupDetailRespJson = await accessGroupDetailResp.json();
    const listOfFeatures = accessGroupDetailRespJson["features"];
    for (const eachFeatureObj of listOfFeatures) {
      console.log(eachFeatureObj);
      if (eachFeatureObj["featureName"] == featureName) {
        featureId = eachFeatureObj["id"];
        break;
      }
    }
    return featureId;
  }

  async fetchAccessGroupId(accessGroupName: string | Roles) {
    let accessGroupId;
    let getAllAccessGroupsForEvents = `/api/access_groups/${this.eventId}/groups`;
    let accessGroupDetailResp = await this.orgApiRequestContext.get(
      getAllAccessGroupsForEvents
    );

    if (!accessGroupDetailResp.ok()) {
      throw new Error(
        `API to fetch access group details failed with response -> ${accessGroupDetailResp.status()}`
      );
    }
    console.log(
      "Group Id response -> ",
      inspect(accessGroupDetailResp, { depth: null })
    );

    const accessGroupDetailRespJson = await accessGroupDetailResp.json();
    for (const eachAccessGroupObject of accessGroupDetailRespJson) {
      if (
        eachAccessGroupObject["groupName"].toLowerCase() ===
        accessGroupName.toLowerCase()
      ) {
        accessGroupId = eachAccessGroupObject["groupId"];
        console.log(
          `Group Name -> ${accessGroupName} || Group Id -> ${accessGroupId}`
        );
        break;
      }
    }
    return accessGroupId;
  }

  async createCustomAccessGroup(accessGroupName: string) {
    let createCustomAccessGroupApi = `/api/access_groups/${this.eventId}/group/${accessGroupName}`;
    let createCustomAccessGroupResponse = await this.orgApiRequestContext.post(
      createCustomAccessGroupApi
    );

    if (createCustomAccessGroupResponse.status() != 200) {
      throw new Error(
        `API to create custom access group failed with - ${createCustomAccessGroupResponse.status()}`
      );
    }

    const accessGroupDetailRespJson =
      await createCustomAccessGroupResponse.json();

    let accessGroupId = await accessGroupDetailRespJson["groupId"];

    return accessGroupId;
  }

  async toggleFeatureForAccessGroup(
    featureName: string,
    accessGroupName: string,
    featureStatus: boolean
  ) {
    const accessGroupId = await this.fetchAccessGroupId(accessGroupName);
    const featureId = await this.fetchFeatureIdForGroup(
      featureName,
      accessGroupId
    );

    let getAccessGroupDetail = `/api/access_groups/${this.eventId}/${accessGroupId}`;

    const patchPayload = {
      id: featureId,
      isActive: featureStatus,
      isStageBreakoutRoomActive: null,
    };

    const updateFeatureStatusForAccessGroupResp =
      await this.orgApiRequestContext.patch(getAccessGroupDetail, {
        data: [patchPayload],
      });

    if (updateFeatureStatusForAccessGroupResp.status() != 200) {
      throw new Error(
        `Patch API call to make onboarding status as ${featureStatus} failed with ${updateFeatureStatusForAccessGroupResp.status()}`
      );
    }

    let featureStatusResp = await this.getAccessForFeatureStatus(
      featureName,
      accessGroupName
    );
    expect(featureStatusResp).toBe(featureStatus);
  }

  async getAccessForFeatureStatus(
    featureName: string,
    accessGroupName: string
  ) {
    const accessGroupId = await this.fetchAccessGroupId(accessGroupName);
    const featureId = await this.fetchFeatureIdForGroup(
      featureName,
      accessGroupId
    );

    let getAccessGroupDetail = `/api/access_groups/${this.eventId}/${accessGroupId}`;

    let accessGroupDetailResp = await this.orgApiRequestContext.get(
      getAccessGroupDetail
    );

    if (accessGroupDetailResp.status() != 200) {
      throw new Error(
        `API to fetch access group details of group id ${accessGroupId} failed with ${accessGroupDetailResp.status()}`
      );
    }

    const accessGroupDetailRespJson = await accessGroupDetailResp.json();

    for (const eachFeatureObj of accessGroupDetailRespJson["features"]) {
      console.log(JSON.stringify(eachFeatureObj));
      if (eachFeatureObj["id"] == featureId) {
        return eachFeatureObj["isMappingActive"];
      }
    }
  }

  async getDayIdForAccessGroup(accessGroupName: string, dayCount: number) {
    const accessGroupId = await this.fetchAccessGroupId(accessGroupName);
    let getAccessGroupDetail = `/api/access_groups/${this.eventId}/${accessGroupId}`;
    let accessGroupDetailResp = await this.orgApiRequestContext.get(
      getAccessGroupDetail
    );
    if (accessGroupDetailResp.status() != 200) {
      throw new Error(
        `API to fetch access group details of group id ${accessGroupId} failed with ${accessGroupDetailResp.status()}`
      );
    }

    const accessGroupDetailRespJson = await accessGroupDetailResp.json();
    const listOfFeatures = accessGroupDetailRespJson["features"];
    for (const eachFeatureObj of listOfFeatures) {
      if (eachFeatureObj["featureName"] == "DAY") {
        const dayFeatureObj = eachFeatureObj["subFeatures"][dayCount - 1];
        console.log(dayFeatureObj);
        console.log(dayFeatureObj["id"]);
        return dayFeatureObj["id"];
      }
    }
  }

  async toggleDayForAccessGroup(
    accessGroupName: string,
    dayCount: number,
    dayStatus: boolean
  ) {
    const accessGroupId = await this.fetchAccessGroupId(accessGroupName);
    const dayId = await this.getDayIdForAccessGroup(accessGroupName, dayCount);

    let getAccessGroupDetail = `/api/access_groups/${this.eventId}/day_access/${accessGroupId}`;

    const patchPayload = {
      id: dayId,
      isActive: dayStatus,
      isStageBreakoutRoomActive: null,
    };

    const updateFeatureStatusForAccessGroupResp =
      await this.orgApiRequestContext.patch(getAccessGroupDetail, {
        data: [patchPayload],
      });

    if (updateFeatureStatusForAccessGroupResp.status() != 200) {
      throw new Error(
        `Patch API call to make onboarding status as ${dayStatus} failed with ${updateFeatureStatusForAccessGroupResp.status()}`
      );
    }
  }

  async getAccessDaysCount(accessGroupName: string) {
    const accessGroupId = await this.fetchAccessGroupId(accessGroupName);
    console.log(accessGroupId);
    let getAccessGroupDetail = `/api/access_groups/${this.eventId}/${accessGroupId}`;
    let accessGroupDetailResp = await this.orgApiRequestContext.get(
      getAccessGroupDetail
    );
    if (accessGroupDetailResp.status() != 200) {
      throw new Error(
        `API to fetch access group details of group id ${accessGroupId} failed with ${accessGroupDetailResp.status()}`
      );
    }

    let result = {
      allowedDays: 0,
      totalDays: 0,
    };

    const accessGroupDetailRespJson = await accessGroupDetailResp.json();
    const listOfFeatures = accessGroupDetailRespJson["features"];
    for (const eachFeatureObj of listOfFeatures) {
      if (eachFeatureObj["featureName"] == "DAY") {
        for (const eachDay of eachFeatureObj["subFeatures"]) {
          result["totalDays"] += 1;
          if (eachDay["isSubFeatureActive"]) {
            result["allowedDays"] += 1;
          }
        }
        console.log(result);
        return result;
      }
    }
  }

  async getAllAccessGroups() {
    const getAllAccessGroupsForEvents = `/api/access_groups/${this.eventId}/groups`;

    let accessGroupDetailResp = await this.orgApiRequestContext.get(
      getAllAccessGroupsForEvents
    );

    if (accessGroupDetailResp.status() != 200) {
      throw new Error(
        `API to fetch access group details failed with ${accessGroupDetailResp.status()}`
      );
    }

    const accessGroupDetailRespJson = await accessGroupDetailResp.json();

    return accessGroupDetailRespJson;
  }

  async getTotalAttendeeCount() {
    let attendeeCountPayload = {
      accessGroupIds: [],
      attendeeStatus: "ALL",
      ticketTypeIds: [],
    };

    let getAttendeeCountApi = `/api/event/user_registration/${this.eventId}/list/count?q=`;

    let attendeeCountResponse = await this.orgApiRequestContext.post(
      getAttendeeCountApi,
      {
        data: attendeeCountPayload,
      }
    );

    if (attendeeCountResponse.status() != 200) {
      throw new Error(
        `API to fetch attendee count failed with ${attendeeCountResponse.status()}`
      );
    }

    // response is integer directly, not json
    let attendeeCount = await attendeeCountResponse
      .text()
      .then((text) => Number(text));

    return attendeeCount;
  }
}
