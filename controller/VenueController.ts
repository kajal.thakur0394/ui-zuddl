import { APIRequestContext, expect } from "@playwright/test";
import ZoneType from "../enums/ZoneTypeEnum";

export class VenueController {
  protected orgApiRequestContext: APIRequestContext;
  protected eventId;

  constructor(orgApiRequestContext: APIRequestContext, eventId) {
    this.orgApiRequestContext = orgApiRequestContext;
    this.eventId = eventId;
  }

  async getVenueZoneList() {
    const getListOfZonesReqApi = `/api/event/${this.eventId}/zone/list/v3`;
    const getListOfZonesRespApi = await this.orgApiRequestContext.get(
      getListOfZonesReqApi
    );

    // validate the get zone list api call
    if (getListOfZonesRespApi.status() != 200) {
      throw new Error(
        `API call to get list of zones failed with ${getListOfZonesRespApi.status()}`
      );
    }

    return getListOfZonesRespApi;
  }

  async updateVenueZoneStatus(zonesToUpdate: Map<ZoneType, boolean>) {
    let venueZoneList = await (await this.getVenueZoneList()).json();
    console.log(venueZoneList);
    venueZoneList.forEach((zone) => {
      if (zonesToUpdate.has(zone.type)) {
        zone.active = zonesToUpdate.get(zone.type);
      }
    });

    const updateZoneReqApi = `/api/event/${this.eventId}/zones/update`;
    const updateZoneRespApi = await this.orgApiRequestContext.post(
      updateZoneReqApi,
      { data: venueZoneList }
    );

    if (updateZoneRespApi.status() != 200) {
      throw new Error(
        `API call to update zone active/inactive failed with ${updateZoneRespApi.status()}`
      );
    }
    console.log(await updateZoneRespApi.json());
  }

  async verifyZoneUpdationViaAPI(zonesToUpdate: Map<ZoneType, boolean>) {
    let venueZoneList = await (await this.getVenueZoneList()).json();
    venueZoneList.forEach(async (zone) => {
      if (zonesToUpdate.has(zone.type)) {
        await expect(zone.active).toEqual(zonesToUpdate.get(zone.type));
      }
    });
  }
}
