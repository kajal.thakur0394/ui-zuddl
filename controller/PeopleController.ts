import { APIRequestContext, APIResponse, Page, test } from "@playwright/test";
import { StageController } from "./StageController";

export class PeopleController {
  readonly orgApiContext: APIRequestContext;
  readonly eventId: string;
  readonly stageController: StageController;

  constructor(orgApiContext: APIRequestContext, eventId: string) {
    this.orgApiContext = orgApiContext;
    this.eventId = eventId;
  }

  async postMessageInPeopleEventChat(
    contentToPost: string,
    accountId: string,
    channelId
  ) {
    const postMessage = await this.orgApiContext.post(
      `/api/channel/${channelId}/attendee/message?receiverAccountId=${accountId}`,
      {
        data: {
          content: contentToPost,
          mediaType: "text",
        },
      }
    );

    if (postMessage.status() != 200) {
      throw new Error(
        `API to send message failed with ${postMessage.status()}`
      );
    }
  }

  async getChannelResponse(anyId: string): Promise<APIResponse> {
    let getChannelIdApiResp: APIResponse;
    await test.step(`Fetching the channel response for ${anyId}`, async () => {
      getChannelIdApiResp = await this.orgApiContext.get(
        `/api/channel?refId=${anyId}`
      );
      if (getChannelIdApiResp.status() != 200) {
        throw new Error(
          `API to fetch channelId for room failed with ${getChannelIdApiResp.status()}`
        );
      }
    });
    return getChannelIdApiResp;
  }

  async getChannelId(anyId: string) {
    const getChannelApiResp = await this.getChannelResponse(anyId);
    const getChannelIdRespJson = await getChannelApiResp.json();
    const channelId = getChannelIdRespJson["channelId"];
    console.log(`The channel id fetched for stage ${anyId} is ${channelId}`);
    return channelId;
  }

  async getMessageListInPeopleEventChat(accountId: string, channelId) {
    const getMessageList = await this.orgApiContext.get(
      `/api/channel/${channelId}/attendee/message/list?receiverAccountId=${accountId}&eventId=${this.eventId}`
    );

    if (getMessageList.status() != 200) {
      throw new Error(
        `API to fetch message list failed with ${getMessageList.status()}`
      );
    }

    return await getMessageList.json();
  }
}
