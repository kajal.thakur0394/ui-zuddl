import { inspect } from "util";
import { DataUtl } from "../util/dataUtil";
import { APIRequestContext, expect } from "@playwright/test";
import { setTimeOut } from "../util/commonUtil";

export class RecordingController {
  readonly orgApiRequestContext: APIRequestContext;
  readonly eventId;

  constructor(
    orgApiRequestContext: APIRequestContext,
    eventId: string | number
  ) {
    this.orgApiRequestContext = orgApiRequestContext;
    this.eventId = eventId;
  }

  async fetchRecordings() {
    const fetchRecordingsUrl = `${
      DataUtl.getApplicationTestDataObj()["studioBaseUrl"]
    }/api/segment/${this.eventId}/list/all?r=`;

    const fetchRecordingsResponse = await this.orgApiRequestContext.post(
      fetchRecordingsUrl
    );

    if (!fetchRecordingsResponse.ok()) {
      console.log(
        `API to fetch recordings failed with response - ${fetchRecordingsResponse}`
      );
    }

    console.log(
      "Recordings -> ",
      inspect(fetchRecordingsResponse, { depth: null })
    );

    return fetchRecordingsResponse;
  }

  async verifyRecordingIsPresent(recordingFileId: number | string) {
    const recordingList = await this.fetchRecordings();

    // for(const recording of recordingList){
    //     if(recording[""] == )
    // }
  }

  async fetchRecordingsFromSetupSide() {
    const fetchRecordingListEndPoint = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/api/recording/${this.eventId}/list`;
    await setTimeOut(6000);
    let recordingResponse = await this.orgApiRequestContext.get(
      fetchRecordingListEndPoint
    );
    let recordingResponseJson = await recordingResponse.json();
    // validate the get recordings api call
    if ( recordingResponse.status() != 200) {
      throw new Error(
        `API call to get list of recordings failed with ${recordingResponse.status()}`
      );
    }
    return await recordingResponseJson;
  }

  async verifyIfDurationIsNonZero() {
    let recordingResponseJson = await this.fetchRecordingsFromSetupSide();
    console.log(await recordingResponseJson[0]);
    
    const segmentStartDateTime = await recordingResponseJson[0].segmentStartDateTime;
    const segmentEndDateTime = await recordingResponseJson[0].segmentEndDateTime;
    const startTime = new Date(segmentStartDateTime).getTime();
    const endTime = new Date(segmentEndDateTime).getTime();
    // Calculate the duration in milliseconds
    const duration = endTime - startTime;
    // Check if duration is non-zero
     expect(duration).toBeGreaterThan(0);
  }
  async verifyIfSizeByteIsNonZero() {
    const recordingResponseJson = await this.fetchRecordingsFromSetupSide();
    const recordingSize = await recordingResponseJson[0].sizeBytes;
    expect(recordingSize).toBeGreaterThan(0);
}
}
