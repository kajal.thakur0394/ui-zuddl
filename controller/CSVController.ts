import { APIRequestContext } from "@playwright/test";

export class CSVController {
  private orgApiRequestContext: APIRequestContext;
  private eventId: any;

  constructor(orgApiRquestContext: APIRequestContext, eventId) {
    this.orgApiRequestContext = orgApiRquestContext;
    this.eventId = eventId;
  }

  async getDownloadCsv() {
    let downloadCsvEndpoint = `api/event/${this.eventId}/registrations/downloadCSV`;
    let downloadCSVResponse = await this.orgApiRequestContext.get(
      downloadCsvEndpoint
    );
    if (downloadCSVResponse.status() != 200) {
      throw new Error(
        `API to fetch download csv response failed with ${downloadCSVResponse.status()}`
      );
    }
    let downloadCSVResponseText = await downloadCSVResponse.text();
    return downloadCSVResponseText;
  }
}
