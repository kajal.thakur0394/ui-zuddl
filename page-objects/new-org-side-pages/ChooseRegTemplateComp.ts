import { Page } from "@playwright/test";
export class ChooseRegTemplateComponent {
  readonly page: Page;
  constructor(page: Page) {
    this.page = page;
  }

  async clickOnUseTemplate() {
    await this.page.click("text=Use template");
  }
}
