import { Locator, Page } from "@playwright/test";
import { AttendeeListPage } from "./AttendeeListPage";
import { EmbeddableFormPage } from "./embeddableFormPage";
export class FormPage {
  readonly page: Page;
  readonly eachMenuItemLocator: Locator;
  readonly embeddableFormSelector: Locator;

  constructor(page: Page) {
    this.page = page;
    this.eachMenuItemLocator = this.page.locator(
      "span[class^='item_listItemBody']"
    );
    this.embeddableFormSelector = this.page.locator(
      "//p[text()='Embeddable form']"
    )
  }

  async openFormTab() {
    await this.eachMenuItemLocator
      .filter({
        hasText: "Registration",
      })
      .click();
    await this.page.locator("li[class^='item_listItemWrapper']")
      .filter({
        hasText: "Form",
      }).nth(1)
      .click();
  }

  async switchToEnbeddableFormTab() {
    await this.embeddableFormSelector.click();
    return new EmbeddableFormPage(this.page);
  }

}
