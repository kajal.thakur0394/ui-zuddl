import { expect, Locator, Page } from "@playwright/test";
export class EditEmailNotificationPage {
  readonly page: Page;
  readonly senderEmailDropDownContainer: Locator;
  readonly senderEmailDropDownOptions: Locator;

  constructor(page: Page) {
    this.page = page;
    this.senderEmailDropDownContainer = this.page.locator(
      "div[class^='email-notification-details-page_contentContainer'] span[id^='react-select']"
    );
    this.senderEmailDropDownOptions = this.page.locator("");
  }

  async activateTheEmail() {}

  async deactivateTheEmail() {}

  async updateSenderEmail() {}

  async clickOnSendersEmailDropDown() {}

  async updateSendersName() {}

  async updateEmailSubject() {}

  async enableCalendarBlock() {}

  async disableCalendarBlock() {}

  async clickOnUseEmailBuilder() {}

  async clickOnTestEmail() {}

  async clickOnCustomiseEmail() {}

  async clickOnRevertToOriginal() {}

  async verifyEmailPreviewIsVisible() {}
}
