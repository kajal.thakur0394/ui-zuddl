import {
  test,
  Locator,
  Page,
  expect,
  APIRequestContext,
} from "@playwright/test";
import { DataUtl } from "../../util/dataUtil";
import { resolve } from "path";
import { fetchMailasaurEmailObject } from "../../util/emailUtil";
import { emailTemplateValidation } from "../../util/emailTemplateValidation";
import { createNewEvent } from "../../util/apiUtil";
import { WebinarController } from "../../controller/WebinarController";

export class EventSeriesPage {
  private page: Page;
  private seriesNameInputLocator: Locator;
  private thumbnailUploadLocator: Locator;
  private createSeriesButton: Locator;
  private saveSeriesButton: Locator;
  private seriesPreviewCardLocator: Locator;
  private seriesAttendeeListLocator: Locator;
  private seriesId: string;
  private seriesEventNameInput: Locator;
  private seriesWebinarNameInput: Locator;
  private descriptionInput: Locator;
  private addEventToSeriesButton: Locator;
  private addWebinarToSeriesButton: Locator;
  private thumbnailPreviewLocator: Locator;
  private startDateTimeLocator: Locator;
  private todaysDateLocator: Locator;
  private timeHourInputLocator: Locator;
  private timeMinutesInputLocator: Locator;
  private amLocator: Locator;
  private pmLocator: Locator;
  private doneButtonLocator: Locator;

  constructor(page: Page, seriesId?: string) {
    this.page = page;
    this.seriesId = seriesId;

    this.seriesNameInputLocator = this.page.locator(
      "input[placeholder*='Enter series name']"
    );
    this.thumbnailUploadLocator = this.page.locator(
      "div[class*='FileUploadBox_content']"
    );
    this.createSeriesButton = this.page.getByRole("button", {
      name: "Create series",
    });
    this.saveSeriesButton = this.page.getByRole("button", {
      name: "Save",
    });
    this.seriesPreviewCardLocator = this.page.locator(
      "div[class*='series-preview-card_wrapper']"
    );
    this.seriesAttendeeListLocator = this.page.locator(
      "tr[class*='list-attendees-page_eventSeriesTableRow']"
    );
    this.seriesEventNameInput = this.page.locator(
      'input[placeholder="Enter event name"]'
    );
    this.seriesWebinarNameInput = this.page.locator(
      'input[placeholder="Enter webinar name"]'
    );
    this.descriptionInput = this.page.locator(
      'textarea[placeholder="Enter description"]'
    );
    this.addEventToSeriesButton = this.page.getByRole("button", {
      name: "Add event to series",
    });
    this.addWebinarToSeriesButton = this.page.getByRole("button", {
      name: "Add webinar to series",
    });
    this.thumbnailPreviewLocator = this.page.locator(
      'div[class*="FileUploadBox_previewImageContainer"]'
    );
    this.startDateTimeLocator = this.page.locator("input[id='startDateTime']");
    this.todaysDateLocator = this.page
      .locator(
        "//td[not(contains(@class,'date-time-field_dateDisabled')) and contains(@class, 'date-time-field_calendarDate') and not(contains(@class, 'date-time-field_notThisMonth'))]"
      )
      .first();
    this.timeHourInputLocator = this.page
      .locator("input[class*='date-time-field_timeInput']")
      .first();
    this.timeMinutesInputLocator = this.page
      .locator("input[class*='date-time-field_timeInput']")
      .nth(1);
    this.amLocator = this.page.getByRole("button", { name: "AM" });
    this.pmLocator = this.page.getByRole("button", { name: "PM" });
    this.doneButtonLocator = this.page.getByText("Done", { exact: true });
  }

  private async navigateToCreateSeriesPage() {
    let createSeriesUrl = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/series/create`;

    await this.page.goto(createSeriesUrl);
  }

  async navigateToSeriesPeoplePage() {
    let seriesPeopleSection = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/series/${this.seriesId}/people/attendees`;

    await this.page.goto(seriesPeopleSection);
  }

  async createSeries(seriesName: string, seriesThumbnailPath: string) {
    await this.navigateToCreateSeriesPage();

    await this.seriesNameInputLocator.fill(seriesName);

    await this.page.locator('div[class*="FileUploadBox"]').first().click();
    await this.page
      .locator('div[class*="FileUploadBox"] input')
      .setInputFiles(resolve(__dirname, seriesThumbnailPath));

    try {
      await expect(this.seriesPreviewCardLocator).toBeVisible({
        timeout: 30000,
      });
    } catch (error) {
      let saveButton = this.page.getByText("Save");
      await saveButton.click();
      await expect(this.seriesPreviewCardLocator).toBeVisible({
        timeout: 30000,
      });
    }

    await this.createSeriesButton.click();

    let apiURI = `/api/series/create`;
    const createSeriesApiResponse = await this.page.waitForResponse(
      (resp) => resp.url().includes(apiURI) && resp.status() === 200
    );

    const responseBody = await createSeriesApiResponse.json();

    return responseBody["eventId"];
  }

  async verifyAttendeeCountInPeopleSection() {
    await this.navigateToSeriesPeoplePage();
    await expect(this.seriesAttendeeListLocator.first()).toBeVisible({
      timeout: 50000,
    });
  }

  async addEventToTheSeries(
    orgApiContext: APIRequestContext,
    eventTitle: string,
    dayDelta: number = 0,
    timeDelta: number = 0
  ) {
    return await createNewEvent({
      api_request_context: orgApiContext,
      event_title: eventTitle,
      add_start_date_from_today: dayDelta,
      seriesEventId: this.seriesId,
      add_hours_to_start_date: timeDelta,
    });
  }

  async addWebinarToTheSeriesByApi(
    orgApiContext: APIRequestContext,
    webinarTitle: string,
    dayDelta: number = 0
  ) {
    let webinarController = new WebinarController(orgApiContext);
    return await webinarController.createNewWebinar({
      title: webinarTitle,
      seriesEventId: this.seriesId,
    });
  }

  async addEventToTheSeriesViaUi(
    eventThumbnailPath: string,
    createEventForToday: boolean = false,
    createPastEvent: boolean = false
  ) {
    let eventWebinarSection = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/series/${this.seriesId}/event-webinar/event/create`;

    await this.page.goto(eventWebinarSection);

    await expect(this.seriesEventNameInput).toBeVisible({ timeout: 10000 });
    await this.seriesEventNameInput.fill(
      `TestEvent - ${DataUtl.getRandomName()}`
    );

    await this.descriptionInput.fill("This is a random description.");

    // await this.page.locator('div[class*="FileUploadBox"]').first().click();
    // await this.page
    //   .locator('div[class*="FileUploadBox"] input')
    //   .setInputFiles(resolve(__dirname, eventThumbnailPath));

    // await expect(this.thumbnailPreviewLocator).toBeVisible({ timeout: 30000 });

    if (createEventForToday === true) {
      await this.setDateTimeForToday();
    } else if (createPastEvent === true) {
      await this.setDateTimeForPastEvent();
    } else {
      await this.setDateTimeForUpcomingEvent();
    }

    await this.addEventToSeriesButton.click();

    let apiURI = `/api/event`;
    const createEventApiResponse = await this.page.waitForResponse(
      (resp) => resp.url().includes(apiURI) && resp.status() === 200
    );

    const responseBody = await createEventApiResponse.json();

    return responseBody["eventId"];
  }

  async addWebinarToTheSeries(
    eventThumbnailPath: string,
    createWebinarForToday: boolean = false,
    createPastWebinar: boolean = false,
    waitForWebinarToComplete: boolean = false
  ) {
    let eventWebinarSection = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/series/${this.seriesId}/event-webinar/webinar/create`;

    await this.page.goto(eventWebinarSection);

    let webinarName = `TestWebinar - ${DataUtl.getRandomName()}`;

    await this.seriesWebinarNameInput.fill(webinarName);

    await this.descriptionInput.fill("This is a random description.");

    // await this.page.locator('div[class*="FileUploadBox"]').first().click();
    // await this.page
    //   .locator('div[class*="FileUploadBox"] input')
    //   .setInputFiles(resolve(__dirname, eventThumbnailPath));

    // await expect(this.thumbnailPreviewLocator).toBeVisible({ timeout: 30000 });

    if (createWebinarForToday === true) {
      await this.setDateTimeForToday();
    } else if (createPastWebinar === true) {
      await this.setDateTimeForPastEvent();
    } else {
      await this.setDateTimeForUpcomingWebinar();
    }

    await this.addWebinarToSeriesButton.click();

    let apiURI = `/api/event/webinar`;
    const createWebinarApiResponse = await this.page.waitForResponse(
      (resp) => resp.url().includes(apiURI) && resp.status() === 200
    );

    const responseBody = await createWebinarApiResponse.json();

    if (waitForWebinarToComplete === true) {
      let eventWebinar = `${
        DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
      }/series/${this.seriesId}/event-webinar`;

      await this.page.goto(eventWebinar);

      let flag: number = 1;
      while (flag != 0) {
        let webinarStatus = await this.page
          .locator(
            `//p[contains(text(), '${webinarName}')]/../../p[contains(@class,'table_event-status')]`
          )
          .textContent();

        if (webinarStatus === "completed") {
          flag = 0;
        }
      }
    }

    return responseBody["eventId"];
  }

  async setDateTimeForToday() {
    await this.startDateTimeLocator.click();

    const d = new Date();
    let h: number = d.getHours();
    let m: number = d.getMinutes();

    let sh = h - 1;
    let minutes = m + 1;
    if (sh < 12) {
      console.log(`Start hour -> ${sh}`);
      await this.todaysDateLocator.click();
      await this.timeHourInputLocator.fill(sh.toString());
      await this.timeMinutesInputLocator.fill(minutes.toString());
      await this.todaysDateLocator.click();
      await this.amLocator.click();
    } else {
      sh = sh - 12;
      console.log(`Start hour -> ${sh}`);
      await this.todaysDateLocator.click();
      await this.timeHourInputLocator.fill(sh.toString());
      await this.timeMinutesInputLocator.fill(minutes.toString());
      await this.todaysDateLocator.click();
      await this.pmLocator.click();
    }

    await this.doneButtonLocator.click();

    h = h + 1;
    if (h < 12) {
      console.log(`End hour -> ${h}`);
      await this.todaysDateLocator.click();
      await this.timeHourInputLocator.fill(h.toString());
      await this.timeMinutesInputLocator.fill(minutes.toString());
      await this.todaysDateLocator.click();
      await this.amLocator.click();
    } else {
      h = h - 12;
      console.log(`End hour -> ${h}`);
      await this.todaysDateLocator.click();
      await this.timeHourInputLocator.fill(h.toString());
      await this.timeMinutesInputLocator.fill(minutes.toString());
      await this.todaysDateLocator.click();
      await this.pmLocator.click();
    }
    await this.doneButtonLocator.click();
  }

  async setDateTimeForPastEvent() {
    await this.startDateTimeLocator.click();

    const d = new Date();
    let h: number = d.getHours();
    let m: number = d.getMinutes();

    let sh = h - 3;
    let minutes = m + 2;

    await this.todaysDateLocator.click();

    await this.timeHourInputLocator.fill(sh.toString());
    await this.timeMinutesInputLocator.fill(minutes.toString());
    if (sh < 12) {
      await this.amLocator.click();
    } else {
      await this.pmLocator.click();
    }
    await this.doneButtonLocator.click();

    await this.timeHourInputLocator.fill(h.toString());
    await this.timeMinutesInputLocator.fill((minutes - 1).toString());
    if (h < 12) {
      await this.amLocator.click();
    } else {
      await this.pmLocator.click();
    }
    await this.doneButtonLocator.click();
  }

  async setDateTimeForUpcomingEvent() {
    await this.startDateTimeLocator.click();

    const d = new Date();
    let h: number = d.getHours();
    let m: number = d.getMinutes();

    let sh = h + 2;

    await this.todaysDateLocator.click();
    await this.timeHourInputLocator.fill(sh.toString());
    await this.timeMinutesInputLocator.fill(m.toString());
    if (sh < 12) {
      await this.amLocator.click();
    } else {
      await this.pmLocator.click();
    }
    await this.doneButtonLocator.click();

    await this.doneButtonLocator.click();
  }

  async setDateTimeForUpcomingWebinar() {
    await this.startDateTimeLocator.click();

    const d = new Date();
    let h: number = d.getHours();
    let m: number = d.getMinutes();

    let sh = h + 2;
    let eh = h + 3;

    await this.todaysDateLocator.click();
    await this.timeHourInputLocator.fill(sh.toString());
    await this.timeMinutesInputLocator.fill(m.toString());
    if (sh < 12) {
      await this.amLocator.click();
    } else {
      await this.pmLocator.click();
    }
    await this.doneButtonLocator.click();

    await this.todaysDateLocator.click();
    await this.timeHourInputLocator.fill(eh.toString());
    await this.timeMinutesInputLocator.fill(m.toString());
    if (eh < 12) {
      await this.amLocator.click();
    } else {
      await this.pmLocator.click();
    }
    await this.doneButtonLocator.click();
  }

  async verifyEmail(email: string) {
    let goToDashboardButtonContents;

    let emailTemplateObject = await fetchMailasaurEmailObject(email, "");

    let emailInvite = new emailTemplateValidation(emailTemplateObject);

    await test.step(`Validating Go to dashboard button text to be visible in email`, async () => {
      goToDashboardButtonContents =
        await emailInvite.fetchGoToDashboardButtonLink();
      expect(
        goToDashboardButtonContents[0],
        "Verifying text of goToDashboard button"
      ).toContain("Go to dashboard");
    });
  }

  async verifyUpdatedCheckinTime(eventId) {
    let advancedSettingsUrl = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/event/${eventId}/registration/advanced-settings`;
    await this.page.goto(advancedSettingsUrl);

    let checkInLocator: Locator = this.page.locator(
      'select[name="checkInEnableTime"]'
    );

    await expect(checkInLocator).toBeVisible({ timeout: 10000 });
  }
}
