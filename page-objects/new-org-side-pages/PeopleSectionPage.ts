import { Locator, Page } from "@playwright/test";
import { AttendeeListPage } from "./AttendeeListPage";
import { SpeakersListPage } from "./SpeakersListPage";
export class PeopleSectionPage {
  readonly page: Page;
  readonly eachMenuItemLocator: Locator;
  constructor(page: Page) {
    this.page = page;
    this.eachMenuItemLocator = this.page.locator(
      "span[class^='item_listItemBody'] p"
    );
  }

  async openAttendeeTab() {
    await this.eachMenuItemLocator
      .filter({
        hasText: "Attendees",
      })
      .click();
    return new AttendeeListPage(this.page);
  }

  async openSpeakerTab() {
    await this.eachMenuItemLocator
      .filter({
        hasText: "Speakers",
      })
      .click();
    return new SpeakersListPage(this.page);
  }
}
