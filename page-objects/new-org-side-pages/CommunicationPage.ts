import { expect, Locator, Page } from "@playwright/test";

export class CommunicationsPage {
  readonly page: Page;
  readonly registrationConfirmationLi: Locator;
  readonly registrationPendingApprovalLi: Locator;
  readonly registrationRejectedLi: Locator;
  readonly eventInvitationLi: Locator;

  constructor(page: Page) {
    this.page = page;
  }

  async toggleView(userType: string) {
    await this.page.getByText(userType, { exact: true }).click();
  }

  async toggleSetting(heading: string) {
    let notificationCard = await this.page
      .locator(`div[class^=email-notification-list-page_card]`)
      .filter({ hasText: heading });

    await notificationCard
      .locator("label[class^=Toggle_switch]")
      .first()
      .click({
        timeout: 10000,
      });
  }

  async verifyCalendarInviteIcon(
    heading: string,
    shouldBeVisible: boolean = false
  ) {
    let notificationCard = await this.page
      .locator(`div[class^=email-notification-list-page_card]`)
      .filter({ hasText: heading });
    const calendarIcon = notificationCard.getByText("Calendar invite included");

    if (shouldBeVisible) {
      await expect(calendarIcon).toBeVisible({
        timeout: 10000,
      });
    } else {
      await expect(calendarIcon).not.toBeVisible({
        timeout: 10000,
      });
    }
  }
}
