import { Page, expect, Locator } from "@playwright/test";
import EmailRestrictionType from "../../enums/emailRestrictionTypeEnum";

export class EmailDomainRestrictionComponent {
  readonly page: Page;
  readonly uploadCsvButton: Locator;
  readonly emailRestrictionDropDownOptions: Locator;
  readonly emailRestrictionDropDown: Locator;
  readonly uploadCsvFrom: Locator;
  readonly csvFileInputContainer: Locator;
  readonly filePreviewOnFileUpload: Locator; //this denotes file is uploaded
  readonly fileUploadSaveButton: Locator;
  readonly uploadedFilePreviewOnMenuSection: Locator;
  readonly checkBoxForDenyEmailRestrictionType: Locator;
  readonly actionButtonOnUploadedCsv: Locator;

  constructor(page: Page) {
    this.page = page;
    this.uploadCsvButton = this.page.locator("text=Upload CSV");
    this.emailRestrictionDropDown = this.page.locator(
      "select[name='emailRestrictionType']"
    );
    this.emailRestrictionDropDownOptions = this.page.locator(
      "select[name='emailRestrictionType'] option"
    );
    this.uploadCsvFrom = this.page.locator("#uploadCsvForm");
    this.csvFileInputContainer = this.page.locator(
      "div[class^='FileUploadBox_dropzoneContainer'] input"
    );
    this.fileUploadSaveButton = this.page.locator("text=Save");
    this.uploadedFilePreviewOnMenuSection = this.page.locator(
      "div[class^='email-restrict-type-sublist_fileName']"
    );
    this.checkBoxForDenyEmailRestrictionType = this.page.locator(
      "div[class^='email-domain-restriction_container'] input[type='checkbox']"
    );
    this.filePreviewOnFileUpload = this.page.locator(
      "div[class^='FileUploadBox_previewFileContainer']"
    );
    this.actionButtonOnUploadedCsv = this.page.locator(
      "div[class^='email-restrict-type-sublist_buttonGroup'] div[role='button']"
    );
  }

  async openSection() {
    await this.page.click("text=Email domain restriction");
  }

  async selectOptionFromDropDown(optionName: EmailRestrictionType) {
    await this.emailRestrictionDropDown.selectOption({ value: optionName });
  }

  async clickOnUploadCsv() {
    await this.uploadCsvButton.click();
  }

  async clickOnProceedButtonOnConfirmationDialogue() {
    await this.page
      .locator("div[class^='confirm-dialog_buttonBlock'] button")
      .filter({ hasText: "Proceed" })
      .click();
  }
  async verifyUploadCsvFromIsVisible() {
    await expect(this.uploadCsvFrom).toBeVisible();
  }

  async uploadCsvForEmailRestriction(csvFileName: string) {
    //verify upload csv form is visible
    await this.csvFileInputContainer.setInputFiles(csvFileName);
    await expect(this.filePreviewOnFileUpload).toBeVisible();
    await this.fileUploadSaveButton.click();
  }

  async verifyUploadedFileIsVisbleBelowMenuSection() {
    await expect(
      this.uploadedFilePreviewOnMenuSection,
      "expecting file name to be visible below dropdown contianer"
    ).toBeVisible();
  }

  async verifyUploadedFileIsNotVisbleBelowMenuSection() {
    await expect(
      this.uploadedFilePreviewOnMenuSection,
      "expecting file name to be visible below dropdown contianer"
    ).toBeHidden();
  }

  async verifyCheckBoxesAreVisible() {
    // verify 2 checkboxes are displayed
    await expect(
      this.checkBoxForDenyEmailRestrictionType,
      "expecting 2 check boxes to be displayed"
    ).toHaveCount(2);
  }
  async downloadUploadedFileFromMenuSection() {
    const [download] = await Promise.all([
      this.page.waitForEvent("download", { timeout: 20000 }),
      this.actionButtonOnUploadedCsv.nth(0).click(),
      // wait for download to start
    ]);
    // Wait for the download process to complete
    await download.path();
    await download.saveAs("downloadedCsvFile.txt");
  }

  async deleteUploadedFileFromMenuSection() {}

  async clickOnRestrictNonBusinessEmailCheckbox() {
    await this.page.getByText("Restrict non-business emails").click();
  }

  async clickOnRestrictEmailFromCsvCheckbox() {
    await this.page.getByText("Restrict emails from a CSV file").click();
  }

  async validateTheDropDownOptions() {
    // verify the drop down contains 3 options
    await expect(this.emailRestrictionDropDownOptions).toHaveCount(3);
    // verify the options for drop down is what is expected
    const arrayOfOptionsValue = ["NONE", "DENY", "ALLOW"];
    let allValuesMatched = true;
    for (let i = 0; i < 3; i++) {
      const eleText = await this.emailRestrictionDropDownOptions
        .nth(i)
        .getAttribute("value");
      if (!arrayOfOptionsValue.includes(eleText!)) {
        console.log(`${eleText} is not present in ${arrayOfOptionsValue}`);
        allValuesMatched = false;
        break;
      }
    }
    expect(
      allValuesMatched,
      "verifying dropdown option value matches with exp data"
    ).toBeTruthy();
  }
}
