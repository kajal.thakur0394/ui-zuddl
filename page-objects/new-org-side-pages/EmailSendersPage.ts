import { expect, Locator, Page } from "@playwright/test";
export interface AddEmailDomainFormParams {
  sendersEmailAddress: string;
  replyEmailAddress?: string;
  sendersName?: string;
}

export class EmailSendersPage {
  readonly page: Page;
  readonly addEmailAddressButton: Locator;
  readonly emailAddressInputField: Locator;
  readonly replyEmailAddressInputField: Locator;
  readonly sendersNameInputField: Locator;
  readonly continueButtonOnAddEmailForm: Locator;
  readonly verifyRecordButtonOnVerifyForm: Locator;
  readonly cancelIconOnForm: Locator;
  readonly eachEmailDomainRecordInList: Locator;
  readonly defaultEmailDomainSenderBadge: Locator;
  readonly threeDotsButton: string;
  readonly makeDefaultOptionInMenu: Locator;
  readonly viewDnsRecordOptionInMenu: Locator;
  readonly deleteOptionInMenu: Locator;

  constructor(page: Page) {
    this.page = page;
    this.addEmailAddressButton = this.page.locator(
      "div[class^='email-senders-page_actionGroupBtns'] button:has-text('Add email address')"
    );
    //add email domain form
    this.emailAddressInputField = this.page.locator("#senderEmail");
    this.replyEmailAddressInputField = this.page.locator("#replyEmail");
    this.sendersNameInputField = this.page.locator("#senderName");
    this.continueButtonOnAddEmailForm = this.page.locator(
      "div[class^='email-sender-form_footer'] button:has-text('Continue')"
    );

    //verify domain form
    this.verifyRecordButtonOnVerifyForm = this.page.locator(
      "div[class^='verify-domain-form_footer'] button"
    );
    //cancel button
    this.cancelIconOnForm = this.page.locator(
      "div[class^='side-panel_modalTitleIcon']"
    );
    //email domain list
    this.eachEmailDomainRecordInList = this.page.locator(
      "table[class^='email-senders-page_emailTable'] tbody tr"
    );
    this.defaultEmailDomainSenderBadge = this.page.locator(
      "div[class*='email-senders-page_defaultBadge']"
    );
    this.threeDotsButton = "div[class^='email-senders-page_dotIcons']";

    this.deleteOptionInMenu = this.page
      .locator("button[class^='kebab-menu_kebabMenuButton']")
      .filter({ hasText: "Delete" });

    this.viewDnsRecordOptionInMenu = this.page
      .locator("button[class^='kebab-menu_kebabMenuButton']")
      .filter({ hasText: "View DNS record" });

    this.makeDefaultOptionInMenu = this.page
      .locator("button[class^='kebab-menu_kebabMenuButton']")
      .filter({ hasText: "Make default" });
  }

  async clickOnAddEmailAddressButton() {
    await this.addEmailAddressButton.click();
  }

  async handleDeleteConfirmationPopup(toDelete = true) {
    const confirmationPopup = this.page.locator(
      "div[class^='confirmation-popup_actions'] button"
    );
    if (toDelete) {
      await confirmationPopup.filter({ hasText: "Delete" }).click();
    } else {
      await confirmationPopup.filter({ hasText: "Cancel" }).click();
    }
  }

  async fillUpAddDomainForm({
    sendersEmailAddress,
    replyEmailAddress = undefined,
    sendersName = undefined,
  }: AddEmailDomainFormParams) {
    await this.page.waitForTimeout(2000); //wait for 2 seconds to this component to load
    await this.emailAddressInputField.fill(sendersEmailAddress);
    if (replyEmailAddress != undefined) {
      await this.replyEmailAddressInputField.fill(replyEmailAddress);
    }
    if (sendersName != undefined) {
      await this.sendersNameInputField.fill(sendersName);
    }
    await this.continueButtonOnAddEmailForm.click();
  }

  async getEmailDomainRecordFromList(sendersDomainName: string) {
    return this.eachEmailDomainRecordInList.filter({
      hasText: sendersDomainName,
    });
  }

  async verifyTotalRecordCountToBe(expectedCount: number) {
    await expect(
      this.eachEmailDomainRecordInList,
      `expecting total ${expectedCount} records to be present`
    ).toHaveCount(expectedCount);
  }

  async getVerificationStatusForSenderEmail(
    senderDomainEmail: string
  ): Promise<Locator> {
    //todo
    const domainRecord = await this.getEmailDomainRecordFromList(
      senderDomainEmail
    );
    const statusColumn = domainRecord.locator("td").nth(3);
    return statusColumn;
  }

  async clickOnVerifyRecordOnDNSRecordForm() {
    await this.verifyRecordButtonOnVerifyForm
      .filter({ hasText: "Verify Record" })
      .click();
  }

  async getCountOfEmailRecordsPresent() {
    return await this.eachEmailDomainRecordInList.count();
  }

  async closeTheDomainAdditionForm() {
    await this.cancelIconOnForm.click();
  }

  async getSendersEmailAddressWithDefaultBadge() {
    const defaultDomainEmailAddressLocator = this.eachEmailDomainRecordInList
      .filter({
        has: this.defaultEmailDomainSenderBadge,
      })
      .locator("td")
      .nth(0);
    return defaultDomainEmailAddressLocator;
  }

  async makeRecordDefault(sendersDomainName) {
    const domainRecordFromList = await this.getEmailDomainRecordFromList(
      sendersDomainName
    );
    await domainRecordFromList.locator(this.threeDotsButton).click();
    await this.makeDefaultOptionInMenu.click();
  }

  async openDNSRecordForEmail(sendersDomainName) {
    const domainRecordFromList = await this.getEmailDomainRecordFromList(
      sendersDomainName
    );
    await domainRecordFromList.locator(this.threeDotsButton).click();
    await this.viewDnsRecordOptionInMenu.click();
  }

  async deleteRecord(sendersDomainName) {
    const domainRecordFromList = await this.getEmailDomainRecordFromList(
      sendersDomainName
    );
    await domainRecordFromList.locator(this.threeDotsButton).click();
    await this.deleteOptionInMenu.click();
    await this.handleDeleteConfirmationPopup(true);
  }
}
