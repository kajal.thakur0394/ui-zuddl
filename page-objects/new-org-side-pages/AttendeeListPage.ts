import { Locator, Page, expect, Selectors } from "@playwright/test";
export class AttendeeListPage {
  readonly page: Page;
  readonly addAttendeeButton: Locator;
  readonly groupOfActionButtons: Locator;
  readonly attendeeCountInfoBar: Locator;
  readonly importCsvDropDownOptions: Locator;
  readonly firstNameInputBox: Locator;
  readonly lastNameInputBox: Locator;
  readonly emailIdInputBox: Locator;
  readonly attendeeFormSubmitButton: Locator;
  readonly sendEmailConfirmationDialogueButtons: Locator;
  readonly addAttendeeButtonAtCentre: Locator;
  readonly attendeeRowRecordLocator: Locator;
  readonly searchIconLocator: Locator;
  readonly searchInputBoxLocator: Locator;
  readonly dotIconToOpenMoreOption: string;
  readonly setDevieLimitButton: Locator;
  readonly notificationContainer: Locator;
  readonly inputBoxToAddListOfEmail: Locator;
  readonly importButtonOnUploadCsvForm: Locator;
  readonly csvFileInputContainer: Locator;
  readonly filePreviewOnFileUpload: Locator;
  constructor(page: Page) {
    this.page = page;
    this.addAttendeeButton = this.page.locator(
      "button:has-text('Add Attendee')"
    );
    this.groupOfActionButtons = this.page.locator(
      "div[class^='header-group_actionGroupBtns']"
    );
    this.attendeeCountInfoBar = this.page.locator(
      "div[class^='header-group_titleWrapper'] p"
    );
    this.importCsvDropDownOptions = this.page.locator(
      "div[class^='header-group_importCsvDropDown'] button"
    );
    //attendee form locator
    this.firstNameInputBox = this.page.locator("#firstName");
    this.lastNameInputBox = this.page.locator("#lastName");
    this.emailIdInputBox = this.page.locator("#email");
    this.attendeeFormSubmitButton = this.page.locator(
      "div[class^='add-attendee-form_footer'] button[type='submit']"
    );
    this.sendEmailConfirmationDialogueButtons = this.page.locator(
      "div[class^='confirm-dialog_buttonBlock'] button p"
    );
    this.addAttendeeButtonAtCentre = this.page.locator(
      "button[class*='CompactEmptyState_button'] p:has-text('Add Attendee')"
    );
    this.attendeeRowRecordLocator = this.page.locator(
      "table[class^='list-attendees-page_attendeeTable'] tbody tr"
    );
    this.searchIconLocator = this.groupOfActionButtons
      .locator("div[role='button']")
      .nth(0);
    this.searchInputBoxLocator = this.page.locator(
      "div[class^='search-bar_fullSearchWrapper'] input"
    );
    this.dotIconToOpenMoreOption =
      "div[class^='list-attendees-page_moreBlock'] div[role='button']";

    this.setDevieLimitButton = this.page.locator(
      "button:has-text('Set device limit')"
    );
    this.notificationContainer = this.page.locator("#notification-app");
    this.inputBoxToAddListOfEmail = this.page.locator("input#emailsList");
    this.importButtonOnUploadCsvForm = this.page.locator(
      "div[class^='upload-csv-modal_footer'] button"
    );
    this.csvFileInputContainer = this.page.locator(
      "div[class^='FileUploadBox_dropzoneContainer'] input"
    );
    this.filePreviewOnFileUpload = this.page.locator(
      "div[class^='FileUploadBox_previewFileContainer']"
    );
  }

  async clickOnEmptyStateAddAttendeeButton() {
    await this.addAttendeeButtonAtCentre.click();
  }

  async clickOnAddAttendeeButtonAppearingOnTopRight() {
    await this.groupOfActionButtons
      .locator("button:has-text('Add Attendee')")
      .click();
  }

  async clickOnSearchIcon() {
    await this.searchIconLocator.click();
  }

  async getCountOfAttendeeFromInfoBar() {
    const attendeeCountText = await this.attendeeCountInfoBar.textContent();
    console.log(attendeeCountText);
    const attendeeCount = attendeeCountText?.split(" ")[0];
    console.log(attendeeCount);

    if (attendeeCount != undefined || attendeeCount != " ") {
      return Number(attendeeCount);
    } else {
      throw new Error(
        `No numeric char fetched from attendee count string ${attendeeCountText}`
      );
    }
  }

  async downloadCsv() {
    await this.openCsvDropdown();
    await this.selectDownloadCsvFromCsvDropDown();
  }
  async openCsvDropdown() {
    await this.groupOfActionButtons.locator("button:text('CSV')").click();
  }

  async selectImportCsvFromCsvDropDown() {
    await this.importCsvDropDownOptions
      .filter({
        hasText: "Import CSV",
      })
      .click();
  }

  async verifyUploadCsvContainerIsVisible() {
    await expect(
      this.page.locator("div[class^='upload-csv-modal_container']")
    ).toBeVisible();
  }

  async triggerImportCsv(fileName: string) {
    await this.csvFileInputContainer.setInputFiles(fileName);
    await expect(this.filePreviewOnFileUpload).toBeVisible();
    await this.importButtonOnUploadCsvForm.click();
  }
  async addListOfEmailsForManualAddition(listOfEmails) {
    for (let eachEamil of listOfEmails) {
      await this.inputBoxToAddListOfEmail.type(eachEamil);
    }
  }

  async clickOnImportButtonToSubmitForm() {
    await this.importButtonOnUploadCsvForm.click();
  }
  async selectDownloadCsvFromCsvDropDown() {
    await this.importCsvDropDownOptions
      .filter({
        hasText: "Download CSV",
      })
      .click({ delay: 2000 });
  }

  async openAddAttendeeForm() {
    await this.clickOnAddAttendeeButtonAppearingOnTopRight();
  }

  async runEmptyStateChecksForAttendeeList() {
    await expect(async () => {
      expect(await this.getCountOfAttendeeFromInfoBar()).toBe(0);
    }, "Attendee count should be 0 in empty state").toPass();

    await expect(
      this.addAttendeeButtonAtCentre,
      "expecting add attendee button to be visible at centre of page in empty state"
    ).toBeVisible();

    await expect(
      this.addAttendeeButton,
      "expecting count of add attendee button to be 2"
    ).toHaveCount(2);
  }

  async fillTheAttendeeForm(attendeeFormData: AttendeeFormData) {
    await this.firstNameInputBox.fill(attendeeFormData.firstName);
    await this.lastNameInputBox.fill(attendeeFormData.lastName);
    await this.emailIdInputBox.fill(attendeeFormData.emailId);
    await this.attendeeFormSubmitButton.click();
  }

  async handleSendEmailConfirmationPopup(accept: boolean) {
    if (accept) {
      await this.sendEmailConfirmationDialogueButtons
        .filter({
          hasText: "Send email",
        })
        .click();
    } else {
      await this.sendEmailConfirmationDialogueButtons
        .filter({
          hasText: "Add without email",
        })
        .click();
    }
  }

  async copyAttendeeInvitationOnClipboard(attendeeEmail: string) {
    const rowObject = this.getAttendeeRowRecordByEmail(attendeeEmail);
    await rowObject.locator(this.dotIconToOpenMoreOption).click();
    await this.clickOnCopyInviteButton();
  }

  async verifyAttendeeRecordCountToBe(expectedCount) {
    await expect(
      this.attendeeRowRecordLocator,
      `expecting attendee row record to be ${expectedCount}`
    ).toHaveCount(expectedCount);
  }

  async clickOnContinueButtonOnMappingModal() {
    await this.page
      .locator("div[class^='custom-mapping-modal'] button")
      .filter({
        hasText: "Continue",
      })
      .click();
  }
  async verifyTheCountOfAttendeeToBe(expectedCount, customTimout = 5000) {
    // verify count visible as text
    await expect(async () => {
      expect(await this.getCountOfAttendeeFromInfoBar()).toBe(expectedCount);
    }, `Expecting attendee count visible on top info bar to be ${expectedCount}`).toPass(
      { timeout: customTimout }
    );
    // verify count of items in attendee list
    await expect(
      this.attendeeRowRecordLocator,
      `expecting attendee row record to be ${expectedCount}`
    ).toHaveCount(expectedCount);
  }

  async triggerSearchFor(searchString: string) {
    await this.searchInputBoxLocator.clear();
    await this.searchInputBoxLocator.fill(searchString);
  }

  async getAttendeeRecordColumnDataForRowIndex(
    rowIndex: number,
    dataToFetch = "status"
  ) {
    let data;
    const attendeeRowColumns = this.attendeeRowRecordLocator
      .nth(rowIndex)
      .locator("td");
    if (dataToFetch == "status") {
      data = await attendeeRowColumns.nth(3).textContent();
    }
    return data;
  }

  //get attendee record object by using email
  public getAttendeeRowRecordByEmail(attendeeEmail: string) {
    return this.attendeeRowRecordLocator
      .filter({
        hasText: attendeeEmail.split("@")[0],
      })
      .filter({
        hasText: attendeeEmail.split("@")[1],
      });
  }
  // change attendee status to checkin
  async updateAttendeeStatusToCheckin(attendeeEmail: string) {
    const rowObject = this.getAttendeeRowRecordByEmail(attendeeEmail);
    await rowObject.locator(this.dotIconToOpenMoreOption).click();
    await this.clickOnCheckInButtonFromHoverActionList();
    //wait for attendee record status to become checkedin
  }

  async resendConfirmationEmail(attendeeEmail: string) {
    const rowObject = this.getAttendeeRowRecordByEmail(attendeeEmail);
    await rowObject.locator(this.dotIconToOpenMoreOption).click();
    await this.clickOnResendInviteButton();
  }

  async deleteAttendeeRecord(attendeeEmail: string) {
    const rowObject = this.getAttendeeRowRecordByEmail(attendeeEmail);
    await rowObject.locator(this.dotIconToOpenMoreOption).click();
    await this.clickOnDeleteButton();
  }

  async handleDeleteConfirmationPopup(toDelete: true) {
    if (toDelete) {
      await this.page
        .locator("div[class*='attendees-page_actions'] button")
        .filter({
          hasText: "Delete",
        })
        .click();
    } else {
      await this.page
        .locator("div[class*='attendees-page_actions'] button")
        .filter({
          hasText: "Cancel",
        })
        .click();
    }
  }

  async verifyNotifcationPopsUpWithMessage(expectedMessage: string) {
    await expect(
      this.notificationContainer,
      `Expecting notification to have message ${expectedMessage}`
    ).toHaveText(expectedMessage);
  }

  async clickOnResendInviteButton() {
    await this.page.locator("button:has-text('Resend invite')").click();
  }

  async clickOnDeleteButton() {
    await this.page.locator("button:has-text('Delete')").click();
  }
  async clickOnCopyInviteButton() {
    await this.page.locator("button:has-text('Copy invite')").click();
  }

  async verifyAttendeeStatusToBe(attendeeEmail: string, expectedSatus: string) {
    // now get the current state of attendee
    const attendeeRowRecord = this.getAttendeeRowRecordByEmail(attendeeEmail);
    const attendeeRecordStatus = attendeeRowRecord.locator("td").nth(3);
    await expect(
      attendeeRecordStatus,
      `Expecting ${attendeeEmail} to have ${expectedSatus}`
    ).toHaveText(expectedSatus);
  }
  async clickOnCheckInButtonFromHoverActionList() {
    await this.page.locator("button:has-text('Check-in')").click();
  }

  async setDeviceLimit(options: {
    noOfDeviceAllowed: string;
    applyToAllAttendee: boolean;
  }) {
    await this.groupOfActionButtons
      .locator("div[class^='header-group_moreBlock'] div[role='button']")
      .click();
    await this.setDevieLimitButton.click();
    await this.page.locator("#loginLimitCount").click();
    await this.page
      .locator("#loginLimitCount")
      .selectOption({ value: options.noOfDeviceAllowed });
    if (options.applyToAllAttendee) {
      await this.page.locator("div[class^='Checkbox_inputContainer']").click();
    }
    await this.page.click("text=Save");
  }
}

export interface AttendeeFormData {
  firstName: string;
  lastName: string;
  emailId: string;
  ticketType?: string;
  title?: string;
  organisation?: string;
}
