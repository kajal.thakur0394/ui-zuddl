import { Locator, Page, expect } from "@playwright/test";
import { DataUtl } from "../../util/dataUtil";
export class VenueSetup {
  readonly page: Page;
  readonly uploadCsvButton: Locator;
  readonly broswerFileButton: Locator;
  readonly importFileButton: Locator;
  readonly succesfullyUploadedMessageLocator: Locator;
  readonly stageSetupAdvanceButton: Locator;
  readonly stageReactionsSection: Locator;
  readonly emojiLocator: Locator;
  readonly selectedEmojiContainer: Locator;

  constructor(page: Page) {
    this.page = page;

    this.uploadCsvButton = this.page.locator(
      "div[class^='venue-schedule_imageAction']"
    );

    this.broswerFileButton = this.page.locator(
      "div[class*='FileUploadBox'] input"
    );

    this.importFileButton = this.page.getByRole("button", { name: "Import" });

    this.succesfullyUploadedMessageLocator = this.page.getByText(
      "Successfully imported"
    );

    this.stageSetupAdvanceButton = this.page.getByText("Advanced");

    this.stageReactionsSection = this.page.getByText("Reactions");

    this.emojiLocator = this.page.locator(
      "div[class*='emoji-list_emojiContainer']"
    );

    this.selectedEmojiContainer = this.page.locator(
      "div[class*='list_selectedEmojiContainer']"
    );
  }

  async navigateToVenueSetup(zone, eventId) {
    let organiserSetupUrl = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/event/${eventId}/venue-setup/${zone}`;

    await this.page.goto(organiserSetupUrl);
  }

  async uploadScheduleCSVFile(fileName) {
    await this.uploadCsvButton.click();

    let filePath = `test-data/${fileName}`;
    await this.broswerFileButton.setInputFiles(filePath);

    let fileNameLocator = this.page.locator(`//div[text()='${fileName}']`);
    await expect(fileNameLocator).toBeVisible({ timeout: 30000 });
    await this.importFileButton.click();
    await expect(this.succesfullyUploadedMessageLocator).toBeVisible({
      timeout: 30000,
    });
  }

  async selectEmojisOfChoice(stageId, eventId) {
    let stageSetupPage = `stages/${stageId}`;
    await this.navigateToVenueSetup(stageSetupPage, eventId);

    await this.stageSetupAdvanceButton.click();
    await this.stageReactionsSection.click();

    for (let i = 1; i <= 5; i++) {
      await this.selectedEmojiContainer.first().click();

      await expect(this.selectedEmojiContainer).toHaveCount(5 - i, {
        timeout: 30000,
      });
    }

    await expect(this.selectedEmojiContainer).not.toBeVisible({
      timeout: 2000,
    });

    let listOfNumbers = await this.getFiveRandomNumberArr(1, 186);

    for (var i = 0; i < 5; i++) {
      let emojiToSelect = this.emojiLocator.nth(listOfNumbers[i]);
      await emojiToSelect.click();

      await expect(this.selectedEmojiContainer).toHaveCount(i + 1, {
        timeout: 30000,
      });
    }
  }

  private async getFiveRandomNumberArr(startNumber: number, endNumber: number) {
    let nums = new Array();

    for (var i = 0; i < 5; i++) {
      nums.push(
        Math.floor(Math.random() * (endNumber - startNumber) + startNumber) + 1
      );
    }

    console.log(nums);
    return nums;
  }
}
