import { Page } from "@playwright/test";
import EventEntryType from "../../enums/eventEntryEnum";
import { EmailDomainRestrictionComponent } from "./EmailDomainRestrictionComponent";
export class AdvancedSettingsPage {
  readonly page: Page;
  readonly emailDomainRestrictionComponent: EmailDomainRestrictionComponent;

  constructor(page: Page) {
    this.page = page;
    this.emailDomainRestrictionComponent = new EmailDomainRestrictionComponent(
      this.page
    );
  }

  async open(url: string) {
    await this.page.goto(url);
    return this;
  }

  async getEmailDomainRestrictionComponent() {
    return this.emailDomainRestrictionComponent;
  }

  async selectEventRegistrationStyle(eventType: EventEntryType) {}
}
