import { expect, Locator, Page } from "@playwright/test";
import {
  AudienceTypeEnum,
  TriggerTypeEnum,
  CustomCommTypeEnum,
} from "../../enums/CustomCommunicationEnums";
import { DataUtl } from "../../util/dataUtil";

export class CustomCommunicationPage {
  readonly page: Page;
  readonly eventId;

  readonly createNewButton: Locator;
  readonly communicationName: Locator;
  readonly audiencesDropdown: Locator;
  readonly sendNowRadioInput: Locator;
  readonly scheduleForLaterRadioInput: Locator;
  readonly triggerBasedRadioInput: Locator;
  readonly triggerDropdown: Locator;
  readonly communicationStatusToggleInput: Locator;
  readonly communicationToggleStatus: Locator;
  readonly proceedButton: Locator;
  readonly senderNameInput: Locator;
  readonly emailSubjectInput: Locator;
  readonly senderEmailDropdown: Locator;
  readonly zuddlDefaultSender: Locator;
  readonly editDetailsButton: Locator;
  readonly editButton: Locator;
  readonly deleteButton: Locator;
  readonly saveButton: Locator;
  readonly sendNow: Locator;
  readonly undoButton: Locator;
  readonly selectDateTime: Locator;
  readonly highlightedDate: Locator;
  readonly timePassedPopUp: Locator;
  readonly selectedMeridian: Locator;
  readonly amButton: Locator;
  readonly pmButton: Locator;
  readonly doneButton: Locator;

  constructor(page: Page, eventId) {
    this.page = page;
    this.eventId = eventId;

    this.createNewButton = this.page
      .locator("div")
      .filter({ hasText: /^Pre-eventLive eventPost-eventCreate new$/ })
      .getByRole("button");
    this.communicationName = this.page.locator(
      "input[placeholder='Enter a name']"
    );
    this.audiencesDropdown = this.page.getByText("Choose audiences");
    this.sendNowRadioInput = this.page
      .locator("div")
      .filter({ hasText: /^Send now$/ })
      .locator("input");
    this.scheduleForLaterRadioInput = this.page
      .locator("div")
      .filter({ hasText: /^Schedule for later$/ })
      .locator("input");
    this.triggerBasedRadioInput = this.page
      .locator("div")
      .filter({ hasText: /^Trigger based$/ })
      .locator("input");
    this.communicationStatusToggleInput = this.page.locator(
      "//p[contains(text(),'Communication status')]/following-sibling::div//input"
    );
    this.communicationToggleStatus = this.page.locator(
      "//p[contains(text(),'Communication status')]/following-sibling::div//p"
    );
    this.triggerDropdown = this.page.locator(
      "//p[contains(text(), 'Select trigger')]/following-sibling::div"
    );
    this.proceedButton = this.page.getByRole("button", { name: "Proceed" });
    this.senderNameInput = this.page.getByLabel("Sender name");
    this.emailSubjectInput = this.page.getByLabel("Email subject");
    this.senderEmailDropdown = this.page.getByLabel("Sender email");
    this.zuddlDefaultSender = this.page.getByLabel("Zuddl default sender");
    this.editDetailsButton = this.page.getByRole("button", {
      name: "Edit details",
      exact: true,
    });
    this.editButton = this.page.getByRole("button", {
      name: "Edit",
      exact: true,
    });
    this.deleteButton = this.page.getByRole("button", {
      name: "Delete",
      exact: true,
    });
    this.saveButton = this.page.getByRole("button", { name: "Save" });
    this.sendNow = this.page.getByRole("button", {
      name: "Send now",
    });
    this.undoButton = this.page.getByRole("button", {
      name: "Undo",
    });
    this.selectDateTime = this.page.getByLabel("Select date and time");
    this.timePassedPopUp = this.page.locator(
      "//p[contains(text(), 'The selected time has already passed')]"
    );
    this.selectedMeridian = this.page.locator(
      "button[class*='date-time-field_meridianButtonSelected']"
    );
    this.amButton = this.page.getByRole("button").filter({ hasText: "AM" });
    this.pmButton = this.page.getByRole("button").filter({ hasText: "PM" });
    this.doneButton = this.page.locator("//div[text()='Done']");
  }

  private async toggleCommunicationStatus(turnCommunicationOn: boolean) {
    const currentStatus = await this.communicationToggleStatus.textContent();

    let commToggle = this.page.locator(
      "//label[contains(@class, 'Toggle_switch')]"
    );
    if (currentStatus === "Inactive" && turnCommunicationOn) {
      await commToggle.click();
      const updatedStatus = await this.communicationToggleStatus.textContent();
      expect(updatedStatus).toBe("Active");
    } else if (currentStatus === "Active" && !turnCommunicationOn) {
      await commToggle.click();
      const updatedStatus = await this.communicationToggleStatus.textContent();
      expect(updatedStatus).toBe("Inactive");
    }
  }

  private async toggleCalenderBlocker(calendarBlockerStatus: boolean) {
    let calendarBlockerToggleInput = this.page.locator(
      "//label[contains(@class,'Toggle_switch')]"
    );
    let calendarBlockerToggle = this.page.locator("#app label").nth(2);

    if (
      (await calendarBlockerToggleInput.isChecked()) &&
      calendarBlockerStatus
    ) {
      await calendarBlockerToggle.click();
      await expect(calendarBlockerToggleInput).not.toBeChecked();
    } else if (
      !(await calendarBlockerToggleInput.isChecked()) &&
      !calendarBlockerStatus
    ) {
      await calendarBlockerToggle.click();
      await expect(calendarBlockerToggleInput).toBeChecked();
    }
  }

  private async createAudienceLocator(audience: AudienceTypeEnum) {
    let locator = this.page.getByRole("button", { name: `${audience}` });
    console.log(`Audience locator -> ${locator}`);
    return locator;
  }

  async verifyOnlyOneCommTypeCanBeSelectedAtATime() {
    let customCommUrl = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/event/${this.eventId}/communication/custom`;
    await this.page.goto(customCommUrl);
    await this.createNewButton.click();

    await this.sendNowRadioInput.click();
    await expect(this.sendNowRadioInput).toBeChecked();
    await expect(this.scheduleForLaterRadioInput).not.toBeChecked();
    await expect(this.triggerBasedRadioInput).not.toBeChecked();

    await this.scheduleForLaterRadioInput.click();
    await expect(this.scheduleForLaterRadioInput).toBeChecked();
    await expect(this.sendNowRadioInput).not.toBeChecked();
    await expect(this.triggerBasedRadioInput).not.toBeChecked();

    await this.triggerBasedRadioInput.click();
    await expect(this.triggerBasedRadioInput).toBeChecked();
    await expect(this.scheduleForLaterRadioInput).not.toBeChecked();
    await expect(this.sendNowRadioInput).not.toBeChecked();
  }

  async verifyScheduleTimeCannotBeInPast(
    audiencesTypes: Array<AudienceTypeEnum>,
    commName: string
  ) {
    let customCommUrl = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/event/${this.eventId}/communication/custom`;
    await this.page.goto(customCommUrl);
    await this.createNewButton.click();
    await this.communicationName.fill(commName);
    await this.audiencesDropdown.click();

    let i = 0;
    for (i = 0; i < audiencesTypes.length; i++) {
      let audienceLoctor = await this.createAudienceLocator(audiencesTypes[i]);
      await audienceLoctor.click();
    }

    await this.scheduleForLaterRadioInput.click();
    await this.selectDateTime.click();

    let highlightedDate: Locator = this.page
      .locator('//div[contains(@class,"date-time-field_selected")]/..')
      .first();

    let date = await highlightedDate.getAttribute("data-value");
    let month = await highlightedDate.getAttribute("data-month");

    let hourInput: Locator = this.page
      .locator("input[class*='date-time-field_timeInput']")
      .first();

    let hourString = await hourInput.getAttribute("value");
    console.log("Hours fectched ->", hourString);
    let h = parseInt(hourString);
    let hourToPass = h - 3;
    console.log("Hours ->", hourToPass);

    if (hourToPass <= 0) {
      hourToPass = 12;
      await hourInput.fill(hourToPass.toString());
      await this.changeMeridian();
    } else if (h === 12) {
      await hourInput.fill(hourToPass.toString());
      await this.changeMeridian();
    } else {
      await hourInput.fill(hourToPass.toString());
    }

    await this.doneButton.click();
    await this.proceedButton.click();
    await expect(this.timePassedPopUp).toBeVisible({ timeout: 10000 });
  }

  private async changeMeridian() {
    let currentMeridian = await this.selectedMeridian.textContent();
    if (currentMeridian == "PM") {
      await this.amButton.click();
    } else {
      await this.pmButton.click();
    }
  }

  async createNewCustomCommunicationDetailsPage(
    audiencesTypes: Array<AudienceTypeEnum>,
    commSendType: "Send now" | "Schedule" | "Trigger",
    commName: string,
    communicationStatusToggle = true,
    trigger?: TriggerTypeEnum
  ) {
    let customCommUrl = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/event/${this.eventId}/communication/custom`;

    await this.page.goto(customCommUrl);
    await this.createNewButton.click();
    await this.communicationName.fill(commName);
    await this.audiencesDropdown.click();

    let i = 0;
    for (i = 0; i < audiencesTypes.length; i++) {
      let audienceLoctor = await this.createAudienceLocator(audiencesTypes[i]);
      await audienceLoctor.click();
    }

    if (commSendType === "Send now") {
      await this.sendNowRadioInput.click();
    } else if (commSendType === "Schedule") {
      await this.scheduleForLaterRadioInput.click();
      await this.toggleCommunicationStatus(communicationStatusToggle);
    } else {
      await this.triggerBasedRadioInput.click();
      try {
        if (trigger != null) {
          await this.triggerDropdown.click();
          await this.page.getByText(trigger, { exact: true }).click();
        }
      } catch (error) {
        console.log("Please provide a proper trigger");
      }
    }
    await this.proceedButton.click();
  }

  async createNewCustomCommunicationEmailPage(
    senderName: string,
    emailSubject: string,
    toggleCalenderBlocker = false
  ) {
    // await this.senderEmailDropdown.click();
    // await this.zuddlDefaultSender.click();
    await expect(this.senderNameInput).toBeVisible({
      timeout: 10000,
    });
    await this.senderNameInput.fill(senderName);
    await this.emailSubjectInput.fill(emailSubject);
    await this.toggleCalenderBlocker(toggleCalenderBlocker);
    await this.proceedButton.click();
  }

  async newCustomCommunicationReviewPage(sendNow = true, edit = false) {
    await expect(this.editButton).toBeVisible({ timeout: 10000 });
    if (edit) {
      await this.editButton.click();
    } else if (sendNow) {
      await this.sendNow.click();
      //clicking pop-up "Send now" button
      await this.sendNow.click();
    } else {
      try {
        await this.saveButton.click();
      } catch (error) {
        await this.saveButton.click();
      }
    }
  }

  async navigateToCustomCommTypePage(customCommType: CustomCommTypeEnum) {
    let tabLocator = this.page.locator(
      `//p[contains(@class, 'custom-communication_tab') and contains(text(),'${customCommType}')]`
    );
    await tabLocator.click();

    let selectedTabLocator = this.page.locator(
      `//p[contains(@class,'custom-communication_selectedTab') and contains(text(),'${customCommType}')]`
    );
    await expect(selectedTabLocator).toBeVisible({ timeout: 10000 });
  }

  async customCommToggleSwitch(
    customCommType: CustomCommTypeEnum,
    customCommunicationName: string,
    toggle: boolean
  ) {
    let toggleLocator = this.page.locator(
      `//p[contains(text(), '${customCommunicationName}')]/../../..//div//label`
    );

    let toggleOnLocator = this.page.locator(
      `//p[contains(text(), '${customCommunicationName}')]/../../..//div//label[contains(@class,'Toggle_switchOn')]`
    );

    await this.navigateToCustomCommTypePage(customCommType);

    if (toggle) {
      try {
        await expect(toggleOnLocator).toBeVisible({ timeout: 5000 });
      } catch (error) {
        await toggleLocator.click();
        await expect(toggleOnLocator).toBeVisible({ timeout: 5000 });
      }
    } else {
      try {
        await expect(toggleOnLocator).not.toBeVisible({ timeout: 5000 });
      } catch (error) {
        await toggleLocator.click();
        await expect(toggleOnLocator).not.toBeVisible({ timeout: 5000 });
      }
    }
  }

  async verifyToggleStatus(
    customCommName: string,
    communicationType: CustomCommTypeEnum,
    communicationStatus: "Active" | "Inactive" | "Completed",
    timeout: number = 300
  ) {
    await this.navigateToCustomCommTypePage(communicationType);
    let commLabelLocator = this.page.getByText(`${customCommName}`, {
      exact: true,
    });
    await expect(commLabelLocator).toBeVisible({ timeout: 10000 });

    let statusTextLocator = this.page.locator(
      `//p[contains(text(), '${customCommName}')]/../following-sibling::div//p[2]`
    );

    await expect(this.undoButton).not.toBeVisible({ timeout: 80000 });

    let statusText;
    while (timeout > 0) {
      statusText = await statusTextLocator.textContent();
      if (statusText === communicationStatus) {
        break;
      } else {
        this.page.waitForTimeout(5000);
        timeout = timeout - 5;
      }
    }
    expect(statusText).toBe(communicationStatus);
  }

  async verifyToggleButtonIsDisabled(
    customCommName: string,
    communicationType: CustomCommTypeEnum
  ) {
    await this.navigateToCustomCommTypePage(communicationType);
    let commLabelLocator = this.page.getByText(`${customCommName}`, {
      exact: true,
    });
    await expect(commLabelLocator).toBeVisible({ timeout: 10000 });

    let toggleLocator = this.page.locator(
      `//p[contains(text(), '${customCommName}')]/../following-sibling::div//label[contains(@class, 'Toggle_switchDisabled')]`
    );

    await expect(toggleLocator).toBeVisible({ timeout: 8000 });
  }

  async verifyUndoRedirectsToEditPage() {
    await expect(this.undoButton).toBeVisible({ timeout: 12000 });
    await this.undoButton.click();
    await expect(this.editButton).toBeVisible({ timeout: 12000 });
  }

  async verifyCommunicationIsEditable(
    customCommName: string,
    communicationType: CustomCommTypeEnum
  ) {
    await this.navigateToCustomCommTypePage(communicationType);
    let commLabelLocator = this.page.getByText(`${customCommName}`, {
      exact: true,
    });
    await expect(commLabelLocator).toBeVisible({ timeout: 10000 });

    let customCommMenuIcon = this.page.locator(
      `//p[contains(text(), '${customCommName}')]/../following-sibling::div/div`
    );
    await customCommMenuIcon.click();

    await this.editButton.click();

    let reviewPage = this.page.locator(
      "div[class*='notification-review_notificationDetails']"
    );

    await expect(reviewPage).toBeVisible({ timeout: 5000 });
  }

  async verifyCommunicationIsDeleted(
    customCommName: string,
    communicationType: CustomCommTypeEnum
  ) {
    await this.navigateToCustomCommTypePage(communicationType);
    let commLabelLocator = this.page.getByText(`${customCommName}`, {
      exact: true,
    });
    await expect(commLabelLocator).toBeVisible({ timeout: 10000 });

    let customCommMenuIcon = this.page.locator(
      `//p[contains(text(), '${customCommName}')]/../following-sibling::div/div`
    );
    await customCommMenuIcon.click();
    await this.deleteButton.click();

    const deleteCustomCommResponse = await this.page.waitForResponse(/DELETED/);
    console.log(
      "Delete Custom Communication Response Status ->",
      deleteCustomCommResponse.status()
    );
    expect(deleteCustomCommResponse.status()).toBe(200);
  }
}
