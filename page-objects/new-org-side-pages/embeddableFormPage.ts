import { Locator, Page } from "@playwright/test";
import { AttendeeListPage } from "./AttendeeListPage";
import { SpeakersListPage } from "./SpeakersListPage";
export class EmbeddableFormPage {
  readonly page: Page;
  readonly eachMenuItemLocator: Locator;
  readonly embedButtonLocator: Locator;
  constructor(page: Page) {
    this.page = page;
    this.embedButtonLocator = this.page.locator("//button[text()='Embed']");
  }

  async verifyEmbedButtonIsVisible() {
    await this.embedButtonLocator.isVisible();
  }

  async clickEmbedButton() {
    await this.embedButtonLocator.click();
  }

  async verifyHtmlCodeisVisible() {
    await this.page.locator("div[class^='embed-form-popup_embedContainer']").isVisible();
  }

  async copyHtmlCode() {
    let htmlCode = await this.page.locator("textarea[class^='textarea_input']").textContent();
    console.log("The html code",htmlCode)
    let dataEventId = htmlCode.toString().split("=")[2].toString().split(/[\s"]+/)[1]
    console.log(dataEventId)
    let dataEnv = htmlCode.toString().split("=")[3].toString().split(/[\s"]+/)[1]
    console.log(dataEnv)
    return [dataEventId,dataEnv];
  }
}
