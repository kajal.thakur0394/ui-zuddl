import { Locator, Page, expect } from "@playwright/test";
export class EmailTemplateEditPage {
  readonly page: Page;
  readonly sendersEmailDropdown: Locator;
  readonly senderNameInputBox: Locator;
  readonly emailSubjectInputBox: Locator;
  readonly activeToggleButton: Locator;
  readonly useEmailBuilderButton: Locator;
  readonly dropDownOptions: Locator;

  constructor(page: Page) {
    this.page = page;
    this.activeToggleButton = this.page
      .locator(
        "div[class^='email-notification-setting_activeToggleContainer']:has-text('Active')"
      )
      .locator("label");
    this.sendersEmailDropdown = this.page.locator(
      "div[class^='email-notification-setting_contentContainer']:has-text('Sender email') div[class$='control']"
    );
    this.senderNameInputBox = this.page.locator("input#from");
    this.emailSubjectInputBox = this.page.locator("textarea#emailSubject");
    this.useEmailBuilderButton = this.page.locator(
      "button:has-text('Use email builder')"
    );
    this.dropDownOptions = this.page.locator("div[class$='option']");
  }

  async verifyTheSendersEmailDropDownIsDisabled() {
    await expect(
      this.sendersEmailDropdown.locator("input"),
      "verify the senders email dropdown is disabled"
    ).toBeDisabled();
  }

  async enableActiveTemplateToggle() {
    await this.activeToggleButton.click();
  }

  async selectEmailDromSendersEmailDropdown(
    emailToSelect: string
  ): Promise<boolean> {
    await this.sendersEmailDropdown.click();
    //wait until the dropdown options are visible
    await expect(this.dropDownOptions.nth(0)).toBeVisible();
    //iterate through the dropdown options and select the email
    let listOfOptions = await this.dropDownOptions.count();
    console.log(`total options found in dropdown = ${listOfOptions}`);
    let emailFound = false;
    for (let i = 0; i < listOfOptions; i++) {
      //fetch text of option
      let currentDropDownOptionValue = await this.dropDownOptions
        .nth(i)
        .textContent();
      console.log(`current dropdown value is ${currentDropDownOptionValue}`);
      if (
        currentDropDownOptionValue?.toLowerCase() == emailToSelect.toLowerCase()
      ) {
        console.log(`matching email found in dropdown, clicking on it`);
        await this.dropDownOptions.nth(i).click();
        emailFound = true;
        break;
      }
    }
    if (emailFound == false) {
      throw new Error(
        `${emailToSelect} not found in the dropdown of senders emails`
      );
    }
    return true;
  }

  async updateSendersName(sendersName: string) {
    await this.senderNameInputBox.fill(sendersName);
  }

  async updateEmailSubject(emailSub: string) {
    await this.emailSubjectInputBox.fill(emailSub);
  }
}
