import { Page } from "@playwright/test";
export class ChooseRegStyleComponent {
  readonly page: Page;

  constructor(page: Page) {
    this.page = page;
  }

  async clickOnContinueButton() {
    await this.page.click("text=Continue");
  }
}
