import { Locator, Page, expect, test } from "@playwright/test";
import { DataUtl } from "../../util/dataUtil";
import EventType from "../../enums/eventTypeEnum";

export class EventSetupPage {
  readonly page: Page;
  readonly eachMenuItemLocator: Locator;
  readonly embeddableFormSelector: Locator;

  constructor(page: Page) {
    this.page = page;
    this.eachMenuItemLocator = this.page.locator(
      "span[class^='item_listItemBody']"
    );
    this.embeddableFormSelector = this.page.locator(
      "//p[text()='Embeddable form']"
    );
  }

  async openEventSetupBasicTab() {
    await this.eachMenuItemLocator
      .filter({
        hasText: "Event Setup",
      })
      .click();
    await this.page
      .locator("li[class^='item_listItemWrapper']")
      .filter({
        hasText: "Basic",
      })
      .nth(1)
      .click();
  }

  async changeEventType(eventType: EventType, eventId) {
    const apiEndpointForUpdateEvent = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/api/event/${eventId}`;
    const updateEventDetailsApiPromise = this.page.waitForRequest(
      (request) =>
        request.url() === apiEndpointForUpdateEvent &&
        request.method() === "POST"
    );
    const highLightedButton = this.page.locator(
      "button[class*='basic-form_eventFormatButtonSelected']"
    );

    await this.page.getByText(eventType).click();
    //verify the button is highligghted as selected have expected text
    await expect(
      highLightedButton,
      `expecting ${eventType} button to be highlighted`
    ).toContainText(eventType, { ignoreCase: true });
    await this.page.getByText("Save").click();
    await this.page
      .locator("h6[class*='confirmation-popup_title']")
      .waitFor({ state: "visible", timeout: 30000 });
    await this.page.getByText("Confirm").click();
    const updateEventDetailsReqPromise = await updateEventDetailsApiPromise;
    const updateEventDetailResp = await updateEventDetailsReqPromise.response();
    expect(
      updateEventDetailResp!.status(),
      "expecting POST API call for update event to be 200"
    ).toBe(200);
  }

  async verifyEventType(eventType: string) {
    let selectedEventType: Locator = this.page
      .locator("button[class*='basic-form_eventFormatButtonSelected']")
      .filter({
        hasText: eventType,
      });
    await expect(
      selectedEventType,
      "Selected event type to be Hybrid"
    ).toBeVisible();
  }

  async changeEventStartTimeFromUI(
    eventHourStartTime: string,
    eventMinutesStartTime: string,
    eventStartTimeMeridian: string,
    eventId,
    calendar: boolean = false,
    resendInvites: boolean = false
  ) {
    const apiEndpointForUpdateEvent = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/api/event/${eventId}`;
    const updateEventDetailsApiPromise = this.page.waitForResponse(
      apiEndpointForUpdateEvent
    );
    let selectedEventStartDate: Locator = this.page
      .locator("div[class^='date-time-field_withIconAndInputContainer']")
      .first();
    await expect(
      selectedEventStartDate,
      "Event start to be visible"
    ).toBeVisible();
    await selectedEventStartDate.click();

    let dateTimeInputLocator = this.page.locator(
      "input[class*='date-time-field_timeInput']"
    );
    let eventStartTimeHour: Locator = dateTimeInputLocator.first();
    let eventStartTimeMinutes: Locator = dateTimeInputLocator.nth(1);

    let eventStartTimeMeridianLocator: Locator = this.page.getByRole("button", {
      name: eventStartTimeMeridian,
    });
    await expect(
      eventStartTimeHour,
      "Selected event start time Hour"
    ).toBeVisible();
    await test.step(`Changing event start time as ${
      eventHourStartTime + eventMinutesStartTime + eventStartTimeMeridian
    }`, async () => {
      await eventStartTimeHour.click();
      await eventStartTimeHour.fill(eventHourStartTime);
      await eventStartTimeMinutes.click();
      await eventStartTimeMinutes.fill(eventMinutesStartTime);
      await eventStartTimeMeridianLocator.click();
    });

    let doneButton: Locator = this.page.locator(
      "button[class*='date-time-field_saveButton']"
    );
    await doneButton.click();

    let saveButton = this.page.getByRole("button").filter({ hasText: "Save" });
    await saveButton.click();

    await this.page.waitForTimeout(20000);

    let resendInvitesPopup: Locator = this.page.getByRole("heading", {
      name: "Event settings saved",
    });

    if (resendInvites) {
      await resendInvitesPopup.waitFor({ state: "visible", timeout: 5000 });
      await expect(
        resendInvitesPopup,
        "Resend invites popup to be visible"
      ).toBeVisible({ timeout: 10000 });
    }
    if (await resendInvitesPopup.isVisible({ timeout: 30000 })) {
      let resendInvitesPopupConfirmButton: Locator = this.page.getByRole(
        "button",
        { name: "Confirm", exact: true }
      );
      let doNotSendCheckbox: Locator = this.page.getByRole("button", {
        name: "Resend updated calendar",
      });
      if (!resendInvites) {
        if (calendar) {
          await doNotSendCheckbox.click();
        }
      }
    }

    let popupConfirmButton: Locator = this.page.getByRole("button", {
      name: "Confirm",
    });
    await popupConfirmButton.click({
      timeout: 20000,
    });

    const updateEventDetailsPromise = await updateEventDetailsApiPromise;
    expect(
      updateEventDetailsPromise.status(),
      "expecting POST API call for update event to be 200"
    ).toBe(200);
  }

  async verifyEventStartTime(
    eventHourStartTime: string,
    eventMinutesStartTime: string,
    eventStartTimeMeridian: string
  ) {
    let eventStartTimeValue = await this.page
      .locator("input[class^='date-time-field_input']")
      .first()
      .getAttribute("value");
    // Get start time text
    expect(
      eventStartTimeValue,
      `Selected event time in hour to contain hour ${eventHourStartTime}`
    ).toContain(eventHourStartTime);
    expect(
      eventStartTimeValue,
      `Selected event time in hour to contain minute ${eventMinutesStartTime}`
    ).toContain(eventMinutesStartTime);
    expect(
      eventStartTimeValue,
      `Selected event time in hour to contain meridain ${eventStartTimeMeridian}`
    ).toContain(eventStartTimeMeridian);
  }
}
