import { Locator, Page, expect } from "@playwright/test";
export class SpeakersListPage {
  readonly page: Page;

  //form locators
  readonly firstNameInputBox: Locator;
  readonly lastNameInputBox: Locator;
  readonly emailIdInputBox: Locator;
  readonly designationInputBox: Locator;
  readonly organisationInputBox: Locator;
  readonly textAreaDescriptionBox: Locator;
  readonly speakerFormSubmitButton: Locator;
  readonly sendEmailConfirmationDialogueButtons: Locator;

  //common ui buttons
  readonly addSpeakerButton: Locator;
  readonly groupOfActionButtons: Locator;
  readonly speakerCountInfoBar: Locator;
  readonly importCsvDropDownOptions: Locator;
  readonly addSpeakerButtonAtCentre: Locator;

  readonly speakerRowRecordLocator: Locator;
  readonly searchIconLocator: Locator;
  readonly searchInputBoxLocator: Locator;
  readonly dotIconToOpenMoreOption: string;
  readonly setDevieLimitButton: Locator;
  readonly notificationContainer: Locator;
  readonly inputBoxToAddListOfEmail: Locator;
  readonly importButtonOnUploadCsvForm: Locator;
  readonly csvFileInputContainer: Locator;
  readonly filePreviewOnFileUpload: Locator;

  constructor(page: Page) {
    this.page = page;
    this.addSpeakerButton = this.page.locator("button:has-text('Add Speaker')");
    this.groupOfActionButtons = this.page.locator(
      "div[class^='list-speakers-page_actionGroupBtns']"
    );
    this.speakerCountInfoBar = this.page.locator(
      "div[class^='plan-restriction-info_infoContainer'] p"
    );
    this.importCsvDropDownOptions = this.page.locator(
      "div[class^='list-speakers-page_importCsvDropDown'] button"
    );
    //speaker form locator
    this.firstNameInputBox = this.page.locator("#firstname");
    this.lastNameInputBox = this.page.locator("#lastname");
    this.emailIdInputBox = this.page.locator("#email");
    this.designationInputBox = this.page.locator("#designation");
    this.organisationInputBox = this.page.locator("#organization");
    this.textAreaDescriptionBox = this.page.locator("#description");
    this.speakerFormSubmitButton = this.page.locator(
      "div[class^='add-speaker-form_footer'] button"
    );

    this.sendEmailConfirmationDialogueButtons = this.page.locator(
      "div[class^='confirm-dialog_buttonBlock'] button p"
    );
    this.addSpeakerButtonAtCentre = this.page.getByRole('button',
      { name: 'Add Speaker', exact: true }
    );
    this.speakerRowRecordLocator = this.page.locator(
      "table[class^='list-speakers-page_speakerTable'] tbody tr"
    );

    this.searchIconLocator = this.groupOfActionButtons
      .locator("div[role='button']")
      .nth(0);

    this.searchInputBoxLocator = this.page.locator(
      "div[class^='list-speakers-page_fullSearchWrapper'] input"
    );

    this.dotIconToOpenMoreOption =
      "div[class^='list-speakers-page_moreBlock'] div[role='button']";

    this.notificationContainer = this.page.locator("#notification-app");

    // upload csv modal
    this.importButtonOnUploadCsvForm = this.page.locator(
      "div[class^='speaker-upload-csv-modal_footer'] button"
    );
    this.csvFileInputContainer = this.page.locator(
      "div[class^='FileUploadBox_dropzoneContainer'] input"
    );
    this.filePreviewOnFileUpload = this.page.locator(
      "div[class^='FileUploadBox_previewFileContainer']"
    );
  }

  async clickOnEmptyStateAddSpeakerButton() {
    await this.addSpeakerButtonAtCentre.click();
  }

  async clickOnAddSpeakerButtonAppearingOnTopRight() {
    await this.groupOfActionButtons
      .locator("button:has-text('Add Speaker')")
      .click();
  }

  async clickOnSearchIcon() {
    await this.searchIconLocator.click();
  }

  async getCountOfSpeakersFromInfoBar() {
    const speakerCountText = await this.speakerCountInfoBar.textContent();
    console.log(speakerCountText);
    const speakerCount = speakerCountText?.split(": ")[1];
    console.log(speakerCount);

    if (speakerCount != undefined || speakerCount != " ") {
      return Number(speakerCount);
    } else {
      throw new Error(
        `No numeric char fetched from speakers count string ${speakerCountText}`
      );
    }
  }

  async openCsvDropdown() {
    await this.groupOfActionButtons.locator("button:text('CSV')").click();
  }

  async selectImportCsvFromCsvDropDown() {
    await this.importCsvDropDownOptions
      .filter({
        hasText: "Import CSV",
      })
      .click();
  }

  async verifyUploadCsvContainerIsVisible() {
    await expect(
      this.page.locator("div[class^='speaker-upload-csv-modal_container']")
    ).toBeVisible();
  }

  async triggerImportCsv(fileName: string) {
    await this.csvFileInputContainer.setInputFiles(fileName);
    await expect(this.filePreviewOnFileUpload).toBeVisible();
    await this.importButtonOnUploadCsvForm.click();
  }

  async clickOnImportButtonToSubmitForm() {
    await this.importButtonOnUploadCsvForm.click();
  }
  async selectDownloadCsvFromCsvDropDown() {
    await this.importCsvDropDownOptions
      .filter({
        hasText: "Download CSV",
      })
      .click();
  }

  async openAddSpeakerForm() {
    await this.clickOnAddSpeakerButtonAppearingOnTopRight();
  }

  async runEmptyStateChecksForSpeakersList() {
    await expect(async () => {
      expect(await this.getCountOfSpeakersFromInfoBar()).toBe(0);
    }, "Speaker count should be 0 in empty state").toPass();

    await expect(
      this.addSpeakerButtonAtCentre,
      "expecting add Speaker button to be visible at centre of page in empty state"
    ).toBeVisible();

    await expect(
      this.addSpeakerButton,
      "expecting count of add Speaker button to be 2"
    ).toHaveCount(2);
  }

  async fillTheSpeakerForm(speakerFormData: SpeakerFormData) {
    await this.firstNameInputBox.fill(speakerFormData.firstName);
    await this.lastNameInputBox.fill(speakerFormData.lastName);
    await this.emailIdInputBox.fill(speakerFormData.emailId);
    await this.speakerFormSubmitButton.click();
  }

  async handleSendEmailConfirmationPopup(accept: boolean) {
    if (accept) {
      await this.sendEmailConfirmationDialogueButtons
        .filter({
          hasText: "Send email",
        })
        .click();
    } else {
      await this.sendEmailConfirmationDialogueButtons
        .filter({
          hasText: "Add without email",
        })
        .click();
    }
  }

  async verifySpeakersRecordCountToBe(expectedCount) {
    await expect(
      this.speakerRowRecordLocator,
      `expecting speaker row record to be ${expectedCount}`
    ).toHaveCount(expectedCount);
  }

  async verifyTheCountOfSpeakerToBe(expectedCount, customTimout = 5000) {
    // verify count visible as text
    await expect(async () => {
      expect(await this.getCountOfSpeakersFromInfoBar()).toBe(expectedCount);
    }, `Expecting speaker count visible on top info bar to be ${expectedCount}`).toPass(
      { timeout: customTimout }
    );
    // verify count of items in speakers list
    await expect(
      this.speakerRowRecordLocator,
      `expecting speaker row record to be ${expectedCount}`
    ).toHaveCount(expectedCount);
  }

  async triggerSearchFor(searchString: string) {
    await this.searchInputBoxLocator.clear();
    await this.searchInputBoxLocator.fill(searchString);
  }

  async getSpeakerRecordColumnDataForRowIndex(
    rowIndex: number,
    dataToFetch = "status"
  ) {
    let data;
    const speakerRowColumns = this.speakerRowRecordLocator
      .nth(rowIndex)
      .locator("td");
    if (dataToFetch == "status") {
      data = await speakerRowColumns.nth(3).textContent();
    }
    return data;
  }

  //get speaker record object by using email
  public getSpeakerRowRecordByEmail(speakerEmail: string) {
    return this.speakerRowRecordLocator
      .filter({
        hasText: speakerEmail.split("@")[0],
      })
      .filter({
        hasText: speakerEmail.split("@")[1],
      });
  }

  async resendConfirmationEmail(speakerEmail: string) {
    const rowObject = this.getSpeakerRowRecordByEmail(speakerEmail);
    await rowObject.locator(this.dotIconToOpenMoreOption).click();
    await this.clickOnResendInviteButton();
  }

  async handleDeleteConfirmationPopup(toDelete: boolean) {
    if (toDelete) {
      await this.page.getByRole('button',
        { name: 'Delete', exact: true }
      ).click();
    }
    else {
      await this.page.getByRole('button',
        { name: 'Cancel', exact: true }
      ).click();
    }
  }

  async deleteSpeakerRecord(speakerEmail: string) {
    const deleteReqPromise = this.page.waitForRequest(
      (request) => request.method() === "DELETE"
    );
    const rowObject = this.getSpeakerRowRecordByEmail(speakerEmail);
    await rowObject.locator(this.dotIconToOpenMoreOption).click();
    await this.clickOnDeleteButton();
    await this.handleDeleteConfirmationPopup(true);
    const deleteReq = await deleteReqPromise;
    const deleteReqRes = await deleteReq.response();
    console.log(`delete req resp is ${deleteReqRes?.status()}`);
    await this.page.waitForTimeout(2000);
  }
  //this method is not on speaker page
  // async handleDeleteConfirmationPopup(toDelete: true) {
  //   if (toDelete) {
  //     await this.page
  //       .locator("div[class^='confirmation-popup_actions'] button")
  //       .filter({
  //         hasText: "Delete",
  //       })
  //       .click();
  //   } else {
  //     await this.page
  //       .locator("div[class^='confirmation-popup_actions'] button")
  //       .filter({
  //         hasText: "Cancel",
  //       })
  //       .click();
  //   }
  // }

  async verifyNotifcationPopsUpWithMessage(expectedMessage: string) {
    await expect(
      this.notificationContainer,
      `Expecting notification to have message ${expectedMessage}`
    ).toHaveText(expectedMessage, { timeout: 20000 });
  }

  async clickOnResendInviteButton() {
    await this.page.locator("button:has-text('Resend invite')").click();
  }

  async clickOnDeleteButton() {
    await this.page.locator("button:has-text('Delete')").click();
  }

  async downloadCsv() {
    await this.openCsvDropdown();
    await this.selectDownloadCsvFromCsvDropDown();
  }
}

export interface SpeakerFormData {
  firstName: string;
  lastName: string;
  emailId: string;
  ticketType?: string;
  title?: string;
  organisation?: string;
}
