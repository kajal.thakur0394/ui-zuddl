import { Locator, Page, expect } from "@playwright/test";

export class TransactionDetailPage {
  readonly page: Page;
  readonly transactionId: string;
  readonly transactionIdLocator: Locator;
  readonly transactionCard: Locator;
  readonly purchaserDetailsSubWrapper: Locator;
  readonly purchaserName: Locator;
  readonly purchaserEmail: Locator;
  readonly transactionDate: Locator;
  readonly invoiceNo: Locator;
  readonly psp: Locator;
  readonly attendeeDetailsCard: Locator;
  readonly transactionHistoryCard: Locator;
  readonly transactionNo: Locator;

  readonly transactionNoCopyIcon: Locator;
  readonly confirmationCopyMessage: Locator;


  constructor(page: Page) {
    this.page = page;
    let url = page.url();
    let urlParts = url.split("/");
    this.transactionId = urlParts[urlParts.length - 1];
    this.transactionIdLocator = this.page.locator(
      `div[class^='styles_backWrapper']`
    );
    this.transactionCard = this.page.locator(
      `div[class^='styles_backWrapper']`
    );
    this.transactionCard = this.page.locator(
      `div[class^='styles_transactionCard']`
    );
    this.purchaserDetailsSubWrapper = this.page.locator(
      `div[class^=styles_purchaserDetailsSubWrapper]`
    );

    // this.transactionNoCopyIcon = this.page
    //   .locator(`div[class^='styles_copyButton__fDoUu']`);
    this.transactionNo = this.page
      .locator(`div[class^='styles_copyButton']`);

    this.transactionNoCopyIcon = this.page
      .locator(`div[class^='styles_copyButton']`)
      .getByRole("button");
    this.confirmationCopyMessage = this.page.getByText(
      "Transaction no. copied to clipboard"
    );

    // within purchaser details sub wrapper, there are :
    // Name
    // Email
    // Transaction date
    // Invoice no.
    // PSP

    // inside purchaseDetailsSubWrapper, filter using text "Name", "Email", "Transaction date", "Invoice no.", "PSP"
    // styles_purchaserDetail
    this.purchaserName = this.page
      .locator(`div[class^='styles_purchaserDetail']`)
      .filter({
        hasText: "Name",
      });

    this.purchaserEmail = this.page
      .locator(`div[class^='styles_purchaserDetail']`)
      .locator(`div[class^='styles_purchaserDetail']`)
      .filter({
        hasText: "Email",
      });

    this.transactionDate = this.page
      .locator(`div[class^='styles_purchaserDetail']`)
      .filter({
        hasText: "Transaction date",
      });

    this.invoiceNo = this.page
      .locator(`div[class^='styles_purchaserDetail']`)
      .filter({
        hasText: "Invoice no.",
      });

    this.psp = this.page
      .locator(`div[class^='styles_purchaserDetail']`)
      .filter({
        hasText: "PSP",
      });

    this.attendeeDetailsCard = this.page
      .locator(`class^='styles_card'`)
      .first();

    this.transactionHistoryCard = this.page
      .locator(`class^='styles_card'`)
      .last();
  }

  async expectCopiedMessageVisible() {
    await expect(this.confirmationCopyMessage).toBeVisible({ timeout: 10000 });
  }

  async copyTransactionNo() {
    await this.transactionNoCopyIcon.click({
      timeout: 10000,
    });
  }
  async getTextelement(): Promise<string> {

    await this.page
      .context()
      .grantPermissions(["clipboard-read", "clipboard-write"]);

    const element = this.transactionNo;
    return await element.innerText();
  }
  async clipboardContent() {
    return await this.page.evaluate(() => navigator.clipboard.readText());
  }

}
