import { Locator, Page, expect } from "@playwright/test";
import { DataUtl } from "../../util/dataUtil";

export class TransactionListPage {
  readonly page: Page;
  readonly transactionsSummary: Locator;
  readonly searchBar: Locator;
  readonly transactionRows: Locator;
  readonly searchInput: Locator;
  readonly transactionColumn: Locator;



  constructor(page: Page) {
    this.page = page;

    this.transactionsSummary = this.page.locator(
      `div[class^='ticket-card_cardContainer']`
    );

    this.searchBar = this.page
      .locator("div")
      .filter({ hasText: /^Export CSV$/ })
      .locator("div");

    this.transactionRows = this.page.locator(
      `tr[class^='transactions-table_tableRow']`
    );
    this.transactionColumn = this.page.locator(
      `td[class^="transactions-table_transactionData"]`
    );

    this.searchInput = this.page.getByPlaceholder("Search by name, email or");
  }

  async clickOnTransactionRow(i: number) {
    const specificTransaction = this.transactionRows.nth(i);
    await specificTransaction.click({ timeout: 10000 });
  }

  async searchTransaction(query: string) {
    await this.searchBar.click({ timeout: 10000 });

    await this.searchInput.fill(query, { timeout: 5000 });
    await this.page.waitForTimeout(5000);
  }

  async getTransactioncount(): Promise<number> {
    return await this.transactionRows.count();
  }

  async getTransactionDetail(i: number): Promise<{ type: string; name: string; quantity: number; amount: string; status: string; paymentStatus: string; date: string } | null> {

    const transactions = this.transactionRows;

    const row = transactions.nth(i);
    const Column = row.locator(`td[class^="transactions-table_transactionData"]`);

    // 0 -> Type
    // 1 -> Name
    // 2 -> Quantity
    // 3 -> Amount
    // 4 -> status
    // 5 -> Payment Status
    // 6 -> date
    const type = (await Column.nth(0).textContent());
    const name = (await Column.nth(1).textContent());
    const quantity = parseInt((await Column.nth(2).textContent()));
    const amount = (await Column.nth(3).textContent());
    const status = (await Column.nth(4).textContent());
    const paymentStatus = (await Column.nth(5).textContent());
    const date = (await Column.nth(6).textContent());

    return { type, name, quantity, amount, status, paymentStatus, date };
  }

  async getTransactionBytext(text: string): Promise<Locator> {
    return this.transactionRows.filter({
      hasText: text,
    });
  }

  // verification of transaction
  async transactionverification(i: number, expectedTransactionDetails: Array<{ expectedName: string; expectedType: string; expectedQuantity: number; expectedAmount: string; expectedStatus: string; expectedPaymentStatus: string; expectedDate: string }>) {
    const transactionDetail = await this.getTransactionDetail(i);

    expect(transactionDetail!.name).toBe(expectedTransactionDetails[i].expectedName);
    expect(transactionDetail!.type).toBe(expectedTransactionDetails[i].expectedType);
    expect(transactionDetail!.quantity).toBe(expectedTransactionDetails[i].expectedQuantity);
    expect(transactionDetail!.amount).toBe(expectedTransactionDetails[i].expectedAmount);
    expect(transactionDetail!.status).toBe(expectedTransactionDetails[i].expectedStatus);
    expect(transactionDetail!.paymentStatus).toBe(expectedTransactionDetails[i].expectedPaymentStatus);
    expect((transactionDetail!.date).split(' ')[0]).toBe(expectedTransactionDetails[i].expectedDate);
  }
}
