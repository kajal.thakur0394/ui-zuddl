import { Locator, Page } from "@playwright/test";

export class FillersUploadPanel {
  readonly page: Page;
  readonly fillerSection: Locator;
  readonly fillerVideoSection: Locator;
  readonly fillerImageSection: Locator;

  constructor(page: Page) {
    this.page = page;
    this.fillerSection = this.page.locator("#producer-sidebar-filler-section");
    this.fillerVideoSection = this.fillerSection
      .locator("div[class^='expandable-content_container']")
      .filter({
        hasText: "Videos",
      });
    this.fillerImageSection = this.fillerSection
      .locator("div[class^='expandable-content_container']")
      .filter({
        hasText: "Images",
      });
  }

  async playFillerVideoOnStage(videoName = "videoplayback") {
    await this.page.waitForTimeout(3000);
    const thisVideoContainer = this.fillerVideoSection.filter({
      hasText: videoName,
    });
    await this.page.getByRole("button", { name: videoName }).click();
  }

  async stopFillerVideoOnStage(videoName = "videoplayback") {
    const thisVideoContainer = this.fillerVideoSection.filter({
      hasText: videoName,
    });
    await thisVideoContainer
      .locator("div[class^='video-clip_hoverActionContainer']")
      .hover();
    await thisVideoContainer
      .locator("div[class^='video-clip_hoverActionContainer']")
      .filter({ hasText: "Stop" })
      .click();
  }

  async broadcastFillerImageOnStage(imageByPositionStartFromZero = 0) {
    const thisFillerImageContainer = this.fillerImageSection
      .locator("div[class^='image_container']")
      .nth(imageByPositionStartFromZero);

    await thisFillerImageContainer
      .locator("div[class^='hover-actions_container']")
      .hover();
    await thisFillerImageContainer
      .locator("div[class^='hover-actions_container']")
      .filter({ hasText: "Show" })
      .click();
  }

  async stopBroadCastedFillerImage(imageByPositionStartFromZero = 0) {
    const thisFillerImageContainer = this.fillerImageSection
      .locator("div[class^='image_container']")
      .nth(imageByPositionStartFromZero);

    await thisFillerImageContainer
      .locator("div[class^='hover-actions_container']")
      .hover();
    await thisFillerImageContainer
      .locator("div[class^='hover-actions_container']")
      .filter({ hasText: "Hide" })
      .click();
  }
}
