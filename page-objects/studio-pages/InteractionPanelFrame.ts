import { expect, FrameLocator, Locator } from "@playwright/test";

export class InteractionPanelIframe {
  readonly frameLocator: FrameLocator;
  readonly chatBoxContainer: Locator;
  readonly chatBoxInputField: Locator;
  readonly sendButtonToSendChatMessage: Locator;
  readonly messageContainerDiv: Locator;
  readonly messageContentLocator: Locator;
  readonly backStageTab: Locator;
  readonly stageTab: Locator;
  constructor(frameLocator: FrameLocator) {
    this.frameLocator = frameLocator;
    this.chatBoxContainer = this.frameLocator.locator(
      "div[data-testid='chatBoxContainer']"
    );
    this.chatBoxInputField = this.frameLocator.locator(
      "input[class^='styles-module__chatInputField']"
    );
    this.sendButtonToSendChatMessage = this.frameLocator.locator(
      "button[data-for='icon-button-tooltip-sendIcon']"
    );
    this.messageContainerDiv = this.frameLocator.locator(
      "div[data-testid='chatBoxContainer'] div[class^='styles-module__container']"
    );
    this.messageContentLocator = this.frameLocator.locator(
      "div[class^='styles-module__contentBody'] div[class^='styles-module__content']"
    );
    this.backStageTab = this.frameLocator.locator("#privateChat");
    this.stageTab = this.frameLocator.locator("#publicChat");
  }

  async sendChatMessage(messageToSend: string) {
    await this.chatBoxInputField.fill(messageToSend);
    await this.sendButtonToSendChatMessage.click();
  }

  async verifyCountOfMessageMatches(expectedMessageCount: number) {
    await expect(this.messageContainerDiv).toHaveCount(expectedMessageCount);
  }

  async verifyMessageWithGivenContentIsVisible(expectedMessageContent: string) {
    // const expectedMessage = this.messageContainerDiv
    //   .filter({ has: this.messageContentLocator })
    //   .filter({ hasText: expectedMessageContent });
    const expectedMessage = this.messageContainerDiv.filter({
      hasText: expectedMessageContent,
    });
    await expect(expectedMessage).toBeVisible();
  }

  async openBackStageTab() {
    await this.backStageTab.click();
  }

  async openStageTab() {
    await this.stageTab.click();
  }
}
