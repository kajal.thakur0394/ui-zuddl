import { expect, Locator, Page, test } from "@playwright/test";

export class BannerUploadPanel {
  readonly page: Page;
  readonly addBannerButton: Locator;
  readonly headlineTextInputField: Locator;
  readonly subtextInputField: Locator;
  readonly addBannerButtonOnAddBannerForm: Locator;
  readonly bannerListContainer: Locator;
  readonly bannerContainer: Locator;
  readonly hoverActionContainer: string;

  constructor(page: Page) {
    this.page = page;
    this.addBannerButton = this.page.locator("#add-banner-button");
    this.headlineTextInputField = this.page.locator("#banner-form-headline");
    this.subtextInputField = this.page.locator("#banner-form-subtext");
    this.addBannerButtonOnAddBannerForm = this.page.locator(
      "div[class^='banner-form_buttonContainer'] button:has-text('Add Banner')"
    );
    this.bannerListContainer = this.page.locator(
      "div[class^='banner-list_container']"
    );
    this.bannerContainer = this.bannerListContainer.locator(
      "div[class^='banner-card_container']"
    );
    this.hoverActionContainer = "div[class^='hover-actions_container']";
  }

  async addNewBanner(bannerHeadLine: string, bannerSubText: string) {
    await test.step(`Clicking on add banner button to open the banner form`, async () => {
      await this.addBannerButton.click();
    });
    await test.step(`fill up the banner form and click on add button to submit it `, async () => {
      await this.headlineTextInputField.fill(bannerHeadLine);
      await this.subtextInputField.fill(bannerSubText);
      await this.addBannerButtonOnAddBannerForm.click();
    });
  }

  async showBannerOnStage(bannerText: string) {
    const thisBanner = this.bannerContainer.filter({ hasText: bannerText });
    await expect(thisBanner).toBeVisible();
    await thisBanner.hover();
    await thisBanner
      .locator(this.hoverActionContainer)
      .filter({ hasText: "Show" })
      .click();
  }

  async hideBannerFromStage(bannerText: string) {
    const thisBanner = this.bannerContainer.filter({ hasText: bannerText });
    await expect(thisBanner).toBeVisible();
    await thisBanner.hover();
    await thisBanner
      .locator(this.hoverActionContainer)
      .filter({ hasText: "Hide" })
      .click();
  }
}
