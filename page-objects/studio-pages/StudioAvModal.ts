import { expect, Locator, Page } from "@playwright/test";

export class StudioAvModal {
  readonly page: Page;
  readonly audioOnButton: Locator;
  readonly videoOnButton: Locator;
  readonly audioOffButton: Locator;
  readonly videoOffButton: Locator;
  readonly shareButton: Locator;

  constructor(page: Page) {
    this.page = page;
    this.audioOffButton = this.page.locator(
      "(//div[contains(@class, 'control-bar-components_avButton')])[1]//div[contains(@class,'coloredAvButton')]"
    );
    this.videoOffButton = this.page.locator(
      "(//div[contains(@class, 'control-bar-components_avButton')])[2]//div[contains(@class,'coloredAvButton')]"
    );
    this.audioOnButton = this.page.locator(
      "(//div[contains(@class, 'control-bar-components_avButton')])[1]//div[not(contains(@class,'coloredAvButton'))]"
    );
    this.videoOnButton = this.page.locator(
      "(//div[contains(@class, 'control-bar-components_avButton')])[2]//div[not(contains(@class,'coloredAvButton'))]"
    );
  }

  async turnAudioOn() {
    if ((await this.audioOnButton.count()) === 0) {
      await this.audioOffButton.click();
    }
  }

  async turnAudioOff() {
    if ((await this.audioOffButton.count()) === 0) {
      await this.audioOnButton.click();
    }
  }

  async turnVideoOn() {
    if ((await this.videoOnButton.count()) === 0) {
      await this.videoOffButton.click();
    }
  }

  async turnVideoOff() {
    if ((await this.videoOffButton.count()) === 0) {
      await this.videoOnButton.click();
    }
  }

  // This function verifies if the Audio state is "on" or "off".
  // expectedAudioState (ture/false)
  // "true" - Audio On || "false" - Audio Off
  async verifyAudioState(expectedAudioState: boolean) {
    if (expectedAudioState) {
      await expect(this.audioOnButton).toBeVisible({ timeout: 15000 });
    } else {
      await expect(this.audioOffButton).toBeVisible({ timeout: 15000 });
    }
  }

  // This function verifies if the Video state is "on" or "off".
  // expectedVideoState (ture/false)
  // "true" - Audio On || "false" - Audio Off
  async verifyVideoState(expectedVideoState: boolean) {
    if (expectedVideoState) {
      await expect(this.videoOnButton).toBeVisible({ timeout: 15000 });
    } else {
      await expect(this.videoOffButton).toBeVisible({ timeout: 15000 });
    }
  }
}
