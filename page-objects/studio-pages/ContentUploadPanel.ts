import { Locator, Page, expect } from "@playwright/test";

export class ContentUploadPanel {
  readonly page: Page;
  readonly pdfSection: Locator;
  readonly pdfDocumentContainer: Locator;
  readonly videoSection: Locator;
  readonly videoClipContainer: Locator;
  readonly addPresentationContainer: Locator;
  readonly addVideoContainer: Locator;
  readonly sideMenuContentButton: Locator;
  readonly nextSlide: Locator;
  readonly previousSlide: Locator;
  readonly slideInfo: Locator;

  constructor(page: Page) {
    this.page = page;
    this.pdfSection = this.page
      .locator("div[class^='expandable-content_container']")
      .filter({ hasText: "Presentations" });
    this.pdfDocumentContainer = this.page.locator(
      "div[class^='pdf-document_container']"
    );
    this.videoSection = this.page
      .locator("div[class^='expandable-content_container']")
      .filter({ hasText: "Videos" });
    this.videoClipContainer = this.page.locator(
      "div[class^='video-clip_container']"
    );
    this.addPresentationContainer = this.page
      .locator("div[class*='add-media-ui_addContainer'] p")
      .filter({ hasText: "Add presentation" });
    this.addVideoContainer = this.page
      .locator("div[class*='add-media-ui_addContainer'] p")
      .filter({ hasText: "Add video" });
    this.sideMenuContentButton = this.page
      .locator("div[class*='sidebar-menu_iconButtonContainer'] p")
      .filter({ hasText: "Content" });
    this.slideInfo = this.page.locator(
      "div[class*='document-controls_infoWrapper'] p"
    );
    this.previousSlide = this.page
      .locator("div[class*='presentation-card_controlButtons'] div")
      .nth(0);
    this.nextSlide = this.page
      .locator("div[class*='presentation-card_controlButtons'] div")
      .nth(1);
  }

  async uploadPdfOnStage(pdfName: string) {
    const pdfPath = `test-data/${pdfName}.pdf`;

    await this.sideMenuContentButton.click();
    const fileChooserPromise = this.page.waitForEvent("filechooser");
    await this.addPresentationContainer.click();
    const fileChooser = await fileChooserPromise;
    await fileChooser.setFiles(pdfPath);

    const thisPdfContainer = this.pdfDocumentContainer.filter({
      hasText: pdfName,
    });
    await expect(thisPdfContainer).toBeVisible({ timeout: 30000 });
  }

  async playPdfOnStage(pdfName: string) {
    await this.sideMenuContentButton.click();
    const thisPdfContainer = this.pdfDocumentContainer.filter({
      hasText: pdfName,
    });
    await thisPdfContainer
      .locator("div[class^='hover-actions_container']")
      .hover();
    await thisPdfContainer.locator("text=Share Presentation").click();
  }

  async getCurrentSlideNumber() {
    let slideNumber = await this.slideInfo.textContent();

    return slideNumber;
  }

  async turnPreviousPage(numberOfTurns: number = 1) {
    let previousSlideButton: Locator;
    for (let i = 0; i < numberOfTurns; i++) {
      previousSlideButton = this.previousSlide.filter({
        hasText: "Icon_disabledButton",
      });
      if ((await previousSlideButton.count()) === 1) {
        break;
      }

      let slideNumber = await this.getCurrentSlideNumber();
      let slideNumberLocator = this.page
        .locator("p")
        .filter({ hasText: slideNumber });

      await this.previousSlide.click();

      await expect(slideNumberLocator).not.toBeVisible({ timeout: 10000 });
    }

    return await this.getCurrentSlideNumber();
  }

  async turnNextPage(numberOfTurns: number = 1) {
    let nextSlideButton: Locator;
    for (let i = 0; i < numberOfTurns; i++) {
      nextSlideButton = this.nextSlide.filter({
        hasText: "Icon_disabledButton",
      });
      if ((await nextSlideButton.count()) === 1) {
        break;
      }

      let slideNumber = await this.getCurrentSlideNumber();
      let slideNumberLocator = this.page
        .locator("p")
        .filter({ hasText: slideNumber });

      await this.nextSlide.click();

      await expect(slideNumberLocator).not.toBeVisible({ timeout: 10000 });
    }

    return await this.getCurrentSlideNumber();
  }

  async stopPlayingPdfOnStage(pdfName: string) {
    await this.sideMenuContentButton.click();
    const thisPdfContainer = this.pdfDocumentContainer.filter({
      hasText: pdfName,
    });
    await thisPdfContainer
      .locator("div[class^='hover-actions_container']")
      .hover();
    await thisPdfContainer.locator("text=Stop sharing").click();
  }

  async playUploadedVideoOnStage(videoName = "videoplayback") {
    await this.page.waitForTimeout(3000);
    const thisVideoContainer = this.videoClipContainer.filter({
      hasText: videoName,
    });
    await this.page.getByRole("button", { name: videoName }).click();
  }

  async stopPlayingVideoOnStage(videoName = "videoplayback") {
    const thisVideoContainer = this.videoClipContainer.filter({
      hasText: videoName,
    });
    await this.page.getByRole("button", { name: videoName }).click();
  }
}
