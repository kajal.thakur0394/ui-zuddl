import { expect, Locator, Page, test } from "@playwright/test";

export class MainStageComponent {
  readonly page: Page;
  readonly mainStageContainer: Locator;
  readonly streamOnMainStageLocator: Locator;
  readonly nameContainerOnStream: Locator;
  readonly pdfPresentationStream: Locator;
  readonly pdfPresentationControl: Locator;
  readonly previousSlideButtonOnPdfControl: Locator;
  readonly nextSlideButtonOnPdfControl: Locator;
  readonly videoPlayBackContainer: Locator;
  readonly logoIndicatorOnMainStage: Locator;
  readonly backGroundImageIndicatorOnMainStage: Locator;
  readonly overLayImageIndicatorOnMainStage: Locator;
  readonly bannerContainerOnMainStage: Locator;
  readonly startSessionButton: Locator;
  readonly endSessionButton: Locator;
  readonly endSessionConfirmButton: Locator;

  constructor(page: Page) {
    this.page = page;
    this.mainStageContainer = this.page.locator("#stageOutputPreviewContainer");
    this.streamOnMainStageLocator = this.page.locator(
      "#stageOutputPreviewContainer div[class^='stage-stream_stageStreamContainer']"
    );
    this.nameContainerOnStream = this.page.locator(
      "div[class^='stage-stream-title_nameTitleThemeContainer']"
    );
    this.pdfPresentationStream = this.page.locator(
      "div[class^='presentation_container']"
    );
    this.pdfPresentationControl = this.page.locator(
      "#presentation-controls-virtual"
    );
    this.previousSlideButtonOnPdfControl = this.pdfPresentationControl
      .locator("div[role='button']")
      .nth(0);
    this.nextSlideButtonOnPdfControl = this.pdfPresentationControl
      .locator("div[role='button']")
      .nth(1);

    this.videoPlayBackContainer = this.page.locator("#sc-portal video");

    this.logoIndicatorOnMainStage = this.page.locator(
      "#stageOutputPreviewContainer div[class^='logo_logoImageContainer'] img[class*='crossfade-effect_fadeOut']"
    );
    this.backGroundImageIndicatorOnMainStage = this.page.locator(
      "#stageOutputPreviewContainer img[class*='background_backgroundImage'][class*='crossfade-effect_fadeOut']"
    );
    this.overLayImageIndicatorOnMainStage = this.page.locator(
      "#stageOutputPreviewContainer img[class*='overlay_overlayImage'][class*='crossfade-effect_fadeOut']"
    );
    this.bannerContainerOnMainStage = this.mainStageContainer.locator(
      "div[class^='banner_bannerContainer']"
    );
    this.startSessionButton = this.page.locator(
      "button:has-text('Start session')"
    );
    this.endSessionButton = this.page.locator("button:has-text('End session')");
    this.endSessionConfirmButton = this.page.getByRole("button", {
      name: "Confirm",
    });
  }

  async startSessionOnStage() {
    await test.step(`clicking on start session button`, async () => {
      await this.startSessionButton.click();
    });
    await test.step(`it will trigger 5 sec timer so waiting for that to timeout`, async () => {
      await this.page.waitForTimeout(1000);
      const timer = this.page.locator(
        "div[class^='go-live-countdown_container']"
      );
      await expect(timer).not.toBeVisible({ timeout: 10000 });
    });
  }

  async endSessionOnStage() {
    await test.step(`clicking on end session button`, async () => {
      await this.endSessionButton.click();
    });

    await test.step(`Click on confirm button`, async () => {
      await this.endSessionConfirmButton.click();
    });
  }
  public getSpeakerStreamContainerFromMainStage(speakerName: string) {
    const thisSpeakerStream = this.streamOnMainStageLocator
      .filter({ has: this.nameContainerOnStream })
      .filter({ hasText: speakerName });
    return thisSpeakerStream;
  }

  public async verifyPresenceOfSpeakerStreamOnMainStage(speakerName: string) {
    await expect(
      this.getSpeakerStreamContainerFromMainStage(speakerName)
    ).toBeVisible();
  }

  public async verifyIfPdfPresentationIsVisibleOnStage() {
    await expect(this.pdfPresentationStream).toBeVisible({ timeout: 15000 });
  }

  public async verifyIfPdfPresentationIsNotVisibleOnStage() {
    await expect(this.pdfPresentationStream).not.toBeVisible();
  }

  // only org can do it
  public async movePdfToNextPage() {
    const changePageApiPromise = this.page.waitForResponse(/change_page/, {
      timeout: 10000,
    });
    await this.nextSlideButtonOnPdfControl.click();
    const changePageApiRes = await changePageApiPromise;
    expect(
      changePageApiRes.status(),
      "expecting change API to be successfull"
    ).toBe(200);
  }

  // only org can do it
  public async movePdfToPreviousPage() {
    const changePageApiPromise = this.page.waitForResponse(/change_page/, {
      timeout: 10000,
    });
    await this.previousSlideButtonOnPdfControl.click();
    const changePageApiRes = await changePageApiPromise;
    expect(
      changePageApiRes.status(),
      "expecting change API to be successfull"
    ).toBe(200);
  }

  async verifyVideoPlayBackVisibleOnMainStage() {
    await expect(
      this.videoPlayBackContainer,
      "expecting video playback contianer to be visible"
    ).toBeVisible({ timeout: 20000 });
  }
  async verifyVideoPlayBackIsNotVisibleOnMainStage() {
    await expect(
      this.videoPlayBackContainer,
      "expecting video playback contianer not to be visible"
    ).not.toBeVisible();
  }

  async verifyBannerIsDisplayedOnMainStage(bannerText: string) {
    await test.step(`verifying that user is able to see banner with ${bannerText} on the main stage`, async () => {
      await expect(
        this.bannerContainerOnMainStage,
        "expecting banner contianer to be visible"
      ).toBeVisible();
      await expect(
        this.bannerContainerOnMainStage,
        "expecting banner contianer to contain given text"
      ).toContainText(bannerText);
    });
  }

  async verifyBannerIsNotDisplayedOnMainStage() {
    await test.step(`verifying that user is not able to see any banner on  main stage`, async () => {
      await expect(this.bannerContainerOnMainStage).not.toBeVisible();
    });
  }

  async verifyIfLogoIsDisplayedOnMainStage() {
    await expect(
      this.logoIndicatorOnMainStage,
      "expecting logo container img tag to have fadeout class with count 1"
    ).toHaveCount(1);
  }

  async verifyLogoIsNotDisplayedOnMainStage() {
    await expect(
      this.logoIndicatorOnMainStage,
      "expecting logo container img tag to have fadeout class with count 2"
    ).toHaveCount(2);
  }

  async verifyBackGroundIsAppliedOnMainstage() {
    await expect(
      this.backGroundImageIndicatorOnMainStage,
      "expecting background container to have 1 image tag with fadeout class"
    ).toHaveCount(1);
  }

  async verifyBackGroundIsNotAppliedOnMainstage() {
    await expect(
      this.backGroundImageIndicatorOnMainStage,
      "expecting background container to have 1 image tag with fadeout class"
    ).toHaveCount(2);
  }

  async verifyOverLayIsBeingDisplayedOnMainStage() {
    await expect(
      this.overLayImageIndicatorOnMainStage,
      "expecting overlay container to have 1 image tag with fadeout class"
    ).toHaveCount(1);
  }

  async verifyOverLayIsNotBeingDisplayedOnMainStage() {
    await expect(
      this.overLayImageIndicatorOnMainStage,
      "expecting overlay container to have 2 image tag with fadeout class"
    ).toHaveCount(2);
  }
}
