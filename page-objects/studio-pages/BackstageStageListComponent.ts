import { Locator, Page, expect } from "@playwright/test";

export class BackstageStageListComponent {
  readonly page: Page;
  readonly backstageContainer: Locator;
  readonly speakerOnStageListContainer: Locator;
  readonly videoStreamLocator: Locator;
  readonly mediaStreamsOnBackstageContainer: Locator;
  readonly addToStageButton: string;
  readonly removeButton: string;
  readonly nameOnStream: Locator;
  readonly stageTabButton: Locator;
  readonly backStageTabButton: Locator;
  readonly updateStageButton: Locator;
  readonly troubleshootPopup: Locator;
  readonly closeIconTroubleshootPopup: Locator;

  constructor(page: Page) {
    this.page = page;
    this.backstageContainer = this.page.locator(
      "div[class^='StageBackstage_speakersListContainer']"
    );
    this.mediaStreamsOnBackstageContainer = this.page.locator(
      "div[class^='StageBackstage_speakersListContainer'] div[class^='media-card_streamVideoCardContainer']"
    );
    this.addToStageButton = "button:has-text('Add to Stage')";
    this.removeButton = "button:has-text('Remove')";
    this.nameOnStream = this.page.locator(
      "p[class*='stream-video_streamUserName']"
    );
    this.stageTabButton = this.page
      .locator(
        "button[class^='StageBackstage_tabNameButton']:has-text('Stage')"
      )
      .nth(0);
    this.backStageTabButton = this.page.locator(
      "button[class^='StageBackstage_tabNameButton']:has-text('Backstage')"
    );
    this.updateStageButton = this.page.locator(
      "button[class*='StageBackstage_updateStagButton']"
    );
    this.troubleshootPopup = this.page.locator(
      "div[class^='troubleshoot-popup_notifierModalBody']"
    );
    this.closeIconTroubleshootPopup = this.page
      .locator("div")
      .filter({
        hasText:
          /^Seems like your internet connection isnot stableTroubleshoot$/,
      })
      .getByRole("img");
  }

  //actions to add on backstage speaker list
  public getSpeakerStreamFromBackStageList(speakerName: string) {
    console.log(
      `find the speaker media stream visible on backstage list of speaker by name: ${speakerName}`
    );
    const thisSpeakerMediaStream = this.mediaStreamsOnBackstageContainer
      .filter({ has: this.nameOnStream })
      .filter({ hasText: speakerName });
    return thisSpeakerMediaStream;
  }

  // get speaker stream from on stage list
  public getSpeakerStreamForOnStageList(speakerName: string) {
    console.log(
      `find the speaker media stream visible on backstage list of speaker by name: ${speakerName}`
    );
    const thisSpeakerMediaStream = this.mediaStreamsOnBackstageContainer
      .filter({
        has: this.page.locator(
          "div[class^='stream-video_onStageSpeakerContainer']"
        ),
      })
      .filter({ has: this.nameOnStream })
      .filter({ hasText: speakerName });
    return thisSpeakerMediaStream;
  }

  //actions to add on to stage
  async addMediaStreamToStageFromBackstage(speakerName: string) {
    console.log(`hover over this stream and then click on add to stage button`);
    await this.getSpeakerStreamFromBackStageList(speakerName).hover();
    await this.getSpeakerStreamFromBackStageList(speakerName)
      .locator(this.addToStageButton)
      .click();
  }

  // add speaker stream by selecting on checkbox
  async selectSpeakerMediaStream(speakerName: string) {
    const checkBoxContainerOnThiSpeakerStream =
      this.getSpeakerStreamFromBackStageList(speakerName).locator(
        "div[class^='Checkbox_inputContainer']"
      );
    await checkBoxContainerOnThiSpeakerStream.click();
  }

  //actions to remove from stage
  async removeMediaStreamToStageFromBackstage(speakerName: string) {
    console.log(
      `hover over this stream and then click on remove from stage button`
    );
    await this.getSpeakerStreamForOnStageList(speakerName).hover();
    await this.getSpeakerStreamForOnStageList(speakerName)
      .locator(this.removeButton)
      .click();
  }

  async openStageTab() {
    await this.stageTabButton.click();
  }

  async openBackstageTab() {
    if (await this.troubleshootPopup.isVisible({ timeout: 1000 })) {
      await this.closeIconTroubleshootPopup.click();
    }
    await this.backStageTabButton.click();
  }

  async speakerVerfiesPresenceOfStreamWithName(speakerName: string) {
    await expect(
      this.page
        .locator("p[class*='stream-video_streamUserName']")
        .filter({ hasText: speakerName })
    ).toBeVisible();
  }

  async organiserVerifiesPresenceOfSpeakerStreamWithName(speakerName: string) {
    await expect(
      this.getSpeakerStreamFromBackStageList(speakerName)
    ).toBeVisible();
  }

  async clickOnUpdateStageButton() {
    await this.updateStageButton.click();
  }
}
