import { expect, Locator, Page } from "@playwright/test";

export class BrandingPanel {
  readonly page: Page;
  readonly brandingLogoOverlaySection: Locator;
  readonly logoContainer: Locator;
  readonly floatingCheckBoxIconOnLogo: string;
  readonly floatingCheckBoxIconOnBackgroundImage: string;
  readonly hoverActionContainer: string;
  readonly backGroundOverLaySection: Locator;
  readonly backGroundImageContainer: Locator;
  readonly overLaySection: Locator;
  readonly overlayImageContainer: Locator;
  readonly videoClipSection: Locator;
  readonly videoClipContainer: Locator;
  readonly brandColorInput: Locator;
  readonly brandColorContainer: Locator;
  readonly themeLocator: Locator;
  readonly currentThemeLocator: Locator;

  constructor(page: Page) {
    this.page = page;
    this.brandingLogoOverlaySection = this.page.locator(
      "#branding-logo-bg-overlay-section"
    );
    this.logoContainer = this.page.locator("div[class^='logo_container']");

    this.floatingCheckBoxIconOnLogo = "div[class^=logo_floatingCheckboxIcon]";
    this.floatingCheckBoxIconOnBackgroundImage =
      "div[class^=image_floatingCheckboxIcon]";
    this.hoverActionContainer = "div[class^='hover-actions_container']";
    this.backGroundOverLaySection = this.page.locator(
      "div[class^='media-list_containerBACKGROUND']"
    );
    this.backGroundImageContainer = this.backGroundOverLaySection.locator(
      "div[class^='image_container']"
    );
    this.overLaySection = this.page.locator(
      "div[class^='media-list_containerOVERLAY']"
    );
    this.overlayImageContainer = this.overLaySection.locator(
      "div[class^='image_container']"
    );
    this.videoClipSection = this.page.locator(
      "div[class^='media-list_containerVIDEO_CLIP']"
    );
    this.videoClipContainer = this.videoClipSection.locator(
      "div[class^='video-clip_wrapper']"
    );
    this.brandColorInput = this.page.locator(
      "input[class*='branding_colorPickerInput']"
    );
    this.brandColorContainer = this.page.locator(
      "div[class*='branding_colorPickerContainer']"
    );
    this.themeLocator = this.page.locator("div[class*='theme-card_themeCard']");
    this.currentThemeLocator = this.page.locator(
      "div[class*='theme-card_cardSelected']"
    );
  }

  async selectLogoByCheckbox(logoToSelectByPosition = 2) {
    // by default it selects 2nd logo i.e. 1st in position in list starting with 0 index
    const logoToSelect = this.logoContainer.nth(0);
    await logoToSelect.hover();
    await expect(logoToSelect).toContainText(new RegExp("Show|Hide", "i"));

    await logoToSelect
      .locator(this.floatingCheckBoxIconOnLogo)
      .waitFor({ state: "attached" });
    await logoToSelect.locator(this.floatingCheckBoxIconOnLogo).click();
  }

  async selectBackgroundByCheckbox(backGroundImageToSelectByPosition = 2) {
    // by default it selects 2nd logo i.e. 1st in position in list starting with 0 index
    const backGroundImageToSelect = this.backGroundImageContainer.nth(
      backGroundImageToSelectByPosition - 1
    );
    await backGroundImageToSelect.hover();
    await expect(backGroundImageToSelect).toContainText("Show");
    await backGroundImageToSelect
      .locator(this.floatingCheckBoxIconOnBackgroundImage)
      .waitFor();
    await backGroundImageToSelect
      .locator(this.floatingCheckBoxIconOnBackgroundImage)
      .click();
  }

  async selectOverlayByCheckbox(overLayImageToSelectByPosition = 2) {
    // by default it selects 2nd logo i.e. 1st in position in list starting with 0 index
    const overLayImageToSelect = this.overlayImageContainer.nth(0);
    await overLayImageToSelect.hover();
    await overLayImageToSelect
      .locator(this.floatingCheckBoxIconOnBackgroundImage)
      .waitFor();
    await overLayImageToSelect
      .locator(this.floatingCheckBoxIconOnBackgroundImage)
      .click();
  }

  async playVideoClipByPosition(videoClipPositionFromZero = 0) {
    const thisVideoClipContainer = this.videoClipContainer.nth(
      videoClipPositionFromZero
    );
    await thisVideoClipContainer
      .locator("div[class^='video-clip_hoverActionContainer']")
      .hover();
    await thisVideoClipContainer
      .locator("div[class^='video-clip_hoverActionContainer']")
      .filter({ hasText: "Play" })
      .click();
  }

  async stopVideoClipByPosition(videoClipPositionFromZero = 0) {
    const thisVideoClipContainer = this.videoClipContainer.nth(
      videoClipPositionFromZero
    );
    await thisVideoClipContainer
      .locator("div[class^='video-clip_hoverActionContainer']")
      .hover();
    await thisVideoClipContainer
      .locator("div[class^='video-clip_hoverActionContainer']")
      .filter({ hasText: "Stop" })
      .click();
  }

  async changeBrandColorByColorCode(colorCode: string) {
    const snapshotRequestPromise = this.page.waitForRequest(
      (request) =>
        request.url().includes("snapshot") && request.method() === "PATCH",
      { timeout: 30000 }
    );
    await this.brandColorInput.clear();
    await this.page.keyboard.type(colorCode);
    await this.currentThemeLocator.click();
    const snapshotRequest = await snapshotRequestPromise;
    const snapshotResponse = await snapshotRequest.response();
    const snapshotBody = snapshotRequest.postDataJSON();
    console.log(snapshotBody);
    expect(snapshotResponse.status()).toBe(200);
  }

  async selectThemeByName(themeName: string) {
    let themeToBeSelected = this.themeLocator.filter({
      hasText: themeName,
    });
    await themeToBeSelected.click();
  }
}

export interface SceneParameters {
  colorCode: string;
  themeName: string;
  logoSrc: string;
  backgroundSrc: string;
  overlaySrc: string;
  streamUserList: string[];
}
