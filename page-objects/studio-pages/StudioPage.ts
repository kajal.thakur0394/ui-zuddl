import test, {
  APIRequestContext,
  BrowserContext,
  FrameLocator,
  Locator,
  Page,
  expect,
} from "@playwright/test";
import { MainStageComponent } from "./MainStageComponent";
import { InteractionPanelIframe } from "./InteractionPanelFrame";
import { RightSideOrganiserControl } from "./RightSideOrganiserControl";
import { BackstageStageListComponent } from "./BackstageStageListComponent";
import { StudioAvModal } from "./StudioAvModal";
import { ContentUploadPanel } from "./ContentUploadPanel";
import { StudioSceneComponent } from "./StudioSceneComponent";
import { StudioController } from "../../controller/StudioController";
import sessionDataPayload from "../../test-data/sessionDataPayload.json";

export class StudioPage {
  readonly page: Page;
  readonly orgApiRequestContext: APIRequestContext;
  readonly studioId: string;

  private backStageComponent: BackstageStageListComponent;
  private mainStageComponent: MainStageComponent;
  private rightSideOrganiserControl: RightSideOrganiserControl;
  private interactionPanelFrameLocator: FrameLocator;
  private studioAvModal: StudioAvModal;
  private contentUploadPanel: ContentUploadPanel;
  private studioSceneComponent: StudioSceneComponent;
  private studioController: StudioController;

  readonly sideInteractionPanelIframe: InteractionPanelIframe;
  readonly studioWalkthroughCloseButton: Locator;
  readonly backstageTab: Locator;
  readonly stageTab: Locator;
  readonly videoStreamsContainer: Locator;
  readonly startSessionButton: Locator;
  readonly startSessionPopupButton: Locator;
  readonly endSessionButton: Locator;
  readonly confirmEndSessionButton: Locator;
  readonly recordingIndicatorContainer: Locator;
  readonly expandStartTimerSection: Locator;
  readonly timerViewerSpeakerOnly: Locator;
  readonly timerViewerEveryone: Locator;
  readonly startTimerButton: Locator;
  readonly stopTimerButton: Locator;
  readonly timerInputContainer: Locator;
  readonly timerContainer: Locator;
  readonly sidebarMenu: Locator;
  readonly engageTab: Locator;
  readonly extrasTab: Locator;
  readonly addLinkIconButton: Locator;
  readonly saveRecordingButton: Locator;
  readonly selectedSaveRecordingButton: Locator;
  readonly confirmButton: Locator;
  readonly doneButton: Locator;
  readonly closePopupButton: Locator;
  readonly recordingStatusContainer: Locator;
  readonly segmentDropdown: Locator;
  readonly dryrunToggleContainer: Locator;
  readonly dryrunToggleSwitch: Locator;
  readonly sessionDropdownContainer: Locator;
  readonly currentSessionNameLocator: Locator;
  readonly addSpeakerButton: Locator;
  readonly startTimerDropdown: Locator;
  readonly stagePreviewContainer: Locator;
  readonly logoImageContainer: Locator;
  readonly backgroundPreviewPresentImage: Locator;
  readonly backgroundPreviewAbsentImage: Locator;
  readonly backgroundContainerLocator: Locator;
  readonly bgContainerImage: Locator;
  readonly dryRunPopup: Locator;
  readonly startDryRunPopupButton: Locator;

  constructor(page: Page, studioController?: StudioController) {
    this.page = page;
    this.studioController = studioController;
    this.backStageComponent = new BackstageStageListComponent(this.page);
    this.mainStageComponent = new MainStageComponent(this.page);
    this.rightSideOrganiserControl = new RightSideOrganiserControl(this.page);
    this.studioAvModal = new StudioAvModal(this.page);
    this.contentUploadPanel = new ContentUploadPanel(this.page);
    this.studioSceneComponent = new StudioSceneComponent(
      this.page,
      this.studioController
    );

    this.interactionPanelFrameLocator = this.page.frameLocator(
      "div[class^='sidebar-navigated-content_iframeContainer'] iframe"
    );
    this.sideInteractionPanelIframe = new InteractionPanelIframe(
      this.interactionPanelFrameLocator
    );
    this.studioWalkthroughCloseButton = this.page.locator(
      "//button[contains(@class,'close-button')]"
    );
    this.stageTab = this.page.locator(
      "//div[contains(@class,'StageBackstage_tabsContainer')]//p[contains(text(),'Stage')]"
    );
    this.backstageTab = this.page.locator(
      "//div[contains(@class,'StageBackstage_tabsContainer')]//p[contains(text(),'Backstage')]"
    );
    this.videoStreamsContainer = this.page.locator(
      "//div[contains(@class,'streamVideoCardContainer')]"
    );
    this.startSessionButton = this.page.locator(
      "//button[contains(@class,'startLiveButton')]"
    );
    this.startSessionPopupButton = this.page
      .locator("div[class*='popup'] button")
      .filter({ hasText: "Start session" });
    this.endSessionButton = this.page.getByRole("button", {
      name: new RegExp("End session|End Dry Run"),
    }); // regex End session or End session

    this.confirmEndSessionButton = this.page.locator("button", {
      hasText: new RegExp("Confirm|End dry run"),
    });

    this.recordingIndicatorContainer = this.page.locator(
      "div[class*='recordingStatusContainer']"
    );
    this.expandStartTimerSection = this.page.locator(
      "div:has-text('Start timer')"
    );
    this.timerViewerSpeakerOnly = this.page.locator(
      "//p[text()='Speakers only']"
    );
    this.startTimerDropdown = this.page
      .locator("button")
      .filter({ hasText: "Start timer" });

    this.stopTimerButton = this.page.getByRole("button", {
      name: "Stop timer",
    });
    this.timerViewerEveryone = this.page.locator("//p[text()='Everyone']");
    this.startTimerButton = this.page.locator(
      "//button[contains(@class, 'timerButton')]"
    );
    this.timerInputContainer = this.page.locator(
      "//input[contains(@class, 'timerInputContainer')]"
    );
    this.timerContainer = this.page.locator(
      "//div[contains(@class,'timerContainer')]"
    );
    this.sidebarMenu = this.page.locator("div[class*='sidebar-menu'] p");
    this.engageTab = this.sidebarMenu.filter({ hasText: "Engage" });
    this.extrasTab = this.sidebarMenu.filter({ hasText: "Extras" });
    this.addLinkIconButton = this.page
      .locator("div[class*='AddIconButton']")
      .first();
    this.selectedSaveRecordingButton = this.page.locator(
      "//p[contains(text(),'Save recording')]/../../../../div[contains(@class,'listItemSelected')]"
    );
    this.saveRecordingButton = this.page.locator(
      "//p[contains(text(),'Save recording')]"
    );
    this.confirmButton = this.page.locator("//button[text()='Confirm']");
    this.doneButton = this.page.locator("//button//p[text()='Done']");
    this.closePopupButton = this.page.locator("div[class*='popup_right']");
    this.segmentDropdown = this.page.locator(
      "div[class*='segments-dropdown_selectContainer']"
    );
    this.dryrunToggleContainer = this.page.locator(
      "div[class*='segments-dropdown_testingSessionContainer']"
    );
    this.dryrunToggleSwitch = this.page.locator(
      "div[class*='segments-dropdown_testingSessionContainer'] label[class*='Toggle_switch']"
    );
    this.sessionDropdownContainer = this.page.locator(
      "div[class*='segments-dropdown_optionContainer']"
    );
    this.currentSessionNameLocator = this.page.locator(
      "div[class*='segments-dropdown_selectedOptionLabel']"
    );
    this.addSpeakerButton = this.page.locator("div[id='user-invite-button']");
    this.stagePreviewContainer = this.page.locator(
      "#stage-output-inner-preview-virtual"
    );
    this.logoImageContainer = this.page.locator(
      "div[class^='logo_logoImageContainer'] img[class*='crossfade-effect_fadeIn' ]"
    );
    this.backgroundPreviewPresentImage = this.page.locator(
      "img[class*='crossfade-effect_fadeIn']"
    );
    this.backgroundPreviewAbsentImage = this.page.locator(
      "img[class*='crossfade-effect_fadeOut']"
    );
    this.backgroundContainerLocator = this.page.locator(
      "div[class^='media-list_containerBACKGROUND__'] p"
    );
    this.bgContainerImage = this.page.locator(
      "div[class^='media-list_containerBACKGROUND__'] div[class^='image_container']"
    );
    this.dryRunPopup = this.page.locator(
      "div[class^='basic-delete-popup_container']"
    );

    this.startDryRunPopupButton = this.dryRunPopup.locator("button", {
      hasText: new RegExp("Start session|Start Dry Run"),
    });
  }

  //component getters
  get getBackStageComponent() {
    return this.backStageComponent;
  }

  get getMainStageComponent() {
    return this.mainStageComponent;
  }

  get getRightSideOrganiserControlPanel() {
    return this.rightSideOrganiserControl;
  }

  get getInteractionPanelIframe() {
    return this.sideInteractionPanelIframe;
  }

  get getStudioAvModal() {
    return this.studioAvModal;
  }

  get getContentPanel() {
    return this.contentUploadPanel;
  }

  get getStudioSceneComponent() {
    return this.studioSceneComponent;
  }

  //actions

  async closeStudioWalkthrough() {
    try {
      await expect
        .soft(this.studioWalkthroughCloseButton)
        .toBeVisible({ timeout: 30000 });
      await this.studioWalkthroughCloseButton.click();
    } catch (error) {
      console.log("Studio walkthrough pop-up not present.");
    }
  }

  async isBackstageVisible() {
    await expect(this.backstageTab).toBeVisible();
  }

  async clickBackstageTab() {
    if (
      (await this.page
        .locator("div[class^='troubleshoot-popup_notifierModalContainer']")
        .count()) > 0
    ) {
      await this.page
        .locator("div[class^='troubleshoot-popup_notifierModalContainer']")
        .getByRole("img")
        .click();
    }
    await this.backstageTab.click();
    const activeBackstageTab = this.page.locator(
      "//div[contains(@class,'StageBackstage_activeTab')]//p[contains(text(),'Backstage')]"
    );
    await expect(activeBackstageTab).toBeVisible({ timeout: 45000 });
  }

  async clickStageTab() {
    await this.stageTab.click();
    const activeStageTab = this.page.locator(
      "//div[contains(@class,'StageBackstage_activeTab')]//p[contains(text(),'Stage')]"
    );
    await expect(activeStageTab).toBeVisible({ timeout: 45000 });
  }

  async verifyCountOfStreamInBackstage(expectedStreamCount: number) {
    await this.clickBackstageTab();
    await expect(this.videoStreamsContainer).toHaveCount(expectedStreamCount, {
      timeout: 60000,
    });
  }

  async verifyCountOfStreamInStage(expectedStreamCount: number) {
    await this.clickStageTab();
    await expect(this.videoStreamsContainer).toHaveCount(expectedStreamCount, {
      timeout: 60000,
    });
  }

  async getUserVideoStreamContainerFromBackstage(userName: string) {
    await this.clickBackstageTab();
    const userStreamInsideBackstage = this.videoStreamsContainer.filter({
      hasText: userName,
    });
    await expect(userStreamInsideBackstage).toBeVisible({ timeout: 25000 });
    return userStreamInsideBackstage;
  }

  async getUserVideoStreamContainerFromStage(userName: string) {
    await this.clickStageTab();
    const userStreamInsideStage = this.videoStreamsContainer.filter({
      hasText: userName,
    });
    await expect(userStreamInsideStage).toBeVisible({ timeout: 25000 });
    return userStreamInsideStage;
  }

  async getRemoveFromStageLocator(userName: string) {
    const removeFromStageLocator = this.page.locator(
      `//p[contains(text(), '${userName}')]/../../../..//button`
    );
    return removeFromStageLocator;
  }

  async getAddToStageLocator(userName: string) {
    const addToStageLocator = this.page.locator(
      `//p[contains(text(), '${userName}')]/../../../..//button`
    );
    return addToStageLocator;
  }

  async getUserContainerLocator(userName: string) {
    const userContainerLocator = this.page.locator(
      `//p[contains(text(), '${userName}')]/../../../..//div[contains(@class,'videoElement')]`
    );
    return userContainerLocator;
  }

  async getUserStageStreamContainer(userName: string) {
    const userStageStreamContainer = this.page.locator(
      `//div[contains(@class,'stageStreamContainer')]//p[contains(text(),'${userName}')]`
    );
    return userStageStreamContainer;
  }

  //Add/Remove user on Stage/Backstage
  async addUserToStageFromBackStage(userName: string, self: boolean = false) {
    if (self) {
      userName = `(You) ${userName}`;
    }
    await test.step("Adding user to stage.", async () => {
      await this.page.waitForTimeout(5000);
      await this.clickBackstageTab();
      await this.page.waitForTimeout(2000);
      const userContainerInBackStage = await this.getUserContainerLocator(
        userName
      );
      await expect(userContainerInBackStage).toBeVisible();
      await userContainerInBackStage.hover();
      const addToStageButton = await this.getAddToStageLocator(userName);
      await expect(addToStageButton).toBeVisible({ timeout: 30000 });
      await addToStageButton.click();
      await this.page.waitForTimeout(8000);
    });

    await test.step("Verify user joined the stage.", async () => {
      await this.clickStageTab();
      await this.page.waitForTimeout(2000);
      const userContainerInStage = await this.getUserContainerLocator(userName);
      await expect(userContainerInStage).toBeVisible({ timeout: 60000 });
    });
  }

  async removeUserFromStage(userName: string) {
    await test.step("Removing user to stage.", async () => {
      await this.clickStageTab();
      const userContainerLocator = await this.getUserContainerLocator(userName);
      await expect(userContainerLocator).toBeVisible();
      await userContainerLocator.hover();
      const removeFromStageButton = await this.getRemoveFromStageLocator(
        userName
      );
      await expect(removeFromStageButton).toBeVisible();
      await removeFromStageButton.click();
    });

    await test.step("Verify user is not on the stage.", async () => {
      const userContainerOnStage = await this.getUserStageStreamContainer(
        userName
      );
      await expect(userContainerOnStage).not.toBeVisible();
    });
  }

  async verifyAttendeeVideoContainer(attendeeName: string) {
    await test.step("Verify attendee is on stage.", async () => {
      const attendeeContainerInStage = await this.getUserContainerLocator(
        attendeeName
      );
      await expect(attendeeContainerInStage).not.toBeVisible();
    });
  }

  //Session Start/End
  async startTheSession(sessionName: string = "") {
    let startSessionButtonText: string = await this.startSessionButton
      .locator("p")
      .textContent();

    let recordingStatus: string = await this.recordingIndicatorContainer
      .locator("p")
      .textContent();

    await this.startSessionButton.click();

    if (
      startSessionButtonText === "Start session" &&
      recordingStatus === "OFF"
    ) {
      await this.startSessionPopupButton.click();
    }

    await expect(this.dryRunPopup).toBeVisible({ timeout: 20000 });
    await this.startDryRunPopupButton.click({
      timeout: 20000,
    });

    await expect(this.endSessionButton).toBeVisible({ timeout: 20000 });
    await expect(
      this.page.locator("div[class*='go-live-countdown_container']")
    ).not.toBeVisible({ timeout: 30000 });
  }

  async endTheSession() {
    await this.endSessionButton.click({ timeout: 5000, force: true });
    await this.confirmEndSessionButton.click({ timeout: 20000, force: true });
    await expect(this.startSessionButton).toBeVisible({ timeout: 20000 });
  }

  async endTheSessionWebinar() {
    await this.endSessionButton.click({ timeout: 5000, force: true });
    await this.confirmEndSessionButton.click({ timeout: 20000, force: true });
    await expect(this.startSessionButton).toBeVisible({ timeout: 20000 });
  }

  async selectTheSession(sessionDataId = 1, sessionName = "") {
    await this.segmentDropdown.click();

    if (sessionName === "") {
      sessionName = await sessionDataPayload[sessionDataId]["title"];
    }

    await this.sessionDropdownContainer
      .filter({ hasText: sessionName })
      .click();
  }

  async clickOnExtraTab() {
    await this.extrasTab.click();
  }

  //Timer related functions
  async startTheTimer(time: string, viewers: string = "Everyone") {
    await this.startTimerDropdown.click();
    await this.timerInputContainer.fill(time);
    if (viewers === "Speakers only") {
      await this.timerViewerSpeakerOnly.click();
    } else {
      await this.timerViewerEveryone.click();
    }
    await this.startTimerButton.click();
  }

  async verifyTimerVisibility(shouldBePresent: boolean = true) {
    if (shouldBePresent) {
      await expect(this.timerContainer).toBeVisible();
    } else {
      await expect(this.timerContainer).not.toBeVisible();
    }
  }

  async stopTheTimer() {
    await test.step("Stop timer.", async () => {
      await this.stopTimerButton.click();
    });
  }

  async clickEngageTab() {
    await test.step("Click Engage tab.", async () => {
      await this.engageTab.click();
    });
  }

  // Change the status of dry-run for current session.
  // saveRecording (true/false)
  // true - enable dry-run || false - disable dry-run
  async changeDryRunStatus(enableDryRun: boolean) {
    await this.segmentDropdown.click();
    let dryrunToggleSwitchOnLocator = this.dryrunToggleContainer.filter({
      hasText: "Toggle_switchOn",
    });

    if (enableDryRun && (await dryrunToggleSwitchOnLocator.count()) === 0) {
      await this.dryrunToggleSwitch.click();

      await expect(dryrunToggleSwitchOnLocator).toBeVisible({ timeout: 10000 });
    } else if (
      !enableDryRun &&
      (await dryrunToggleSwitchOnLocator.count()) === 1
    ) {
      await this.dryrunToggleSwitch.click();

      await expect(dryrunToggleSwitchOnLocator).not.toBeVisible({
        timeout: 10000,
      });
    }
  }

  // Change the status of recording for current session.
  // saveRecording (true/false)
  // true - save the recording || false - don't save the recording
  async changeRecordingStatus(saveRecording: boolean) {
    await this.addLinkIconButton.click();

    if (
      saveRecording &&
      (await this.selectedSaveRecordingButton.count()) === 0
    ) {
      await this.saveRecordingButton.click();
      await expect(this.selectedSaveRecordingButton).toBeVisible({
        timeout: 30000,
      });
    } else if (
      !saveRecording &&
      (await this.selectedSaveRecordingButton.count()) === 1
    ) {
      await this.saveRecordingButton.click();
      await this.confirmButton.click();
      await expect(this.selectedSaveRecordingButton).not.toBeVisible({
        timeout: 30000,
      });
    }

    this.closePopupButton.click();
  }

  // true - Session is being recorded || false - Session is not being recorded
  async verifyRecordingStatus(recordingStatus: boolean) {
    if (recordingStatus) {
      let recordingOnLocator = this.recordingIndicatorContainer.filter({
        hasText: "REC",
      });

      await expect(recordingOnLocator).toBeVisible({ timeout: 15000 });
    } else {
      let recordingOffLocator = this.recordingIndicatorContainer.filter({
        hasText: "OFF",
      });

      await expect(recordingOffLocator).toBeVisible({ timeout: 15000 });
    }
  }

  async goToSession(sessionDataId: number = 0, expectedSessionName?: string) {
    let dropdownLocator = this.page.locator(
      "div[class^='segments-dropdown_selectContainer'] div[class*='control']"
    );
    await expect(dropdownLocator).toBeVisible({ timeout: 45000 });
    await dropdownLocator.click();

    console.log(`session drop down opened`);

    let sessionName: string;
    if (sessionDataId != 0) {
      sessionName = await sessionDataPayload[sessionDataId]["title"];
    } else {
      sessionName = expectedSessionName;
    }

    console.log(`Session name - ${sessionName}`);

    let sessionLocator = this.sessionDropdownContainer.filter({
      hasText: sessionName,
    });

    await expect(sessionLocator).toBeVisible({ timeout: 45000 });

    await sessionLocator.click();

    const nameLocator = this.currentSessionNameLocator.filter({
      hasText: sessionName,
    });

    await expect(nameLocator).toBeVisible({ timeout: 50000 });
  }

  async clickAddSpeakerButton() {
    await this.addSpeakerButton.click();
  }

  async clickInviteSpeakerButtonAndVerifyPage() {
    let inviteSpeakerButton = this.page
      .locator("button")
      .filter({ hasText: "Invite speaker" });
    await inviteSpeakerButton.click();

    await this.page.waitForTimeout(20000);
  }

  async verifyPageUrl(context: BrowserContext, expectedURL: string) {
    let pages = context.pages();
    let actualURL = pages[1].url();

    console.log(`Actual URL - ${actualURL}`);
    console.log(`Expected URL - ${expectedURL}`);

    expect(actualURL).toEqual(expectedURL);
  }

  async verifyPresenceOfStageContainer() {
    await expect(this.stagePreviewContainer).toBeEnabled();
  }

  async verifyAbsenceOfLogoImage() {
    await expect(this.logoImageContainer).toBeHidden();
  }

  async verifyPresenceOfLogoImage() {
    await expect(this.logoImageContainer).toBeEnabled();
  }
  async verifyPresenceOfBackgroundImage() {
    await this.page.waitForTimeout(6000);
    await expect(this.backgroundPreviewPresentImage.first()).toBeEnabled();
  }
  async verifyAbsenceOfBackgroundImage() {
    await this.page.waitForTimeout(6000);
    await expect(this.backgroundPreviewAbsentImage.first()).toBeEnabled();
  }
  async enableOrDisableBackgroundImage(addBG: boolean) {
    if (addBG) {
      if (this.backgroundContainerLocator.filter({ hasText: "Show" })) {
        await this.bgContainerImage.click();
      }
    } else {
      if (this.backgroundContainerLocator.filter({ hasText: "Hide" })) {
        await this.bgContainerImage.click();
      }
    }
  }
}
