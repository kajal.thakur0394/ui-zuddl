import { FrameLocator, Locator, Page, test } from "@playwright/test";
import { BannerUploadPanel } from "./BannerUploadPanel";
import { BrandingPanel } from "./BrandingPanel";
import { ContentUploadPanel } from "./ContentUploadPanel";
import { FillersUploadPanel } from "./FillersUploadPanel";
import { InteractionPanelIframe } from "./InteractionPanelFrame";

export class RightSideOrganiserControl {
  readonly page: Page;
  readonly engageMenuOption: Locator;
  readonly interactionPanel: FrameLocator;
  readonly sideBarInteractionPanel: InteractionPanelIframe;
  readonly contentMenuOption: Locator;
  readonly brandingMenuOption: Locator;
  readonly bannerMenuOption: Locator;
  readonly fillerMenuOption: Locator;

  constructor(page: Page) {
    this.page = page;
    this.engageMenuOption = this.page.getByRole("button", { name: "Engage" });
    this.interactionPanel = this.page.frameLocator(
      "div[class^='sidebar-navigated-content_iframeContainer'] iframe"
    );
    this.sideBarInteractionPanel = new InteractionPanelIframe(
      this.interactionPanel
    );
    this.contentMenuOption = this.page.getByRole("button", { name: "Content" });
    this.brandingMenuOption = this.page.getByRole("button", {
      name: "Branding",
    });
    this.bannerMenuOption = this.page.getByRole("button", { name: "Banners" });
    this.fillerMenuOption = this.page.getByRole("button", { name: "Fillers" });
  }

  async clickOnEngageMenuOption() {
    await this.engageMenuOption.click();
    return this.sideBarInteractionPanel;
  }

  async clickOnContentMenuOption() {
    await this.contentMenuOption.click();
    return new ContentUploadPanel(this.page);
  }

  async clickOnBrandingMenuOption() {
    await this.brandingMenuOption.click();
    return new BrandingPanel(this.page);
  }

  async clickOnBannerMenuOption() {
    await test.step("Opening banner section on studio", async () => {
      await this.bannerMenuOption.click();
    });
    return new BannerUploadPanel(this.page);
  }

  async clickOnFillerMenuOption() {
    await test.step("Opening banner section on studio", async () => {
      await this.fillerMenuOption.click();
    });
    return new FillersUploadPanel(this.page);
  }
}
