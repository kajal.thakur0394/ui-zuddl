import { expect, Locator, Page } from "@playwright/test";
import { StudioPage } from "./StudioPage";

export class StudioJoiningAvModal {
  page: Page;
  readonly audioButton: Locator;
  readonly videoButton: Locator;
  readonly fullNameInputBox: Locator;
  readonly titleInputBox: Locator;
  readonly enterStudioButton: Locator;
  readonly videoStreamPreviewOnAvModal: Locator;

  constructor(page: Page) {
    this.page = page;
    this.audioButton = this.page
      .locator(
        "div[class^='control-bar-components_avButton'] div[role='button']"
      )
      .nth(0);
    this.videoButton = this.page
      .locator(
        "div[class^='control-bar-components_avButton'] div[role='button']"
      )
      .nth(1);
    this.titleInputBox = this.page.locator("#joining-modal-title");
    this.fullNameInputBox = this.page.locator("joining-modal-fullname");
    this.enterStudioButton = this.page.locator(
      "button:has-text('Enter studio')"
    );
    this.videoStreamPreviewOnAvModal = this.page.locator("#videoId video");
  }

  //actions

  async clickOnAudioButton() {
    // to mute/unmute audio
    await this.audioButton.click();
  }
  async clickOnVideoButton() {
    // to mute/unmute video
    await this.videoButton.click();
  }

  async enterFullName(nameToEnter: string) {
    await this.fullNameInputBox.fill(nameToEnter);
  }

  async enterTitle(titleToEnter: string) {
    await this.titleInputBox.fill(titleToEnter);
  }

  async clickOnEnterStudioButton() {
    await this.enterStudioButton.click();
  }

  async verifyVideoPreviewIsLoaded() {
    //wait for video stream preview to load
    await expect(
      this.videoStreamPreviewOnAvModal,
      "expecting video stream preview to load on av modal"
    ).toBeVisible({ timeout: 45000 });
  }

  async enterInsideTheStudio({
    nameToEnter = "",
    titleToEnter = "",
    muteVideo = true,
    muteAudio = true,
  }) {
    await this.verifyVideoPreviewIsLoaded();
    // handle the av control
    if (nameToEnter) {
      await this.enterFullName(nameToEnter);
    }
    if (titleToEnter) {
      await this.enterTitle(titleToEnter);
    }
    if (muteAudio) {
      await this.audioButton.click();
    }
    if (muteVideo) {
      await this.videoButton.click();
    }
    //finally click on enter studio button
    await this.clickOnEnterStudioButton();
    return new StudioPage(this.page);
  }
}
