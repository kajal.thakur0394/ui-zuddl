import {
  APIRequestContext,
  expect,
  Locator,
  Page,
  test,
} from "@playwright/test";
import sceneDataPayload from "../../test-data/sceneDataPayload.json";
import { BrandingPanel, SceneParameters } from "./BrandingPanel";
import { StudioController } from "../../controller/StudioController";

export class StudioSceneComponent {
  readonly page: Page;
  readonly orgApiRequestContext: APIRequestContext;
  readonly studioId: string;

  private brandingPanel: BrandingPanel;
  private studioController: StudioController;

  readonly saveSceneButton: Locator;
  readonly sceneBrandColor: string;
  readonly fullscreenButton: Locator;
  readonly defaultScreenButton: Locator;
  readonly fullscreenContainer: Locator;
  readonly sceneTheme: string;
  readonly sceneLogo: string;
  readonly sceneBackground: string;
  readonly sceneOverlay: string;
  readonly brandColorInput: Locator;
  readonly currentThemeLocator: Locator;
  readonly themeLocator: Locator;
  readonly currentLogoLocator: Locator;
  readonly logoOneLocator: Locator;
  readonly logoTwoLocator: Locator;
  readonly currentBackgroundLocator: Locator;
  readonly backgroundOneLocator: Locator;
  readonly backgroundTwoLocator: Locator;
  readonly backgroundThreeLocator: Locator;
  readonly currentOverlayLocator: Locator;
  readonly layoutOneLocator: Locator;
  readonly layoutTwoLocator: Locator;
  readonly layoutThreeLocator: Locator;
  readonly sceneCardContainer: Locator;
  readonly streamNameTitleContainer: Locator;
  readonly placeholderNameContainer: Locator;
  readonly updateStageButton: Locator;
  readonly addPlaceholderButton: Locator;
  readonly sidebarMenu: Locator;
  readonly brandingTab: Locator;
  readonly extrasTab: Locator;
  readonly stageTab: Locator;
  actualSceneData: SceneParameters;
  expectedSceneData: SceneParameters;

  constructor(page: Page, studioController?: StudioController) {
    this.page = page;
    this.studioController = studioController;

    this.brandingPanel = new BrandingPanel(this.page);

    this.saveSceneButton = this.page.locator(
      "button[class*='saveAShotButton']"
    );
    this.brandColorInput = this.page.locator(
      "input[class*='branding_colorPickerInput']"
    );
    this.currentThemeLocator = this.page.locator(
      "div[class*='theme-card_cardSelected'] p"
    );
    this.themeLocator = this.page.locator("div[class*='theme-card_themeCard']");
    this.currentLogoLocator = this.page.locator(
      "//div[contains(@class,'hover-actions_activeContainer')]/../img[contains(@class,'logo_preview')]"
    );
    this.logoOneLocator = this.page.locator(
      "(//img[contains(@class,'logo_preview')])[1]"
    );
    this.logoTwoLocator = this.page.locator(
      "(//img[contains(@class,'logo_preview')])[2]"
    );
    this.currentBackgroundLocator = this.page.locator(
      "//div[contains(@class,'containerBACKGROUND')]//div[contains(@class,'hover-actions_activeContainer')]/../img[contains(@class,'image_preview')]"
    );
    this.backgroundOneLocator = this.page.locator(
      "(//div[contains(@class,'containerBACKGROUND')]//img)[1]"
    );
    this.backgroundTwoLocator = this.page.locator(
      "(//div[contains(@class,'containerBACKGROUND')]//img)[2]"
    );
    this.backgroundThreeLocator = this.page.locator(
      "(//div[contains(@class,'containerBACKGROUND')]//img)[3]"
    );
    this.currentOverlayLocator = this.page.locator(
      "//div[contains(@class,'containerOVERLAY')]//div[contains(@class,'hover-actions_activeContainer')]/../img[contains(@class,'image_preview')]"
    );
    this.layoutOneLocator = this.page.locator(
      "(//div[contains(@class,'containerOVERLAY')]//img)[1]"
    );
    this.layoutTwoLocator = this.page.locator(
      "(//div[contains(@class,'containerOVERLAY')]//img)[2]"
    );
    this.layoutThreeLocator = this.page.locator(
      "(//div[contains(@class,'containerOVERLAY')]//img)[3]"
    );
    this.streamNameTitleContainer = this.page.locator(
      "div[class*='stage-stream-title_nameTitleThemeContainer']"
    );
    this.placeholderNameContainer = this.page.locator(
      "div[class*='stage-stream_stageStreamContainer'] p[class*='stream-placeholder_name']"
    );
    this.updateStageButton = this.page.locator("button[class*='updateStag']");
    this.addPlaceholderButton = this.page.locator(
      "div[id='add-placeholder-button']"
    );
    this.sidebarMenu = this.page.locator("div[class*='sidebar-menu'] p");
    this.brandingTab = this.sidebarMenu.filter({ hasText: "Branding" });
    this.extrasTab = this.sidebarMenu.filter({ hasText: "Extras" });
    this.stageTab = this.page.locator(
      "//div[contains(@class,'StageBackstage_tabsContainer')]//p[contains(text(),'Stage')]"
    );
    this.fullscreenButton = this.page.locator(
      "(//div[contains(@class,'layout-selector-and-icon-set_icons')])[2]"
    );
    this.defaultScreenButton = this.page.locator(
      "//div[contains(@class,'layout-selector-and-icon-set_icons')]"
    );
    this.fullscreenContainer = this.page.locator(
      "div[class*='stage-container_previewAreaFullScreen']"
    );
  }

  async clickOnExtraTab() {
    await this.extrasTab.click();
  }

  get getBrandingPanel() {
    return this.brandingPanel;
  }

  async clickBrandingTab() {
    await test.step("Click Branding tab.", async () => {
      await this.brandingTab.click();
    });
  }

  async saveCurrentScene(studioId?: string) {
    let apiURI = `/api/studio/${studioId}/shot`;
    console.log(`URI - ${apiURI}`);

    let responseList = await Promise.all([
      this.saveSceneButton.click(),
      this.page.waitForResponse(
        (resp) => resp.url().includes(apiURI) && resp.status() === 200
      ),
    ]);

    console.log(responseList);

    let apiResponse = responseList[1];

    console.log(apiResponse);

    const responseBody = await apiResponse.json();
    await this.page.waitForTimeout(5000);
    return responseBody["shotId"];
  }

  async changeToFullscreenView() {
    await this.fullscreenButton.click();
  }

  async verifyFullscreenView() {
    await expect(this.fullscreenContainer).toBeVisible({ timeout: 30000 });
  }

  async changeToDefaultScreenView() {
    if ((await this.fullscreenContainer.count()) === 0) {
      await this.defaultScreenButton.hover();
      await this.defaultScreenButton.click();
    }
  }

  //changing the scene configuration as per user data
  async changeStageConfiguration(dataSetNumber: number = 1) {
    await this.clickBrandingTab();

    await test.step("Changing Brand color.", async () => {
      if (this.studioController === null) {
        throw new Error(
          "Studio Controller object is null, please check object initilisation."
        );
      }
      await this.studioController.updateBrandColor(
        sceneDataPayload[dataSetNumber]["colorCode"]
      );
    });

    await test.step("Changing Theme.", async () => {
      await this.brandingPanel.selectThemeByName(
        sceneDataPayload[dataSetNumber]["themeName"]
      );
    });

    await test.step("Changing Logo.", async () => {
      await this.brandingPanel.selectLogoByCheckbox(
        sceneDataPayload[dataSetNumber]["logoNumber"]
      );
    });

    await test.step("Changing Background.", async () => {
      await this.brandingPanel.selectBackgroundByCheckbox(
        sceneDataPayload[dataSetNumber]["backgroundNumber"]
      );
    });

    await test.step("Changing Overlay.", async () => {
      await this.brandingPanel.selectOverlayByCheckbox(
        sceneDataPayload[dataSetNumber]["overlayNumber"]
      );
    });

    await test.step("Apply changes.", async () => {
      await this.updateStageButton.click();
    });
  }

  async getSpeakerPlaceholderCheckboxLocator(speakerName: string) {
    const checkboxLocator = this.page.locator(
      `//div[contains(@class,'speaker-dropdown')]//p[text()='${speakerName}']/../div[contains(@class,'Checkbox')]`
    );
    return checkboxLocator;
  }

  async getSpeakerPlaceholderNameLocator(speakerName: string) {
    const placeholderNameLocator = this.placeholderNameContainer.filter({
      hasText: speakerName,
    });
    return placeholderNameLocator;
  }

  async addSpeakerPlaceholders(speakerNameList: string[]) {
    const listLength = speakerNameList.length;

    this.addPlaceholderButton.click();

    for (let i = 0; i < listLength; i++) {
      let speakerPlaceholderCheckboxLocator =
        await this.getSpeakerPlaceholderCheckboxLocator(speakerNameList[i]);

      await speakerPlaceholderCheckboxLocator.click();
      let speakerPlaceholderLocator =
        await this.getSpeakerPlaceholderNameLocator(speakerNameList[i]);
      await expect(speakerPlaceholderLocator).toBeVisible({ timeout: 30000 });
    }
  }

  private async getAttributeOfLocator(eleLocator: Locator, attribute: string) {
    let response: string = "";
    await expect.soft(eleLocator).toBeVisible({ timeout: 10000 });
    let countOfElement = await eleLocator.count();

    if (countOfElement != 0) {
      response = await eleLocator.getAttribute(attribute);
    } else {
      console.log(`Element not found.`);
    }
    return response;
  }

  private async getCurrentThemeName() {
    return await this.currentThemeLocator.textContent();
  }

  //Captures details of the current stage.
  private async captureStageConfiguration() {
    await this.clickBrandingTab();
    let sceneDetails: SceneParameters;
    let color: string;
    let theme: string;
    let logo: string;
    let background: string;
    let overlay: string;
    let streamUser: string[];

    await test.step("Colorcode.", async () => {
      color = await this.getAttributeOfLocator(this.brandColorInput, "value");
      console.log(`Color Code - ${color}`);
    });

    await test.step("Theme Name.", async () => {
      theme = await this.getCurrentThemeName();
      console.log(`Theme Name - ${theme}`);
    });

    await test.step("Logo Src.", async () => {
      logo = await this.getAttributeOfLocator(this.currentLogoLocator, "src");
      console.log(`Logo Src - ${logo}`);
    });

    await test.step("Background src.", async () => {
      background = await this.getAttributeOfLocator(
        this.currentBackgroundLocator,
        "src"
      );
      console.log(`Background Src - ${background}`);
    });

    await test.step("Overlay src.", async () => {
      overlay = await this.getAttributeOfLocator(
        this.currentOverlayLocator,
        "src"
      );
      console.log(`Overlay Src - ${overlay}`);
    });

    await test.step("Stream user list.", async () => {
      streamUser = await this.getListOfUserCurrentlyStreaming();
      console.log(`Stream user list - ${streamUser}`);
    });

    sceneDetails = {
      colorCode: color,
      themeName: theme,
      logoSrc: logo,
      backgroundSrc: background,
      overlaySrc: overlay,
      streamUserList: streamUser,
    };

    return sceneDetails;
  }

  async captureExpectedData() {
    this.expectedSceneData = await this.captureStageConfiguration();
  }

  //Validate actual and expected scene configuration data.
  async validateTheScene() {
    await this.clickBrandingTab();

    await test.step("Capture Actual scene data.", async () => {
      this.actualSceneData = await this.captureStageConfiguration();
    });

    await test.step("Validating streams.", async () => {
      this.actualSceneData = await this.captureStageConfiguration();
      let actualNameList = this.actualSceneData.streamUserList;
      let expectedNameList = this.expectedSceneData.streamUserList;
      let actualNameListCount = actualNameList.length;
      let expectedNameListCount = expectedNameList.length;

      console.log(`Expected list - ${expectedNameList}`);
      console.log(`Actual list - ${actualNameList}`);

      if (actualNameListCount === expectedNameListCount) {
        let itr = 0;
        while (itr < actualNameListCount) {
          const indexOfUser = expectedNameList.indexOf(actualNameList[itr]);
          if (indexOfUser === -1) {
            console.log("Stream list doesn't match.");
            break;
          } else {
            console.log(`${actualNameList[itr]}  user stream found.`);
          }
          itr++;
        }
      }
    });

    await test.step("Validating brand color.", async () => {
      let actual = this.actualSceneData.colorCode;
      let expected = this.expectedSceneData.colorCode;
      console.log(`Expected Color - ${expected} || Actual Color - ${actual}`);
      expect.soft(actual).toEqual(expected);
    });

    await test.step("Validating theme.", async () => {
      let actual = this.actualSceneData.themeName;
      let expected = this.expectedSceneData.themeName;
      console.log(`Expected Theme - ${expected} || Actual Theme - ${actual}`);
      expect.soft(actual).toEqual(expected);
    });

    await test.step("Validating logo.", async () => {
      let actual = this.actualSceneData.logoSrc;
      let expected = this.expectedSceneData.logoSrc;
      console.log(`Expected Logo - ${expected} || Actual Logo - ${actual}`);
      expect.soft(actual).toEqual(expected);
    });

    await test.step("Validating background.", async () => {
      let actual = this.actualSceneData.backgroundSrc;
      let expected = this.expectedSceneData.backgroundSrc;
      console.log(
        `Expected Background - ${expected} || Actual Background - ${actual}`
      );
      expect.soft(actual).toEqual(expected);
    });

    await test.step("Validating overlay.", async () => {
      let actual = this.actualSceneData.overlaySrc;
      let expected = this.expectedSceneData.overlaySrc;
      console.log(
        `Expected Overlay - ${expected} || Actual Overlay - ${actual}`
      );
      expect.soft(actual).toEqual(expected);
    });
  }

  async deleteScene(sceneName: string = "Scene 1") {
    await test.step(`Deleting scene - ${sceneName}`, async () => {
      let sceneContainerLocator = await this.getSceneLocator(sceneName);
      sceneContainerLocator.hover();
      let sceneExtraIconLocator = await this.getSceneExtraIconLocator(
        sceneName
      );
      sceneExtraIconLocator.click();
      let deletButtonLocator = await this.getSceneDeleteAndUpdateButtonLocator(
        sceneName
      );
      await deletButtonLocator.filter({ hasText: "Delete" }).click();
    });
  }

  async verifySceneIsPresent(sceneName: string = "Scene 1") {
    await test.step(`Verifying scene - ${sceneName} is present.`, async () => {
      let sceneContainerLocator = await this.getSceneLocator(sceneName);
      await expect(sceneContainerLocator).toBeVisible({ timeout: 45000 });
    });
  }

  async verifySceneIsAbsent(sceneName: string = "Scene 1") {
    await test.step(`Verifying scene - ${sceneName} is not present.`, async () => {
      let sceneContainerLocator = await this.getSceneLocator(sceneName);
      await expect(sceneContainerLocator).not.toBeVisible({ timeout: 30000 });
    });
  }

  async updateScene(sceneName: string = "Scene 1") {
    await test.step(`Updating scene - ${sceneName}`, async () => {
      let sceneContainerLocator = await this.getSceneLocator(sceneName);
      sceneContainerLocator.hover();
      let sceneExtraIconLocator = await this.getSceneExtraIconLocator(
        sceneName
      );
      sceneExtraIconLocator.click();
      let updateButtonLocator = await this.getSceneDeleteAndUpdateButtonLocator(
        sceneName
      );
      await updateButtonLocator.filter({ hasText: "Update" }).click();
    });
  }

  async playScene(sceneName: string = "Scene 1") {
    await this.page.waitForTimeout(5000);
    await test.step(`Appling scene - ${sceneName}`, async () => {
      const sceneContainer = await this.getSceneLocator(sceneName);
      const playSceneButton = await this.getPlaySceneLocator(sceneName);

      await sceneContainer.hover();
      await playSceneButton.click();
      await sceneContainer.hover();

      //Added as a fix for BUG-1948(untill resolved)
      try {
        await expect(playSceneButton).not.toBeVisible({ timeout: 50000 });
      } catch (error) {
        await sceneContainer.hover();
        await playSceneButton.click();
        await sceneContainer.hover();
        await expect(playSceneButton).not.toBeVisible({ timeout: 50000 });
      }
      await this.page.waitForTimeout(5000);
    });
  }

  async getSceneLocator(sceneName: string) {
    let sceneContainerLocator = this.page.locator(
      `//div[contains(text(),'${sceneName}')]/../..//div[contains(@class,'shotsCardContainer')]`
    );
    return sceneContainerLocator;
  }

  async getSceneExtraIconLocator(sceneName: string) {
    let sceneExtraIconLocator = this.page.locator(
      `//div[contains(text(),'${sceneName}')]/../..//div[contains(@class,'ShotsCard_kebabContainer')]//div[contains(@class,'Icon_container')]`
    );
    return sceneExtraIconLocator;
  }

  async getPlaySceneLocator(sceneName: string) {
    let playSceneLocator = this.page.locator(
      `//div[contains(text(),'${sceneName}')]/../..//button[contains(@class,'playShotButton')]`
    );
    return playSceneLocator;
  }

  async getSceneDeleteAndUpdateButtonLocator(sceneName: string) {
    let sceneButtonLocator = this.page.locator(
      `//div[contains(text(),'${sceneName}')]/../..//div[contains(@class,'kebabMenu')]//button`
    );
    return sceneButtonLocator;
  }

  async getListOfUserCurrentlyStreaming() {
    let userNameList: string[] = [];
    let numberOfStreams = await this.streamNameTitleContainer.count();

    let itr = 0;
    while (itr < numberOfStreams) {
      let userName = await this.streamNameTitleContainer.nth(itr).textContent();
      console.log(`User${itr + 1} - ${userName}.`);
      userNameList.push(userName);
      itr++;
    }

    numberOfStreams = await this.placeholderNameContainer.count();
    itr = 0;
    while (itr < numberOfStreams) {
      let userName = await this.placeholderNameContainer.nth(itr).textContent();
      console.log(`Placeholder${itr + 1} - ${userName}.`);
      userNameList.push(userName);
      itr++;
    }

    return userNameList;
  }

  async verifyCountOfPlaceholderInStage(expectedPlaceholderCount = 2) {
    await this.stageTab.click();
    await this.page.waitForTimeout(5000);
    await expect(this.placeholderNameContainer).toHaveCount(
      expectedPlaceholderCount,
      {
        timeout: 30000,
      }
    );
  }
}
