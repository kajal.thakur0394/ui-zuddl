import { EventSetupPage } from "../new-org-side-pages/event-setup";
import { Page } from "@playwright/test";


export class EventSetupHandler extends EventSetupPage {
    constructor(page: Page) {
        super(page);
    }
}
