import { Locator, Page, expect } from "@playwright/test";
import { DataUtl } from "../../util/dataUtil";
import { Features } from "../../enums/AccessControlEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

export class LiveEventSideVerifications {
  private readonly page;
  private readonly eventId;
  private readonly attendeeEmail;

  private readonly navbarLobbyLocator: Locator;
  private readonly navbarScheduleLocator: Locator;
  private readonly navbarStageLocator: Locator;
  private readonly navbarRoomsLocator: Locator;
  private readonly navbarExpoLocator: Locator;
  private readonly navbarNetworkingLocator: Locator;

  constructor(page: Page, eventId, attendeeEmail: string) {
    this.page = page;
    this.eventId = eventId;
    this.attendeeEmail = attendeeEmail;

    this.navbarLobbyLocator = this.page.getByText(`Lobby`, { exact: true });
    this.navbarScheduleLocator = this.page.getByText(`Schedule`, {
      exact: true,
    });
    this.navbarStageLocator = this.page.getByText(`Stage`, { exact: true });
    this.navbarRoomsLocator = this.page.getByText(`Rooms`, { exact: true });
    this.navbarExpoLocator = this.page.getByText(`Expo`, { exact: true });
    this.navbarNetworkingLocator = this.page.getByText(`Networking`, {
      exact: true,
    });
  }

  private async verifyNavbarVisibility() {
    let navbarLocator = this.page.locator("div[id='top-nav-bar']");
    await expect(navbarLocator).toBeVisible();
  }

  async attendeeJoinsTheEvent() {
    let magicLink = await QueryUtil.fetchMagicLinkFromDB(
      this.eventId,
      this.attendeeEmail
    );

    await this.page.goto(magicLink);
    await this.page.click("text=Enter Now");
    await this.verifyNavbarVisibility();
  }

  async reloadPage() {
    await this.page.reload();
    await this.verifyNavbarVisibility();
  }

  async verifyStageVisibility(stageName: string, visibilityStatus: boolean) {
    let stagePageUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${this.eventId}/stages/`;

    await this.page.goto(stagePageUrl);

    let stageLocator = this.page.getByText(stageName, {
      exact: true,
    });
    await expect(stageLocator).toBeVisible({ visible: visibilityStatus });
  }

  async verifyScheduleAccessibiity(SessionName: string, visibility: boolean) {
    let schedulePageUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${this.eventId}/schedule/`;

    await this.page.goto(schedulePageUrl);

    let sessionLocator = this.page.getByText(SessionName, { exact: true });
    await expect(sessionLocator).toBeVisible({ visible: visibility });
  }

  async verifyNavbarSections(featureName: Features, visibilityStatus) {
    switch (featureName) {
      case Features.LOBBY:
        await expect(this.navbarLobbyLocator).toBeVisible({
          visible: visibilityStatus,
        });
        break;

      case Features.SCHEDULE:
        await expect(this.navbarScheduleLocator).toBeVisible({
          visible: visibilityStatus,
        });
        break;

      case Features.STAGE:
        await expect(this.navbarStageLocator).toBeVisible({
          visible: visibilityStatus,
        });
        break;

      case Features.ROOM:
        await expect(this.navbarRoomsLocator).toBeVisible({
          visible: visibilityStatus,
        });
        break;

      case Features.EXPO:
        await expect(this.navbarExpoLocator).toBeVisible({
          visible: visibilityStatus,
        });
        break;

      case Features.NETWORKING:
        await expect(this.navbarNetworkingLocator).toBeVisible({
          visible: visibilityStatus,
        });
        break;
    }
  }

  async verifyScheduleVisibility() {}
}
