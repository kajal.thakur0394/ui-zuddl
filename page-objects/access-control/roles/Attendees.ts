import { Locator, Page, expect } from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import { Features } from "../../../enums/AccessControlEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

export class AccessControlAttendeeVerification {
  private readonly page;
  private readonly eventId;
  private readonly attendeeEmail;

  private readonly navbarLobbyLocator: Locator;
  private readonly navbarScheduleLocator: Locator;
  private readonly navbarStageLocator: Locator;
  private readonly navbarRoomsLocator: Locator;
  private readonly navbarExpoLocator: Locator;
  private readonly navbarNetworkingLocator: Locator;
  private readonly scheduleContainerLocator: Locator;
  private readonly blankSchedulePageLocator: Locator;
  private readonly notifivationIconLocator: Locator;

  constructor(page: Page, eventId, attendeeEmail: string) {
    this.page = page;
    this.eventId = eventId;
    this.attendeeEmail = attendeeEmail;

    this.navbarLobbyLocator = this.page.getByText(`Lobby`, { exact: true });
    this.navbarScheduleLocator = this.page.getByText(`Schedule`, {
      exact: true,
    });
    this.navbarStageLocator = this.page.getByText(`Stage`, { exact: true });
    this.navbarRoomsLocator = this.page.getByText(`Rooms`, { exact: true });
    this.navbarExpoLocator = this.page.getByText(`Expo`, { exact: true });
    this.navbarNetworkingLocator = this.page.getByText(`Networking`, {
      exact: true,
    });
    this.scheduleContainerLocator = this.page.locator(
      'div[class*="segmentInnerContainer"]'
    );
    this.blankSchedulePageLocator = this.page.getByText(
      "We're glad to have you here, AutomationSchedule is not out yet.Please explore"
    );
    this.notifivationIconLocator = this.page.locator("#notificationBellIcon");
  }

  private async verifyNavbarVisibility() {
    let navbarLocator = this.page.locator("div[id='top-nav-bar']");
    await expect(navbarLocator).toBeVisible();
  }

  async attendeeJoinsTheEvent() {
    let magicLink = await QueryUtil.fetchMagicLinkFromDB(
      this.eventId,
      this.attendeeEmail
    );

    await this.page.goto(magicLink);
    await this.page.click("text=Enter Now");
    await this.verifyNavbarVisibility();
  }

  async reloadPage() {
    await this.page.reload();
    await this.verifyNavbarVisibility();
  }

  async verifyStageVisibility(stageName: string, visibilityStatus: boolean) {
    let stagePageUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${this.eventId}/stages/`;

    await this.page.goto(stagePageUrl);

    let stageLocator = this.page.getByText(stageName, {
      exact: true,
    });
    await expect(stageLocator).toBeVisible({ visible: visibilityStatus });
  }

  async verifyStageAccessibiity(stageName: string) {
    let stagePageUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${this.eventId}/stages/`;

    await this.page.goto(stagePageUrl);

    let stageLocator = this.page.getByText(stageName, { exact: true });
    await stageLocator.click();

    let stageContainerLocator = this.page
      .frameLocator('iframe[title="Stage Studio"]')
      .locator("#app");
    await expect(stageContainerLocator).toBeVisible({ timeout: 5000 });
  }

  async verifyNavbarSections(featureName: Features, visibilityStatus) {
    switch (featureName) {
      case Features.LOBBY:
        await expect(this.navbarLobbyLocator).toBeVisible({
          visible: visibilityStatus,
        });
        break;

      case Features.SCHEDULE:
        await expect(this.navbarScheduleLocator).toBeVisible({
          visible: visibilityStatus,
        });
        break;

      case Features.STAGE:
        await expect(this.navbarStageLocator).toBeVisible({
          visible: visibilityStatus,
        });
        break;

      case Features.ROOM:
        await expect(this.navbarRoomsLocator).toBeVisible({
          visible: visibilityStatus,
        });
        break;

      case Features.EXPO:
        await expect(this.navbarExpoLocator).toBeVisible({
          visible: visibilityStatus,
        });
        break;

      case Features.NETWORKING:
        await expect(this.navbarNetworkingLocator).toBeVisible({
          visible: visibilityStatus,
        });
        break;
    }
  }

  async showAllSchedules() {
    let dateFilterButton = this.page.locator("#dateFilterButton");
    let selectAllInput = this.page.locator(
      '(//div[contains(@class,"datesListContainer")]//input)[1]'
    );

    await dateFilterButton.click();
    await selectAllInput.click();
    await expect(selectAllInput).toBeChecked({ checked: false });
    await selectAllInput.click();
    await expect(selectAllInput).toBeChecked({ checked: true });
  }

  async verifyScheduleVisibility(
    scheduleName: string,
    visibilityStatus: boolean
  ) {
    let schedulePageUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${this.eventId}/schedule/`;

    await this.page.goto(schedulePageUrl);

    try {
      await expect(this.scheduleContainerLocator).toBeVisible();
      await this.showAllSchedules();
    } catch (error) {
      await expect(this.blankSchedulePageLocator).toBeVisible();
    }

    let scheduleLocator = this.page.getByText(scheduleName, { exact: true });
    await expect(scheduleLocator).toBeVisible({ visible: visibilityStatus });
  }

  async verifyNoAccessMessage() {
    let schedulePageUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${this.eventId}/stages/`;
    await this.page.goto(schedulePageUrl);

    let noAccessMessagePopup = this.page.getByText(
      "Uh Oh.You do not have access"
    );

    await expect(noAccessMessagePopup).toBeVisible();
  }

  async verifyRoomVisibility(roomName: string, visibilityStatus: boolean) {
    let roomPageUrl = `${DataUtl.getApplicationTestDataObj().baseUrl}/l/event/${
      this.eventId
    }/discussions?redirect=true`;

    await this.page.goto(roomPageUrl);

    let roomsContainerLocator = this.page.locator(
      'div[class*="liveEventInnerLayoutBody"]'
    );
    await expect(roomsContainerLocator).toBeVisible();

    let roomLocator = this.page
      .locator("label")
      .filter({ hasText: `${roomName}` });
    await expect(roomLocator).toBeVisible({ visible: visibilityStatus });
  }

  async verifyStageNameVisibilityUnderScheduleFilter(
    stageName: string,
    toggle: boolean
  ) {
    let schedulePageUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${this.eventId}/schedule/`;
    await this.page.goto(schedulePageUrl);

    await expect(this.scheduleContainerLocator).toBeVisible();

    let skipButton = this.page.getByText("Skip");
    try {
      await expect(skipButton).toBeVisible();
      await skipButton.click();
    } catch (error) {
      console.log("Skip button not present.");
    }

    let venueFilterButton = this.page.locator("#venueFilterButton");
    let stageFilterExpand = this.page.locator(
      '//div[contains(@class, "venueLabel") and text()="Stage"]/..//div[ contains(@class, "expandIcon")]'
    );

    await venueFilterButton.click();

    try {
      await stageFilterExpand.click();
    } catch (error) {
      console.log("Stage filter option is missing as there is no stage ");
    }

    let stageNameFilter = this.page.locator(
      ` //div[contains(@class, "venueLabel") and text()="${stageName}"]`
    );
    await expect(stageNameFilter).toBeVisible({ visible: toggle });
  }

  async verifyStageAccessViaLink(
    stageId: string | number,
    accessible: boolean
  ) {
    const schedulePageUrl = `${
      DataUtl.getApplicationTestDataObj().baseUrl
    }/l/event/${this.eventId}/stages/${stageId}`;

    await this.page.goto(schedulePageUrl);
    await this.page.waitForTimeout(5000);
    const currentUrl = this.page.url();

    if (!accessible && currentUrl.includes(stageId)) {
      throw new Error(
        `Stage is accessible even after diasbling the access for the user.`
      );
    } else if (accessible && !currentUrl.includes(stageId)) {
      throw new Error(
        `Stage is not accessible even after enabling the access for the user.`
      );
    }
  }

  async verifySessionLiveNotification(
    sessionName: string,
    visibilityStatus: boolean
  ) {
    await this.notifivationIconLocator.click();

    let sessionLiveNotification = this.page.locator(
      `//div[contains(@class,"notificationChild")]//*[text() = "${sessionName}"]`
    );

    await expect(sessionLiveNotification).toBeVisible({
      visible: visibilityStatus,
    });
  }
}
