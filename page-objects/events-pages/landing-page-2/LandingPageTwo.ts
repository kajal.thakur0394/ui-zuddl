import { expect, Locator, Page } from "@playwright/test";
import { RegistrationFormComponent } from "./RegistrationFormComponent";
import { LoginOptionsComponent } from "./LoginOptionsComponent";
import { RestrictedEventScreen } from "./RestrictedEventScreen";
import { FAQsComponent } from "./FAQscomponents";
import { ScheduleComponent } from "./ScheduleComponent";
import { SpeakerComponent } from "./SpeakerComponents";
import { AboutSectionComponent } from "./AboutSectionComponenet";
import { SponsorComponent } from "./SponsorComponent";
import { BoothSectionComponent } from "./BoothSectionComponent";
import { SpeakerLandingComponent } from "./SpeakerLandingComponents";
import { RestrictedSpeakerAccessPage } from "./RestrictedSpeakerAccessPage";
import { RegistrationConfirmationComponent } from "./RegistrationConfirmationComponent";
import { WebinarStage } from "../../webinar-pages/webinarStage";
import { AddToCalendar } from "./addToCalendar";
import { DateTimecounterComponent } from "./DateTimecounterComponent";
import { LobbyPage } from "../zones/LobbyPage";
import { GuestAuthFormComponent } from "./GuestAuthFormComponent";

export class LandingPageTwo {
  readonly page: Page;
  readonly registrationFormComponent: RegistrationFormComponent;
  readonly loginOptionsComponent: LoginOptionsComponent;
  readonly restrictedEventScreenComponent: RestrictedEventScreen;
  readonly faqscomponent: FAQsComponent;
  readonly scheduleComponent: ScheduleComponent;
  readonly speakerComponent: SpeakerComponent;
  readonly aboutSectionComponent: AboutSectionComponent;
  readonly sponsorComponent: SponsorComponent;
  readonly boothSectionComponent: BoothSectionComponent;
  readonly speakerLandingComponent: SpeakerLandingComponent;
  readonly restrictedSpeakerAccessPage: RestrictedSpeakerAccessPage;
  readonly registrationConfirmationComponent: RegistrationConfirmationComponent;
  readonly notificationContainer: Locator;
  readonly enterNowButton: Locator;
  readonly addToCalendar: AddToCalendar;
  readonly dateTimeCounter: DateTimecounterComponent;
  readonly loginAsGuestButton: Locator;

  constructor(page: Page) {
    this.page = page;
    this.registrationFormComponent = new RegistrationFormComponent(page);
    this.loginOptionsComponent = new LoginOptionsComponent(page);
    this.restrictedEventScreenComponent = new RestrictedEventScreen(page);
    this.faqscomponent = new FAQsComponent(page);
    this.scheduleComponent = new ScheduleComponent(page);
    this.speakerComponent = new SpeakerComponent(page);
    this.sponsorComponent = new SponsorComponent(page);
    this.boothSectionComponent = new BoothSectionComponent(page);
    this.speakerLandingComponent = new SpeakerLandingComponent(page);
    this.restrictedSpeakerAccessPage = new RestrictedSpeakerAccessPage(page);
    this.registrationConfirmationComponent =
      new RegistrationConfirmationComponent(page);
    this.notificationContainer = this.page.locator(
      "div[class^='styles-module__notificationContainer']"
    );
    this.enterNowButton = this.page.locator("button:has-text('Enter Now')");
    this.addToCalendar = new AddToCalendar(page);
    this.dateTimeCounter = new DateTimecounterComponent(page);
    this.aboutSectionComponent = new AboutSectionComponent(page);
    this.loginAsGuestButton = this.page.locator(
      "div[class^='styles-module__guestLoginBtn']"
    );
  }

  async clickOnLoginAsGuest() {
    await this.loginAsGuestButton.click();
    return new GuestAuthFormComponent(this.page);
  }

  async getLoginOptionsComponent() {
    return await this.loginOptionsComponent;
  }

  async getRegistrationFormComponent() {
    return await this.registrationFormComponent;
  }

  async getRestrcitedEventScreen() {
    return await this.restrictedEventScreenComponent;
  }

  async load(url: string) {
    await this.page.goto(url);
  }

  async getFAQsComponents() {
    return await this.faqscomponent;
  }

  async getScheduleComponent() {
    return await this.scheduleComponent;
  }

  async getSpeakerComponents() {
    return await this.speakerComponent;
  }

  async getAboutSectionComponents() {
    return await this.aboutSectionComponent;
  }

  async getSponsorComponents() {
    return await this.sponsorComponent;
  }

  async getBoothSectionComponents() {
    return await this.boothSectionComponent;
  }

  async getSpeakerLandingComponent() {
    return await this.speakerLandingComponent;
  }

  async getEventRestrictedSpeakersPage() {
    return await this.restrictedSpeakerAccessPage;
  }

  async getRegistrationConfirmationScreen() {
    return await this.registrationConfirmationComponent;
  }

  async getDateTimeCounterComponent() {
    return await this.dateTimeCounter;
  }

  //toast message on submitting email for otp when magic link enabled
  async isSuccessToastMessageVisible() {
    //toast message on submitting email for otp when magic link enabled
    await expect(
      this.page.locator("div[class^='styles-module__magicLinkSentText']")
    ).toContainText("Success");
  }

  async clickOnEnterNowButton() {
    await this.enterNowButton.click();
  }

  async clickOnLoginButton() {
    //this comes on speaker landing page
    await this.page.click("text=Log in");
    return new LoginOptionsComponent(this.page);
  }
  async clickOnSigninButton() {
    //this comes on speaker landing page
    await this.page.click("text=Sign in");
    return new LoginOptionsComponent(this.page);
  }

  async waitForToastMessageToDisappear() {
    // await expect(this.notificationContainer,'expecting notification container to be visible').toBeVisible()
    // await this.notificationContainer.waitFor({state:"hidden",timeout:15000})
    try {
      await this.page.waitForSelector(
        "div[class^='styles-module__notificationContainer']",
        {
          state: "visible",
        }
      );
      // await this.page.waitForSelector("div[class^='styles-module__notificationContainer']",{
      //     state:"hidden"
      // })
      // await expect(this.notificationContainer).toBeHidden({timeout:15000})
      await this.page
        .locator("div[class^='styles-module__notificationContainer'] button")
        .click();
    } catch (err) {
      console.error(
        `${err} occured while trying to click on notification container button`
      );
    }
  }

  async isLobbyLoaded() {
    await this.page.waitForURL(/lobby/, {
      timeout: 10000,
    });
    return new LobbyPage(this.page);
  }

  async isStageLoaded(isWebinar = false) {
    await this.page.waitForURL(/stage/, {
      timeout: 10000,
    });
    if (isWebinar) {
      return new WebinarStage(this.page);
    }
  }

  async isNotificationToastVisibleOnEnterNowPage() {
    await this.notificationContainer.waitFor({ state: "visible" });
    return await this.notificationContainer.isVisible();
  }

  async getAddToCalendarComponenet() {
    return await this.addToCalendar;
  }
}
