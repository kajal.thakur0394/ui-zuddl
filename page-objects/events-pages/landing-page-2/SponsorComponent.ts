import { expect, Locator, Page } from "@playwright/test";

export class SponsorComponent {
  //selectors
  readonly page: Page;
  readonly Sponsor: Locator;
  readonly SpeakerFields: Locator;

  //constructor
  constructor(page: Page) {
    this.page = page;
    this.Sponsor = this.page.locator("id=landingPageSponsorsSection");
    this.SpeakerFields = this.page.locator(
      "div[class^='styles-module__sponsorCard']"
    );
  }

  async getTotalNumberOfSponsors() {
    let countOfSponsores = await this.SpeakerFields.count();
    return countOfSponsores;
  }

  async isVisible() {
    await expect(this.Sponsor).toBeVisible();
  }

  async isHidden() {
    await expect(this.Sponsor).toBeHidden();
  }

  async getSponsorDetails() {
    let noOfSponsors = await this.SpeakerFields.count();
    const SponsorDetails = new Array();
    // console.log(""+noOfSpeakers);
    for (let index = 0; index < noOfSponsors; index++) {
      const element = this.SpeakerFields.nth(index);
      let elementText = (await element.innerHTML()).toString();
      console.log(elementText);
      SponsorDetails.push(elementText);
    }
    return SponsorDetails;
  }
}
