import { expect, Page } from "@playwright/test";
export class RestrictedEventScreen {
  readonly page: Page;
  constructor(page: Page) {
    this.page = page;
  }

  async isVisible() {
    let expectedText1Locator = this.page.locator(
      "text=text=Entered email has no access for this event"
    );
    let expectedText2Locator = this.page.locator(
      "text=You do not have access to this"
    );

    if (
      (await expectedText1Locator.isVisible()) ||
      (await expectedText2Locator.isVisible())
    ) {
      return true;
    }
    return false;
  }

  async isPrivateEventScreenVisible() {
    await expect(
      this.page.locator("div[class^='styles-module__mainHeading']")
    ).toHaveText("Private Event");
  }

  async isPrivateEventScreenVisibleForOTPFlow() {
    await expect(
      this.page.locator("text=Entered email has no access for this event")
    ).toBeVisible();
  }

  async isEventEndedScreenisVisible() {
    await expect(
      this.page.locator("text=The event you're trying to join has ended")
    ).toBeVisible();
  }
}
