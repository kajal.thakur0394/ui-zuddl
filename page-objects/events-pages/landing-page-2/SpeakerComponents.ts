import { expect, Locator, Page } from "@playwright/test";

export class SpeakerComponent {
  //selectors
  readonly page: Page;
  readonly Speaker: Locator;
  readonly SpeakerFields: Locator;

  //constructor
  constructor(page: Page) {
    this.page = page;
    this.Speaker = this.page.locator("id=landingPageSpeakersSection");
    this.SpeakerFields = this.page.locator(
      "div[class^='styles-module__speakerCardContent']"
    );
  }

  async getTotalNumberOfSpeakers() {
    let countOfSpeakers = await this.SpeakerFields.count();
    return countOfSpeakers;
  }

  async isVisible() {
    await expect(this.Speaker).toBeVisible();
  }

  async isHidden() {
    await expect(this.Speaker).toBeHidden();
  }

  async getSpeakerDetails() {
    let noOfSpeakers = await this.SpeakerFields.count();
    const speakerDetails = new Array();
    // console.log(""+noOfSpeakers);
    for (let index = 0; index < noOfSpeakers; index++) {
      const element = this.SpeakerFields.nth(index);
      let elementText = (await element.allTextContents()).toString();
      console.log(elementText);
      speakerDetails.push(elementText);
    }
    return speakerDetails;
  }
}
