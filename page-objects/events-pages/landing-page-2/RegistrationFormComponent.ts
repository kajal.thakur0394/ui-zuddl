import { expect, Locator, Page } from "@playwright/test";
import { DataUtl } from "../../../util/dataUtil";
import { LoginOptionsComponent } from "./LoginOptionsComponent";

export class RegistrationFormComponent {
  //selectors
  readonly page: Page;
  readonly regFormHeader: Locator;
  readonly regFormFieldLabel: Locator;
  readonly regFormFieldContainer: Locator; // filed container is ancestor div of field label
  readonly registerNowButton: Locator;
  readonly alreadyRegisteredButton: Locator;
  readonly regFormContainer: Locator;

  //constructor
  constructor(page: Page) {
    this.page = page;
    this.regFormHeader = this.page.locator(
      "div[class^='styles-module__loginHeaderContainer']"
    );
    this.regFormContainer = this.page.locator(
      "div[class^='styles-module__formSectionContainer']"
    );
    // this.regFormLocator=this.page.locator("text=Enter your details") //header on this form with this text
    this.regFormFieldLabel = this.page.locator("label");
    this.registerNowButton = this.page.locator(
      "button:has-text('Register now')"
    );
    this.regFormFieldContainer = this.page.locator(
      "div[class^='styles-module__formSectionRowContainer']"
    );
    this.alreadyRegisteredButton = this.page.locator(
      "text=Already registered?"
    );
  }

  async getTotalNumberOfRegistrationFields() {
    let countOfFields = await this.regFormFieldLabel.count();
  }

  async fillUpTheRegistrationForm(
    userRegistrationFormObj,
    options?: {
      fillMandatoryFields?: boolean | true;
      fillOptionalFields?: boolean | true;
      clickOnContinueButton?: boolean | false;
      fieldsToSkip?: string[];
      ConditionalsFieldsToSkip?: string[];
      clickOnDesclaimerButton?: boolean | false;
      fieldsToBeHidden?: string[];
    },
    isSuccessfulRegistrationExpected: boolean = true
  ) {
    await this.page.waitForTimeout(5000);
    let registrationPostApiPromise;
    if (isSuccessfulRegistrationExpected) {
      registrationPostApiPromise = this.page.waitForResponse(
        /user_registration/,
        {
          timeout: 30000,
        }
      );
    }
    // for each reg form field, we will iterate and fill up the form
    for (let eachField in userRegistrationFormObj) {
      console.log(`handling the field ${eachField}`);
      let filedObj = userRegistrationFormObj[eachField]; //will get the object
      let fieldLabel = filedObj["label"];
      let fieldType = filedObj["fieldType"];
      let fieldValueToEnter = filedObj["dataToEnter"];
      let isFieldOptional = filedObj["isOptionalField"];
      let isFieldConditional = filedObj["isConditionaField"];
      // if condition is given to fill only mandatory fields
      console.log(
        `handling field ${fieldLabel} of type ${fieldType} with value ${fieldValueToEnter}`
      );
      if (options) {
        if (options.fieldsToSkip?.includes(fieldLabel)) {
          console.log("Field Label excluded:", fieldLabel);
          continue;
        }
        if (options?.fillMandatoryFields && !options.fillOptionalFields) {
          if (isFieldOptional) {
            console.log(`filed is optional hence skipping it`);
            continue;
          } else {
            await this.enterDataInRegFormField(
              fieldLabel,
              fieldType,
              fieldValueToEnter
            );
          }
        } else if (
          !options?.fillMandatoryFields &&
          options?.fillOptionalFields
        ) {
          if (!isFieldOptional) {
            console.log(`filed is mandatory hence skipping it`);
            continue;
          } else {
            await this.enterDataInRegFormField(
              fieldLabel,
              fieldType,
              fieldValueToEnter
            );
          }
        } else if (options?.fillMandatoryFields && options.fillOptionalFields) {
          console.log(`filling up the field`);
          await this.enterDataInRegFormField(
            fieldLabel,
            fieldType,
            fieldValueToEnter
          );
        }
      } else {
        await this.enterDataInRegFormField(
          fieldLabel,
          fieldType,
          fieldValueToEnter
        );
      }
    }

    for (let fieldLabelToCheck in options?.fieldsToBeHidden) {
      console.log(
        `Verifying field with label ${options?.fieldsToBeHidden[fieldLabelToCheck]} is hidden.`
      );
      await this.regFieldIsHidden(options?.fieldsToBeHidden[fieldLabelToCheck]);
    }

    //if its before auth, then its register button else its continue button
    if (options?.clickOnDesclaimerButton) {
      await this.clickOnDesclaimerButtonOnRegisterationForm();
    }
    //if its before auth, then its register button else its continue button
    if (options?.clickOnContinueButton) {
      await this.clickOnContinueButtonOnRegisterationForm();
    } else {
      await this.registerNowButton.click();
    }

    const response = await registrationPostApiPromise;
    if (isSuccessfulRegistrationExpected) {
      if (response.status() != 200 && response.status() != 406) {
        throw new Error(
          `user registration post api failed with ${response.status()}`
        );
      }
    }
  }

  async clickOnContinueButtonOnRegisterationForm() {
    await this.page.locator("text=Continue").click();
  }

  async clickOnDesclaimerButtonOnRegisterationForm() {
    await this.page.locator("label[for='label0']").click();
  }

  async enterDataInRegFormField(
    fieldLabel: string,
    fieldType: string,
    dataToEnter: any
  ) {
    let regFieldContainer: Locator = await this.getTheRegFieldContainer(
      fieldLabel
    );
    //now check what is the field type of this and accordingly enter or select the data
    switch (fieldType) {
      case "text": {
        if (fieldLabel === 'Country') {
          await this.page.locator(
            `div[class^='styles-module__selectField']:has-text('${fieldLabel}')`).click();
          await this.page.locator((`div[id^='react-select-']:has-text('${dataToEnter}')`)).nth(1).click();
        } else {
          let inputBox = await regFieldContainer.locator("input");
          await inputBox.fill(dataToEnter);
        }
        break;
      }
      case "multiselect-dropdown": {
        let multiSelectDropdownContainer = regFieldContainer.locator(
          "div[class='dropdown-container']"
        );
        await multiSelectDropdownContainer.click();
        // check if type of dataToEnter is not list then throw error
        if (!Array.isArray(dataToEnter)) {
          throw new Error(
            `For multiselect dropdown options,  invalid type passed, expected type is list`
          );
        }
        // list of options i want to select
        for (let i in dataToEnter) {
          let optionToSelect = dataToEnter[i];
          console.log(`selection option ${optionToSelect}`);
          await multiSelectDropdownContainer
            .locator(`label[role='option']:has-text('${optionToSelect}')`)
            .click();
          console.log(`selected option ${optionToSelect}`);
        }
        //after selecting the options, we will have to close the dropdown options
        await multiSelectDropdownContainer
          .locator(".dropdown-heading-dropdown-arrow")
          .click();
        break;
      }
      case "dropdown": {
        // in this case list of options should be given to select
        let dropdownContainer = regFieldContainer.locator(
          "div[class*='styles-module__selectField']"
        );
        await dropdownContainer.click();
        await dropdownContainer
          .locator(`div[class*='option']:has-text('${dataToEnter}')`)
          .click();
        break;
      }
      case "number": {
        //works same as input box
        let inputBox = regFieldContainer.locator("input");
        await inputBox.fill(dataToEnter);
        break;
      }
      default: {
        throw new Error(
          `Invalid field type ${fieldType} passed for field label ${fieldLabel}`
        );
      }
    }
  }

  async regFieldIsVisible(fieldLabel: string) {
    let thisRegFieldContainer: Locator = this.regFormFieldContainer.filter({
      has: this.regFormFieldLabel.filter({
        hasText: fieldLabel,
      }),
    });
    // verify the field is visible.
    await expect(
      thisRegFieldContainer,
      `expecting Field with label ${fieldLabel} to be visible`
    ).toBeVisible();
  }

  async regFieldIsHidden(fieldLabel: string) {
    let thisRegFieldContainer: Locator = this.regFormFieldContainer.filter({
      has: this.regFormFieldLabel.filter({
        hasText: fieldLabel,
      }),
    });
    // verify the field is hidden.
    await expect(
      thisRegFieldContainer,
      `expecting Field with label ${fieldLabel} to not be visible`
    ).not.toBeVisible();
  }

  async getTheRegFieldContainer(fieldLabel: string) {
    let thisRegFieldContainer: Locator = this.regFormFieldContainer.filter({
      has: this.regFormFieldLabel.filter({
        hasText: fieldLabel,
      }),
    });
    // verify the count is 1, otherwise throw error cz we expect to find only 1 unique locator
    await expect(
      thisRegFieldContainer,
      `expecting Field with label ${fieldLabel} to have count ${1}`
    ).toHaveCount(1);
    return thisRegFieldContainer;
  }

  async clickOnAlreadyRegisteredButton() {
    await this.alreadyRegisteredButton.click();
    return new LoginOptionsComponent(this.page);
  }

  async isVisible() {
    await expect(this.registerNowButton).toBeVisible();
  }

  async emailFieldIsPrefilledWith(emailAddress: string) {
    let emailFieldLocator = await this.getTheRegFieldContainer("Email");
    let inputField = emailFieldLocator.locator("input");
    await expect(inputField).toHaveValue(emailAddress);
  }

  async isNotVisible() {
    await expect(this.registerNowButton).toBeHidden();
  }

  async isRegistrationConfirmationMessageVisible(
    events_with_magic_link: boolean = false
  ) {
    let messageLocator: Locator = this.page.locator(
      "div[class^='styles-module__registrationMessageC']"
    );
    if (events_with_magic_link) {
      await expect(
        messageLocator,
        "Checking registration confirmation message for events with magic link"
      ).toContainText("Check your email for the event invite");
    }
    await expect(
      messageLocator,
      "Checking registration confirmation message for events without magic link"
    ).toHaveText("You've successfully registered!");
  }

  async verifyThisFieldIsRequiredErrorMessageIsDisplayed() {
    let errorMessage = this.page.locator("text=This field is required").first();
    await expect(
      errorMessage,
      "expecting atleast 1 error message on mandatory field is visisble"
    ).toBeVisible();
  }

  async verifyDisclaimerIsRequiredErrorMessageIsDisplayed() {
    let errorMessage = this.page.locator("text=Please accept these conditions to register").first();
    await expect(
      errorMessage,
      "You are registering and giving your consent to save you records"
    ).toBeVisible();
  }

}
