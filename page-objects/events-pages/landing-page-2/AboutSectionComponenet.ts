import { expect, Locator, Page } from "@playwright/test";

export class AboutSectionComponent {
  //selectors
  readonly page: Page;
  readonly About: Locator;
  readonly AboutText: Locator;

  //constructor
  constructor(page: Page) {
    this.page = page;
    this.About = this.page.locator("id=landingPageAboutSection");
    this.AboutText = this.page.locator(
      "div[class^='styles-module__aboutSectionContent']"
    );
  }

  async isVisible() {
    await expect(this.About).toBeVisible();
  }

  async isHidden() {
    await expect(this.About).toBeHidden();
  }

  async getAboutText() {
    return await this.AboutText.allInnerTexts();
  }

  async verifyAboutText(textToBeverified: string) {
    let aboutText =  await this.AboutText.allInnerTexts();
    console.log("The aboutText content is: ",aboutText)
    expect(aboutText,`About text to be ${textToBeverified}`).toContain(textToBeverified);
  }

}
