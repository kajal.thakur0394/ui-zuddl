import { expect, Locator, Page } from "@playwright/test";

export class GuestAuthFormComponent {
  //selectors
  readonly page: Page;
  readonly firstNameInputBox: Locator;
  readonly lastNameInputBox: Locator;
  readonly emailInputBox: Locator;
  readonly continueButton: Locator;

  //constructor
  constructor(page: Page) {
    this.page = page;
    this.firstNameInputBox = this.page.locator("input[name='firstName']");
    this.lastNameInputBox = this.page.locator("input[name='lastName']");
    this.emailInputBox = this.page.locator("input[name='email']");
    this.continueButton = this.page.locator("button:has-text('Continue')");
  }

  //actions
  async fillGuestAuthForm({ firstName, lastName, email }) {
    await this.firstNameInputBox.fill(firstName);
    await this.lastNameInputBox.fill(lastName);
    await this.emailInputBox.fill(email);
    await this.continueButton.click();
  }
}
