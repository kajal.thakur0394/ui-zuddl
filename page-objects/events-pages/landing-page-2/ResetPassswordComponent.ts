import { expect, FrameLocator, Locator, Page } from "@playwright/test";

export class ResetPasswordComponent {
  //selectors
  readonly page: Page;
  readonly setPasswordInputBox: Locator;
  readonly confirmPasswordInputBox: Locator;
  readonly continueButton: Locator;

  //constructor
  constructor(page: Page) {
    this.page = page;
    this.setPasswordInputBox = this.page
      .locator("input[class^='styles-module__passwordField']")
      .nth(0);
    this.confirmPasswordInputBox = this.page
      .locator("input[class^='styles-module__passwordField']")
      .nth(1);
    this.continueButton = this.page.locator("button:has-text('Continue')");
  }

  //actions
  async setPassword(userPassword: string) {
    await expect(
      this.setPasswordInputBox,
      `expecting set password input box to be visible in order to fill it`
    ).toBeVisible();
    await this.setPasswordInputBox.fill(userPassword);
    await this.page.keyboard.press("Tab");
    await this.confirmPasswordInputBox.fill(userPassword);
    await this.continueButton.click();
  }
}
