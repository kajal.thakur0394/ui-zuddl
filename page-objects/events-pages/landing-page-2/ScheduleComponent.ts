import { expect, Locator, Page } from "@playwright/test";

export class ScheduleComponent {
  //selectors
  readonly page: Page;
  readonly Schedule: Locator;
  readonly ScheduleFields: Locator;
  readonly HighlightedDate: Locator;
  readonly scheduledates: Locator;

  //constructor
  constructor(page: Page) {
    this.page = page;
    this.Schedule = this.page.locator("id=landingPageFAQsSection");
    this.ScheduleFields = this.page.locator(
      "div[class^='styles-module__faqContent']"
    );
    this.HighlightedDate = this.page.locator(
      "div[class^='styles-module__selectedDay']"
    );
    this.scheduledates = this.page.locator(
      "div[class^='styles-module__afterSelectedDay']"
    );
  }

  async isVisible() {
    await expect(this.Schedule).toBeVisible();
  }

  async isHidden() {
    await expect(this.Schedule).toBeHidden();
  }

  async isDateHighlighted() {
    await expect(this.HighlightedDate).toBeVisible();
  }

  async getHighlightedDate() {
    return this.page.locator(
      "//div[@class='styles-module__selectedDay___2xHyx']//div[2]"
    );
    // console.log(Date);
    // return (""+Date);
  }

  async getHighlightedDay() {
    return this.page.locator(
      "//div[@class='styles-module__selectedDay___2xHyx']//div[1]"
    );
    // console.log(""+Day);
    // return (""+Day);
  }

  async isHighlightedDayMatchingCurrentDay() {
    var days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    var d = new Date();
    var dayName = days[d.getDay()];
    var currDate = d.getDate();
    await expect(await this.getHighlightedDay()).toHaveText(dayName);
    await expect(await this.getHighlightedDate()).toHaveText("" + currDate);
  }

  async isCorrectStartDateDisplayed(eventStartDateinAttendeeTimezone) {
    var days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    var d = new Date(eventStartDateinAttendeeTimezone);
    var dayName = days[d.getDay()];
    var currDate = d.getDate();
    await expect(await this.getHighlightedDay()).toHaveText(dayName);
    await expect(await this.getHighlightedDate()).toHaveText("" + currDate);
  }

  async getScheduleDatesComponents() {
    return this.scheduledates;
  }

  async getScheduleDaySessions(date) {
    let noOfScheduleDates = await this.scheduledates.count();
    // console.log(""+noOfScheduleDates);
    for (let index = 0; index < noOfScheduleDates; index++) {
      const element = this.scheduledates.nth(index);
      console.log(index);
      let ScheduleDate = (await element.allInnerTexts())
        .toString()
        .trim()
        .split(/\s+/)[1];
      // console.log(ScheduleDate.trim().split(/\s+/))
      if (ScheduleDate == date) {
        // console.log("Date found");
        element.click();
        let ScheduleDateTimings = await this.page
          .locator("div[class^='styles-module__segmentDayLine']")
          .textContent();
        let ScheduleDateTitle = await this.page
          .locator("div[class^='styles-module__segmentTitle']")
          .textContent();
        let ScheduleDateDescription = await this.page
          .locator("div[class^='styles-module__segmentDescription']")
          .textContent();
        // console.log(ScheduleDateTimings+" "+ScheduleDateTitle + " "+ScheduleDateDescription);
        return (
          ScheduleDateTimings +
          " " +
          ScheduleDateTitle +
          " " +
          ScheduleDateDescription
        );
      }
    }
    return "No Schedule Session on Specified date";
  }
}
