import { expect, Page } from "@playwright/test";
export class DateTimecounterComponent {
  readonly page: Page;
  constructor(page: Page) {
    this.page = page;
  }

  async isCorrectDatetimeDisplayed(
    eventStartDateinAttendeeTimezone,
    eventEndDateinAttendeeTimezone
  ) {
    let expectedStartDate = new Date(
      eventStartDateinAttendeeTimezone
    ).toString();
    console.log(expectedStartDate);
    // Wed May 17 2023 23:26:08 GMT+0530 (India Standard Time)
    // May 17, 2023, 4:51 PM - May 17, 2023, 10:51 PM (GMT+5:30)
    expectedStartDate =
      expectedStartDate.split(" ")[1] +
      " " +
      new Date(eventStartDateinAttendeeTimezone).getDate() +
      ", " +
      expectedStartDate.split(" ")[3] +
      ", " +
      new Date(eventStartDateinAttendeeTimezone).toLocaleTimeString('en', { timeStyle: "short", })
      ;
    console.log("expectedStartDate: ", expectedStartDate)
    let expectedEndDate = new Date(eventEndDateinAttendeeTimezone).toString();
    console.log(expectedEndDate);
    expectedEndDate =
      expectedEndDate.split(" ")[1] +
      " " +
      new Date(eventEndDateinAttendeeTimezone).getDate() +
      ", " +
      expectedEndDate.split(" ")[3] +
      ", " +
      new Date(eventEndDateinAttendeeTimezone).toLocaleTimeString('en', { timeStyle: "short", })
      ;
    console.log("expectedEndDate: ", expectedEndDate)
    let expectedDate = expectedStartDate + " - " + expectedEndDate;
    console.log("Expected Date and day:", expectedDate);
    await expect(
      this.page
        .locator('p[class^="styles-module__eventDate"]')
    ).toContainText(expectedDate);
  }
}
