import { expect, Locator, Page } from "@playwright/test";

export class FAQsComponent {
  //selectors
  readonly page: Page;
  readonly FAQs: Locator;
  readonly FAQsFields: Locator;
  readonly FAQsQuestion: Locator;
  readonly FAQsAnswer: Locator;

  //constructor
  constructor(page: Page) {
    this.page = page;
    this.FAQs = this.page.locator("id=landingPageFAQsSection");
    this.FAQsFields = this.page.locator(
      "div[class^='styles-module__faqContent']"
    );
    this.FAQsQuestion = this.page.locator(
      "div[class^=styles-module__question]"
    );
    this.FAQsAnswer = this.page.locator("div[class^=styles-module__answer]");
  }

  async getTotalNumberOfFAQs() {
    let countOfFields = await this.FAQsFields.count();
  }

  async getFAQsQuestionComponent() {
    return this.FAQsQuestion;
  }

  async getFAQsAnswerComponent() {
    return this.FAQsAnswer;
  }

  async isVisible() {
    await expect(this.FAQs).toBeVisible();
  }

  async isHidden() {
    await expect(this.FAQs).toBeHidden();
  }

  async isFAQsAnswervisible() {
    await expect(this.FAQsAnswer).toBeVisible();
  }

  async isFAQsAnswerHidden() {
    await expect(this.FAQsAnswer).toBeHidden();
  }

  async getFAQsQuestionText() {
    let TotalNumberOfFAQs = await this.FAQsFields.count();
    let FAQsQuestions = new Array();
    for (let index = 0; index < TotalNumberOfFAQs; index++) {
      const element = this.FAQsQuestion.nth(index);
      let FAQsQuestiontext = await (await element.allTextContents()).toString();
      // console.log("FAQs Qustion for index"+index+": "+FAQsQuestiontext);
      FAQsQuestions.push(FAQsQuestiontext);
    }
    return FAQsQuestions;
  }

  async getFAQsAnswerText() {
    let TotalNumberOfFAQs = await this.FAQsFields.count();
    let FAQsAnswers = new Array();
    for (let index = 0; index < TotalNumberOfFAQs; index++) {
      const element = this.FAQsQuestion.nth(index);
      element.click();
    }
    for (let index = 0; index < TotalNumberOfFAQs; index++) {
      const element = this.FAQsAnswer.nth(index);
      let FAQsAnswertext = await (await element.allTextContents()).toString();
      console.log("FAQs Qustion for index" + index + ": " + FAQsAnswertext);
      FAQsAnswers.push(FAQsAnswertext);
    }
    console.log(FAQsAnswers);
    return FAQsAnswers;
  }
}
