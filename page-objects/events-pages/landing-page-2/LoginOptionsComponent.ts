import { expect, Locator, Page } from "@playwright/test";
import { RegistrationFormComponent } from "./RegistrationFormComponent";
import { fetchOtpFromEmail } from "../../../util/emailUtil";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import EventRole from "../../../enums/eventRoleEnum";
import { ResetPasswordComponent } from "./ResetPassswordComponent";
import { RestrictedEventScreen } from "./RestrictedEventScreen";
import { RestrictedSpeakerAccessPage } from "./RestrictedSpeakerAccessPage";
import EventAuthType from "../../../enums/eventAuthTypeEnum";

export class LoginOptionsComponent {
  //selectors
  readonly page: Page;
  readonly emailInputBox: Locator; //common input box used in otp as well as in pwd flow as well
  readonly submitButton: Locator;
  readonly loginViaPasswordButton: Locator;
  readonly enterNowButton: Locator; // this comes as submit button on login via password screen on submitting email
  readonly enterPasswordBox: Locator; // input box to set password
  readonly continueButton: Locator;
  readonly otpInputBoxes: Locator;
  readonly forgotPasswordButton: Locator;
  readonly resetPasswordComponent: ResetPasswordComponent;
  readonly signInButton: Locator;
  readonly emailInputBoxForOTP: Locator; //email input box for otp
  readonly doneButton: Locator;
  //social login buttons
  readonly googleLoginButton: Locator;
  readonly linkedinLoginButton: Locator;
  readonly facebookLoginButton: Locator;
  readonly ssoLoginButton: Locator;

  //constructor
  constructor(page: Page) {
    this.page = page;
    this.emailInputBox = this.page.locator("input[name^='email']"); //header on this form with this text
    this.submitButton = this.page.locator("button:has-text('Submit')");
    this.loginViaPasswordButton = this.page.locator(
      "text=Log in with Password"
    );
    this.enterNowButton = this.page.locator("button:has-text('Enter Now')");
    this.enterPasswordBox = this.page.locator(
      "input[class^='styles-module__passwordField']"
    );
    this.continueButton = this.page.locator("button:has-text('Continue')");
    this.otpInputBoxes = this.page.locator(
      "input[class^='styles-module__otpInput']"
    );
    this.forgotPasswordButton = this.page.locator("text=Forgot your password?");
    this.resetPasswordComponent = new ResetPasswordComponent(this.page);
    this.signInButton = this.page.locator("button:has-text('Sign In')");
    this.emailInputBoxForOTP = this.page.locator("input[name='emailInput']");
    this.doneButton = this.page.locator("button:has-text('Done')");
    //social login button
    this.googleLoginButton = this.page.locator(
      "div[class^='styles-module__socialIconBtnContainer'] button"
    );
    this.facebookLoginButton = this.page
      .locator("div[class^='styles-module__socialIconBtnContainer'] button")
      .nth(1);
    this.linkedinLoginButton = this.page
      .locator("div[class^='styles-module__socialIconBtnContainer'] button")
      .nth(2);
    this.ssoLoginButton = this.page.locator(
      "div[class^='styles-module__socialIconSSO']"
    );
  }

  // async submitEmailForOTPVerification(email_add:string){
  //     await this.emailInputBox.fill(email_add)
  //     await this.submitButton.click()
  // }

  async fillEmailInOTPEmailInputBoxAndSubmit(emailToFill: string) {
    await this.emailInputBoxForOTP.fill(emailToFill);
    await this.continueButton.click();
  }

  async submitEmailForOtpVerification(
    userEventRoleDto: UserEventRoleDto,
    originPage = "attendeeLandingPage"
  ) {
    console.log(
      `handling OTP auth for ${userEventRoleDto.userInfoDto.userEmail}`
    );
    await this.emailInputBox.fill(userEventRoleDto.userInfoDto.userEmail);
    if (await this.signInButton.isVisible()) {
      await this.signInButton.click();
      await this.page.waitForTimeout(1000);
    }
    if (await this.submitButton.isVisible()) {
      await this.submitButton.click();
      await this.page.waitForTimeout(1000);
    }
    if (originPage == "attendeeLandingPage") {
      if (
        userEventRoleDto.eventInfoDto.eventEntryType ==
        EventEntryType.INVITE_ONLY
      ) {
        if (userEventRoleDto.userRoleInThisEvent == EventRole.NOTEXISTS) {
          // show invite only event screen
          return new RestrictedEventScreen(this.page);
        } else {
          let otp = await fetchOtpFromEmail(
            userEventRoleDto.userInfoDto.userEmail,
            userEventRoleDto.eventInfoDto.eventTitle
          );
          await this.enterAndSubmitOtp(otp);
        }
      } else {
        if (!userEventRoleDto.hasUserRegisteredToThisEvent) {
          return new RegistrationFormComponent(this.page);
        }
        let otp = await fetchOtpFromEmail(
          userEventRoleDto.userInfoDto.userEmail,
          userEventRoleDto.eventInfoDto.eventTitle
        );
        await this.enterAndSubmitOtp(otp);
      }
    } else {
      console.log("handing the otp authentication on speaker  landing page");
      if (
        userEventRoleDto.userRoleInThisEvent == EventRole.NOTEXISTS ||
        userEventRoleDto.userRoleForThisEvent == EventRole.ATTENDEE
      ) {
        return new RestrictedSpeakerAccessPage(this.page);
      } else {
        console.log(
          `User has an event role already ${userEventRoleDto.userRoleForThisEvent}`
        );
        let otp = await fetchOtpFromEmail(
          userEventRoleDto.userInfoDto.userEmail,
          userEventRoleDto.eventInfoDto.eventTitle
        );
        await this.enterAndSubmitOtp(otp);
      }
    }
  }

  async enterAndSubmitOtp(otp) {
    if (otp.length != 6) {
      throw new Error(`OTP length should be 6 but we got otp as ${otp}`);
    }
    let otpInputboxesLength = await this.otpInputBoxes.count();
    if (otpInputboxesLength != 6) {
      throw new Error(
        `OTP input box should be 6 but we got only ${otpInputboxesLength}`
      );
    }
    for (let i = 0; i < otpInputboxesLength; i++) {
      await this.otpInputBoxes.nth(i).fill(otp[i]);
    }
    //submit
    await this.continueButton.click();
  }

  async submitEmailForMagicLink(userEventRoleDto: UserEventRoleDto) {
    await this.emailInputBox.fill(userEventRoleDto.userInfoDto.userEmail);
    await this.submitButton.click();
    if (
      userEventRoleDto.eventInfoDto.eventEntryType == EventEntryType.INVITE_ONLY
    ) {
      if (userEventRoleDto.userRoleInThisEvent == EventRole.NOTEXISTS) {
        // show invite only event screen
        return new RestrictedEventScreen(this.page);
      }
    }
  }

  async submitEmailForPasswordLogin(
    userEventRoleDto: UserEventRoleDto,
    originPage = "attendeeLandingPage"
  ) {
    await this.emailInputBox.fill(userEventRoleDto.userInfoDto.userEmail);
    if (await this.signInButton.isVisible()) {
      await this.signInButton.click();
      await this.page.waitForTimeout(1000);
    }
    // if (await this.submitButton.isVisible()){
    //         await this.submitButton.click()
    //         await this.page.waitForTimeout(1000)
    //     }
    // now on based of different conditon, next component will be shown

    if (originPage == "attendeeLandingPage") {
      if (
        userEventRoleDto.eventInfoDto.eventEntryType == EventEntryType.REG_BASED
      ) {
        console.log("the event is reg based");
        if (userEventRoleDto.userRoleForThisEvent != EventRole.NOTEXISTS) {
          console.log("the event role exists for this user");
          console.log("checking if this user already have a password");
          if (userEventRoleDto.userInfoDto.hasSetPassword) {
            await this.enterPasswordBox.fill(
              userEventRoleDto.userInfoDto.userPassword
            );
            await this.continueButton.click();
          } else {
            if (await this.forgotPasswordButton.isVisible()) {
              await this.forgotPasswordButton.click();
              console.log("clicked on forgot password");
            }
            // click on submit button
            await this.submitButton.click();
            let otp = await fetchOtpFromEmail(
              userEventRoleDto.userInfoDto.userEmail,
              userEventRoleDto.eventInfoDto.eventTitle
            );
            // submit the otp
            await this.enterAndSubmitOtp(otp);
            // now the reset password screen should be visible
            await this.resetPasswordComponent.setPassword("Zuddl@123");
            userEventRoleDto.userInfoDto.userPassword = "Zuddl@123";
          }
        } else {
          console.log("the event role does not exists");

          if (userEventRoleDto.hasUserRegisteredToThisEvent == false) {
            console.log("user is not reg to the event already");
            //throw user back to the registration form
            await this.submitButton.click(); // for now its visible
            return new RegistrationFormComponent(this.page);
          } else {
            console.log("user is already reg to the event already");

            //check if account table entry exists
            if (userEventRoleDto.userInfoDto.hasEntryInAccountTable == false) {
              console.log("user does not have entry in account table");
              await this.submitButton.click(); // for now its visible
              let otp = await fetchOtpFromEmail(
                userEventRoleDto.userInfoDto.userEmail,
                userEventRoleDto.eventInfoDto.eventTitle
              );
              //trigger OTP flow
              await this.enterAndSubmitOtp(otp);
              // set password screen should appear - TO DO by dev, for now it just enters
              // await this.resetPasswordComponent.setPassword("Zuddl@123")
              // userEventRoleDto.userInfoDto.userPassword = "Zuddl@123"
            } else {
              console.log(
                "user has entry in account table so showing password screen"
              );
              //check if user has a set password TODO -> next iteration

              if (userEventRoleDto.userInfoDto.hasSetPassword == false) {
                console.log(
                  "user does not have a set password hence going through password forgot flow"
                );
                //trigger forgot password button to set password
                if (await this.forgotPasswordButton.isVisible()) {
                  await this.forgotPasswordButton.click();
                  console.log("clicked on forgot password");
                }
                // click on submit button
                await this.submitButton.click();
                let otp = await fetchOtpFromEmail(
                  userEventRoleDto.userInfoDto.userEmail,
                  userEventRoleDto.eventInfoDto.eventTitle
                );
                // submit the otp
                await this.enterAndSubmitOtp(otp);
                // now the reset password screen should be visible
                await this.resetPasswordComponent.setPassword("Zuddl@123");
                userEventRoleDto.userInfoDto.userPassword = "Zuddl@123";
              } else {
                console.log(
                  "user already have a set password hence showing enter password screen"
                );
                // show password screen
                await this.enterPasswordBox.fill(
                  userEventRoleDto.userInfoDto.userPassword
                );
                await this.continueButton.click();
              }
            }
          }
        }
      } else if (
        userEventRoleDto.eventInfoDto.eventEntryType ==
        EventEntryType.INVITE_ONLY
      ) {
        console.log("Handling submit with password flow for invite only event");
        if (userEventRoleDto.userRoleInThisEvent == EventRole.NOTEXISTS) {
          //return the restrictedEvent screen
          return new RestrictedEventScreen(this.page);
        } else {
          console.log("case where user event role exists for this event");
          //show enter password screen
          if (!userEventRoleDto.userInfoDto.hasSetPassword) {
            console.log("case where user have never set his password before");
            if (await this.forgotPasswordButton.isVisible()) {
              await this.forgotPasswordButton.click();
              console.log("clicked on forgot password");
            }
            // click on submit button
            await this.submitButton.click();
            let otp = await fetchOtpFromEmail(
              userEventRoleDto.userInfoDto.userEmail,
              userEventRoleDto.eventInfoDto.eventTitle
            );
            // submit the otp
            if (otp == null || otp == undefined) {
              throw Error("No OTP could be recived when checking email inbox");
            }
            await this.enterAndSubmitOtp(otp);
            // now the reset password screen should be visible
            await this.resetPasswordComponent.setPassword("Zuddl@123");
            userEventRoleDto.userInfoDto.userPassword = "Zuddl@123";
          } else {
            console.log("case where user already have set his password before");
            await this.enterPasswordBox.fill(
              userEventRoleDto.userInfoDto.userPassword
            );
            await this.continueButton.click();
          }
        }
      }
    } else {
      console.log("handing the password flow for speaker landing page");
      let restrictedSpeakerAccessPage = new RestrictedSpeakerAccessPage(
        this.page
      );

      if (
        userEventRoleDto.userRoleInThisEvent == EventRole.NOTEXISTS ||
        userEventRoleDto.userRoleForThisEvent == EventRole.ATTENDEE
      ) {
        await expect(
          await restrictedSpeakerAccessPage.noAccessMessage(),
          "Restricted entry only for speakers page is visible"
        ).toBeVisible();
        return restrictedSpeakerAccessPage;
      } else {
        let accessAvailable = false;
        while (!accessAvailable) {
          try {
            await expect(
              await restrictedSpeakerAccessPage.noAccessMessage(),
              "Restricted entry only for speakers page is not visible"
            ).not.toBeVisible();
            accessAvailable = true;
          } catch (error) {
            console.log(
              "Restricted entry only for speakers page is visible. Refreshing the page"
            );
            await this.page.reload();
            await this.page.waitForTimeout(2000);
            await this.emailInputBox.fill(
              userEventRoleDto.userInfoDto.userEmail
            );
            if (await this.signInButton.isVisible()) {
              await this.signInButton.click();
              await this.page.waitForTimeout(1000);
            }
          }
        }
        if (userEventRoleDto.userInfoDto.hasSetPassword) {
          await this.enterPasswordBox.fill(
            userEventRoleDto.userInfoDto.userPassword
          );
          await this.continueButton.click();
        } else {
          if (await this.forgotPasswordButton.isVisible()) {
            await this.forgotPasswordButton.click();
            console.log("clicked on forgot password");
          }
          // click on submit button
          await this.continueButton.click();
          let otp = await fetchOtpFromEmail(
            userEventRoleDto.userInfoDto.userEmail,
            userEventRoleDto.eventInfoDto.eventTitle
          );
          // submit the otp
          await this.enterAndSubmitOtp(otp);
          // now the reset password screen should be visible
          await this.resetPasswordComponent.setPassword("Zuddl@123");
          userEventRoleDto.userInfoDto.userPassword = "Zuddl@123";
          await this.doneButton.click();
        }
      }
    }
  }

  async clickOnLoginOption(loginOption: EventAuthType) {
    console.log(`clicking on auth option ${loginOption}`);
    if (loginOption == EventAuthType.SSO) {
      await this.ssoLoginButton.click();
    } else {
      await this.googleLoginButton.click();
    }
  }
}

// user point of view
