import { expect, Locator, Page } from "@playwright/test";

export class BoothSectionComponent {
  //selectors
  readonly page: Page;
  readonly BoothSection: Locator;
  readonly BoothFields: Locator;

  //constructor
  constructor(page: Page) {
    this.page = page;
    this.BoothSection = this.page.locator("id=landingPageBoothsSection");
    this.BoothFields = this.page.locator(
      "div[class^='styles-module__boothCard']"
    );
  }

  async getTotalNumberOfBooths() {
    let countOfSponsores = await this.BoothFields.count();
    return countOfSponsores;
  }

  async isVisible() {
    await expect(this.BoothSection).toBeVisible();
  }

  async isHidden() {
    await expect(this.BoothSection).toBeHidden();
  }
}
