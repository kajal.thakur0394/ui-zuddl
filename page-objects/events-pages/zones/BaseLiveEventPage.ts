import { Page } from "@playwright/test";
import { AnnouncementForm } from "../live-side-components/AnnouncementForm";
import { PublishedAnnouncementComponent } from "../live-side-components/PublishedAnnouncement";
import { InteractionPanel } from "../live-side-components/InteractionPanel";
import { TopNavBarComponent } from "../live-side-components/TopNavBarComponent";

export class BaseLiveEventPage {
  //all commom components we will initialise in the base event page
  readonly page: Page;
  readonly announcementFormComponent: AnnouncementForm;
  readonly publishedAnnouncementComponent: PublishedAnnouncementComponent;
  readonly interactionPanelComponent: InteractionPanel;
  readonly topNavbarComponent: TopNavBarComponent;

  constructor(page: Page) {
    this.page = page;
    this.announcementFormComponent = new AnnouncementForm(page);
    this.publishedAnnouncementComponent = new PublishedAnnouncementComponent(
      page
    );
    this.interactionPanelComponent = new InteractionPanel(page);
    this.topNavbarComponent = new TopNavBarComponent(this.page);
  }

  //getters
  get getAnnouncementFormComponent() {
    //to be used only by organiser or moderator
    return this.announcementFormComponent;
  }

  get getPublishedAnnouncementComponent() {
    //to be used only by organiser or moderator
    return this.publishedAnnouncementComponent;
  }

  get getInteractionPanelComponent() {
    //to be used only by organiser or moderator
    return this.interactionPanelComponent;
  }

  get getTopNavBarComponent() {
    return this.topNavbarComponent;
  }
}
