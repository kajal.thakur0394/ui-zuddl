import { Locator, Page, expect } from "@playwright/test";
import { InteractionPanel } from "../live-side-components/InteractionPanel";
import { PublishedAnnouncementComponent } from "../live-side-components/PublishedAnnouncement";

export class SchedulePage extends InteractionPanel {
  readonly emptyStateScheduleHeading: Locator;
  readonly noScheduleOutMessage: Locator;
  readonly scheduleTopButton: Locator;
  readonly completedButton: Locator;
  readonly backstageButton: Locator;
  readonly joinButton: Locator;
  readonly delayedButton: Locator;
  readonly profileCompletionPopupSkipButton: Locator;
  readonly remindMeButton: Locator;
  readonly markFavouriteIcon: Locator;
  readonly startTime: Locator;
  readonly endTime: Locator;
  readonly subVenue: Locator;
  readonly dateElement: Locator;
  readonly scheduleInfo: Locator;
  readonly scheduleName: Locator;
  readonly scheduleDescription: Locator;
  readonly speakerList: Locator;
  readonly speakerIcon: Locator;
  readonly sessionTagsContainer: Locator;
  readonly liveEventContainer: Locator;

  venue: Locator;

  constructor(page: Page) {
    super(page);
    this.emptyStateScheduleHeading = this.page.locator(
      "div[class^='styles-module__headingElement'] h2"
    );
    this.noScheduleOutMessage = this.page
      .locator("div[class^='styles-module__headingWithUnderline'] h6")
      .first();

    this.scheduleTopButton = this.page.getByTestId("navbar-menu-schedule");
    this.completedButton = this.page.getByRole("button", { name: "Completed" });
    this.backstageButton = this.page.getByRole("button", { name: "Backstage" });
    this.joinButton = this.page.getByRole("button", { name: "Join" });
    this.delayedButton = this.page.getByRole("button", { name: "Delayed" });
    this.profileCompletionPopupSkipButton = this.page.getByRole("button", {
      name: "Skip",
    });
    this.remindMeButton = this.page
      .getByTestId("schedule_remind_me")
      .locator("a");
    this.markFavouriteIcon = this.page.getByTestId("make_favourite").first();
    this.startTime = this.page
      .locator("span[class^='styles-module__start']")
      .first();
    this.endTime = this.page
      .locator("span[class^='styles-module__end']")
      .first();
    this.subVenue = this.page
      .locator("div[class^='styles-module__bottomInfo']")
      .first();
    this.dateElement = this.page
      .locator("p[class^='styles-module__dateInfo']")
      .first();
    this.scheduleInfo = this.page
      .locator("div[class^='styles-module__scheduleInfo']")
      .first();
    this.scheduleName = this.scheduleInfo.locator("h2");
    this.scheduleDescription = this.scheduleInfo.locator("p");
    this.speakerList = this.page
      .locator("div[class^='styles-module__speakerList']")
      .first();
    this.speakerIcon = this.page.locator("#schedule-stage-speaker div").first();
    this.sessionTagsContainer = this.page
      .locator("section[class^='styles-module__tagsViewContainer']")
      .first();
    this.liveEventContainer = this.page.locator(
      "div[class*='liveEventInnerLayoutBody']"
    );
  }

  async getPublishedAnnouncementComponent() {
    return new PublishedAnnouncementComponent(this.page);
  }

  async verifyTopScheduleButtonIsVisible() {
    await this.page.waitForTimeout(5000); // implement a better wait
    await expect(this.scheduleTopButton).toBeVisible({ timeout: 10000 });
  }

  async waitForPageToLoad() {
    await expect(this.liveEventContainer).toBeVisible({ timeout: 30000 });
  }

  async verifyCompletedButtonIsVisible() {
    await expect(this.completedButton).toBeVisible({ timeout: 10000 });
  }

  async verifyBackstageButtonIsVisible() {
    await expect(this.backstageButton).toBeVisible({ timeout: 10000 });
  }

  async verifyJoinButtonIsVisible() {
    await expect(this.joinButton).toBeVisible({ timeout: 10000 });
  }

  async verifyDelayedStatusIsVisible() {
    await expect(this.delayedButton).toBeVisible({ timeout: 10000 });
  }

  async verifyRemindMeButtonVisile() {
    await expect(this.remindMeButton).toBeVisible({ timeout: 10000 });
  }
  async skipProfileCompletionPopup() {
    try {
      await this.profileCompletionPopupSkipButton.click();
    } catch (error) {
      console.log("Skip button not visible.");
    }
  }

  async verifyNoScheduleOutMessageIsVisible() {
    await expect(this.noScheduleOutMessage).toBeVisible({ timeout: 10000 });
  }

  async verifySessionPresenceById(sessionId: any) {
    let sessionLocator = this.page.locator(
      `div[id*='event-${sessionId}'] button`
    );

    await expect(sessionLocator).toBeVisible({ timeout: 10000 });
  }

  async verifySessionPresenceByName(sessionName: any) {
    let sessionLocator = this.page.getByRole("heading", {
      name: sessionName,
    });
    await expect(sessionLocator).toBeVisible({ timeout: 30000 });
  }

  async verifySessionAbsence(sessionId: any) {
    let sessionLocator = this.page.locator(
      `div[id*='event-${sessionId}'] button`
    );

    await expect(sessionLocator).not.toBeVisible({ timeout: 10000 });
  }

  async clickOnSessionBackstageButton(sessionId: any) {
    let backstageButton = this.page
      .locator(`div[id*='event-${sessionId}'] button`)
      .filter({ hasText: "Backstage" });

    try {
      await this.verifySessionPresenceById(sessionId);
    } catch (error) {
      this.page.reload();
      await this.verifySessionPresenceById(sessionId);
    }

    await backstageButton.click();
  }
  async validateVenue(venue: string) {
    if (venue === "NONE") {
      return;
    }
    venue = venue.replace(/_/g, "-");
    const venueRegex = new RegExp(venue, "i");
    this.venue = this.page
      .getByRole("tabpanel", { name: "Sessions" })
      .getByText(venueRegex);
    await expect(this.venue).toBeVisible({ timeout: 10000 });
  }

  async validateSubVenueName(venue: string, subVenue: string) {
    this.validateVenue(venue);
    await expect(this.subVenue).toBeVisible({ timeout: 10000 });
    await expect(this.subVenue).toHaveText(new RegExp(subVenue, "i"));
  }

  async validateFavouriteIconisVisble() {
    await expect(this.markFavouriteIcon).toBeVisible({ timeout: 10000 });
  }

  async validateTime(payloadStartTime, payloadEndTime) {
    const startTimeText = await this.startTime.innerText();
    const endTimeText = await this.endTime.innerText();
    console.log(`startTimeText: ${startTimeText}`);
    console.log(`endTimeText: ${endTimeText}`);
    console.log(`payloadStartTime: ${payloadStartTime}`);
    console.log(`payloadEndTime: ${payloadEndTime}`);
    const dateText = await this.dateElement.innerText();
    console.log(`dateText: ${dateText}`);
    const payloadStartDateTime = new Date(payloadStartTime);
    const payloadEndDateTime = new Date(payloadEndTime);
    await expect(
      this.compareTime(startTimeText, payloadStartTime)
    ).toBeTruthy();
    await expect(this.compareTime(endTimeText, payloadEndTime)).toBeTruthy();
  }

  async validateScheduleName(payloadScheduleName) {
    const scheduleNameText = await this.scheduleName.innerText();
    expect(scheduleNameText).toEqual(payloadScheduleName);
  }

  async validateScheduleDescription(payloadScheduleDescription) {
    const scheduleDescriptionText = await this.scheduleDescription.innerText();
    expect(scheduleDescriptionText).toEqual(payloadScheduleDescription);
  }

  async compareTime(timeText, payloadTime) {
    const time = timeText.split(" ")[0];
    const AMPM = timeText.split(" ")[1];
    const timeParts = time.split(":");
    const payloadTimeParts = payloadTime.split(":");
    let timeHours = parseInt(timeParts[0]);
    if (AMPM === "PM") {
      timeHours += 12;
    }
    const timeMinutes = parseInt(timeParts[1]);
    const payloadTimeHours = parseInt(payloadTimeParts[0]);
    const payloadTimeMinutes = parseInt(payloadTimeParts[1]);
    if (timeHours === payloadTimeHours) {
      return timeMinutes === payloadTimeMinutes;
    }
    return false;
  }

  async compareDate(dateText, payloadDate) {
    // dateText = "Tue, 8th Aug"
    // payloadDate is object of type Date
    const dateParts = dateText.split(" ");
    const date = parseInt(dateParts[1].replace(/[^0-9]/g, ""));
    const month = dateParts[2];
    const payloadDateDate = payloadDate.getDate();
    const payloadDateMonth = payloadDate.toLocaleString("default", {
      month: "short",
    });
    if (date === payloadDateDate) {
      return month === payloadDateMonth;
    }
    return false;
  }

  async verifySpeakerDetails(SpeakerDetails: {
    speakerId: string;
    speakerFirstName: string;
    speakerLastName: string;
    speakerDesignation?: string;
    speakerBio?: string;
    speakerCompany?: string;
  }) {
    const speakerName =
      SpeakerDetails.speakerFirstName + " " + SpeakerDetails.speakerLastName;
    let headline = "";
    if (SpeakerDetails.speakerDesignation) {
      headline.concat(SpeakerDetails.speakerDesignation);
      if (SpeakerDetails.speakerCompany) {
        headline.concat(", " + SpeakerDetails.speakerCompany);
      }
    }
    if (SpeakerDetails.speakerCompany) {
      headline.concat(SpeakerDetails.speakerCompany);
    }
    console.log("headline: " + headline);
    console.log("speakerName: " + speakerName);
    console.log("speakerBio: " + SpeakerDetails.speakerBio);
    console.log("speakerCompany: " + SpeakerDetails.speakerCompany);

    // check if icon is visible
    await expect(this.speakerIcon).toBeVisible({ timeout: 10000 });
    await this.speakerIcon.click();

    const speaker = this.speakerList
      .locator("p[class^='styles-module__name']")
      .filter({ hasText: speakerName });

    await expect(speaker).toBeVisible({ timeout: 10000 });

    if (headline) {
      const speakerHeadline = this.speakerList
        .locator("p[class^='styles-module__headline']")
        .filter({ hasText: headline });
      await expect(speakerHeadline).toBeVisible({ timeout: 10000 });
    }

    if (SpeakerDetails.speakerBio) {
      const speakerBio = this.speakerList
        .locator("p[class^='styles-module__bio']")
        .filter({ hasText: SpeakerDetails.speakerBio });
      await expect(speakerBio).toBeVisible({ timeout: 10000 });
    }
  }

  async verifyTag(tagName: string) {
    const exactTagRegex = new RegExp(`^${tagName}$`);
    let tagElement = this.page
      .locator("div[class^='styles-module__tagButton']")
      .filter({ hasText: exactTagRegex });
    await expect(tagElement).toBeVisible({ timeout: 10000 });
    return tagElement;
  }

  async verifyTags(tags: string[]) {
    for (const tag of tags) {
      await this.verifyTag(tag);
    }
  }

  async selectTag(tag: string) {
    let tagElement = await this.verifyTag(tag);
    await tagElement.click();
  }

  async unselectActiveTag(tagName: string) {
    let activeTag = this.page
      .locator("div[class^='styles-module__tagActiveButton']")
      .filter({ hasText: tagName });
    await expect(activeTag).toBeVisible({ timeout: 10000 });
    await activeTag.click();
  }
}
