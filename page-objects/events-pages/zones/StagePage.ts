import { expect, Locator, Page } from "@playwright/test";
import { AVModal } from "../live-side-components/AvModal";
import { GreenRoom } from "../site-modules/GreenRoom";
import { MainStageContainer } from "../site-modules/MainStageContainer";
import { InteractionPanel } from "../live-side-components/InteractionPanel";
import { PublishedAnnouncementComponent } from "../live-side-components/PublishedAnnouncement";
import { AVStream } from "../live-side-components/AVStream";

export class StagePage extends InteractionPanel {
  readonly emptyStageHeading: Locator;
  readonly volumeToggleButtonOnBottomBar: Locator;
  readonly joinBackStageModal: Locator;
  readonly goBackStageButtonOnStageJoinModal: Locator;
  readonly viewAsAttendeeButtonOnStageJoinModal: Locator;
  readonly greenRoomContainer: GreenRoom;
  readonly mainStageContainer: MainStageContainer;
  readonly emojiContainer: Locator;
  readonly emojis: Locator;
  readonly timerContainer: Locator;
  readonly closeStageModel: Locator;
  readonly joinBackstageButton: Locator;
  readonly exitRoom: Locator;
  readonly avmodal: AVModal;

  //..........//
  //Components//
  //..........//
  readonly avStreamComponent: AVStream;

  constructor(page: Page) {
    super(page);
    this.emptyStageHeading = this.page.locator(
      "div[class^='styles-module__emptyStateWrappe'] h2"
    );
    this.volumeToggleButtonOnBottomBar = this.page.locator(
      "#volume-toggle-button"
    );
    this.goBackStageButtonOnStageJoinModal = this.page.locator(
      "button[data-testid='go_backstage']"
    );
    this.viewAsAttendeeButtonOnStageJoinModal = this.page.locator(
      "button[data-testid='view_as_attendee']"
    );
    this.greenRoomContainer = new GreenRoom(this.page);
    this.mainStageContainer = new MainStageContainer(this.page);
    this.emojiContainer = this.page.getByLabel("Emoji Reactions");
    this.emojis = this.page.locator("span[class^='styles-module__emoji']");
    this.timerContainer = this.page.locator(
      "//div[contains(@class,'timerTextContainer')]"
    );
    this.closeStageModel = this.page.locator(
      "button[class^='styles-module__modalClose___']"
    );
    this.exitRoom = this.page.locator("label").filter({ hasText: "Exit" });
    this.joinBackstageButton = this.page.getByRole("button", {
      name: "Join backstage",
    });

    ///.................///
    //....Components....//
    ///.................///
    this.avStreamComponent = new AVStream(page);
  }

  get getAvComponent() {
    return this.avStreamComponent;
  }

  async clickOnJoinBackStageButtonOnJoinStageModal() {
    await this.goBackStageButtonOnStageJoinModal.click();
    return new AVModal(this.page);
  }

  async clickOnViewAsAttendeeOnJoinStageModal() {
    await this.viewAsAttendeeButtonOnStageJoinModal.click();
    return new AVModal(this.page);
  }

  async verifyEmojiContainerIsVisible() {
    const emojiMenuLocator = this.page.locator(
      "div[class^='styles-module__emojiMenu']"
    );
    const emojiContainerLocator = this.emojiContainer;
    await this.page.waitForTimeout(2000);

    if (
      (await emojiMenuLocator.isVisible()) ||
      (await emojiContainerLocator.isVisible())
    ) {
      console.log("Emoji container is visible");
    } else {
      throw new Error("Emoji container is not visible");
    }
  }

  async verifyEmojisAreVisible() {
    // await expect(this.emojiContainer).toBeVisible({ timeout: 240000 });
    await expect(this.emojis).toHaveCount(5);
  }

  async verifyEmojisAreNotVisible() {
    await expect(this.emojiContainer).not.toBeVisible({ timeout: 10000 });
  }

  async closeWelcomeStagePopUp() {
    await this.closeStageModel.click();
    await expect(this.closeStageModel).toBeHidden();
  }

  async verifyTimerVisibility(shouldBePresent: boolean = true) {
    if (shouldBePresent) {
      await expect(this.timerContainer).toBeVisible({ timeout: 5000 });
    } else {
      await expect(this.timerContainer).not.toBeVisible({ timeout: 5000 });
    }
  }

  async verifyJoinBackstageButtonVisibility() {
    await expect(this.joinBackstageButton).toBeVisible({ timeout: 5000 });
  }

  get getGreenRoomContainer() {
    return this.greenRoomContainer;
  }

  get getMainStageContainer() {
    return this.mainStageContainer;
  }

  get getAVModal() {
    return new AVModal(this.page);
  }

  async exitRoomz() {
    await expect(this.exitRoom).toBeVisible();
    await this.exitRoom.click();
  }

  async getPublishedAnnouncementComponent() {
    return new PublishedAnnouncementComponent(this.page);
  }
}
