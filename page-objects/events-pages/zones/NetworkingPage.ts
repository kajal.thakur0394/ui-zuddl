import { Locator, Page } from "@playwright/test";
import { InteractionPanel } from "../live-side-components/InteractionPanel";
import { PublishedAnnouncementComponent } from "../live-side-components/PublishedAnnouncement";

export class NetworkingPage extends InteractionPanel {
  readonly recommendedProfileContainer: Locator;
  readonly startNetworkingButton: Locator;
  readonly browseAllAttendeeSection: Locator;
  readonly yourConnectionsSection: Locator;
  constructor(page: Page) {
    super(page);
    this.startNetworkingButton = this.page.locator(
      "button:has-text('Start Networking')"
    );
    this.recommendedProfileContainer = this.page.locator(
      "div[class^='styles-module__orderOneWrapper']:has-text('Recommended Profiles')"
    );
    this.browseAllAttendeeSection = this.page.locator(
      "div[class^='styles-module__orderTwoWrapper']:has-text('All Attendees')"
    );
    this.yourConnectionsSection = this.page.locator(
      "div[class^='styles-module__orderThreeWrapper']:has-text('Your Connections')"
    );
  }

  async getPublishedAnnouncementComponent() {
    return new PublishedAnnouncementComponent(this.page);
  }
}
