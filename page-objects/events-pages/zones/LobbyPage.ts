import { expect, Locator, Page, errors, test } from "@playwright/test";
import { InteractionPanel } from "../live-side-components/InteractionPanel";
import { PublishedAnnouncementComponent } from "../live-side-components/PublishedAnnouncement";
import { TopNavBarComponent } from "../live-side-components/TopNavBarComponent";

export class LobbyPage extends InteractionPanel {
  readonly lobbyButton: Locator;
  readonly skipButton: Locator;
  readonly letsGoButton: Locator;
  readonly nextButton: Locator;

  constructor(page: Page) {
    super(page);
    this.skipButton = this.page.locator("button:has-text('Skip')");
    this.letsGoButton = this.page.locator("text=Let's go");
    this.nextButton = this.page.locator("button:has-text('Next')");
    this.lobbyButton = this.page
      .locator("a[class^='styles-module__option']")
      .filter({ hasText: "LOBBY" });
  }

  // actions
  async handleEditProfilePrompt(toSkip = true) {
    await test.step(`Handling edit profile prompt`, async () => {
      if (toSkip) {
        await test.step(`Clicking on skip button on edit profile prompt`, async () => {
          await this.skipButton.click({ timeout: 15000 });
        });
      } else {
        await test.step(`Clicking on lets go button on edit profile prompt`, async () => {
          await this.letsGoButton.click();
        });
      }
    });
  }

  // actions
  async handleTourPrompt(toSkip = true) {
    const skipButton = this.page.locator("a:has-text('Skip')");
    await test.step(`Handling zone tour prompt`, async () => {
      if (toSkip) {
        await test.step(`Clicking on skip button on tour prompt`, async () => {
          try {
            await skipButton.click({ timeout: 10000 });
          } catch (error) {
            if (error instanceof errors.TimeoutError) {
              console.log("skip button never appeared");
            }
          }
        });
      } else {
        await test.step(`Clicking on next button on tour prompt`, async () => {
          await this.nextButton.click();
        });
      }
    });
  }

  // Verify lobby widgets

  // Verify image widget
  async verifyImageWidget(imageAlt: string, urlRedirectedTo: string) {
    let imageWidgetLocator = this.page.getByAltText(imageAlt).nth(1);
    // Verify redirected webpage is same as urlRedirected
    var pagePromise = this.page
      .context()
      .waitForEvent("page", (p) => p.url() == urlRedirectedTo);
    await imageWidgetLocator.click();
    const newPage = await pagePromise;
    console.log("Redirected page url is", newPage.url());
    expect(newPage.url(), `Redirected url to be ${urlRedirectedTo}`).toBe(
      urlRedirectedTo
    );
    await newPage.close();
  }

  // Verify video widget
  async verifyVideoWidget() {
    //styles-module__videoContainer___pJlTK
    let videoWidgetLocator: Locator = this.page.locator(
      "div[class^='styles-module__videoContainer']"
    );
    // Verify video widget is visible
    await expect(
      videoWidgetLocator,
      "Expect video widget locator to be visible"
    ).toBeVisible({ timeout: 60000 });
  }

  // Verify Internal image carousel widget
  async verifyInternalImageCarouselWidget(
    urlRedirectedTo: string | RegExp,
    index: number
  ) {
    let imageCarouseloWidgetLocator: Locator = this.page
      .locator("div[class^='styles-module__imageCarouselWidgetContainer']")
      .nth(index);
    // Verify image carousel widget is visible
    await expect(
      imageCarouseloWidgetLocator,
      "Expect image widget locator to be visible"
    ).toBeVisible();
    await imageCarouseloWidgetLocator.click();
    expect(this.page.url(), `Redirected url to be ${urlRedirectedTo}`).toBe(
      urlRedirectedTo
    );
    // Getting back to lobby
    await this.lobbyButton.click();
    await this.page.waitForLoadState();
  }

  // Verify External image carousel widget
  async verifyExternalImageCarouselWidget(
    index: number,
    urlRedirectedTo: string
  ) {
    let imageCarouseloWidgetLocator: Locator = this.page
      .locator("div[class^='styles-module__imageCarouselWidgetContainer']")
      .nth(index);
    // Verify image carousel widget is visible
    await expect(
      imageCarouseloWidgetLocator,
      "Expect image widget locator to be visible"
    ).toBeVisible();
    // Verify redirected webpage is same as urlRedirected
    var pagePromise = this.page
      .context()
      .waitForEvent("page", (p) => p.url() == urlRedirectedTo);
    await imageCarouseloWidgetLocator.click();
    const newPage = await pagePromise;
    console.log("Redirected page url is", newPage.url());
    expect(newPage.url(), `Redirected url to be ${urlRedirectedTo}`).toBe(
      urlRedirectedTo
    );
    await newPage.close();
  }

  // Verify Ticker widget and its content
  async verifyTickerWidget(tickercontent: string) {
    let tickerWidgetLocator: Locator = this.page.locator(
      "div[class^='styles-module__tickerContainer___1eiAg']"
    );
    // Verify ticker widget is visible
    await expect(tickerWidgetLocator, "Ticker widget is visible").toBeVisible();
    // Verify the content of ticker widget
    let tickerWidgetLocatorContent = await tickerWidgetLocator.textContent();
    console.log("tickerWidgetLocatorContent: ", tickerWidgetLocatorContent);
    expect(tickerWidgetLocatorContent, "Verifying ticker widget content").toBe(
      tickercontent
    );
  }

  // Verify Go to widget
  async verifyGoToWidget(urlRedirectedTo: string | RegExp) {
    let goToWidgetLocator: Locator = this.page.locator(
      "div[class^='widget widget-BUTTON']"
    );
    // Verify image carousel widget is visible
    await expect(
      goToWidgetLocator,
      "Expect go to widget locator to be visible"
    ).toBeVisible();
    await goToWidgetLocator.click();
    expect(this.page.url(), `Redirected url to be ${urlRedirectedTo}`).toBe(
      urlRedirectedTo
    );
    // Getting back to lobby
    await this.lobbyButton.click();
    await this.page.waitForLoadState();
  }

  // Verify Hotspot widget
  async verifyHotspotWidget(urlRedirectedTo: string | RegExp) {
    let hotspotLocator: Locator = this.page.locator(
      "div[class^='widget widget-HOTSPOT']"
    );
    // Verify image carousel widget is visible
    await expect(
      hotspotLocator,
      "Expect hotspot locator to be visible"
    ).toBeVisible();
    await hotspotLocator.click();
    expect(this.page.url(), `Redirected url to be ${urlRedirectedTo}`).toBe(
      urlRedirectedTo
    );
    // Getting back to lobby
    await this.lobbyButton.click();
    await this.page.waitForLoadState();
  }

  // Verify Iframe widget
  async verifyIframeWidget(srcIframe: string) {
    let iframeLocator: Locator = this.page.locator(
      "div[class^='widget widget-IFRAME']"
    );
    // Verify image carousel widget is visible
    await expect(
      iframeLocator,
      "Expect hotspot locator to be visible"
    ).toBeVisible();
    // Verifying source of iframe locator
    let iframeSrc = await iframeLocator.locator("iframe").getAttribute("src");
    console.log("iframeSrc: ", iframeSrc);
    expect(iframeSrc, "Verifying iframe src to be same as passed").toBe(
      srcIframe
    );
  }

  // Verify File download widget
  async verifyFileDownloadWidget(redirectedHref: string | RegExp) {
    let fileDownloadWidgetLocator: Locator = this.page
      .locator("div[class^='widget widget-FILE']")
      .first();
    // Verify Text widget is visible
    await expect(
      fileDownloadWidgetLocator,
      "Expect File widget locator to be visible"
    ).toBeVisible();
    // Verifying href of  locator
    let fileDownloadWidgetHref = await fileDownloadWidgetLocator
      .locator("a")
      .getAttribute("href");
    console.log("iframeSrc: ", fileDownloadWidgetHref);
    expect(
      fileDownloadWidgetHref,
      "Verifying iframe src to be same as passed"
    ).toBe(redirectedHref);
  }

  async reload() {
    await this.page.reload();
  }

  async getPublishedAnnouncementComponent() {
    return new PublishedAnnouncementComponent(this.page);
  }

  async getTopNavBarComponent() {
    return new TopNavBarComponent(this.page);
  }
}
