import { Locator, Page, expect } from "@playwright/test";
import { InteractionPanel } from "../live-side-components/InteractionPanel";
import { PublishedAnnouncementComponent } from "../live-side-components/PublishedAnnouncement";

export class RoomsListingPage extends InteractionPanel {
  readonly emptyStateRoomsHeading: Locator;
  readonly noRoomsOpenMessage: Locator;
  readonly roomsLocator: Locator;
  readonly enterRoomButtonLocator: Locator;
  readonly joinRoomLocator: Locator;
  readonly exitRoomLocator: Locator;

  constructor(page: Page) {
    super(page);
    this.emptyStateRoomsHeading = this.page.locator(
      "div[class^='styles-module__headingElement'] h2"
    );
    this.noRoomsOpenMessage = this.page
      .locator("div[class^='styles-module__headingWithUnderline'] h6")
      .first();

    this.roomsLocator = this.page.locator(
      "div[class^='styles-module__discussionTableCutOutContainer']"
    );
    this.enterRoomButtonLocator = this.page.locator("button");
    this.joinRoomLocator = this.page.locator("div[data-testid='Join Room']");
    this.exitRoomLocator = this.page.locator("button[data-testid='exit_room']");
  }
  async getPublishedAnnouncementComponent() {
    return new PublishedAnnouncementComponent(this.page);
  }

  async enterIntoRoom() {
    await expect(
      this.enterRoomButtonLocator.filter({ hasText: "Enter" })
    ).toBeVisible();
    await this.enterRoomButtonLocator.filter({ hasText: "Enter" }).click();
  }

  async joinRoom() {
    await expect(this.joinRoomLocator).toBeVisible();
    await this.joinRoomLocator.click();
  }

  async exitTheRoom() {
    await this.exitRoomLocator.click();
    let exitRoom1Locator = this.page
      .locator("button[class^='styles-module__'] span")
      .filter({ hasText: "Leave" });

    let exitRoom2Locator = this.page
      .locator("button[class^='styles-module__'] span")
      .filter({ hasText: "Exit" });

    if ((await exitRoom1Locator.count()) > 0) {
      await exitRoom1Locator.click();
    } else if ((await exitRoom2Locator.count()) > 0) {
      await exitRoom2Locator.nth(1).click();
    }
  }
}
