import { test, expect, Locator, Page } from "@playwright/test";
import { PublishedAnnouncementComponent } from "../live-side-components/PublishedAnnouncement";
import { InteractionPanel } from "../live-side-components/InteractionPanel";

export class ExpoPage extends InteractionPanel {
  readonly expoButton: Locator;
  readonly emptyStateExpoHeading: Locator;
  readonly noExpoOpenMessage: Locator;
  readonly expoTitle: Locator;
  readonly enterZoneButton: Locator;
  readonly booth: Locator;
  readonly zone: Locator;
  readonly enterZoneLocator: Locator;
  readonly enterBoothLocator: Locator;
  readonly boothChatIconClick: Locator;
  readonly

  constructor(page: Page) {
    super(page);
    this.emptyStateExpoHeading = this.page.locator(
      "div[class^='styles-module__headingElement'] h2"
    );

    this.noExpoOpenMessage = this.page
      .locator("div[class^='styles-module__headingWithUnderline'] h6")
      .first();

    this.expoTitle = this.page
      .locator("div[class^='styles-module__title']")
      .first();

    this.enterZoneButton = this.page.locator("button", {
      hasText: "Enter zone",
    });

    this.expoButton = this.page
      .locator("a[class^='styles-module__option']")
      .filter({ hasText: "EXPO" });

    this.booth = this.page.locator("div[class^='styles-module__boothName']");

    this.zone = this.page.locator("div[class^='styles-module__zoneCard']");

    this.enterZoneLocator = this.page.locator("text=Enter zone");
    this.enterBoothLocator = this.page.locator(
      "div[class^='styles-module__newBoothCard']"
    );
    this.boothChatIconClick = this.page.locator("label", { hasText: "CHAT" });
  }

  async clickOnChatIcon() {
    await this.boothChatIconClick.click();
  }

  // Enter zone of expo
  async enterZone(zoneName: string = "Zone1") {
    this.zone.filter({ hasText: zoneName }).click();
  }

  async enterBooth(boothName: string = "Booth1") {
    this.booth.filter({ hasText: boothName }).click();
  }

  async enterBoothWithId(boothId: string) {
    let boothLocator = this.page.locator(
      `//div[contains(@class, 'newBoothCard')]//a[contains(@href, '${boothId}')]`
    );

    boothLocator.click();
  }

  async verifyBooth(boothName: string) {
    let boothLocator = this.booth.filter({ hasText: boothName });

    await expect(
      boothLocator,
      "Expecting " + boothName + " to be visible."
    ).toBeVisible({ timeout: 10000 });
  }

  // Verify expo widgets

  // Verify image widget
  async verifyImageWidget(imageAlt: string, urlRedirectedTo: string) {
    let imageWidgetLocator = this.page.getByAltText(imageAlt).nth(1);
    // Verify redirected webpage is same as urlRedirected
    var pagePromise = this.page
      .context()
      .waitForEvent("page", (p) => p.url() == urlRedirectedTo);
    await imageWidgetLocator.click();
    const newPage = await pagePromise;
    console.log("Redirected page url is", newPage.url());
    expect(newPage.url(), `Redirected url to be ${urlRedirectedTo}`).toBe(
      urlRedirectedTo
    );
    await newPage.close();
  }

  // Verify video widget
  async verifyVideoWidget() {
    //styles-module__videoContainer___pJlTK
    let videoWidgetLocator: Locator = this.page.locator(
      "div[class^='styles-module__videoContainer']"
    );
    // Verify video widget is visible
    await expect(
      videoWidgetLocator,
      "Expect video widget locator to be visible"
    ).toBeVisible({ timeout: 60000 });
  }

  // Verify Internal image carousel widget
  async verifyInternalImageCarouselWidget(
    urlRedirectedTo: string | RegExp,
    index: number
  ) {
    let imageCarouseloWidgetLocator: Locator = this.page
      .locator("div[class^='styles-module__imageCarouselWidgetContainer']")
      .nth(index);
    // Verify image carousel widget is visible
    await expect(
      imageCarouseloWidgetLocator,
      "Expect image widget locator to be visible"
    ).toBeVisible();
    await imageCarouseloWidgetLocator.click();
    expect(this.page.url(), `Redirected url to be ${urlRedirectedTo}`).toBe(
      urlRedirectedTo
    );
    // Getting back to lobby
    await this.expoButton.click();
    await this.enterZone();
    await this.enterBooth();
    await this.page.waitForLoadState();
  }

  // Verify External image carousel widget
  async verifyExternalImageCarouselWidget(
    index: number,
    urlRedirectedTo: string
  ) {
    let imageCarouseloWidgetLocator: Locator = this.page
      .locator("div[class^='styles-module__imageCarouselWidgetContainer']")
      .nth(index);
    // Verify image carousel widget is visible
    await expect(
      imageCarouseloWidgetLocator,
      "Expect image widget locator to be visible"
    ).toBeVisible();
    // Verify redirected webpage is same as urlRedirected
    var pagePromise = this.page
      .context()
      .waitForEvent("page", (p) => p.url() == urlRedirectedTo);
    await imageCarouseloWidgetLocator.click();
    const newPage = await pagePromise;
    console.log("Redirected page url is", newPage.url());
    expect(newPage.url(), `Redirected url to be ${urlRedirectedTo}`).toBe(
      urlRedirectedTo
    );
    await newPage.close();
  }

  // Verify Ticker widget and its content
  async verifyTickerWidget(tickercontent: string) {
    let tickerWidgetLocator: Locator = this.page.locator(
      "div[class^='styles-module__tickerContainer']"
    );
    // Verify ticker widget is visible
    await expect(tickerWidgetLocator, "Ticker widget is visible").toBeVisible();
    // Verify the content of ticker widget
    await expect(tickerWidgetLocator).toContainText(tickercontent);
  }

  // Verify Go to widget
  async verifyGoToWidget(urlRedirectedTo: string | RegExp) {
    let goToWidgetLocator: Locator = this.page
      .locator("div[class^='widget widget-BUTTON']")
      .first();
    // Verify image carousel widget is visible
    await expect(
      goToWidgetLocator,
      "Expect go to widget locator to be visible"
    ).toBeVisible();
    await goToWidgetLocator.click();
    expect(this.page.url(), `Redirected url to be ${urlRedirectedTo}`).toBe(
      urlRedirectedTo
    );
    // Getting back to lobby
    await this.expoButton.click();
    await this.enterZone();
    await this.enterBooth();
    await this.page.waitForLoadState();
  }

  // Verify Contact us widget
  async verifyContactusWidget(contactText: string | RegExp) {
    let contactWidgetLocator: Locator = this.page
      .locator("div[class^='widget widget-BUTTON']")
      .nth(1);
    // Verify Contact widget is visible
    await expect(
      contactWidgetLocator,
      "Expect go to widget locator to be visible"
    ).toBeVisible();
    await expect(
      contactWidgetLocator,
      `expecting contact widget to contain text ${contactText}`
    ).toContainText(contactText);
  }

  // Verify Hotspot widget
  async verifyHotspotWidget(urlRedirectedTo: string | RegExp) {
    let hotspotLocator: Locator = this.page.locator(
      "div[class^='widget widget-HOTSPOT']"
    );
    // Verify image carousel widget is visible
    await expect(
      hotspotLocator,
      "Expect hotspot locator to be visible"
    ).toBeVisible();
    await hotspotLocator.click();
    expect(this.page.url(), `Redirected url to be ${urlRedirectedTo}`).toBe(
      urlRedirectedTo
    );
    // Getting back to lobby
    await this.expoButton.click();
    await this.enterZone();
    await this.enterBooth();
    await this.page.waitForLoadState();
  }

  // Verify Iframe widget
  async verifyIframeWidget(srcIframe: string) {
    let iframeLocator: Locator = this.page.locator(
      "div[class^='widget widget-IFRAME']"
    );
    // Verify image carousel widget is visible
    await expect(
      iframeLocator,
      "Expect hotspot locator to be visible"
    ).toBeVisible();
    // Verifying source of iframe locator
    let iframeSrc = await iframeLocator.locator("iframe").getAttribute("src");
    console.log("iframeSrc: ", iframeSrc);
    expect(iframeSrc, "Verifying iframe src to be same as passed").toBe(
      srcIframe
    );
  }

  // Verify File download widget
  async verifyFileDownloadWidget(redirectedHref: string | RegExp) {
    let fileDownloadWidgetLocator: Locator = this.page
      .locator("div[class^='widget widget-FILE']")
      .first();
    // Verify Text widget is visible
    await expect(
      fileDownloadWidgetLocator,
      "Expect File widget locator to be visible"
    ).toBeVisible();
    // Verifying href of  locator
    let fileDownloadWidgetHref = await fileDownloadWidgetLocator
      .locator("a")
      .getAttribute("href");
    console.log("iframeSrc: ", fileDownloadWidgetHref);
    expect(
      fileDownloadWidgetHref,
      "Verifying iframe src to be same as passed"
    ).toBe(redirectedHref);
  }

  // Verify Coupon Code widget
  async verifyCouponCodeWidget(couponCode: string, couponExpiryDate: string) {
    let couponWidgetLocator: Locator = this.page
      .locator("div[class^='widget widget-COUPON_WIDGET']")
      .first();
    // Verify Text widget is visible
    await expect(
      couponWidgetLocator,
      "Expect File widget locator to be visible"
    ).toBeVisible();
    // Verifying Coupon code
    // let couponCodeWidgetText = await couponWidgetLocator
    //   .locator("div[class^=styles-module__couponCode]")
    //   .textContent();
    // console.log("couponCodeWidgetText:", couponCodeWidgetText);

    await expect(
      couponWidgetLocator,
      `expecting coupon code locator to have text ${couponCode}`
    ).toContainText(couponCode);
    // expect(couponCodeWidgetText, "Verifying coupon code").toBe(couponCode);

    //verify expiry date of widget

    let expiryDateWidgetText = couponWidgetLocator.locator(
      "div[class^=styles-module__expiry]"
    );

    await expect(expiryDateWidgetText).toContainText(couponExpiryDate);

    let claimCouponLocator: Locator = couponWidgetLocator.locator("label", {
      hasText: "CLAIM COUPON",
    });

    await claimCouponLocator.click();
    // Verify Thanks I will take it is visible.
    let thanksLocator: Locator = this.page.getByText("Thanks! I'll Take it");
    await expect(thanksLocator).toBeVisible();
    await thanksLocator.click();
  }

  // Verify Text widget
  async verifyTextWidget(textContentofwidget: string) {
    let textWidgetLocator: Locator = this.page.locator(
      "div[class^='widget widget-TEXT']"
    );
    // Verify Text widget is visible
    await expect(
      textWidgetLocator,
      "Expect Text widget locator to be visible"
    ).toBeVisible();

    // verify the content of text widget
    // await expect(
    //   textWidgetLocator,
    //   `expecting the content of text widget to have ${textContentofwidget}`
    // ).toContainText(textContentofwidget);
  }
  async enterIntoBooth() {
    await this.enterBoothLocator.click();
    await this.reload();
    await this.boothChatIconClick.click();
  }

  async verifyBoothVisibility(
    boothId: string,
    boothName: string,
    expectedVisibility: boolean = true
  ) {
    let noBoothMessageLocator: Locator = this.page.locator(
      "//h6[contains(@class, 'text') and text()='Expos are not open yet.']"
    );

    let boothLocator: Locator = this.page.locator(
      `//div[contains(@class, 'newBoothCard')]//a[contains(@href, '${boothId}')]`
    );

    await test.step(`Verify the Visibilty of booth - ${boothName} || Expected Visibility Stauts - ${expectedVisibility}`, async () => {
      if (expectedVisibility) {
        await expect(
          boothLocator,
          `Expect booth - ${boothName} to be visible.`
        ).toBeVisible({
          timeout: 15000,
        });
      } else {
        await expect(
          noBoothMessageLocator,
          "Expos are not open yet."
        ).toBeVisible({
          timeout: 10000,
        });
      }
    });
  }

  async reload() {
    await this.page.reload();
  }

  async getPublishedAnnouncementComponent() {
    return new PublishedAnnouncementComponent(this.page);
  }
  
  async clickOnEnterZoneButton() {
    await this.enterZoneButton.click();
  }
}
