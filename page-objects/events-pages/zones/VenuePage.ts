import { Locator, Page, expect } from "@playwright/test";
import ZoneType from "../../../enums/ZoneTypeEnum";
import { PublishedAnnouncementComponent } from "../live-side-components/PublishedAnnouncement";

export class VenuePage {
  readonly page: Page;
  readonly zoneTileLocator: Locator;
  readonly toggleStatusString: string;
  readonly toggleLocatorString: string;
  toggleButton: Locator;
  currentZoneName: ZoneType;

  constructor(page: Page) {
    this.page = page;
    this.zoneTileLocator = this.page.locator(
      "div[class^='draggable-list_draggableItem']"
    );
    this.toggleLocatorString = "label[class^='Toggle_switch']";
    this.toggleStatusString = "input[type='checkbox']";
  }

  async toggleZone(zoneName: ZoneType) {
    await this.createToggleButtonLocator(zoneName);
    await this.toggleButton.click();
  }

  private async createToggleButtonLocator(zoneName: ZoneType) {
    if (!this.toggleButton || zoneName !== this.currentZoneName) {
      const zoneTileLocatorForGivenZone = this.zoneTileLocator.filter({
        hasText: zoneName,
      });
      this.toggleButton = zoneTileLocatorForGivenZone.locator(
        this.toggleLocatorString
      );
      this.currentZoneName = zoneName;
    }
  }

  async verifyZoneisDisabledFromUI(zoneName: ZoneType) {
    await this.createToggleButtonLocator(zoneName);
    const isChecked = await this.toggleButton
      .locator(this.toggleStatusString)
      .getAttribute("checked");
    await expect(isChecked).toBe(null);
  }
  async verifyZoneisEnabledFromUI(zoneName: ZoneType) {
    await this.createToggleButtonLocator(zoneName);
    const isChecked = await this.toggleButton
      .locator(this.toggleStatusString)
      .getAttribute("checked");
    await expect(isChecked).not.toBe(null);
  }

  async getPublishedAnnouncementComponent() {
    return new PublishedAnnouncementComponent(this.page);
  }
}
