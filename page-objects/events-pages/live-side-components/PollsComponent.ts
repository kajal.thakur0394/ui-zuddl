import { Locator, Page, expect, test } from "@playwright/test";
export class PollsComponent {
  readonly page: Page;
  readonly draftPollsTab: Locator;
  readonly publishedPollsTab: Locator;
  readonly createPollButton: Locator;
  readonly pollListContainer: Locator;
  readonly eachPollContainerLocator: Locator;
  readonly publishPollButton: string;
  readonly publishQuizButton: string;
  readonly pollOptionDropDown: string;
  readonly deleteOptionOnDropDown: Locator;
  readonly editOptionOnDropDown: Locator;
  readonly pollContainer: Locator;
  readonly zonePollTab: Locator;
  readonly closeAndHideLocator: Locator;
  readonly newPollButtonLocator: Locator;
  readonly eventTabListLocator: Locator;
  readonly draftsPollTabLocator: Locator;
  readonly publishPollTabLocator: Locator;
  readonly createPollTabLocator: Locator;

  //poll create form
  readonly pollCreateFormContainer: Locator;
  readonly pollQuestionInputBox: Locator;
  readonly optionAInputBox: Locator;
  readonly optionBInputBox: Locator;
  readonly buttonToAddAnotherOption: Locator;
  readonly saveDraftButton: Locator;
  readonly publishNowButton: Locator;
  readonly toggleButtonToEnableOrDisableQuiz: Locator;
  readonly buttonToCloseCreatePollForm: Locator;
  readonly deleteOptionButton: string;
  readonly allOptionsLocator: Locator;

  //load more polls
  readonly loadMorePollsButtonLocator: Locator;

  //close and hidden box
  readonly closeNhiddenTextBox: Locator;

  //poll options
  readonly closePollDropDownOptionLocator: Locator;
  readonly closeNHideDropDownOptionLocator: Locator;
  readonly numberOfOptionsLocator: Locator;

  //prompt button to approve or reject
  readonly approvalButtonOnPrompt: Locator;

  constructor(page: Page) {
    this.page = page;
    this.draftPollsTab = this.page.locator("#draftPolls");
    this.publishedPollsTab = this.page.locator("#publishedPolls");
    this.createPollButton = this.page
      .locator("button")
      .filter({ hasText: "Create Poll" });
    this.pollListContainer = this.page.locator(
      "div[class^='styles-module__pollListContainer']"
    );
    this.eachPollContainerLocator = this.pollListContainer.locator(
      "div[class^='styles-module__infiniteScrollItem']"
    );
    this.publishPollButton = "Publish Poll";
    this.publishQuizButton = "Publish Quiz";
    this.pollOptionDropDown = "div[class^='styles-module__pollDropdown']";
    this.deleteOptionOnDropDown = this.page
      .locator("div[class^='styles-module__dropdownOption']")
      .filter({ hasText: "Delete" });
    this.editOptionOnDropDown = this.page
      .locator("div[class^='styles-module__dropdownOption']")
      .filter({ hasText: "Edit" });

    //poll create form
    this.pollCreateFormContainer = this.page.locator(
      "div[class^='styles-module__pollContainer']"
    );
    this.pollQuestionInputBox =
      this.page.getByPlaceholder("Type Poll Question");
    this.optionAInputBox = this.page.getByPlaceholder("Type Option A");
    this.optionBInputBox = this.page.getByPlaceholder("Type Option B");
    this.buttonToAddAnotherOption = this.page.locator(
      "div[class^='styles-module__addAnotherButton']"
    );
    this.saveDraftButton = this.page
      .locator("button")
      .filter({ hasText: "Save Draft" });
    this.publishNowButton = this.page
      .locator("button")
      .filter({ hasText: "Publish Now" });
    this.toggleButtonToEnableOrDisableQuiz = this.page.locator(
      "div[data-testid='toggle-field']"
    );
    this.buttonToCloseCreatePollForm = this.page.locator(
      "div[class^='styles-module__crossIconContainer___']"
    );
    this.deleteOptionButton = "div[class^='styles-module__deleteContainer']";
    this.allOptionsLocator = this.page
      .locator("div[class^='styles-module__container']")
      .filter({ has: this.page.locator(this.deleteOptionButton) });

    // load more button to load new polls
    this.loadMorePollsButtonLocator = this.page.locator(
      "div[class^='styles-module__loadMoreButton']"
    );

    //close and hidden box
    this.closeNhiddenTextBox = this.page.locator(
      "div[class^='styles-module__closeAndHiddenTextBox']"
    );

    //poll dropdown options
    this.closePollDropDownOptionLocator = this.page
      .locator("div[class^='styles-module__dropdownOption']")
      .filter({ hasText: "Close Poll" });

    this.closeNHideDropDownOptionLocator = this.page
      .locator("div[class^='styles-module__dropdownOption']")
      .filter({ hasText: "Close & Hide" });

    // approval button
    this.approvalButtonOnPrompt = this.page.locator(
      "div[class^='styles-module__approveButton']"
    );
    this.numberOfOptionsLocator = this.page.locator(
      "div[class^='styles-module__optionQuestion___']"
    );
    this.pollContainer = this.page.locator(
      "div[class^='styles-module__pollOptionsContainer']"
    );
    this.closeAndHideLocator = this.page.locator(
      "div[class^='styles-module__closeAndHiddenTextBox']"
    );
    this.newPollButtonLocator = this.page.locator(
      "div[class^='styles-module__buttonTextContainer___']"
    );
    this.eventTabListLocator = this.page.locator(
      "div[class^='styles-module__primaryIconSidePanelOptionsContainer']"
    );
    this.draftPollsTab = this.page.locator("#draftPolls");
    this.publishedPollsTab = this.page.locator("#publishedPolls");
    this.createPollTabLocator = this.page.locator(
      "div[class^='styles-module__createPollCTA___']"
    );
    this.zonePollTab = this.page.locator("#ZONE_POLL");
  }

  //publish poll

  getPollByTitle(expectedPollTitle: string) {
    return this.eachPollContainerLocator.filter({ hasText: expectedPollTitle });
  }

  getPollByListIndex(listIndex) {
    return this.eachPollContainerLocator.nth(listIndex);
  }
  async publishPollWithTitle(pollTitle: string) {
    const pollContainer = this.getPollByTitle(pollTitle);
    const publishButtonLocator = pollContainer.locator("button").filter(
      { hasText: this.publishPollButton } //Publish Poll button locator
    );
    const publishQuizButtonLocator = pollContainer.locator("button").filter(
      { hasText: this.publishQuizButton } //Publish Quiz button locator
    );

    if ((await publishButtonLocator.count()) > 0) {
      await publishButtonLocator.click();
    } else {
      await publishQuizButtonLocator.click();
    }
    await this.approvalButtonOnPrompt.click();
    await this.verifyPollIsNotVisibleOnList(pollTitle);
  }

  async publishPollByListIndex(listIndex: number) {
    const pollContainer = this.getPollByListIndex(listIndex);
    await pollContainer
      .locator("button")
      .filter({ hasText: this.publishPollButton })
      .click();
  }
  //delete poll
  async deletePollWithTitle(pollTitle: string) {
    const pollContainer = this.getPollByTitle(pollTitle);
    await pollContainer.locator(this.pollOptionDropDown).click();
    await this.deleteOptionOnDropDown.click();
  }

  async deletePollByListindex(listIndex: number) {
    const pollContainer = this.getPollByListIndex(listIndex);
    await pollContainer.locator(this.pollOptionDropDown).click();
    await this.deleteOptionOnDropDown.click();
  }

  //click on edit poll
  async openPollToEdit(pollTitle: string) {
    const pollContainer = this.getPollByTitle(pollTitle);
    await pollContainer.locator(this.pollOptionDropDown).click();
    await this.editOptionOnDropDown.click();
  }

  //delte the option
  async deleteTheOption(optionText: string) {
    const optionTextLocator = this.page.locator(`input[value=${optionText}]`);
    const thisOptionLocator = this.allOptionsLocator.filter({
      has: optionTextLocator,
    });
    const deleteButtonOnIt = thisOptionLocator.locator(this.deleteOptionButton);
    await deleteButtonOnIt.click();
  }

  async clickToSelectCorrectAnswerForQuiz(expectedAnswer: string) {
    await this.page.locator("div[class^='styles-module__selectField']").click();
    await this.page
      .locator("div[id*='react-select']")
      .filter({ hasText: expectedAnswer })
      .click();
  }

  // load more button to load new polls
  async clickOnLoadMoreButtonToLoadNewPolls() {
    await this.loadMorePollsButtonLocator.click();
  }

  async verifyClosedAndHideenBoxIsVisibleOnPoll(expectedPollTitle: string) {
    const pollLocator = this.getPollByTitle(expectedPollTitle);
    const closeNHideenBoxOnThisPoll = pollLocator.filter({
      has: this.closeNhiddenTextBox,
    });
    await expect(
      closeNHideenBoxOnThisPoll,
      "expecting close and hidden text box to be visible on the poll "
    ).toBeVisible();
  }

  async verifyPollIsClosed(expectedPollTitle: string) {
    const pollLocator = this.getPollByTitle(expectedPollTitle);
    const pollFooter = pollLocator.locator(
      "div[class^='styles-module__pollFooter']"
    );
    await expect(
      pollFooter,
      `expecting poll footer to show text Closed`
    ).toContainText("Poll closed");
  }

  async verifyPollIsVisibleOnList(expectedPollTitle: string) {
    const pollLocator = this.getPollByTitle(expectedPollTitle);
    await expect(
      pollLocator,
      `expecting poll with title ${expectedPollTitle} to be visible`
    ).toBeVisible();
  }

  async verifyPollIsNotVisibleOnList(expectedPollTitle: string) {
    const pollLocator = this.getPollByTitle(expectedPollTitle);
    await expect(
      pollLocator,
      `expecting poll with title ${expectedPollTitle} not to be visible`
    ).not.toBeVisible();
  }

  // tabs
  async openDraftPollsTab() {
    await this.draftPollsTab.click();
  }

  async openPublishedPollsTab() {
    await this.publishedPollsTab.click();
  }

  //voting on poll
  async selectOptionToVoteOnPoll(
    pollTitle: string,
    pollOptionToSelect: string
  ) {
    const pollVoteApiRespPromise = this.page.waitForResponse(/vote/);
    const pollOptionLocator = this.getOptionLocatorOnPoll(
      pollTitle,
      pollOptionToSelect
    );
    await expect(
      pollOptionLocator,
      `Expecting poll option with text ${pollOptionToSelect} to be visible`
    ).toBeVisible();
    await pollOptionLocator.click();
    //wait for the vote api response
    await test.step(`Expecting poll vote API to trigger and return with 200 response`, async () => {
      const pollVoteApiPromise = await pollVoteApiRespPromise;
      expect(
        pollVoteApiPromise.status(),
        `expecting vote API to hit status 200`
      ).toBe(200);
    });
  }

  async verifyVoteCountOnPollMatches(
    pollTitle: string,
    expectedVoteCount: number
  ) {
    const pollContainer = this.getPollByTitle(pollTitle);
    const voteContainerOnThisPoll = pollContainer.locator(
      "div[class^='styles-module__totalVotesCount']"
    );
    await expect(
      voteContainerOnThisPoll,
      `expecting vote count to be ${expectedVoteCount}`
    ).toContainText(expectedVoteCount.toString(), { timeout: 20000 });
  }

  async verifyVoteCountToShowEmpty(pollTitle: string) {
    const pollContainer = this.getPollByTitle(pollTitle);
    const voteContainerOnThisPoll = pollContainer.locator(
      "div[class^='styles-module__totalVotesCount']"
    );
    await expect(
      voteContainerOnThisPoll,
      `expecting vote count to be ''`
    ).toContainText("");
  }

  async closeThePoll(pollTitle: string) {
    await test.step(`Closing the poll with title ${pollTitle}`, async () => {
      const pollLocator = this.getPollByTitle(pollTitle);
      const pollCloseApiPromise = this.page.waitForResponse(/close/);
      const menuOptionLocator = pollLocator.locator(this.pollOptionDropDown);
      await menuOptionLocator.click();
      await test.step(`choosing close option from the dropdown`, async () => {
        await this.closePollDropDownOptionLocator.click();
      });
      await test.step(`Clicking on close button on confirmation prompt`, async () => {
        await this.approvalButtonOnPrompt.click();
      });
      await test.step(`Waiting for close API to trigger and return 200 status code`, async () => {
        const pollCloseApiResp = await pollCloseApiPromise;
        expect(
          pollCloseApiResp.status(),
          `expecting poll close api to return with 200 status code`
        ).toBe(200);
      });
    });
  }

  async closeNhideThePoll(pollTitle: string) {
    await test.step(`Closing the poll with title ${pollTitle}`, async () => {
      const pollLocator = this.getPollByTitle(pollTitle);
      const pollCloseApiPromise = this.page.waitForResponse(/close/);
      const menuOptionLocator = pollLocator.locator(this.pollOptionDropDown);
      await menuOptionLocator.click();
      await test.step(`choosing close option from the dropdown`, async () => {
        await this.closeNHideDropDownOptionLocator.click();
      });
      await test.step(`Clicking on close button on confirmation prompt`, async () => {
        await this.approvalButtonOnPrompt.click();
      });
      await test.step(`Waiting for close API to trigger and return 200 status code`, async () => {
        const pollCloseApiResp = await pollCloseApiPromise;
        expect(
          pollCloseApiResp.status(),
          `expecting poll close api to return with 200 status code`
        ).toBe(200);
      });
    });
  }

  async verifyPollOptionIsDisabled(pollTitle: string, pollOptionText: string) {
    const pollOptionContainer = this.getPollOptionContainerOnPoll(
      pollTitle,
      pollOptionText
    );
    await expect(
      pollOptionContainer,
      `expecting poll option ${pollOptionText} to be disabled`
    ).toContain(/optionDisabled/);
  }
  getOptionLocatorOnPoll(pollTitle: string, pollOptionText: string) {
    const pollLocator = this.getPollByTitle(pollTitle);
    const optionLocatorOnThisPoll = pollLocator
      .locator("div[class^='styles-module__optionQuestion']")
      .filter({ hasText: pollOptionText });
    return optionLocatorOnThisPoll;
  }

  getPollOptionContainerOnPoll(pollTitle: string, pollOptionText: string) {
    const pollLocator = this.getPollByTitle(pollTitle);
    const pollOptionContainer = pollLocator
      .locator(
        "div[class^='styles-module__optionsContainer'] div[class^='styles-module__option_']"
      )
      .filter({ hasText: pollOptionText });
    return pollOptionContainer;
  }

  async verifyExpectedOptionVisibleinPoll(optionName: string) {
    await expect(
      this.numberOfOptionsLocator.filter({ hasText: optionName })
    ).toBeVisible();
  }

  async verifyPollCannotBeCasted(pollName: string) {
    const nestedLocatorString = "div[class^='styles-module__option___']";
    const isLocatorPresent2 = this.pollContainer
      .filter({ hasText: pollName })
      .locator(nestedLocatorString)
      .first();
    const cursor = await isLocatorPresent2.evaluate((element) =>
      window.getComputedStyle(element).getPropertyValue("cursor")
    );
    expect(cursor).toEqual("not-allowed");
  }

  async verifyCloseAndHideButtonIsVisible() {
    await expect(this.closeAndHideLocator).toBeVisible();
  }

  async verifyCloseAndHideButtonIsInvisible() {
    await expect(this.closeAndHideLocator).toBeHidden();
  }

  async verifyNewPollsButtonVisible() {
    await expect(
      this.newPollButtonLocator.filter({ hasText: "New Polls" })
    ).toBeVisible();
  }

  async clickonNewPollsButton() {
    this.newPollButtonLocator.click();
  }

  async verifyPollsTabtoBeInvisibleInInteractionPanel() {
    await expect(
      this.eventTabListLocator.filter({ hasText: "Polls" })
    ).toBeHidden();
  }

  async verifyPollsTabtoBeVisibleInInteractionPanel() {
    await expect(
      this.eventTabListLocator.filter({ hasText: "Polls" })
    ).toBeVisible();
  }

  async verifyDraftsPollTabToBePresent() {
    await expect(
      this.draftPollsTab.filter({ hasText: "Drafts" })
    ).toBeVisible();
  }

  async verifyPublishedPollTabToBePresent() {
    await expect(
      this.publishedPollsTab.filter({ hasText: "Published" })
    ).toBeVisible();
  }

  async verifyCreatePollTabToBePresent() {
    await expect(
      this.createPollTabLocator.filter({ hasText: "Create Poll" })
    ).toBeVisible();
  }

  async selectPollTab() {
    await this.zonePollTab.click();
  }
}
