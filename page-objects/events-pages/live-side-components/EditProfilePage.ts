import { Locator, Page, expect } from "@playwright/test";
import { EditProfileFieldObject } from "../../../test-data/editProfileForm";

export enum EditProfileTabs {
  BASIC_DETAILS_TAB = "Basic Details",
  MY_INTERESTS_TAB = "My Interests",
  NETWORKING_FILTERS_TAB = "Networking Filters",
}
export class EditProfilePage {
  readonly page: Page;
  readonly basicDetailsTab: Locator;
  readonly myInterestsTab: Locator;
  readonly networkingFiltersTab: Locator;
  readonly firstNameInputBox: Locator;
  readonly lastNameInputBox: Locator;
  readonly titleInputBox: Locator;
  readonly companyInputbox: Locator;
  readonly saveNnextButton: Locator;
  readonly saveNCloseButton: Locator;
  readonly editProfileModal: Locator;

  constructor(page: Page) {
    this.page = page;
    this.firstNameInputBox = this.page.getByPlaceholder("John", {
      exact: true,
    });
    this.lastNameInputBox = this.page.getByPlaceholder("Doe", { exact: true });
    this.saveNnextButton = this.page.locator("button:has-text('Save & Next')");
    this.saveNCloseButton = this.page.locator(
      "button:has-text('Save & Close')"
    );
    this.editProfileModal = this.page.locator(
      "div[class*='styles-module__detailedProfileUpdateModal']"
    );
  }

  async openTab(tabName: EditProfileTabs) {
    await this.page.click(`text=${tabName}`);
  }

  async fillUserProfileForm(listOfFieldsObj: any) {
    console.log(`handing user field form data`);
    for (const eachFieldObject of listOfFieldsObj) {
      await this.handleTheProfileField(eachFieldObject);
    }
    //click on save button
    await this.saveNnextButton.click();
    this.page.waitForTimeout(1000);
    await this.saveNnextButton.click();
    this.page.waitForTimeout(1000);
    await this.saveNCloseButton.click();
  }

  async handleTheProfileField(fieldDataObject: EditProfileFieldObject) {
    console.log(`evaulating field with label ${fieldDataObject.fieldLabel}`);
    if (fieldDataObject.fieldType == "text") {
      await this.handleTextField(fieldDataObject);
    } else if (fieldDataObject.fieldType == "numeric") {
      await this.handleNumericField(fieldDataObject);
    } else if (fieldDataObject.fieldType == "dropdown") {
      await this.handleDropDownField(fieldDataObject);
    } else if (fieldDataObject.fieldType == "multiselect") {
      await this.handleMultiSelectField(fieldDataObject);
    }
  }

  async handleTextField(fieldDataObject: EditProfileFieldObject) {
    if (!fieldDataObject.toSkip) {
      console.log(
        `handling text field with label ${fieldDataObject.fieldLabel} by entering data ${fieldDataObject.dataToEnter}`
      );
      if (fieldDataObject.fieldLabel == "BIO") {
        //the input field of bio is of type text area
        await this.page
          .locator(
            "div[class^='styles-module__formSectionRowContainer'] div[class^='styles-module__container']"
          )
          .filter({ hasText: fieldDataObject.fieldLabel })
          .locator("textarea")
          .fill(fieldDataObject.dataToEnter);
      } else if (fieldDataObject.fieldLabel == "COUNTRY") {
        await this.page
          .locator(
            `div[class^='styles-module__selectField']:has-text('${fieldDataObject.fieldLabel}')`
          )
          .click();
        await this.page
          .locator(
            `div[id^='react-select-']:has-text('${fieldDataObject.dataToEnter}')`
          )
          .nth(1)
          .click();
      } else {
        await this.page
          .locator(
            "div[class^='styles-module__formSectionRowContainer'] div[class^='styles-module__container']"
          )
          .filter({ hasText: fieldDataObject.fieldLabel })
          .locator("input")
          .fill(fieldDataObject.dataToEnter);
      }
    } else {
      console.log(
        `Skipping field ${fieldDataObject.fieldLabel} as per given test data instruction`
      );
    }
  }

  async handleNumericField(fieldDataObject: EditProfileFieldObject) {
    if (!fieldDataObject.toSkip) {
      console.log(
        `handling numeric field with label ${fieldDataObject.fieldLabel} by entering data ${fieldDataObject.dataToEnter}`
      );
      await this.page
        .locator(
          "div[class^='styles-module__formSectionRowContainer'] div[class^='styles-module__container']"
        )
        .filter({ hasText: fieldDataObject.fieldLabel })
        .locator("input")
        .fill(fieldDataObject.dataToEnter);
    } else {
      console.log(
        `Skipping field ${fieldDataObject.fieldLabel} as per given test data instruction`
      );
    }
  }

  async handleDropDownField(fieldDataObject: EditProfileFieldObject) {
    console.log(`handling dropdown field`);
    if (!fieldDataObject.toSkip) {
      console.log(
        `handling dropdown field with label ${fieldDataObject.fieldLabel} by entering data ${fieldDataObject.dropDownOptionToSelect}`
      );
      const dropDownContainer = this.page
        .locator(
          "div[class^='styles-module__formSectionRowContainer'] div[class^='styles-module__container']"
        )
        .filter({ hasText: fieldDataObject.fieldLabel });
      await dropDownContainer.locator("div[class$='control']").click();
      //select the option
      const dropdownOption: Locator = dropDownContainer
        .locator("div[id^='react-select']")
        .filter({
          hasText: fieldDataObject.dropDownOptionToSelect,
        });
      //clicking on dropdown option to select
      await dropdownOption.click();
    } else {
      `Skipping field ${fieldDataObject.fieldLabel} as per given test data instruction`;
    }
  }

  async handleMultiSelectField(fieldDataObject: EditProfileFieldObject) {
    if (!fieldDataObject.toSkip) {
      console.log(
        `handling multiselect field with label ${fieldDataObject.fieldLabel} by entering data ${fieldDataObject.dataToEnter}`
      );
      const multiSelectDropDownContainer = this.page
        .locator(
          "div[class^='styles-module__formSectionRowContainer'] div[class^='styles-module__container']"
        )
        .filter({ hasText: fieldDataObject.fieldLabel })
        .locator(".dropdown-container");
      await multiSelectDropDownContainer.click();
      //select the option
      const multiSelectOption = multiSelectDropDownContainer.locator(
        "div.dropdown-content li"
      );
      //clicking on each option to select all options from multiselect dropdown
      for (const eachOption of fieldDataObject.listOfOptionsToChooseInMultiSelectDropDown) {
        await multiSelectOption
          .filter({
            hasText: eachOption,
          })
          .click();
      }
    } else {
      `Skipping field ${fieldDataObject.fieldLabel} as per given test data instruction`;
    }
  }

  async verifyEditProfileModalIsVisible() {
    await expect(this.editProfileModal).toBeVisible();
  }
}
