import { Locator, Page, expect, test } from "@playwright/test";
import {
  AnswersViewingSortingEnum,
  OrganisersAnswersViewingSortingEnum,
  QuestionsSortingEnum,
} from "../../../enums/qNaSortingEnum";
import exp from "constants";

export class QnAComponent {
  //send question components
  readonly questionInputBoxContainer: Locator;
  readonly questionInputBox: Locator;
  readonly submitButtonToSubmitQuestion: Locator;
  readonly questionAfterReplyLocator: Locator;
  readonly answerTitleLocator: Locator;
  readonly answeredTextLocator: Locator;

  //tabs
  readonly newQuestionTab: Locator;
  readonly answweredTab: Locator;
  readonly actionBarLocator: Locator;

  //list of question container
  readonly listOfQuestionsContainer: Locator;
  readonly eachQuestionContainer: Locator;

  //action button on question card
  readonly upvoteButton: Locator;
  readonly upvotedButton: Locator;
  readonly downvoteButton: Locator;
  readonly upvoteCountLocator: Locator;
  readonly upvoteCount2Locator: Locator;
  readonly replyByTextButtonToOpenReplyContainer: Locator;
  readonly answerLiveButton: Locator;
  readonly deleteQuestionButton: Locator;
  readonly answeredTickButton: Locator;
  readonly doneButtonToCompleteLiveAnswer: Locator;

  //question filter for new question tab
  readonly questionListFilterForQuestionsTab: Locator;
  readonly eachfilterDropDownOptionForSortingQuestion: Locator;

  //answered filter for viewing option
  readonly questionListFilterForViewAnswers: Locator;
  readonly eachfilterDropDownOptionForViewingAnswers: Locator;
  readonly questionFilterVoteAnswer: Locator;
  readonly questionDropdown: Locator;
  readonly questionNameList: Locator;

  // question answering component reply with text
  readonly replyQuestionContainerBox: Locator;
  readonly replyQuestionInputBox: Locator;
  readonly cancelReplyButton: Locator;
  readonly replyButtonToSubmitReply: Locator;

  constructor(readonly page: Page) {
    this.page = page;

    //send question components
    this.questionInputBoxContainer = this.page.locator(
      "div[class^='styles-module__chatInputFieldContainer']"
    );
    this.questionInputBox = this.questionInputBoxContainer.locator("input");
    this.submitButtonToSubmitQuestion = this.questionInputBoxContainer.locator(
      "button[data-testid='chat-input-send-button']"
    );

    //tabs
    this.newQuestionTab = this.page.locator("#newQuestions");
    this.answweredTab = this.page.locator("#answeredQuestions");

    //list of questions container
    this.listOfQuestionsContainer = this.page.locator(
      "div[class^='styles-module__questionListContainer']"
    );
    this.eachQuestionContainer =
      this.listOfQuestionsContainer.locator("div[id^='qna']");

    // action button on the question card
    this.upvoteButton = this.page.locator("button:has-text('Upvote')");
    this.downvoteButton = this.page.locator("button:has-text('Upvoted')");
    this.replyByTextButtonToOpenReplyContainer = this.page.locator(
      "button:has-text('Reply')"
    );
    this.answerLiveButton = this.page.locator("button:has-text('Answer Live')");
    this.deleteQuestionButton = this.page.locator(
      "button[class^='styles-module__delete']"
    );
    this.answeredTickButton = this.page.locator(
      "div[class^='styles-module__answeredContainer']"
    );
    this.doneButtonToCompleteLiveAnswer = this.page.locator(
      "button:has-text('Done')"
    );

    //filters for questions
    this.questionListFilterForQuestionsTab = this.listOfQuestionsContainer
      .locator("div[class^='styles-module__questionFilterContainer']")
      .nth(0);
    this.eachfilterDropDownOptionForSortingQuestion =
      this.questionListFilterForQuestionsTab.locator(
        "div[class^='styles-module__filterDropdownOption']"
      );

    //filters for viewing answers
    this.questionListFilterForViewAnswers = this.listOfQuestionsContainer
      .locator("div[class^='styles-module__questionFilterContainer']")
      .nth(1);
    this.eachfilterDropDownOptionForViewingAnswers =
      this.questionListFilterForViewAnswers.locator(
        "div[class^='styles-module__filterDropdownOption']"
      );

    //REPLY Question component with text
    this.replyQuestionContainerBox = this.page.locator(
      "div[class^='styles-module__replyQuestionContainer']"
    );
    this.replyQuestionInputBox =
      this.replyQuestionContainerBox.locator("textarea");

    this.replyButtonToSubmitReply = this.replyQuestionContainerBox.locator(
      "button[class^='styles-module__replyReplyButton']"
    );
    this.cancelReplyButton = this.replyQuestionContainerBox.locator(
      "button[class^='styles-module__replyCancelButton']"
    );
    this.actionBarLocator = this.page.locator("div[class^='styles-module__actionBar___'] button");
    this.questionAfterReplyLocator = this.page.locator("div[class^='styles-module__question___']");
    this.answerTitleLocator = this.page.locator("div[class^='styles-module__answerTitle___']");
    this.answeredTextLocator = this.page.locator("div[class^='styles-module__answerText___']");
    this.upvoteCountLocator = this.page.locator("div[class^='styles-module__upVotesCountContainer']");
    this.upvotedButton = this.page.locator("button:has-text('Upvoted')");
    this.questionDropdown = this.page.locator("div[class^='styles-module__filterDropdownOption']");
    this.questionFilterVoteAnswer = this.page.locator("div[class^='styles-module__questionFilterContainer']");
    this.questionNameList = this.page.locator("div[class^='styles-module__question__']");
    this.upvoteCount2Locator = this.page.locator("div[class^='styles-module__upVotesCountContainerWithIcon'] span");
  }

  //enter question and submit
  async enterQuestionToAsk(questionText: string) {
    await test.step(`Asking question ${questionText}`, async () => {
      await this.questionInputBox.fill(questionText);
      await this.submitButtonToSubmitQuestion.click();
    });
  }

  //verify question appears on the panel
  async verifyQuestionWithGivenTextIsVisible(questionText: string) {
    await this.page.waitForTimeout(3000);
    await test.step(`Verify if question with given text ${questionText} is visible in the list of question`, async () => {
      const questionToLookOut = await this.eachQuestionContainer.filter({
        hasText: questionText,
      });

      await expect(
        questionToLookOut,
        `expecting question with text ${questionText} to be visible`
      ).toBeVisible();
    });
  }
  async verifyQuestionWithGivenTextIsNotVisible(questionText: string) {
    await this.page.waitForTimeout(1000);
    await test.step(`Verify if question with given text ${questionText} is  not visible in the list of question`, async () => {
      const questionToLookOut = await this.eachQuestionContainer.filter({
        hasText: questionText,
      });

      await expect(
        questionToLookOut,
        `expecting question with text ${questionText} not to be visible`
      ).not.toBeVisible();
    });
  }

  async replyGivenQuestionWithText(
    questionText: string,
    replyWithText: string
  ) {
    await test.step(`Going to reply question ${questionText} with this text ${replyWithText}`, async () => {
      const questionToLookOut = this.eachQuestionContainer.filter({
        hasText: questionText,
      });
      const replyWithTextButton = questionToLookOut.locator(
        this.replyByTextButtonToOpenReplyContainer
      );
      await test.step(`click on reply button`, async () => {
        await replyWithTextButton.click();
      });

      await test.step(`Enter text in the reply text box`, async () => {
        await this.replyQuestionInputBox.fill(replyWithText);
      });

      await test.step(`Submit the reply by clicking on reply button`, async () => {
        await this.replyButtonToSubmitReply.click();
      });
    });
  }

  async replyToGivenQuestion(
    questionText: string,
    { replyWithText = true, textToEnterInReply = "reply by automation" }
  ) {
    let questionToLookOut: Locator;
    await test.step(`Locate the question with text ${questionText}`, async () => {
      questionToLookOut = this.eachQuestionContainer.filter({
        hasText: questionText,
      });
    });

    if (replyWithText) {
      await test.step(`Going to reply to the given question by text and entering answer as ${textToEnterInReply}`, async () => {
        const replyWithTextButton = questionToLookOut.locator(
          this.replyByTextButtonToOpenReplyContainer
        );
        await test.step(`click on reply button`, async () => {
          await replyWithTextButton.click();
        });

        await test.step(`Enter text in the reply text box`, async () => {
          await this.replyQuestionInputBox.fill(textToEnterInReply);
        });

        await test.step(`Submit the reply by clicking on reply button`, async () => {
          await this.replyButtonToSubmitReply.click();
        });
      });
    } else {
      await test.step(`Going to reply by going live`, async () => {
        const replyByGoingLiveButton = questionToLookOut.locator(
          this.answerLiveButton
        );
        await replyByGoingLiveButton.click();
      });
    }
  }

  async markQuestionBeingAnsweredByLiveAsDone(questionText: string) {
    await test.step(`Going to find question ${questionText} and click on Done button to mark question is done being answered in live mode`, async () => {
      const questionToLookOut = this.eachQuestionContainer.filter({
        hasText: questionText,
      });
      const doneButton = questionToLookOut.locator(
        this.doneButtonToCompleteLiveAnswer
      );
      await test.step(`Clicking on done button`, async () => {
        await doneButton.click();
      });
    });
  }

  async verifyQuestionHasBeenAnsweredLiveStatusIsVisible(questionText: string) {
    await test.step(`Verify Answered Live status is visible for question with text ${questionText}`, async () => {
      const questionToLookOut = this.eachQuestionContainer.filter({
        hasText: questionText,
      });
      const answeredLiveStatus = questionToLookOut.locator(
        this.answeredTickButton
      );
      await expect(answeredLiveStatus).toContainText("Answered Live");
    });
  }

  async verifyQuestionHasBeenAnsweredByTextStatusIsVisible(
    questionText: string
  ) {
    await test.step(`Verify Answered Live status is visible for question with text ${questionText}`, async () => {
      const questionToLookOut = this.eachQuestionContainer.filter({
        hasText: questionText,
      });
      const answeredLiveStatus = questionToLookOut.locator(
        this.answeredTickButton
      );
      await expect(answeredLiveStatus).toContainText("Answered");
    });
  }

  async deleteQuestion(questionText: string) {
    await test.step(`Going to locate the question by ${questionText} and click on delete button to delete it`, async () => {
      const questionToLookOut = this.eachQuestionContainer.filter({
        hasText: questionText,
      });
      const deleteButton = questionToLookOut.locator(this.deleteQuestionButton);
      await deleteButton.click();
    });
  }

  async attendeeSwitchToNewQuestionsTab() {
    await test.step(`Attendee click on new question tab to switch on to it`, async () => {
      await this.newQuestionTab.click();
    });
  }

  async attendeeSwitchToAnsweredTab() {
    await test.step(`Attendee click on answered tab to switch on to it`, async () => {
      await this.answweredTab.click();
    });
  }

  async attendeeApplyFilterOnNewQuestionsTab(
    sortByOption: QuestionsSortingEnum
  ) {
    await test.step(`Attendee going to sort the question appearing on list by filter option ${sortByOption}`, async () => {
      await this.questionListFilterForQuestionsTab.click();
      const filterOption =
        this.eachfilterDropDownOptionForSortingQuestion.filter({
          hasText: sortByOption,
        });
      await expect(
        filterOption,
        `expecting option ${sortByOption} to be visible`
      ).toBeVisible();
      await filterOption.click();
    });
  }

  async attendeeApplyFilterOnAnswersTab(
    sortByOptionForQuestion: QuestionsSortingEnum,
    sortByOptionForViewingOptionsForAnswers: AnswersViewingSortingEnum
  ) {
    await test.step(`Attendee going to sort the question appearing on list by filter option ${sortByOptionForQuestion}`, async () => {
      await this.questionListFilterForQuestionsTab.click();
      const filterOption =
        this.eachfilterDropDownOptionForSortingQuestion.filter({
          hasText: sortByOptionForQuestion,
        });
      await expect(
        filterOption,
        `expecting option ${sortByOptionForQuestion} to be visible`
      ).toBeVisible();
      await filterOption.click();
    });

    await test.step(`Attendee going to sort the answers appearing on list by viewing filter option ${sortByOptionForViewingOptionsForAnswers}`, async () => {
      await this.questionListFilterForViewAnswers.click();
      const filterOptionForViewingAnswers =
        this.eachfilterDropDownOptionForViewingAnswers.filter({
          hasText: sortByOptionForViewingOptionsForAnswers,
        });
      await expect(
        filterOptionForViewingAnswers,
        `expecting option ${sortByOptionForViewingOptionsForAnswers} to be visible`
      ).toBeVisible();
      await filterOptionForViewingAnswers.click();
    });
  }

  async organiserApplyFilterOnQuestionsTab(
    sortByOptionForQuestion: QuestionsSortingEnum,
    sortByOptionToFilterAnsweredVsNot: OrganisersAnswersViewingSortingEnum
  ) {
    await test.step(`Organiser going to sort the question appearing on list by filter option ${sortByOptionForQuestion}`, async () => {
      await this.questionListFilterForQuestionsTab.click();
      const filterOption =
        this.eachfilterDropDownOptionForSortingQuestion.filter({
          hasText: sortByOptionForQuestion,
        });
      await expect(
        filterOption,
        `expecting option ${sortByOptionForQuestion} to be visible`
      ).toBeVisible();
      await filterOption.click();
    });

    await test.step(`Organieer going to filter the list based on its answerd or not or all ${sortByOptionToFilterAnsweredVsNot}`, async () => {
      await this.questionListFilterForViewAnswers.click();
      const filterOptionForViewingAnswers =
        this.eachfilterDropDownOptionForViewingAnswers.filter({
          hasText: sortByOptionToFilterAnsweredVsNot,
        });
      await expect(
        filterOptionForViewingAnswers,
        `expecting option ${sortByOptionToFilterAnsweredVsNot} to be visible`
      ).toBeVisible();
      await filterOptionForViewingAnswers.click();
    });
  }

  async verifyOrganiserViewOnQuestionsTab() {
    await expect(this.listOfQuestionsContainer.filter({ hasText: 'Upvotes' })).toBeVisible();
    await expect(this.listOfQuestionsContainer.filter({ hasText: 'All' })).toBeVisible();
  }

  async verifyAttendeeViewOnQuestionsTab() {
    await expect(this.newQuestionTab.filter({ hasText: 'New Questions' })).toBeVisible();
    await expect(this.answweredTab.filter({ hasText: 'Answered' })).toBeVisible();
    await expect(this.listOfQuestionsContainer.filter({ hasText: 'Upvotes' })).toBeVisible();
  }

  async clickOnAnswerLiveTab() {
    await this.answerLiveButton.click();
  }
  async clickOnNewQuestionsTab() {
    await this.newQuestionTab.click();
  }

  async clickOnAnsweredTab() {
    await this.answweredTab.click();
  }

  async clickOnUpvoteButton() {
    await this.upvoteButton.click();
  }

  async verifyVisibilityOfReplyOrAnswerLiveOrDeleteButton() {
    await expect(this.actionBarLocator.filter({ hasText: "Reply" })).toBeVisible();
    await expect(this.actionBarLocator.filter({ hasText: "Answer Live" })).toBeVisible();
    await expect(this.deleteQuestionButton).toBeVisible();
  }

  async verifyInvisibilityOfReplyOrAnswerLiveOrDeleteButton() {
    await expect(this.actionBarLocator.filter({ hasText: "Reply" })).toBeHidden();
    await expect(this.actionBarLocator.filter({ hasText: "Answer Live" })).toBeHidden();
    await expect(this.deleteQuestionButton).toBeHidden();
  }

  async verifyElementsPresentPostReply(question: string, answer: string, isOrganiser: boolean) {
    if (!isOrganiser) {
      await this.attendeeSwitchToAnsweredTab();
      expect(await this.questionAfterReplyLocator.innerText()).toEqual(question);
      await expect(this.answeredTickButton.filter({ hasText: "Answered" })).toBeVisible();
      await expect(this.actionBarLocator.filter({ hasText: "Hide Reply" })).toBeVisible();
    } else {
      expect(await this.questionAfterReplyLocator.innerText()).toEqual(question);
      await expect(this.answeredTickButton.filter({ hasText: "Answered" })).toBeVisible();
      await expect(this.actionBarLocator.filter({ hasText: "Hide Reply" })).toBeVisible();
      await expect(this.deleteQuestionButton).toBeVisible();
    }
    await expect(this.answerTitleLocator.filter({ hasText: "ANSWER" })).toBeVisible();
    await expect(this.answeredTextLocator.filter({ hasText: answer })).toBeVisible();
  }

  async verifyElementspresentPostLiveAnswer() {
    await expect(this.doneButtonToCompleteLiveAnswer).toBeVisible();
    await expect(this.deleteQuestionButton).toBeVisible();
  }

  async verifyVisibilityOfQuestion(questionText: string) {
    await this.clickOnNewQuestionsTab();
    expect(await this.questionAfterReplyLocator.innerText()).toEqual(questionText);
  }

  async getUpvoteCount() {
    return await this.upvoteCountLocator.innerText();
  }

  async verifyUpvotesButtonPresence() {
    await expect(this.upvotedButton).toBeVisible();
  }

  async clickOnDownvoteButton() {
    await this.upvotedButton.click();
    console.log("here");
    await expect(this.page.locator("//button//label[text()='Upvote']")).toBeVisible();
  }
  async verifyTimeStampOrderByLatest(expectedTimeStamp: string[]) {
    await this.questionNameList.first().waitFor({ timeout: 5000 });
    let actualquestionNames = await this.questionNameList.allInnerTexts();
    expect(actualquestionNames[0]).toEqual(expectedTimeStamp[0]);
  }

  async verifyMaxUpvoteCount(expectectedCount: number) {
    expect(Number(await this.upvoteCount2Locator.first().innerText())).toEqual(expectectedCount);
  }

  async selectFilterDropdown(value: string) {
    await this.questionFilterVoteAnswer.first().click();
    await this.page.waitForTimeout(1000);
    if (value === 'Upvotes') {
      await this.questionDropdown.first().click();
    } else {
      await this.questionDropdown.nth(1).click();
    }
    await this.page.waitForTimeout(1000);
  }

  async selectUpvoteOrLatestDropdown(value: string) {
    await this.page.waitForTimeout(1000);
    await this.questionFilterVoteAnswer.first().click();
    await this.page.waitForTimeout(1000);
    if (value === 'Upvotes') {
      await this.questionDropdown.first().click();
    } else {
      await this.questionDropdown.nth(1).click();
    }
    await this.page.waitForTimeout(1000);
  }

  async selectAnsweredOrUnAnsweredDropdown(value: string) {
    await this.questionFilterVoteAnswer.nth(1).click();
    await this.page.waitForTimeout(1000);
    if (value === 'Answered') {
      await this.questionDropdown.nth(1).click();
    } else if (value === 'Unanswered') {
      await this.questionDropdown.nth(2).click();
    } else {
      await this.questionDropdown.first().click();
    }
    await this.page.waitForTimeout(1000);
  }

  async selectAnswerWithTextOrMyQuestionsDropdown(value: string) {
    await this.questionListFilterForViewAnswers.click();
    await this.page.waitForTimeout(1000);
    if (value === 'Answered with text') {
      await this.questionDropdown.nth(1).click();
    } else if (value === 'My Questions') {
      await this.questionDropdown.nth(2).click();
    } else {
      await this.questionDropdown.first().click();
    }
    await this.page.waitForTimeout(1000);
  }


}
