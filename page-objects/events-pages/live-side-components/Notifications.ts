import { Locator, Page, expect } from "@playwright/test";
import exp from "constants";
import ErrorMessage from "../../../enums/ErrorMessageEnum";
import TextMessage from "../../../enums/TextMessageEnum";

export class NotificationToastMessages {
    readonly page: Page;
    readonly notValidQuestionLocator: Locator;
    readonly questionTitleLocator: Locator;
    readonly answeredTextLocator: Locator;
    readonly visitRoomButtonLocator: Locator;
    readonly notificationTitleLocator: Locator;


    constructor(page: Page) {
        this.page = page;
        this.notValidQuestionLocator = this.page.locator("div[class^='styles-module__messageContainer___'] p[class^='styles-module__notificationMessage___']");
        this.questionTitleLocator = this.page.locator("div[class^='styles-module__title__']");
        this.answeredTextLocator = this.page.locator("div[class^='styles-module__text___']");
        this.visitRoomButtonLocator = this.page.locator("button:has-text('Visit Room')");
        this.notificationTitleLocator = this.page.locator("div[class^='styles-module__notificationTitle']");
    }

    async verifyNotAValidQuestionMessage() {
        expect(await this.notValidQuestionLocator.innerText()).toEqual(ErrorMessage.NO_VALID_QUESTION);
        await this.page.waitForTimeout(5000);
    }

    async verifyReplyQuestionMessage(repliedText: string) {
        await expect(this.questionTitleLocator.filter({ hasText: "Answered" })).toBeVisible();
        expect(await this.answeredTextLocator.innerText()).toEqual(repliedText);
    }

    async verifyVisitRoomMessage() {
        await expect(this.visitRoomButtonLocator).toBeVisible();
    }

    async verifyUpvoteMessage() {
        expect(await this.notificationTitleLocator.innerText()).toContain(TextMessage.UPVOTE_MESSAGE);
    }
}