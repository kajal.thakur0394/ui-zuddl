import { Locator, Page, expect } from "@playwright/test";
import {
  AnnouncementDefaultZones,
  EventZones,
} from "../../../enums/EventZoneEnum";

export class AnnouncementForm {
  readonly announcementFormContainer: Locator;
  readonly announcementNoteInputBox: Locator;
  readonly redirectToDropdownLocator: Locator;
  readonly redirectToZoneLocatorDropdown: Locator;
  readonly redirectToSubZoneLocatorDropdown: Locator;
  readonly beginAnnouncementButtonLocator: Locator;
  readonly stopAnnouncementButtonLocator: Locator;
  readonly visibleAtZoneOptionContainer: Locator;
  readonly cancelButtonLocatorToClearRedirectDropdownOption: Locator;

  constructor(readonly page: Page) {
    this.page = page;
    this.announcementFormContainer = this.page.locator(
      "div[class^='styles-module__announcementContainer']"
    );
    this.announcementNoteInputBox = this.announcementFormContainer.locator(
      "textarea[class^='styles-module__announcementText']"
    );
    this.redirectToDropdownLocator = this.announcementFormContainer
      .locator("div[class^='styles-module__redirectToWrapper']")
      .filter({ hasText: "REDIRECT TO" })
      .locator("div[class$='control']");

    this.redirectToZoneLocatorDropdown = this.announcementFormContainer
      .locator("div[class^='styles-module__redirectToWrapper']")
      .filter({ hasText: "SELECT WITHIN ZONE" })
      .locator("div[class$='control']");

    this.redirectToSubZoneLocatorDropdown = this.announcementFormContainer
      .locator("div[class^='styles-module__redirectToWrapper']")
      .filter({ hasText: "SELECT ZONE" })
      .locator("div[class$='control']")
      .nth(1);

    this.beginAnnouncementButtonLocator = this.announcementFormContainer
      .locator("button")
      .filter({ hasText: "Begin Announcement" });

    this.stopAnnouncementButtonLocator = this.announcementFormContainer
      .locator("button")
      .filter({ hasText: "Begin Announcement" });
  }

  async chooseRedirectToOption(selectOption: "Internal" | "External") {
    await this.redirectToDropdownLocator.click();

    const dropdownOption = this.redirectToDropdownLocator
      .locator("div[id^='react-select']")
      .filter({
        hasText: selectOption,
      });
    await dropdownOption.click();
  }

  async chooseZoneToRedirectTo(zoneName: EventZones | string) {
    await this.redirectToZoneLocatorDropdown.click();
    const dropdownOption = this.redirectToZoneLocatorDropdown
      .locator("div[id^='react-select']")
      .filter({
        hasText: zoneName,
      });
    await dropdownOption.click();
  }

  async chooseSubZoneToRedirectTo(subZoneName: string) {
    //first check if subzone dropdown is visible
    await expect(
      this.redirectToSubZoneLocatorDropdown,
      "expecting subzone dropdown to be visible"
    ).toBeVisible();
    await this.redirectToSubZoneLocatorDropdown.click();
    const dropdownOption = this.redirectToSubZoneLocatorDropdown
      .locator("div[id^='react-select']")
      .filter({
        hasText: subZoneName,
      });
    await dropdownOption.click();
  }

  async clickOnBeginAnnouncementButton() {
    await this.beginAnnouncementButtonLocator.click();
  }

  async clickOnStopAnnouncementButton() {
    await this.stopAnnouncementButtonLocator.click();
  }

  getListOfDefaultZonesOnAnnouncementForm() {
    const listOfZones = new Array<string>();
    listOfZones.push(AnnouncementDefaultZones.LOBBY);
    listOfZones.push(AnnouncementDefaultZones.EXPO);
    listOfZones.push(AnnouncementDefaultZones.SCHEDULE);
    listOfZones.push(AnnouncementDefaultZones.ROOMS);
    listOfZones.push(AnnouncementDefaultZones.NETWORKING);
    listOfZones.push(AnnouncementDefaultZones.STAGE);
    return listOfZones;
  }

  async clickOnZoneOptionToSetAnnouncementVisibilty(zoneName: string) {
    const zoneOptionLocator = this.visibleAtZoneOptionContainer.filter({
      hasText: zoneName,
    });
    //expect for the visibility of the locator
    await expect(
      zoneOptionLocator,
      `expecting zone option with name ${zoneName} is visible for visibility options`
    ).toBeVisible();
    //click on to select it
    await zoneOptionLocator.click();
  }

  async chooseVisibleAtZoneOptions(
    selectAllDefaultZones = true,
    listOfZones?: Array<AnnouncementDefaultZones>
  ) {
    if (selectAllDefaultZones) {
      const listOfDefaultZones = this.getListOfDefaultZonesOnAnnouncementForm();
      for (const eachZone of listOfDefaultZones) {
        await this.clickOnZoneOptionToSetAnnouncementVisibilty(eachZone);
      }
    } else {
      //select the given zones
      for (const eachZone of listOfZones) {
        await this.clickOnZoneOptionToSetAnnouncementVisibilty(eachZone);
      }
    }
  }

  async setRedirectToDropDownToNone() {
    await this.cancelButtonLocatorToClearRedirectDropdownOption.click();
    //first see if an option is selected
    await expect(
      this.redirectToDropdownLocator.locator(
        "div[class*='indicatorContainer']"
      ),
      "expecting redirect dropdown has an option selected"
    ).toHaveCount(2);

    //click on the first option which will clear the option selected
    await this.redirectToDropdownLocator
      .locator("div[class*='indicatorContainer']")
      .nth(0)
      .click();
  }
}
