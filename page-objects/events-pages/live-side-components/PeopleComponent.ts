import { Locator, Page, expect } from "@playwright/test";
import exp from "constants";
export class PeopleComponent {
  readonly page: Page;
  readonly searchPeopleLocator: Locator;
  readonly privateChatIconLocator: Locator;
  readonly toolTipLocator: Locator;
  readonly toolTipLocator2: Locator;

  constructor(page: Page) {
    this.page = page;
    this.searchPeopleLocator = this.page.getByPlaceholder('Search for People');
    this.privateChatIconLocator = this.page.locator("div[class^='styles-module__iconContainer']");
    this.toolTipLocator = this.page.locator("#tooltip-disabled-chat-disabled");
    this.toolTipLocator2 = this.page.locator("div[data-for='tooltip-disabled-chat-disabled']");
  }

  async searchForPeople(name: string) {
    await this.searchPeopleLocator.fill(name);
  }

  async clickOnPrivateChatIcon(name) {
    const attendeeContainer = this.page.locator("div[class^='styles-module__attendeeListItemContainer']").filter({ hasText: name });
    await attendeeContainer.locator("div[class^='styles-module__iconContainer']").click();
  }

  async verifyHoverMessage(hoverText: string) {
    await this.toolTipLocator2.hover();
    await expect(this.toolTipLocator2).toBeVisible();
    await expect(this.toolTipLocator2).toHaveText(hoverText);
  }

}
