import { expect, Locator, Page } from "@playwright/test";
import onBoardingMessageEnum from "../../../enums/onBoardingMessageEnum";

export class OnboardingCheckComponent {
  readonly page: Page;
  readonly continueBrowser: Locator;
  readonly connectivityIssueDetected: Locator;
  readonly networkConnectionCheck: Locator;
  readonly browserNetworkMessage: Locator;

  constructor(page: Page) {
    this.page = page;
    this.continueBrowser = this.page.locator(
      'button[class^="styles-module__coloursWarningBase"]'
    );
    this.connectivityIssueDetected = this.page.locator(
      "span[class^='styles-module__text-warning']"
    );
    this.networkConnectionCheck = this.page.locator(
      "span[class^='styles-module__text-neutral-monochrome-contrast']"
    );
    this.browserNetworkMessage = this.page.locator(
      'span[class^="styles-module__text-neutral-contrast-3"]'
    );
  }

  async passNetworkBrowserCheck() {
    await expect(this.networkConnectionCheck).toContainText(
      onBoardingMessageEnum.BROWSERCHECKMESSAGE
    );
    await expect(this.networkConnectionCheck).toContainText(
      onBoardingMessageEnum.CONNECTIONCHECKMESSAGE
    );
    await expect(this.browserNetworkMessage).toContainText(
      onBoardingMessageEnum.JOINMESSAGE
    );
    await expect(this.browserNetworkMessage).toContainText(
      onBoardingMessageEnum.NETWORKCHECKMESSAGE
    );
  }

  async continueBrowserClick() {
    await this.continueBrowser.click();
  }

  async isBrowserNetworkCheckVisible() {
    await this.networkConnectionCheck.waitFor({ state: "hidden" });
    return await this.networkConnectionCheck.isHidden();
  }
}
