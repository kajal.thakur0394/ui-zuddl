import { Locator, Page, expect, test } from "@playwright/test";
import { QnAComponent } from "./qNaComponent";
import exp from "constants";

export class ModeratedQnAComponent extends QnAComponent {
  //for review tab
  readonly reviewQuestionsTab: Locator;
  readonly publishedQuestionsTab: Locator;
  readonly waitingForReviewTextLocator: Locator;
  readonly questionContainerLocator: Locator;
  readonly reviewContainerLocator: Locator;
  readonly questionNameLocator: Locator;
  readonly approveButtonLocator: Locator;
  readonly rejectButtonLocator: Locator;
  readonly selectAllCheckboxLocator: Locator;
  readonly selectAllLocator: Locator;


  // for published tab
  constructor(readonly page: Page) {
    super(page);
    //review tab
    this.reviewQuestionsTab = this.page.locator("#questionsToBeReviewed");
    this.publishedQuestionsTab = this.page.locator("#publishedQuestions");
    this.waitingForReviewTextLocator = this.page.locator("div[class^='styles-module__waitingForReviewContainer']");
    this.questionContainerLocator = this.page.locator("div[id^='qna-']");
    this.reviewContainerLocator = this.page.locator("div[class^='styles-module__reviewContainer']");
    this.questionNameLocator = this.page.locator("div[class^='styles-module__question___']");
    this.approveButtonLocator = this.page.locator("div[class^='styles-module__approveButton___']");
    this.rejectButtonLocator = this.page.locator("div[class^='styles-module__rejectButton___']");
    this.selectAllLocator = this.page.locator("div[class^='styles-module__selectAllContainer']");
    this.selectAllCheckboxLocator = this.page.locator("div[class^='styles-module__selectAllContainer'] div");
  }

  async organiserSwitchesToReviewTab() {
    await test.step(`Organiser click on review tab to switch on to it`, async () => {
      await this.reviewQuestionsTab.click();
    });
  }

  async organiserSwitchesToPublishedQuestionsTab() {
    await test.step(`Organiser click on review tab to switch on to it`, async () => {
      await this.publishedQuestionsTab.click();
    });
  }

  async verifyOrganiserViewOnQnATab() {
    await expect(this.reviewQuestionsTab).toBeVisible();
    await expect(this.publishedQuestionsTab).toBeVisible();
  }

  async verifyWaitingForReviewTextPresent(questionText: string) {
    const questionContainerToLookOut = this.questionContainerLocator.filter({
      hasText: questionText,
    });
    const questionModule = questionContainerToLookOut.locator(
      this.waitingForReviewTextLocator);
    // await expect(this.questionContainerLocator.filter({ hasText: questionText })).toBeVisible();
    expect(await questionModule.innerText()).toEqual("Waiting for review");
  }

  async verifyWaitingForReviewTextNotPresent(questionText: string) {
    const questionContainerToLookOut = this.questionContainerLocator.filter({
      hasText: questionText,
    });
    const questionModule = questionContainerToLookOut.locator(
      this.waitingForReviewTextLocator
    );
    //await expect(this.questionContainerLocator.filter({ hasText: questionText })).toBeHidden();
    await expect(questionModule).toBeHidden();
  }

  async verifyQuestionPresentInReviewTab(questionText: string) {
    const questionContainerToLookOut = this.reviewContainerLocator.filter({
      hasText: questionText,
    });
    const questionModule = questionContainerToLookOut.locator(
      this.questionNameLocator
    );
    expect(await questionModule.innerText()).toEqual(questionText);
  }

  async verifyQuestionNotPresentInReviewTab() {
    await expect(this.reviewContainerLocator).toBeHidden();
  }

  async organiserApprovesTheQuestion(questionText: string) {
    const questionContainerToLookOut = this.reviewContainerLocator.filter({
      hasText: questionText,
    });
    await questionContainerToLookOut.locator(this.approveButtonLocator).click()
  }

  async organiserRejectsTheQuestion(questionText: string) {
    const questionContainerToLookOut = this.reviewContainerLocator.filter({
      hasText: questionText,
    });
    await questionContainerToLookOut.locator(this.rejectButtonLocator).click()
  }

  async clickOnPublishedTab() {
    await this.publishedQuestionsTab.click();
  }

  async clickOnReviewTab() {
    await this.reviewQuestionsTab.click();
  }

  async verifyQuestionPresentInPublishedTab(questionText: string) {
    const questionContainerToLookOut = this.questionContainerLocator.filter({
      hasText: questionText,
    });
    const questionModule = questionContainerToLookOut.locator(
      this.questionNameLocator
    );
    expect(await questionModule.innerText()).toEqual(questionText);
  }

  async verifyQuestionNotPresentInPublishedTab() {
    await expect(this.questionContainerLocator).toBeHidden();
  }

  async verifyQuestionNotPresentInNewQuestionsTab() {
    await expect(this.questionContainerLocator).toBeHidden();
  }

  async verifyQuestionNotPresentInAnsweredTab() {
    await this.clickOnAnsweredTab()
    await expect(this.questionContainerLocator).toBeHidden();
  }

  async verifySelectAllChecboxPresent() {
    await expect(this.selectAllLocator).toBeVisible();
  }

  async clickOnSelectCheckbox() {
    await this.selectAllCheckboxLocator.click();
  }

}
