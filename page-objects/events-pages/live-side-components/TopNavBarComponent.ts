import { Locator, Page, expect } from "@playwright/test";
import { ExpoPage } from "../zones/ExpoPage";
import { NetworkingPage } from "../zones/NetworkingPage";
import { RoomsListingPage } from "../zones/RoomsPage";
import { SchedulePage } from "../zones/SchedulePage";
import { StagePage } from "../zones/StagePage";
import { EditProfilePage } from "./EditProfilePage";
import ZoneType from "../../../enums/ZoneTypeEnum";

export class TopNavBarComponent {
  readonly page: Page;
  readonly userProfileIcon: Locator;
  readonly editProfileOption: Locator;
  readonly logOutOption: Locator;
  //top nav bar zone tabs
  readonly scheduleZoneTab: Locator;
  readonly stageZoneTab: Locator;
  readonly roomsZoneTab: Locator;
  readonly expoZoneTab: Locator;
  readonly networkingZoneTab: Locator;
  readonly zoneListLiveEvent: Locator;
  readonly lobbyZoneTab: Locator;

  constructor(page: Page) {
    this.page = page;
    this.userProfileIcon = this.page.locator(
      "div[data-testid='nav_bar_options_dropdown']"
    );
    this.editProfileOption = this.page
      .locator("div[class^='styles-module__navBarDropdownOption']")
      .filter({ hasText: "Edit profile" });
    this.logOutOption = this.page
      .locator("div[class^='styles-module__navBarDropdownOption']")
      .filter({ hasText: "Logout" });
    // zones tab locator
    this.scheduleZoneTab = this.page.locator("a#navbar-menu-schedule");
    this.stageZoneTab = this.page.locator("a#navbar-menu-stage");
    this.roomsZoneTab = this.page.locator("a#navbar-menu-rooms");
    this.expoZoneTab = this.page.locator("a#navbar-menu-expo");
    this.lobbyZoneTab = this.page.locator("a#navbar-menu-lobby");

    this.networkingZoneTab = this.page.locator("a#navbar-menu-networking");
    this.zoneListLiveEvent = this.page.locator(
      "a[data-testid^='navbar-menu-']"
    );
  }

  async openEditProfile(skipToolTip = true) {
    if (skipToolTip) {
      await this.page.click("text=Skip", { timeout: 20000 });
    }
    await this.userProfileIcon.click();

    await this.editProfileOption.click();
    return new EditProfilePage(this.page);
  }

  async logoutFromEvent(skipToolTip = true) {
    if (skipToolTip) {
      await this.page.click("text=Skip", { timeout: 20000 });
    }
    await this.userProfileIcon.click();
    await this.logOutOption.click();
  }

  // top nav bar zones
  async switchToZone(zoneName: ZoneType) {
    switch (zoneName) {
      case ZoneType.SCHEDULE:
        await this.scheduleZoneTab.click();
        return new SchedulePage(this.page);
      case ZoneType.STAGE:
        await this.stageZoneTab.click();
        return new StagePage(this.page);
      case ZoneType.ROOMS:
        await this.roomsZoneTab.click();
        return new RoomsListingPage(this.page);
      case ZoneType.EXPO:
        await this.expoZoneTab.click();
        return new ExpoPage(this.page);
      case ZoneType.NETWORKING:
        await this.networkingZoneTab.click();
        return new NetworkingPage(this.page);
      default:
        await this.lobbyZoneTab.click();
        break;
    }
  }
  async verifyZoneDisabledonLiveEvent(zoneName: ZoneType) {
    const zoneLink = this.zoneListLiveEvent.filter({ hasText: zoneName });
    console.log(zoneLink);
    if ((await this.zoneListLiveEvent.count()) === 5) {
      //Locator not available in DOM
      expect(await zoneLink.count()).toEqual(0);
    } else {
      await expect(zoneLink).toBeHidden(); //if locator is present in DOM, but marked as hidden
    }
  }
  async verifyZoneEnabledeonLiveEvent(zoneName: ZoneType) {
    const zoneLink = this.zoneListLiveEvent.filter({ hasText: zoneName });
    await expect(zoneLink).toBeVisible();
  }
}
