import { expect, Locator, Page, test } from "@playwright/test";
export class ChatComponent {
  readonly page: Page;
  // chat
  readonly inputBoxForChat: Locator;
  readonly sendIconOnChat: Locator;
  readonly chatBoxContainer: Locator;
  //message properties
  readonly chatMessageContent: Locator;
  readonly messageDiv: Locator;
  readonly messageSenderName: Locator;
  readonly messageSenderLabel: Locator;
  readonly messageSentTime: Locator;
  readonly messageContent: Locator;
  readonly arrowIconOnSidePanel: Locator;
  readonly emptyChatContainerLocator: Locator;

  //pinned chat components
  readonly menuOptionsOnMessage: string;
  readonly pinMessageOptionLocator: Locator;
  readonly unpinMessageOptionLocator: Locator;
  readonly deleteMessageOptionLocator: Locator;
  readonly pinnedChatContainer: Locator;
  readonly expandOrCollapseButtonOnPinnedChat: Locator;
  readonly messageDivLocatorInPinnedChatLocator: Locator;
  readonly pinnedChatCountIndicatorLocator: Locator;
  readonly;

  //content chat components
  readonly profileNameText: Locator;
  readonly senderName: Locator;
  readonly senderLabel: Locator;
  readonly senderTimeStamp: Locator;

  //tabs
  readonly backstageChatTab: Locator;
  readonly stageChatTab: Locator; //event chat
  readonly eventTab: Locator

  //moderated chat button
  readonly moderatedChatButton: Locator;
  readonly moderatedChatListContainer: Locator;
  readonly moderatedMessageList: Locator;
  readonly eachModeratedMessageCardContainer: Locator;
  readonly approveButtonLocator: Locator;
  readonly rejectButtonLocator: Locator;

  constructor(page: Page) {
    this.page = page;
    // chat components
    this.eventTab = this.page.locator("#EVENT");

    this.inputBoxForChat = this.page.locator(
      "input[class^='styles-module__chatInputField']"
    );
    this.sendIconOnChat = this.page.locator(
      "button[data-testid='chat-input-send-button']"
    );
    this.chatBoxContainer = this.page.locator(
      "div[data-testid='chatBoxContainer']"
    );
    this.emptyChatContainerLocator = this.page.locator(
      "div[data-testid='chatBoxContainer'] div[class^='styles-module__emptyChatState']"
    );
    //message components
    this.messageDiv = this.page.locator(
      "div[data-testid='chatBoxContainer'] div[class^='styles-module__container']"
    );
    this.chatMessageContent = this.page.locator(
      "div[class^='styles-module__contentBody'] div[class^='styles-module__content'], div[class^='styles-module__content']"
    );

    // tab
    this.backstageChatTab = this.page.locator("#privateChat");
    this.stageChatTab = this.page.locator("#publicChat");

    //pin chat components
    this.pinMessageOptionLocator = this.page.locator("text=Pin Message");
    this.unpinMessageOptionLocator = this.page.locator("text=Unpin Message");
    this.deleteMessageOptionLocator = this.page.locator("text=Delete");
    this.pinnedChatContainer = this.page.locator(
      "div[class^='styles-module__pinnedChatContainer']"
    );
    this.expandOrCollapseButtonOnPinnedChat = this.pinnedChatContainer.locator(
      "div[class^='styles-module__expandCollapseButton']"
    );
    this.messageDivLocatorInPinnedChatLocator =
      this.pinnedChatContainer.locator(
        "div[class^='styles-module__contentBody']"
      );
    this.menuOptionsOnMessage = "div[class^='styles-module__menuDropdown']";
    this.pinnedChatCountIndicatorLocator = this.page.locator(
      "div[class^='styles-module__pinnedCountContainer']"
    );

    //content chat components
    this.profileNameText = this.page.locator(
      "div[class^='styles-module__chatProfilePic_']"
    );
    this.senderName = this.page.locator(
      "div[class^='styles-module__senderName'] span"
    );
    this.senderLabel = this.page.locator(
      "span[class^='styles-module__text-success']"
    );
    this.senderTimeStamp = this.page.locator(
      "div[class^='styles-module__time___X-oXs'] span"
    );

    //moderated chat
    this.moderatedChatButton = this.page.locator(
      "div[class^='styles-module__moderatButtonContainer']"
    );
    this.moderatedChatListContainer = this.page.locator(
      "div[data-testid='Approve-Message-List']"
    );
    this.moderatedMessageList = this.moderatedChatListContainer.locator(
      "div[class^='styles-module__messagesListContainer']"
    );
    this.eachModeratedMessageCardContainer = this.moderatedMessageList.locator(
      "div[class^='styles-module__cardContainer']"
    );

    this.approveButtonLocator = this.page.locator("button:has-text('Approve')");
    this.rejectButtonLocator = this.page.locator("button:has-text('Reject')");
  }

  async sendChatMessage(messageToSend: string) {
    let postMessageApiPromise = this.page.waitForResponse(/message/);
    await this.inputBoxForChat.fill(messageToSend);
    await this.sendIconOnChat.click();
    let postMessageApiResp = await postMessageApiPromise;
    expect(postMessageApiResp.status()).toBe(200);
  }

  getCountOfTotalMessageVisibleInChatContainer() {
    return this.messageDiv;
  }

  async verifyCountOfMessageMatches(expectedMessageCount: number) {
    await expect(
      this.messageDiv,
      `expecting msg count in main container to be ${expectedMessageCount}`
    ).toHaveCount(expectedMessageCount);
  }

  async verifyMessageWithGivenContentIsVisible(expectedMessageContent: string) {
    const expectedMessage = this.messageDiv
      .filter({ has: this.chatMessageContent })
      .filter({ hasText: expectedMessageContent });
    await expect(expectedMessage).toBeVisible({ timeout: 15000 });
  }

  async verifyMessageWithGivenContentIsNotVisible(
    expectedMessageContent: string
  ) {
    const expectedMessage = this.messageDiv
      .filter({ has: this.chatMessageContent })
      .filter({ hasText: expectedMessageContent });
    await expect(expectedMessage).not.toBeVisible({ timeout: 15000 });
  }

  async openStageChatTab() {
    await this.stageChatTab.click();
  }

  async openBackStageChatTab() {
    await this.backstageChatTab.click();
  }

  //pinned chat message actions
  async pinTheChatMessage(messageContent: string) {
    const messageDivLocator = this.messageDiv.filter({
      hasText: messageContent,
    });
    await messageDivLocator.hover();
    const menuLocatorOnMessage = messageDivLocator.locator(
      this.menuOptionsOnMessage
    );
    await expect(
      menuLocatorOnMessage,
      `expecting menu dropdown to be visible when hover over message with content ${messageContent}`
    ).toBeVisible();

    await menuLocatorOnMessage.click();
    await this.pinMessageOptionLocator.click();
  }

  async unpinTheChatMessage(messageContent: string) {
    //verify expected chat is visible in pinned section
    await expect(
      this.pinnedChatContainer.filter({ hasText: messageContent })
    ).toBeVisible();
    const messageDivLocator = this.messageDiv.filter({
      hasText: messageContent,
    });
    await messageDivLocator.hover();
    const menuLocatorOnMessage = messageDivLocator.locator(
      this.menuOptionsOnMessage
    );
    await expect(
      menuLocatorOnMessage,
      `expecting menu dropdown to be visible when hover over message with content ${messageContent}`
    ).toBeVisible();
    await menuLocatorOnMessage.click();
    await expect(
      this.unpinMessageOptionLocator,
      "expecting unpin option to be visible after clickin on menu"
    ).toBeVisible();
    await this.unpinMessageOptionLocator.click();
  }

  async deleteMessageFromMainMessageContainer(messageContent: string) {
    const messageDivLocator = this.messageDiv.filter({
      hasText: messageContent,
    });
    await messageDivLocator.hover();
    const menuLocatorOnMessage = messageDivLocator.locator(
      this.menuOptionsOnMessage
    );
    await expect(
      menuLocatorOnMessage,
      `expecting menu dropdown to be visible when hover over message with content ${messageContent}`
    ).toBeVisible();
    await menuLocatorOnMessage.click();
    await this.deleteMessageOptionLocator.click();
  }

  async clickToExpandOrCollapsePinnedChatSection() {
    await this.expandOrCollapseButtonOnPinnedChat.click();
  }

  async verifyIfPinnedChatContainerContainsExpectedNumberOfMessages(
    expectedCount: number
  ) {
    await expect(
      this.messageDivLocatorInPinnedChatLocator,
      `expecting msg count to be ${expectedCount} in pinned chat container`
    ).toHaveCount(expectedCount);
  }

  async verifyMessageWithGivenContentIsVisibleInPinnedContainer(
    expectedMessageContent: string
  ) {
    let isMessageFound = false;
    const totalMessageCount =
      await this.messageDivLocatorInPinnedChatLocator.count();

    for (let i = 0; i < totalMessageCount; i++) {
      const fetchedMessageContent =
        await this.messageDivLocatorInPinnedChatLocator.nth(i).textContent();
      if (fetchedMessageContent?.includes(expectedMessageContent)) {
        isMessageFound = true;
        break;
      }
    }
    return isMessageFound;
  }

  async verifyPinOptinIsDisabledForThisMessage(messageContent: string) {
    const messageDivLocator = this.messageDiv.filter({
      hasText: messageContent,
    });
    await messageDivLocator.hover();
    const menuLocatorOnMessage = messageDivLocator.locator(
      this.menuOptionsOnMessage
    );
    await expect(
      menuLocatorOnMessage,
      `expecting menu dropdown to be visible when hover over message with content ${messageContent}`
    ).toBeVisible();
    await menuLocatorOnMessage.click();
    await this.pinMessageOptionLocator.hover();
    await expect(
      this.page.locator("text=Only 3 messages can be pinned at one time")
    ).toBeVisible();
  }

  async clickOnEventTab() {
    await this.eventTab.click();
  }

  async verifyEmptyChatContainerIsVisible() {
    await expect(
      this.emptyChatContainerLocator,
      "expecting empty chat container state is visible"
    ).toBeVisible();
  }

  async verifyPinChatOptionVisiblityForMessageByContent(
    messageContent: string,
    expectingToBeVisible = true
  ) {
    const messageDivLocator = this.messageDiv.filter({
      hasText: messageContent,
    });
    await messageDivLocator.hover();
    const menuOptionsOnMessage = messageDivLocator.locator(
      this.menuOptionsOnMessage
    );
    await expect(
      menuOptionsOnMessage,
      "expecting menu options dropdown to be visible"
    ).toBeVisible();
    await menuOptionsOnMessage.click();
    if (expectingToBeVisible) {
      await expect(
        this.pinMessageOptionLocator,
        `expecting pin option to  be visible in dropdown`
      ).toBeVisible();
    } else {
      await expect(
        this.pinMessageOptionLocator,
        `expecting pin option to not be visible in dropdown`
      ).not.toBeVisible();
    }
  }
  async verifyPinnedChatCountIndicatorMatches(expectedNumber: number) { }

  async verifyPinChatOptionVisiblityForMessageByIndex(
    messageIndex: number,
    expectingToBeVisible = true
  ) {
    const messageDivLocator = this.messageDiv.nth(messageIndex);
    await messageDivLocator.hover();
    const menuOptionsOnMessage = messageDivLocator.locator(
      this.menuOptionsOnMessage
    );
    await expect(
      menuOptionsOnMessage,
      "expecting menu options dropdown to be visible"
    ).toBeVisible();
    await menuOptionsOnMessage.click();
    if (expectingToBeVisible) {
      await expect(
        this.pinMessageOptionLocator,
        `expecting pin option to  be visible in dropdown`
      ).toBeVisible();
    } else {
      await expect(
        this.pinMessageOptionLocator,
        `expecting pin option to not be visible in dropdown`
      ).not.toBeVisible();
    }
  }

  async fetchAttendeeContent(): Promise<{ [key: string]: string }> {
    let attendeeContentDetails: { [key: string]: string } = {};
    attendeeContentDetails.profileName =
      (await this.profileNameText.first().textContent()) ?? "";
    attendeeContentDetails.senderName =
      (await this.senderName.first().textContent()) ?? "";
    attendeeContentDetails.senderTimeStamp =
      (await this.senderTimeStamp.first().textContent()) ?? "";
    return attendeeContentDetails;
  }

  async fetchOrganiserContent(): Promise<{ [key: string]: string }> {
    let organiserContentDetails: { [key: string]: string } = {};
    organiserContentDetails.profileName =
      (await this.profileNameText.last().textContent()) ?? "";
    organiserContentDetails.senderName =
      (await this.senderName.last().textContent()) ?? "";
    organiserContentDetails.senderLabel =
      (await this.senderLabel.textContent()) ?? "";
    organiserContentDetails.senderTimeStamp =
      (await this.senderTimeStamp.last().textContent()) ?? "";
    return organiserContentDetails;
  }
  async verifyAllAttendeeContentsVisibleInPinnedContainer(attendeeContentDetails: {
    [key: string]: string;
  }) {
    await expect(
      await this.profileNameText.first().textContent(),
      `expecting attendee profile name to be ${attendeeContentDetails["profileName"]}`
    ).toEqual(attendeeContentDetails["profileName"]);

    await expect(
      await this.senderName.first().textContent(),
      `expecting attendee sender name to be ${attendeeContentDetails["senderName"]}`
    ).toEqual(attendeeContentDetails["senderName"]);

    await expect(
      await this.senderTimeStamp.first().textContent(),
      `expecting attendee sender timestamp to be ${attendeeContentDetails["senderTimeStamp"]}`
    ).toEqual(attendeeContentDetails["senderTimeStamp"]);
  }

  async verifyAllOrganiserContentsVisibleInPinnedContainer(organiserContentDetails: {
    [key: string]: string;
  }) {
    await expect(
      await this.profileNameText.nth(1).textContent(),
      `expecting attendee profile name to be ${organiserContentDetails["profileName"]}`
    ).toEqual(organiserContentDetails["profileName"]);

    await expect(
      await this.senderName.nth(1).textContent(),
      `expecting attendee sender name to be ${organiserContentDetails["senderName"]}`
    ).toEqual(organiserContentDetails["senderName"]);

    await expect(
      await this.senderLabel.first().textContent(),
      `expecting attendee sender name to be ${organiserContentDetails["senderLabel"]}`
    ).toEqual(organiserContentDetails["senderLabel"]);

    await expect(
      await this.senderTimeStamp.nth(1).textContent(),
      `expecting attendee sender timestamp to be ${organiserContentDetails["senderTimeStamp"]}`
    ).toEqual(organiserContentDetails["senderTimeStamp"]);
  }

  async verifyPinChatUIComponenttobeVisible() {
    await expect(
      this.pinnedChatContainer,
      `expecting pinned chat UI component to be visible`
    ).toBeVisible();
  }

  async verifyPinChatUIComponenttobeInvisible() {
    await expect(
      this.pinnedChatContainer,
      `expecting pinned chat UI component to be invisible`
    ).toBeHidden();
  }
  async verifyPinMessageComponentTobeInvisible(messageContent: string) {
    const messageDivLocator = this.messageDiv.filter({
      hasText: messageContent,
    });
    await messageDivLocator.hover();
    const menuLocatorOnMessage = messageDivLocator.locator(
      this.menuOptionsOnMessage
    );
    await expect(
      menuLocatorOnMessage,
      `expecting menu dropdown to be invisible when hover over message with content ${messageContent}`
    ).toBeHidden();
  }

  async verifyPinMessageComponentTobeVisible(messageContent: string) {
    const messageDivLocator = this.messageDiv.filter({
      hasText: messageContent,
    });
    await messageDivLocator.hover();
    const menuLocatorOnMessage = messageDivLocator.locator(
      this.menuOptionsOnMessage
    );
    await expect(
      menuLocatorOnMessage,
      `expecting menu dropdown to be visible when hover over message with content ${messageContent}`
    ).toBeVisible();
  }

  async verifyModeratedChatButtonIsVisibleOnChatContainer() {
    await expect(this.moderatedChatButton).toBeVisible();
  }

  async verifyModeratedChatButtonIsNotVisibleOnChatContainer() {
    await expect(this.moderatedChatButton).not.toBeVisible();
  }

  async updateStatusOfChatWaitingForModeration(
    chatMessageContent: string,
    toApprove = true
  ) {
    await test.step(`Click on moderated chat button`, async () => {
      await this.moderatedChatButton.click();
    });

    await test.step(`Verify moderated chat container is visible with list of messages to approve`, async () => {
      await expect(this.moderatedChatListContainer).toBeVisible();
    });

    if (toApprove) {
      await test.step(`Verify chat message is visible inside list for approval with content : ${chatMessageContent} and then org approves it `, async () => {
        const moderatedMessageCard =
          this.eachModeratedMessageCardContainer.filter({
            hasText: chatMessageContent,
          });

        await expect(
          moderatedMessageCard,
          `expecting chat message with content ${chatMessageContent} to be visible in list of moderated message`
        ).toBeVisible();

        const approvalButton = moderatedMessageCard.locator(
          this.approveButtonLocator
        );

        await expect(approvalButton).toBeVisible();

        await approvalButton.click();
      });
    } else {
      await test.step(`Verify chat message is visible inside list for rejection with content : ${chatMessageContent} and then org rejects it `, async () => {
        const moderatedMessageCard =
          this.eachModeratedMessageCardContainer.filter({
            hasText: chatMessageContent,
          });

        await expect(
          moderatedMessageCard,
          `expecting chat message with content ${chatMessageContent} to be visible in list of moderated message`
        ).toBeVisible();

        const rejectionButton = moderatedMessageCard.locator(
          this.rejectButtonLocator
        );

        await expect(rejectionButton).toBeVisible();

        await rejectionButton.click();
      });
    }

    await test.step(`Click on go back button to go back to main chat contianer`, async () => {
      await this.moderatedChatListContainer
        .locator("div[class^='styles-module__backIcon']")
        .click();
    });
  }
}
