import { expect, Locator, Page, FrameLocator } from "@playwright/test";
import { ChatComponent } from "./ChatComponent";
import { JoinRequestTab } from "./JoinRequestTab";
import { PeopleComponent } from "./PeopleComponent";
import { PollsComponent } from "./PollsComponent";
import { QnAComponent } from "./qNaComponent";
import { ModeratedQnAComponent } from "./moderatedQnAcomponent";
import { NotificationToastMessages } from "./Notifications";

export class InteractionPanel {
  readonly page: Page;
  //closed interaction panel icons
  readonly chatButtonOnClosedInteractionPanel: Locator;
  readonly peopleButtonOnClosedInteractionPanel: Locator;
  //open interaction panel icons
  readonly chatTabOnOpenSidePanel: Locator;
  readonly peopleTabOnOpenSidePanel: Locator;
  readonly qnaTabOnOpenSidePanel: Locator;
  // close interaction panel
  readonly arrowIconOnSidePanel: Locator;

  readonly joinTabOnOpenSidePanel: Locator;

  readonly sideBarIframeInStudio: FrameLocator;
  readonly joinTabOnOpenSidePanelInStudio: Locator;
  // chat section and people components
  private chatComponent: ChatComponent;
  private peopleComponent: PeopleComponent;
  private qnaComponent: QnAComponent;
  private getModeratedQnaComponent: ModeratedQnAComponent;
  //polls tab
  readonly pollsTabOnSidePanel: Locator;

  readonly sideInteractionPanelLocator: Locator;

  readonly pollsComponent: PollsComponent;
  readonly notifications: NotificationToastMessages;

  constructor(page: Page) {
    this.page = page;
    // closed interaction panel button
    this.chatButtonOnClosedInteractionPanel = this.page.locator(
      "button[data-for='icon-button-tooltip-chat']"
    );
    this.peopleButtonOnClosedInteractionPanel = this.page.locator(
      "button[data-for='icon-button-tooltip-people']"
    );
    // open state  interaction panel button
    this.chatTabOnOpenSidePanel = this.page
      .locator("#ZONE_CHAT")
      .getByRole("tab", { name: "chat" })

      .filter({ hasText: "chat" });
    this.peopleTabOnOpenSidePanel = this.page
      .getByRole("tab", { name: "people" })
      .first();
    this.qnaTabOnOpenSidePanel = this.page
      .locator("#ZONE_QNA")
      .getByRole("tab", { name: "q&a" });
    //arrow icon to close side panel
    this.arrowIconOnSidePanel = this.page.locator(
      "div[class*='styles-module__sidePanelCloseArrowBorder']"
    );
    this.sideInteractionPanelLocator = this.page.locator(
      "div[class^='styles-module__sidePanelContentContainer']"
    );
    this.joinTabOnOpenSidePanel = this.page
      .getByRole("tab", { name: "join" })
      .locator("div");

    this.sideBarIframeInStudio = this.page.frameLocator(
      "div[class^='sidebar-navigated'] iframe"
    );
    this.joinTabOnOpenSidePanelInStudio = this.sideBarIframeInStudio
      .getByRole("tab", { name: "join" })
      .locator("div");

    //components initialisation
    this.chatComponent = new ChatComponent(this.page);
    this.peopleComponent = new PeopleComponent(this.page);
    this.qnaComponent = new ModeratedQnAComponent(this.page);
    this.getModeratedQnaComponent = new ModeratedQnAComponent(this.page);
    //polls tab
    this.pollsTabOnSidePanel = this.page
      .locator("#ZONE_POLL")
      .getByRole("tab", { name: "polls" });
    //POLLS component
    this.pollsComponent = new PollsComponent(this.page);
    this.notifications = new NotificationToastMessages(this.page);
  }

  //getters
  get getChatComponent() {
    return this.chatComponent;
  }

  get getPeopleComponent() {
    return this.peopleComponent;
  }

  get getqNaComponent() {
    return this.qnaComponent;
  }

  get getModeratedqNaComponent() {
    return this.getModeratedQnaComponent;
  }
  get getNotificationsComponent() {
    return this.notifications;
  }
  // actions
  async isInteractionPanelClosed() {
    try {
      await expect(
        this.chatButtonOnClosedInteractionPanel,
        `expecting interaction panel to be in closed state`
      ).toBeVisible();
      return true;
    } catch (err) {
      return false;
    }
  }

  async isInteractionPanelOpen() {
    try {
      await expect(
        this.chatTabOnOpenSidePanel,
        `expecting interaction panel to be in open state`
      ).toBeVisible();
      return true;
    } catch (err) {
      return false;
    }
  }
  async verifyIneractionPanelIsClosed() {
    await expect(
      this.chatButtonOnClosedInteractionPanel,
      `expecting chat icon to be visible on closed interaction panel state`
    ).toBeVisible();
  }
  // closed interaction panel actions
  async clickOnChatIconOfClosedInteractionPanelState() {
    await this.chatButtonOnClosedInteractionPanel.click();
    return this.pollsComponent;
  }
  async clickOnPeopleIconOfClosedInteractionPanelState() {
    await this.peopleButtonOnClosedInteractionPanel.click();
  }
  // action on open state of interaction panel
  async clickOnPeopleTabOnInteractionPanel() {
    await this.peopleTabOnOpenSidePanel.click();
    return this.peopleComponent;
  }
  async clickOnChatTabOnInteractionPanel() {
    await this.chatTabOnOpenSidePanel.click();
    return this.chatComponent;
  }
  async clickOnQnATabOnInteractionPanel() {
    await this.qnaTabOnOpenSidePanel.click();
    return this.qnaComponent;
  }

  async openJoinTabOnInteraactionPanel() {
    await this.joinTabOnOpenSidePanel.click();
    return new JoinRequestTab(this.page);
  }

  async openJoinTabOnInteraactionPanelStudio() {
    await this.joinTabOnOpenSidePanelInStudio.click();
    return new JoinRequestTab(this.page);
  }

  // close the interaction panel
  async closeInteractionPanel() {
    await this.arrowIconOnSidePanel.click();
  }

  async isInteractionPanelOpenAndVisible() {
    await expect(this.sideInteractionPanelLocator).toBeVisible();
  }

  // open poll tab
  async openPollsTab() {
    await this.pollsTabOnSidePanel.click();
    return this.pollsComponent;
  }

  get getPollComponent() {
    return this.pollsComponent;
  }
}
