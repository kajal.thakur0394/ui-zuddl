import { expect, FrameLocator, Locator, Page } from "@playwright/test";

export class JoinRequestTab {
  readonly page: Page;
  readonly sendStageJoinReqButton: Locator;
  readonly waitingForStageApprovalButton: Locator;
  readonly videoPreviewStreamOnJoinRequest: Locator;
  readonly buttonToMuteAudioOnStream: Locator;
  readonly buttonToUnmuteAudioOnStream: Locator;
  readonly buttonToMuteVideoOnStream: Locator;
  readonly buttonToUnmuteVideoOnSteam: Locator;
  readonly joinStageButtton: Locator;
  readonly exitStageButton: Locator;
  readonly attendeeStageVideoContainer: Locator;

  //organiser view
  readonly joinRequestContainerLocator: Locator;
  readonly acceptRequestButtonLocatorString: string;
  readonly rejectRequestButtonLocatorString: string;
  readonly removeFromStageLocatorString: string;

  //organiser view studio
  readonly sideBarIframeStudio: FrameLocator;
  readonly joinRequestContainerLocatorStudio: Locator;
  readonly acceptRequestButtonLocatorStringStudio: string;
  readonly rejectRequestButtonLocatorStringStudio: string;
  readonly removeFromStageLocatorStringStudio: string;

  constructor(page: Page) {
    this.page = page;
    this.sendStageJoinReqButton = this.page.locator(
      "div[class^='styles-module__sendRequestButtonContainer']"
    );
    this.waitingForStageApprovalButton = this.page.locator(
      "div[class^='styles-module__awaitApprovalButtonContainer']"
    );
    this.joinStageButtton = this.page.locator(
      "div[class^='styles-module__joinStageButtonContainer']"
    );
    this.exitStageButton = this.page.locator(
      "div[class^='styles-module__exitStageButtonContainer']"
    );
    this.videoPreviewStreamOnJoinRequest = this.page.locator(
      "#preview-stream video"
    );
    this.buttonToMuteAudioOnStream = this.page.locator(
      "button[data-for='icon-button-tooltip-mic']"
    );
    this.buttonToUnmuteAudioOnStream = this.page.locator(
      "button[data-for='icon-button-tooltip-micOff']"
    );
    this.buttonToMuteVideoOnStream = this.page.locator(
      "button[data-for='icon-button-tooltip-video']"
    );
    this.buttonToUnmuteVideoOnSteam = this.page.locator(
      "button[data-for='icon-button-tooltip-videoOff']"
    );
    //organiser view

    this.joinRequestContainerLocator = this.page.locator(
      "div[class*='styles-module__requestTile']"
    );

    this.acceptRequestButtonLocatorString =
      "div[class^='styles-module__acceptButton']";

    this.rejectRequestButtonLocatorString =
      "div[class^='styles-module__rejectButton']";

    this.removeFromStageLocatorString = "text=Remove from Stage";

    //using framelocator to get the iframe container
    this.sideBarIframeStudio = this.page.frameLocator(
      "div[class^='sidebar-navigated'] iframe"
    );

    this.joinRequestContainerLocatorStudio = this.sideBarIframeStudio.locator(
      "div[class*='styles-module__requestContainer']"
    );
  }

  async clickOnSendStageJoinRequest() {
    await this.sendStageJoinReqButton.click();
  }

  async isVideoStreamLoaded() {
    await expect(
      this.videoPreviewStreamOnJoinRequest,
      "expecting video stream preview to be loaded"
    ).toBeVisible();
  }

  async muteMyAudio() {
    await this.buttonToMuteAudioOnStream.click();
  }

  async unmuteMyAudio() {
    await this.buttonToUnmuteAudioOnStream.click();
  }

  async muteMyVideo() {
    await this.buttonToMuteVideoOnStream.click();
  }

  async unmuteMyVideo() {
    await this.buttonToMuteVideoOnStream.click();
  }

  async isWaitingForApprovalButtonIsVisible() {
    await expect(
      this.waitingForStageApprovalButton,
      "expecting waiting for approval button to appear of join request to attendee"
    ).toBeVisible();
  }

  //appears to attendee side
  async clickOnJoinStageButton() {
    await this.joinStageButtton.click();
  }

  getJoinRequestContainerOfAttendee(attendeeName: string) {
    return this.joinRequestContainerLocator.filter({ hasText: attendeeName });
  }

  async getJoinRequestContainerOfAttendeeStudio(attendeeName: string) {
    return this.joinRequestContainerLocatorStudio.filter({
      hasText: attendeeName,
    });
  }

  async approveAttendeeJoinRequest(attendeeName: string) {
    const attendeeJoinReqContainer =
      this.getJoinRequestContainerOfAttendee(attendeeName);
    await attendeeJoinReqContainer
      .locator(this.acceptRequestButtonLocatorString)
      .click();
  }

  async approveAttendeeJoinRequestStudio(attendeeName: string) {
    const attendeeJoinReqContainer = this.joinRequestContainerLocatorStudio
      .locator("div[class^='styles-module__requestTile']")
      .filter({ hasText: attendeeName });

    await expect(
      attendeeJoinReqContainer,
      `expecting attendee : ${attendeeName} request to be visible`
    ).toBeVisible();

    const acceptButtonOnReqTile = attendeeJoinReqContainer.locator(
      "div[class^='styles-module__acceptButton']"
    );

    await expect(
      acceptButtonOnReqTile,
      "expecting accept button to be visible"
    ).toBeVisible();
    await acceptButtonOnReqTile.click();
  }

  async rejectAttendeeJoinRequest(attendeeName: string) {
    const attendeeJoinReqContainer =
      this.getJoinRequestContainerOfAttendee(attendeeName);
    await attendeeJoinReqContainer
      .locator(this.rejectRequestButtonLocatorString)
      .click();
  }

  async removeAttendeeFromStage(attendeeName: string) {
    const attendeeJoinReqContainer =
      this.getJoinRequestContainerOfAttendee(attendeeName);
    await attendeeJoinReqContainer
      .locator(this.removeFromStageLocatorString)
      .click();
  }

  async removeAttendeeFromStageStudio(attendeeName: string) {
    const attendeeJoinReqContainer = this.joinRequestContainerLocatorStudio
      .locator("div[class*='styles-module__requestTile']")
      .filter({ hasText: attendeeName });

    await expect(
      attendeeJoinReqContainer,
      `expecting attendee : ${attendeeName} request to be visible`
    ).toBeVisible();

    const removeButtonOnReqTile = attendeeJoinReqContainer.locator(
      "div[class^='styles-module__completeButton']"
    );
  }
}
