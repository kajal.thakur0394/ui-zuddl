import { Locator, Page, WebSocket, expect } from "@playwright/test";

export class PublishedAnnouncementComponent {
  readonly publishedAnnouncementContainer: Locator;
  readonly takeMeButtonOnPublishedAnnouncement: Locator;
  readonly cancelButtonOnPublishedAnnouncement: Locator;
  readonly contentDivLocatorOnAnnouncement: Locator;
  constructor(readonly page: Page) {
    this.page = page;
    this.publishedAnnouncementContainer = this.page.locator(
      "div[class^='styles-module__announcementBar_']"
    );
    this.takeMeButtonOnPublishedAnnouncement =
      this.publishedAnnouncementContainer.locator("button:has-text('Take me')");
    this.cancelButtonOnPublishedAnnouncement =
      this.publishedAnnouncementContainer.locator(
        "div[data-testid='cross-icon']"
      );
    this.contentDivLocatorOnAnnouncement =
      this.publishedAnnouncementContainer.locator(
        "div[class^='styles-module__announcmentContentDiv']"
      );
  }

  async clickOnCancelButtonToCloseAnnouncement() {
    await this.cancelButtonOnPublishedAnnouncement.click();
  }

  async clickOnTakeMeButtonOnAnnouncement() {
    await this.takeMeButtonOnPublishedAnnouncement.click();
  }

  async waitForWebsocketEvent(): Promise<void> {
    console.log("waitng for websocket established");
    return new Promise((resolve) => {
      this.page.on("websocket", (ws: WebSocket) => {
        console.log("websocket connection established");
        ws.on("framereceived", (f) => {
          console.log(f);
        });
        resolve();
      });
    });
  }

  async isPublishedAnnouncementVisible() {
    //await this.page.waitForTimeout(5000);
    await expect(
      this.publishedAnnouncementContainer,
      "expecting announcement container to be visible"
    ).toBeVisible({ timeout: 25000 });
  }

  async verifyAnnouncementWithTextIsVisible(expectedText: string) {
    //first verify if the announcement container is visible
    await this.isPublishedAnnouncementVisible();
    await expect(
      this.contentDivLocatorOnAnnouncement,
      `expecting announcement container to have text ${expectedText}`
    ).toContainText(expectedText, { ignoreCase: true });
  }

  async verifyAnnouncementRedirectsTo(
    expectedUrl: string,
    isExternalRedirect = false
  ) {
    //click on take me button
    if (isExternalRedirect) {
      const popupPromise = this.page.waitForEvent("popup");
      await this.clickOnTakeMeButtonOnAnnouncement();
      const popup = await popupPromise;
      await popup.waitForLoadState();
      await expect(
        this.page,
        `expecting page to navigate to ${expectedUrl}`
      ).toHaveURL(expectedUrl);
    } else {
      await this.clickOnTakeMeButtonOnAnnouncement();

      await expect(
        this.page,
        `expecting page to navigate to ${expectedUrl}`
      ).toHaveURL(expectedUrl);
    }
  }
}
