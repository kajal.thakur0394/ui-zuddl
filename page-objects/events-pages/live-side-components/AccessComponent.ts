import { expect, Locator, Page } from "@playwright/test";
import ErrorMessage from "../../../enums/ErrorMessageEnum";
export class AccessComponent {
    readonly page: Page;
    readonly accessTextLocator: Locator

    constructor(page: Page) {
        this.page = page;
        this.accessTextLocator = this.page.locator("div[class^='styles-module__errorTagLine']");
    }

    async verifyNoAccessMessageVisible() {
        await expect(this.accessTextLocator).toContainText(ErrorMessage.NO_ACCESS);
    }
}
