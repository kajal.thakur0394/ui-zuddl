import { expect, FrameLocator, Locator, Page } from "@playwright/test";

export class AVStream {
  readonly page: Page;
  readonly nameContainerOnStream: Locator;
  readonly streamContainer: Locator;
  readonly webSiteSharingStreamContainer: Locator;
  readonly pdfStreamContainer: Locator;
  readonly zoomButton:Locator;
  readonly screenShareDisableModal:Locator;
  readonly screenShareModalCloseButton:Locator;

  constructor(page: Page) {
    this.page = page;
    this.nameContainerOnStream = this.page.locator(
      "div[class^='styles-module__gradientName']"
    );
    this.streamContainer = this.page.locator(
      "div[class*='styles-module__streamContainer']"
    );
    this.webSiteSharingStreamContainer = this.page.locator(
      "div[class^='styles-module__iframeContainer'] iframe"
    );
    this.pdfStreamContainer = this.page.locator(
      "div[class^='styles-module__document']"
    );
    this.zoomButton = this.page.locator(
      "div[class^='styles-module__fullscreenIcon']"
    );
    this.screenShareDisableModal = this.page.locator(
      "getByText('Screen sharing is disabled')"
    );
    this.screenShareModalCloseButton = this.page.locator(
      'button[class^=styles-module__modalClose_]'
    )

    
  }

  // return the stream container after filtering by name
  findStreamContainerByUserName(userNameOnStream: string) {
    console.log(`finding stream with text ${userNameOnStream}`);
    return this.streamContainer.filter({ hasText: userNameOnStream });
    //   .filter({ has: this.nameContainerOnStream })
    //   .filter({
    //     hasText: userNameOnStream,
    //   });
  }

  async verifyUserStreamIsVisible(userName: string) {
    const userStreamContainer = this.findStreamContainerByUserName(userName);
    await expect(
      userStreamContainer,
      `expecting user stream with name ${userName} to be visible`
    ).toBeVisible({ timeout: 40000 });
  }

  async verifyUserStreamIsNotVisible(userName: string) {
    const userStreamContainer = this.findStreamContainerByUserName(userName);
    await expect(
      userStreamContainer,
      `expecting user stream with name ${userName} not to be visible`
    ).not.toBeVisible();
  }

  async verifyCountOfStreamInsideRoomMatches(expectedCount: number) {
    await expect(
      this.streamContainer,
      `expecting total stream count to match ${expectedCount}`
    ).toHaveCount(expectedCount, { timeout: 40000 });
  }

  async verifyCountOfWebSiteStreamMatches(expectedCount: number) {
    await expect(
      this.webSiteSharingStreamContainer,
      `expecting total website sharing stream count to match ${expectedCount}`
    ).toHaveCount(expectedCount, { timeout: 40000 });
  }

  async verifyPdfStreamCountMatches(expectedCount: number) {
    await expect(
      this.pdfStreamContainer,
      `expecting total pdf sharing stream count to match ${expectedCount}`
    ).toHaveCount(expectedCount, { timeout: 40000 });
  }

  async verifyZoomButtonIsActive() {
    await this.zoomButton.click();
  }

  async verifyScreenShareDisabled(){
    await expect(this.page.getByText('Screen sharing is disabled')).toBeVisible({timeout:10000});
  }

  async closeModal(){
    await this.screenShareModalCloseButton.click();
  }  
}
