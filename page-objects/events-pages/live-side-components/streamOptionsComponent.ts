/**
 * This stream bar represents the bottom stream container which have options to
 * unmute/mute audio and vidio
 * screensharing options
 * emoji reactions
 * raise hand
 */

import { Locator, Page, expect, test } from "@playwright/test";
import { ScreenSharingOptions } from "../../../enums/screensharingOptionEnum";
import { VenueOptions } from "../../../enums/venueOptionsEnums";

export class StreamOptionsComponent {
  readonly muteaudioButtonForMyStream: Locator;
  readonly mutevideoButtonForMyStream: Locator;

  readonly unmuteaudioButtonForMyStream: Locator;
  readonly unmutevideoButtonForMyStream: Locator;

  readonly myScreenShareMenuButton: Locator;
  readonly myScreenShareOptions: Locator;

  readonly emojiReactionMenuButton: Locator;

  readonly raiseMyHandButton: Locator;
  readonly loweMyHandButton: Locator;

  readonly moreOptionButton: Locator;
  readonly disableScreenShareButton: Locator;

  constructor(readonly page: Page) {
    this.page = page;

    //av mute & unmute
    this.muteaudioButtonForMyStream = this.page.locator(
      "#stream-audio button[data-tip='Mute']"
    );
    this.unmuteaudioButtonForMyStream = this.page.locator(
      "#stream-audio button[data-tip='Unmute']"
    );
    this.mutevideoButtonForMyStream = this.page.locator(
      "#stream-video button[data-tip='Hide Video']"
    );
    this.unmutevideoButtonForMyStream = this.page.locator(
      "#stream-video button[data-tip='Show Video']"
    );
    this.moreOptionButton = this.page.locator("button:nth-child(6)");
    this.disableScreenShareButton = this.page.locator("label span");

    //screenshare
    this.myScreenShareMenuButton = this.page.locator("#stream-screen-share");
    this.myScreenShareOptions = this.page.locator(
      "div[class^='styles-module__popupContainer'] div[class^='styles-module__option_']"
    );

    //handraise
    this.raiseMyHandButton = this.page.locator(
      "button[class^='styles-module__raisedHandButton']"
    );
    this.loweMyHandButton = this.page.locator(
      "button[class^='styles-module__raisedHand'][data-tip='Lower hand']"
    );
  }

  // raise hand controller
  async raiseMyHand() {
    await this.raiseMyHandButton.click();
  }

  async lowerMyHand() {
    await this.loweMyHandButton.click();
  }

  //screen sharing related actions

  async clickOnScreenShareButton() {
    await this.myScreenShareMenuButton.click();
  }

  async clickToStopScreenShare() {
    await this.myScreenShareMenuButton.click();
  }
  async verifyGivenScreenSharingOptionIsNotVisible(
    screensharingOptions: ScreenSharingOptions
  ) {
    const optionLocator = this.page.locator(
      "div[class^='styles-module__popupContainer'] div[class^='styles-module__option_']"
    );
    const thisOption = optionLocator.filter({
      hasText: screensharingOptions,
    });
    await expect(
      thisOption,
      `expecting ${screensharingOptions} to not be visible for this user`
    ).not.toBeVisible();
  }

  async selectScreenSharingOption(
    screensharingOptions: ScreenSharingOptions,
    venue: VenueOptions,
    isSomeoneAlreadySharingWebSite = false,
    webSiteToShare = "https://www.example.com/",
    pdfToShare = "sample.pdf"
  ) {
    if (screensharingOptions === ScreenSharingOptions.WEBSITE) {
      await this.handleWebSiteShare(
        isSomeoneAlreadySharingWebSite,
        webSiteToShare
      );
    } else if (screensharingOptions === ScreenSharingOptions.PDF_FILE) {
      if (venue === "LegacyBackstage") {
        await this.handlePdfShareInLegacyBackstage(pdfToShare);
      } else {
        await this.handlePdfShare(pdfToShare);
      }
    } else if (screensharingOptions === ScreenSharingOptions.YOUR_SCREEN) {
      await this.handleEntireScreenShare();
    }
  }

  private async handleWebSiteShare(
    isSomeoneAlreadySharingWebSite: boolean,
    webSiteToShare: string
  ) {
    await test.step(`Finding the website sharing option in menu and click on it`, async () => {
      const websiteSharingOption = this.myScreenShareOptions.getByText(
        ScreenSharingOptions.WEBSITE,
        {
          exact: true,
        }
      );
      await websiteSharingOption.click();
    });

    await test.step(`Now Checking if someone is already sharing the website, then a popup message should be shown`, async () => {
      if (isSomeoneAlreadySharingWebSite) {
        await test.step(`yes someone is already sharing the website, so now verifying if poup was shown`, async () => {
          const someoneIsSharingScreenTooltip = this.page
            .getByText("Someone is already sharing Website Link")
            .first();
          await expect(
            someoneIsSharingScreenTooltip,
            `expecting someone is sharing tool tip to be visible`
          ).toBeVisible();
        });
        //just return it from here
        return;
      } else {
        await test.step("No website is already being shared, so user should be able to share the website", async () => {
          await this.page
            .locator("div[class^='styles-module__shareWebsiteContainer'] input")
            .fill(webSiteToShare);

          //wait for iframe to appear
          const previewFrameLocator = this.page.frameLocator(
            "iframe[class^='styles-module__iframeContainer']"
          );
          const h1Locator = previewFrameLocator.locator("h1");
          await expect(h1Locator).toContainText("Example Domain", {
            timeout: 50000,
          });

          //now click on share website button
          await this.page
            .locator("button")
            .filter({ hasText: "Share Website" })
            .click();
        });
      }
    });
  }

  private async handlePdfShare(pdfToShare?: string) {
    await test.step(`Finding the pdf sharing option in menu and click on it and upload the pdf ${pdfToShare}`, async () => {
      const fileChooserPromise = this.page.waitForEvent("filechooser");

      const pdfSharingOption = this.myScreenShareOptions.filter({
        hasText: ScreenSharingOptions.PDF_FILE,
      });

      await pdfSharingOption.click();
      const pdfPath = `test-data/${pdfToShare}`;
      const fileChooser = await fileChooserPromise;
      await fileChooser.setFiles(pdfPath);
    });
  }

  private async handlePdfShareInLegacyBackstage(pdfToShare?: string) {
    await test.step(`Finding the pdf sharing option in menu and click on it and upload the pdf ${pdfToShare}`, async () => {
      let pdfname = "SamplePDF";

      const pdfSharingOption = this.myScreenShareOptions.filter({
        hasText: ScreenSharingOptions.PDF_FILE,
      });

      await pdfSharingOption.click();
      const pdfPath = `test-data/${pdfToShare}`;

      const uploadNewPresentationButton = this.page.getByText(
        "Upload New Presentation"
      );
      await uploadNewPresentationButton.click();

      const presentationTitleInput = this.page
        .getByTestId("green_room_action_panel_menu")
        .getByRole("textbox");
      await presentationTitleInput.fill(pdfname);

      const uploadPresentationField = this.page.getByText(
        "Upload Presentation"
      );
      const fileChooserPromise = this.page.waitForEvent("filechooser");
      await uploadPresentationField.click();

      const fileChooser = await fileChooserPromise;
      await fileChooser.setFiles(pdfPath);

      let pdfPreviewContainer = this.page.locator(
        "img[class*='styles-module__image']"
      );
      await expect(pdfPreviewContainer).toBeVisible({ timeout: 30000 });

      await this.page.getByRole("button", { name: "Add Presentaion" }).click();

      let uploadedPdfContainer = this.page.locator(
        `//span[contains(text(), '${pdfname}')]/..//following-sibling::div`
      );
      await expect(uploadedPdfContainer).toBeVisible({ timeout: 50000 });
      await uploadedPdfContainer.click();

      await this.page.locator(`//button//span[contains(text(),'Yes')]`).click();
    });
  }

  private async handleEntireScreenShare() {
    await test.step(`Going to select Your Screen option from screen share menu and click on it`, async () => {
      const websiteSharingOption = this.myScreenShareOptions.filter({
        hasText: ScreenSharingOptions.YOUR_SCREEN,
      });
      await websiteSharingOption.click();
    });
  }

  // audio controller
  async muteMyAudio() {
    await this.muteaudioButtonForMyStream.click();
  }

  async unmuteMyAudio() {
    await this.unmuteaudioButtonForMyStream.click();
  }

  // video controller
  async muteMyVideo() {
    await this.mutevideoButtonForMyStream.click();
  }

  async unmuteMyVideo() {
    await this.unmutevideoButtonForMyStream.click();
  }

  async disableScreenShareOption() {
    await this.moreOptionButton.click();
    await this.disableScreenShareButton.click();
  }
}
