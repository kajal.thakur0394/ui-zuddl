import { expect, Locator, Page } from "@playwright/test";

export class AVModal {
  readonly page: Page;
  readonly joinButtonOnAvModal: Locator;
  readonly enterStudioButtonSAB: Locator;
  readonly videoStreamPreview: Locator;
  readonly videoStreamPreviewSAB: Locator; // SABS - Studio As Backstage
  readonly videoStreamPlaceholderSAB: Locator;
  readonly buttonToUnmuteAudio: Locator;
  readonly buttonToMuteAudio: Locator;
  readonly buttonToMuteVideo: Locator;
  readonly buttonToUnmuteVideo: Locator;
  readonly avModalLocator: Locator;
  readonly closeModalLocator: Locator;
  readonly exitModalLocator: Locator;

  constructor(page: Page) {
    this.page = page;
    this.joinButtonOnAvModal = this.page.locator(
      "div[class^='styles-module__joinStreamButton']"
    );
    this.enterStudioButtonSAB = this.page.locator(
      "//button[contains(@class, 'button')]//p[contains(text(),'Enter studio')]"
    );
    this.videoStreamPreview = this.page.locator(
      "div[class^='styles-module__streamPreviewModalVideoPreview'] #preview-stream video"
    );
    this.videoStreamPreviewSAB = this.page.locator(
      "div[class^='video-preview_videoStreamElement']"
    );
    this.videoStreamPlaceholderSAB = this.page.locator(
      "div[class^='LetterPlaceholder_container']"
    );
    this.buttonToMuteAudio = this.page.locator(
      "div[class^='styles-module__streamPreviewActionButtonsRow'] button[data-tip='Mute']"
    );
    this.buttonToUnmuteAudio = this.page.locator(
      "div[class^='styles-module__streamPreviewActionButtonsRow'] button[data-tip='Unmute']"
    );
    this.buttonToMuteVideo = this.page.locator(
      "div[class^='styles-module__streamPreviewActionButtonsRow'] button[data-tip='Hide Video']"
    );
    this.buttonToUnmuteVideo = this.page.locator(
      "div[class^='styles-module__streamPreviewActionButtonsRow'] button[data-tip='Show Video']"
    );
    this.avModalLocator = this.page.locator(
      "div[class*='styles-module__streamPreviewModal']"
    );
    this.closeModalLocator = this.page.locator(
      "button[class^='styles-module__modalClose___']"
    );
    this.exitModalLocator = this.page.locator(
      "button[class^='styles-module__noBGContrast']"
    );
  }

  async clickOnJoinButtonOnAvModal() {
    await this.joinButtonOnAvModal.click();
  }

  async clickOnEnterStudioOnAvModal() {
    await this.enterStudioButtonSAB.click();

    await this.verifyVideoStreamAVModalNotPresentOnSAB();
  }

  async isVideoStreamLoadedOnAVModal() {
    await expect(
      this.videoStreamPreview,
      "expecting video stream to be visibel on AV modal"
    ).toBeVisible({ timeout: 50000 });
  }

  async isVideoStreamLoadedOnAVModalSAB() {
    await expect(
      this.videoStreamPreviewSAB,
      "expecting video stream to be visibel on AV modal"
    ).toBeVisible({ timeout: 40000 });

    await expect(
      this.videoStreamPlaceholderSAB,
      "Expecting placeholder to be invisible on AV modal."
    ).not.toBeVisible({ timeout: 10000 });
  }

  async verifyVideoStreamAVModalNotPresentOnSAB() {
    await expect(
      this.videoStreamPreviewSAB,
      "expecting video stream to be visibel on AV modal"
    ).not.toBeVisible({ timeout: 10000 });
  }

  async muteAudioOnMyStream() {
    await this.buttonToMuteAudio.click();
  }

  async unmuteAudioOnMyStream() {
    await this.buttonToUnmuteAudio.click();
  }

  async muteVideoOnMyStream() {
    await this.buttonToMuteVideo.click();
  }

  async unmuteVideoOnMyStream() {
    await this.buttonToUnmuteVideo.click();
  }

  async muteAudioVideoStreams() {
    await this.isVideoStreamLoadedOnAVModal();
    await this.muteVideoOnMyStream();
    await this.muteAudioOnMyStream();
  }

  // async closeAndExitModal() {
  //   await this.isVideoStreamLoadedOnAVModal();
  //   await this.closeModalLocator.click();
  //   await expect(this.exitModalLocator).toBeVisible();
  //   await this.exitModalLocator.click();
  // }
  // cross button is no more available on AV modal

  async isAvModalVisible() {
    await expect(
      this.avModalLocator,
      "expecting AV join modal to be visible"
    ).toBeVisible();
  }
}
