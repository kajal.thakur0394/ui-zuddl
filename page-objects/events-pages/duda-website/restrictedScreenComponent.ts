import { expect, FrameLocator } from "@playwright/test";
export class RestrictedEventScreen {
  readonly registrationIframe: FrameLocator;
  constructor(registrationIframe: FrameLocator) {
    this.registrationIframe = registrationIframe;
  }

  async isVisible() {
    let expectedText1Locator = this.registrationIframe.locator(
      "text=text=Entered email has no access for this event"
    );
    let expectedText2Locator = this.registrationIframe.locator(
      "text=You do not have access to this"
    );

    if (
      (await expectedText1Locator.isVisible()) ||
      (await expectedText2Locator.isVisible())
    ) {
      return true;
    }
    return false;
  }

  async isPrivateEventScreenVisible() {
    await expect(
      this.registrationIframe.locator(
        "div[class^='styles-module__mainHeading']"
      )
    ).toHaveText("Private Event");
  }

  async isPrivateEventScreenVisibleForOTPFlow() {
    await expect(
      this.registrationIframe.locator(
        "text=Entered email has no access for this event"
      )
    ).toBeVisible();
  }

  async isEventEndedScreenisVisible() {
    await expect(
      this.registrationIframe.locator(
        "text=The event you're trying to join has ended"
      )
    ).toBeVisible();
  }
}
