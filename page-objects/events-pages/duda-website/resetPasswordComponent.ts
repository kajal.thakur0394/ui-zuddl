import { expect, FrameLocator, Locator, Page } from "@playwright/test";

export class ResetPasswordComponent {
  //selectors
  readonly registrationIframe: FrameLocator;
  readonly setPasswordInputBox: Locator;
  readonly confirmPasswordInputBox: Locator;
  readonly continueButton: Locator;
  readonly page: Page;

  //constructor
  constructor(registrationIframe: FrameLocator, page: Page) {
    this.page = page;
    this.registrationIframe = registrationIframe;
    this.setPasswordInputBox = this.registrationIframe
      .locator("input[class^='styles-module__passwordField']")
      .nth(0);
    this.confirmPasswordInputBox = this.registrationIframe
      .locator("input[class^='styles-module__passwordField']")
      .nth(1);
    this.continueButton = this.registrationIframe.locator(
      "button:has-text('Continue')"
    );
  }

  //actions
  async setPassword(userPassword: string) {
    await this.setPasswordInputBox.fill(userPassword);
    await this.page.keyboard.press("Tab");
    await this.confirmPasswordInputBox.fill(userPassword);
    await this.continueButton.click();
  }
}
