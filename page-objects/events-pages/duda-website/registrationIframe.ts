import { expect, FrameLocator, Locator, Page } from "@playwright/test";
import { LoginOptionsComponent } from "./loginOptionsComponent";
import { RegistrationFormComponent } from "./registrationFormComponent";
const fs = require("fs");

export class RegistrationWidgetIframe {
  readonly registrationIframe: FrameLocator;
  readonly eventEndedTextContainer: Locator;
  private readonly loginOptionComponent: LoginOptionsComponent;
  private readonly registrationFormComponent: RegistrationFormComponent;
  readonly page: Page;
  readonly enterNowButtonOnIframe: Locator;
  readonly notificationContainer: Locator;
  readonly mainHeadingOnRestrictedScreenForInviteOnlyEvent: Locator;
  readonly addToCalendarButton: Locator;
  readonly futureScreenContainer: Locator;
  readonly notificationMessage: Locator;
  readonly fieldErrorValidationMessage: Locator;

  constructor(iframeLocator: FrameLocator, page: Page) {
    this.registrationIframe = iframeLocator;
    this.page = page;
    this.eventEndedTextContainer = this.registrationIframe.locator(
      "div[class*='styles-module__embedMainContainer'] div[class^='styles-module__heading']"
    );
    this.registrationFormComponent = new RegistrationFormComponent(
      this.registrationIframe,
      page
    );
    this.loginOptionComponent = new LoginOptionsComponent(
      this.registrationIframe,
      this.page
    );
    this.enterNowButtonOnIframe = this.registrationIframe
      .locator("button")
      .filter({ hasText: "Enter Now" });
    this.notificationContainer = this.registrationIframe.locator(
      "div[class^='styles-module__notificationContainer']"
    );
    this.mainHeadingOnRestrictedScreenForInviteOnlyEvent =
      this.registrationIframe.locator(
        "div[class^='styles-module__mainHeading']"
      );
    this.addToCalendarButton = this.registrationIframe.locator(
      "a[class^='react-add-to-calendar']"
    );
    this.futureScreenContainer = this.registrationIframe.locator(
      "div[class^='styles-module__mainContainer']"
    );
    this.notificationMessage = this.registrationIframe.locator(
      "p[class^='styles-module__notificationMessage']"
    );
    this.fieldErrorValidationMessage = this.registrationIframe.locator(
      "p[class^='styles-module__fieldValidationErrorText']"
    );
  }

  async verifyEventIsEndedTextIsVisible() {
    const actualText = await this.eventEndedTextContainer.textContent();
    expect(actualText).toBe("The event you're trying to join  has ended");
  }

  get getRegistrationFormComponent() {
    return this.registrationFormComponent;
  }

  get getLoginOptionsComponent() {
    return this.loginOptionComponent;
  }

  async clickOnEnterNowButton() {
    await this.enterNowButtonOnIframe.click();
  }

  async verifyNotificationIsSeenWithMessage(expectedMessage: string) {
    await expect(this.notificationContainer).toContainText(expectedMessage);
  }

  async verifyInviteOnlyRestrictedScreenIsVisibleForAttendee() {
    await expect(
      this.mainHeadingOnRestrictedScreenForInviteOnlyEvent
    ).toContainText(" Entered email has no access for this event");
  }

  async verifyAddToCalendarIsVisible() {
    await expect(this.addToCalendarButton).toBeVisible();
  }

  async verifyEventIsInFutureScreenIsVisible() {
    await expect(this.futureScreenContainer).toContainText(
      "The event has not started yet",
      { timeout: 15000 }
    );
  }

  async verifyRestrictedDomainNotificationIsVisible() {
    await expect(this.notificationMessage).toContainText(
      "This email domain is restricted"
    );
  }

  async verifyRestrictedDomainFieldValidationErrorIsVisible() {
    await expect(this.fieldErrorValidationMessage).toContainText(
      "This email domain is restricted"
    );
  }

  async fetchCalendarLinksFromAddToCalendarButton() {
    await this.addToCalendarButton.click();
    // Downloading Apple ics data
    const downloadPromise = this.page.waitForEvent("download");
    await this.registrationIframe.locator('a[class^="apple-link"]').click();
    const download = await downloadPromise;
    // Wait for the download process to complete
    console.log(await download.path());
    // Save downloaded file somewhere
    const AppleICSFile = await fs.readFileSync(await download.path());
    let AppleIcsfileName =
      "apple" + Date.now().toString() + download.suggestedFilename();
    console.log("Appleics file", AppleIcsfileName);
    fs.writeFileSync(AppleIcsfileName, AppleICSFile);

    // Downloading Outlook ICS data
    await this.addToCalendarButton.click();
    const outlookDownloadPromise = this.page.waitForEvent("download");
    await this.registrationIframe.locator('a[class^="outlook-link"]').click();
    const outlookDownload = await outlookDownloadPromise;
    // Save downloaded file somewhere
    const OutlookICSFile = await fs.readFileSync(await outlookDownload.path());
    let OutlookIcsfileName =
      "Outlook" + Date.now().toString() + outlookDownload.suggestedFilename();
    fs.writeFileSync(OutlookIcsfileName, OutlookICSFile);
    console.log(OutlookIcsfileName);

    // Fetching calendar links
    await this.addToCalendarButton.click();
    let googleInviteLink = await this.registrationIframe
      .locator('a[class^="google-link"]')
      .getAttribute("href");
    console.log("Google invite link", googleInviteLink);
    let yahooInviteLink = await this.registrationIframe
      .locator('a[class^="yahoo-link"]')
      .getAttribute("href");
    console.log("Yahoo invite link", yahooInviteLink);
    return [
      AppleIcsfileName,
      OutlookIcsfileName,
      googleInviteLink,
      yahooInviteLink,
    ];
  }
}
