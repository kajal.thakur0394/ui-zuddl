import { expect, FrameLocator } from "@playwright/test";
export class RestrictedSpeakerAccessPage {
  readonly registrationIframe: FrameLocator;
  constructor(registrationIframe: FrameLocator) {
    this.registrationIframe = registrationIframe;
  }

  async isVisible() {
    await expect(
      this.registrationIframe.locator("text=have access to this"),
      "Restricted entry only for speakers page is visible"
    ).toBeVisible({ timeout: 15000 });
  }

  async chooseToGoToAttendeePage() {
    await expect(
      this.registrationIframe.locator(
        "span[class^='styles-module__attendeeLpRoute']"
      ),
      "Expecting click here button present to go to attendee landing page"
    ).toBeVisible();
    await this.registrationIframe
      .locator("span[class^='styles-module__attendeeLpRoute']")
      .click();
  }
}
