import { FrameLocator, Locator, Page } from "@playwright/test";
import { RegistrationWidgetIframe } from "./registrationIframe";
import { RestrictedSpeakerAccessPage } from "./restrictedSpeakerAccessPage";

export class DudaLandingPage {
  readonly page: Page;
  readonly registerButton: Locator;
  readonly enterNowButton: Locator;
  readonly registrationIframe: FrameLocator;
  readonly eventEndedTextContainer: Locator;
  readonly restrictedSpeakerAccessPage: RestrictedSpeakerAccessPage;
  constructor(page: Page) {
    this.page = page;
    this.registerButton = this.page.locator(
      "button[class*='registrationButton']"
    );
    this.enterNowButton = this.page
      .locator("button.registrationButton")
      .filter({ hasText: "Enter Now" });

    this.registrationIframe = this.page.frameLocator(
      "iframe.registrationIframe"
    );
    this.eventEndedTextContainer = this.registrationIframe.locator(
      "div[class*='styles-module__embedMainContainer'] div[class^='styles-module__heading']"
    );
    this.restrictedSpeakerAccessPage = new RestrictedSpeakerAccessPage(
      this.registrationIframe
    );
  }

  get getRegistrationIframeWidget() {
    return new RegistrationWidgetIframe(this.registrationIframe, this.page);
  }

  get getRestrictedSpeakerAccessPage() {
    return this.restrictedSpeakerAccessPage;
  }
  async clickOnRegisterButton() {
    await this.page.waitForTimeout(5000);
    await this.registerButton.click();
    return new RegistrationWidgetIframe(this.registrationIframe, this.page);
  }

  async isLobbyLoaded() {
    await this.page.waitForURL(/lobby/, {
      timeout: 50000,
    });
  }

  async isStageLoaded() {
    await this.page.waitForURL(/stage/, {
      timeout: 50000,
    });
  }

  async clickOnEnterNowButton() {
    await this.enterNowButton.click();
  }

  async load(url: string) {
    console.log(`Loading url ${url}`);
    await this.page.goto(url);
    console.log(`url loaded`);
  }
}
