import { expect, FrameLocator, Locator, Page } from "@playwright/test";

export class GuestAuthFormComponent {
  //selectors
  readonly registrationIframe: FrameLocator;
  readonly firstNameInputBox: Locator;
  readonly lastNameInputBox: Locator;
  readonly emailInputBox: Locator;
  readonly continueButton: Locator;

  //constructor
  constructor(registrationIframe: FrameLocator) {
    this.registrationIframe = registrationIframe;
    this.firstNameInputBox = this.registrationIframe.locator(
      "input[name='firstName']"
    );
    this.lastNameInputBox = this.registrationIframe.locator(
      "input[name='lastName']"
    );
    this.emailInputBox = this.registrationIframe.locator("input[name='email']");
    this.continueButton = this.registrationIframe
      .locator("button")
      .filter({ hasText: "Continue" });
  }

  //actions
  async fillGuestAuthForm({ firstName, lastName, email }) {
    await this.firstNameInputBox.fill(firstName);
    await this.lastNameInputBox.fill(lastName);
    await this.emailInputBox.fill(email);
    await this.continueButton.click();
  }
}
