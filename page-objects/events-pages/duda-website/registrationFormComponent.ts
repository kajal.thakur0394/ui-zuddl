import { expect, FrameLocator, Locator, Page } from "@playwright/test";
import { GuestAuthFormComponent } from "./guestLoginComponent";
import { EditProfileFieldObject } from "../../../test-data/editProfileForm";

export class RegistrationFormComponent {
  readonly page: Page;
  readonly registrationIframe: FrameLocator;
  readonly firstName: Locator;
  readonly lastName: Locator;
  readonly email: Locator;
  readonly registerNowButton: Locator;
  readonly registrationConfirmationMessageContainer: Locator;
  readonly alreadyRegisteredButtonContainer: Locator;
  readonly alreadyRegisteredButtonContainer2: Locator;
  readonly loginAsGuestButton: Locator;
  readonly loginAsGuestButton2: Locator;
  readonly errorMessageLocator: Locator;

  constructor(registrationIFrame: FrameLocator, page: Page) {
    this.page = page;
    this.registrationIframe = registrationIFrame;
    this.firstName = this.registrationIframe.locator("input[name='firstName']");
    this.lastName = this.registrationIframe.locator("input[name='lastName']");
    this.email = this.registrationIframe.locator("input[name='email']");
    this.registerNowButton = this.registrationIframe.getByRole("button", {
      name: "Register Now",
    });
    this.registrationConfirmationMessageContainer =
      this.registrationIframe.locator(
        "div[class^='styles-module__registrationMessageContainer']"
      );
    this.alreadyRegisteredButtonContainer = this.registrationIframe.locator(
      "div[class^='styles-module__alreadyRegisteredText']"
    );
    this.loginAsGuestButton = this.registrationIframe.locator(
      "div[class^='styles-module__guestLoginText']"
    );
    this.loginAsGuestButton2 = this.registrationIframe.locator(
      "div[class^='styles-module__guestLoginBtn___']"
    );
    this.errorMessageLocator = this.registrationIframe.locator(
      "p[class^='styles-module__mandatoryDisclaimerWarningText']"
    );
    this.alreadyRegisteredButtonContainer2 = this.registrationIframe.locator(
      "button[class^='styles-module__alreadyRegisteredBtn']"
    );
  }

  async fillUpTheRegistrationForm({ firstName, lastName, email }) {
    await expect(
      this.firstName,
      "expecting first name input box to be visible"
    ).toBeVisible({ timeout: 25000 });
    console.log(`First Name -> ${firstName}`);
    console.log(`Last Name -> ${lastName}`);
    console.log(`Email ID -> ${email}`);
    await this.firstName.fill(firstName);
    await this.lastName.fill(lastName);
    await this.email.fill(email);
    await this.registerNowButton.click();
  }

  async fillUpTheRegistrationFormForPredefinedFields(
    userRegistrationFormObj,
    options?: {
      fillMandatoryFields?: boolean | true;
      fillOptionalFields?: boolean | true;
      clickOnSubmitButton?: boolean | false;
      fieldsToSkip?: string[];
      ConditionalsFieldsToSkip?: string[];
      clickOnDesclaimerButton?: boolean | false;
    }
  ) {
    let registratrionFormLocator = this.page.locator("div[id*='regFormId1']");
    await expect(registratrionFormLocator).toBeVisible({ timeout: 500000 });
    // for each reg form field, we will iterate and fill up the form
    for (let eachField in userRegistrationFormObj) {
      console.log(`handling the field ${eachField}`);
      let filedObj = userRegistrationFormObj[eachField]; //will get the object
      let fieldLabel = filedObj["label"];
      let fieldType = filedObj["fieldType"];
      let fieldValueToEnter = filedObj["dataToEnter"];
      let isFieldOptional = filedObj["isOptionalField"];
      let isFieldConditional = filedObj["isConditionaField"];
      // if condition is given to fill only mandatory fields
      console.log(
        `handling field ${fieldLabel} of type ${fieldType} with value ${fieldValueToEnter}`
      );

      if (options) {
        if (options.fieldsToSkip?.includes(fieldLabel)) {
          console.log("Field Label excluded:", fieldLabel);
          continue;
        }
        if (options?.fillMandatoryFields && !options.fillOptionalFields) {
          if (isFieldOptional) {
            console.log(`filed is optional hence skipping it`);
            continue;
          } else {
            await this.enterDataInRegFormField(
              fieldLabel,
              fieldType,
              fieldValueToEnter,
              isFieldConditional
            );
          }
        } else if (
          !options?.fillMandatoryFields &&
          options?.fillOptionalFields
        ) {
          if (!isFieldOptional) {
            console.log(`filed is mandatory hence skipping it`);
            continue;
          } else {
            await this.enterDataInRegFormField(
              fieldLabel,
              fieldType,
              fieldValueToEnter,
              isFieldConditional
            );
          }
        } else if (options?.fillMandatoryFields && options.fillOptionalFields) {
          console.log(`filling up the field`);
          await this.enterDataInRegFormField(
            fieldLabel,
            fieldType,
            fieldValueToEnter,
            isFieldConditional
          );
        }
      } else {
        await this.enterDataInRegFormField(
          fieldLabel,
          fieldType,
          fieldValueToEnter,
          isFieldConditional
        );
      }
    }

    // //if its before auth, then its register button else its continue button
    // if (options?.clickOnDesclaimerButton) {
    //   await this.clickOnDesclaimerButtonOnRegisterationForm();
    // }

    //if its before auth, then its register button else its continue button
    if (options?.clickOnSubmitButton) {
      await this.registerNowButton.click();
    } else {
      await this.registerNowButton.click();
    }
  }

  async enterDataInRegFormField(
    fieldLabel: string,
    fieldType: string,
    dataToEnter: any,
    isConditionalField: boolean = false
  ) {
    if (isConditionalField) {
      await this.page.waitForTimeout(1000);
    }
    let regFieldContainer;
    try {
      regFieldContainer = await this.getTheRegFieldContainer(
        fieldLabel,
        fieldType
      );
    } catch (err) {
      throw Error(
        `Invalid field label ${fieldLabel} passed, not found in the registartion form.`
      );
    }
    console.log(
      `Entering data : ${dataToEnter} in field with label ${fieldLabel} and field type ${fieldType}`
    );

    //now check what is the field type of this and accordingly enter or select the data
    switch (fieldType) {
      case "text": {
        let inputBox = await regFieldContainer;
        await inputBox.fill(dataToEnter);
        break;
      }
      case "multiselect-dropdown": {
        let dropdownFieldObject: EditProfileFieldObject = {
          fieldName: fieldLabel,
          fieldType: "dropdown",
          fieldLabel: fieldLabel,
          dropDownOptionToSelect: dataToEnter,
          isDefaultField: true,
          isConditionalField: false,
          isMandatoryField: false,
          toSkip: false,
          dataToEnter: dataToEnter,
          listOfOptionsToChooseInMultiSelectDropDown: dataToEnter,
        };
        await this.handleMultiSelectField(dropdownFieldObject);
        break;
      }
      case "dropdown": {
        let dropdownFieldObject: EditProfileFieldObject = {
          fieldName: fieldLabel,
          fieldType: fieldType,
          fieldLabel: fieldLabel,
          dropDownOptionToSelect: dataToEnter,
          isDefaultField: true,
          isConditionalField: false,
          isMandatoryField: false,
          toSkip: false,
          dataToEnter: dataToEnter,
          listOfOptionsToChooseInMultiSelectDropDown: dataToEnter,
        };
        await this.handleDropDownField(dropdownFieldObject);
        break;
      }
      case "number": {
        let inputBox = await regFieldContainer;
        await inputBox.fill(dataToEnter);
        break;
      }
      default: {
        throw Error(
          `Invalid field type ${fieldType} passed for field label ${fieldLabel}`
        );
      }
    }
  }

  async getTheRegFieldContainer(fieldLabel: string, fieldType: string) {
    if (fieldType.includes("dropdown")) {
      return;
    }
    let thisRegFieldContainer: Locator;
    thisRegFieldContainer = this.registrationIframe.getByPlaceholder(
      `${fieldLabel} *`
    );
    // verify the count is 1, otherwise throw error cz we expect to find only 1 unique locator
    let count = await thisRegFieldContainer.count();
    console.log("Count of locator is:", count);
    if (count === 0 && fieldLabel === "Email") {
      thisRegFieldContainer = this.registrationIframe.getByPlaceholder(
        `Enter email address *`
      );
      count = await thisRegFieldContainer.count();
      console.log("Count of locator is:", count);
    }
    await expect(
      thisRegFieldContainer,
      `Field with label ${fieldLabel} not found`
    ).toHaveCount(1);
    return thisRegFieldContainer;
  }

  async handleDropDownField(fieldDataObject) {
    console.log(`handling dropdown field`);
    if (!fieldDataObject.toSkip) {
      console.log(
        `handling dropdown field with label ${fieldDataObject.fieldLabel} by entering data ${fieldDataObject.dropDownOptionToSelect}`
      );
      const dropDownContainer = this.registrationIframe
        .locator(
          "div[class^='styles-module__formSectionRowContainer'] div[class^='styles-module__container']"
        )
        .filter({ hasText: fieldDataObject.fieldLabel });
      await dropDownContainer.locator("div[class$='control']").click();
      //select the option
      const dropdownOption: Locator = dropDownContainer
        .locator("div[id^='react-select']")
        .filter({
          hasText: fieldDataObject.dropDownOptionToSelect,
        });
      //clicking on dropdown option to select
      console.log("Dropdown Filled successfully.");
      await dropdownOption.click();
    }
  }

  async handleMultiSelectField(fieldDataObject: EditProfileFieldObject) {
    if (!fieldDataObject.toSkip) {
      console.log(
        `handling multiselect field with label ${fieldDataObject.fieldLabel} by entering data ${fieldDataObject.dataToEnter}`
      );
      const multiSelectDropDownContainer = this.registrationIframe
        .locator(
          "div[class^='styles-module__formSectionRowContainer'] div[class^='styles-module__container']"
        )
        .filter({ hasText: fieldDataObject.fieldLabel })
        .locator(".dropdown-container");
      await multiSelectDropDownContainer.click();
      //select the option
      const multiSelectOption = multiSelectDropDownContainer.locator(
        "div.dropdown-content li"
      );
      //clicking on each option to select all options from multiselect dropdown
      for (const eachOption of fieldDataObject.listOfOptionsToChooseInMultiSelectDropDown) {
        await multiSelectOption
          .filter({
            hasText: eachOption,
          })
          .click();
      }
      await multiSelectDropDownContainer.click();
    } else {
      `Skipping field ${fieldDataObject.fieldLabel} as per given test data instruction`;
    }
  }

  async verifySuccessFullRegistrationMessageIsVisible() {
    await expect(this.registrationConfirmationMessageContainer).toContainText(
      "You've successfully registered! ",
      { timeout: 30000 }
    );
  }

  async clickOnAlreadyRegisteredButton() {
    //await this.page.waitForTimeout(10000);
    let locator1 = this.alreadyRegisteredButtonContainer;
    let locator2 = this.alreadyRegisteredButtonContainer2;
    if (await locator1.isVisible()) {
      await this.alreadyRegisteredButtonContainer.click();
    } else {
      await this.alreadyRegisteredButtonContainer2.click();
    }
  }

  async isRegistrationFormVisible() {
    await expect(
      this.firstName,
      "expecting registration form to be visible"
    ).toBeVisible({ timeout: 15000 });
  }

  async verifyEmailFieldIsPreFilledWith(expectedEmail: string) {
    await expect(this.email).toHaveValue(expectedEmail);
  }

  async clickOnLoginAsGuestButton() {
    let locator1 = this.loginAsGuestButton;
    let locator2 = this.loginAsGuestButton2;
    if (await locator1.isVisible()) {
      await this.loginAsGuestButton.click();
    } else {
      await this.loginAsGuestButton2.click();
    }
    return new GuestAuthFormComponent(this.registrationIframe);
  }

  async verifyDisclaimerIsRequiredErrorMessageIsDisplayed() {
    let errorMessage = this.errorMessageLocator.first();

    await expect(
      errorMessage,
      "Error message should be visible and contain warning."
    ).toContainText("Please accept these conditions to register:", {
      timeout: 30000,
    });
  }
}
