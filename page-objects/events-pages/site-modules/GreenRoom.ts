import { expect, Locator, Page } from "@playwright/test";
import { StreamOptionsComponent } from "../live-side-components/streamOptionsComponent";

//backstage section inside stage
export class GreenRoom {
  readonly page: Page;
  readonly greenRoomContainer: Locator;
  readonly videoStreamsInsideGreenRoomContainer: Locator;
  readonly handRaiseButtonOnVideoStreamToSendToStage: string;
  readonly handRaiseButtonOnVideoStreamToSendToGreenRoom: string;
  readonly kickOutButton: string;

  readonly yesButtonOnconfirmationPopup: Locator;
  readonly cancelButtonOnconfirmationPopup: Locator;
  readonly videoStreamInsideMainStageContainer: Locator;
  readonly mainStageContainer: Locator;

  readonly dropDownMenuForSessionControl: Locator;
  readonly buttonToEnableDryRunToggle: Locator;
  readonly buttonToDisableDryRunToggle: Locator;

  readonly startSessionButton: Locator;
  readonly endSesionButton: Locator;

  readonly startDryRunButton: Locator;
  readonly endDryRunButton: Locator;

  //exit stage and backstage button
  readonly exitStageButton: Locator;
  readonly exitBackStageButton: Locator;

  //exit and stay here button on exit stage confirmation popup
  readonly exitButonOnExitStageConfirmationPopup: Locator;
  readonly stayHereButtonOnExitStageConfirmationPopup: Locator;

  // remove and cancel button on remove stream confirmation popup
  readonly cancelButtonOnRemoveStreamConfirmationModal: Locator;
  readonly removeButtonOnRemoveStreamConfirmationModal: Locator;

  //roll call and speakers on stage section
  readonly rollCallSection: Locator;
  readonly speakersOnStageSection: Locator;
  readonly rollCallListContainer: Locator;
  readonly rollCallEachRowLocator: Locator;
  readonly onStageButtonOnRollCall: string;
  readonly speakerOnStageListContainer: Locator;
  readonly speakerOnStageEachRowLocator: Locator;
  readonly offStageButtonOnSpeakersOnStageRow: string;

  // extras section
  readonly extraSectionTab: Locator;
  readonly beginTimerSection: Locator;
  readonly startTimerButton: Locator;
  readonly stopTimerButton: Locator;
  readonly confettiSection: Locator;
  readonly runningTimerOnMainStage: Locator;
  readonly startConfettiButton: Locator;
  readonly stopConfettiButton: Locator;
  readonly timer: Locator;

  // media section
  readonly mediaSectionTab: Locator;
  readonly imageOverLaySection: Locator;
  readonly eachImageOverLayLocator: Locator;
  readonly overLayImageNameLocator: string;
  readonly stopBroadCastingButtonOnImageOverLay: string;
  readonly overLayOnMainStage: Locator;
  readonly presentationsSection: Locator;

  //presentation section
  readonly uploadNewPresentationButton: Locator;
  readonly presentationTitleInput: Locator;

  // video broadcast
  readonly videoBroadcastSection: Locator;
  readonly eachVideoLocator: Locator;

  // live session tab
  readonly liveSessionSectionTab: Locator;

  //breakout rooms section
  readonly breakOutSection: Locator;
  readonly breakOutStartButton: Locator;
  readonly confirmButtonOnBreakoutPrompt: Locator;
  readonly cancelButtonOnBreakoutPrompt: Locator;
  readonly exitButtonOnLeaveStagePromptDuringBreakout: Locator;
  readonly stayHereButtonOnLeaveStagePromptDuringBreakout: Locator;

  //..........//
  //Components//
  //..........//
  readonly streamOptionsComponent: StreamOptionsComponent;

  constructor(page: Page) {
    this.page = page;
    this.greenRoomContainer = this.page.locator(
      "div[class^='styles-module__greenRoomStreamsMainContainer']"
    );
    this.videoStreamsInsideGreenRoomContainer = this.greenRoomContainer.locator(
      "div[class*='styles-module__streamContainer']"
    );
    this.handRaiseButtonOnVideoStreamToSendToStage =
      "button#hand-raise-to-stage";
    this.handRaiseButtonOnVideoStreamToSendToGreenRoom =
      "button#hand-raise-to-backstage";

    this.kickOutButton = "button[data-tip='Remove User']";

    this.yesButtonOnconfirmationPopup = this.page
      .locator("div[class^='styles-module__modal']")
      .locator("button:has-text('Yes')");
    this.cancelButtonOnconfirmationPopup = this.page
      .locator("div[class^='styles-module__modal']")
      .locator("button:has-text('Cancel')");
    this.mainStageContainer = this.page.locator(
      "div[class^='styles-module__mainStageContainer']"
    );
    this.videoStreamInsideMainStageContainer = this.mainStageContainer.locator(
      "div[class*='styles-module__streamContainer']"
    );
    this.dropDownMenuForSessionControl = this.page.locator(
      "#topStreamOptionsContainer div[class^='styles-module__dropDownMainContainer']"
    );

    this.buttonToDisableDryRunToggle = this.page.locator(
      "div[class^='styles-module__dryRunContainer'] div[class*='styles-module__toggleFieldButtonOn']"
    );
    this.buttonToEnableDryRunToggle = this.page.locator(
      "div[class^='styles-module__dryRunContainer'] div[class*='styles-module__toggleFieldButtonOff']"
    );
    this.startSessionButton = this.page.locator(
      "button:has-text('START SESSION')"
    );
    this.endSesionButton = this.page.locator("button:has-text('END SESSION')");

    this.startDryRunButton = this.page.locator(
      "button:has-text('START DRY RUN ')"
    );
    this.endDryRunButton = this.page.locator("button:has-text('END DRY RUN')");

    //exit stage and backstage button
    this.exitStageButton = this.page.locator("button:has-text('EXIT STAGE')");
    this.exitBackStageButton = this.page.locator(
      "button:has-text('EXIT BACKSTAGE')"
    );

    //exit button and stay here button on exit stage confirmation popup
    this.exitButonOnExitStageConfirmationPopup = this.page
      .locator("div[class^='styles-module__modal']")
      .locator("button:has-text('Yes, Leave')");

    this.stayHereButtonOnExitStageConfirmationPopup = this.page
      .locator("div[class^='styles-module__modal']")
      .locator("button:has-text('Stay Here')");

    this.removeButtonOnRemoveStreamConfirmationModal = this.page
      .locator("div[class^='styles-module__modal']")
      .locator("button:has-text('Remove')");

    this.cancelButtonOnRemoveStreamConfirmationModal = this.page
      .locator("div[class^='styles-module__modal']")
      .locator("button:has-text('Remove')");

    // roll call and speakers on stage section
    this.rollCallSection = this.page
      .locator("div[class^='styles-module__currentSessionTabItemContents']")
      .locator("div[class^='styles-module__container']")
      .nth(1);
    this.speakersOnStageSection = this.page
      .locator("div[class^='styles-module__currentSessionTabItemContents']")
      .locator("div[class^='styles-module__container']")
      .nth(0);

    this.rollCallListContainer = this.page.locator(
      "div[class^='styles-module__rollCallContainer']"
    );
    this.rollCallEachRowLocator = this.page.locator(
      "div[class^='styles-module__rollCallRowContainer']"
    );
    this.onStageButtonOnRollCall = "button:has-text('on Stage')";

    this.speakerOnStageListContainer = this.page.locator(
      "div[class^='styles-module__speakersOnStageContainer']"
    );
    this.speakerOnStageEachRowLocator = this.page.locator(
      "div[class^='styles-module__speakersOnStageRowContainer']"
    );
    this.offStageButtonOnSpeakersOnStageRow = "button:has-text('off Stage')";

    // extras section
    this.extraSectionTab = this.page.locator("#announcements");
    // begin timer
    this.beginTimerSection = this.page.locator(
      "div[class^='styles-module__titleWrapper']:has-text('Begin Timer')"
    );
    this.startTimerButton = this.page.locator("button:has-text('Start Timer')");
    this.stopTimerButton = this.page.locator("button:has-text('Stop Timer')");
    this.runningTimerOnMainStage = this.page.locator(
      "div[class^='styles-module__timerTextContainer']"
    );

    //confetti section
    this.confettiSection = this.page.locator(
      "div[class^='styles-module__titleWrapper']:has-text('Control Confetti')"
    );
    this.startConfettiButton = this.page.locator(
      "button:has-text('Start confetti')"
    );
    this.stopConfettiButton = this.page.locator("button:has-text('Stop now')");
    // media tab
    this.mediaSectionTab = this.page.locator("#media");
    this.imageOverLaySection = this.page.locator(
      "div[class^='styles-module__titleWrapper']:has-text('Image Overlays')"
    );
    this.eachImageOverLayLocator = this.page.locator(
      "div[class^='styles-module__overlayImages'] div[class*='styles-module__imageList'] div[role='button']"
    );
    this.stopBroadCastingButtonOnImageOverLay =
      "div[class^='styles-module__broadcastText']:has-text('Stop Broadcasting')";

    this.overLayImageNameLocator = "div[class^='styles-module__nameWrapper']";

    this.overLayOnMainStage = this.mainStageContainer.locator(
      "img[class^='styles-module__overlayImage']"
    );

    //Presentation Section
    this.presentationsSection = this.page.getByText("Presentations");
    this.uploadNewPresentationButton = this.page.getByText(
      "Upload New Presentation"
    );
    this.presentationTitleInput = this.page.locator(
      "input[class*='inputField']"
    );

    // video broadcast section
    this.videoBroadcastSection = this.page.locator(
      "div[class^='styles-module__titleWrapper']:has-text('Videos')"
    );
    this.eachVideoLocator = this.page.locator(
      "div[class^='styles-module__videos'] div[class*='styles-module__imageList'] div[role='button']"
    );
    this.stopBroadCastingButtonOnImageOverLay =
      "div[class^='styles-module__broadcastText']:has-text('Stop Broadcasting')";

    this.overLayImageNameLocator = "div[class^='styles-module__nameWrapper']";

    this.overLayOnMainStage = this.mainStageContainer.locator(
      "img[class^='styles-module__overlayImage']"
    );
    // live sessions tab
    this.liveSessionSectionTab = this.page.locator("#currentSession");
    //breakout rooms
    this.breakOutSection = this.page.locator(
      "div[class^='styles-module__titleWrapper']:has-text('Breakout Rooms')"
    );
    this.breakOutStartButton = this.page.locator(
      "button:has-text('Start Breakout Rooms')"
    );
    this.confirmButtonOnBreakoutPrompt = this.page.locator(
      "button:has-text('Confirm')"
    );
    this.cancelButtonOnBreakoutPrompt = this.page.locator(
      "button:has-text('Cancel')"
    );
    this.exitButtonOnLeaveStagePromptDuringBreakout = this.page.getByRole(
      "button",
      {
        name: "Yes, Leave",
        exact: true,
      }
    );
    this.stayHereButtonOnLeaveStagePromptDuringBreakout = this.page.locator(
      "button:has-text('Stay here')"
    );
    this.timer = this.page.locator("//div[contains(@class,'_timerText_')]");

    ///.................///
    //....Components....//
    ///.................///
    this.streamOptionsComponent = new StreamOptionsComponent(page);
  }

  get getStreamOptionsComponent() {
    return this.streamOptionsComponent;
  }

  async isGreenRoomVisible() {
    await expect(this.greenRoomContainer).toBeVisible();
  }

  async isGreenRoomNotVisible() {
    await expect(this.greenRoomContainer).not.toBeVisible();
  }

  async verifyCountOfStreamInsideGreenRoomMatches(expectedStreamCount) {
    await expect(this.videoStreamsInsideGreenRoomContainer).toHaveCount(
      expectedStreamCount,
      { timeout: 30000 }
    );
  }

  // get user stream container from green room
  async getUserVideoStreamContainerFromGreenRoom(userName: string) {
    const userStreamInsideGreenRoom =
      this.videoStreamsInsideGreenRoomContainer.filter({
        hasText: userName,
      });

    await expect(userStreamInsideGreenRoom).toBeVisible({ timeout: 20000 });
    return userStreamInsideGreenRoom;
  }

  async sendUserToOnstageFromBackStageUsingRaiseHand(userName: string) {
    const userStreamInsideGreenRoom =
      await this.getUserVideoStreamContainerFromGreenRoom(userName);

    const raiseHandButtonForThisUser = userStreamInsideGreenRoom.locator(
      this.handRaiseButtonOnVideoStreamToSendToStage
    );
    await expect(raiseHandButtonForThisUser).toBeVisible({ timeout: 30000 });

    await raiseHandButtonForThisUser.click();
    await this.yesButtonOnconfirmationPopup.click();
  }

  async openRollCallSection() {
    await this.rollCallSection.click();
    await expect(
      this.rollCallListContainer,
      "roll call list container is visible"
    ).toBeVisible({ timeout: 25000 });
  }

  async openSpeakersOnStageSection() {
    await this.speakersOnStageSection.click();
  }
  getRollCallContainerOfUser(speakerName: string) {
    return this.rollCallEachRowLocator.filter({ hasText: speakerName });
  }

  getSpeakerOnListContainerOfUser(speakerName: string) {
    return this.speakerOnStageEachRowLocator.filter({ hasText: speakerName });
  }

  async sendSpeakerToOnStageFromBackStageUsingRollCall(speakerName: string) {
    const rollCallContainerForUser =
      this.getRollCallContainerOfUser(speakerName);
    await rollCallContainerForUser
      .locator(this.onStageButtonOnRollCall)
      .click();
    await this.yesButtonOnconfirmationPopup.click();
  }

  async sendSpeakerToGreenRoomFromMainStageUsingSpeakersOnStageContainer(
    userName: string
  ) {
    await expect(
      this.speakerOnStageListContainer,
      "expecting speakers on list contatiner to be visible"
    ).toBeVisible({ timeout: 30000 });
    const speakerOnStageContainerForUser =
      this.getSpeakerOnListContainerOfUser(userName);
    await speakerOnStageContainerForUser
      .locator(this.offStageButtonOnSpeakersOnStageRow)
      .click();
    await this.yesButtonOnconfirmationPopup.click();
  }

  // get user stream container from main stage room
  async getUserVideoStreamContainerFromMainStage(userName: string) {
    const userStreamInsideMainStage =
      this.videoStreamInsideMainStageContainer.filter({
        hasText: userName,
      });
    await expect(userStreamInsideMainStage).toBeVisible({ timeout: 25000 });
    return userStreamInsideMainStage;
  }

  async sendUserToGreenRoomFromBackStageUsingLowerHand(userName: string) {
    const userStreamInsideMainStage =
      await this.getUserVideoStreamContainerFromMainStage(userName);
    const raiseHandButtonForThisUser = userStreamInsideMainStage.locator(
      this.handRaiseButtonOnVideoStreamToSendToGreenRoom
    );
    await raiseHandButtonForThisUser.click();
    await this.yesButtonOnconfirmationPopup.click();
  }

  async verifyCountOfStreamInsideMainStageContainer(expectedStreamCount) {
    await expect(this.videoStreamInsideMainStageContainer).toHaveCount(
      expectedStreamCount,
      { timeout: 30000 }
    );
  }

  async disableDryRun() {
    await this.dropDownMenuForSessionControl.click();
    await this.buttonToDisableDryRunToggle.click();
    await this.yesButtonOnconfirmationPopup.click();
  }

  async enableDryRun() {
    await this.dropDownMenuForSessionControl.click();
    await this.buttonToEnableDryRunToggle.click();
    await this.yesButtonOnconfirmationPopup.click();
  }

  async startTheSession() {
    await this.startSessionButton.click();
    await this.yesButtonOnconfirmationPopup.click();
  }

  async endTheSession() {
    await this.endSesionButton.click();
    await this.yesButtonOnconfirmationPopup.click();
  }

  async startTheDryRun() {
    await this.startDryRunButton.click();
    await this.yesButtonOnconfirmationPopup.click();
  }

  async endTheDryRun() {
    await this.endDryRunButton.click();
    await this.yesButtonOnconfirmationPopup.click();
  }

  async startSpeakerTimer() {
    await this.extraSectionTab.click();
    await this.beginTimerSection.click();
    await this.startTimerButton.click();
  }

  async endSpeakerTimer() {
    await this.extraSectionTab.click();
    await this.stopTimerButton.click();
  }

  async verifyTimerVisibility(expectedVisibilityStatus: boolean) {
    if (expectedVisibilityStatus) {
      await expect(this.timer).toBeVisible({ timeout: 10000 });
    } else {
      await expect(this.timer).not.toBeVisible({ timeout: 10000 });
    }
  }

  // this options appears to speaker to exit from the stage and he will go to backstage
  async exitFromStageByClickinOnExitStageButton() {
    await this.exitStageButton.click();
    await this.exitButonOnExitStageConfirmationPopup.click();
  }

  // this options appears to attendee and organiser to exit from the stage and he will go to backstage
  async exitFromStageByClickinOnExitBackStageButton() {
    await this.exitBackStageButton.click();
    await this.exitButonOnExitStageConfirmationPopup.click();
  }

  async kickOffAttendeeOutOfStageFromMainStage(userName: string) {
    const userStreamInsideMainStage =
      await this.getUserVideoStreamContainerFromMainStage(userName);
    const kickOutButtonForUserStream = userStreamInsideMainStage.locator(
      this.kickOutButton
    );
    await kickOutButtonForUserStream.click();
    await this.removeButtonOnRemoveStreamConfirmationModal.click();
  }

  async kickOffSpeakerOutFromMainStage(userName: string) {
    const userStreamInsideMainStage =
      await this.getUserVideoStreamContainerFromMainStage(userName);
    const kickOutButtonForUserStream = userStreamInsideMainStage.locator(
      this.kickOutButton
    );
    await kickOutButtonForUserStream.click();
    await this.removeButtonOnRemoveStreamConfirmationModal.click();
  }

  async kickOffUserOutOfStageFromGreenRoom(userName: string) {
    const userStreamInsideMainStage =
      await this.getUserVideoStreamContainerFromGreenRoom(userName);
    const kickOutButtonForUserStream = userStreamInsideMainStage.locator(
      this.kickOutButton
    );
    await kickOutButtonForUserStream.click();
    await this.removeButtonOnRemoveStreamConfirmationModal.click();
  }

  // EXTRA SECTION HANDLING

  async openExtraSection() {
    await this.extraSectionTab.click();
  }

  async openBeginTimerSection() {
    await this.beginTimerSection.click();
  }

  async clickOnstartTimer() {
    await this.startTimerButton.click();
  }

  async clickOnStopTimer() {
    await this.stopTimerButton.click();
  }

  async isRunningTimerVisibleOnMainStage() {
    await expect(
      this.runningTimerOnMainStage,
      "expecting running timer is visible on main stage"
    ).toBeVisible({ timeout: 20000 });
  }

  async isRunningTimerNotVisibleOnMainStage() {
    await expect(
      this.runningTimerOnMainStage,
      "expecting running timer is not visible on main stage"
    ).not.toBeVisible({ timeout: 20000 });
  }

  // confetti
  async openConfettiSection() {
    await this.confettiSection.click();
  }

  async clickOnStartConfettiButton() {
    await this.startConfettiButton.click();
  }

  async clickOnStopNowButtonToStopConfetti() {
    await this.stopConfettiButton.click();
  }

  async isConfettiDisplayingOnMainStage() {
    await expect(
      this.page.locator("div canvas"),
      "expecting confetti to be displayed over main stagee"
    ).toBeVisible({ timeout: 20000 });
  }

  async isConfettiNotDisplayingOnMainStage() {
    await expect(
      this.page.locator("div canvas"),
      "expecting confetti to be displayed over main stagee"
    ).not.toBeVisible({ timeout: 20000 });
  }

  // Media tab

  async openMediaTab() {
    await this.mediaSectionTab.click();
  }

  async openImageOverLaySection() {
    await this.imageOverLaySection.click();
  }

  async openSessionVideoBroadcastSection() {
    await this.videoBroadcastSection.click();
  }

  async openPresentationsSection() {
    await this.presentationsSection.click();
  }

  getImageOverLayContainer(name: string) {
    return this.eachImageOverLayLocator.filter({ hasText: name });
  }
  async startImageOverLayBroadcasting(imageName: string) {
    const imageOverLayContainer = this.getImageOverLayContainer(imageName);
    await imageOverLayContainer.click();
  }

  async stopImageOverLayBroadcasting(imageName: string) {
    const imageOverLayContainer = this.getImageOverLayContainer(imageName);
    await imageOverLayContainer
      .locator(this.stopBroadCastingButtonOnImageOverLay)
      .click();
    await this.yesButtonOnconfirmationPopup.click();
  }

  async verifyOverLayIsVisibleOnMainStage() {
    await expect(this.overLayOnMainStage).toBeVisible({ timeout: 20000 });
  }
  async verifyOverLayIsNotVisibleOnMainStage() {
    await expect(this.overLayOnMainStage).not.toBeVisible({ timeout: 25000 });
  }

  async uploadPdfFileForPresentation(pathToPdfFile: string) {
    await this.openMediaTab();
    await this.openPresentationsSection();
    await this.uploadNewPresentationButton.click();
    await this.presentationTitleInput.fill("SamplePDF");
  }

  //BREAK OUT ROOM SECTION
  async openBreakoutRoomsSection() {
    await this.breakOutSection.click();
  }

  async clickOnStartBreakoutRoomButton() {
    await this.breakOutStartButton.click();
  }

  async clickOnConfirmButtonOnBreakoutPrompt() {
    await this.confirmButtonOnBreakoutPrompt.click();
  }

  async startBreakoutRooms() {
    await this.clickOnStartBreakoutRoomButton();
    await this.clickOnConfirmButtonOnBreakoutPrompt();
    //wait for 5 sec timer to start breakout room
    await this.page.waitForTimeout(9000);
  }

  async isExitStagePromptVisibleToOrganiser() {
    await expect(this.exitButtonOnLeaveStagePromptDuringBreakout).toBeVisible();
  }

  async clickOnExitStagePromptDuringBreakout() {
    await this.exitButtonOnLeaveStagePromptDuringBreakout.click();
  }
}
