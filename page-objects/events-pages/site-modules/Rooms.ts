import { Locator, Page, expect, test } from "@playwright/test";
import { AVStream } from "../live-side-components/AVStream";
import { InteractionPanel } from "../live-side-components/InteractionPanel";
import { StreamOptionsComponent } from "../live-side-components/streamOptionsComponent";

export class RoomModule extends AVStream {
  readonly confirmationPopup: Locator;
  readonly exitConfirmationPopup: Locator;
  readonly stayHereButtonOnExitRoomPopup: Locator;
  readonly roomTitleLocator: Locator;
  readonly exitButtonForRoom: Locator;
  readonly exitRoomButtonOnExitRoomPopup: Locator;
  //components
  readonly streamOptionsComponent: StreamOptionsComponent;

  //interaction panel composition
  readonly interactionPanelComponent: InteractionPanel;

  constructor(page: Page) {
    super(page);
    this.exitConfirmationPopup = this.page.locator(
      "div[class^='styles-module__modal']"
    );
    this.exitButtonForRoom = this.page.locator(
      "button[data-testid='exit_room']"
    );
    this.exitRoomButtonOnExitRoomPopup = this.exitConfirmationPopup.locator(
      "button:has-text('Yes, Leave')"
    );
    this.stayHereButtonOnExitRoomPopup = this.exitConfirmationPopup.locator(
      "button:has-text('Stay here')"
    );
    this.roomTitleLocator = this.page.locator(
      "div[class^='styles-module__roomTitleContainer']"
    );
    this.interactionPanelComponent = new InteractionPanel(page);
    this.streamOptionsComponent = new StreamOptionsComponent(page);
  }

  get getStreamOptionsComponent() {
    return this.streamOptionsComponent;
  }
  get getInteractionPanelComponent() {
    return this.interactionPanelComponent;
  }

  async clickOnExitRoom() {
    await this.exitButtonForRoom.click();
  }

  async handleExitConfirmationPopup(clickOnExit: boolean = true) {
    if (clickOnExit) {
      await this.exitRoomButtonOnExitRoomPopup.click();
    } else {
      await this.stayHereButtonOnExitRoomPopup.click();
    }
  }

  async verifyRoomTitleMatches(expectedTitle: string) {
    await expect(this.roomTitleLocator).toContainText(expectedTitle);
  }
}
