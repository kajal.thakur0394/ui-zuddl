import { expect, Locator, Page } from "@playwright/test";

export class MainStageContainer {
  readonly page: Page;
  readonly mainStageContainer: Locator;
  readonly videoStreamInsideMainStageContainer: Locator;
  readonly dropDownMenuForSessionControl: Locator;
  readonly buttonToEnableDryRunToggle: Locator;
  readonly buttonToDisableDryRunToggle: Locator;

  readonly startSessionButton: Locator;
  readonly endSesionButton: Locator;

  readonly startDryRunButton: Locator;
  readonly endDryRunButton: Locator;

  readonly yesButtonOnconfirmationPopup: Locator;
  readonly cancelButtonOnconfirmationPopup: Locator;

  readonly handRaiseButtonOnVideoStreamToSendToGreenRoom: string;

  readonly overLayOnMainStage: Locator;
  readonly stageOutputContainer: Locator;

  constructor(page: Page) {
    this.page = page;
    this.mainStageContainer = this.page.locator(
      "div[class^='styles-module__mainStageContainer']"
    );
    this.yesButtonOnconfirmationPopup = this.page
      .locator("div[class^='styles-module__modal']")
      .locator("button:has-text('Yes')");
    this.cancelButtonOnconfirmationPopup = this.page
      .locator("div[class^='styles-module__modal']")
      .locator("button:has-text('Cancel')");
    this.handRaiseButtonOnVideoStreamToSendToGreenRoom =
      "button#hand-raise-to-backstage";
    this.videoStreamInsideMainStageContainer = this.mainStageContainer.locator(
      "div[class*='styles-module__streamContainer']"
    );
    this.startDryRunButton = this.page.locator(
      "button:has-text('START DRY RUN ')"
    );
    this.endDryRunButton = this.page.locator("button:has-text('END DRY RUN')");
    this.startSessionButton = this.page.locator(
      "button:has-text('START SESSION')"
    );
    this.endSesionButton = this.page.locator("button:has-text('END SESSION')");
    this.buttonToDisableDryRunToggle = this.page.locator(
      "div[class^='styles-module__dryRunContainer'] div[class*='styles-module__toggleFieldButtonOn']"
    );
    this.buttonToEnableDryRunToggle = this.page.locator(
      "div[class^='styles-module__dryRunContainer'] div[class*='styles-module__toggleFieldButtonOff']"
    );
    this.buttonToDisableDryRunToggle = this.page.locator(
      "div[class^='styles-module__dryRunContainer'] div[class*='styles-module__toggleFieldButtonOn']"
    );
    this.buttonToEnableDryRunToggle = this.page.locator(
      "div[class^='styles-module__dryRunContainer'] div[class*='styles-module__toggleFieldButtonOff']"
    );
    // overlay image
    this.overLayOnMainStage = this.mainStageContainer.locator(
      "img[class^='styles-module__overlayImage']"
    );
  }

  async sendUserToGreenRoomFromBackStageUsingLowerHand(userName: string) {
    const userStreamInsideMainStage =
      this.videoStreamInsideMainStageContainer.filter({
        hasText: userName,
      });
    await expect(userStreamInsideMainStage).toBeVisible({ timeout: 30000 });
    const raiseHandButtonForThisUser = userStreamInsideMainStage.locator(
      this.handRaiseButtonOnVideoStreamToSendToGreenRoom
    );
    await raiseHandButtonForThisUser.click();
    await this.yesButtonOnconfirmationPopup.click();
  }

  async verifyCountOfStreamInsideMainStageContainer(
    expectedStreamCount: number
  ) {
    await expect(this.videoStreamInsideMainStageContainer).toHaveCount(
      expectedStreamCount,
      { timeout: 30000 }
    );
  }

  async disableDryRun() {
    await this.dropDownMenuForSessionControl.click();
    await this.buttonToDisableDryRunToggle.click();
    await this.yesButtonOnconfirmationPopup.click();
  }

  async enableDryRun() {
    await this.dropDownMenuForSessionControl.click();
    await this.buttonToEnableDryRunToggle.click();
    await this.yesButtonOnconfirmationPopup.click();
  }

  async startTheSession() {
    await this.startSessionButton.click();
    await this.yesButtonOnconfirmationPopup.click();
  }

  async endTheSession() {
    await this.endSesionButton.click();
    await this.yesButtonOnconfirmationPopup.click();
  }

  async startTheDryRun() {
    await this.startDryRunButton.click();
    await this.yesButtonOnconfirmationPopup.click();
  }

  async endTheDryRun() {
    await this.endDryRunButton.click();
    await this.yesButtonOnconfirmationPopup.click();
  }

  async verifyOverLayIsVisibleOnMainStage() {
    await expect(this.overLayOnMainStage).toBeVisible();
  }
  async verifyOverLayIsNotVisibleOnMainStage() {
    await expect(this.overLayOnMainStage).not.toBeVisible();
  }
}
