import { expect, Locator, Page } from "@playwright/test";
// import camelCase from 'camelcase'
// const camelCase = require("camelcase")

export class RegistrationFormComponent {
  //selectors
  readonly page: Page;
  readonly regFormHeader: Locator;
  readonly regFormFieldLabel: Locator;
  readonly regFormFieldContainer: Locator; // filed container is ancestor div of field label
  readonly registerNowButton: Locator;
  readonly alreadyRegisteredButton: Locator;
  readonly regFormContainer: Locator;
  readonly notificationContainer: Locator;
  readonly notificationMessage: Locator;

  //constructor
  constructor(page: Page) {
    this.page = page;
    this.regFormHeader = this.page.locator(
      "div[class^='styles-module__loginHeaderContainer']"
    );
    this.regFormContainer = this.page.locator(
      "div[class^='styles-module__formSectionContainer']"
    );
    // this.regFormLocator=this.page.locator("text=Enter your details") //header on this form with this text
    this.regFormFieldLabel = this.page.locator("label");
    this.registerNowButton = this.page.locator(
      "button:has-text('Register now')"
    );
    this.regFormFieldContainer = this.page.locator(
      "input[class^='Input_input__2U23S']"
    );
    this.alreadyRegisteredButton = this.page.locator(
      "text=Already registered? Log in"
    );
    this.notificationContainer = this.page.locator(
      "div[class^='styles-module__notificationContainer']"
    );
    this.notificationMessage = this.page.locator(
      "p[class^='styles-module__notificationMessage']"
    );
  }

  async getTotalNumberOfRegistrationFields() {
    let countOfFields = await this.regFormFieldLabel.count();
    return countOfFields;
  }

  async fillUpTheRegistrationForm(
    userRegistrationFormObj,
    options?: {
      fillMandatoryFields?: boolean | true;
      fillOptionalFields?: boolean | true;
      clickOnSubmitButton?: boolean | false;
      fieldsToSkip?: string[];
      ConditionalsFieldsToSkip?: string[];
      clickOnDesclaimerButton?: boolean | false;
    }
  ) {
    // for each reg form field, we will iterate and fill up the form
    for (let eachField in userRegistrationFormObj) {
      console.log(`handling the field ${eachField}`);
      let filedObj = userRegistrationFormObj[eachField]; //will get the object
      let fieldLabel = filedObj["label"];
      let fieldType = filedObj["fieldType"];
      let fieldValueToEnter = filedObj["dataToEnter"];
      let isFieldOptional = filedObj["isOptionalField"];
      let isFieldConditional = filedObj["isConditionaField"];
      // if condition is given to fill only mandatory fields
      console.log(
        `handling field ${fieldLabel} of type ${fieldType} with value ${fieldValueToEnter}`
      );
      if (options) {
        if (options.fieldsToSkip?.includes(fieldLabel)) {
          console.log("Field Label excluded:", fieldLabel);
          continue;
        }
        if (options?.fillMandatoryFields && !options.fillOptionalFields) {
          if (isFieldOptional) {
            console.log(`filed is optional hence skipping it`);
            continue;
          } else {
            await this.enterDataInRegFormField(
              fieldLabel,
              fieldType,
              fieldValueToEnter,
              isFieldConditional
            );
          }
        } else if (
          !options?.fillMandatoryFields &&
          options?.fillOptionalFields
        ) {
          if (!isFieldOptional) {
            console.log(`filed is mandatory hence skipping it`);
            continue;
          } else {
            await this.enterDataInRegFormField(
              fieldLabel,
              fieldType,
              fieldValueToEnter,
              isFieldConditional
            );
          }
        } else if (options?.fillMandatoryFields && options.fillOptionalFields) {
          console.log(`filling up the field`);
          await this.enterDataInRegFormField(
            fieldLabel,
            fieldType,
            fieldValueToEnter,
            isFieldConditional
          );
        }
      } else {
        await this.enterDataInRegFormField(
          fieldLabel,
          fieldType,
          fieldValueToEnter,
          isFieldConditional
        );
      }
    }

    //if its before auth, then its register button else its continue button
    if (options?.clickOnDesclaimerButton) {
      await this.clickOnDesclaimerButtonOnRegisterationForm();
    }
    //if its before auth, then its register button else its continue button
    if (options?.clickOnSubmitButton) {
      await this.clickOnSubmitButtonOnRegisterationForm();
    } else {
      await this.registerNowButton.click();
    }
  }

  async clickOnSubmitButtonOnRegisterationForm() {
    await this.page.locator("text=Submit").click();
  }

  async clickOnDesclaimerButtonOnRegisterationForm() {
    await this.page.locator("div[class^='Checkbox_input']").click();
  }
  async enterDataInRegFormField(
    fieldLabel: string,
    fieldType: string,
    dataToEnter: any,
    isConditionalField: boolean = false
  ) {
    if (isConditionalField) {
      await this.page.waitForTimeout(1000);
    }
    let regFieldContainer;
    try {
      regFieldContainer = await this.getTheRegFieldContainer(
        fieldLabel,
        fieldType
      );
    } catch (err) {
      throw Error(
        `Invalid field label ${fieldLabel} passed, not found in the registartion form.`
      );
    }
    console.log(
      `Entering data : ${dataToEnter} in field with label ${fieldLabel} and field type ${fieldType}`
    );
    //now check what is the field type of this and accordingly enter or select the data
    switch (fieldType) {
      case "text": {
        let inputBox = await regFieldContainer;
        await inputBox.fill(dataToEnter);
        break;
      }
      case "multiselect-dropdown": {
        let multiSelectDropdownContainer: Locator = await regFieldContainer;
        await this.page.waitForTimeout(1000);

        // check if type of dataToEnter is not list then throw error
        if (!Array.isArray(dataToEnter)) {
          throw Error(
            `For multiselect dropdown options,  invalid type passed, expected type is list`
          );
        }
        await multiSelectDropdownContainer.click({
          force: true,
        });

        // list of options i want to select
        let valueString = "";
        for (let i in dataToEnter) {
          let optionToSelect = dataToEnter[i];
          console.log(`selection option ${optionToSelect}`);
          if (i == "0") {
            valueString = optionToSelect;
          } else {
            valueString = valueString + ", " + optionToSelect;
          }

          let dropdownOption = this.page
            .locator("div[class*='option']")
            .filter({
              hasText: optionToSelect,
            });

          let attempts = 0;
          const maxAttempts = 5;
          let found = false;
          while (attempts < maxAttempts) {
            try {
              await expect(dropdownOption).toBeVisible({ timeout: 10000 });
              found = true;
              break;
            } catch (error) {
              console.log(
                `dropdown option ${optionToSelect} not visible, trying again`
              );
              try {
                await multiSelectDropdownContainer.click({
                  force: true,
                  timeout: 10000,
                });
              } catch (error) {
                console.log("error in clicking on multiselect dropdown");
                return;
              }
              attempts++;
            }
          }

          if (!found) {
            throw Error(
              `dropdown option ${optionToSelect} not visible after ${maxAttempts} attempts`
            );
          }

          await dropdownOption.click();

          console.log(`selected option ${optionToSelect}`);
          // if next item is visible (count is 1) then click on it
          dropdownOption = this.page.locator("div[class*='option']").filter({
            hasText: dataToEnter[parseInt(i) + 1],
          });
          if ((await dropdownOption.count()) != 1) {
            await this.page
              .getByText(valueString, {
                exact: true,
              })
              .click();
          }
        }

        break;
      }
      case "dropdown": {
        // in this case list of options should be given to select
        let dropdownContainer: Locator = await regFieldContainer;
        await this.page.waitForTimeout(1000);
        await dropdownContainer.click({
          force: true,
        });
        await this.page.waitForTimeout(1000);
        // await dropdownContainer.locator("div[class$='control']").click();
        let dropdownOption = this.page.locator("div[class$='option']").filter({
          hasText: dataToEnter,
        });
        if (fieldLabel == "countryembed") {
          console.log("Changing the locator");
          dropdownOption = dropdownOption.nth(1);
        }
        //clicking on dropdown option to select
        await dropdownOption.click();
        break;
      }
      case "number": {
        //works same as input box
        let inputBox = await regFieldContainer;
        await inputBox.fill(dataToEnter);
        break;
      }
      default: {
        throw Error(
          `Invalid field type ${fieldType} passed for field label ${fieldLabel}`
        );
      }
    }
  }

  async getTheRegFieldContainer(fieldLabel: string, fieldType: string) {
    let defaultRegFields = ["First Name", "Last Name", "Email"];
    let thisRegFieldContainer: Locator;
    if (defaultRegFields.includes(fieldLabel)) {
      console.log(fieldLabel, " is default reg field.");
      thisRegFieldContainer = this.page.getByLabel(`${fieldLabel}`);
    } else if (
      fieldLabel == "countryembed" ||
      fieldLabel == "Which city has the best food?"
    ) {
      thisRegFieldContainer = this.page
        .locator("div[class^=' css-ilpwti']")
        .first();
    } else if (fieldType == "multiselect-dropdown") {
      // get the div which contains the text field label and type is div
      // regex for checking the text is present (not exact match but contains)
      let labelElement = this.page.getByText(fieldLabel, {
        exact: true,
      });
      let labelElementParent = labelElement.locator("..");
      thisRegFieldContainer = labelElementParent.locator(
        ".css-u16d66-control > .css-1d8n9bt"
      );
    } else if (fieldType == "dropdown") {
      let labelElement = this.page.getByText(fieldLabel, {
        exact: true,
      });
      let labelElementParent = labelElement.locator("..");

      thisRegFieldContainer = labelElementParent.locator(".css-ilpwti");
    } else {
      console.log(fieldLabel, " is not default reg field.");
      if (fieldType == "text" || fieldType == "number") {
        thisRegFieldContainer = this.page.getByLabel(`${fieldLabel}`);
      } else {
        thisRegFieldContainer = this.page.getByText(`${fieldLabel}`).nth(1);
      }
    }
    // verify the count is 1, otherwise throw error cz we expect to find only 1 unique locator
    let count = await thisRegFieldContainer.count();
    console.log("The count is:", count);
    // if( count == 0){
    //   import('camelcase').then((camelcase) => {
    //     // use camelcase module here
    //     fieldLabel = camelcase.default(fieldLabel);
    //     console.log("fieldLabel after camel case is:",fieldLabel);
    //   }).catch((error) => {
    //     // handle error here
    //   });
    //   thisRegFieldContainer = this.page.getByTestId(fieldLabel);
    // }
    await expect(
      thisRegFieldContainer,
      `Field with label ${fieldLabel} not found`
    ).toHaveCount(1);
    return thisRegFieldContainer;
  }

  async isVisible() {
    await expect(this.registerNowButton).toBeVisible();
  }

  async isNotVisible() {
    await expect(
      this.registerNowButton,
      "expecting reg form to be hideen"
    ).toBeHidden();
  }

  async isAlreadyRegisteredButtonVisible() {
    await expect(
      this.alreadyRegisteredButton,
      "expecting already registered button to be visible"
    ).toBeVisible();
    return true;
  }

  async verifyThisFieldIsRequiredErrorMessageIsDisplayed() {
    let errorMessage = this.page.locator("text=This field is required").first();
    await expect(
      errorMessage,
      "expecting atleast 1 error message on mandatory field is visisble"
    ).toBeVisible();
  }

  async isRegistrationFormVisible() {
    let firstRegFieldLocator = this.page
      .locator("div[class^='styles-module__formSectionRowContainer']")
      .nth(1);
    await expect(firstRegFieldLocator).toBeVisible();
  }

  async errorToastMessageAppearsToUseBusinessEmails() {
    await expect(this.notificationMessage).toHaveText(
      "This email domain is restricted. Try a different email."
    );
  }
}
