import { expect, Locator, Page } from "@playwright/test";
import { RegistrationFormComponent } from "./RegistrationFormComponent";


export class LandingPage {
  readonly page: Page;
  readonly registrationFormComponent: RegistrationFormComponent;
  readonly notificationContainer: Locator;
  readonly disclaimer: Locator;
  readonly registrationNotAvailable: Locator;

    constructor(page:Page){
        this.page = page
        this.registrationFormComponent = new RegistrationFormComponent(page)
        this.notificationContainer = this.page.locator(
          "div[class^='styles-module__notificationContainer']"
        );
        this.disclaimer = this.page.locator("text=Please accept these conditions to register:")
        this.registrationNotAvailable = this.page.locator("text=Registration for this event is not available. Contact your organizer.")
    }



  async getRegistrationFormComponent() {
    return await this.registrationFormComponent;
  }

  async verifyDisclaimer() {
    await expect(this.disclaimer).toBeVisible();
  }

  async verifyRegistrationNotAvailable() {
    await expect(this.registrationNotAvailable).toBeVisible();
  }

  async load(url: string) {
    console.log(`Loading url ${url}`);
    await this.page.goto(url);
    console.log(`url loaded`);
  }
}
