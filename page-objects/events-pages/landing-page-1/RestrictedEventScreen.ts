import {expect, Page} from '@playwright/test'
export class RestrictedEventScreen{
    readonly page:Page
    constructor(page:Page){
        this.page = page
    }

    async isPrivateEventScreenVisible(){
        await expect(this.page.locator("div[class^='styles-module__mainHeading']")).toHaveText('Private Event')
    }

    async isPrivateEventScreenVisibleForOTPFlow(){
        await expect(this.page.locator("text=Entered email has no access for this event")).toBeVisible()

    }
    
    async isEventEndedScreenisVisible(){
        await expect(this.page.locator("text=Event has ended!")).toBeVisible()

    }

    
}