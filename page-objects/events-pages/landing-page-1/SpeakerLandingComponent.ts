import {Page} from '@playwright/test'
import { LoginOptionsComponent } from './LoginOptionsComponent'
export class SpeakerLandingComponent{
    readonly page:Page
    constructor(page:Page){
        this.page = page
    }

    async clickOnLoginButton(){
        await this.page.click("text=Log in")
        return new LoginOptionsComponent(this.page)
    }


}