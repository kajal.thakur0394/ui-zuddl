import { expect, Page, Locator } from "@playwright/test";
import { LoginOptionsComponent } from "./LoginOptionsComponent";
export class RegistrationConfirmationComponent {
  readonly page: Page;
  readonly backButton: Locator;

  constructor(page: Page) {
    this.page = page;
    this.backButton = this.page.locator(
      "div[class^='styles-module__backArrow']"
    );
  }

  async isRegistrationConfirmationMessageVisible(
    events_with_magic_link: boolean = false
  ) {
    let messageLocator: Locator = this.page.locator(
      "div[class^='styles-module__logoHeaderBody']"
    );
    if (events_with_magic_link) {
      await expect(
        messageLocator,
        "Checking registration confirmation message for events with magic link"
      ).toContainText("Check your email for the event invite");
    }
    await expect(
      messageLocator,
      "Checking registration confirmation message for events without magic link"
    ).toContainText("You've successfully registered!");
  }

  async clickOnAlreadyRegisteredButton() {
    await this.page.locator("text=Already registered? Log in").click();
    return new LoginOptionsComponent(this.page);
  }

  async isAlreadyRegisteredMessageVisible() {
    await expect(
      this.page.locator("text=You've already registered")
    ).toBeVisible();
  }

  async clickOnBackButton() {
    await this.backButton.click();
  }
}
