import { expect, Locator, Page } from "@playwright/test";

export class ResetPasswordComponent {
  //selectors
  readonly page: Page;
  readonly setPasswordInputBox: Locator;
  readonly confirmPasswordInputBox: Locator;
  readonly continueButton: Locator;
  readonly notificationContainer: Locator;

  //constructor
  constructor(page: Page) {
    this.page = page;
    this.setPasswordInputBox = this.page.locator(
      "input[data-testid='set-password']"
    );
    this.confirmPasswordInputBox = this.page.locator(
      "input[data-testid='confirm-password']"
    );
    this.continueButton = this.page.locator("button:has-text('Continue')");
    this.notificationContainer = this.page.locator(
      "div[class^='styles-module__notificationContainer']"
    );
  }

  //actions
  async setPassword(userPassword: string) {
    //wait for notification toast message to disappear
    await this.setPasswordInputBox.fill(userPassword);
    await this.page.keyboard.press("Tab");
    await this.confirmPasswordInputBox.fill(userPassword);
    await this.continueButton.click();
  }
}
