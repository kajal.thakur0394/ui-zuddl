import { expect, Locator, Page } from "@playwright/test";
import { RegistrationFormComponent } from "./RegistrationFormComponent";
import { UserEventRoleDto } from "../../../dto/userEventRoleDto";
import EventEntryType from "../../../enums/eventEntryEnum";
import EventRole from "../../../enums/eventRoleEnum";
import { ResetPasswordComponent } from "./ResetPassswordComponent";
import { RestrictedEventScreen } from "./RestrictedEventScreen";
import { RestrictedSpeakerAccessPage } from "./RestrcitedSpeakerAccessPage";
import EventAuthType from "../../../enums/eventAuthTypeEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

export class LoginOptionsComponent {
  //selectors
  readonly page: Page;
  readonly emailInputBox: Locator; //common input box used in otp as well as in pwd flow as well
  readonly submitButton: Locator;
  readonly loginViaPasswordButton: Locator;
  readonly enterNowButton: Locator; // this comes as submit button on login via password screen on submitting email
  readonly enterPasswordBox: Locator; // input box to set password
  readonly continueButton: Locator;
  readonly otpInputBoxes: Locator;
  readonly forgotPasswordButton: Locator;
  readonly resetPasswordComponent: ResetPasswordComponent;
  readonly emailInputBoxForOTP: Locator; //email input box for otp
  //social login buttons
  readonly googleLoginButton: Locator;
  readonly linkedinLoginButton: Locator;
  readonly facebookLoginButton: Locator;
  readonly ssoLoginButton: Locator;

  //constructor
  constructor(page: Page) {
    this.page = page;
    this.emailInputBox = this.page.locator("input[name='emailInput']"); //header on this form with this text
    // this.emailInputBoxForOTP = this.page.locator("input[name='email']");
    this.emailInputBoxForOTP = this.page
      .getByPlaceholder("Enter email")
      .first();
    this.submitButton = this.page
      .getByRole("button", { name: "Submit" })
      .first();
    this.loginViaPasswordButton = this.page.locator(
      "p:has-text('Log in with password')"
    );
    this.enterNowButton = this.page.locator("button:has-text('Enter Now')");
    this.enterPasswordBox = this.page.locator(
      "input[data-testid='enter-password']"
    );
    this.continueButton = this.page.locator("button:has-text('Continue')");
    this.otpInputBoxes = this.page.locator(
      "input[class^='styles-module__otpInput']"
    );
    this.forgotPasswordButton = this.page.locator("text=Forgot your password?");
    this.resetPasswordComponent = new ResetPasswordComponent(this.page);

    //social login button
    this.googleLoginButton = this.page.locator(
      "div[class^='styles-module__socialButtonContainer'] button"
    );
    // this.googleLoginButton = this.page
    //   .locator("div[class^='styles-module__socialButtonContainer'] button div")
    //   .nth(0);
    this.facebookLoginButton = this.page
      .locator("div[class^='styles-module__socialButtonContainer'] button")
      .nth(1);
    this.linkedinLoginButton = this.page
      .locator("div[class^='styles-module__socialButtonContainer'] button")
      .nth(2);
    this.ssoLoginButton = this.page.locator(
      "div[class^='styles-module__ssoLoginButton'] button div"
    );
  }

  // async submitEmailForOTPVerification(email_add:string){
  //     await this.emailInputBox.fill(email_add)
  //     await this.submitButton.click()
  // }

  async submitEmailForOtpVerification(
    userEventRoleDto: UserEventRoleDto,
    originPage = "attendeeLandingPage"
  ) {
    console.log(
      `handling OTP auth for ${userEventRoleDto.userInfoDto.userEmail}`
    );
    await this.page.waitForTimeout(2000);
    await this.emailInputBoxForOTP.fill(userEventRoleDto.userInfoDto.userEmail);
    await this.submitButton.click();
    if (originPage == "attendeeLandingPage") {
      if (
        userEventRoleDto.eventInfoDto.eventEntryType ==
        EventEntryType.INVITE_ONLY
      ) {
        if (
          userEventRoleDto.userRoleInThisEvent == EventRole.NOTEXISTS &&
          !userEventRoleDto.hasUserRegisteredToThisEvent
        ) {
          // show invite only event screen
          return new RestrictedEventScreen(this.page);
        } else {
          // let otp = await fetchOtpFromEmail(userEventRoleDto.userInfoDto.userEmail,
          //     userEventRoleDto.eventInfoDto.eventTitle)
          let otp = await QueryUtil.fetchLoginOTPForEvent(
            userEventRoleDto.userInfoDto.userEmail,
            userEventRoleDto.eventInfoDto.eventId
          );
          await this.enterAndSubmitOtp(otp);
        }
      } else {
        if (!userEventRoleDto.hasUserRegisteredToThisEvent) {
          return new RegistrationFormComponent(this.page);
        }
        // let otp = await fetchOtpFromEmail(userEventRoleDto.userInfoDto.userEmail,
        //     userEventRoleDto.eventInfoDto.eventTitle)
        let otp = await QueryUtil.fetchLoginOTPForEvent(
          userEventRoleDto.userInfoDto.userEmail,
          userEventRoleDto.eventInfoDto.eventId
        );
        await this.enterAndSubmitOtp(otp);
      }
    } else {
      console.log("handing the otp authentication on speaker  landing page");
      if (
        userEventRoleDto.userRoleInThisEvent == EventRole.NOTEXISTS ||
        userEventRoleDto.userRoleForThisEvent == EventRole.ATTENDEE
      ) {
        return new RestrictedSpeakerAccessPage(this.page);
      } else {
        console.log(
          `User has an event role already ${userEventRoleDto.userRoleForThisEvent}`
        );
        // let otp = await fetchOtpFromEmail(userEventRoleDto.userInfoDto.userEmail,
        //     userEventRoleDto.eventInfoDto.eventTitle)
        let otp = await QueryUtil.fetchLoginOTPForEvent(
          userEventRoleDto.userInfoDto.userEmail,
          userEventRoleDto.eventInfoDto.eventId
        );
        await this.enterAndSubmitOtp(otp);
      }
    }
  }

  async submitEmailForMagicLink(userEventRoleDto: UserEventRoleDto) {
    await this.emailInputBox.fill(userEventRoleDto.userInfoDto.userEmail);
    await this.page.waitForTimeout(1000);
    await this.submitButton.click();
    if (
      userEventRoleDto.eventInfoDto.eventEntryType == EventEntryType.INVITE_ONLY
    ) {
      if (userEventRoleDto.userRoleInThisEvent == EventRole.NOTEXISTS) {
        // show invite only event screen
        return new RestrictedEventScreen(this.page);
      }
    }
  }

  async clickOnSubmitButton() {
    await this.submitButton.click();
  }

  async enterAndSubmitOtp(otp) {
    console.log(`trying to enter otp ${otp} in the otp boxes`);
    if (otp.length != 6) {
      throw new Error(`OTP length should be 6 but we got otp as ${otp}`);
    }
    await expect(
      this.otpInputBoxes.first(),
      "Expecting otp screen to be visible"
    ).toBeVisible({ timeout: 15000 });

    for (let i = 0; i < 6; i++) {
      await this.otpInputBoxes.nth(i).fill(otp[i]);
    }
    //submit
    console.log(`after entering otp clicking on continue button`);
    await this.continueButton.click();
  }

  async submitEmailForPasswordLogin(
    userEventRoleDto: UserEventRoleDto,
    originPage = "attendeeLandingPage",
    useForgotPassword: boolean = false
  ) {
    await this.page.waitForTimeout(2000);
    await this.loginViaPasswordButton.click();
    await expect(
      this.loginViaPasswordButton,
      "waiting for login via password screen to disappear"
    ).not.toBeVisible();
    await this.emailInputBox.fill(userEventRoleDto.userInfoDto.userEmail);
    await this.enterNowButton.click();
    await this.page.waitForTimeout(10000); //waiting for 10 seconds timeout
    // handle next steps based on diff conditions
    if (
      userEventRoleDto.eventInfoDto.eventEntryType == EventEntryType.REG_BASED
    ) {
      console.log(
        "Handling password login flow for reg based event entry type"
      );
      await this.handlePasswordFlowForRegBasedEvent(
        userEventRoleDto,
        originPage,
        useForgotPassword
      );
    } else if (
      userEventRoleDto.eventInfoDto.eventEntryType == EventEntryType.INVITE_ONLY
    ) {
      console.log(
        "Handling password login flow for invite based event entry type"
      );
      await this.handlePasswordFlowForInviteBasedEvent(
        userEventRoleDto,
        originPage,
        useForgotPassword
      );
    }
    //else can be added for ticketed event in case
  }

  async handlePasswordFlowForRegBasedEvent(
    userEventRoleDto: UserEventRoleDto,
    originPage = "attendeeLandingPage",
    useForgotPassword: boolean = false
  ) {
    //you have submitted your email to trigger password flow but what happens next is question
    if (originPage == "attendeeLandingPage") {
      console.log("Handling password login flow for attendee landing page");
      await this.handlePasswordFlowForRegBasedEventOnAttendeeLandingPage(
        userEventRoleDto,
        useForgotPassword
      );
    } else {
      // when origin page == 'speakerLandingPage'
      console.log("Handling password login flow for speaker landing page");
      await this.handlePasswordFlowForRegBasedEventOnSpeakerLandingPage(
        userEventRoleDto,
        useForgotPassword
      );
    }
  }

  async handlePasswordFlowForRegBasedEventOnAttendeeLandingPage(
    userEventRoleDto: UserEventRoleDto,
    useForgotPassword: boolean = false
  ) {
    let userInfo = userEventRoleDto.userInfoDto;
    if (
      !userEventRoleDto.hasUserRegisteredToThisEvent &&
      !userInfo.hasEntryInAccountTable
    ) {
      // return the registration form component
      await this.submitButton.click();
      return new RegistrationFormComponent(this.page);
    } else if (
      !userEventRoleDto.hasUserRegisteredToThisEvent &&
      userInfo.hasEntryInAccountTable
    ) {
      return new RegistrationFormComponent(this.page);
    }
    // handle registered user cases
    if (
      userEventRoleDto.hasUserRegisteredToThisEvent &&
      !userInfo.hasEntryInAccountTable
    ) {
      // initiate otp flow for the user
      console.log(
        "initiating otp flow since user does not have entry in account table"
      );
      await this.submitButton.click();
      console.log("clicked on submit button");
      // let otp = await fetchOtpFromEmail(userEventRoleDto.userInfoDto.userEmail,
      //     userEventRoleDto.eventInfoDto.eventTitle)
      let otp = await QueryUtil.fetchLoginOTPForEvent(
        userEventRoleDto.userInfoDto.userEmail,
        userEventRoleDto.eventInfoDto.eventId
      );
      console.log("fetched otp for this flow is" + otp);
      console.log("now attempting to enter the otp");
      await this.enterAndSubmitOtp(otp);
      // await this.resetPasswordComponent.setPassword("Zuddl@123")
    } else {
      await this.enterPasswordBox.fill(
        userEventRoleDto.userInfoDto.userPassword
      );
      await this.continueButton.click();
    }
  }

  async handlePasswordFlowForRegBasedEventOnSpeakerLandingPage(
    userEventRoleDto: UserEventRoleDto,
    useForgotPassword: boolean = false
  ) {
    let userInfo = userEventRoleDto.userInfoDto;
    if (
      userEventRoleDto.userRoleInThisEvent == EventRole.ATTENDEE ||
      userEventRoleDto.userRoleInThisEvent == EventRole.NOTEXISTS
    ) {
      console.log(
        "user is either attendee or does not have event role, hence sending him to restricted login page"
      );
      return new RestrictedSpeakerAccessPage(this.page);
    }
    // for speakers, organisers and moderators
    if (!userInfo.hasEntryInAccountTable) {
      // no entry in account table
      console.log(
        "invited user has account entry, hence expecting him to trigger otp flow to set password"
      );
      await this.submitButton.click();
      // let otp = await fetchOtpFromEmail(userEventRoleDto.userInfoDto.userEmail,
      //     userEventRoleDto.eventInfoDto.eventTitle)
      let otp = await QueryUtil.fetchLoginOTPForEvent(
        userEventRoleDto.userInfoDto.userEmail,
        userEventRoleDto.eventInfoDto.eventId
      );
      await this.enterAndSubmitOtp(otp);
      // await this.resetPasswordComponent.setPassword("Zuddl@123")
    } else {
      // have entry in account table
      console.log(
        "invited user has account entry, hence expecting him to enter password"
      );
      if (useForgotPassword) {
        console.log("using forgot password flow to login via password");
        await this.forgotPasswordButton.click();
        await this.submitButton.click();
        // let otp = await fetchOtpFromEmail(userEventRoleDto.userInfoDto.userEmail,
        // userEventRoleDto.eventInfoDto.eventTitle)
        let otp = await QueryUtil.fetchLoginOTPForEvent(
          userEventRoleDto.userInfoDto.userEmail,
          userEventRoleDto.eventInfoDto.eventId
        );
        await this.enterAndSubmitOtp(otp);
        await this.resetPasswordComponent.setPassword("Zuddl@123");
        userEventRoleDto.userInfoDto.userPassword = "Zuddl@123";
      } else {
        console.log("entering the password");
        await this.enterPasswordBox.fill(
          userEventRoleDto.userInfoDto.userPassword
        );
        await this.continueButton.click();
      }
    }
  }

  async handlePasswordFlowForInviteBasedEvent(
    userEventRoleDto: UserEventRoleDto,
    originPage = "attendeeLandingPage",
    useForgotPassword: boolean = false
  ) {
    let userInfo = userEventRoleDto.userInfoDto;
    let eventInfo = userEventRoleDto.eventInfoDto;
    //you have submitted your email to trigger password flow but what happens next is question
    if (originPage == "attendeeLandingPage") {
      await this.handlePasswordFlowForInviteBasedEventOnAttendeeLandingPage(
        userEventRoleDto,
        useForgotPassword
      );
    } else {
      await this.handlePasswordFlowForInviteBasedEventOnSpeakerLandingPage(
        userEventRoleDto,
        useForgotPassword
      );
    }
  }

  async handlePasswordFlowForInviteBasedEventOnAttendeeLandingPage(
    userEventRoleDto: UserEventRoleDto,
    useForgotPassword: boolean = false
  ) {
    let userInfo = userEventRoleDto.userInfoDto;
    if (!userEventRoleDto.hasUserRegisteredToThisEvent) {
      //return the restricted screen if attendee is not registered/invited
      return new RestrictedEventScreen(this.page);
    }
    // handle registered user cases
    if (!userInfo.hasEntryInAccountTable) {
      // initiate otp flow for the user
      // await this.page.pause();
      console.log(
        `Triggering otp flow for this user by clicking on submit otp button`
      );
      await this.submitButton.click();
      // let otp = await fetchOtpFromEmail(userEventRoleDto.userInfoDto.userEmail,
      //     userEventRoleDto.eventInfoDto.eventTitle)
      let otp = await QueryUtil.fetchLoginOTPForEvent(
        userEventRoleDto.userInfoDto.userEmail,
        userEventRoleDto.eventInfoDto.eventId
      );
      await this.enterAndSubmitOtp(otp);
      // await this.resetPasswordComponent.setPassword("Zuddl@123")
    } else {
      await this.enterPasswordBox.fill(
        userEventRoleDto.userInfoDto.userPassword
      );
      await this.continueButton.click();
    }
  }

  async handlePasswordFlowForInviteBasedEventOnSpeakerLandingPage(
    userEventRoleDto: UserEventRoleDto,
    useForgotPassword: boolean = false
  ) {
    let userInfo = userEventRoleDto.userInfoDto;
    if (
      !userEventRoleDto.hasUserRegisteredToThisEvent ||
      userEventRoleDto.userRoleInThisEvent == EventRole.NOTEXISTS
    ) {
      return new RestrictedSpeakerAccessPage(this.page);
    }
    if (!userInfo.hasEntryInAccountTable) {
      await this.submitButton.click();
      // let otp = await fetchOtpFromEmail(userEventRoleDto.userInfoDto.userEmail,
      //     userEventRoleDto.eventInfoDto.eventTitle)
      let otp = await QueryUtil.fetchLoginOTPForEvent(
        userEventRoleDto.userInfoDto.userEmail,
        userEventRoleDto.eventInfoDto.eventId
      );
      await this.enterAndSubmitOtp(otp);
      // await this.resetPasswordComponent.setPassword("Zuddl@123")
    } else {
      await this.enterPasswordBox.fill(
        userEventRoleDto.userInfoDto.userPassword
      );
      await this.continueButton.click();
    }
  }

  async clickOnLoginOption(loginOption: EventAuthType) {
    console.log(`clicking on auth option ${loginOption}`);
    if (loginOption == EventAuthType.SSO) {
      await this.ssoLoginButton.click();
    } else {
      await this.googleLoginButton.click();
    }
  }
}

/* reg based event (attendee landing page)
    1. new user who comes to the event landing page
        - he registered
        - otp flow will get triggered
    2. existing user who does not have password but have account table entry
        - he registered
        - he will get enter password screen
    3. user have password:
        - enter password
    
Invite based event (attendee landing page)
    1. if a user is registered

Speaker landing page
    1. 
*/
