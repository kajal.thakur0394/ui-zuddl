import { expect, Locator, Page } from "@playwright/test";
import { RegistrationFormComponent } from "./RegistrationFormComponent";
import { LoginOptionsComponent } from "./LoginOptionsComponent";
import { RestrictedEventScreen } from "./RestrictedEventScreen";
import { SpeakerLandingComponent } from "./SpeakerLandingComponent";
import { RegistrationConfirmationComponent } from "./RegistrationConfirmationComponent";
import { RestrictedSpeakerAccessPage } from "./RestrcitedSpeakerAccessPage";
import { DateTimecounterComponent } from "./DateTimecounterComponent";
import { GuestAuthFormComponent } from "./GuestAuthFormComponent";
import { AddToCalendar } from "./addToCalendar";
import { LobbyPage } from "../zones/LobbyPage";

export class LandingPageOne {
  readonly page: Page;
  readonly registrationFormComponent: RegistrationFormComponent;
  readonly loginOptionsComponent: LoginOptionsComponent;
  readonly restrictedEventScreenComponent: RestrictedEventScreen;
  readonly speakerLandingComponent: SpeakerLandingComponent;
  readonly registrationConfirmationComponent: RegistrationConfirmationComponent;
  readonly restrictedSpeakerAccessPage: RestrictedSpeakerAccessPage;
  readonly notificationContainer: Locator;
  readonly enterNowButton: Locator;
  readonly eventTitle: Locator;
  readonly dateTimeCounter: DateTimecounterComponent;
  readonly loginAsGuestButton: Locator;
  readonly addToCalendar: AddToCalendar;
  readonly signoutButton: Locator;
  readonly deactivatedEmailToast: Locator;

  constructor(page: Page) {
    this.page = page;
    this.registrationFormComponent = new RegistrationFormComponent(page);
    this.loginOptionsComponent = new LoginOptionsComponent(page);
    this.restrictedEventScreenComponent = new RestrictedEventScreen(page);
    this.speakerLandingComponent = new SpeakerLandingComponent(page);
    this.registrationConfirmationComponent =
      new RegistrationConfirmationComponent(page);
    this.restrictedSpeakerAccessPage = new RestrictedSpeakerAccessPage(page);
    this.notificationContainer = this.page.locator(
      "div[class^='styles-module__notificationContainer']"
    );
    this.enterNowButton = this.page.locator("button:has-text('Enter Now')");
    this.eventTitle = this.page.locator(
      "div[class^='styles-module__eventTitle']"
    );
    this.dateTimeCounter = new DateTimecounterComponent(page);
    this.addToCalendar = new AddToCalendar(page);
    this.loginAsGuestButton = this.page.locator(
      "div[class^='styles-module__guestLoginText']"
    );
    this.signoutButton = this.page.getByRole("button", { name: "Sign Out" });
    this.deactivatedEmailToast = this.page.getByText(
      "This email is deactivated from this event"
    );
  }

  async getLoginOptionsComponent() {
    return await this.loginOptionsComponent;
  }

  async getRegistrationFormComponent() {
    return this.registrationFormComponent;
  }

  async getRestrcitedEventScreen() {
    return await this.restrictedEventScreenComponent;
  }

  async getAddToCalendarComponenet() {
    return await this.addToCalendar;
  }

  async clickOnLoginAsGuest() {
    await this.loginAsGuestButton.click();
    return new GuestAuthFormComponent(this.page);
  }
  async load(url: string) {
    console.log(`Loading url ${url}`);
    await this.page.goto(url);
    console.log(`url loaded`);
    await this.page.waitForTimeout(5000);
  }

  async getSpeakerLandingComponent() {
    return await this.speakerLandingComponent;
  }

  async getDateTimeCounterComponent() {
    return await this.dateTimeCounter;
  }

  //toast message on submitting email for otp when magic link enabled
  async isSuccessToastMessageVisible() {
    //toast message on submitting email for otp when magic link enabled
    await expect(
      this.page.locator("div[class^='styles-module__notificationContainer']")
    ).toContainText("success");
  }

  async isNotificationToastVisibleOnEnterNowPage() {
    await this.notificationContainer.waitFor({ state: "visible" });
    return await this.notificationContainer.isVisible();
  }
  async clickOnEnterNowButton() {
    await expect(
      this.enterNowButton,
      `expecting enter now button to be visible before i click on it`
    ).toBeVisible();
    await this.enterNowButton.click();
  }

  async waitForToastMessageToDisappear() {
    try {
      await this.page.waitForSelector(
        "div[class^='styles-module__notificationContainer']",
        {
          state: "visible",
        }
      );
      // await this.page.waitForSelector("div[class^='styles-module__notificationContainer']",{
      //     state:"hidden"
      // })
      // await expect(this.notificationContainer).toBeHidden({timeout:15000})
      await this.page
        .locator("div[class^='styles-module__notificationContainer'] button")
        .click();
    } catch (err) {
      console.error(
        `${err} occured while trying to click on notification container button`
      );
    }
  }

  async getRegistrationConfirmationScreen() {
    return await this.registrationConfirmationComponent;
  }

  async getEventRestrictionSpeakersPage() {
    return await this.restrictedSpeakerAccessPage;
  }

  async clickOnLoginButton() {
    //this comes on speaker landing page
    await this.page.waitForTimeout(2000);
    await this.page.click("text=Log in");
    return new LoginOptionsComponent(this.page);
  }

  async isLobbyLoaded() {
    await this.page.waitForURL(/lobby/, {
      timeout: 20000,
    });
    return new LobbyPage(this.page);
  }

  async isStageLoaded() {
    await this.page.waitForURL(/stage/, {
      timeout: 20000,
    });
  }

  async clickSignout() {
    await this.signoutButton.click();
  }
}
