import { Page, Locator } from "@playwright/test";
const fs = require("fs");

export class AddToCalendar {
  readonly page: Page;
  constructor(page: Page) {
    this.page = page;
  }

  async fetchCalendarLinksFromAddToCalendarButton() {
    let addTooCalendarButton: Locator = this.page.locator(
      'a[class^="react-add-to-calendar__button"]'
    );
    await addTooCalendarButton.click();
    // Downloading Apple ics data
    const downloadPromise = this.page.waitForEvent("download");
    await this.page.locator('a[class^="apple-link"]').click();
    const download = await downloadPromise;
    // Wait for the download process to complete
    console.log(await download.path());
    // Save downloaded file somewhere
    const AppleICSFile = await fs.readFileSync(await download.path());
    let AppleIcsfileName =
      "apple" + Date.now().toString() + download.suggestedFilename();
    console.log("Appleics file", AppleIcsfileName);
    fs.writeFileSync(AppleIcsfileName, AppleICSFile);

    // Downloading Outlook ICS data
    await addTooCalendarButton.click();
    const outlookDownloadPromise = this.page.waitForEvent("download");
    await this.page.locator('a[class^="outlook-link"]').click();
    const outlookDownload = await outlookDownloadPromise;
    // Save downloaded file somewhere
    const OutlookICSFile = await fs.readFileSync(await outlookDownload.path());
    let OutlookIcsfileName =
      "Outlook" + Date.now().toString() + outlookDownload.suggestedFilename();
    fs.writeFileSync(OutlookIcsfileName, OutlookICSFile);
    console.log(OutlookIcsfileName);

    // Fetching calendar links
    await addTooCalendarButton.click();
    let googleInviteLink = await this.page
      .locator('a[class^="google-link"]')
      .getAttribute("href");
    console.log("Google invite link", googleInviteLink);
    let yahooInviteLink = await this.page
      .locator('a[class^="yahoo-link"]')
      .getAttribute("href");
    console.log("Yahoo invite link", yahooInviteLink);
    return [
      AppleIcsfileName,
      OutlookIcsfileName,
      googleInviteLink,
      yahooInviteLink,
    ];
  }
}
