import { expect, Page } from "@playwright/test";
export class DateTimecounterComponent {
  readonly page: Page;
  constructor(page: Page) {
    this.page = page;
  }

  async isCorrectDatetimeDisplayed(
    eventStartDateinAttendeeTimezone,
    eventEndDateinAttendeeTimezone
  ) {
    if (
      new Date(eventStartDateinAttendeeTimezone).getDate() !=
      new Date(eventEndDateinAttendeeTimezone).getDate()
    ) {
      let expectedStartDate = new Date(
        eventStartDateinAttendeeTimezone
      ).toString();
      console.log(expectedStartDate);
      expectedStartDate =
        expectedStartDate.split(" ")[0] +
        ", " +
        new Date(eventStartDateinAttendeeTimezone).getDate() +
        " " +
        expectedStartDate.split(" ")[1] +
        ", " +
        expectedStartDate.split(" ")[3];
      let expectedEndDate = new Date(eventEndDateinAttendeeTimezone).toString();
      console.log(expectedEndDate);
      expectedEndDate =
        expectedEndDate.split(" ")[0] +
        ", " +
        new Date(eventEndDateinAttendeeTimezone).getDate() +
        " " +
        expectedEndDate.split(" ")[1] +
        ", " +
        expectedEndDate.split(" ")[3];
      let expectedDate = expectedStartDate + " -  " + expectedEndDate;
      console.log("Expected Date and day:", expectedDate);
      await expect(
        this.page
          .locator('div[class^="styles-module__dateTimeContainer"]')
          .nth(1)
      ).toHaveText(expectedDate);
    } else {
      console.log(eventStartDateinAttendeeTimezone);
      let expectedStartDateTime = new Date(eventStartDateinAttendeeTimezone)
        .toLocaleString("en-US", {
          hour: "numeric",
          minute: "numeric",
          hour12: true,
        })
        .toString();
      console.log(expectedStartDateTime);
      let expectedEndtDateTime = new Date(eventEndDateinAttendeeTimezone)
        .toLocaleString("en-US", {
          hour: "numeric",
          minute: "numeric",
          hour12: true,
        })
        .toString();
      console.log(expectedEndtDateTime);
      let expectedDateTime =
        expectedStartDateTime + " - " + expectedEndtDateTime;
      await expect(
        this.page
          .locator('div[class^="styles-module__dateTimeContainer"]')
          .nth(0)
      ).toContainText(expectedDateTime);
    }
  }
}
