import {expect, Page} from '@playwright/test'
export class RestrictedSpeakerAccessPage{
    readonly page:Page
    constructor(page:Page){
        this.page = page
    }

    async isVisible(){
        await expect(this.page.locator("text=You don't have access to this event"),
        "Restricted entry only for speakers page is visible"
        ).toBeVisible()
    }

    async chooseToGoToAttendeePage(){
        await expect(this.page.locator("span[class^='styles-module__attendeeLpRoute']"),
        "Expecting click here button present to go to attendee landing page"
        ).toBeVisible()
        await this.page.locator("span[class^='styles-module__attendeeLpRoute']").click()
    }

}