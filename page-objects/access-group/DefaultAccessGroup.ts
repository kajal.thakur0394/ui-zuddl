import { Locator, Page, expect } from "@playwright/test";
import { DataUtl } from "../../util/dataUtil";

export class DefaultAccessGroup {
  readonly page;
  readonly eventId;

  readonly moderatorLocator: Locator;
  readonly organizerLocator: Locator;
  readonly speakerLocator: Locator;
  readonly registrationsLocator: Locator;

  constructor(page: Page, eventId) {
    this.page = page;
    this.eventId = eventId;

    this.moderatorLocator = this.page.getByText("Moderators");
    this.organizerLocator = this.page.getByText("Organizers");
    this.speakerLocator = this.page.getByText("Speaker");
    this.registrationsLocator = this.page.getByText("Registrations");
  }

  private async navigateToPeopleSection() {
    let setupPageUrl = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/event/${this.eventId}/people/audiences`;

    await this.page.goto(setupPageUrl);
  }

  private async expandGroup(groupName: "Role" | "Status" | "Custom groups") {
    let groupLocator = this.page.getByText(groupName);
    let groupListLocator = this.page.locator(
      `//*[text()='${groupName}']/../../../following-sibling::div[contains(@class,'group-card_subGroupList')]`
    );

    await this.navigateToPeopleSection();
    await expect(groupLocator).toBeVisible({ timeout: 15000 });

    try {
      await expect(groupListLocator).toBeVisible({ timeout: 10000 });
    } catch (error) {
      await groupLocator.click();
    }
  }

  async verifyRoleVisibility(
    roleName: "Organizers" | "Moderators" | "Speakers" | "Registrations",
    visibilityStatus
  ) {
    let roleLocator = this.page.locator(
      `//p[text()="Role"]//../../../..//p[text()="${roleName}"]`
    );

    await this.expandGroup("Role");
    await expect(roleLocator).toBeVisible({ visible: visibilityStatus });
  }

  async verifyTheUserEmailVisibilityUnderRelevantSection(
    emailId: string,
    userType: "Speaker" | "Attendee",
    isVisible: boolean = true
  ) {
    await this.expandGroup("Role");

    let emailLocator = this.page.getByText(emailId);

    if (userType === "Speaker") {
      await this.verifyRoleVisibility("Speakers", true);
      let roleLocator = this.page.locator(
        `//p[text()="Role"]//../../../..//p[text()="Speakers"]`
      );
      await roleLocator.click();
    } else if (userType === "Attendee") {
      await this.verifyRoleVisibility("Registrations", true);
      let roleLocator = this.page.getByText(`Registrations`, { exact: true });
      await roleLocator.click();
    }

    await expect(emailLocator).toBeVisible({ visible: isVisible });
  }
}
