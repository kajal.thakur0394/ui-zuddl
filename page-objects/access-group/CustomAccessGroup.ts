import { Locator, Page, expect } from "@playwright/test";
import { DataUtl } from "../../util/dataUtil";
import { DefaultAccessGroup } from "./DefaultAccessGroup";
import {
  FilterOperators,
  FilterOperatorsUI,
  Filters,
} from "../../enums/AccessGroupEnum";

export class CustomAccessGroup {
  private readonly page;
  private readonly eventId;

  private readonly createCustomGroupButton: Locator;
  private readonly customGroupNameInput: Locator;
  private readonly activeGroupRadioButton: Locator;
  private readonly staticGroupRadioButton: Locator;
  private readonly enableAccessControlsToggle: Locator;
  private readonly nextButton: Locator;
  private readonly addFilterButton: Locator;
  private readonly applyFiltersButton: Locator;
  private readonly createGroupButton: Locator;
  private readonly customGroupsLocator: Locator;
  readonly defaultAccessGroupObject: DefaultAccessGroup;

  constructor(page: Page, eventId) {
    this.page = page;
    this.eventId = eventId;

    this.createCustomGroupButton = this.page.getByText("Create custom group");
    this.customGroupNameInput = this.page.getByPlaceholder("Enter group name");
    this.activeGroupRadioButton = this.page.locator(
      "//*[text()='Active group']/../..//input"
    );
    this.staticGroupRadioButton = this.page.locator(
      "//*[text()='Static group']/../..//input"
    );
    this.enableAccessControlsToggle = this.page.locator("label").nth(1);
    this.nextButton = this.page.getByText("Next");
    this.addFilterButton = this.page.getByText("Add filter").first();
    this.applyFiltersButton = this.page.getByRole("button", {
      name: "Apply filters",
    });
    this.createGroupButton = this.page.getByRole("button", {
      name: "Create group",
    });
    this.customGroupsLocator = this.page.getByText("Custom groups");

    this.defaultAccessGroupObject = new DefaultAccessGroup(
      this.page,
      this.eventId
    );
  }

  get getDefaultAccessGroup() {
    return this.defaultAccessGroupObject;
  }

  private async navigateToPeopleSection() {
    let peopleSectionUrl = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/event/${this.eventId}/people/audiences`;

    await this.page.goto(peopleSectionUrl);
  }

  async createCustomAccessGroup({
    groupName,
    groupType,
    enableAccessControls,
    filters,
    ifConditions,
    conditionData,
    filterLogic,
  }: CustomGroupDatatype) {
    await this.navigateToPeopleSection();
    await this.createCustomGroupButton.click();

    await this.customGroupNameInput.fill(groupName);
    if (groupType === "Active") await this.activeGroupRadioButton.check();
    else await this.staticGroupRadioButton.check();

    if (enableAccessControls) await this.enableAccessControlsToggle.check();
    else await this.enableAccessControlsToggle.uncheck();

    await this.nextButton.click();

    await this.addFilterButton.click();
    await expect(this.page.locator("div[id*='listbox']")).toBeVisible();
    for (var i = 0; i < filters.length; i++) {
      await this.addFilter(filters[i]);
    }
    await this.page
      .locator("div[class*='select-popout_multiSelectWrapper']")
      .click();

    for (var i = 0; i < filters.length; i++) {
      await this.applyFilterConditions(
        filters[i],
        ifConditions[i],
        conditionData[i]
      );
    }

    if (filters.length > 1) {
      let logicDropdownLocator = this.page.locator(
        "div[class*='filter-view_selectCondition'] div[class*='indicatorContainer']"
      );

      await expect(logicDropdownLocator).toBeVisible();
      await logicDropdownLocator.click();

      await this.page
        .locator(`//div[contains(text(),'(${filterLogic})')]`)
        .first()
        .click();
    }

    await this.applyFiltersButton.click();
    await this.createGroupButton.click();
  }

  async addFilter(filter: Filters) {
    let filterLocator = this.page.getByRole("button", {
      name: `${filter}`,
    });
    await filterLocator.click();
  }

  async applyFilterConditions(
    filter: Filters,
    filterOperators: FilterOperatorsUI,
    conditionData: string
  ) {
    let filterExpandButton = this.page.getByRole("button", {
      name: `${filter}`,
    });
    await expect(filterExpandButton).toBeVisible();
    await filterExpandButton.click();

    if (filter === Filters.STATUS) {
      await this.page
        .getByText(`${filterOperators}`, {
          exact: true,
        })
        .click();
    } else {
      let smallCaseFilterName = filter.toLowerCase();
      let ifConditionDropdownSelector = this.page.locator(
        `//*[text()='if ${smallCaseFilterName}']/..//*[contains(@class,'indicatorContainer')]`
      );
      let conditionLocator = this.page.getByText(`${filterOperators}`, {
        exact: true,
      });

      await expect(ifConditionDropdownSelector).toBeVisible();
      await ifConditionDropdownSelector.click();
      await conditionLocator.click();
      await this.page.getByPlaceholder("Enter text here").fill(conditionData);
    }
  }

  async verifyCustomAccessGroupIsVisible(groupName: string) {
    await this.navigateToPeopleSection();

    try {
      await expect(this.customGroupsLocator).toBeVisible({ timeout: 5000 });
    } catch (error) {
      await this.page.reload();
      await expect(this.customGroupsLocator).toBeVisible({ timeout: 5000 });
    }
    await this.customGroupsLocator.click();

    let groupNameLocator = this.page.getByText(groupName);
    await expect(groupNameLocator).toBeVisible();

    return groupNameLocator;
  }

  async verifyEmailIdAddedInCustomGroup(
    groupName: string,
    email: string,
    isVisible: boolean = true
  ) {
    let groupLocator = await this.verifyCustomAccessGroupIsVisible(groupName);
    await groupLocator.click();

    let emailLocator = this.page.getByText(email, { exact: "true" });
    await expect(emailLocator).toBeVisible({ visible: isVisible });
  }
}

interface CustomGroupDatatype {
  groupName: string;
  groupType: "Active" | "Static";
  enableAccessControls: boolean;
  filters: Array<Filters>;
  ifConditions: Array<FilterOperatorsUI>;
  conditionData: Array<string>;
  filterLogic?: "AND" | "OR";
}
