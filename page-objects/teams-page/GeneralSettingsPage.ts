import { expect, Locator, Page } from "@playwright/test";
import { MemberDetailsInOrganisation } from "../../interfaces/OrganisationControllerInterface";


export class GeneralSettingsPage {
    readonly page: Page;
    readonly teamLocator: Locator;
    readonly createTeamButtonLocator: Locator;
    readonly teamNameCheckLocator: Locator;
    readonly teamMembersButtonLoator: Locator;
    readonly searchMemberLocator: Locator;
    readonly emailListLocator: Locator;
    readonly roleLabelLocator: Locator;
    readonly productRoleLocator: Locator;
    readonly teamNamesListLocator: Locator;
    readonly searchMemberListLocator: Locator;
    readonly inviteSentLocator: Locator;
    readonly teamNameLocator: Locator;
    readonly inviteMembersButtonLocator: Locator;
    readonly memberDetailComponentLocator: Locator;
    readonly generalPageMenu: Locator;
    readonly removeAdminOption: Locator;
    readonly removeModalBtn: Locator;
    readonly inviteBtnCheck:Locator;
    readonly makeOwnerOption:Locator;
    readonly confirmBtnForOwner: Locator;
    readonly changeRolePopUpButton: Locator;
    readonly createEventButton: Locator;
    readonly createNewStudio: Locator;
    readonly newWebinarButton: Locator;
    readonly homeButton:Locator;
    readonly popUpCloseButton:Locator;

    //create team container locators
    readonly createTeamTextLocator: Locator;
    readonly enterTeamNameLocator: Locator;
    readonly createButtonLocator: Locator;

    //manage access container locators
    readonly manageAccessLinkLocator: Locator;
    readonly teamListArrowLocator: Locator;
    readonly teamCheckBoxListLocator: Locator;
    readonly saveButtonLocator: Locator;

    constructor(page: Page) {
        this.page = page;
        this.teamLocator = this.page.locator("div[class^='org-detail_arrowContainer']");
        this.createTeamButtonLocator = this.page.locator("ul[class^='menu_actionPopover'] li:has-text('Create Team')");
        this.createTeamTextLocator = this.page.locator("h6:has-text('Create a team')");
        this.enterTeamNameLocator = this.page.locator("#team-name");
        this.createButtonLocator = this.page.locator("button:has-text('Create')");
        this.teamNameCheckLocator = this.page.locator("ul[class^='menu_actionPopover'] li");
        this.teamMembersButtonLoator = this.page.locator("p:has-text('Team members')");
        this.searchMemberLocator = this.page.locator("input[placeholder*='Search member']");
        this.emailListLocator = this.page.locator("p[class*='userEmail']");
        this.roleLabelLocator = this.page.locator("p[class*='member-detail_role']");
        this.productRoleLocator = this.page.locator("p[class*='member-detail_orgRole']");
        this.teamNamesListLocator = this.page.locator("div[class*='member-detail_MemberOrgDetails'] p").nth(1);
        this.searchMemberListLocator = this.page.locator("div[class^='member-detail_displayFlex_'] p[class*='userEmail']");
        this.inviteSentLocator = this.page.locator("div[class^='member-detail_memberDetails']");
        this.teamNameLocator = this.page.locator("div[class^='org-detail_orgNameContainer'] p[class*='styles_medium']");
        this.inviteMembersButtonLocator = this.page.locator("button[class^='team_addMemberButton'] p:has-text('Invite members')");
        this.memberDetailComponentLocator = this.page.locator("div[class^='member-detail_displayFlex_']");
        this.manageAccessLinkLocator = this.page.locator("ul[class*='detail_popoverClassName'] li:has-text('Manage team access ')");
        this.teamListArrowLocator = this.page.locator("div[class*='-indicatorContainer']");
        this.teamCheckBoxListLocator = this.page.locator("div[class*='-menu'] div[id^='react-select-']");
        this.saveButtonLocator = this.page.locator("div[class^='manage-team-access_footer'] button[type='submit']");
        this.generalPageMenu = this.page.locator('.member-detail_iconClasses__9yxgP').first();
        this.removeAdminOption = this.page.getByText('Remove as admin');
        this.removeModalBtn = this.page.getByRole('button', { name: 'Remove' });
        this.inviteBtnCheck = this.page.getByRole('button', { name: 'Invite members' });
        this.makeOwnerOption = this.page.getByText('Make owner');
        this.confirmBtnForOwner = this.page.getByRole('button', { name: 'Confirm' });
        this.changeRolePopUpButton = this.page.getByRole('button', { name: 'Change role' });
       // this.createEventButton = this.page.getByRole('button', { name: 'New event Create stunning events' });
        this.createEventButton=this.page.locator("p:has-text('Create stunning events')");
        //this.createNewStudio = this.page.getByRole('button', { name: 'New studio Professional live stream' });
        this.createNewStudio=this.page.locator("p:has-text('Professional live stream')");
        //this.newWebinarButton = this.page.getByRole('button', { name: 'New webinar Host engaging webinars' });
        this.newWebinarButton = this.page.locator("p:has-text('Host engaging webinars')");
        this.homeButton = this.page.getByRole('link', { name: 'Home' });
        this.popUpCloseButton = this.page.locator('#app-modal path').nth(1)
    }

    async openTeamList() {
        await expect(this.teamLocator).toBeVisible();
        await this.teamLocator.click();
        await this.page.waitForLoadState('networkidle');
    }

    async clickOnCreateTeam() {
        await this.openTeamList();
        await this.createTeamButtonLocator.click();
        await expect(this.createTeamTextLocator).toBeVisible();
    }

    async enterTeamName(teamName: string) {
        await expect(this.enterTeamNameLocator).toBeVisible();
        await this.enterTeamNameLocator.fill(teamName);
    }

    async clickOnCreateButton() {
        await this.createButtonLocator.click();
    }

    async createNewTeam(teamName: string) {
        await this.clickOnCreateTeam();
        await this.enterTeamName(teamName);
        await this.clickOnCreateButton();
        await this.openTeamList();
        await expect(this.teamNameCheckLocator.filter({ hasText: new RegExp('^' + teamName + '$') })).toBeVisible();
    }

    async selectATeam(teamName: string) {
        if (await this.teamLocator.count() > 0) {
            await this.openTeamList();
        }
        let x = await this.teamNameCheckLocator.filter({ hasText: new RegExp('^' + teamName + '$') }).count();
        await this.teamNameCheckLocator.filter({ hasText: new RegExp('^' + teamName + '$') }).click();
        await this.verifyCurrentTeamSelected(teamName);
    }

    async verifyCurrentTeamSelected(teamName: string) {
        await this.page.waitForTimeout(5000);
        expect(await this.teamNameLocator.innerText()).toEqual(teamName);
    }

    async verifyCurrentTeamFromTeamList(teamNames: string[]) {
        await this.page.waitForTimeout(5000);
        let actualTeamName = await this.teamNameLocator.innerText(
            {timeout: 5000}
        );
        expect(teamNames).toContain(actualTeamName);
    }        

    async openTeamMembersPage() {
        await this.teamMembersButtonLoator.click();
        await this.page.waitForLoadState('domcontentloaded');
    }

    async verifySearchMemberFieldToBePresent() {
        await expect(this.searchMemberLocator).toBeVisible();
    }

    async searchMemberByEmail(memberDetail: string) {
        await this.verifySearchMemberFieldToBePresent();
        await this.searchMemberLocator.fill(memberDetail);
    }

    async verifyGeneralSettingsSearchMemberFlow(memberDetail: string) {
        await this.searchMemberByEmail(memberDetail);
        expect(this.searchMemberListLocator.filter({ hasText: memberDetail })).toBeTruthy();
    }

    async verifyInvitationMessageToBeVisible(firstName: string) {
        console.log(`visibility count of invite sent text is `, await this.inviteSentLocator.filter({ hasText: firstName }).locator("p").filter({ hasText: "Invite sent." }).count());
        await expect(this.inviteSentLocator.filter({ hasText: firstName }).locator("p").filter({ hasText: "Invite sent." })).toBeVisible();
    }

    async verifyInvitationMessageToBeInvisible(firstName: string) {
        console.log(`invisibility count of invite sent text is `, await this.inviteSentLocator.filter({ hasText: firstName }).locator("p").filter({ hasText: "Invite sent." }).count());
        await expect(this.inviteSentLocator.filter({ hasText: firstName }).locator("p").filter({ hasText: "Invite sent." })).toBeHidden({ timeout: 5000 });
    }

    async verifiyInvisibilityOfInviteMembersButton() {
        await expect(this.inviteMembersButtonLocator).toBeHidden();
    }

    async verifiyVisibilityOfInviteMembersButton() {
        await expect(this.inviteMembersButtonLocator).toBeVisible();
    }

    async fetchLabelOfMember(): Promise<string> {
        await expect(this.roleLabelLocator).toBeVisible();
        return await this.roleLabelLocator.innerText();
    }

    async fetchProductRoleOfMember(): Promise<string> {
        await expect(this.productRoleLocator).toBeVisible();
        let productRole = await this.productRoleLocator.innerText();
        return productRole;
    }

    async VerifyMemberEmailToBePresent(memberName: string) {
        await expect(this.emailListLocator).toBeVisible();
        await expect(this.emailListLocator.filter({ hasText: memberName })).toBeVisible();
    }

    async fetchTeamNames(): Promise<string[]> {
        let list: string[];
        await expect(this.teamNamesListLocator).toBeVisible();
        let teamNames = await this.teamNamesListLocator.innerText();
        if (teamNames.includes(',')) {
            list = teamNames.split(',').map((item) => item.trim());
        } else {
            list = [teamNames];
        }
        return list;
    }

    async fetchMemberDetailsInOrganisation(ownerEmail: string) {
        await this.VerifyMemberEmailToBePresent(ownerEmail);
        let productRole = await this.fetchProductRoleOfMember();
        let role = await this.fetchLabelOfMember();
        let teamList = await this.fetchTeamNames()
        const actualMemberDetails: MemberDetailsInOrganisation = {
            email: ownerEmail,
            productRole: productRole,
            myTeamNameList: teamList,
            label: role
        }
        return actualMemberDetails;
    }

    async VerifyMemberDetailsInOrgListingPage(expectedMemberDetails: MemberDetailsInOrganisation, actualMemberDetails: object) {
        const actualKeys = Object.keys(actualMemberDetails);
        for (let key of actualKeys) {
            await expect(expectedMemberDetails[key]).toEqual(actualMemberDetails[key]);
        }
    }

    async openManageAccessComponent(memberEmail: string) {
        await (this.memberDetailComponentLocator.filter({ hasText: memberEmail }).locator("span[class^='member-detail_iconClasses']")).click();
        await this.page.waitForTimeout(2000);
        await this.manageAccessLinkLocator.click();
    }

    async selectOrDeselectTeamFromManageAccessComponent(teamNameList: string[]) {
        await this.teamListArrowLocator.click();
        for (const team of teamNameList) {
            if (this.teamCheckBoxListLocator.filter({ hasText: team })) {
                await this.teamCheckBoxListLocator.filter({ hasText: team }).locator("div[class^='Checkbox_container']").click({ timeout: 5000 });
            }
        }
        await this.teamListArrowLocator.click();
        await this.saveButtonLocator.click();
    }

    async verifyTeamNamePresentInAList(teamName: string) {
        await expect(this.teamNameCheckLocator.filter({ hasText: new RegExp('^' + teamName + '$') })).toBeVisible();
    }

    async verifyTeamNameNotPresentInAList(teamName: string) {
        await expect(this.teamNameCheckLocator.filter({ hasText: new RegExp('^' + teamName + '$') })).toBeHidden();
    }

    async removeFromAdmin(){
        await this.generalPageMenu.click();
        await this.removeAdminOption.click();
        await this.removeModalBtn.click();
    }

    async verifyInviteBtn(){
        await expect(this.inviteBtnCheck).toHaveCount(0)
    }

    async makeOtherMemberAsOwner(){
        let memberDetails = this.page.locator('div[class^="member-detail_displayFlex"]').filter({hasText: 'mailosaur.net'}).nth(1);
        await memberDetails.locator('.member-detail_iconClasses__9yxgP').click();
        await this.makeOwnerOption.click();
        await this.confirmBtnForOwner.click();
    }
    
    async verifyChangeRoleButtonAppears(){
        await expect(this.changeRolePopUpButton).toHaveCount(1);
    }
    
    async verifyWebinarButton(){
        await this.newWebinarButton.click();
        await this.verifyChangeRoleButtonAppears();
        await this.popUpCloseButton.click();
    }

    async verifyEventButton(){
        await this.createEventButton.click();
        await this.verifyChangeRoleButtonAppears();
        await this.popUpCloseButton.click();
    }

    async verifyStudioButton(){
        await this.createNewStudio.click();
        await this.verifyChangeRoleButtonAppears();
        await this.popUpCloseButton.click();
    }

    async clickOnHomeButton(){
        await this.homeButton.click();
    }


}
