import { expect, Locator, Page } from "@playwright/test";
import exp from "constants";
import { MemberRolesCapitaliseEnum } from "../../enums/MemberRolesEnum";

export class TeamMembersPage {
  readonly page: Page;
  readonly teamMemberListLocator: Locator;
  readonly emailListLocator: Locator;
  readonly searchMemberLocator: Locator;
  readonly searchMemberListLocator: Locator;
  readonly addToTeamButtonLocator: Locator;
  readonly addMembersToTeamStringLocator: Locator;
  readonly membersListLocator: Locator;
  readonly searchMemberFromDropdownLocator: Locator;
  readonly selectMemberCheckboxLocator: Locator;

  constructor(page: Page) {
    this.page = page;
    this.teamMemberListLocator = this.page.locator(
      "div[class^='team-member-card_profileDetails'] div[class^='team-member-card_memberDetails']"
    );
    this.emailListLocator = this.teamMemberListLocator.locator(
      "p[class*='userEmail']"
    );
    this.searchMemberLocator = this.page.locator(
      "input[placeholder*='Search team member']"
    );
    this.searchMemberListLocator = this.page.locator(
      "div[class^='team-member-card_detailContainer'] p[class*='userEmail']"
    );
    this.addToTeamButtonLocator = this.page.locator(
      "button:has-text('Add to team')"
    );
    this.addMembersToTeamStringLocator = this.page.locator(
      "h6:has-text('Add members to the team')"
    );
    this.membersListLocator = this.page.locator(
      "div[class^='search-member_memberCard']"
    );
    this.searchMemberFromDropdownLocator = this.page.locator(
      "input[placeholder='Search by name']"
    );
    this.selectMemberCheckboxLocator = this.page.locator(
      "div[class^='search-member_memberContentContainer']"
    );
  }

  async verifyOrganizationRoleOfMember(
    rolesToCheck: Map<string, MemberRolesCapitaliseEnum>
  ) {
    for (const emailElement of await this.emailListLocator.elementHandles()) {
      const email = await emailElement.innerText();
      if (
        rolesToCheck.has(email) &&
        this.emailListLocator.filter({ hasText: email })
      ) {
        expect(
          await this.teamMemberListLocator
            .filter({ hasText: email })
            .locator("p[class*='card_role_']")
            .innerText()
        ).toEqual(rolesToCheck.get(email));
      }
    }
  }

  async verifySearchMemberFieldToBePresent() {
    await expect(this.searchMemberLocator).toBeVisible();
  }

  async searchMemberByEmail(memberDetail: string) {
    await this.verifySearchMemberFieldToBePresent();
    await this.searchMemberLocator.fill(memberDetail);
  }

  async verifyTeamMembersSearchFlow(memberDetail: string) {
    await this.searchMemberByEmail(memberDetail);
    expect(
      this.searchMemberListLocator.filter({ hasText: memberDetail })
    ).toBeTruthy();
  }

  async addToATeamButton() {
    await this.addToTeamButtonLocator.click();
  }

  async verifyAddMembersToTeamDropdownToBePresent() {
    await expect(this.addMembersToTeamStringLocator).toBeVisible();
    expect(await this.membersListLocator.count()).toBeGreaterThan(0);
  }

  async addNewMemberToATeam(email: string) {
    await this.searchMemberFromDropdownLocator.fill(email);
    await this.selectMemberCheckboxLocator.filter({ hasText: email }).click();
    await this.page.getByRole("button", { name: "Add" }).click();
    await expect(
      this.page.locator(
        `div[class^='team-member-card_memberDetails__'] p:has-text('${email}')`
      )
    ).toBeVisible();
  }
}
