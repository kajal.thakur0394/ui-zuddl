import { Locator, Page, expect } from "@playwright/test";
import { updateStatusFromIntegration } from "../../util/apiUtil";
import { IntegrationPage } from "./IntegrationPage";

export class SalesforcePage extends IntegrationPage {
  // readonly integrationButtonClick: Locator;
  // readonly integrationTab: Locator;
  readonly salesforceInstallStatus: Locator;
  readonly chickNextOnAuthPage: Locator;
  readonly chickFinishOnAuthPage: Locator;
  readonly clickOnCreateNewPage: Locator;
  readonly clickOnContinueBtn: Locator;
  readonly addFieldBtn: Locator;
  readonly pushDataLocator: Locator;
  readonly integrationLabel: Locator;
  readonly addingCampainId: Locator;
  readonly saveBtn: Locator;
  readonly firstNameField: Locator;
  readonly lastNameField: Locator;
  readonly emailField: Locator;
  readonly bioField: Locator;
  readonly registerField: Locator;
  readonly attendePageSignOut: Locator;
  readonly attendePageNewUser: Locator;
  readonly uninstallSalesforce: Locator;
  readonly salesForceAccountText: Locator;
  readonly importDataTab: Locator;
  readonly enableImportData: Locator;
  readonly contactCustomField: Locator;
  readonly campainId: Locator;
  readonly disableImportData: Locator;
  readonly confirmDisableImportData: Locator;
  readonly salesforceTab: Locator;
  readonly customizeButtonForPush: Locator;
  readonly customizeButtonForPull: Locator;
  readonly leadCustomFieldTab: Locator;
  readonly deleteOneCustomeFieldForPull: Locator;
  readonly deleteOneCustomeField: Locator;
  readonly saveCustomeField: Locator;
  readonly contactCustomFieldTab: Locator;
  readonly campaignMemberCustomeFieldTab: Locator;
  readonly contactCustomeFieldTabForPull: Locator;
  readonly editButtonLocator: Locator;
  readonly saveButtonLocator: Locator;
  readonly passwordButton: Locator;
  readonly loginButton: Locator;
  // readonly editButton: Locator;
  // readonly installAppButton: Locator;
  // readonly defaultFieldLocator: Locator;
  // readonly addFieldButtonLocator: Locator;
  // readonly actionContainer: Locator;
  // readonly fieldSectionContainer: Locator;

  constructor(page: Page) {
    super(page);
    // this.integrationButtonClick = this.page.locator(
    //   "p:has-text('Integrations')"
    // );
    // this.integrationTab = this.page.locator(
    //   "div[class^='integration_integrationContainer']>div>div>button>>nth=0"
    // );
    this.salesforceInstallStatus = this.page.locator(
      "button[class*='integration-details-popup_actionButton']"
    );
    this.chickNextOnAuthPage = this.page.getByRole("button", { name: "Next" });
    this.chickFinishOnAuthPage = this.page.getByRole("button", {
      name: "Finish",
    });
    this.clickOnCreateNewPage = this.page.getByRole("button", {
      name: "Create new page",
    });
    this.clickOnContinueBtn = this.page.getByRole("button", {
      name: "Continue",
    });
    this.addFieldBtn = this.page.locator("p:has-text('Add field')");
    this.pushDataLocator = this.page.locator(
      'button[id="integration_push_data_Send registrant data to Salesforce"]'
    );
    this.integrationLabel = this.page.locator("#integrationExportData label");
    this.addingCampainId = this.page.locator(
      "input[class^='Input_input']>>nth=0"
    );
    this.saveBtn = this.page.locator("button[class*='Button_button']");
    this.firstNameField = this.page.getByPlaceholder("First Name *");
    this.lastNameField = this.page.getByPlaceholder("Last Name *");
    this.emailField = this.page.getByPlaceholder("Email *");
    this.bioField = this.page.getByPlaceholder("Bio ");
    this.registerField = this.page.getByRole("button", {
      name: "Register now",
    });
    this.attendePageSignOut = this.page.getByRole("button", {
      name: "Sign Out",
    });
    this.attendePageNewUser = this.page.locator(
      "span[class^='list-attendees-page_id']"
    );
    this.uninstallSalesforce = this.page.getByRole("button", {
      name: "Uninstall app",
    });
    this.salesForceAccountText = this.page.getByText(
      "Automation Org's Salesforce account 1"
    );
    this.importDataTab = this.page.locator(
      '[id="integration_push_data_Receive\\ registrant\\ data\\ from\\ Salesforce"]'
    );
    this.enableImportData = this.page.locator("#pullDataModal label");
    this.contactCustomField = this.page.getByText("Contact custom fields");
    this.campainId = this.page.getByText("Campaign ID");
    this.disableImportData = this.page.locator("#pullDataModal label span");
    this.confirmDisableImportData = this.page.getByRole("button", {
      name: "Yes",
    });
    this.salesforceTab = this.page.getByText("Salesforce", { exact: true });
    this.customizeButtonForPush = this.page.locator(
      '[id="integration_push_data_Send\\ registrant\\ data\\ to\\ Salesforce"]'
    );
    this.customizeButtonForPull = this.page.locator(
      '[id="integration_push_data_Receive\\ registrant\\ data\\ from\\ Salesforce"]'
    );
    this.leadCustomFieldTab = this.page.getByText("Lead custom fields");
    this.deleteOneCustomeFieldForPull = this.page
      .locator(".custom-field-mapping_actionContainer__KKtYF")
      .first();
    this.deleteOneCustomeField = this.page
      .locator(".custom-field-mapping_actionContainer__KKtYF")
      .first();
    this.saveCustomeField = this.page.getByRole("button", { name: "Save" });
    this.contactCustomFieldTab = this.page.getByText("Contact custom fields");
    this.campaignMemberCustomeFieldTab = this.page.getByText(
      "Campaign member custom fields"
    );
    this.contactCustomeFieldTabForPull = this.page.getByText(
      "Contact custom fields"
    );
    this.editButtonLocator = this.page.locator(
      "button[class*='AuthSlot-title-edit_']"
    );
    this.saveButtonLocator = this.page.locator(
      "button[data-qa='authentication-save-button']"
    );
    this.passwordButton = this.page.locator("#password");
    this.loginButton = this.page.locator("#Login");
    // this.editButton = this.page.locator(
    //   "button[class*='integration-details-popup_editButton']"
    // );
    // this.installAppButton = this.page
    //   .getByRole("button")
    //   .filter({ hasText: "Install app" });
    // this.defaultFieldLocator = this.page.locator(
    //   "div[class*='orgFieldMappingDropdown']"
    // );
    // this.addFieldButtonLocator = this.page.locator(
    //   "button[class*='addFieldBtn']"
    // );
    // this.fieldSectionContainer = this.page.locator(
    //   "div[class^='custom-field-mapping_fieldSectionContainer']"
    // );
    // this.actionContainer = this.page.locator(
    //   " div[class*='actionContainer'] div[class^='Icon']"
    // );
    //Authentication for Salesforce successfully updated
  }

  async clickOnIntegrationButton() {
    await this.integrationButtonClick.click();
  }

  async selectIntegrationTab() {
    await this.integrationTab.click();
  }

  async getSalesforceInstallStatus() {
    let salesforceStatus = await this.salesforceInstallStatus.textContent();
    return salesforceStatus;
  }

  // async clickNextOnAuthPage() {
  //   let iframeElement = this.page.frameLocator("iframe#previewFrame").nth(1);
  //   //await expect(iframeElement.getByRole('button', { name: 'Next' })).toBeEnabled();
  //   const textElement = await iframeElement
  //     .getByRole("button", { name: "Next" })
  //     .click();
  // }

  // async clickFinishOnAuthPage() {
  //   const iframeElement = this.page.frameLocator("iframe#previewFrame").nth(1);
  //   await expect(
  //     iframeElement.getByRole("button", { name: "Finish" })
  //   ).toBeVisible();
  //   const textElement = await iframeElement
  //     .getByRole("button", { name: "Finish" })
  //     .click();
  // }

  async clickOnRegPageNewPageBtn() {
    await this.clickOnCreateNewPage.click();
  }

  async clickOnRegPageContinueBtn() {
    await expect(this.clickOnContinueBtn).toBeVisible();
    await this.clickOnContinueBtn.click();
  }

  async load(url: string) {
    console.log(`Loading url ${url}`);
    await this.page.goto(url);
    console.log(`url loaded`);
  }

  async addFieldBtnClick() {
    await expect(this.addFieldBtn).toBeVisible();
    await this.addFieldBtn.click();
  }

  async pushDataLocatorClick() {
    await this.pushDataLocator.click();
  }

  async addingCampainID(campainId){
    await this.addingCampainId.fill(campainId);
    await this.saveBtn.filter({ hasText: "Save" }).click();
    await this.page.waitForTimeout(2000);
  }

  async savingCustomActivities(){
    await this.saveBtn.filter({ hasText: "Save" }).click();
    await this.page.waitForTimeout(2000);
  }

  async savingleadCustomRegField(){
    await this.saveBtn.filter({ hasText: "Save" }).click();
    await this.page.waitForTimeout(2000);
  }

  async gotoToLeadCaptureFieldsURL(){
    const leadCustomBtn = this.page.locator('text=Go to lead capture fields');
    await expect(leadCustomBtn).toBeVisible();
    await leadCustomBtn.click();
  }
  
  async verifyLeadCaptureURL(){
    const currentURL = this.page.url();
    if(currentURL){
      expect(currentURL).toContain('/lead-capture-settings');
    }
  }

  async addCustomeLeadCaptureField(){
    const addFieldBtn = this.page.locator('text=Add new field');
    await expect(addFieldBtn).toBeVisible();
    await addFieldBtn.click();
    await this.page.fill('input[placeholder="Type your question"]', 'Age');
    await this.page.waitForTimeout(2000);
    const saveField =  this.page.locator('text=Add field');
    await expect(saveField).toBeVisible();
    await saveField.click();
    await this.page.waitForTimeout(2000);
  }

  async verifyCustomDropDown() {
    const dropdownLocator = this.page.locator('.css-1df43wa-control').first();
    await expect(dropdownLocator).toBeVisible();
    await dropdownLocator.click();
    const fieldName = this.page.getByText('Age (shortanswer)', { exact: true })
    await expect(fieldName).toBeVisible();
  }

  async addingCampainIDAndSaving(campaignId) {
    await this.addingCampainId.fill(campaignId);
    await this.saveBtn.filter({ hasText: "Save" }).click();
    await this.page.waitForTimeout(2000);
    await this.saveBtn.filter({ hasText: "Save" }).click();
    await this.page.waitForTimeout(2000);
    await this.saveBtn.filter({ hasText: "Save" }).click();
    await this.page.waitForTimeout(2000);
    await this.saveBtn.filter({ hasText: "Save" }).click();
    await this.page.waitForTimeout(2000);
    await this.saveBtn.filter({ hasText: "Save" }).click();
    await this.saveBtn.filter({ hasText: "Confirm" }).click();
  }

  async registeringAttende(
    firstName: string,
    lastName: string,
    emailId: string,
    bio: string
  ) {
    await this.firstNameField.fill(firstName);
    await this.lastNameField.fill(lastName);
    await this.emailField.fill(emailId);
    await this.bioField.fill(bio);
    await this.registerField.click();
  }

  async attendePageSignOutClick() {
    await this.attendePageSignOut.click();
  }

  async attendePageNewUserVerification(attendeEmailId) {
    let attendiId = await this.attendePageNewUser.textContent();
    expect(attendiId, "Expecting email ID to be nam@gmail.com").toEqual(
      attendeEmailId
    );
  }

  async verifyTrayData(trayDatas: any[], emailId: string) {
    for (let data = 0; data < trayDatas.length; data++) {
      if (trayDatas[data].Email === emailId) {
        console.log(trayDatas[data].Email);
        return true;
      }
    }
    return false;
  }

  async uninstallSalesforceOnBtnClick() {
    await this.uninstallSalesforce.click();
  }

  async getSalesForceAccountText() {
    const iframeElement = this.page.frameLocator("iframe#previewFrame").nth(1);
    const textElement = iframeElement.locator(
      "div[class^='AuthSlot-title-status___'] + div"
    );
    return await textElement.textContent();
  }

  async getVerificationSalesforceStatus(datas: any[]) {
    for (let data = 0; data < datas.length; data++) {
      if (
        datas[data].name === "Salesforce" &&
        datas[data].solutionStatus === "ENABLE"
      ) {
        return true;
      }
    }
    return false;
  }

  async verifySalesforceInstallation(status: boolean) {
    expect(status, "Expecting status to be true").toEqual(true);
  }

  async verifyCampainID(pushData: any) {
    let cmpId = pushData[0].eventIntegrationId;
    if (cmpId === "7015j0000011XlbAAE") {
      return true;
    }
    return false;
  }

  async verifyDisableStatus(pushData: any) {
    console.log(pushData);
    for (let data = 0; data < pushData.length; data++) {
      console.log(pushData[data].integrationType);
      if (
        pushData[data].integrationType === "SALESFORCE" &&
        pushData[data].status == false
      ) {
        return true;
      }
    }
    return false;
  }

  async checkImportDataClickable() {
    await this.importDataTab.click();
    // await this.enableImportData.click();
    // await this.contactCustomField.click();
    // await this.campainId.click();
    await this.saveBtn.filter({ hasText: "Save" }).click();
    await this.page.waitForTimeout(2000);
    await this.saveBtn.filter({ hasText: "Save" }).click();
    await this.page.waitForTimeout(2000);
    await this.addingCampainId.fill("7015j0000011XlbAAE");
    await this.saveBtn.filter({ hasText: "Save" }).click();
    await this.page.waitForTimeout(2000);
  }

  async clickOnImportDataTab() {
    await this.importDataTab.click();
    await this.saveBtn.filter({ hasText: "Save" }).click();
    await this.page.waitForTimeout(2000);
    await this.saveBtn.filter({ hasText: "Save" }).click();
    await this.page.waitForTimeout(2000);
    await this.addingCampainId.fill("7015j0000011YZqAAM");
    await this.saveBtn.filter({ hasText: "Save" }).click();
    // await this.enableImportData.click();
    //await this.contactCustomField.click();
    await this.disableImportData.click();
    await this.confirmDisableImportData.click();
  }

  async clickOnEditButton() {
    await this.editButton.click();
  }
  async clickOnInstallAppButton() {
    await this.installAppButton.click();
  }

  async salesForceInstallation(
    dashboardUrl,
    integrationPageUrl,
    orgApiContext
  ) {
    await this.load(dashboardUrl);
    await this.clickOnIntegrationButton();
    await this.selectIntegrationTab();
    let installationCheck = await this.getSalesforceInstallStatus();
    if (installationCheck === "Install app") {
      await this.clickOnInstallAppButton();
      let salesforceText = await this.getSalesForceAccountText();
      if (salesforceText === "Automation Org's Salesforce account 1") {
        await this.clickNextOnAuthPage();
        await this.clickFinishOnAuthPage();
        await this.page.getByRole("button", { name: "Finish" }).click();
        await updateStatusFromIntegration(orgApiContext);
      }
    }
  }

  async veirfySalesforceTabPresent() {
    await expect(this.salesforceTab).toBeVisible({ timeout: 1000 });
  }

  async gotoLeadCustomFieldTabForPush(tabNumber: number) {
    await this.customizeButtonForPush.click();
    if (tabNumber === 1) {
      await this.leadCustomFieldTab.click();
    } else if (tabNumber === 2) {
      await this.contactCustomFieldTab.click();
    } else if (tabNumber === 3) {
      await this.campaignMemberCustomeFieldTab.click();
    }
  }

  async deleteOneCustomFieldForPush() {
    await this.deleteOneCustomeField.click();
    await this.saveCustomeField.click();
  }

  async gotoLeadCustomFieldTabForPull(tabNumber: number) {
    await this.customizeButtonForPull.click();
    if (tabNumber === 1) {
      /* empty */
    } else if (tabNumber === 2) {
      await this.contactCustomeFieldTabForPull.click();
    }
  }

  async deleteOneCustomFieldForPull() {
    await this.deleteOneCustomeFieldForPull.click();
    await this.saveCustomeField.click();
  }

  // async openViewDetailsAndCheckInstallationStatus(url: string) {
  //   await this.load(url);
  //   await this.clickOnIntegrationButton();
  //   await this.selectIntegrationTab();
  //   let status = await this.getSalesforceInstallStatus();
  //   if (status === "Install app") {
  //     return true;
  //   }
  //   return false;
  // }

  // async IntegrateApp(url: string) {
  //   if (await this.openViewDetailsAndCheckInstallationStatus(url)) {
  //     //installation
  //     await this.clickOnInstallAppButton();
  //   } else {
  //     //edit
  //     await this.clickOnEditButton();
  //     await this.page.waitForTimeout(5000);
  //   }
  //   await this.clickNextOnAuthPage();
  //   await this.clickFinishOnAuthPage();
  // }

  // async IntegrateAppE2E(url: string) {
  //   await this.IntegrateApp(url);
  //   await this.page.getByRole("button", { name: "Finish" }).click();
  // }

  // async verifyPresenceOfDefaultFields() {
  //   let defaultFieldList = [
  //     "Email (string)",
  //     "First Name (string)",
  //     "Last Name (string)",
  //   ];
  //   for (let field of defaultFieldList) {
  //     await expect(
  //       this.defaultFieldLocator.filter({ hasText: field }).first()
  //     ).toBeVisible();
  //   }
  // }

  async clickOnAddFieldButton() {
    await this.addFieldBtn.click();
  }

  async deleteField(fieldName: string) {
    const fieldSectionContainerToLookOut = this.fieldSectionContainer.filter({
      hasText: fieldName,
    });
    let deleteFieldButton = fieldSectionContainerToLookOut.locator(
      this.actionContainer
    );
    await this.page.waitForTimeout(5000);
    await deleteFieldButton.click();
    await this.page.waitForTimeout(2000);
  }

  async verifyFieldToBeDeleted(fieldName: string) {
    const fieldSectionContainerToLookOut = this.fieldSectionContainer.filter({
      hasText: fieldName,
    });
    await this.deleteField(fieldName);
    await expect(fieldSectionContainerToLookOut).toBeHidden();
  }
}
