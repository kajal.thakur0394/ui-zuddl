import { Locator, Page, expect } from "@playwright/test";
import integrationType from "../../enums/integrationTYpeEnum";

export class IntegrationPage {
  readonly page: Page;
  readonly defaultFieldLocator: Locator;
  readonly editButton: Locator;
  readonly installAppButton: Locator;
  readonly integrationButtonClick: Locator;
  readonly integrationTab: Locator;
  readonly installStatus: Locator;
  readonly fieldSectionContainer: Locator;
  readonly actionContainer: Locator;
  readonly viewDetailsLinkHubspotLocator: Locator;
  constructor(page: Page) {
    this.page = page;
    this.defaultFieldLocator = this.page.locator(
      "div[class*='orgFieldMappingDropdown']"
    );
    this.editButton = this.page.locator(
      "button[class*='integration-details-popup_editButton']"
    );
    this.installAppButton = this.page
      .getByRole("button")
      .filter({ hasText: "Install app" });
    this.integrationButtonClick = this.page.locator(
      "p:has-text('Integrations')"
    );
    this.integrationTab = this.page.locator(
      "div[class^='integration_integrationContainer']>div>div>button>>nth=0"
    );
    this.installStatus = this.page.locator(
      "button[class*='integration-details-popup_actionButton']"
    );
    this.fieldSectionContainer = this.page.locator(
      "div[class^='custom-field-mapping_fieldSectionContainer']"
    );
    this.actionContainer = this.page.locator(
      " div[class*='actionContainer'] div[class^='Icon']"
    );
    this.viewDetailsLinkHubspotLocator = this.page.locator(
      "#integration_landing_page_HubSpot, #integration_landing_page_Hubspot"
    );
  }
  async clickOnEditButton() {
    await this.editButton.click();
  }

  async clickOnInstallAppButton() {
    await this.installAppButton.click();
  }

  async load(url: string) {
    console.log(`Loading url ${url}`);
    await this.page.goto(url);
    console.log(`url loaded`);
  }

  async clickNextOnAuthPage() {
    let iframeElement = this.page.frameLocator("iframe#previewFrame").nth(1);
    //await expect(iframeElement.getByRole('button', { name: 'Next' })).toBeEnabled();
    const textElement = await iframeElement
      .getByRole("button", { name: "Next" })
      .click();
  }

  async clickFinishOnAuthPage() {
    const iframeElement = this.page.frameLocator("iframe#previewFrame").nth(1);
    await expect(
      iframeElement.getByRole("button", { name: "Finish" })
    ).toBeVisible();
    const textElement = await iframeElement
      .getByRole("button", { name: "Finish" })
      .click();
  }

  async IntegrateApp(url: string, moduleName: integrationType) {
    if (await this.openViewDetailsAndCheckInstallationStatus(url, moduleName)) {
      //installation
      await this.clickOnInstallAppButton();
    } else {
      //edit
      await this.clickOnEditButton();
      await this.page.waitForTimeout(5000);
    }
    await this.clickNextOnAuthPage();
    await this.clickFinishOnAuthPage();
  }

  async IntegrateAppE2E(url: string, moduleName: integrationType) {
    await this.IntegrateApp(url, moduleName);
    await this.page.getByRole("button", { name: "Finish" }).click();
  }

  async verifyPresenceOfDefaultFields() {
    let defaultFieldList = [
      "Email (string)",
      "First Name (string)",
      "Last Name (string)",
    ];
    for (let field of defaultFieldList) {
      await expect(
        this.defaultFieldLocator.filter({ hasText: field }).first()
      ).toBeVisible();
    }
  }

  async clickOnNextButton() {
    let iframeElement = this.page.frameLocator("iframe#previewFrame").nth(1);
    //await expect(iframeElement.getByRole('button', { name: 'Next' })).toBeEnabled();
    const textElement = await iframeElement
      .getByRole("button", { name: "Next" })
      .click();
    //await this.page.getByRole("button", { name: "Next" }).click();
  }

  async clickOnFinishButton() {
    let iframeElement = this.page.frameLocator("iframe#previewFrame").nth(1);
    //await expect(iframeElement.getByRole('button', { name: 'Next' })).toBeEnabled();
    const textElement = await iframeElement
      .getByRole("button", { name: "Finish" })
      .click();
    //await this.page.getByRole("button", { name: "Finish" }).click();
  }
  async clickOnIntegrationButton() {
    await this.integrationButtonClick.click();
  }

  async selectIntegrationTab() {
    await this.integrationTab.click();
  }
  async openViewDetailsPage(moduleName: integrationType) {
    switch (moduleName) {
      case integrationType.HUBSPOT:
        await this.viewDetailsLinkHubspotLocator.click();
        break;
      case integrationType.SALESFORCE:
        await this.selectIntegrationTab();
        break;
    }
  }

  async openViewDetailsAndCheckInstallationStatus(
    url: string,
    moduleName: integrationType
  ) {
    await this.load(url);
    await this.clickOnIntegrationButton();
    await this.openViewDetailsPage(moduleName);
    let status = await this.getInstallStatus();
    if (status === "Install app") {
      return true;
    }
    return false;
  }

  async getInstallStatus() {
    let status = await this.installStatus.textContent();
    return status;
  }

  async verifyFieldToBeDeleted(fieldName: string) {
    const fieldSectionContainerToLookOut = this.fieldSectionContainer.filter({
      hasText: fieldName,
    });
    await this.deleteField(fieldName);
    await expect(fieldSectionContainerToLookOut).toBeHidden();
  }

  async deleteField(fieldName: string) {
    const fieldSectionContainerToLookOut = this.fieldSectionContainer.filter({
      hasText: fieldName,
    });
    let deleteFieldButton = fieldSectionContainerToLookOut.locator(
      this.actionContainer
    );
    await this.page.waitForTimeout(5000);
    await deleteFieldButton.click();
    await this.page.waitForTimeout(2000);
  }
}
