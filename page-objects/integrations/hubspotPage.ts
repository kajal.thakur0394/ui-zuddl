import { Locator, Page, expect } from "@playwright/test";
import { DataUtl } from "../../util/dataUtil";
import { IntegrationPage } from "./IntegrationPage";

export class HubspotPage extends IntegrationPage {
  readonly viewDetailsLinkLocator: Locator;
  readonly installuninstallButtonLocator: Locator;
  readonly editButtonLocator: Locator;
  readonly customizeButtonLocator: Locator;
  readonly toggleSwitchLocator: Locator;
  readonly saveBtn: Locator;
  readonly contactCustomFieldTab: Locator;

  constructor(page: Page) {
    super(page);
    this.viewDetailsLinkLocator = this.page.locator(
      "#integration_landing_page_HubSpot, #integration_landing_page_Hubspot"
    );
    this.editButtonLocator = this.page.locator(
      "button[class*='integration-details-popup_edit']"
    );
    this.installuninstallButtonLocator = this.page.locator(
      "button[class*='integration-details-popup_action']"
    );
    this.customizeButtonLocator = this.page.locator(
      "button[id='integration_push_data_Send registrant data to HubSpot']"
    );
    this.toggleSwitchLocator = this.page.locator(
      "div[class^='popup-header_action'] label[class*='Toggle_switch']"
    );
    this.saveBtn = this.page.locator("button[class*='Button_button']");
    this.contactCustomFieldTab = this.page.getByText("Custom fields");
  }

  async openIntegrationListPage() {
    let integrationListUrl = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/dashboard/integration/list`;
    await this.page.goto(integrationListUrl);
  }

  async openViewDetailsPage() {
    await this.viewDetailsLinkLocator.click();
  }

  async goToDashboard() {
    let dashboardUrl = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/dashboard`;
    await this.page.goto(dashboardUrl);
  }

  async clickOnEditButton() {
    await this.editButtonLocator.click();
  }

  async verifyPresenceOfButton(buttonText: string) {
    await expect(
      this.installuninstallButtonLocator.filter({ hasText: buttonText })
    ).toBeVisible();
  }

  async clickOnInstallOrUninstallAppButton() {
    await this.installuninstallButtonLocator.click();
  }

  async clickOnNextButton() {
    let iframeElement = this.page.frameLocator("iframe#previewFrame").nth(1);
    //await expect(iframeElement.getByRole('button', { name: 'Next' })).toBeEnabled();
    const textElement = await iframeElement
      .getByRole("button", { name: "Next" })
      .click();
    //await this.page.getByRole("button", { name: "Next" }).click();
  }

  async clickOnFinishButton() {
    let iframeElement = this.page.frameLocator("iframe#previewFrame").nth(1);
    //await expect(iframeElement.getByRole('button', { name: 'Next' })).toBeEnabled();
    const textElement = await iframeElement
      .getByRole("button", { name: "Finish" })
      .click();
    //await this.page.getByRole("button", { name: "Finish" }).click();
  }

  async verifyAccountName(accountName: string) {
    await this.page.waitForTimeout(2000);
    let iframeElement = this.page.frameLocator("iframe#previewFrame").nth(1);
    await expect(iframeElement.getByText(accountName)).toBeVisible();
  }

  async openEventIntegrationPage(eventId: any) {
    let integrationListUrl = `${
      DataUtl.getApplicationTestDataObj()["newOrgSideBaseUrl"]
    }/event/${eventId}/event-integration`;
    await this.page.goto(integrationListUrl);
    await this.page.waitForLoadState("networkidle");
  }

  async enableToggle() {
    await this.customizeButtonLocator.click();
    // await this.toggleSwitchLocator.click();
    await this.saveBtn.filter({ hasText: "Save" }).click();
    await this.page.waitForTimeout(1000);
    await this.saveBtn.filter({ hasText: "Save" }).click();
    await this.page.waitForTimeout(1000);
    await this.page.getByRole("button", { name: "Confirm" }).click();
  }

  async disableToggle() {
    await this.toggleSwitchLocator.click();
    await this.page.getByRole("button", { name: "Yes" }).click();
  }

  async openCustomFieldTab() {
    await this.customizeButtonLocator.click();
    await this.contactCustomFieldTab.click();
  }
}
