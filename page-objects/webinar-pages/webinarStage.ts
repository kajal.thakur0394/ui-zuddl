import { expect, FrameLocator, Locator, Page, test } from "@playwright/test";
import { InteractionPanel } from "../events-pages/live-side-components/InteractionPanel";
import { StudioJoiningAvModal } from "../studio-pages/studioEntryAvModal";
import { MainStageContainer } from "../events-pages/site-modules/MainStageContainer";
import { StagePage } from "../events-pages/zones/StagePage";

export class WebinarStage {
  readonly page: Page;

  readonly joinBackStageButtonOnBottomBar: Locator;
  readonly interactionPanel: InteractionPanel;
  readonly attendeePreviewOfStage: FrameLocator;
  //welcome stage modal
  readonly viewAsAttendeeButton: Locator;
  readonly goBackstageButton: Locator;
  //main stage components
  readonly stagePreviewContainer: Locator;
  //speaker stream
  readonly speakerNameTileOnStream: Locator;
  readonly streamTilesOnStagePreview: Locator;
  //pdf presentation
  readonly pdfPresentationStream: Locator;
  // video playback
  readonly videoPlayBackStream: Locator;
  // logo
  readonly logoIndicatorOnMainStage: Locator;
  // background
  readonly backGroundImageIndicatorOnMainStage: Locator;
  // overlay
  readonly overLayImageIndicatorOnMainStage: Locator;
  // banner
  readonly bannerContainerOnMainStage: Locator;
  // filler image
  readonly fillerImageContainerOnMainStage: Locator;
  // timer Container
  readonly timerContainerOnMainStage: Locator;
  readonly mainStageContainer: MainStageContainer;
  readonly stagePage: StagePage;
  readonly stageVideoContainers: Locator;
  readonly stageLoadingContainer: Locator;

  // shared content
  readonly presentationContainer: Locator;

  constructor(page: Page) {
    this.page = page;
    this.stagePage = new StagePage(this.page);
    this.interactionPanel = new InteractionPanel(this.page);
    this.mainStageContainer = new MainStageContainer(this.page);

    this.joinBackStageButtonOnBottomBar = this.page.locator(
      "div[class^='styles-module__bottomStreamBar'] button:has-text('JOIN BACKSTAGE')"
    );
    this.viewAsAttendeeButton = this.page.locator(
      "button[data-testid='view_as_attendee']"
    );
    this.goBackstageButton = this.page.locator(
      "button[data-testid='go_backstage']"
    );
    this.attendeePreviewOfStage = this.page.frameLocator(
      "iframe[title='Stage Studio']"
    );
    this.stagePreviewContainer = this.attendeePreviewOfStage.locator(
      "#stageOutputPreviewContainer"
    );
    this.speakerNameTileOnStream = this.attendeePreviewOfStage.locator(
      "div[class^='stage-stream-title_nameTitleThemeContainer']"
    );
    this.streamTilesOnStagePreview = this.attendeePreviewOfStage.locator(
      "div[class^='stage-stream_stageStreamContainer']"
    );
    this.pdfPresentationStream = this.attendeePreviewOfStage.locator(
      "div[class^='presentation_container']"
    );
    this.videoPlayBackStream = this.page
      .frameLocator('iframe[title="Stage Studio"]')
      .locator("video");
    this.logoIndicatorOnMainStage = this.attendeePreviewOfStage.locator(
      "#stageOutputPreviewContainer div[class^='logo_logoImageContainer'] img[class*='crossfade-effect_fadeOut']"
    );
    this.backGroundImageIndicatorOnMainStage =
      this.attendeePreviewOfStage.locator(
        "#stageOutputPreviewContainer img[class*='background_backgroundImage'][class*='crossfade-effect_fadeOut']"
      );
    this.overLayImageIndicatorOnMainStage = this.attendeePreviewOfStage.locator(
      "#stageOutputPreviewContainer img[class*='overlay_overlayImage'][class*='crossfade-effect_fadeOut']"
    );
    this.bannerContainerOnMainStage = this.stagePreviewContainer.locator(
      "div[class^='banner_bannerContainer']"
    );
    this.fillerImageContainerOnMainStage = this.attendeePreviewOfStage
      .locator("div[class^='filler-screen_container'] img[src]")
      .first();
    this.timerContainerOnMainStage = this.attendeePreviewOfStage.locator(
      "div[class^='timer_timerContainer']"
    );
    this.stageVideoContainers = this.attendeePreviewOfStage.locator(
      "div[class*='stageStreamContainer']"
    );
    this.stageLoadingContainer = this.attendeePreviewOfStage.locator(
      "div[class*='waiting-screen_container']"
    );
    this.presentationContainer = this.attendeePreviewOfStage.locator(
      "div[class*='presentation_container']"
    );
  }

  //Getter functions
  get getMainStageContainer() {
    return this.mainStageContainer;
  }

  get getStagePage() {
    return this.stagePage;
  }

  get getInteractionPanel() {
    return this.interactionPanel;
  }

  //Actions
  //only organiser can perform this
  async clickOnJoinBackStageButtonOnBottomNavBar() {
    await this.joinBackStageButtonOnBottomBar.click();
    return new StudioJoiningAvModal(this.page);
  }

  async clickOnViewAsAttendeeButtonOnWelcomeStageModal() {
    await this.viewAsAttendeeButton.click();
  }

  async clickOnGoBackstageOnWelcomeStageModal() {
    await this.goBackstageButton.click();
    return new StudioJoiningAvModal(this.page);
  }

  async verifySpeakerStreamWithNameIsVisibleForAttendee(speakerName: string) {
    await expect(
      this.speakerNameTileOnStream.filter({ hasText: speakerName })
    ).toBeVisible();
  }

  async verifySpeakerStreamWithNameIsNotVisibleForAttendee(
    speakerName: string
  ) {
    await expect(
      this.speakerNameTileOnStream.filter({ hasText: speakerName })
    ).not.toBeVisible();
  }

  async verifyPdfStreamIsVisibleForAttendee() {
    await expect(
      this.pdfPresentationStream,
      "expecting attendee should be able to view pdf stream on stage"
    ).toBeVisible({ timeout: 20000 });
  }

  async verifyPdfStreamIsNotVisibleForAttendee() {
    await expect(
      this.pdfPresentationStream,
      "expecting attendee should be able to view pdf stream on stage"
    ).not.toBeVisible({ timeout: 20000 });
  }

  async verifyStagePreviewVisible() {
    await expect(this.stagePreviewContainer).toBeVisible({ timeout: 20000 });
  }
  async verifyStagePreviewIsNotVisible() {
    await expect(this.stagePreviewContainer).not.toBeVisible();
  }

  getSpeakerStreamTileWithName(speakerName: string) {
    const thisSpeakerTile = this.streamTilesOnStagePreview.filter({
      hasText: speakerName,
    });
    return thisSpeakerTile;
  }

  async verifyVideoStreamIsVisibleForSpeaker(speakerName: string) {
    const speakerStreamLocator = this.getSpeakerStreamTileWithName(speakerName);
    const videoStreamLocator = speakerStreamLocator.locator("video");
    await expect(videoStreamLocator).toBeVisible({ timeout: 20000 });
  }

  async verifyVideoStreamIsNotVisibleForSpeaker(speakerName: string) {
    const speakerStreamLocator = this.getSpeakerStreamTileWithName(speakerName);
    const videoStreamLocator = speakerStreamLocator.locator("video");
    await expect(videoStreamLocator).not.toBeVisible({ timeout: 20000 });
  }

  async verifyVideoPlayBackIsVisibleOnStage() {
    await expect(
      this.videoPlayBackStream,
      "expecting video playback container to be visible"
    ).toBeVisible({ timeout: 20000 });
  }

  async verifyFillerImageBroadcastIsVisibleOnStage() {
    await expect(this.fillerImageContainerOnMainStage).toBeVisible();
  }
  async verifyFillerImageBroadcastIsNotVisibleOnStage() {
    await expect(this.fillerImageContainerOnMainStage).not.toBeVisible();
  }

  async verifyVideoPlayBackIsNotVisibleOnStage() {
    await expect(
      this.videoPlayBackStream,
      "expecting video playback container to not be visible"
    ).not.toBeVisible();
  }
  async verifyIfLogoIsDisplayedOnMainStage() {
    await expect(
      this.logoIndicatorOnMainStage,
      "expecting logo container img tag to have fadeout class with count 1"
    ).toHaveCount(1, { timeout: 15000 });
  }

  async verifyLogoIsNotDisplayedOnMainStage() {
    await expect(
      this.logoIndicatorOnMainStage,
      "expecting logo container img tag to have fadeout class with count 2"
    ).toHaveCount(2, { timeout: 15000 });
  }

  async verifyBackGroundIsAppliedOnMainstage() {
    await expect(
      this.backGroundImageIndicatorOnMainStage,
      "expecting background container to have 1 image tag with fadeout class"
    ).toHaveCount(1, { timeout: 15000 });
  }

  async verifyBackGroundIsNotAppliedOnMainstage() {
    await expect(
      this.backGroundImageIndicatorOnMainStage,
      "expecting background container to have 1 image tag with fadeout class"
    ).toHaveCount(2, { timeout: 15000 });
  }

  async verifyBannerIsDisplayedOnMainStage(bannerText: string) {
    await test.step(`verifying that attendee is able to see banner with ${bannerText} on the main stage`, async () => {
      await expect(
        this.bannerContainerOnMainStage,
        "expecting banner contianer to be visible"
      ).toBeVisible();
      await expect(
        this.bannerContainerOnMainStage,
        "expecting banner contianer to contain given text"
      ).toContainText(bannerText);
    });
  }

  async verifyBannerIsNotDisplayedOnMainStage() {
    await test.step(`verifying that attendee is not able to see any banner on  main stage`, async () => {
      await expect(this.bannerContainerOnMainStage).not.toBeVisible();
    });
  }

  async verifyOverLayIsBeingDisplayedOnMainStage() {
    await expect(
      this.overLayImageIndicatorOnMainStage,
      "expecting overlay container to have 1 image tag with fadeout class"
    ).toHaveCount(1, { timeout: 15000 });
  }

  async verifyOverLayIsNotBeingDisplayedOnMainStage() {
    await expect(
      this.overLayImageIndicatorOnMainStage,
      "expecting overlay container to have 2 image tag with fadeout class"
    ).toHaveCount(2, { timeout: 15000 });
  }

  async verifyTimerVisibility(shouldBePresent: boolean = true) {
    if (shouldBePresent) {
      await expect(this.timerContainerOnMainStage).toBeVisible({
        timeout: 40000,
      });
    } else {
      await expect(this.timerContainerOnMainStage).not.toBeVisible({
        timeout: 40000,
      });
    }
  }

  async verifyAttendeeScreenIsLoaded() {
    await expect(this.stageLoadingContainer).not.toBeVisible({
      timeout: 50000,
    });
  }

  async verifyNumberOfVisibileStreams(numberOfStreams: number) {
    await expect(this.stageVideoContainers).toHaveCount(numberOfStreams, {
      timeout: 45000,
    });
  }

  async verifyPresentationIsVisible(presentationStatus: boolean = true) {
    if (presentationStatus) {
      await expect(this.presentationContainer).toBeVisible({
        timeout: 45000,
      });
    } else {
      await expect(this.presentationContainer).not.toBeVisible({
        timeout: 45000,
      });
    }
  }
}
