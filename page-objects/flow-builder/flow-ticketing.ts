import { expect, FrameLocator, Locator, Page } from "@playwright/test";
import exp from "constants";
import { ExpectedTicketDetailsData } from "../../interfaces/FlowBuilerControllerInterface";
import { convertStringToNumber } from "../../util/commonUtil";
import { DataUtl } from "../../util/dataUtil";

type AddOn = {
  addOnName?: string;
  quantity?: number;
  price?: number;
};

export class FlowBuilderPage {
  readonly page: Page;
  readonly loginAsGuestButton: Locator;
  readonly formIframe: FrameLocator;
  readonly pageHeading: Locator;
  readonly disclaimerCheckbox: Locator;
  readonly continueButton: Locator;
  readonly countryCodeDropDown: Locator;
  readonly phoneNumberInput: Locator;
  readonly phoneNumberInputText: Locator;
  readonly multiSelectDropDownButton: Locator;
  readonly richTextContainer: Locator;
  readonly errorMessage: Locator;
  readonly disclaimerErrorMessage: Locator;
  readonly ticketDiv: Locator;
  readonly couponInput: Locator;
  readonly orderSummaryDiv: Locator;
  readonly paymentCountryDropDown: Locator;
  readonly editTicketButton: Locator;
  readonly editTicketModalContainer: Locator;
  readonly nestedFormIframe: FrameLocator;
  readonly makePaymentButton: Locator;
  readonly applyCodeButton: Locator;
  readonly ticketName: Locator;
  readonly ticketPrice: Locator;
  readonly couponAppliedText: Locator;
  readonly couponDiscountValue: Locator;
  readonly ticketTotalPrice: Locator;
  readonly ticketPriceLocator: Locator;
  readonly removeCouponCodeLocator: Locator;
  readonly transactionCodeLocator: Locator;
  readonly editDetailsLocator: Locator;
  readonly editDetailsSummary: Locator;
  readonly useOfflinePaymentButton: Locator;
  readonly copyInfoButton: Locator;
  readonly ticketingConfirmationWait: Locator;
  readonly ticketDetailsElement: Locator;
  readonly editDetailsFlow: Locator;
  readonly errorBanner: Locator;
  readonly creditCardRadioButton: Locator;
  readonly nextAttendeeButton: Locator;

  constructor(page: Page) {
    this.page = page;
    this.formIframe = this.page.frameLocator("#zuddl-flow iframe");
    this.nestedFormIframe = this.page
      .frameLocator("#zuddl-flow iframe")
      .frameLocator('iframe[name^="__privateStripeFrame"]');
    this.pageHeading = this.formIframe.getByRole("heading").nth(0);
    this.disclaimerCheckbox = this.formIframe.locator(
      "div[class^='Checkbox_container']"
    );
    // this.continueButton = this.formIframe.getByRole("button", {
    //   name: "Continue",
    // });

    // now continue button has two names, Continue and Register now
    let regexp = new RegExp("Continue|Register now|Save");
    this.continueButton = this.formIframe.getByRole("button", {
      name: regexp,
    });

    this.nextAttendeeButton = this.formIframe.getByRole("button", {
      name: "Next attendee",
    });
    this.makePaymentButton = this.formIframe.getByRole("button", {
      name: "Make payment",
    });
    this.phoneNumberInput = this.formIframe.locator(
      "div[class^='phone-input']"
    );
    this.countryCodeDropDown = this.phoneNumberInput.locator(
      "div[class^='Dropdown__indicators']"
    );
    this.phoneNumberInputText = this.phoneNumberInput.locator(
      "input[type='number']"
    );
    this.multiSelectDropDownButton = this.formIframe.locator(
      "div[class^='Dropdown__indicator Dropdown__dropdown-indicator']"
    );
    this.richTextContainer = this.formIframe.locator(
      "div[class^='rich-text_richTextContainer']"
    );
    this.errorMessage = this.formIframe
      .locator("p[class*='Input_errorMessage']")
      .first();
    this.disclaimerErrorMessage = this.formIframe.locator(
      "p[class*='Checkbox_errorMessage']"
    );
    this.couponInput = this.formIframe.getByPlaceholder("Enter coupon code");
    this.orderSummaryDiv = this.formIframe
      .locator("div[class^='order-summary_attendee']")
      .nth(0);

    this.paymentCountryDropDown = this.formIframe
      .locator("div[id^='react-select']")
      .first();
    this.editTicketButton = this.formIframe.locator(
      "div[class^='ticket-details_editButton']"
    );
    this.editTicketModalContainer = this.formIframe.locator(
      "div[class*='editTicketModalContainer']"
    );
    this.applyCodeButton = this.formIframe.getByText("Apply code");
    this.ticketName = this.formIframe
      .locator("p[class*='ticket-details_ticketTitle']")
      .first();
    this.ticketPrice = this.formIframe.locator("p[class*='TicketCart__price']");
    this.couponAppliedText = this.formIframe.locator(
      // "p[class*='TicketCart__coupon']"
      "div[class*='ticket-details_couponDiscount']"
    );
    this.couponDiscountValue = this.formIframe.locator(
      "p[class*='TicketCart__discount-value']"
    );
    this.ticketTotalPrice = this.formIframe.locator(
      "h6[class*='TicketCart__total-price']"
    );
    this.removeCouponCodeLocator = this.formIframe.getByText("Remove");
    this.transactionCodeLocator = this.formIframe.locator(
      "div[class*='thank-you-page_transactionId']"
    );
    this.editDetailsLocator = this.page.getByRole("button", {
      name: "Edit details",
    });
    this.editDetailsSummary = this.page
      .frameLocator("#zuddl-flow iframe")
      .getByRole("button", { name: "Edit details" })
      .last();

    this.useOfflinePaymentButton = this.formIframe.getByRole("button", {
      name: "Pay by invoice",
    });

    this.copyInfoButton = this.formIframe.getByRole("button", {
      name: "Copy info",
    });

    this.ticketingConfirmationWait = this.formIframe.getByText(
      "Do not close or refresh this"
    );

    this.ticketDetailsElement = this.formIframe.locator(
      "div[class^='ticket-details_ticketDetails']"
    );

    this.editDetailsFlow = this.formIframe.getByRole("button", {
      name: "Edit details",
    });

    this.errorBanner = this.formIframe
      .locator(".styles_errorBanner__4cynS")
      .first();

    this.creditCardRadioButton = this.formIframe.getByRole("button", {
      name: "Credit card",
    });
  }

  async getTextInputField(name: string) {
    await expect(this.formIframe.getByLabel(name)).toBeVisible({
      timeout: 25000,
    });
    await this.page.waitForTimeout(500);
    return this.formIframe.getByLabel(name);
  }

  async getPaymentInputField(name: string) {
    await expect(
      this.nestedFormIframe.first().getByPlaceholder(name)
    ).toBeVisible({
      timeout: 5000,
    });
    return this.nestedFormIframe.first().getByPlaceholder(name);
  }

  async getTicketDiv(name: string) {
    await expect(
      this.formIframe
        .locator("div[class^='ticket-card_mainContainer']")
        .filter({
          hasText: name,
        })
    ).toBeVisible({
      timeout: 5000,
    });

    return this.formIframe
      .locator("div[class^='ticket-card_mainContainer']")
      .filter({
        hasText: name,
      });
  }
  async isTicketDivPresent(name: string, timeout: number = 10000) {
    try {
      await expect(
        this.formIframe
          .locator("div[class^='ticket-card_mainContainer']")
          .filter({
            hasText: name,
          })
      ).toBeVisible({
        timeout: timeout,
      });
      return true;
    } catch (e) {
      return false;
    }
  }

  async fillRegistrationForm({
    fillNonMandatoryForm = true,
    fillLogic = true,
    weekend = true,
    isholiday = true,
    fillHoliday = true,
    logicRichText = true,
    userFirstName = "Test",
    userLastName = "User",
    userEmail = "testattendee@zuddl.com",
    expectErrorPreContinue = false,
    expectErrorPostContinue = false,
    expectedFormHeading,
    skipMandatoryDisclaimer = false,
    phoneNumber = "1234567890",
    luckyNumber = "6",
    country = "India",
    ticketInfo = {
      ticketName: "ticket1",
      quantity: 1,
      price: 0,
      coupon: "",
      expectSoldOut: false,
    },
    expectLockTicket = false,
    ticketAsFirstStep = false,
    attendeeDetails = [
      {
        firstName: "Test",
        lastName: "User",
        email: "",
      },
    ],
  }) {
    // check and if doesnt match then wait and check again
    let formHeading: string;
    await this.page.waitForTimeout(3000);
    try {
      formHeading = await this.pageHeading.innerText();
      expect(formHeading).toContain(expectedFormHeading);
    } catch (error) {
      await this.page.waitForTimeout(10000);
      formHeading = await this.pageHeading.innerText();
      expect(formHeading).toContain(expectedFormHeading);
    }

    switch (formHeading) {
      case "Form - Mandatory": {
        await this.handleMandatoryForm(
          userFirstName,
          userLastName,
          userEmail,
          skipMandatoryDisclaimer
        );
        break;
      }
      case "Non Mandatory Form - 1": {
        if (!fillNonMandatoryForm) {
          break;
        }
        await this.handleNonMandatoryForm(fillLogic, phoneNumber);
        break;
      }
      case "Selection Form": {
        await this.handleSelectionForm(weekend, fillHoliday, isholiday);
        break;
      }
      case "Logic Rich Text": {
        if (!logicRichText) {
          break;
        }
        await this.handleLogicRichText();
        break;
      }
      case "New Form": {
        await (
          await this.getTextInputField("newly added")
        ).fill("New field data");
        break;
      }
      case "Form": {
        await (await this.getTextInputField("First Name")).fill(userFirstName);
        await (await this.getTextInputField("Last Name")).fill(userLastName);
        await (await this.getTextInputField("Email address")).fill(userEmail);
        break;
      }
      case "Tickets": {
        await this.handleTicketing(
          ticketInfo.ticketName,
          ticketInfo.quantity,
          ticketInfo.price,
          ticketInfo.coupon,
          ticketInfo.expectSoldOut,
          expectLockTicket
        );
        await this.page.waitForTimeout(5000);
        if (ticketInfo.expectSoldOut) {
          return;
        }
        break;
      }
      case "Ticket details": {
        console.log(attendeeDetails);
        console.log(ticketInfo);
        await this.handleBulkTicketing(
          attendeeDetails,
          ticketInfo.quantity,
          ticketAsFirstStep
        );
        return;
      }
      case "Form - Mandatory-Integration": {
        await this.handleMandatoryFormWithIntegration(
          userFirstName,
          userLastName,
          userEmail
        );
        break;
      }
      case "phone number form": {
        await this.fillPhoneNumber(phoneNumber);
        break;
      }
      case "Are you lucky?": {
        await this.fillAreYouLuckyForm(luckyNumber);
        break;
      }
      case "Nationality Check": {
        await this.fillNationalityCheck(country);
        break;
      }

      case "After ticket is chosen": {
        // is optional so choosing to skip
        break;
      }
    }

    if (expectErrorPreContinue) {
      await expect(this.errorMessage).toBeVisible({
        timeout: 10000,
      });
    }
    if (expectLockTicket === false) {
      await this.continueButton.click({
        timeout: 5000,
      });
    }

    if (skipMandatoryDisclaimer) {
      await expect(this.disclaimerErrorMessage).toBeVisible({
        timeout: 10000,
      });
    }

    if (expectErrorPostContinue) {
      await expect(this.errorMessage).toBeVisible({
        timeout: 10000,
      });
    }
  }

  async handleMultiSelect(toBeSelected: string[]) {
    await this.multiSelectDropDownButton.click();

    for (const option of toBeSelected) {
      await this.page.keyboard.type(option);
      await this.page.keyboard.press("Enter");
    }
  }

  async verifyFreeTicketDetails(
    isCouponApplied: boolean,
    ticketDetailsData: ExpectedTicketDetailsData
  ) {
    await expect(this.ticketName).toContainText(ticketDetailsData.ticketName);
    await expect(this.ticketPrice).toContainText(
      ticketDetailsData.totalPriceAfterDiscount
    );
    if (isCouponApplied) {
      await expect(this.couponDiscountValue).toContainText(
        ticketDetailsData.dicountValue
      );
      await expect(this.couponAppliedText).toBeVisible();
    }
    await expect(this.ticketTotalPrice).toContainText(
      ticketDetailsData.ticketPrice
    );
  }

  async verifyPaidTicketDetails(
    isCouponApplied: boolean,
    ticketDetailsData: ExpectedTicketDetailsData
  ) {
    await expect(this.ticketName).toContainText(ticketDetailsData.ticketName);
    if (isCouponApplied) {
      await expect(this.couponDiscountValue).toContainText(
        ticketDetailsData.dicountValue
      );
      await expect(this.couponAppliedText).toBeVisible();
      let actualTotalPriceAfterDiscount =
        "$" +
        (
          (await convertStringToNumber(ticketDetailsData.ticketPrice)) +
          (await convertStringToNumber(
            await this.couponDiscountValue.textContent()
          ))
        ).toString();
      await expect(this.ticketTotalPrice).toHaveText(
        actualTotalPriceAfterDiscount
      );
    } else {
      await expect(this.ticketTotalPrice).toContainText(
        ticketDetailsData.ticketPrice
      );
    }
  }

  async verifyPaidTicketDetailsWithAddOns(
    isCouponApplied: boolean,
    ticketDetailsData: ExpectedTicketDetailsData,
    addOns: AddOn[]
  ) {
    await expect(this.ticketName).toContainText(ticketDetailsData.ticketName);
    if (isCouponApplied) {
      await expect(this.couponDiscountValue).toContainText(
        ticketDetailsData.dicountValue
      );
      // await expect(this.couponAppliedText).toBeVisible();
      let actualTotalPriceAfterDiscount = `$${(
        (await convertStringToNumber(ticketDetailsData.ticketPrice)) -
        (await convertStringToNumber(ticketDetailsData.dicountValue)) +
        addOns.reduce((acc, addOn) => acc + addOn.price, 0)
      ).toString()}`;

      console.log(addOns.reduce((acc, addOn) => acc + addOn.price, 0));

      await expect(this.ticketTotalPrice).toHaveText(
        actualTotalPriceAfterDiscount
      );
    } else {
      await expect(this.ticketTotalPrice).toContainText(
        ticketDetailsData.ticketPrice
      );
    }
  }

  async verifySuccessFullRegistration({
    userFirstName = "Test",
    userLastName = "User",
    email = "testattendee@zuddl.com",
  }) {
    expect(
      this.formIframe
        .getByRole("heading", {
          name: "Your purchase was successful!",
        })
        .isVisible()
    );

    expect(
      this.formIframe
        .getByRole("heading", {
          name: "Thank you for registering!",
        })
        .isVisible()
    );
    expect(
      this.formIframe
        .getByText(
          "Your registration is complete. We've sent you an email with the ticket details."
        )
        .isVisible()
    );

    expect(
      this.formIframe.getByText(`${userFirstName} ${userLastName}`).isVisible()
    );
    expect(this.formIframe.getByText(email).isVisible());
  }

  async handleMandatoryForm(
    userFirstName: string,
    userLastName: string,
    userEmail: string,
    skipMandatoryDisclaimer: boolean
  ) {
    await (await this.getTextInputField("First Name")).fill(userFirstName);
    await (await this.getTextInputField("Last Name")).fill(userLastName);
    await (await this.getTextInputField("Email address")).fill(userEmail);
    await (await this.getTextInputField("Company")).fill("Zuddl");
    if (skipMandatoryDisclaimer) {
      return;
    }
    await this.disclaimerCheckbox.click();
  }
  async handleMandatoryFormWithIntegration(
    userFirstName: string,
    userLastName: string,
    userEmail: string
  ) {
    await (await this.getTextInputField("First Name")).fill(userFirstName);
    await (await this.getTextInputField("Last Name")).fill(userLastName);
    await (await this.getTextInputField("Email address")).fill(userEmail);
    await (
      await this.getTextInputField("MyField")
    ).fill("my custom field text");
  }

  async handleNonMandatoryForm(fillLogic: boolean, phoneNumber: string) {
    await this.countryCodeDropDown.click();
    await this.page.keyboard.type("+91");
    await this.page.keyboard.press("Enter");
    // check if phoneNumber string has non-numeric characters, if yes then verify that after filling the input field, the value is only numeric
    if (phoneNumber.match(/\D/g)) {
      await this.phoneNumberInputText.click();
      await this.page.keyboard.type(phoneNumber);
      await this.page.waitForTimeout(1000);
      expect(
        await this.phoneNumberInputText.inputValue(),
        "Expecting alphabets to not be typed in."
      ).toMatch(/\d/g);
    } else {
      await this.phoneNumberInputText.fill(phoneNumber);
    }
    if (!fillLogic) {
      return;
    }
    await (await this.getTextInputField("Title - Optional")).fill("Engineer");
    // Wait for this.getTextInputField("Bio - Optional (with logic)") to be visible
    await (
      await this.getTextInputField("Bio - Optional (with logic)")
    ).fill("I am a test user");
  }

  async handleSelectionForm(
    weekend: boolean,
    fillHoliday: boolean,
    isholiday?: boolean
  ) {
    let holiday = isholiday;
    if (weekend) {
      await this.handleMultiSelect(["Saturday", "Sunday"]);
      await this.page.waitForTimeout(10000);
      if (fillHoliday) {
        await this.formIframe
          .locator("div[class^='Dropdown__indicators']")
          .nth(1)
          .click();
        if (holiday) {
          await this.page.keyboard.type("yes");
        } else {
          await this.page.keyboard.type("no");
        }
        await this.page.keyboard.press("Enter");
      }
    } else {
      await this.handleMultiSelect(["Monday", "Tuesday", "Wednesday"]);
    }
  }

  async handleLogicRichText() {
    await (
      await this.getTextInputField("Your LinkedIn")
    ).fill("https://www.linkedin.com/in/testuser/");
    await this.page.waitForTimeout(1000);
    await expect(this.richTextContainer).toBeVisible({
      timeout: 5000,
    });
    expect(await this.richTextContainer.innerText()).toContain(
      "Terms and conditions : Zuddl"
    );
    // expect an anchor tag (not text) to be present with href as http://zuddl.com
    let anchorTag = this.richTextContainer.locator("a");
    expect(await anchorTag.getAttribute("href")).toContain("zuddl.com");
  }

  async handleTicketing(
    ticketToSelect: string,
    quantity: number,
    price: number,
    coupon: string,
    expectSoldOut: boolean,
    expectLockTicket: boolean
  ) {
    await this.page.waitForTimeout(5000);
    let ticketDiv = await this.getTicketDiv(ticketToSelect);
    let priceLabel: string;
    if (price === 0) {
      priceLabel = "Free";
    } else {
      priceLabel = `$${price}`;
    }
    if (expectSoldOut) {
      // expect element with text SOLD-OUT to be present
      await expect(ticketDiv.getByText("SOLD OUT")).toBeVisible();
      return;
    }
    // expect the ticket div to contain the ticket name and price
    expect(await ticketDiv.innerText()).toContain(ticketToSelect);
    expect(await ticketDiv.innerText()).toContain(priceLabel);
    // expect the ticket div to contain the quantity selector
    if (expectLockTicket) {
      await this.verifyLockedTicketPriceVisible(ticketToSelect, priceLabel);
      await this.verifyLockedTicketStatusVisible(ticketToSelect);
    } else {
      let quantitySelector = ticketDiv.locator(
        "div[class^='Dropdown__indicators']"
      );
      await expect(quantitySelector).toBeVisible({ timeout: 5000 });
      await quantitySelector.click();
      await this.page.keyboard.type(quantity.toString());
      await this.page.keyboard.press("Enter");
    }

    if (coupon !== "") {
      await this.page.waitForTimeout(5000);
      await this.couponInput.fill(coupon);
      await this.page.waitForTimeout(5000);
      await this.applyCodeButton.click();
    }
  }

  async applyCouponCode(coupon: string) {
    if (coupon !== "") {
      await this.page.waitForTimeout(5000);
      await this.couponInput.fill(coupon);
      await this.page.waitForTimeout(5000);
      await this.applyCodeButton.click();
    }
  }

  async verifyCouponInvalidity(errorType = "invalid") {
    let errorMessage;
    if (errorType === "invalid") {
      errorMessage = "Invalid coupon code. Try a different coupon.";
    } else if (errorType === "expired") {
      errorMessage = "Coupon expired";
    } else {
      errorMessage = RegExp(
        "Invalid coupon code. Try a different coupon.|Coupon expired"
      ); // regex for either invalid or expired
    }
    await expect(
      this.formIframe.locator("p[class*='Input_errorMessage']")
    ).toHaveText(errorMessage);
  }

  async verifyOrderSummary({
    dataToVerify = {
      firstName: "Jxtin",
      lastName: "zuddl",
      email: "attendee_1701781550508_588@1gb4osi2.mailosaur.net",
      ticketName: "ticket1",
    },
    editTicket = false,
    newTicket = {
      ticketName: "ticket2",
      expectSoldOut: false,
      quantity: 1,
    },
  }) {
    await expect(this.orderSummaryDiv).toBeVisible({ timeout: 5000 });
    let orderSummary = {};
    for (let i = 0; i < 4; i++) {
      let summaryItem = this.orderSummaryDiv.locator("div");
      orderSummary[
        await summaryItem.locator("p[class*='label']").nth(i).innerText()
      ] = await summaryItem.locator("p[class*='value']").nth(i).innerText();
    }
    console.log(orderSummary);

    expect(orderSummary["First name"]).toBe(dataToVerify.firstName);
    expect(orderSummary["Last name"]).toBe(dataToVerify.lastName);
    expect(orderSummary["Email"]).toBe(dataToVerify.email);
    expect(orderSummary["Ticket"]).toBe(dataToVerify.ticketName);
    if (editTicket) {
      await this.editTicketButton.click();
      await expect(
        this.editTicketModalContainer,
        "Expecting edit ticket modal to be visible."
      ).toBeVisible({
        timeout: 5000,
      });
      let ticketDiv = await this.getTicketDiv(newTicket.ticketName);
      if (newTicket.expectSoldOut) {
        // expect element with text SOLD-OUT to be present
        await expect(ticketDiv.getByText("SOLD OUT")).toBeVisible();
        return;
      }
      let quantitySelector = ticketDiv.locator(
        "div[class^='Dropdown__indicators']"
      );
      await expect(quantitySelector).toBeVisible({ timeout: 5000 });
      await quantitySelector.click();
      await this.page.keyboard.type(newTicket.quantity.toString());
      await this.page.keyboard.press("Enter");
      await this.formIframe
        .locator("#attendee-ticket-confirmation-popup")
        .getByRole("button", { name: "Continue" })
        .click();
      await this.page.waitForTimeout(5000);
    }
    await this.continueButton.click();
    if (editTicket) {
      if (newTicket.expectSoldOut) {
        return;
      }
      await this.continueButton.click();

      await this.verifyOrderSummary({
        dataToVerify: {
          firstName: dataToVerify.firstName,
          lastName: dataToVerify.lastName,
          email: dataToVerify.email,
          ticketName: newTicket.ticketName,
        },
      });
    }
  }

  async verifyOrderSummaryBulk({
    dataToVerify = {
      attendeeDetails: [
        {
          firstName: "Test",
          lastName: "User",
          email: "",
        },
      ],
      ticketName: "ticket1",
    },
  }) {
    const orderSummaryListDiv = this.formIframe.locator(
      "div[class^='accordion_accordionDetails']"
    );
    let orderSummaryBox = orderSummaryListDiv.locator(
      "div[class^='order-summary_attendeeDetailsLayout']"
    );
    for (let i = 0; i < dataToVerify.attendeeDetails.length; i++) {
      //p[class*='value'] -> user details
      // nth(4*i) -> firstName
      // nth(4*i+1) -> lastName
      // nth(4*i+2) -> email
      // nth(4*i+3) -> ticketName
      await expect(
        orderSummaryBox.locator("p[class*='value']").nth(4 * i)
      ).toHaveText(dataToVerify.attendeeDetails[i].firstName);
      await expect(
        orderSummaryBox.locator("p[class*='value']").nth(4 * i + 1)
      ).toHaveText(dataToVerify.attendeeDetails[i].lastName);
      await expect(
        orderSummaryBox.locator("p[class*='value']").nth(4 * i + 2)
      ).toHaveText(dataToVerify.attendeeDetails[i].email);
      await expect(
        orderSummaryBox.locator("p[class*='value']").nth(4 * i + 3)
      ).toHaveText(dataToVerify.ticketName);
    }
    const continueButton = this.formIframe.getByRole("button", {
      name: "Continue",
    });
    await continueButton.click();
  }

  async fillPaymentDetailsFreeTicket({
    firstName = "",
    lastName = "",
    company = "",
    emailAddress = "",
    country = "India",
    streetAddress = "Random Home",
    city = "Bhopal",
    zipCode = 111,
    coupon = "",
    wantToContinue = true,
    fillAddress = false,
  }) {
    if (coupon !== "") {
      await this.couponInput.fill(coupon);
    }
    if (firstName !== "") {
      await (
        await this.getTextInputField("First Name")
      ).fill(firstName, {
        timeout: 10000,
      });
    }
    if (lastName !== "") {
      await (
        await this.getTextInputField("Last Name")
      ).fill(lastName, {
        timeout: 10000,
      });
    }
    if (company !== "") {
      await (
        await this.getTextInputField("Company")
      ).fill(company, {
        timeout: 10000,
      });
    }
    if (emailAddress !== "") {
      await (
        await this.getTextInputField("Email address")
      ).fill(emailAddress, {
        timeout: 10000,
      });
    }

    if (!fillAddress) {
      if (wantToContinue) await this.continueButton.click();
      return;
    }
    await this.page.waitForTimeout(3000);
    if (!fillAddress) {
      if (wantToContinue) await this.continueButton.click();
      return;
    }
    await this.page.waitForTimeout(3000);
    await (
      await this.getTextInputField("Street and house number")
    ).fill(streetAddress);
    await (await this.getTextInputField("City")).fill(city);
    await (await this.getTextInputField("Zip code")).fill(zipCode.toString());
    if (wantToContinue) await this.continueButton.click();
  }

  async fillPaymentDetailsPaidTicket({
    cardNumber = "4242 4242 4242 4242",
    cvv = "123",
    expiryDate = "01/56",
  }) {
    await this.creditCardRadioButton.click({ timeout: 10000 });

    await this.fillPaymentDetailsFreeTicket({
      wantToContinue: false,
      fillAddress: true,
    });

    await (
      await this.getPaymentInputField("1234 1234 1234 1234")
    ).fill(cardNumber, { timeout: 10000 });
    await (
      await this.getPaymentInputField("CVC")
    ).fill(cvv, { timeout: 10000 });
    await (
      await this.getPaymentInputField("MM / YY")
    ).fill(expiryDate, {
      timeout: 10000,
    });
    await this.makePaymentButton.click({ timeout: 10000 });
    // wait till the payment options become invisible
    await (
      await this.getPaymentInputField("1234 1234 1234 1234")
    ).waitFor({
      state: "hidden",
      timeout: 25000,
    });
    await this.waitForPaymentConfirmation();
  }

  async useOfflinePaymentMethod({ }) {
    await this.useOfflinePaymentButton.click({ timeout: 10000 });
    await this.fillPaymentDetailsFreeTicket({
      wantToContinue: false,
      fillAddress: false,
    });
    await this.verifyBankTransferDetails({});
    await this.makePaymentButton.click({ timeout: 5000 });
  }

  async verifyBankTransferDetails({
    accountHolderName = "Account holder name",
    branchLocation = "Branch/location",
    accountNumber = "Account number",
    bankName = "Bank name",
    swiftCode = "SWIFT code",
    routingNumber = "Routing number",
  }) {
    const details = {
      "Account holder Name": accountHolderName,
      "Branch/location": branchLocation,
      "Account number": accountNumber,
      "Bank name": bankName,
      "SWIFT code": swiftCode,
      "Routing number": routingNumber,
    };
    for (const [key, value] of Object.entries(details)) {
      await this.formIframe.getByText(`${key}: ${value}`).isVisible();
    }
    await this.page
      .context()
      .grantPermissions(["clipboard-read", "clipboard-write"]);
    await this.copyInfoButton.click();
    await this.page.waitForTimeout(5000);
    const handle = await this.page.evaluateHandle(() =>
      navigator.clipboard.readText()
    );
    const clipboardContent = await handle.jsonValue();
    console.log(clipboardContent);
    expect(clipboardContent).toContain(
      `Account holder name: ${accountHolderName} 
Branch/location: ${branchLocation} 
Account number: ${accountNumber} 
Bank name: ${bankName} 
SWIFT code: ${swiftCode} 
Routing number: ${routingNumber} `
    );

    await handle.dispose();
  }

  async verifySingleTicketingSuccess({
    firstName = "Jxtin",
    lastName = "zuddl",
    email = "jxtin@zuddl.com",
    ticketDetails = {
      ticketName: "ticket1",
    },
  }) {
    expect(this.formIframe.getByText(`${firstName} ${lastName}`).isVisible());
    expect(this.formIframe.getByText(email).isVisible());
    expect(
      this.formIframe.getByText(`1 x ${ticketDetails.ticketName}`).isVisible()
    );
  }

  async verifyBulkTicketingSuccess({
    attendeeDetails = [
      {
        firstName: "Test",
        lastName: "User",
        email: "",
      },
    ],
    ticketDetails = {
      ticketName: "ticket1",
    },
    hasAddOns = false,
    addOnDetails = [
      {
        addOnName: "AddOn1",
        quantity: 1,
        price: 0,
      },
    ],
  }) {
    for (let attendeDetail of attendeeDetails) {
      await expect(
        this.formIframe.getByText(
          `${attendeDetail.firstName} ${attendeDetail.lastName}`
        )
      ).toBeVisible();
      await expect(
        this.formIframe.getByText(attendeDetail.email).first()
      ).toBeVisible();
    }
    await expect(
      this.formIframe.getByText(
        `${attendeeDetails.length} x ${ticketDetails.ticketName}`
      )
    ).toBeVisible();

    if (hasAddOns) {
      for (let addOnDetail of addOnDetails) {
        await expect(
          this.formIframe.getByText(
            `${addOnDetail.quantity} x ${addOnDetail.addOnName}`
          )
        ).toBeVisible();
      }
    }
  }

  async verifyOrderTotal({ price = 0 }) {
    await expect(
      this.formIframe.getByText(`Order total: $${price}`)
    ).toBeVisible();
  }

  async verifyNoTicketsToShow() {
    await expect(
      this.formIframe.getByText("There are no tickets to show")
    ).toBeVisible();
  }

  async handleBulkTicketing(
    attendeeDetails = [
      {
        firstName: "Test",
        lastName: "User",
        email: "",
      },
    ],
    ticketQty = 1,
    ticketFirstStep = false
  ) {
    expect(ticketQty).toBe(attendeeDetails.length);
    let startTicket = 1;
    if (ticketFirstStep) {
      startTicket = 0;
    }
    for (let i = startTicket; i < ticketQty; i++) {
      await (
        await this.getTextInputField("First Name")
      ).fill(attendeeDetails[i].firstName);
      await (
        await this.getTextInputField("Last Name")
      ).fill(attendeeDetails[i].lastName);
      await (
        await this.getTextInputField("Email address")
      ).fill(attendeeDetails[i].email);
      await this.page.waitForTimeout(1000);
      await this.continueButton.click();
      await this.page.waitForTimeout(1000);
    }
    await this.page.waitForTimeout(5000);
  }

  async getAvailableTicketCountRange({
    ticketName = "ticket1",
    expectSoldOut = false,
    price = 0,
  }) {
    await this.page.waitForTimeout(5000);
    let ticketDiv = await this.getTicketDiv(ticketName);
    let priceLabel: string;
    if (price === 0) {
      priceLabel = "Free";
    } else {
      priceLabel = `$${price}`;
    }
    if (expectSoldOut) {
      // expect element with text SOLD-OUT to be present
      await expect(ticketDiv.getByText("SOLD OUT")).toBeVisible();
      return;
    }
    // expect the ticket div to contain the ticket name and price
    expect(await ticketDiv.innerText()).toContain(ticketName);
    expect(await ticketDiv.innerText()).toContain(priceLabel);
    // expect the ticket div to contain the quantity selector
    let quantitySelector = ticketDiv.locator(
      "div[class^='Dropdown__indicators']"
    );
    await expect(quantitySelector).toBeVisible({ timeout: 5000 });
    await quantitySelector.click();
    await this.page.waitForTimeout(3000);
    let maxAvailableTicketText = await ticketDiv
      .locator("div[class^='Dropdown__option']")
      .last()
      .innerText();
    let minAvailableTicketText = await ticketDiv
      .locator("div[class^='Dropdown__option']")
      .nth(1) // first option is 0 always
      .innerText();

    let maxAvailableTicketCount = isNaN(parseInt(maxAvailableTicketText))
      ? 0
      : parseInt(maxAvailableTicketText);

    let minAvailableTicketCount = isNaN(parseInt(minAvailableTicketText))
      ? 0
      : parseInt(minAvailableTicketText);

    return [minAvailableTicketCount, maxAvailableTicketCount];
  }

  async chooseTicketQuantity({
    ticketName = "ticket1",
    quantity = 0,
    coupon = "",
    price = 0,
    expectSoldOut = false,
  }) {
    await this.page.waitForTimeout(5000);
    let ticketDiv = await this.getTicketDiv(ticketName);
    let priceLabel: string;
    if (price === 0) {
      priceLabel = "Free";
    } else {
      priceLabel = `$${price}`;
    }
    if (expectSoldOut) {
      // expect element with text SOLD-OUT to be present
      await expect(ticketDiv.getByText("SOLD OUT")).toBeVisible();
      return;
    }
    // expect the ticket div to contain the ticket name and price
    expect(await ticketDiv.innerText()).toContain(ticketName);
    expect(await ticketDiv.innerText()).toContain(priceLabel);
    // expect the ticket div to contain the quantity selector
    let quantitySelector = ticketDiv.locator(
      "div[class^='Dropdown__indicators']"
    );
    await expect(quantitySelector).toBeVisible({ timeout: 5000 });
    await quantitySelector.click();
    if (quantity === 0) {
      // get min available ticket count
      let minAvailableTicketText = await ticketDiv
        .locator("div[class^='Dropdown__option']")
        .nth(1) // first option is 0 always
        .innerText();
      quantity = parseInt(minAvailableTicketText);
    } else if (quantity === -1) {
      // get max available ticket count
      let maxAvailableTicketText = await ticketDiv
        .locator("div[class^='Dropdown__option']")
        .last()
        .innerText();
      quantity = parseInt(maxAvailableTicketText);
    }
    await this.page.keyboard.type(quantity.toString());
    await this.page.keyboard.press("Enter");
    if (coupon !== "") {
      await this.couponInput.fill(coupon);
    }
  }

  async verifyTicketChangeConfirmation() {
    await expect(
      this.formIframe.locator("#attendee-ticket-confirmation-popup"),
      "Expecting ticket change confirmation modal to be visible."
    ).toBeVisible({
      timeout: 5000,
    });
    await this.formIframe
      .locator("#attendee-ticket-confirmation-popup")
      .getByRole("button", { name: "Continue" })
      .click();
    await this.page.waitForTimeout(5000);
  }

  async verifyLockedTicketPriceVisible(
    ticketName: string,
    ticketPrice: string
  ) {
    const ticketPriceLocator = this.formIframe.locator(
      `//div[contains(@class,'leftSideContainer')]//h6[text()='${ticketName}']//parent::div//following-sibling::div//p[contains(@class,'Ticket__price')]`
    );
    await expect(ticketPriceLocator).toHaveText(ticketPrice);
  }

  async verifyLockedTicketStatusInvisible(ticketName: string) {
    const ticketPriceLocator = this.formIframe.locator(
      `//div[contains(@class,'leftSideContainer')]//h6[text()='${ticketName}']//parent::div//following-sibling::div//p[contains(@class,'Ticket__locked')]`
    );
    await expect(ticketPriceLocator).toBeHidden();
  }

  async verifyLockedTicketStatusVisible(ticketName: string) {
    const ticketPriceLocator = this.formIframe.locator(
      `//div[contains(@class,'leftSideContainer')]//h6[text()='${ticketName}']//parent::div//following-sibling::div//p[contains(@class,'Ticket__locked')]`
    );
    await expect(ticketPriceLocator).toBeVisible();
  }

  async checkTicketPresent(ticketName: string) {
    let ticketNameLocator = this.formIframe.getByRole("heading", {
      name: ticketName,
    });
    await expect(ticketNameLocator).toBeVisible();
  }

  async checkTicketNotPresent(ticketName: string) {
    let ticketNameLocator = this.formIframe.getByRole("heading", {
      name: ticketName,
    });
    await expect(ticketNameLocator).toBeHidden();
  }

  async removeCouponCode() {
    await this.removeCouponCodeLocator.click();
  }

  async selectQuantityFromDropdownAndVerifyCouponDetails(
    ticketToSelect: string,
    quantity: number,
    isPaid: boolean,
    isCouponApplied: boolean,
    ticketDetailsData: ExpectedTicketDetailsData
  ) {
    await this.page.waitForTimeout(5000);
    let ticketDiv = await this.getTicketDiv(ticketToSelect);
    let quantitySelector = ticketDiv.locator(
      "div[class^='Dropdown__indicators']"
    );
    await expect(quantitySelector).toBeVisible({ timeout: 5000 });
    await quantitySelector.click();
    await this.page.keyboard.type(quantity.toString());
    await this.page.keyboard.press("Enter");
    if (isPaid) {
      await this.verifyPaidTicketDetails(isCouponApplied, ticketDetailsData);
    } else {
      await this.verifyFreeTicketDetails(isCouponApplied, ticketDetailsData);
    }
  }

  async clickOnContinueButton() {
    await this.continueButton.click({
      timeout: 10000,
    });
  }

  async fillSingleFieldForm({
    expectedFormHeading = "Form",
    expectedFieldName = "First Name",
    fieldValue = "Test",
    expectError = false,
    clickContinue = true,
  }) {
    let formHeadingText = await this.pageHeading.innerText();
    expect(formHeadingText).toContain(expectedFormHeading);
    // field name is p tag with class containing Form__label
    let fieldName = this.formIframe.locator(`p[class*='Form__label']`);
    await expect(fieldName).toContainText(expectedFieldName);
    let inputField = this.formIframe.locator(`input`);
    await inputField.fill(fieldValue);
    if (clickContinue) {
      await this.continueButton.click();
    }
    if (expectError) {
      await expect(this.errorMessage).toBeVisible();
    } else {
      await this.page.waitForTimeout(5000);
    }
  }

  async fillSingleFieldFormTicket({
    expectedFormHeading = "Form",
    expectedFieldName = "First Name",
    fieldValue = "Test",
    expectError = false,
    clickContinue = true,
  }) {
    await expect(
      this.formIframe.getByRole("button", { name: expectedFormHeading })
    ).toBeVisible({ timeout: 5000 });

    await expect(this.formIframe.getByLabel(expectedFieldName)).toBeVisible({
      timeout: 5000,
    });

    await this.formIframe.getByLabel(expectedFieldName).fill(fieldValue);

    if (clickContinue) {
      await this.continueButton.click();
    }
    if (expectError) {
      await expect(this.errorMessage).toBeVisible();
    }
  }

  async handlePhoneNumber8Form({ luckyNumber = "5", countryName = "India" }) {
    let luckyNumberInput = this.formIframe.getByLabel(
      "What is your lucky number?"
    );
    await luckyNumberInput.fill(luckyNumber);

    await this.paymentCountryDropDown.click({
      force: true,
    });
    await this.page.waitForTimeout(3000);
    await this.page.keyboard.type(countryName);
    await this.page.waitForTimeout(3000);
    await this.page.keyboard.press("Enter");
    await this.continueButton.click();
    await this.page.waitForTimeout(5000);
  }

  async handleTicketForm({
    firstName = "Test",
    lastName = "User",
    email = "jxtin@zuddl.com",
  }) {
    await (
      await this.getTextInputField("First Name")
    ).fill(firstName, {
      timeout: 30000,
    });
    await (
      await this.getTextInputField("Last Name")
    ).fill(lastName, {
      timeout: 30000,
    });
    await (
      await this.getTextInputField("Email address")
    ).fill(email, {
      timeout: 30000,
    });
    await this.continueButton.click({ timeout: 10000 });
  }

  async verifySelectedTicketCount({
    ticketName = "ticket1",
    expectedTicketCount = 1,
  }) {
    await this.page.waitForTimeout(5000);
    let ticketDiv = await this.getTicketDiv(ticketName);
    let ticketQty = parseInt(
      await ticketDiv
        .locator("div[class^='Dropdown__single-value']")
        .innerText()
    );

    expect(ticketQty).toBe(expectedTicketCount);
  }

  async verifyCouponApplied({ couponCode = "test" }) {
    await expect(this.couponAppliedText).toBeVisible();
    return expect(this.couponInput).toHaveValue(couponCode);
  }

  async getTransactionCode() {
    return await this.transactionCodeLocator.innerText();
  }
  async getEditDetailsURL(eventId) {
    let transactionCode = await this.getTransactionCode();
    return `${DataUtl.getApplicationTestDataObj().baseUrl
      }/p/event/${eventId}/transaction/${transactionCode}`;
  }

  async fillOtpAndLogin(otp: string) {
    await this.page.waitForTimeout(5000);
    await this.page.keyboard.type(otp);
    await this.page.keyboard.press("Enter");
  }

  async verifyAttendeeDetails({
    firstName = "Test",
    lastName = "User",
    email = "jxtin@zuddl.com",
    ticketName = "ticket1",
    tktPrice = "$0",
  }) {
    await expect(this.page.getByText(firstName, { exact: true })).toBeVisible();
    await expect(this.page.getByText(lastName, { exact: true })).toBeVisible();
    await expect(this.page.getByText(email)).toBeVisible();
    await expect(
      this.page.getByText(ticketName, { exact: true })
    ).toBeVisible();
    await expect(
      this.page.getByText(`US${tktPrice}`, { exact: true })
    ).toBeVisible();
  }

  async editDetails({
    firstName = "Test",
    lastName = "User",
    email = "jxtin@zuddl.com",
  }) {
    await this.editDetailsLocator.click();
    await this.page.waitForTimeout(5000);
    await this.page.getByLabel("First name").fill(firstName);
    await this.page.getByLabel("Last name").fill(lastName);
    await this.page.getByLabel("Email address").fill(email);
    await this.page.locator("button").filter({ hasText: "Continue" }).click();
  }
  async editDetailsFlowForm({
    firstName = "Test",
    lastName = "User",
    email = "jxtin@zuddl.com",
  }) {
    await this.editDetailsFlow.first().click();
    await this.page.waitForTimeout(5000);
    await this.formIframe.getByLabel("First name").fill(firstName);
    await this.formIframe.getByLabel("Last name").fill(lastName);
    await this.formIframe.getByLabel("Email address").fill(email);
    await this.continueButton.click();
  }

  async verifyDetailsOnModifyLink({
    firstName = "Test",
    lastName = "User",
    email = "jxtin@zuddl.com",
  }) {
    await this.page.waitForTimeout(5000);
    await expect(this.page.getByLabel("First name")).toHaveValue(firstName);
    await expect(this.page.getByLabel("Last name")).toHaveValue(lastName);
    await expect(this.page.getByLabel("Email address")).toHaveValue(email);
  }

  async modifyDetails({ firstName = "Test", lastName = "User" }) {
    await this.page.getByLabel("First name").fill(firstName);
    await this.page.getByLabel("Last name").fill(lastName);
    await this.page.getByRole("button", { name: "Continue" }).click();
  }

  async waitForPaymentConfirmation() {
    await this.ticketDetailsElement.waitFor({
      state: "hidden",
      timeout: 25000,
    });
    await this.ticketingConfirmationWait.waitFor({
      state: "hidden",
      timeout: 25000,
    });
  }

  async expectTicket({
    ticketName = "ticket1",
    ticketPrice = 0,
    expectSoldOut = false,
    shouldBeVisible = true,
  }) {
    if (!shouldBeVisible) {
      await expect(
        this.formIframe.getByRole("heading", { name: ticketName })
      ).toBeHidden();
      return;
    }
    // validate ticket name and price
    let ticketDiv = await this.getTicketDiv(ticketName);
    let priceLabel: string;
    if (ticketPrice === 0) {
      priceLabel = "Free";
    } else {
      priceLabel = `$${ticketPrice}`;
    }
    if (expectSoldOut) {
      // expect element with text SOLD-OUT to be present
      await expect(ticketDiv.getByText("SOLD OUT")).toBeVisible();
      return;
    }
    // expect the ticket div to contain the ticket name and price
    expect(await ticketDiv.innerText()).toContain(ticketName);
    expect(await ticketDiv.innerText()).toContain(priceLabel);
  }

  async fillPhoneNumber(phoneNumber: string) {
    await this.countryCodeDropDown.first().click({ force: true });
    await this.page.keyboard.type("+91");
    await this.page.keyboard.press("Enter");

    await this.phoneNumberInputText.first().click();
    await this.page.keyboard.type(phoneNumber);
    await this.page.waitForTimeout(1000);
    expect(
      await this.phoneNumberInputText.first().inputValue(),
      "Expecting alphabets to not be typed in."
    ).toMatch(/\d/g);
  }

  async fillAreYouLuckyForm(luckyNumber: string) {
    await (await this.getTextInputField("Roll a dice")).fill(luckyNumber);
  }

  async fillNationalityCheck(country: string) {
    await this.paymentCountryDropDown.click({
      force: true,
    });
    await this.page.waitForTimeout(3000);
    await this.page.keyboard.type(country);
    await this.page.waitForTimeout(3000);
    await this.page.keyboard.press("Enter");
  }

  async switchTicket({
    ticketName = "ticket1",
    expectSoldOut = false,
    price = 0,
    quantity = 1,
  }) {
    await this.editDetailsFlow.click({
      timeout: 5000,
    });

    await expect(
      this.editTicketModalContainer,
      "Expecting edit ticket modal to be visible."
    ).toBeVisible({
      timeout: 5000,
    });
    let ticketDiv = await this.getTicketDiv(ticketName);
    if (expectSoldOut) {
      // expect element with text SOLD-OUT to be present
      await expect(ticketDiv.getByText("SOLD OUT")).toBeVisible();
      return;
    }
    let quantitySelector = ticketDiv.locator(
      "div[class^='Dropdown__indicators']"
    );
    await expect(quantitySelector).toBeVisible({ timeout: 5000 });
    await quantitySelector.click();
    await this.page.keyboard.type(quantity.toString());
    await this.page.keyboard.press("Enter");
    await this.formIframe
      .locator("#attendee-ticket-confirmation-popup")
      .getByRole("button", { name: "Continue" })
      .click();
    await this.page.waitForTimeout(5000);
  }

  async getAddonDiv(addOnName: string, expectVisible: boolean = true) {
    const addonDivLocator = this.formIframe
      .locator("div[class^='add-on-row_addOnContainer']")
      .filter({
        hasText: addOnName,
      })
      .first();
    if (expectVisible) {
      await expect(addonDivLocator).toBeVisible({
        timeout: 15000,
      });
      return addonDivLocator;
    }
    await expect(addonDivLocator).toBeHidden({
      timeout: 15000,
    });
  }

  async selectAddOns(addOns: AddOn[]) {
    let totalAddonQuantity = 0;
    let usedQuantity = 0;
    for (let addOn of addOns) {
      totalAddonQuantity += addOn.quantity;
    }
    for (let addOn of addOns) {
      for (let i = 0; i < addOn.quantity; i++) {
        await this.page.waitForTimeout(5000);
        let addOnDiv = await this.getAddonDiv(addOn.addOnName);
        let price: string;
        if (addOn.price === 0) {
          price = "Free";
        } else {
          price = `$${addOn.price}`;
        }
        console.log("AddOn: ", addOn.addOnName);
        await addOnDiv.getByRole("button", { name: "Select" }).click({
          force: true,
          timeout: 5000,
        });
        await this.page.waitForTimeout(5000);
        console.log("AddOn selected: ", addOn.addOnName);
        usedQuantity++;
        // on lastAddOn, dont click on next attendee button
        if (usedQuantity !== totalAddonQuantity) {
          await this.nextAttendeeButton.click({
            timeout: 5000,
            force: true,
          });
        }
      }
    }
    await this.continueButton.click({
      timeout: 5000,
    });
    await this.page.waitForTimeout(5000);
  }
}
