import { expect, test } from "@playwright/test";
import BrowserFactory from "./util/BrowserFactory";
import { DataUtl } from "./util/dataUtil";
import { fetchOtpFromEmail } from "./util/emailUtil";

test("Getting environment cookies", async () => {
  const run_env = process.env.run_env;
  if (run_env === "pre-prod" || run_env === "staging") {
    console.log(`Loading organiser cookies for env ${run_env}`);
    console.log(`No need to login to get cookies`);
  } else {
    console.log(`No cookies found for the given env`);
    let loginUrl = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/login`;
    console.log(`Login url is ${loginUrl}`);

    let orgBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });

    let orgLoginPage = await orgBrowserContext.newPage();
    await orgLoginPage.goto(loginUrl);
    console.log(`Login page opened`);
    await orgLoginPage.waitForTimeout(5000);
    await orgLoginPage
      .getByRole("button", { name: "Log in with email" })
      .click();
    await orgLoginPage.waitForTimeout(5000);
    console.log(`Clicked on login with email`);
    await orgLoginPage
      .getByPlaceholder("johndoe@example.com")
      .fill(DataUtl.getApplicationTestDataObj()["eventsOrganiserEmail"]);
    await orgLoginPage
      .getByRole("button", { name: "Request login code" })
      .click();

    let otp = await fetchOtpFromEmail(
      DataUtl.getApplicationTestDataObj()["eventsOrganiserEmail"],
      "zuddl"
    );

    await orgLoginPage.waitForTimeout(5000);
    await orgLoginPage.waitForTimeout(5000);
    await orgLoginPage.keyboard.type(otp);
    await orgLoginPage.keyboard.press("Enter");
    await expect(
      orgLoginPage.getByRole("heading", { name: "Home" }).locator("div")
    ).toBeVisible({
      timeout: 10000,
    });

    let cookies = await orgBrowserContext.cookies();
    console.log(`Cookies are ${cookies}`);
    await orgBrowserContext.storageState({
      path: `${run_env}.json`,
    });
  }
});
