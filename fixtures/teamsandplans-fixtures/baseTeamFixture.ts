import { test, APIRequestContext, BrowserContext, Page } from "@playwright/test";
import BrowserFactory from "../../util/BrowserFactory";
import { DataUtl } from "../../util/dataUtil";

type contextInfo = {
  page: Page;
  requestContext: APIRequestContext;
  context: BrowserContext;
};

type UserBrowserContextInfo = {
  ownerContextInfo: contextInfo;
  adminContextInfo: contextInfo;
  organiserContextInfo: contextInfo;
  moderatorContextInfo: contextInfo;
  speakerContextInfo: contextInfo;
  attendeeContextInfo: contextInfo;
};

type teamsTestData = {
  organiserEmail: string;
  speakerEmail: string;
  attendeeEmail: string;
  moderatorEmail: string;
};
let ownerBrowserContext: BrowserContext;
let adminBrowserContext: BrowserContext;

export const baseTeamTest = test.extend<UserBrowserContextInfo & teamsTestData>({
  ownerContextInfo: async ({ }, use) => {
    //owner context request and page
    ownerBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    const ownerPage = await ownerBrowserContext.newPage();
    const ownerApiContext = ownerPage.request;
    const ownerContextInfo = {
      page: ownerPage,
      requestContext: ownerApiContext,
      context: ownerBrowserContext,
    };
    await use(ownerContextInfo);
  },
  adminContextInfo: async ({ }, use) => {
    //admin context request and page
    adminBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    const adminPage = await adminBrowserContext.newPage();
    const adminApiContext = adminPage.request;
    const adminContextInfo = {
      page: adminPage,
      requestContext: adminApiContext,
      context: adminBrowserContext,
    };
    await use(adminContextInfo);
  },
  organiserContextInfo: async ({ }, use) => {
    const organiserBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    const organiserPage = await organiserBrowserContext.newPage();
    const organiserRequestContext = organiserBrowserContext.request;

    const organiserContextInfo = {
      page: organiserPage,
      requestContext: organiserRequestContext,
      context: organiserBrowserContext,
    };
    await use(organiserContextInfo);
    //once the test is over
    await organiserBrowserContext?.close();
  },
  moderatorContextInfo: async ({ }, use) => {
    const moderatorBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    const moderatorPage = await moderatorBrowserContext.newPage();
    const moderatorRequestContext = moderatorBrowserContext.request;

    const moderatorContextInfo = {
      page: moderatorPage,
      requestContext: moderatorRequestContext,
      context: moderatorBrowserContext,
    };
    await use(moderatorContextInfo);
    //once the test is over
    await moderatorBrowserContext?.close();
  },
  speakerContextInfo: async ({ }, use) => {
    const speakerBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    const speakerPage = await speakerBrowserContext.newPage();
    const speakerRequestContext = speakerBrowserContext.request;

    const speakerContextInfo = {
      page: speakerPage,
      requestContext: speakerRequestContext,
      context: speakerBrowserContext,
    };
    await use(speakerContextInfo);
    //once the test is over
    await speakerBrowserContext?.close();
  },
  attendeeContextInfo: async ({ }, use) => {
    const attendeeBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    const attendeePage = await attendeeBrowserContext.newPage();
    const attendeeRequestContext = attendeeBrowserContext.request;

    const attendeeContextInfo = {
      page: attendeePage,
      requestContext: attendeeRequestContext,
      context: attendeeBrowserContext,
    };
    await use(attendeeContextInfo);
    //once the test is over
    await attendeeBrowserContext?.close();
  },
  organiserEmail: async ({ }, use) => {
    const organiserEmail = DataUtl.getRandomOrganiserEmail();
    await use(organiserEmail);
  },
  speakerEmail: async ({ }, use) => {
    const speakerEmail = DataUtl.getRandomSpeakerEmail();
    await use(speakerEmail);
  },
  attendeeEmail: async ({ }, use) => {
    const attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await use(attendeeEmail);
  },
  moderatorEmail: async ({ }, use) => {
    const moderatorEmail = DataUtl.getRandomModeratorEmail();
    await use(moderatorEmail);
  }
}
);
baseTeamTest.afterAll(async () => {
  //once the all the tests are over
  await ownerBrowserContext?.close();
  await adminBrowserContext?.close();
});


