import { StagePage } from "../../page-objects/events-pages/zones/StagePage";
import { RoomsListingPage } from "../../page-objects/events-pages/zones/RoomsPage";
import { LobbyPage } from "../../page-objects/events-pages/zones/LobbyPage";
import { baseTest } from "./baseEventFixture";
import { expect } from "@playwright/test";
import { DataUtl } from "../../util/dataUtil";
import EventEntryType from "../../enums/eventEntryEnum";
import { QueryUtil } from "playwright-qa/DB/QueryUtil";

type announcementSuitePages = {
  //organiser pages
  organiserPages: {
    stagePage: StagePage;
    roomsListingPage: RoomsListingPage;
    lobbyPage: LobbyPage;
  };

  //speaker pages
  speakerPages: {
    stagePage: StagePage;
    roomsListingPage: RoomsListingPage;
    lobbyPage: LobbyPage;
  };

  //attendee pages
  attendeePages: {
    stagePage: StagePage;
    roomsListingPage: RoomsListingPage;
    lobbyPage: LobbyPage;
  };
};

type announcementTestData = {
  speakerEmail: string;
  attendeeEmail: string;
};

export const announcement_tests = baseTest.extend<
  announcementSuitePages & announcementTestData
>({
  //pages

  organiserPages: async ({ organiserContextInfo, eventInfoModal }, use) => {
    await announcement_tests.step(
      `Organiser loggin in through landing page`,
      async () => {
        await organiserContextInfo.page.goto(
          eventInfoModal.getAttendeeLandingPageLink()
        );
        await organiserContextInfo.page.click("text=Enter Now");
        await expect(
          organiserContextInfo.page,
          `expecting organiser to be present inside stage`
        ).toHaveURL(/stage/);
      }
    );

    await announcement_tests.step(
      `Initializing organiser pages in relation to announcement suite`,
      async () => {
        const stagePage = new StagePage(organiserContextInfo.page);
        const roomsListingPage = new RoomsListingPage(
          organiserContextInfo.page
        );
        const lobbyPage = new LobbyPage(organiserContextInfo.page);

        const organiserPages = {
          stagePage: stagePage,
          roomsListingPage: roomsListingPage,
          lobbyPage: lobbyPage,
        };
        await use(organiserPages);
      }
    );
  },

  speakerEmail: async ({}, use) => {
    const speakerEmail = DataUtl.getRandomSpeakerEmail();
    await use(speakerEmail);
  },

  attendeeEmail: async ({}, use) => {
    const attendeeEmail = DataUtl.getRandomAttendeeEmail();
    await use(attendeeEmail);
  },

  speakerPages: async (
    {
      speakerContextInfo,
      speakerEmail,
      eventController,
      dbUtil,
      eventInfoModal,
    },
    use
  ) => {
    await announcement_tests.step(
      `First enabling magic link for the speaker`,
      async () => {
        await eventController.updateEventLandingPageSettings({
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
          isSpeakermagicLinkEnabled: true,
        });
      }
    );

    await announcement_tests.step(
      `Disable onboarding checks for speaker`,
      async () => {
        await eventController.getAccessGroupController.handleOnboardingChecksForSpeaker(
          false
        );
      }
    );

    await announcement_tests.step(
      `Speaker login and enter inside the event`,
      async () => {
        await eventController.inviteSpeakerToTheEventByApi(
          speakerEmail,
          "prateekSpeaker",
          "QA"
        );
        const speakerMagicLink = await QueryUtil.fetchMagicLinkForSpeakerFromDB(
          eventInfoModal.eventId,
          speakerEmail
        );
        await speakerContextInfo.page.goto(speakerMagicLink);
        await speakerContextInfo.page.click("text=Enter Now");
        await expect(
          speakerContextInfo.page,
          `expecting speaker to be present inside stage`
        ).toHaveURL(/stage/);
      }
    );

    await announcement_tests.step(
      "Initializing speaker pages for this suite",
      async () => {
        const stagePage = new StagePage(speakerContextInfo.page);
        const roomsListingPage = new RoomsListingPage(speakerContextInfo.page);
        const lobbyPage = new LobbyPage(speakerContextInfo.page);

        const speakerPages = {
          stagePage: stagePage,
          roomsListingPage: roomsListingPage,
          lobbyPage: lobbyPage,
        };
        await use(speakerPages);
      }
    );
  },
  attendeePages: async (
    {
      attendeeContextInfo,
      dbUtil,
      eventInfoModal,
      attendeeEmail,
      eventController,
    },
    use
  ) => {
    await announcement_tests.step(
      `First enabling magic link for the attendee`,
      async () => {
        await eventController.updateEventLandingPageSettings({
          eventEntryType: EventEntryType.REG_BASED,
          isMagicLinkEnabled: true,
          isSpeakermagicLinkEnabled: true,
        });
      }
    );

    await announcement_tests.step(
      `Disable onboarding checks for attendee`,
      async () => {
        await eventController.getAccessGroupController.handleOnboardingChecksForAttendee(
          false
        );
      }
    );
    await announcement_tests.step(
      `Attendee login and enter inside the event`,
      async () => {
        await eventController.inviteAttendeeToTheEvent(attendeeEmail);
        const attendeeMagicLink = await QueryUtil.fetchMagicLinkFromDB(
          eventInfoModal.eventId,
          attendeeEmail
        );
        await attendeeContextInfo.page.goto(attendeeMagicLink);
        await attendeeContextInfo.page.click("text=Enter Now");
        await expect(
          attendeeContextInfo.page,
          `expecting attendee to be present inside Lobby`
        ).toHaveURL(/lobby/);
      }
    );

    await announcement_tests.step(
      `Initiating attendee pages for announcement suite`,
      async () => {
        const stagePage = new StagePage(attendeeContextInfo.page);
        const roomsListingPage = new RoomsListingPage(attendeeContextInfo.page);
        const lobbyPage = new LobbyPage(attendeeContextInfo.page);

        const attendeePages = {
          stagePage: stagePage,
          roomsListingPage: roomsListingPage,
          lobbyPage: lobbyPage,
        };
        await use(attendeePages);
      }
    );
  },
});
