import {
  test,
  APIRequestContext,
  BrowserContext,
  Page,
} from "@playwright/test";
import { EventController } from "../../controller/EventController";
import BrowserFactory from "../../util/BrowserFactory";
import { DataUtl } from "../../util/dataUtil";
import { DBUtil } from "../../util/dbUtil";
import { EventInfoModel } from "../../models/eventModels";

interface eventInfo {
  eventInfoModal: EventInfoModel;
}

interface eventSetupFixture extends eventInfo {
  eventController: EventController;
}

type contextInfo = {
  page: Page;
  requestContext: APIRequestContext;
  context: BrowserContext;
};

type UserBrowserContextInfo = {
  organiserContextInfo: contextInfo;
  speakerContextInfo: contextInfo;
  attendeeContextInfo: contextInfo;
};

type databaseUtil = {
  dbUtil: DBUtil;
};

export const baseTest = test.extend<
  UserBrowserContextInfo & eventSetupFixture & databaseUtil
>({
  organiserContextInfo: async ({}, use) => {
    const organiserBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: true,
    });
    const organiserPage = await organiserBrowserContext.newPage();
    const organiserRequestContext = organiserBrowserContext.request;

    const organiserContextInfo = {
      page: organiserPage,
      requestContext: organiserRequestContext,
      context: organiserBrowserContext,
    };
    await use(organiserContextInfo);
    //once the test is over
    await organiserBrowserContext?.close();
  },
  speakerContextInfo: async ({}, use) => {
    const speakerBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    const speakerPage = await speakerBrowserContext.newPage();
    const speakerRequestContext = speakerBrowserContext.request;

    const speakerContextInfo = {
      page: speakerPage,
      requestContext: speakerRequestContext,
      context: speakerBrowserContext,
    };
    await use(speakerContextInfo);
    //once the test is over
    await speakerBrowserContext?.close();
  },
  attendeeContextInfo: async ({}, use) => {
    const attendeeBrowserContext = await BrowserFactory.getBrowserContext({
      laodOrganiserCookies: false,
    });
    const attendeePage = await attendeeBrowserContext.newPage();
    const attendeeRequestContext = attendeeBrowserContext.request;

    const attendeeContextInfo = {
      page: attendeePage,
      requestContext: attendeeRequestContext,
      context: attendeeBrowserContext,
    };
    await use(attendeeContextInfo);
    //once the test is over
    await attendeeBrowserContext?.close();
  },

  eventInfoModal: async ({ organiserContextInfo }, use) => {
    await baseTest.step(`Generating a new event using API`, async () => {
      const eventTitle = DataUtl.getRandomEventTitle();
      const eventId = await EventController.generateNewEvent(
        organiserContextInfo.requestContext,
        {
          event_title: eventTitle,
        }
      );
      const eventInfoModal = new EventInfoModel(eventId, eventTitle);
      await use(eventInfoModal);
    });
  },

  eventController: async ({ organiserContextInfo, eventInfoModal }, use) => {
    const eventController = new EventController(
      organiserContextInfo.requestContext,
      eventInfoModal.eventId
    );
    await use(eventController);
  },

  dbUtil: async ({ organiserContextInfo }, use) => {
    const dbUtil = new DBUtil(organiserContextInfo.page);
    await use(dbUtil);
  },
});
