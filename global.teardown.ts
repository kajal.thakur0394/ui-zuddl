import { test } from "@playwright/test";
import BrowserFactory from "./util/BrowserFactory";
import { cancelEvent } from "./util/apiUtil";
import { QueryUtil } from "./DB/QueryUtil";
import staticEventIds from "./staticEvents.json";

test("Cancel Events as TearDown", async () => {
  let orgBrowserContext = await BrowserFactory.getBrowserContext({
    laodOrganiserCookies: true,
  });
  await test.step("Updating status as Canceled of events", async () => {
    let eventIdsArray = await QueryUtil.fetchEventListForNonCancelledEvent();
    console.log("Count of events to be canceled: ", eventIdsArray.length);
    console.log("Event Ids Array: ", eventIdsArray);
    // Call the function to cancel events
    await cancelEvents(eventIdsArray);
  });

  async function cancelEvents(eventIds: string[]) {
    for (let eventId of eventIds) {
      if (staticEventIds.includes(eventId)) {
        console.log(`Event (${eventId}) is in static event list, skipping`);
        continue;
      }
      try {
        await cancelEvent(orgBrowserContext.request, eventId);
        await new Promise((resolve) => setTimeout(resolve, 1000));
        console.log(`Event (${eventId}) successfully canceled`);
      } catch (error) {
        console.error(`Failed to cancel event (${eventId}):`, error);
      }
    }
  }
});
