export const Queries = {
  getMagicLink: (
    eventId: any,
    email: string
  ) => `SELECT a.registration_id,b.magic_token_id from event_registration a inner join magic_token b on a.registration_id = b.registration_id and a.event_id = b.event_id 
  where a.event_id = '${eventId}'  and b.is_valid = true and a.email='${email}' order by b.created_at desc limit 1;`,
  getSpeakerMagicLink: (
    eventId: any,
    email: string
  ) => `select b.speaker_id, b.magic_token_id from speaker a inner join magic_token b on a.speaker_id = b.speaker_id 
  where a.event_id = '${eventId}' and b.is_valid = true and a.email= '${email}' order by b.created_at desc limit 1`,
  getLoginOTPForEvent: (email: string, eventId: any) =>
    `select otp_code from otp where email='${email}' and event_id = '${eventId}'  and is_active = true order by id desc limit 1;`,
  getEventEmailRestrictionQuery: (eventId: any) =>
    `select email_restriction_type, restricted_email_domains_types,restricted_email_domains_filename from event_landing_page where event_id = '${eventId}';`,
  getVivenuTransactionRecord: (eventId: any, email: string) =>
    `select first_name, last_name, email, currency, price, regular_price, applied_coupons, total_tickets, company, street, city, postal from vivenu_transaction where email='${email}' and event_id='${eventId}' order by created_at desc limit 1;`,
  checkIfUserEmailIsActiveInEventRegTable: (eventId: any, email: string) =>
    `select is_active from event_registration where email = '${email}' and event_id = '${eventId}';`,
  checkIfUserActiveInOrganisation: (
    accountId: string,
    organisationId: string
  ) =>
    `select is_active from organization_member where organization_id='${organisationId}' and account_id='${accountId}';`,
    checkIfUserPresentInOrganisation: (
      organisationId: string,
      accountId: string,
    ) =>
      `select count(*) from organization_member where organization_id='${organisationId}' and account_id='${accountId}';`,
  checkIfUserEntryPresentInEventRegTable: (eventId: any, email: string) =>
    `select count(*) attendee_record_count from event_registration where email = '${email}' and event_id = '${eventId}';`,
  checkIfSpeakerRecordPresentInDBForEvent: (eventId: any, email: string) =>
    `select count(*) as speaker_record_count from speaker where email = '${email}' and event_id = '${eventId}';`,
  getUserAccessGroupIdForEvent: (eventId: any, email: string) =>
    `select u.access_group_id from account a inner join user_access_group_mapping u on a.account_id = u.account_id where a.email = '${email}' and u.event_id='${eventId}';`,
  getLoginOTPByVerificationId: (email: string, verificationId: string) =>
    `select otp_code from otp where email='${email}' and id='${verificationId}';`,
  getUserEventRoleData: (eventId: any, email: string) =>
    `select e.* from event_role e inner join account a on e.account_id = a.account_id where e.event_id = '${eventId}' and a.email = '${email}';`,
  getUserEventRegistrationData: (eventId: any, email: string) =>
    `select * from event_registration where event_id = '${eventId}' and email='${email}';`,
  getUserRegAccountEventRoleData: (eventId: any, email: string) =>
    `select a.email, a.registration_id, a.event_id, b.account_id,c.event_role_id, a.first_name as reg_first_name, a.last_name as reg_last_name,a.designation as reg_designation, a.company as reg_company, a.phone_number as reg_phone_number, 
    b.first_name as account_first_name, b.last_name as account_last_name, b.designation as account_designation,b.company as account_company,b.phone_number as account_phone_number,
    c.role, c.status
    from event_registration
    a left join account b on a.email = b.email 
    left join event_role c on c.account_id = b.account_id
    where a.event_id = '${eventId}' and a.email = '${email}';`,
  getUserDataV1BetaRegistration: (eventId: any, email: string) =>
    `select a.email, a.registration_id, a.event_id, b.account_id,c.event_role_id, a.first_name as reg_first_name, a.last_name as reg_last_name,a.designation as reg_designation, a.company as reg_company, a.phone_number as reg_phone_number, 
    b.first_name as account_first_name, b.last_name as account_last_name, b.designation as account_designation,b.company as account_company,b.phone_number as account_phone_number,
    c.role, c.status
    from event_registration
    a inner join account b on a.email = b.email 
    inner join event_role c on c.account_id = b.account_id
    where a.event_id = '${eventId}' and b.email = '${email}';`,
  getRegisterUserCustomFields: (eventId: any, email: string) =>
    `select custom_field from event_registration where event_id = '${eventId}' and email='${email}' limit 1;`,
  getEventIdListForNotCanceledEvents: (organisationId) =>
    `SELECT event_id 
FROM event WHERE organization_id = '${organisationId}' AND end_date_time >= CURRENT_TIMESTAMP AND status IN ('DRAFT', 'PUBLISHED');`,
};
