import { Client } from "pg";
import { determineDatabaseConfig } from "./DatabaseConfiguration";

export class DBConnection {
  private static instance: DBConnection;
  private dbConfig: any;
  private client: Client;

  private constructor() {
    this.dbConfig = determineDatabaseConfig();
  }

  public static getInstance(): DBConnection {
    if (!this.instance) {
      this.instance = new DBConnection();
    }
    return this.instance;
  }

  public async getClient(): Promise<Client> {
    if (!this.client) {
      this.client = new Client(this.dbConfig);
    }
    return this.client;
  }

  public async connect(): Promise<void> {
    try {
      await this.client.connect();
      console.log(`Connected to the database`);
    } catch (err) {
      console.error("Error connecting to the database:", err);
      throw err;
    }
  }

  public async disconnect(): Promise<void> {
    if (this.client && this.client._connected) {
      try {
        await this.client.end();
        console.log("Disconnected from the database");
      } catch (err) {
        console.error("Error disconnecting from the database:", err);
        throw err; // Rethrow the error to handle it where disconnect() is called.
      } finally {
        this.client = null; // Reset the client reference after disconnection.
      }
    }
  }
}
