import { QueryManager } from "./QueryManager";
import { DBConnection } from "./DBConnection";
import { DataUtl } from "../util/dataUtil";
import { expect } from "@playwright/test";
export class QueryUtil {
  static client: any;

  private static async disconnectClient() {
    if (this.client) {
      await DBConnection.getInstance().disconnect();
      this.client = null;
    }
  }

  private static async initializeClient() {
    if (!this.client) {
      console.log("inisde initialisese client");
      this.client = await DBConnection.getInstance().getClient();
    }
  }

  static async fetchMagicLinkFromDB(
    eventId: any,
    email: string
  ): Promise<string> {
    let magicTokenId: any;
    let registrationId: any;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${eventId}`);
      let resultSet = await this.client.query(
        QueryManager.getMagicLink(eventId, email)
      );
      registrationId = resultSet.rows[0].registration_id;
      magicTokenId = resultSet.rows[0].magic_token_id;
      let baseUrl = DataUtl.getApplicationTestDataObj().baseUrl;
      let magicLinkUrl = `${baseUrl}/p/a/event/${eventId}?re=${registrationId}&mt=${magicTokenId}`;
      console.log("magicLinkUrl: " + magicLinkUrl);
      return magicLinkUrl;
    } catch (error) {
      console.error("Error fetching magic link from the database:", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }

  static async fetchLoginOTPForEvent(
    user_email: string,
    eventId
  ): Promise<string> {
    let otpCode: any;
    await QueryUtil.initializeClient();
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${eventId}`);
      let resultSet = await this.client.query(
        QueryManager.getLoginOTPForEvent(user_email, eventId)
      );
      await new Promise((r) => setTimeout(r, 30000));
      if (resultSet.rows.length > 0) {
        otpCode = await resultSet.rows[0].otp_code;
      } else {
        await new Promise((r) => setTimeout(r, 30000));

        let resultSet = await this.client.query(
          QueryManager.getLoginOTPForEvent(user_email, eventId)
        );
        otpCode = await resultSet.rows[0].otp_code;
      }
      return otpCode;
    } catch (error) {
      console.error("Error fetching OTP-EventID from database", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient();
    }
  }

  static async fetchLoginOTPByVerificationId(
    user_email: string,
    verification_id: string
  ): Promise<string> {
    let otpCode: any;
    await QueryUtil.initializeClient();
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${verification_id}`);
      let resultSet = await this.client.query(
        QueryManager.getLoginOTPByVerificationId(user_email, verification_id)
      );
      await new Promise((r) => setTimeout(r, 30000));
      if (resultSet.rows.length > 0) {
        otpCode = await resultSet.rows[0].otp_code;
      } else {
        await new Promise((r) => setTimeout(r, 30000));
        let resultSet = await this.client.query(
          QueryManager.getLoginOTPByVerificationId(user_email, verification_id)
        );
        otpCode = await resultSet.rows[0].otp_code;
      }
      return otpCode;
    } catch (error) {
      console.error("Error fetching OTP-VerificationID from database", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient();
    }
  }

  static async validateEventEmailRestrictionInDB(
    event_id,
    expectedRestrictionType,
    expectedEmailDomainType,
    expectedCsvName
  ) {
    await QueryUtil.initializeClient();
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${event_id}`);
      let resultSet = await this.client.query(
        QueryManager.getEventEmailRestrictionQuery(event_id)
      );
      let actualEventEmailRestrictionType =
        resultSet.rows[0].email_restriction_type;
      expect(
        actualEventEmailRestrictionType,
        `expecting email restriction in DB to be ${expectedRestrictionType}`
      ).toBe(expectedRestrictionType);
      let actualRestrictedEmailDomainType =
        resultSet.rows[0].restricted_email_domains_types;
      expect(
        actualRestrictedEmailDomainType,
        `expecting email domain type to be ${expectedEmailDomainType}`
      ).toBe(expectedEmailDomainType);
      let actualRestrictedEmailCSVType =
        resultSet.rows[0].restricted_email_domains_filename;
      expect(
        actualRestrictedEmailCSVType,
        `expecting email domain filename to be ${expectedCsvName}`
      ).toBe(expectedCsvName);
    } catch (error) {
      console.error(
        "Error fetching Event Email Restriction data from database",
        error
      );
      throw error;
    } finally {
      await QueryUtil.disconnectClient();
    }
  }

  static async fetchDudaAttendeeMagicLinkFromDB(
    eventId,
    email: string,
    dudaPublishedAttendeeLandingPage: string
  ): Promise<string> {
    let magicTokenId: any;
    let registrationId: any;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${eventId}`);
      let resultSet = await this.client.query(
        QueryManager.getMagicLink(eventId, email)
      );
      registrationId = resultSet.rows[0].registration_id;
      magicTokenId = resultSet.rows[0].magic_token_id;
      let baseUrl = dudaPublishedAttendeeLandingPage;
      let magicLinkUrl = `${baseUrl}?re=${registrationId}&mt=${magicTokenId}`;
      console.log("magicLinkUrl: " + magicLinkUrl);
      return magicLinkUrl;
    } catch (error) {
      console.error("Error fetching duda magic link from the database:", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }
  static async fetchMagicLinkForSpeakerFromDB(
    eventId,
    email: string
  ): Promise<string> {
    let magicTokenId: any;
    let speakerId: any;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${eventId}`);
      let resultSet = await this.client.query(
        QueryManager.getSpeakerMagicLink(eventId, email)
      );
      speakerId = resultSet.rows[0].speaker_id;
      magicTokenId = resultSet.rows[0].magic_token_id;
      let baseUrl = DataUtl.getApplicationTestDataObj().baseUrl;
      let magicLinkUrl = `${baseUrl}/p/event/${eventId}?re=${speakerId}&mt=${magicTokenId}`;
      console.log(`speaker magic link is ${magicLinkUrl}`);
      return magicLinkUrl;
    } catch (error) {
      console.error(
        "Error fetching speaker magic link from the database:",
        error
      );
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }
  static async fetchDudaMagicLinkForSpeakerFromDB(
    eventId,
    email: string,
    dudaSpeakerLandingPage: string
  ): Promise<string> {
    let magicTokenId: any;
    let speakerId: any;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${eventId}`);
      let resultSet = await this.client.query(
        QueryManager.getSpeakerMagicLink(eventId, email)
      );
      speakerId = resultSet.rows[0].speaker_id;
      magicTokenId = resultSet.rows[0].magic_token_id;
      let baseUrl = dudaSpeakerLandingPage;
      let magicLinkUrl = `${baseUrl}&re=${speakerId}&mt=${magicTokenId}`;
      console.log(`speaker duda magic link is ${magicLinkUrl}`);
      return magicLinkUrl;
    } catch (error) {
      console.error(
        "Error fetching speaker duda magic link from the database:",
        error
      );
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }

  static async fetchCustomFieldsFilledByRegisteredUser(eventId, email: string) {
    let extractedCustomFields: any;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${eventId}`);
      let resultSet = await this.client.query(
        QueryManager.getRegisterUserCustomFields(eventId, email)
      );
      extractedCustomFields = resultSet.rows[0].custom_field;
      return extractedCustomFields;
    } catch (error) {
      console.error("Error fetching Custom Fields from the database:", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }

  static async fetchUserEventRegistrationDataFromDb(
    eventId,
    email: string
  ): Promise<object> {
    let userRegData: object;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${eventId}`);
      let resultSet = await this.client.query(
        QueryManager.getUserEventRegistrationData(eventId, email)
      );
      userRegData = resultSet.rows[0];
      expect(
        userRegData["registration_id"],
        "expecting registration id to be not null"
      ).not.toBe(null);
      return userRegData;
    } catch (error) {
      console.error("Error fetching EventRegData from the database:", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }
  static async fetchUserDataForV1BetaFlowValidation(
    eventId,
    email: string
  ): Promise<object> {
    let userRegData: object;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${eventId}`);
      let resultSet = await this.client.query(
        QueryManager.getUserDataV1BetaRegistration(eventId, email)
      );
      userRegData = resultSet.rows[0];
      expect(
        userRegData["registration_id"],
        "expecting registration id to be not null"
      ).not.toBe(null);
      expect(
        userRegData["event_role_id"],
        "expecting event role id to not be null"
      ).not.toBe(null);
      expect(
        userRegData["account_id"],
        "expecting account id not to be null"
      ).not.toBe(null);
      return userRegData;
    } catch (error) {
      console.error(
        "Error fetching User registration V1Betadata from the database:",
        error
      );
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }

  static async fetchUserDataFromEventRegAccountAndEventRoleTable(
    eventId,
    email: string
  ): Promise<object> {
    let userRegData: object;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${eventId}`);
      let resultSet = await this.client.query(
        QueryManager.getUserRegAccountEventRoleData(eventId, email)
      );
      userRegData = resultSet.rows[0].registration_id;
      return userRegData;
    } catch (error) {
      console.error(
        "Error fetching User registration data for EventRegAccount and Role from the database:",
        error
      );
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }
  static async fetchUserEventRoleData(eventId, email: string) {
    let userEventRoleData: object;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${eventId}`);
      let resultSet = await this.client.query(
        QueryManager.getUserEventRoleData(eventId, email)
      );
      userEventRoleData = await resultSet.rows[0];
      expect(
        userEventRoleData["account_id"],
        "expecting account id not to be null in event role data"
      ).not.toBe(null);
      return userEventRoleData;
    } catch (error) {
      console.error("Error fetching EventRoleData from the database:", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }

  static async fetchUserAccessGroupIdForEvent(email: string, eventId: string) {
    let accessGroupId: string;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${eventId}`);
      let resultSet = await this.client.query(
        QueryManager.getUserAccessGroupIdForEvent(eventId, email)
      );
      accessGroupId = resultSet.rows[0]["access_group_id"];
      expect(
        accessGroupId,
        "expecting account id not to be null in event role data"
      ).not.toBe(null);
      return accessGroupId;
      //check code
    } catch (error) {
      console.error(
        "Error fetching User AccessGroup from the database:",
        error
      );
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }

  static async verifyUserEmailIsPresentInSpeakerTableForAnEvent(
    email: string,
    eventId: string
  ) {
    let isSpeakerEmailPresentInSpeakerTable: boolean = false;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${eventId}`);
      let resultSet = await this.client.query(
        QueryManager.checkIfSpeakerRecordPresentInDBForEvent(eventId, email)
      );
      expect(
        parseInt(resultSet.rows[0]["speaker_record_count"]),
        "expecting speaker record count to be 1 for given email and event id"
      ).toBe(1);
      isSpeakerEmailPresentInSpeakerTable = true;
      return isSpeakerEmailPresentInSpeakerTable;
    } catch (error) {
      console.error("Error fetching User RegData from the database:", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }

  static async verifyUserEmailIsNotPresentInSpeakerTableForAnEvent(
    email: string,
    eventId: string
  ) {
    let isSpeakerEmailNotPresentInSpeakerTable: boolean = false;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${eventId}`);
      let resultSet = await this.client.query(
        QueryManager.checkIfSpeakerRecordPresentInDBForEvent(eventId, email)
      );
      expect(
        parseInt(resultSet.rows[0]["speaker_record_count"]),
        "expecting speaker record count to be 0 for given email and event id"
      ).toBe(0);
      isSpeakerEmailNotPresentInSpeakerTable = true;
      return isSpeakerEmailNotPresentInSpeakerTable;
    } catch (error) {
      console.error("Error fetching User RegData from the database:", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }

  static async verifyUserEmailIsPresentInEventRegTableForGivenEvent(
    email: string,
    eventId: string
  ) {
    let getEventRegTablEntryForEmail;
    let isUserEmailPresentInEventRegTableForGivenEvent: boolean = false;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${eventId}`);
      let resultSet = await this.client.query(
        QueryManager.checkIfUserEntryPresentInEventRegTable(eventId, email)
      );
      if (resultSet.rows.length > 0) {
        getEventRegTablEntryForEmail = resultSet.rows[0];
      } else {
        await new Promise((r) => setTimeout(r, 30000));
        let resultSet = await this.client.query(
          QueryManager.checkIfUserEntryPresentInEventRegTable(eventId, email)
        );
        getEventRegTablEntryForEmail = resultSet.rows[0];
      }
      expect(
        parseInt(getEventRegTablEntryForEmail["attendee_record_count"]),
        "expecting attendee record count to be 1 for given email and event id in event reg table"
      ).toBe(1);
      isUserEmailPresentInEventRegTableForGivenEvent = true;
      return isUserEmailPresentInEventRegTableForGivenEvent;
      //check code
    } catch (error) {
      console.error("Error fetching User RegData from the database:", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }
  static async verifyUserEmailIsNotPresentInEventRegTableForGivenEvent(
    email: string,
    eventId: string
  ) {
    let isUserEmailPresentInEventRegTableForGivenEvent: boolean = false;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${eventId}`);
      let resultSet = await this.client.query(
        QueryManager.checkIfUserEntryPresentInEventRegTable(eventId, email)
      );
      let getEventRegTablEntryForEmail = resultSet.rows[0];
      expect(
        parseInt(getEventRegTablEntryForEmail["attendee_record_count"]),
        "expecting attendee record count to be 1 for given email and event id in event reg table"
      ).toBe(1);
      isUserEmailPresentInEventRegTableForGivenEvent = true;
      return isUserEmailPresentInEventRegTableForGivenEvent;
      //check code
    } catch (error) {
      console.error("Error fetching User RegData from the database:", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }

  static async verifyIfUserEmailIsActiveInEventRegTableForGivenEvent(
    email: string,
    eventId: string
  ) {
    let isUserEmailPresentInEventRegTableForGivenEvent: boolean = false;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${eventId}`);
      let resultSet = await this.client.query(
        QueryManager.checkIfUserEmailIsActiveInEventRegTable(eventId, email)
      );
      let getEventRegTablEntryForEmail = await resultSet.rows[0];
      expect(
        getEventRegTablEntryForEmail["is_active"],
        "expecting is_active record count to be true for given email and eventId"
      ).toBe(true);
      isUserEmailPresentInEventRegTableForGivenEvent = true;
      return isUserEmailPresentInEventRegTableForGivenEvent;
    } catch (error) {
      console.error("Error fetching User EmailData from the database:", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }
  static async verifyIfUserEmailIsNotActiveInEventRegTableForGivenEvent(
    email: string,
    eventId: string
  ) {
    let isUserEmailPresentInEventRegTableForGivenEvent: boolean = false;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      console.log(`running query for ${eventId}`);
      let resultSet = await this.client.query(
        QueryManager.checkIfUserEmailIsActiveInEventRegTable(eventId, email)
      );
      let getEventRegTablEntryForEmail = await resultSet.rows[0];
      expect(
        getEventRegTablEntryForEmail["is_active"],
        "expecting is_active record count to be false for given email and eventId"
      ).toBe(false);
      isUserEmailPresentInEventRegTableForGivenEvent = false;
      return isUserEmailPresentInEventRegTableForGivenEvent;
    } catch (error) {
      console.error("Error fetching User emailData from the database:", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }
  static async verifyIfUserIsActiveInOrganisation(
    email: string,
    organisationId: string,
    accountId: string
  ) {
    let isUserActiveInOrganisation: boolean = false;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      let resultSet = await this.client.query(
        QueryManager.checkIfUserActiveInOrganisation(organisationId, accountId)
      );
      let getEventRegTablEntryForEmail = await resultSet.rows[0];
      expect(
        getEventRegTablEntryForEmail["is_active"],
        "expecting is_active record count to be true for given email and eventId"
      ).toBe(true);
      isUserActiveInOrganisation = true;
      return isUserActiveInOrganisation;
    } catch (error) {
      console.error("Error fetching User RegData from the database:", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }
  static async verifyIfUserIsInActiveInOrganisation(
    email: string,
    organisationId: string,
    accountId: string
  ) {
    let isUserActiveInOrganisation: boolean = false;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      let resultSet = await this.client.query(
        QueryManager.checkIfUserPresentInOrganisation(organisationId, accountId)
      );
      let getEventRegTablEntryForEmail = await resultSet.rows[0];
      expect(
        getEventRegTablEntryForEmail["is_active"],
        "expecting is_active record count to be false for given email and eventId"
      ).toBe(false);
      isUserActiveInOrganisation = false;
      return isUserActiveInOrganisation;
    } catch (error) {
      console.error("Error fetching User RegData from the database:", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }
  static async verifyIfUserPresentInOrganisation(
    email: string,
    organisationId: string,
    accountId: string
  ) {
    let isUserPresentInOrganisation: boolean = false;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      let resultSet = await this.client.query(
        QueryManager.checkIfUserPresentInOrganisation(organisationId, accountId)
      );
      let getEventRegTablEntryForEmail = await resultSet.rows[0];
      expect(
        parseInt(getEventRegTablEntryForEmail["count"]),
        "expecting record count to be greater than 0"
      ).toBeGreaterThan(0);
      isUserPresentInOrganisation = true;
      return isUserPresentInOrganisation;
    } catch (error) {
      console.error("Error fetching User RegData from the database:", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }

  static async verifyIfUserAbsentInOrganisation(
    email: string,
    organisationId: string,
    accountId: string
  ) {
    let isUserPresentInOrganisation: boolean = false;
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      let resultSet = await this.client.query(
        QueryManager.checkIfUserPresentInOrganisation(organisationId, accountId)
      );
      let getEventRegTablEntryForEmail = await resultSet.rows[0];
      expect(
        parseInt(getEventRegTablEntryForEmail["count"]),
        "expecting record count to be greater than 0"
      ).toEqual(0);
      isUserPresentInOrganisation = true;
      return isUserPresentInOrganisation;
    } catch (error) {
      console.error("Error fetching User RegData from the database:", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }
  //check from here
  static async fetchEventListForNonCancelledEvent() {
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      let resultSet = await this.client.query(
        QueryManager.getEventIdListForNotCanceledEvents()
      );
      let eventIdList = [];
      for (let i = 0; i < resultSet.rows.length; i++) {
        eventIdList.push(resultSet.rows[i].event_id);
      }
      return eventIdList;
      //check code
    } catch (error) {
      console.error("Error fetching Custom Fields from the database:", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }
  static async fetchVivenuTransactionRecord(eventId: string, email: string) {
    await QueryUtil.initializeClient(); // Ensure initializeClient() is always called first.
    console.log("Currently client is: " + this.client);
    await this.client.connect();
    try {
      let resultSet = await this.client.query(
        QueryManager.getVivenuTransactionRecord(eventId, email)
      );
      console.log("resultSet.rows[0]: ", resultSet.rows[0]);
      return resultSet.rows[0];
      //check code
    } catch (error) {
      console.error("Error fetching Custom Fields from the database:", error);
      throw error;
    } finally {
      await QueryUtil.disconnectClient(); // Ensure disconnectClient() is always called, even if an error occurs.
    }
  }
}
