import { Queries } from "./Queries";

export class QueryManager {
  static getMagicLink(eventId: any, email: string): string {
    return Queries.getMagicLink(eventId, email);
  }
  static getLoginOTPForEvent(email: string, eventId: any): string {
    return Queries.getLoginOTPForEvent(email, eventId);
  }
  static getLoginOTPByVerificationId(
    user_email: string,
    verification_id: string
  ): string {
    return Queries.getLoginOTPByVerificationId(user_email, verification_id);
  }
  static getSpeakerMagicLink(eventId: any, email: string): string {
    return Queries.getSpeakerMagicLink(eventId, email);
  }
  static getEventEmailRestrictionQuery(eventId: any) {
    return Queries.getEventEmailRestrictionQuery(eventId);
  }
  static getRegisterUserCustomFields(eventId: any, email: string): string {
    return Queries.getRegisterUserCustomFields(eventId, email);
  }
  static getUserEventRegistrationData(eventId: any, email: string): string {
    return Queries.getUserEventRegistrationData(eventId, email);
  }
  static getUserDataV1BetaRegistration(eventId: any, email: string) {
    return Queries.getUserDataV1BetaRegistration(eventId, email);
  }
  static getUserRegAccountEventRoleData(eventId: any, email: string) {
    return Queries.getUserRegAccountEventRoleData(eventId, email);
  }
  static getUserEventRoleData(eventId: any, email: string) {
    return Queries.getUserEventRoleData(eventId, email);
  }
  static getUserAccessGroupIdForEvent(eventId: any, email: string) {
    return Queries.getUserAccessGroupIdForEvent(eventId, email);
  }
  static checkIfSpeakerRecordPresentInDBForEvent(eventId: any, email: string) {
    return Queries.checkIfSpeakerRecordPresentInDBForEvent(eventId, email);
  }
  static checkIfUserEntryPresentInEventRegTable(eventId: any, email: string) {
    return Queries.checkIfUserEntryPresentInEventRegTable(eventId, email);
  }
  static checkIfUserEmailIsActiveInEventRegTable(eventId: any, email: string) {
    return Queries.checkIfUserEmailIsActiveInEventRegTable(eventId, email);
  }
  static getEventIdListForNotCanceledEvents(): string {
    const organisationId = {
      master: "b331838e-10e4-4588-9e2f-15fd5b8ce5e4",
      staging: "ff87ce2a-4b7f-4f9d-90e8-4c434a862564",
      "pre-prod": "83696f2a-2664-4aa9-8e48-954200efa54f",
    }; // add more or get from env

    // if process.env.run_env not in organisationId, return empty string
    if (!organisationId[process.env.run_env]) {
      return Queries.getEventIdListForNotCanceledEvents("");
    }
    return Queries.getEventIdListForNotCanceledEvents(
      organisationId[process.env.run_env]
    );
  }
  static getVivenuTransactionRecord(eventId: any, email: string): string {
    return Queries.getVivenuTransactionRecord(eventId, email);
  }
  static checkIfUserPresentInOrganisation(
    organisationId: string,
    accountId: string
  ): string {  
    return Queries.checkIfUserPresentInOrganisation(organisationId, accountId);
  }
  static checkIfUserActiveInOrganisation(
    organisationId: string,
    accountId: string
  ): string {
    return Queries.checkIfUserActiveInOrganisation(organisationId, accountId);
  }
}
