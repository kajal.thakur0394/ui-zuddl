import { log } from "console";

export interface DatabaseConfig {
  user: string;
  host: string;
  database: string;
  password: string;
  port: number;
  ssl: { rejectUnauthorized: boolean };
}

const config: DatabaseConfig = {
  user: process.env.POSTGRES_USER || "",
  host: process.env.POSTGRES_SERVER || "",
  database: process.env.EVENTS_DB_NAME || "",
  password: process.env.POSTGRES_USER_PASSWORD || "", // Use GitHub Secret for password,
  port: 5432,
  ssl: {
    rejectUnauthorized: false,
  },
};

export function determineDatabaseConfig(): DatabaseConfig {
  return dBConfig();
}

function dBConfig() {
  console.log("db-->" + JSON.stringify(config));
  return config;
}
