import applicationData from "../test-data/applicationData.json";

export class DataUtl {
  static getApplicationTestDataObj() {
    if (
      process.env.run_env == null ||
      process.env.run_env == undefined ||
      process.env.run_env == "staging"
    ) {
      //by default set to staging
      console.log(
        "Since no env variable is set for appEnv, invoking this for staging"
      );
      return applicationData.staging;
    } else if (process.env.run_env == "pre-prod") {
      return applicationData["pre-prod"];
    } else if (process.env.run_env == "master") {
      return applicationData.master;
    } else {
      throw Error("Error in invoking test data object");
    }
  }
  static getRandomUserEmail(prefix: string, emailDomain?): string {
    // "attendee"; //Optimise this code
    let randomString: string = Date.now().toString();
    let randomNum: string = Math.floor(Math.random() * 1000).toString();
    if (emailDomain == undefined) {
      emailDomain = this.getApplicationTestDataObj()["mailosaurDomain"];
    }
    const randomEmail = `${prefix}_${randomString}_${randomNum}@${emailDomain}`;
    console.log(`${randomEmail}`);
    return randomEmail;
  }

  static getRandomAttendeeEmail(emailDomain?): string {
    let prefix: string = "attendee";
    let randomString: string = Date.now().toString();
    let randomNum: string = Math.floor(Math.random() * 1000).toString();
    if (emailDomain == undefined) {
      emailDomain = this.getApplicationTestDataObj()["mailosaurDomain"];
    }
    const randomEmail = `${prefix}_${randomString}_${randomNum}@${emailDomain}`;
    console.log(`${randomEmail}`);
    return randomEmail;
  }

  static getRandomOrganiserEmail(emailDomain?): string {
    let prefix: string = "organiser";
    let randomString: string = Date.now().toString();
    let randomNum: string = Math.floor(Math.random() * 1000).toString();
    if (emailDomain == undefined) {
      emailDomain = this.getApplicationTestDataObj()["mailosaurDomain"];
    }
    const randomEmail = `${prefix}_${randomString}_${randomNum}@${emailDomain}`;
    console.log(`${randomEmail}`);
    return randomEmail;
  }

  static getRandomModeratorEmail(emailDomain?): string {
    let prefix: string = "moderator";
    let randomString: string = Date.now().toString();
    let randomNum: string = Math.floor(Math.random() * 1000).toString();
    if (emailDomain == undefined) {
      emailDomain = this.getApplicationTestDataObj()["mailosaurDomain"];
    }
    const randomEmail = `${prefix}_${randomString}_${randomNum}@${emailDomain}`;
    console.log(`${randomEmail}`);
    return randomEmail;
  }

  static getRandomAdminEmail(emailDomain?): string {
    let prefix: string = "admin";
    let randomString: string = Date.now().toString();
    let randomNum: string = Math.floor(Math.random() * 1000).toString();
    if (emailDomain == undefined) {
      emailDomain = this.getApplicationTestDataObj()["mailosaurDomain"];
    }
    const randomEmail = `${prefix}_${randomString}_${randomNum}@${emailDomain}`;
    console.log(`${randomEmail}`);
    return randomEmail;
  }

  static getRandomOrganisationOwnerEmail(emailDomain?): string {
    let prefix: string = "orgownerautomation";
    let randomString: string = Date.now().toString();
    let randomNum: string = Math.floor(Math.random() * 1000).toString();
    if (emailDomain == undefined) {
      emailDomain = this.getApplicationTestDataObj()["mailosaurDomain"];
    }
    const randomEmail = `${prefix}_${randomString}_${randomNum}@${emailDomain}`;
    console.log(`${randomEmail}`);
    return randomEmail;
  }

  static getRandomSenderEmailDomainWithVerifiedDns(
    emailDomain = "qa.zuddl.io"
  ) {
    let prefix: string = "customDomain";
    let randomString: string = Date.now().toString();
    let randomNum: string = Math.floor(Math.random() * 1000).toString();
    const randomEmail = `${prefix}_${randomString}_${randomNum}@${emailDomain}`;
    console.log(`random custom domain email is ${randomEmail}`);
    return randomEmail;
  }

  static getRandomSenderEmailDomainWithUnVerifiedDns(
    emailDomain = "qa1.zuddl.io"
  ) {
    let prefix: string = "customDomain";
    let randomString: string = Date.now().toString();
    let randomNum: string = Math.floor(Math.random() * 1000).toString();
    const randomEmail = `${prefix}_${randomString}_${randomNum}@${emailDomain}`;
    console.log(`random custom domain email is ${randomEmail}`);
    return randomEmail;
  }

  static getRandomAttendeeTestJoynEmail(): string {
    let prefix: string = "attendee";
    let randomString: string = Date.now().toString();
    let randomNum: string = Math.floor(Math.random() * 1000).toString();
    let emailDomain = "testjoyn.com";
    const randomEmail = `${prefix}_${randomString}_${randomNum}@${emailDomain}`;
    console.log(`${randomEmail}`);
    return randomEmail;
  }

  static getRandomAttendeeMailosaurEmail(): string {
    let prefix: string = "attendee";
    let randomString: string = Date.now().toString();
    let randomNum: string = Math.floor(Math.random() * 1000).toString();
    let emailDomain = this.getApplicationTestDataObj()["mailosaurDomain"];
    const randomEmail = `${prefix}_${randomString}_${randomNum}@${emailDomain}`;
    console.log(`${randomEmail}`);
    return randomEmail;
  }

  static getRandomSpeakerEmail(): string {
    let prefix: string = "speaker";
    let randomString: string = Date.now().toString();
    let randomNum: string = Math.floor(Math.random() * 1000).toString();

    let emailDomain = this.getApplicationTestDataObj()["mailosaurDomain"];
    const randomEmail = `${prefix}_${randomString}_${randomNum}@${emailDomain}`;
    console.log(`${randomEmail}`);
    return randomEmail;
  }

  static getRandomSpeakerMailosaurEmail(): string {
    let prefix: string = "speaker";
    let randomString: string = Date.now().toString();
    let randomNum: string = Math.floor(Math.random() * 1000).toString();

    let emailDomain = this.getApplicationTestDataObj()["mailosaurDomain"];
    const randomEmail = `${prefix}_${randomString}_${randomNum}@${emailDomain}`;
    console.log(`${randomEmail}`);
    return randomEmail;
  }

  static getRandomSpeakerTestJoynEmail(): string {
    let prefix: string = "speaker";
    let randomString: string = Date.now().toString();
    let randomNum: string = Math.floor(Math.random() * 1000).toString();

    let emailDomain = "testjoyn.com";
    const randomEmail = `${prefix}_${randomString}_${randomNum}@${emailDomain}`;
    console.log(`${randomEmail}`);
    return randomEmail;
  }

  static getRandomS3FileName() {
    let randomString: string = Date.now().toString();
    let randomNum: string = Math.floor(Math.random() * 100000).toString();
    return `${randomString}-${randomNum}`;
  }

  static getRandomEventTitle() {
    return Date.now().toString() + "_automationevent";
  }
  static getRandomTeamName() {
    let prefix = "team";
    let randomNum1: string = Math.floor(Math.random() * 1000).toString();
    let nameString = `${prefix} ${randomNum1}`;
    return nameString;
  }

  static getRandomCsvName(prefix = "attendee") {
    let randomCsvString: string = Date.now().toString();
    let randomNum1: string = Math.floor(Math.random() * 1000).toString();
    let randomNum2: string = Math.floor(Math.random() * 1000).toString();
    let randomNum3: string = Math.floor(Math.random() * 10).toString();

    return `${prefix}_${randomNum1}_${randomNum2}_${randomNum3}_${randomCsvString}.csv`;
  }

  static getRandomName() {
    let prefix = "automation";
    let randomNum1: string = Math.floor(Math.random() * 1000).toString();
    let nameString = `${prefix} ${randomNum1}`;
    return nameString;
  }

  static getListOfRandomAttendeeEmail(numberOfRecordsToGenerate: number) {
    const listOfRandomAttendees: string[] = [];
    while (numberOfRecordsToGenerate > 0) {
      const randomAttendeeEmail = DataUtl.getRandomAttendeeEmail();
      listOfRandomAttendees.push(randomAttendeeEmail);
      numberOfRecordsToGenerate--;
    }
    return listOfRandomAttendees;
  }

  static getRandomOrganisationName() {
    let prefix = "automationOrg";
    let randomNum1: string = Math.floor(Math.random() * 1000).toString();
    let nameString = `${prefix} ${randomNum1}`;
    return nameString;
  }
  static getRandomCouponName() {
    let prefix = "coupon";
    let randomNum1: string = Date.now().toString();
    let randomNum2: string = Math.floor(Math.random() * 1000).toString();
    let nameString = `${prefix} ${randomNum1} ${randomNum2}`;
    return nameString;
  }

  static getRandomColorCode(withHash: boolean = false) {
    let colorCode;
    if (withHash) {
      colorCode =
        "#" +
        (0x1000000 + Math.random() * 0xffffff).toString(16).substring(1, 7);
    } else {
      colorCode = (0x1000000 + Math.random() * 0xffffff)
        .toString(16)
        .substring(1, 7);
    }

    console.log(`Random Color code generated - ${colorCode}`);
    return colorCode;
  }

  static randomIntFromInterval(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  static generateRandomBoothName() {
    let prefix = "booth";
    let randomNum1: string = Math.floor(Math.random() * 1000).toString();
    let nameString = `${prefix} ${randomNum1}`;
    return nameString;
  }

  static getRandomImageUrl() {
    let randomImageNumber = Math.floor(Math.random() * 8) + 1;
    console.log(randomImageNumber);
    return `https://phoenixlive.imgix.net/website/bg-common-${randomImageNumber}.png`;
  }
}
