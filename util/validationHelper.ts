export function validateRegisteredUserCustomFieldDataWithDBData(
  userRegistrationExpectedData,
  fetchedDatafromDb,
  isDisclaimerPresent = false
) {
  /**
   * Test data object which was passed to the test case which tells what each field value is suppose to be
   * Custom field object fetched from metabase query
   * e.g data from db "{"selectyourfavcountries": "Italy,India", "Whichplaceyouwouldliketovisit": "Taj mahal(India)", "SelectyourfavcuisineoutofItalyandChina": "Italy"}"
   *  "email": "anyrandomemail@testjoyn.com",
        "firstName": "Prateek",
        "lastName": "Automation"
   */
  //neutralise the field name in db data
  const fetchedDbDataWithNeutralisedKeys =
    neutraliseEachKeyInDbTestData(fetchedDatafromDb);
  // filter only custom fields from given reigstration test data
  const filteredCustomFieldsData = filterOnlyCustomFieldsFromTestData(
    userRegistrationExpectedData
  );

  for (const [eachFieldName, eachFieldValue] of Object.entries(
    filteredCustomFieldsData
  )) {
    //convert field name to label name
    const neutralisedFieldLabelFromTestData =
      neutraliseTheGivenFieldName(eachFieldName);
    console.log(
      `checking if neutralised field ${neutralisedFieldLabelFromTestData} has expected data ${eachFieldValue}`
    );
    const fetchedValueFromDb =
      fetchedDbDataWithNeutralisedKeys[neutralisedFieldLabelFromTestData];

    //now compare the test data
    if (fetchedValueFromDb != eachFieldValue) {
      console.log(
        `Expected value was ${eachFieldValue} but value from db ${fetchedValueFromDb} for key ${eachFieldName}`
      );
      throw new Error(
        `User custom fields data from DB did not match the given test data for ${eachFieldName}`
      );
    }
  }
  return true;
}

export function neutraliseTheGivenFieldName(fieldName: String) {
  /**
   * trim space
   * make it lower case
   * remove space between words
   * remove any `?` if at the last of the string
   */
  var regexPattern = /[^A-Za-z0-9]/g;
  let neutralisedFieldName = fieldName.replace(/ /g, "").replace(regexPattern,"");
  return neutralisedFieldName;
}

export function neutraliseEachKeyInDbTestData(customFieldsDataFromDB) {
  let dbDataWithNeutralisedCustomFieldsName = {};
  for (const [keyName, keyValue] of Object.entries(customFieldsDataFromDB)) {
    console.log(`neutralising key ${keyName}`)
    const neutralKey = neutraliseTheGivenFieldName(keyName);
    console.log(`after neutralising  key is ${keyName}`)

    dbDataWithNeutralisedCustomFieldsName[neutralKey] = keyValue;
  }
  return dbDataWithNeutralisedCustomFieldsName;
}

export function filterOnlyCustomFieldsFromTestData(userRegistrationTestData) {
  const listOfDefaultFields = [
    "email",
    "firstName",
    "lastName",
    "headline",
    "phoneNumber",
    "company",
    "designation",
    "bio",
    "country",
  ];
  const customFieldOnlyTestData = {};
  for (const [key, value] of Object.entries(userRegistrationTestData)) {
    console.log(key);
    if (listOfDefaultFields.includes(key)) {
      continue;
    } else {
      customFieldOnlyTestData[key] = value;
    }
  }
  console.log(customFieldOnlyTestData);
  return customFieldOnlyTestData;
}
