const fs = require('fs');
const ical = require('node-ical');
const {writeFile } = require("fs").promises;

export const convertInAnotherTimeZone = (date, timeZone) => {
  return new Date(
    new Date(date).toLocaleString('en-US', {
      timeZone: timeZone,
    }),
  );
};



export const changeTimeZone = (date, timeZone) => {
  const options = {
    weekday: "short",
    day: "numeric",
    month: "short",
    year: "numeric",
    timeZone: timeZone,
  };
  if (typeof date === 'string') {
    return new Date(
      new Date(date).toLocaleString('en-US', options),
    );
  }

  return new Date(
    date.toLocaleString('en-US', options),
  );
}


export const checkIfFileExists = (filePath) => {
  const fs = require('fs')
  try {
    if (fs.existsSync(filePath)) {
      return true
    }
  } catch (err) {
    console.error(err)
    return false
  }
}
// Converts a date into format Sunday, 1st December
export const convertDateinWords = (date, timeZone) => {
  const weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  const month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  const d = new Date(convertInAnotherTimeZone(date, timeZone));
  const nth = function (d) {
    if (d > 3 && d < 21) return 'th';
    switch (d % 10) {
      case 1: return "st";
      case 2: return "nd";
      case 3: return "rd";
      default: return "th";
    }
  }
  let expectedStartDate = weekday[d.getDay()] + ", " + d.getDate() + nth(d.getDate()) + " " + month[d.getMonth()];
  return expectedStartDate;
}

// Function to delete file given the file path
export const deleteFile = (filePath) => {
  if (checkIfFileExists(filePath)) {
    fs.unlinkSync(filePath);
  }
  else {
    console.log(`${filePath} does not exist.`)
  }
}

// Function to parse ICS using node-ical given the File path
export const parseICS = (filePath) => {
  const icalFile = ical.sync.parseFile(filePath);
  return icalFile
}

export async function readCsvFileAsJson(filePath){
  const csv=require('csvtojson')
  const jsonArray=await csv().fromFile(filePath);
  return jsonArray
}
export async function readCsvStringAsJson(filePath){
  const csv=require('csvtojson')
  const jsonArray=await csv().fromString(filePath);
  return jsonArray
}
export async function writeCSV(fileName, data) {
  await writeFile(fileName, data, "utf8");
}
//function to convert UTC to IST hours
export async function formatdateString(dateString) {
  let date = new Date(dateString);
  date.setHours(date.getHours() + 6);
  return date.toISOString().slice(0, -5) + "+05:30";
}

export async function convertStringToNumber(data){
  return parseFloat(data.replace(/[^\d.-]/g, '')); 
}
export async function setTimeOut(timeout){
  await new Promise((r) => setTimeout(r, timeout));
}


