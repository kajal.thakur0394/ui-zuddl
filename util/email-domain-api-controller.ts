import { APIRequestContext } from "@playwright/test";

export class EmailDomainAPIController {
  public orgApiRequestContext: APIRequestContext;

  constructor(orgApiRequestContext: APIRequestContext) {
    this.orgApiRequestContext = orgApiRequestContext;
  }

  async addNewEmailDomain({ senderEmail, replyEmail, senderName }) {
    let addDomainApiResp = await this.orgApiRequestContext.post(
      "/api/email-custom-sender",
      {
        data: {
          replyEmail: replyEmail,
          senderEmail: senderEmail,
          senderName: senderName,
        },
      }
    );
    if (addDomainApiResp.status() != 200) {
      throw new Error(
        `API to add new domain failed with ${addDomainApiResp.status()}`
      );
    }
    return addDomainApiResp;
  }

  async verifyEmailDomain(customDomainId) {
    let verifyDomainResp = await this.orgApiRequestContext.post(
      `/api/custom-domain/email/${customDomainId}/verify`
    );
    if (verifyDomainResp.status() != 200) {
      throw new Error(
        `API to verify custom domain failed with ${verifyDomainResp.status()}`
      );
    }
    return verifyDomainResp;
  }

  async deleteCustomSenderEmailDomainId(customSenderDomainId) {
    console.log(`deleting sender domain id  : ${customSenderDomainId}`);
    let deleteCustomSenderDomainResp = await this.orgApiRequestContext.delete(
      `/api/email-custom-sender/${customSenderDomainId}`
    );
    if (deleteCustomSenderDomainResp.status() != 200) {
      throw new Error(
        `API to delete custom domain failed with ${deleteCustomSenderDomainResp.status()}`
      );
    }
    return deleteCustomSenderDomainResp;
  }

  async deleteAllDomainsExceptZuddlEmail() {
    let listOfCustomSenderEmailsJson = await (
      await this.fetchListOfCustomSenderEmails()
    ).json();

    //delete all emails except for zuddl default domain id
    for (let each_custom_email_obj of listOfCustomSenderEmailsJson) {
      if (each_custom_email_obj.senderEmail != "noreply@zuddl.com") {
        await this.deleteCustomSenderEmailDomainId(
          each_custom_email_obj.customSenderId
        );
      }
    }
  }

  async getSendersDomainIdForEmail(emailToFetchSenderDomainIdFor: string) {
    console.log(
      `fetching zuddl domain sender id for email from list of domains ${emailToFetchSenderDomainIdFor}`
    );

    let senderEmailDomainId;
    let listOfCustomSenderEmailsJson = await (
      await this.fetchListOfCustomSenderEmails()
    ).json();
    // if no domains are found then raise an exception
    if (listOfCustomSenderEmailsJson.length == 0) {
      throw new Error(`empty list returned in list of custom sender emails`);
    }
    //fetch
    console.log(
      `iterating each object and fetching the custom domain id with matchin email`
    );
    for (let each_custom_email_obj of listOfCustomSenderEmailsJson) {
      let senderDomainEmail = each_custom_email_obj.senderEmail;
      if (
        senderDomainEmail.toLowerCase() ===
        emailToFetchSenderDomainIdFor.toLowerCase()
      ) {
        senderEmailDomainId = each_custom_email_obj.customSenderId;
        console.log(
          `sender id found for given ${emailToFetchSenderDomainIdFor} which is ${senderEmailDomainId}`
        );
        return senderEmailDomainId;
      }
    }
    if (senderEmailDomainId == undefined) {
      throw new Error(
        `Email ${emailToFetchSenderDomainIdFor} not found in the list of getCustomDomainApi `
      );
    }
    return senderEmailDomainId;
  }
  async fetchListOfCustomSenderEmails() {
    let getCustomEmailDominListResp = await this.orgApiRequestContext.get(
      "/api/email-custom-sender/"
    );
    if (getCustomEmailDominListResp.status() != 200) {
      throw new Error(
        `API to fetch list of custom sender email domains failed with ${getCustomEmailDominListResp.status()}`
      );
    }
    return getCustomEmailDominListResp;
  }

  async updateSenderEmailDomainForNotificationType(
    eventId,
    customDomainId,
    notificationType
  ) {
    // get notification id by the notifcation type
    let notificationSettingId = "";
    const url = `/api/notifications/${eventId}/event_setting/${notificationSettingId}/email_sender/?customSenderId=${customDomainId}`;
    let updateSenderEmailDomainResp = await this.orgApiRequestContext.patch(
      url
    );
    if (updateSenderEmailDomainResp.status() != 200) {
      throw new Error(`API call to update sender email id for notifcation failed with 
        ${updateSenderEmailDomainResp.status()}`);
    }
    return updateSenderEmailDomainResp;
  }

  async updateSenderNameForNotificationType(
    eventId,
    sendersName: string,
    notificationType
  ) {
    // get notification id by the notifcation type
    let notificationSettingId = "";
    const url = `/api/notifications/${eventId}/event_setting/${notificationSettingId}/sender_name/?sender_name=${sendersName}`;
    let updateSendersNameResp = await this.orgApiRequestContext.patch(url);
    if (updateSendersNameResp.status() != 200) {
      throw new Error(`API call to update senders name for notifcation failed with 
        ${updateSendersNameResp.status()}`);
    }
    return updateSendersNameResp;
  }
}
