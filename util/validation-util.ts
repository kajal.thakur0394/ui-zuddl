import { expect } from "@playwright/test";
import axios from "axios";
import { EventInfoDTO } from "../dto/eventInfoDto";
import EventReminderHeader from "../enums/EventReminderHeaderEnum";
import { convertDateinWords } from "./commonUtil";
import {
  emailReminderTemplateValidation,
  emailTemplateValidation,
} from "./emailTemplateValidation";

import fs from "fs";
import pdfParse from "pdf-parse";

export async function validateIcsData(
  ICSdata,
  eventInfoDto: EventInfoDTO,
  isInPersonEvent: boolean = false
) {
  const event: any = Object.values(ICSdata)[0];
  let icsEventTitle = event["summary"];
  console.log("icsEventTitle: ", icsEventTitle);
  expect(icsEventTitle, "Validating ics Event Title").toContain(
    eventInfoDto.eventTitle
  );

  // if start and end date are different then skip hours and minutes
  let icsStartTime = new Date(event.start).toISOString();
  // let icsEndTime = new Date(event.end).toISOString();
  // if (
  //   new Date(event.start).toDateString() !== new Date(event.end).toDateString()
  // ) {
  //   icsStartTime = new Date(event.start).toDateString();
  //   icsEndTime = new Date(event.end).toDateString();
  //   expect(icsStartTime, "Validating event Start time in ICS file").toContain(
  //     new Date(eventInfoDto.startDateTime).toDateString()
  //   );
  // } else {
  //   expect(icsStartTime, "Validating event Start time in ICS file").toContain(
  //     new Date(
  //       new Date(eventInfoDto.startDateTime).setMilliseconds(0)
  //     ).toISOString()
  //   );
  // }
  expect(icsStartTime, "Validating event Start time in ICS file").toContain(
    new Date(
      new Date(eventInfoDto.startDateTime).setMilliseconds(0)
    ).toISOString()
  );

  if (!isInPersonEvent) {
    console.log("event URL: ", event.url);
    let icsEventUrl = event.url;
    expect(icsEventUrl, "Validating event URL").toContain(eventInfoDto.eventId);
    let icsEventLocation = event.location;
    expect(
      icsEventLocation,
      "Validating event location to contain Event URL as virtual event"
    ).toBe(icsEventUrl);
  }
}

export async function validateFutureIcsData(
  ICSdata,
  eventInfoDto: EventInfoDTO
) {
  const event: any = Object.values(ICSdata)[0];
  console.log("event", event);
  console.log(ICSdata);
  // let icsEventTitle = event["summary"]
  // console.log("icsEventTitle: ", icsEventTitle)
  // await expect(icsEventTitle, "Validating ics Event Title").toContain(await eventInfoDto.eventTitle)
  let icsStartTime = new Date(event.start).toISOString();
  expect(icsStartTime, "Validating event Start time in ICS file").toContain(
    new Date(
      new Date(eventInfoDto.startDateTime).setMilliseconds(0)
    ).toISOString()
  );
  console.log("event URL: ", event.url);
  let icsEventUrl = event.url;
  expect(icsEventUrl, "Validating event URL").toContain(eventInfoDto.eventId);
  let icsEventLocation = event.location;
  expect(
    icsEventLocation,
    "Validating event location to contain Event URL as virtual event"
  ).toContain(icsEventUrl);
}

export async function validateDudaFutureIcsData(
  ICSdata,
  eventInfoDto: EventInfoDTO,
  dudaPublishedAttendeeLandingPage: string
) {
  const event: any = Object.values(ICSdata)[0];
  console.log("event", event);
  console.log(ICSdata);
  let icsEventTitle = event["summary"];
  console.log("icsEventTitle: ", icsEventTitle);
  expect(icsEventTitle, "Validating ics Event Title").toContain(
    eventInfoDto.eventTitle
  );
  let icsStartTime = new Date(event.start).toISOString();
  expect(icsStartTime, "Validating event Start time in ICS file").toContain(
    new Date(
      new Date(eventInfoDto.startDateTime).setMilliseconds(0)
    ).toISOString()
  );
  console.log("event URL: ", event.url);
  let icsEventUrl = event.url;
  expect(icsEventUrl, "Validating event URL").toContain(
    dudaPublishedAttendeeLandingPage
  );
  // Location to contain duda url
  let icsEventLocation = event.location + "/";
  expect(
    icsEventLocation,
    "Validating event location to contain Event URL as virtual event"
  ).toContain(dudaPublishedAttendeeLandingPage);
}

export async function fetchMagicLinkFromSendGridurl(
  sendGridUrl: string
): Promise<string> {
  let magicLink: string;
  let sendGridResp = await axios.get(sendGridUrl);

  if (sendGridResp.status === 200) {
    magicLink = sendGridResp.request.res.responseUrl;
  } else {
    throw new Error(
      `Send grid url did not give 302 redirect response for url ${sendGridUrl}`
    );
  }
  return magicLink;
}
export async function validateMagicLink(magicLink: string, eventInfoDto) {
  if (magicLink.includes("click")) {
    magicLink = await fetchMagicLinkFromSendGridurl(magicLink);
  }
  expect(magicLink, "Verify the magic link  is having same event ID").toContain(
    eventInfoDto.eventId
  );
  expect(magicLink, "Verify the magic link  is having mt tag").toContain("mt");
  expect(magicLink, "Verify the magic link  is having re tag").toContain("re");
}

export async function validateCalenderLink(calenderLink: string, eventId) {
  calenderLink = calenderLink.includes("click")
    ? await fetchMagicLinkFromSendGridurl(calenderLink)
    : calenderLink;
  expect(
    calenderLink,
    "Verify the calender link  is having same event ID"
  ).toContain(eventId);
  expect(calenderLink, "Verify the calender link  is having mt tag").toContain(
    "mt"
  );
  expect(calenderLink, "Verify the calender link  is having re tag").toContain(
    "re"
  );
  expect(
    calenderLink,
    "Verify the calender link  is having calender tag"
  ).toContain("calendar");
}

export async function validateAddToCalenderLink(calenderLink, eventId) {
  expect(
    calenderLink,
    "Verify the calender link  is having same event ID"
  ).toContain(eventId);
  expect(calenderLink, "Verify the calender link  is having re tag").toContain(
    "r"
  );
  expect(
    calenderLink,
    "Verify the calender link  is having calender tag"
  ).toContain("calendar");
}

export async function validateEventTitle(
  emailInvite: emailReminderTemplateValidation | emailTemplateValidation,
  eventInfoDto: EventInfoDTO
) {
  let eventTitle = await emailInvite.fetchEventTitle();
  console.log("Event title recieved", eventTitle);
  expect(eventTitle, "Verifying Event title").toContain(
    eventInfoDto.eventTitle
  );
}

export async function validateEventHeader(
  emailInvite: emailReminderTemplateValidation,
  eventReminderHeader: EventReminderHeader
) {
  let eventHeader = await emailInvite.fetchReminderHeader();
  expect(eventHeader, "Verifying Event title").toContain(eventReminderHeader);
}

export async function validateEventStartDate(
  emailInvite: emailReminderTemplateValidation | emailTemplateValidation,
  eventInfoDto: EventInfoDTO
) {
  let eventStartDate = await emailInvite.fetchEventstartDate();
  console.log("Event start date recieved:", eventStartDate);
  // Expected Start date in organizer Time Zone
  let expectedStartDate = convertDateinWords(
    eventInfoDto.startDateTime,
    eventInfoDto.timeZone
  );
  console.log("Expected Start Date:", expectedStartDate);
  expect(eventStartDate, "Verifying Event title").toContain(expectedStartDate);
}

export async function validateEventStartTime(
  emailInvite: emailReminderTemplateValidation | emailTemplateValidation,
  eventInfoDto: EventInfoDTO
) {
  let eventStartTime = await emailInvite.fetchEventstartTime();
  // Expected Start time in organizer Time Zone
  console.log(eventInfoDto.timeZone);
  let expectedStartTime = new Date(eventInfoDto.startDateTime).toLocaleString(
    "en-US",
    { hour: "2-digit", minute: "2-digit", timeZone: eventInfoDto.timeZone }
  );

  let expectedHourString = expectedStartTime.split(":")[0];
  let expectedMinString = expectedStartTime.split(":")[1];
  expectedMinString = expectedMinString.substring(0, 2);
  let amOrPm = expectedStartTime.includes("AM") ? "AM" : "PM";

  expectedStartTime = `${expectedHourString}:${expectedMinString} ${amOrPm}`;

  let doesEmailContainExpectedEventStartTime = true;
  eventStartTime = eventStartTime.trim();
  expectedStartTime = expectedStartTime.trim();
  console.log(`after trim`);
  console.log(`Event start time recieved: ${eventStartTime}`);
  console.log(`ExpectedStartTime: ${expectedStartTime}`);
  if (!eventStartTime.includes(expectedStartTime)) {
    console.log(
      `expected start time : ${expectedStartTime} is not found inside ${eventStartTime}}`
    );
    doesEmailContainExpectedEventStartTime = false;
  }
  expect(doesEmailContainExpectedEventStartTime).toBeTruthy();
}

export async function validateTimeZoneCode(
  emailInvite: emailReminderTemplateValidation | emailTemplateValidation,
  timeZoneCode: string
) {
  expect(emailInvite.htmlBody, "Verifying Timezone code").toContain(
    timeZoneCode
  );
}

// Validate email heaeder of thank you for coming reminder
export async function validateEmailHeaderOfThankYouForComing(
  emailInvite: emailReminderTemplateValidation,
  eventInfoDto: EventInfoDTO
) {
  let emailHeader = await emailInvite.fetchReminderHeaderForThanksforComing();
  console.log(`fetched email header text is ${emailHeader}`);
  expect(
    emailHeader,
    "The header should contain 'It was great to see you."
  ).toContain(
    `${EventReminderHeader.EmailThankYouForComing} at ${eventInfoDto.eventTitle}`
  );
}
export async function validateEmailReminder(
  fetched_email: emailReminderTemplateValidation,
  eventInfoDto: EventInfoDTO,
  eventReminderHeader: EventReminderHeader,
  timezoneCode: string = "(IST)"
) {
  // Validating Event Header
  await validateEventHeader(fetched_email, eventReminderHeader);
  // Validating Go to event button contains magic link tags
  let gotoButtonText, GoToButtonLink;
  let gotoButtonContents = await fetched_email.getGoToButtonlink();
  gotoButtonText = gotoButtonContents[0];
  GoToButtonLink = gotoButtonContents[1];
  expect(gotoButtonText, "Verifying text of goTo button").toContain(
    "Go to Event"
  );
  await validateMagicLink(GoToButtonLink, eventInfoDto);
  // Validating Event title
  await validateEventTitle(fetched_email, eventInfoDto);
  // Verifying start date and time of event in Email
  await validateEventStartDate(fetched_email, eventInfoDto);
  // Verifying start date and time of event in Email
  await validateEventStartTime(fetched_email, eventInfoDto);
  // Verifying timezone code in email
  await validateTimeZoneCode(fetched_email, timezoneCode);
}

export async function validateFieldData(
  fetchedProfileDataJson,
  fieldLabel: string,
  fieldType: string,
  dataToEnter: any
) {
  const customFields = JSON.parse(await fetchedProfileDataJson["customField"]);
  var regexPattern = /[^A-Za-z0-9]/g;
  let values =
    customFields[fieldLabel.replace(/ /g, "").replace(regexPattern, "")];
  console.log("Data to validate", dataToEnter, " values", values);
  if (values == undefined) {
    throw new Error(
      `Not able to fetch the data from json for validation of ${fieldLabel} edit profile field data.`
    );
  }
  if (fieldType == "multiselect-dropdown") {
    for (let eachValue in values) {
      console.log(typeof values[eachValue], values[eachValue]);
      let value = values[eachValue]["value"];
      console.log("Multiselect value", value);
      expect(dataToEnter[eachValue]).toBe(value);
    }
  } else {
    if (typeof values == "object") {
      values = values["value"];
    }
    // console.log("Values type: ",typeof(values))
    expect(values).toContain(dataToEnter);
  }
}

export async function validateFieldDataFromDB(
  fetchedProfileDataJson,
  fieldLabel: string,
  fieldType: string,
  dataToEnter: any
) {
  const customFields = fetchedProfileDataJson;
  var regexPattern = /[^A-Za-z0-9]/g;
  let filedLabelName = fieldLabel.replace(/ /g, "").replace(regexPattern, "");
  if (filedLabelName == "Title") {
    filedLabelName = "designation";
  }
  if (filedLabelName == "Phonenumber") {
    console.log("Changing phone number field label");
    filedLabelName = "phoneNumber";
  }
  let values = await customFields[filedLabelName];

  console.log(
    "FieldLabelName to validate:",
    filedLabelName,
    "Data to validate",
    dataToEnter,
    " values",
    values
  );
  if (values == undefined) {
    throw new Error(
      `Not able to fetch the data from json for validation of ${fieldLabel} edit profile field data.`
    );
  }
  if (fieldType == "multiselect-dropdown") {
    for (let eachValue in values) {
      console.log(typeof values[eachValue], values[eachValue]);
      let value = values[eachValue]["value"];
      console.log("Multiselect value", value);
      expect(dataToEnter[eachValue]).toBe(value);
    }
  } else {
    if (typeof values == "object") {
      values = values["value"];
    }
    // console.log("Values type: ",typeof(values))
    expect(values).toContain(dataToEnter);
  }
}

export async function validatePreDefinedFieldDataFromDB(
  fetchedProfileDataJson,
  fieldLabel: string,
  fieldType: string,
  dataToEnter: any
) {
  const customFields = JSON.parse(fetchedProfileDataJson);
  console.log(typeof customFields);
  var regexPattern = /[^A-Za-z0-9_]/g;
  let fieldLabelName = fieldLabel
    .replace(/ /g, "_")
    .toLowerCase()
    .replace(regexPattern, "");
  if (fieldLabelName == "title") {
    fieldLabelName = "designation";
  }
  if (fieldLabelName == "Phonenumber") {
    console.log("Changing phone number");
    fieldLabelName = "phoneNumber";
  }
  let values = customFields[fieldLabelName];
  console.log("Fieldlabelname:", fieldLabelName);
  console.log("Data to validate", dataToEnter, " values", values);
  if (values == undefined) {
    throw new Error(
      `Not able to fetch the data from json for validation of ${fieldLabel} edit profile field data.`
    );
  }
  if (fieldType == "multiselect-dropdown") {
    for (let eachValue in values) {
      console.log(typeof values[eachValue], values[eachValue]);
      let value = values[eachValue]["value"];
      console.log("Multiselect value", value);
      expect(dataToEnter[eachValue]).toBe(value);
    }
  } else {
    if (typeof values == "object") {
      values = values["value"];
    }
    // console.log("Values type: ",typeof(values))
    expect(values).toContain(dataToEnter);
  }
}

export async function validateEditProfileConditionalFieldDataFromDB(
  fetchedProfileDataJson: JSON | any,
  userRegistrationFormObj,
  options?: {
    checkMandatoryFields?: boolean | true;
    checkOptionalFields?: boolean | true;
    fieldsToSkip?: string[];
  }
) {
  console.log("fetchedProfileDataJson:", fetchedProfileDataJson);
  // for each reg form field, we will iterate and fill up the form
  for (let eachField in userRegistrationFormObj) {
    console.log(`handling the field ${eachField}`);
    let filedObj = userRegistrationFormObj[eachField]; //will get the object
    let fieldLabel = filedObj["label"];
    let fieldType = filedObj["fieldType"];
    let fieldValueToEnter = filedObj["dataToEnter"];
    let isFieldOptional = filedObj["isOptionalField"];
    let isFieldConditional = filedObj["isConditionaField"];
    // if condition is given to fill only mandatory fields
    console.log(
      `handling field ${fieldLabel} of type ${fieldType} with value ${fieldValueToEnter}`
    );
    if (options) {
      if (options.fieldsToSkip?.includes(fieldLabel)) {
        console.log("Field Label excluded:", fieldLabel);
        continue;
      }
      if (options?.checkMandatoryFields && !options.checkOptionalFields) {
        if (isFieldOptional) {
          console.log(`field is optional hence skipping it`);
          continue;
        } else {
          await validateFieldDataFromDB(
            fetchedProfileDataJson,
            fieldLabel,
            fieldType,
            fieldValueToEnter
          );
        }
      } else if (
        !options?.checkMandatoryFields &&
        options?.checkOptionalFields
      ) {
        if (!isFieldOptional) {
          console.log(`field is mandatory hence skipping it`);
          continue;
        } else {
          await validateFieldDataFromDB(
            fetchedProfileDataJson,
            fieldLabel,
            fieldType,
            fieldValueToEnter
          );
        }
      } else if (options?.checkMandatoryFields && options.checkOptionalFields) {
        console.log(`checking up the field`);
        await validateFieldDataFromDB(
          fetchedProfileDataJson,
          fieldLabel,
          fieldType,
          fieldValueToEnter
        );
      }
    } else {
      await validateFieldDataFromDB(
        fetchedProfileDataJson,
        fieldLabel,
        fieldType,
        fieldValueToEnter
      );
    }
  }
  return true;
}

export async function validatePreDefinedfieldsFromDB(
  fetchedProfileDataJson: JSON | any,
  userRegistrationFormObj,
  options?: {
    checkMandatoryFields?: boolean | true;
    checkOptionalFields?: boolean | true;
    fieldsToSkip?: string[];
  }
) {
  console.log("fetchedProfileDataJson:", fetchedProfileDataJson);
  // for each reg form field, we will iterate and fill up the form
  for (let eachField in userRegistrationFormObj) {
    console.log(`handling the field ${eachField}`);
    let filedObj = userRegistrationFormObj[eachField]; //will get the object
    let fieldLabel = filedObj["label"];
    let fieldType = filedObj["fieldType"];
    let fieldValueToEnter = filedObj["dataToEnter"];
    let isFieldOptional = filedObj["isOptionalField"];
    let isFieldConditional = filedObj["isConditionaField"];
    // if condition is given to fill only mandatory fields
    console.log(
      `handling field ${fieldLabel} of type ${fieldType} with value ${fieldValueToEnter}`
    );
    if (options) {
      if (options.fieldsToSkip?.includes(fieldLabel)) {
        console.log("Field Label excluded:", fieldLabel);
        continue;
      }
      if (options?.checkMandatoryFields && !options.checkOptionalFields) {
        if (isFieldOptional) {
          console.log(`field is optional hence skipping it`);
          continue;
        } else {
          await validatePreDefinedFieldDataFromDB(
            fetchedProfileDataJson,
            fieldLabel,
            fieldType,
            fieldValueToEnter
          );
        }
      } else if (
        !options?.checkMandatoryFields &&
        options?.checkOptionalFields
      ) {
        if (!isFieldOptional) {
          console.log(`field is mandatory hence skipping it`);
          continue;
        } else {
          await validatePreDefinedFieldDataFromDB(
            fetchedProfileDataJson,
            fieldLabel,
            fieldType,
            fieldValueToEnter
          );
        }
      } else if (options?.checkMandatoryFields && options.checkOptionalFields) {
        console.log(`checking up the field`);
        await validatePreDefinedFieldDataFromDB(
          fetchedProfileDataJson,
          fieldLabel,
          fieldType,
          fieldValueToEnter
        );
      }
    } else {
      await validatePreDefinedFieldDataFromDB(
        fetchedProfileDataJson,
        fieldLabel,
        fieldType,
        fieldValueToEnter
      );
    }
  }
  return true;
}

export async function validateEditProfileConditionalFieldDataFromEventRoleApi(
  fetchedProfileDataJson: JSON,
  userRegistrationFormObj,
  options?: {
    checkMandatoryFields?: boolean | true;
    checkOptionalFields?: boolean | true;
    fieldsToSkip?: string[];
  }
) {
  // for each reg form field, we will iterate and fill up the form
  for (let eachField in userRegistrationFormObj) {
    console.log(`handling the field ${eachField}`);
    let filedObj = userRegistrationFormObj[eachField]; //will get the object
    let fieldLabel = filedObj["label"];
    let fieldType = filedObj["fieldType"];
    let fieldValueToEnter = filedObj["dataToEnter"];
    let isFieldOptional = filedObj["isOptionalField"];
    let isFieldConditional = filedObj["isConditionaField"];
    // if condition is given to fill only mandatory fields
    console.log(
      `handling field ${fieldLabel} of type ${fieldType} with value ${fieldValueToEnter}`
    );
    if (options) {
      if (options.fieldsToSkip?.includes(fieldLabel)) {
        console.log("Field Label excluded:", fieldLabel);
        continue;
      }
      if (options?.checkMandatoryFields && !options.checkOptionalFields) {
        if (isFieldOptional) {
          console.log(`field is optional hence skipping it`);
          continue;
        } else {
          await validateFieldData(
            fetchedProfileDataJson,
            fieldLabel,
            fieldType,
            fieldValueToEnter
          );
        }
      } else if (
        !options?.checkMandatoryFields &&
        options?.checkOptionalFields
      ) {
        if (!isFieldOptional) {
          console.log(`field is mandatory hence skipping it`);
          continue;
        } else {
          await validateFieldData(
            fetchedProfileDataJson,
            fieldLabel,
            fieldType,
            fieldValueToEnter
          );
        }
      } else if (options?.checkMandatoryFields && options.checkOptionalFields) {
        console.log(`checking up the field`);
        await validateFieldData(
          fetchedProfileDataJson,
          fieldLabel,
          fieldType,
          fieldValueToEnter
        );
      }
    } else {
      await validateFieldData(
        fetchedProfileDataJson,
        fieldLabel,
        fieldType,
        fieldValueToEnter
      );
    }
  }
}

export async function validateTicketPdfContent(
  ticketPdfPath: string,
  expectedTicketDetails: {
    ticketCode: string;
    startTime: string;
    eventTitle: string;
    ticketPrice: string;
    timeZone: string;
  }
) {
  let dataBuffer = fs.readFileSync(ticketPdfPath);
  let data = await pdfParse(dataBuffer);
  console.log(data.text);
  console.log("=====================================");
  let ticketDetails = data.text;
  // format = Wed, Mar 20
  // remove bracket from timezone
  let timezone = expectedTicketDetails.timeZone
    .replace("(", "")
    .replace(")", "");

  let startDate = new Date(expectedTicketDetails.startTime).toLocaleDateString(
    "en-US",
    { weekday: "short", month: "short", day: "numeric", timeZone: timezone }
  );
  // let startTime; // format 01:57 PM • IST
  let startTime = new Date(expectedTicketDetails.startTime).toLocaleTimeString(
    "en-US",
    { hour: "2-digit", minute: "2-digit", timeZone: timezone }
  );

  let toBeValidated = {
    ticketCode: expectedTicketDetails.ticketCode,
    startTime: `${startTime}`,
    eventTitle: expectedTicketDetails.eventTitle,
    startDate: startDate,
    ticketPrice: expectedTicketDetails.ticketPrice,
  };

  for (let eachKey in toBeValidated) {
    expect(ticketDetails, `Validating ${eachKey}`).toContain(
      toBeValidated[eachKey]
    );
  }
}
