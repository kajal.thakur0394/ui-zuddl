import {
  chromium,
  BrowserContext,
  Browser,
  LaunchOptions,
  test,
} from "@playwright/test";
import cookieData from "../org_cookies.json";
import BrowserName from "../enums/BrowserEnum";
import { DataUtl } from "./dataUtil";

class BrowserFactory {
  private static chromiumInstance: Browser | null = null;
  private static chromeInstance: Browser | null = null;

  private constructor() {
    // Private constructor to prevent external instantiation
  }

  public static async getChromeBrowserInstance(): Promise<Browser> {
    // define launch options based on av flag is enabled
    const chromeLaunchOptions: LaunchOptions = {
      channel: "chrome",
      // Add any necessary launch options here
      args: [
        "--use-fake-ui-for-media-stream",
        "--use-fake-device-for-media-stream",
        "--disable-dev-shm-usage",
        "--disable-software-rasterizer",
        "--disable-gpu",
        "--auto-select-desktop-capture-source=Entire screen",
        "--enable-usermedia-screen-capturing",
      ],
    };
    await test.step(`Loading chrome browser instance`, async () => {
      await test.step(`Checking if chrome static instance is null`, async () => {
        if (!BrowserFactory.chromeInstance) {
          await test.step(`static chrome instance for this class found not defined as setting this now`, async () => {
            BrowserFactory.chromeInstance = await chromium.launch(
              chromeLaunchOptions
            );
          });
        }
      });
    });

    return BrowserFactory.chromeInstance;
  }

  public static async getChromiumBrowserInstance(): Promise<Browser> {
    // define launch options based on av flag is enabled
    const chromiumLaunchOptions: LaunchOptions = {
      // Add any necessary launch options here
      args: [
        "--use-fake-ui-for-media-stream",
        "--use-fake-device-for-media-stream",
        "--disable-dev-shm-usage",
        "--disable-software-rasterizer",
        "--disable-gpu",
        "--auto-select-desktop-capture-source=Entire screen",
        "--enable-usermedia-screen-capturing",
      ],
    };
    await test.step(`Loading chromium browser instance`, async () => {
      await test.step(`Checking if chromium static instance is null or undefined`, async () => {
        if (!BrowserFactory.chromiumInstance) {
          await test.step(`static chromium instance for this class found not defined so,  setting this now`, async () => {
            BrowserFactory.chromiumInstance = await chromium.launch(
              chromiumLaunchOptions
            );
          });
        }
      });
    });

    return BrowserFactory.chromiumInstance;
  }

  public static async getBrowserContext({
    browserName = BrowserName.CHROMIUM,
    grantCameraAndMicPermission = true,
    laodOrganiserCookies = true,
    timeZoneId = "Asia/Kolkata",
  }: BrowserContextParameters) {
    let browserInstance: Browser;
    let browserContext: BrowserContext;
    await test.step(`Loading browser context for browser ${browserName}`, async () => {
      if (browserName === BrowserName.CHROME) {
        browserInstance = await BrowserFactory.getChromeBrowserInstance();
      } else if (browserName === BrowserName.CHROMIUM) {
        browserInstance = await BrowserFactory.getChromiumBrowserInstance();
      }
    });
    //check if we need to load organiser cookies
    await test.step(`Checking we need to load org cookies`, async () => {
      await test.step(`Loading org cookies as per given flag`, async () => {
        if (laodOrganiserCookies) {
          console.log(`Loading organiser cookies`);
          browserContext = await browserInstance.newContext({
            storageState: await BrowserFactory.getOrgCookies(),
            timezoneId: timeZoneId,
          });
        } else {
          console.log(`not loading organiser cookies`);
          browserContext = await browserInstance.newContext({
            timezoneId: timeZoneId,
          });
        }
      });
    });

    await test.step(`Checking if we need to grant camera and microphone permission`, async () => {
      //check if we need to grant permission
      if (grantCameraAndMicPermission) {
        await test.step(`Giving permission of camera and microphone to this context`, async () => {
          await browserContext.grantPermissions(["camera", "microphone"]);
        });
      }
    });

    //load cookies
    return browserContext;
  }

  public static async getOrgCookies() {
    let orgCookies;
    await test.step(`Loading organiser cookies as per the run env`, async () => {
      const run_env = process.env.run_env;
      await test.step(`loading cookie data for env ${run_env}`, async () => {
        if (run_env === "staging") {
          orgCookies = cookieData["staging"]["organiser"];
        } else if (run_env == "pre-prod") {
          orgCookies = cookieData["pre-prod"]["organiser"];
        } else {
          console.log(`No cookies found for the given env`);
          console.log(`Checking local cookies`);
          orgCookies = require(`../${run_env}.json`);
        }
      });
    });

    await test.step(`Checking before returning the cookies, that these are not null`, async () => {
      if (orgCookies == null || orgCookies == undefined) {
        throw new Error(`Organiser authentication cookies could not be found`);
      }
    });
    return orgCookies;
  }

  public static async closeBrowserContext(browser: BrowserContext | Browser) {
    await test.step(`Trying to close the given browser context`, async () => {
      try {
        await browser?.close();
      } catch (error) {
        await test.step(`Exception occured while closing context, handling it by not doing anything`, async () => {
          console.log(`inside catch -->${error.message}`);
        });
      }
    });
  }
}

export default BrowserFactory;

interface BrowserContextParameters {
  browserName?: BrowserName;
  grantCameraAndMicPermission?: boolean;
  laodOrganiserCookies?: boolean;
  timeZoneId?: string;
}
