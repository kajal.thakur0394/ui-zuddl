import { expect } from "@playwright/test";
import { DataUtl } from "./dataUtil";

export class emailTemplateValidation {
  htmlBody: string;
  fetched_email;

  constructor(fetched_email) {
    this.fetched_email = fetched_email;
    this.htmlBody = this.fetched_email.html.body;
  }

  async fetchAttendeeName(): Promise<string> {
    let attendeeName;
    let htmlBody = this.htmlBody;
    let bodyTexts;
    let bodyTextsLength;
    try {
      if (htmlBody.includes("<h1>")) {
        console.log("htmlBody includes h1");
        bodyTexts = htmlBody.split("</h1>")[0].split(">");
      } else if (htmlBody.includes("Hi&nbsp;")) {
        console.log("htmlBody includes Hi&nbsp;");
        bodyTexts = [htmlBody.split("Hi&nbsp;")[1].split("<br")[0]];
      } else {
        console.log("htmlBody includes p");
        bodyTexts = htmlBody.split("</p>")[0].split("Hi ");
      }
      bodyTextsLength = bodyTexts.length;
      console.log(bodyTextsLength);
      console.log("bodyTexts is " + bodyTexts);
      bodyTextsLength = bodyTexts.length;
      attendeeName = bodyTexts[bodyTextsLength - 1];
      return attendeeName;
    } catch (e) {
      console.error(
        "Exception occured while retrieving Attendee Name " + e.message
      );
      throw e;
    } finally {
      return attendeeName;
    }
  }
  async fetchTicketCode(): Promise<string> {
    let ticketCode;
    let htmlBody = this.htmlBody;
    let bodyTexts;
    let bodyTextsLength;
    try {
      bodyTexts = htmlBody.split("</p>")[0].split(">");
      bodyTextsLength = bodyTexts.length;
      ticketCode = bodyTexts[bodyTextsLength - 1];
      ticketCode = ticketCode.replace(/\s/g, "");
      // remove &nbsp; from the ticket code
      ticketCode = ticketCode.replace(/&nbsp;/g, "");
    } catch (e) {
      console.error(
        "Exception occured while retrieving Attendee Name " + e.message
      );
      throw e;
    } finally {
      return ticketCode;
    }
  }

  async verifyTicketCode(ticketCode: string) {
    let expectedImgUrl = `${
      DataUtl.getApplicationTestDataObj()["baseUrl"]
    }/api/ticketType/create-qr-code?size&#x3D;120&amp;data&#x3D;${ticketCode}`;
    let expectedImgTag = `<img
                                        src="${expectedImgUrl}"`;
    console.log("expectedImgTag is " + expectedImgUrl);
    console.log("htmlBody is " + this.htmlBody);
    expect(this.htmlBody.includes(expectedImgUrl)).toBeTruthy();
    expect(this.htmlBody.includes(expectedImgTag)).toBeTruthy();
  }

  async fetchAttendeeNameFlow(): Promise<string> {
    let attendeeName;
    let htmlBody = this.htmlBody;
    let bodyTexts;
    let bodyTextsLength;
    try {
      // attendee name is sandwiched between Hi&nbsp; and <br
      bodyTexts = htmlBody.split("Hi&nbsp;")[1].split("<br");
      attendeeName = bodyTexts[0].trim();
      console.log("Attendee Name is " + attendeeName);
      return attendeeName;
    } catch (e) {
      console.error(
        "Exception occured while retrieving Attendee Name " + e.message
      );
      throw e;
    } finally {
      return attendeeName;
    }
  }

  async fetchEventTitle(): Promise<string> {
    let htmlBody = this.htmlBody;
    let fetched_email_obj_id;
    let eventTitle;
    let bodyTexts;
    let bodyTextsLength;
    try {
      bodyTexts = htmlBody.split("</b>")[0].split(">");
      bodyTextsLength = bodyTexts.length;
      eventTitle = bodyTexts[bodyTextsLength - 1];
      console.log(`Event title -> ${eventTitle}`);
      return eventTitle;
    } catch (e) {
      console.error(
        "Exception occured while retrieving event Title " + e.message
      );
      throw e;
    } finally {
      return eventTitle;
    }
  }

  async fetchEventstartDate(): Promise<string> {
    let fetched_email_obj_id;
    let htmlBody = this.htmlBody;
    let eventStartTime;
    let bodyTexts;
    let bodyTextsLength;
    try {
      bodyTexts = htmlBody.split("</div>")[0].split("<div>");
      bodyTextsLength = bodyTexts.length;
      eventStartTime = bodyTexts[bodyTextsLength - 1];
      return eventStartTime;
    } catch (e) {
      console.error(
        "Exception occured while retrieving email to fetch event start Date" +
          e.message
      );
      throw e;
    } finally {
      return eventStartTime;
    }
  }

  async fetchEventstartTime(): Promise<string> {
    let fetched_email_obj_id;
    let htmlBody = this.htmlBody;
    let eventStartTime;
    let bodyTexts;
    let bodyTextsLength;

    try {
      // eventStartTime is sandwiched between from&nbsp;05:40 PM&nbsp;(IST) onwards;
      if (htmlBody.includes("from&nbsp;")) {
        bodyTexts = htmlBody.split("from&nbsp;")[1].split("&nbsp;");
      } else {
        bodyTexts = htmlBody.split("from ")[1].split("(");
      }
      eventStartTime = bodyTexts[0];
      console.log(`Event start time -> ${eventStartTime}`);
      if (!eventStartTime.includes(":")) {
        bodyTexts = htmlBody.split("</div>")[4].split(">");
        bodyTextsLength = bodyTexts.length;
        eventStartTime = bodyTexts[bodyTextsLength - 1];
      }
    } catch (e) {
      console.error(
        "Exception occurred while retrieving email to fetch event start Time " +
          e.message
      );
      throw e;
    } finally {
      return eventStartTime;
    }
  }

  async getGoToButtonlink(): Promise<any> {
    let fetched_email_obj_id;
    let invite_link;
    let inviteText;

    try {
      const fetched_email = this.fetched_email;
      console.log("fetched message is " + fetched_email);
      fetched_email_obj_id = await fetched_email.id;

      //fetch the magic link from here
      let links_in_email = await fetched_email.html.links;
      console.log(
        `total links fetched from the email is ${links_in_email.length}`
      );
      invite_link = await links_in_email[4].href;
      inviteText = await links_in_email[4].text;
      console.log("email text href at link index" + 4 + " is  " + inviteText);
    } catch (e) {
      console.error(
        "Exception occured while retrieving Go to Event button link " +
          e.message
      );
    } finally {
      console.log("invite Text", inviteText);
      return [inviteText, invite_link];
    }
  }

  async fetchIcsContents(): Promise<any> {
    let fetched_email_obj_id;
    let outlookinvite_link;
    let appleinvite_link;
    let outlookinvitetext;
    let appleinvitetext;

    try {
      const fetched_email = this.fetched_email;
      console.log("fetched message is " + fetched_email);
      fetched_email_obj_id = await fetched_email.id;

      //fetch the magic link from here
      let links_in_email = await fetched_email.html.links;
      console.log(
        `total links fetched from the email is ${links_in_email.length}`
      );
      outlookinvite_link = await links_in_email[1].href;
      outlookinvitetext = await links_in_email[1].text;
      appleinvite_link = await links_in_email[3].href;
      appleinvitetext = await links_in_email[3].text;
    } catch (e) {
      console.error(
        "Exception occured while retrieving ICS contents." + e.message
      );
    } finally {
      return [
        outlookinvite_link,
        outlookinvitetext,
        appleinvite_link,
        appleinvitetext,
      ];
    }
  }

  async fetchCalenderInvites(): Promise<any> {
    let fetched_email_obj_id;
    let googleinvite_link;
    let yahooinvite_link;
    let googleinvitetext;
    let yahooinvitetext;

    try {
      const fetched_email = this.fetched_email;
      // console.log("fetched message is " + JSON.stringify(fetched_email));
      fetched_email_obj_id = await fetched_email.id;

      //fetch the magic link from here
      let links_in_email = await fetched_email.html.links;
      console.log(
        `total links fetched from the email is ${links_in_email.length}`
      );
      googleinvite_link = await links_in_email[0].href;
      googleinvitetext = await links_in_email[0].text;
      yahooinvite_link = await links_in_email[2].href;
      yahooinvitetext = await links_in_email[2].text;
    } catch (e) {
      console.error(
        "Exception occured while retrieving calender invites." + e.message
      );
    } finally {
      return [
        googleinvite_link,
        googleinvitetext,
        yahooinvite_link,
        yahooinvitetext,
      ];
    }
  }

  async fetchAllLinks(): Promise<any> {
    let fetched_email_obj_id;
    let links: Array<string> = new Array<string>();

    try {
      const fetched_email = this.fetched_email;
      fetched_email_obj_id = await fetched_email.id;

      //fetch the magic link from here
      let links_in_email = await fetched_email.html.links;

      let arrayLength = links_in_email.length;
      console.log(`Total links fetched from the email is ${arrayLength}`);

      for (var i = 0; i < arrayLength; i++) {
        links.push(links_in_email[i].href);
      }
    } catch (e) {
      console.error(
        "Exception occured while retrieving calender invites." + e.message
      );
    } finally {
      return links;
    }
  }

  async fetchGoToDashboardButtonLink(): Promise<any> {
    let fetched_email_obj_id;
    let dashboardInvite_link;
    let dashboardInvite_text;

    try {
      const fetched_email = this.fetched_email;
      console.log("fetched message is " + fetched_email);
      fetched_email_obj_id = await fetched_email.id;

      //fetch all the links from here
      let links_in_email = await fetched_email.html.links;
      console.log(
        `total links fetched from the email is ${links_in_email.length}`
      );
      dashboardInvite_link = await links_in_email[0].href;
      dashboardInvite_text = await links_in_email[0].text;
      console.log(
        "email text href at link index" + 0 + " is  " + dashboardInvite_text
      );
    } catch (e) {
      console.error(
        "Exception occured while retrieving Go to dashboard button link " +
          e.message
      );
    } finally {
      console.log("dashboard invite Text", dashboardInvite_text);
      return [dashboardInvite_text, dashboardInvite_link];
    }
  }

  async fetchEmailAttachments(): Promise<any> {
    let emailAttachments;

    try {
      const fetched_email = this.fetched_email;

      //Fetch all attachments
      emailAttachments = await fetched_email.attachments;

      return emailAttachments;
    } catch (e) {
      console.error(
        "Exception occured while retrieving attachments " + e.message
      );
    }
  }
}

export class emailReminderTemplateValidation {
  htmlBody: string;
  fetched_email;

  constructor(fetched_email) {
    this.fetched_email = fetched_email;
    this.htmlBody = fetched_email.html.body;
  }

  async fetchReminderHeader(): Promise<string> {
    let header;
    let htmlBody = this.htmlBody;
    let bodyTexts;
    let bodyTextsLength;
    try {
      bodyTexts = htmlBody.split("</p>")[0].split(">");
      bodyTextsLength = bodyTexts.length;
      header = bodyTexts[bodyTextsLength - 1];
      return header;
    } catch (e) {
      console.error(
        "Exception occured while retrieving Attendee Name " + e.message
      );
      throw e;
    } finally {
      return header;
    }
  }

  fetchReminderHeaderForThanksforComing() {
    let header;
    let htmlBody = this.htmlBody;
    let bodyTexts;
    let bodyTextsLength;
    var HTMLParser = require("node-html-parser");
    const root = HTMLParser.parse(htmlBody);
    const paragraphNode = root.querySelector("table.mainContent p");
    const textContent = paragraphNode.text;
    console.log(textContent);
    return textContent;
  }

  async fetchEventTitle(): Promise<string> {
    let htmlBody = this.htmlBody;
    let fetched_email_obj_id;
    let eventTitle;
    let bodyTexts;
    let bodyTextsLength;
    try {
      bodyTexts = htmlBody.split("</b>")[0].split(">");
      bodyTextsLength = bodyTexts.length;
      eventTitle = bodyTexts[bodyTextsLength - 1];
      return eventTitle;
    } catch (e) {
      console.error(
        "Exception occured while retrieving event Title " + e.message
      );
      throw e;
    } finally {
      return eventTitle;
    }
  }

  async fetchEventstartDate(): Promise<string> {
    let fetched_email_obj_id;
    let htmlBody = this.htmlBody;
    let eventStartTime;
    let bodyTexts;
    let bodyTextsLength;
    try {
      bodyTexts = htmlBody.split("</div>")[0].split("<div>");
      bodyTextsLength = bodyTexts.length;
      eventStartTime = bodyTexts[bodyTextsLength - 1];
      return eventStartTime;
    } catch (e) {
      console.error(
        "Exception occured while retrieving email to fetch event start Date" +
          e.message
      );
      throw e;
    } finally {
      return eventStartTime;
    }
  }

  async fetchEventstartTime(): Promise<string> {
    let fetched_email_obj_id;
    let htmlBody = this.htmlBody;
    let eventStartTime;
    let bodyTexts;
    let bodyTextsLength;

    try {
      bodyTexts = htmlBody.split("</div>")[2].split(">");
      bodyTextsLength = bodyTexts.length;
      eventStartTime = bodyTexts[bodyTextsLength - 1];
      return eventStartTime;
    } catch (e) {
      console.error(
        "Exception occured while retrieving email to fetch event start Time " +
          e.message
      );
      throw e;
    } finally {
      return eventStartTime;
    }
  }

  async getGoToButtonlink(): Promise<any> {
    let fetched_email_obj_id;
    let invite_link;
    let inviteText;

    try {
      const fetched_email = this.fetched_email;
      console.log("fetched message is " + fetched_email);
      fetched_email_obj_id = await fetched_email.id;

      //fetch the magic link from here
      let links_in_email = await fetched_email.html.links;
      console.log(
        `total links fetched from the email is ${links_in_email.length}`
      );
      invite_link = await links_in_email[4].href;
      inviteText = await links_in_email[4].text;
      console.log("email text href at link index" + 4 + " is  " + inviteText);
    } catch (e) {
      console.error(
        "Exception occured while retrieving Go to Event button link " +
          e.message
      );
    } finally {
      console.log("invite Text", inviteText);
      return [inviteText, invite_link];
    }
  }

  async fetchIcsContents(): Promise<any> {
    let fetched_email_obj_id;
    let outlookinvite_link;
    let appleinvite_link;
    let outlookinvitetext;
    let appleinvitetext;

    try {
      const fetched_email = this.fetched_email;
      console.log("fetched message is " + fetched_email);
      fetched_email_obj_id = await fetched_email.id;

      //fetch the magic link from here
      let links_in_email = await fetched_email.html.links;
      console.log(
        `total links fetched from the email is ${links_in_email.length}`
      );
      outlookinvite_link = await links_in_email[1].href;
      outlookinvitetext = await links_in_email[1].text;
      appleinvite_link = await links_in_email[3].href;
      appleinvitetext = await links_in_email[3].text;
    } catch (e) {
      console.error(
        "Exception occured while retrieving ICS contents." + e.message
      );
    } finally {
      return [
        outlookinvite_link,
        outlookinvitetext,
        appleinvite_link,
        appleinvitetext,
      ];
    }
  }

  async fetchCalenderInvites(): Promise<any> {
    let fetched_email_obj_id;
    let googleinvite_link;
    let yahooinvite_link;
    let googleinvitetext;
    let yahooinvitetext;

    try {
      const fetched_email = this.fetched_email;
      console.log("fetched message is " + fetched_email);
      fetched_email_obj_id = await fetched_email.id;

      //fetch the magic link from here
      let links_in_email = await fetched_email.html.links;
      console.log(
        `total links fetched from the email is ${links_in_email.length}`
      );
      googleinvite_link = await links_in_email[0].href;
      googleinvitetext = await links_in_email[0].text;
      yahooinvite_link = await links_in_email[2].href;
      yahooinvitetext = await links_in_email[2].text;
    } catch (e) {
      console.error(
        "Exception occured while retrieving calender invites." + e.message
      );
    } finally {
      return [
        googleinvite_link,
        googleinvitetext,
        yahooinvite_link,
        yahooinvitetext,
      ];
    }
  }
}
