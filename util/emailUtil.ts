import { expect } from "@playwright/test";

const Mailosaur = require("mailosaur");
const mailasour_key = "uOhccejA7j1fo8C6";
const mailasour_server_id = "1gb4osi2";
const fs = require("fs");

export async function fetchOtpFromEmail(
  reciever_email_add: string,
  event_name,
  subj_line = "Login code for"
): Promise<any> {
  let fetched_email_obj_id;
  let otp;
  let message;
  const mailosaur = new Mailosaur(mailasour_key);
  let cur_datetime = new Date();
  //subtract 5 min from cur_datetime
  cur_datetime.setMinutes(cur_datetime.getMinutes() - 5);
  const criteria = {
    subject: subj_line,
    sentTo: reciever_email_add,
    body: event_name,
  };
  try {
    console.log(`trying to find otp email with criteria ${criteria}`);
    message = await mailosaur.messages.get(mailasour_server_id, criteria, {
      receivedAfter: cur_datetime,
      timeout: 60000, // in milli seconds
    });
    fetched_email_obj_id = message.id;
    otp = message.subject.split(": ")[1];
    console.log("otp fetched is " + otp);
    return otp;
  } catch (e) {
    console.error(
      `Exception occured : ${e.message} while retrieving email from mailaosaur for ${reciever_email_add}`
    );
    throw e;
  } finally {
    console.log("deleting the mailosaur email");
    if (fetched_email_obj_id) {
      await deleteEmailByObjId(fetched_email_obj_id);
    }
    return otp;
  }
}

async function deleteEmailByObjId(emailObjId) {
  const mailosaur = new Mailosaur(mailasour_key);
  console.log("trying to delete the email in mailaosaur");
  try {
    await mailosaur.messages.del(emailObjId);
  } catch (err) {
    console.log(`error occured ${err.message} while trying to delete email`);
  }
}

function fetchStudioSpeakerInviteEmail(
  reciever_email_add: string,
  sub_line: string
) {}

export async function fetchAttendeeInviteMagicLink(
  reciever_email_add: string,
  sub_line: string
): Promise<string> {
  let fetched_email_obj_id;
  let magic_link;
  const mailosaur = new Mailosaur(mailasour_key);
  let cur_datetime = new Date();
  //subtract 5 min from cur_datetime
  cur_datetime.setMinutes(cur_datetime.getMinutes() - 5);

  const criteria = {
    sentTo: reciever_email_add,
    subject: sub_line,
    // receivedAfter:cur_datetime
  };
  try {
    console.log(`trying to fetch message for ${reciever_email_add}`);
    const fetched_email = await mailosaur.messages.get(
      mailasour_server_id,
      criteria,
      {
        receivedAfter: cur_datetime,
        timeout: 60000, // in milli seconds
      }
    );
    console.log("fetched message is " + fetched_email);
    fetched_email_obj_id = fetched_email.id;
    //fetch the magic link from here
    let links_in_email = fetched_email.html.links;
    console.log(
      `total links fetched from the email is ${links_in_email.length}`
    );
    magic_link = links_in_email[4].href;
    console.log("email link href at link index" + 4 + " is  " + magic_link);
    return magic_link;
  } catch (e) {
    console.error(
      "Exception occured while retrieving email to fetch magic link " +
        e.message
    );
    throw new Error(
      `Exception occurred while retrieving email from mailosaur for email ${reciever_email_add}`
    );
  } finally {
    if (fetched_email_obj_id) {
      await deleteEmailByObjId(fetched_email_obj_id);
    }
    return magic_link;
  }
}

export async function getInviteLinkFromSpeakerInviteEmail(
  reciever_email_add: string,
  sub_line: string
): Promise<string> {
  let fetched_email_obj_id;
  let invite_link;
  const mailosaur = new Mailosaur(mailasour_key);
  let cur_datetime = new Date();
  //subtract 5 min from cur_datetime
  cur_datetime.setMinutes(cur_datetime.getMinutes() - 5);

  const criteria = {
    sentTo: reciever_email_add,
    subject: sub_line,
  };
  try {
    console.log(
      `Trying to fetch speaker invite email from emailbox for user : ${reciever_email_add}`
    );
    const fetched_email = await mailosaur.messages.get(
      mailasour_server_id,
      criteria,
      {
        receivedAfter: cur_datetime,
        timeout: 60000, // in milli seconds
      }
    );
    console.log("fetched message is " + fetched_email);
    fetched_email_obj_id = await fetched_email.id;
    //fetch the magic link from here
    let links_in_email = await fetched_email.html.links;
    console.log(
      `total links fetched from the email is ${links_in_email.length}`
    );
    invite_link = await links_in_email[4].href;
    console.log("email link href at link index" + 4 + " is  " + invite_link);
  } catch (e) {
    console.error(
      "Exception occured while retrieving speaker invite email to fetch invite link " +
        e.message
    );
    throw new Error(
      "Exception occured while retrieving speaker invite email to fetch invite link " +
        e.message
    );
  } finally {
    if (fetched_email_obj_id) {
      await deleteEmailByObjId(fetched_email_obj_id);
    }
    return invite_link;
  }
}

export async function fetchMailasaurEmailObject(
  reciever_email_add: string,
  sub_line: string,
  deleteEmail: boolean = true,
  timeout: number = 120
): Promise<any> {
  let fetched_email_obj_id;
  let invite_link;
  let fetched_email;

  const mailosaur = new Mailosaur(mailasour_key);
  let cur_datetime = new Date();
  //subtract 5 min from cur_datetime
  cur_datetime.setMinutes(cur_datetime.getMinutes() - 5);

  const criteria = {
    sentTo: reciever_email_add,
    subject: sub_line,
  };

  try {
    console.log(
      `Trying to fetch invite email from emailbox for user: ${reciever_email_add}`
    );
    fetched_email = await mailosaur.messages.get(
      mailasour_server_id,
      criteria,
      {
        receivedAfter: cur_datetime,
        timeout: 1000 * timeout, // in milli seconds
      }
    );
    console.log("fetched message is " + fetched_email);
    fetched_email_obj_id = await fetched_email.id;
  } catch (e) {
    console.error("Exception occured while retrieving email " + e.message);
    throw new Error("Exception occured while retrieving email  " + e.message);
  } finally {
    if (fetched_email_obj_id && deleteEmail) {
      // await deleteEmailByObjId(fetched_email_obj_id);
    }

    expect(fetched_email).not.toBeUndefined();

    return fetched_email;
  }
}

export async function verifyMailasaurEmailNotReceived(
  reciever_email_add: string,
  sub_line: string,
  timeout: number = 10
): Promise<any> {
  let fetched_email_obj_id;
  let fetched_email;

  const mailosaur = new Mailosaur(mailasour_key);
  let cur_datetime = new Date();
  //subtract 5 min from cur_datetime
  cur_datetime.setMinutes(cur_datetime.getMinutes() - 5);

  const criteria = {
    sentTo: reciever_email_add,
    subject: sub_line,
  };

  try {
    console.log(
      `Trying to fetch invite email from emailbox for user: ${reciever_email_add}`
    );
    fetched_email = await mailosaur.messages.get(
      mailasour_server_id,
      criteria,
      {
        receivedAfter: cur_datetime,
        timeout: 1000 * timeout, // in milli seconds
      }
    );
    console.log("fetched message is " + fetched_email);
    fetched_email_obj_id = await fetched_email.id;
  } catch (e) {
    console.error(
      "Exception occured while retrieving email. /nError Message -> " +
        e.message
    );
  } finally {
    expect(fetched_email).toBeUndefined();
  }
}

export async function recievedNewEmail(
  reciever_email_add: string,
  sub_line: string,
  timeout: number = 60
): Promise<boolean> {
  const mailosaur = new Mailosaur(mailasour_key);
  let cur_datetime = new Date();
  //subtract 5 min from cur_datetime
  cur_datetime.setMinutes(cur_datetime.getMinutes() - 5);

  const criteria = {
    sentTo: reciever_email_add,
    subject: sub_line,
  };
  console.log(
    `Trying to fetch invite email from emailbox for user: ${reciever_email_add}`
  );
  let fetched_email;
  try {
    fetched_email = await mailosaur.messages.get(
      mailasour_server_id,
      criteria,
      {
        receivedAfter: cur_datetime,
        timeout: 1000 * timeout, // in milli seconds
      }
    );
  } catch (e) {
    console.error("Exception occured while retrieving email " + e.message);
  } finally {
    return fetched_email != null && fetched_email != undefined;
  }
}

// For fetching the ICS attachment from mailosaur email.
export async function getIcsAttachment(
  reciever_email_add: string,
  sub_line: string,
  deleteEmail: boolean = true,
  timeout: number = 60
): Promise<any> {
  let fetched_email_obj_id;
  let invite_link;
  let fetched_email;

  let IcsAttachment;
  let fileName = null;
  const mailosaur = new Mailosaur(mailasour_key);
  try {
    let mailosaurEmailObject = await fetchMailasaurEmailObject(
      reciever_email_add,
      sub_line,
      false,
      timeout
    );
    let attachments: any = mailosaurEmailObject.attachments;
    fetched_email_obj_id = mailosaurEmailObject.id;
    for (const attachment of Object.values(attachments)) {
      if (attachment["contentType"] == "text/calendar") {
        console.log("The ics file attachments ID is");
        IcsAttachment = await mailosaur.files.getAttachment(attachment["id"]);
        fileName = attachment["id"] + attachment["fileName"];
        fs.writeFileSync(fileName, IcsAttachment);
      }
    }
    return IcsAttachment;
  } catch (e) {
    console.error(
      "Exception occured while retrieving ICS attachment" + e.message
    );
    throw new Error(
      "Exception occured while retrieving ICS attachment" + e.message
    );
  } finally {
    if (fetched_email_obj_id && deleteEmail) {
      // await deleteEmailByObjId(fetched_email_obj_id);
    }
    console.log("The delete object ID is:", deleteEmailByObjId);
    if (fileName == null) {
      throw new Error(`No ICS attachment found in the mail`);
    }
    console.log("ICS attachment file name is:", fileName);
    // console.log("ICS attachment",IcsAttachment.toString('base64'))
    return fileName;
  }
}

export async function getTicketAttachment(
  reciever_email_add: string,
  sub_line: string,
  timeout: number = 60
) {
  let fetched_email_obj_id;
  let ticketPdf;
  let fileName = null;
  const mailosaur = new Mailosaur(mailasour_key);
  try {
    let mailosaurEmailObject = await fetchMailasaurEmailObject(
      reciever_email_add,
      sub_line,
      false,
      timeout
    );
    let attachments: any = mailosaurEmailObject.attachments;
    fetched_email_obj_id = mailosaurEmailObject.id;
    for (const attachment of Object.values(attachments)) {
      if (attachment["contentType"] == "application/pdf") {
        console.log(attachment);
        console.log("The pdf file attachments ID is", attachment["id"]);
        console.log("The pdf file attachments ID is");
        ticketPdf = await mailosaur.files.getAttachment(attachment["id"]);
        fileName = attachment["id"] + attachment["fileName"];
        fs.writeFileSync(fileName, ticketPdf);
      }
    }
  } catch (e) {
    console.error(
      "Exception occured while retrieving PDF attachment" + e.message
    );
    throw new Error(
      "Exception occured while retrieving PDF attachment" + e.message
    );
  } finally {
    return fileName;
  }
}
