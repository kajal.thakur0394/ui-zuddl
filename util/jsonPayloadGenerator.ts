import BoothLayout from "../enums/boothLayoutEnum";
import {
  generateRandomWord,
  getRandomBoothSize,
} from "../util/randomWordGenerator";

export function boothWithoutZonePayloadGenerator(
  eventId: string,
  boothName?: string
) {
  //add user input scope
  const boothJSON: CreateBoothWithoutZonePayload = {
    boothCategory: getRandomBoothSize(),
    boothOrder: 1,
    boothOwners: [],
    boothType: BoothLayout.NORMAL,
    description: "This is default description",
    eventId: eventId,
    hiddenBooth: false,
    name: boothName != null ? boothName : randomNameGenerator(),
    tagline: "",
  };

  return boothJSON;
}

export function boothWithinZonePayloadGenerator(
  eventId: string,
  zoneId: string,
  boothName?: string
) {
  //add user input scope
  const boothJSON: CreateBoothWithinZonePayload = {
    boothCategory: getRandomBoothSize(),
    boothOrder: 1,
    boothOwners: [],
    boothType: BoothLayout.NORMAL,
    boothZoneId: zoneId,
    description: "This is default description",
    eventId: eventId,
    hiddenBooth: false,
    name: boothName != null ? boothName : randomNameGenerator(),
    tagline: "",
  };

  return boothJSON;
}

export function zonePayloadGenerator(title: string = randomNameGenerator()) {
  const zoneJSON: CreateZonePaylaod = {
    title: title,
  };

  return zoneJSON;
}

export function widgetPayloadGenerator(
  layoutId: string,
  x?: number,
  y?: number,
  width?: number,
  height?: number,
  mobX?: number,
  mobY?: number,
  mobWidth?: number,
  mobHeight?: number,
  orderId?: number,
  config = generateWidgetConfig(),
  widgetType?: string
) {
  const widgetJSON: CreateWidgetPayload = {
    x: 0,
    y: 0,
    width: 715,
    height: 80,
    mobX: 0,
    mobY: 0,
    mobWidth: 200,
    mobHeight: 80,
    layoutId: layoutId,
    orderId: 1,
    config: config,
    widgetType: "TEXT",
  };

  return widgetJSON;
}

function randomNameGenerator() {
  const boothName: string = generateRandomWord() + " " + generateRandomWord();
  return boothName;
}

function generateWidgetConfig(
  type: string = "TEXT",
  widgetTitle: string = randomNameGenerator(),
  widgetText: string = randomNameGenerator(),
  backgroundColour: string = "#0080FF",
  textColour: string = "#FFFFFF",
  fontSize: string = "14",
  fontWeight: string = "normal",
  fontFamily: string = "Arial",
  textAlign: string = "left"
) {
  let config: string = `{\"type\":\"${type}\",\"widgetTitle\":\"${widgetTitle}\",\"displayText\":[{\"widgetTitle\":\"${widgetTitle}\"},{\"text\":\"<p>${widgetText}<\/p>\\n\"},{\"backgroundColour\":\"${backgroundColour}\"},{\"textColour\":\"${textColour}\"},{\"fontSize\":\"${fontSize}\"},{\"fontWeight\":\"${fontWeight}\"},{\"fontFamily\":\"${fontFamily}\"},{\"textAlign\":\"${textAlign}\"}]}`;

  return config;
}

export interface CreateZonePaylaod {
  title?: string;
}

export interface CreateBoothWithoutZonePayload {
  boothCategory?: string;
  boothOrder?: number;
  boothOwners?: Array<string>;
  boothType?: string;
  description?: string;
  eventId: string;
  hiddenBooth?: boolean;
  name: string;
  tagline?: string;
}

export interface CreateBoothWithinZonePayload {
  boothCategory?: string;
  boothOrder?: number;
  boothOwners?: Array<string>;
  boothType?: string;
  boothZoneId: string;
  description?: string;
  eventId: string;
  hiddenBooth?: boolean;
  name: string;
  tagline?: string;
}

export interface CreateWidgetPayload {
  x?: number;
  y?: number;
  width?: number;
  height?: number;
  mobX?: number;
  mobY?: number;
  mobWidth?: number;
  mobHeight?: number;
  layoutId: string;
  orderId?: number;
  config: string;
  widgetType?: string;
}
