import { APIRequestContext } from "@playwright/test";
import EventRole from "../enums/eventRoleEnum";
import EventSettingID from "../enums/EventSettingEnum";
import EventSeriesCommunicationEnum from "../enums/EventSeriesCommunicationEnum";
import { inspect } from "util";

export async function fetchSettingsID(
  api_request_context: APIRequestContext,
  eventID: Number | String,
  attendeeNotificationTtype: EventSettingID | EventSeriesCommunicationEnum,
  roleType
): Promise<any> {
  let attendeeSettingID = null;

  const attendeeSettings = await (
    await api_request_context.get(
      `/api/notifications/${eventID}/EMAIL/?roleType=${roleType}`
    )
  ).json();

  console.log("Settings -> ", inspect(attendeeSettings, { depth: null }));

  console.log("\n");
  console.log(`Expected notification type - ${attendeeNotificationTtype}`);

  for (var setting of attendeeSettings) {
    console.log("The notification Type:", setting["notificationType"]);
    if (setting["notificationType"] == attendeeNotificationTtype) {
      attendeeSettingID = setting["settingId"];
      break;
    }
  }

  if (attendeeSettingID == null) {
    throw new Error(`Setting ID not found : ${eventID} `);
  }

  console.log("The attendee Setting ID ", attendeeSettingID);
  return attendeeSettingID;
}

// Enable or disable email communication trigger on different scenarios like 5 minute before event start and you want to attach calender or not
export async function enableDisableEmailTrigger(
  api_request_context: APIRequestContext,
  eventID: Number | String,
  notificationType: EventSettingID | EventSeriesCommunicationEnum,
  addCalenderAttachment: boolean = false,
  roleType: EventRole,
  enableEmailTrigger: boolean
) {
  const settingID = await fetchSettingsID(
    api_request_context,
    eventID,
    notificationType,
    roleType
  );

  // Enabling Email notification
  const apiResponse = await api_request_context.patch(
    `/api/notifications/${eventID}/event_setting/${settingID}/${enableEmailTrigger}?shouldUpdateCalendarInviteStatus=${addCalenderAttachment}`
  );

  if (apiResponse.status() != 200) {
    throw new Error(
      `API to trigger ${notificationType} email failed with ${apiResponse.status()} error.}`
    );
  }

  console.log(`Email trigger status is ${apiResponse.status()}`);
  if (addCalenderAttachment) {
    await enableCalendarAttachment(
      api_request_context,
      eventID,
      settingID,
      addCalenderAttachment
    );
  }
}

// Api to include calendar attachment in e-mail
export async function enableCalendarAttachment(
  api_request_context: APIRequestContext,
  eventID: Number | String,
  attendeeSettingID,
  toggle: boolean = true
) {
  // Enabling Email notification  // Adding calender Attachment in email invite
  const res = await api_request_context.patch(
    `/api/notifications/${eventID}/event_setting/${attendeeSettingID}/calendar_invite/${toggle}`
  );

  if (res.status() != 200) {
    throw new Error(
      `API to add calendar attachment ${eventID} email failed with ${res.status()} error.}`
    );
  }

  console.log(`the include calender Invite status is ${res.status()}`);
}
