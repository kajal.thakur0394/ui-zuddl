import { APIRequestContext, APIResponse, expect } from "@playwright/test";
import { DataUtl } from "./dataUtil";
import { readFileSync } from "fs";
import add_aattendee_csv_payload from "../util/add_attendee_csv_payload.json";
import { readCsvFileAsJson, writeCSV } from "../util/commonUtil";
const { json2csvAsync } = require("json-2-csv");
const { readFile, writeFile } = require("fs").promises;

export class CsvApiController {
  public orgApiRequestContext: APIRequestContext;

  constructor(orgApiRequestContext: APIRequestContext) {
    this.orgApiRequestContext = orgApiRequestContext;
  }

  async getSignedS3Url(csvFileToUpload) {
    let S3_SIGNED_PUT_URL = "/api/file-upload/signed_put";
    let filename = DataUtl.getRandomS3FileName();
    console.log(`the random file name is ${filename}`);
    let resp = await this.orgApiRequestContext.post(S3_SIGNED_PUT_URL, {
      data: filename,
    });
    let respStatus = resp.status();
    let respBody = await resp.json();
    if (respStatus != 200) {
      throw new Error(
        `API call to get s3 signed url failed with ${respStatus} and body is ${respBody}`
      );
    }
    // now get the resp json
    let preSignedUrl = respBody["presignedPutUrl"];
    let renderUrl = respBody["renderUrl"];

    console.log(
      `presigned url is ${preSignedUrl} and render url is ${renderUrl}`
    );

    const file = readFileSync(csvFileToUpload, "utf-8");
    let uploadReq = await this.orgApiRequestContext.put(preSignedUrl, {
      data: file,
    });
    console.log(uploadReq.status());
    console.log(uploadReq);
    return filename;
  }

  async processAttendeeCsvOnEndPoint(
    eventId,
    fileName,
    mappingObject?,
    listOfCsvFieldMapping?
  ) {
    let mappingUrl = `/api/event/${eventId}/csv/mapping`;
    // let csvFieldMapping="{\"email\":\"email\",\"firstName\":\"firstName\",\"lastName\":\"lastName\",\"headline\":\"headline\",\"phoneNumber\":\"phoneNumber\",\"company\":\"company\",\"designation\":\"designation\",\"bio\":\"bio\",\"country\":\"country\"}"
    let csvMappingObject;
    if (!mappingObject) {
      console.log(`since mapping object is undefined`);
      csvMappingObject = {
        email: "email",
        firstName: "firstName",
        lastName: "lastName",
        headline: "headline",
        phoneNumber: "phoneNumber",
        company: "company",
        designation: "designation",
        bio: "bio",
        country: "country",
      };
    } else {
      csvMappingObject = mappingObject;
    }

    let mappingReqPaylod = {
      csvFieldMapping: JSON.stringify(csvMappingObject),
      eventId: eventId,
    };

    console.log(`the mapping payload is `);
    console.log(JSON.stringify(csvMappingObject));

    // let mappingReqResponse = await this.orgApiRequestContext.patch(mappingUrl, {
    //   data: {
    //     mappingReqPaylod,
    //   },
    // });
    let mappingReqResponse = await this.orgApiRequestContext.patch(mappingUrl, {
      data: mappingReqPaylod,
    });

    let mappingReqResponseBody = await mappingReqResponse.json();
    let mappingReqResponseStatus = mappingReqResponse.status();
    if (mappingReqResponseStatus != 200) {
      throw new Error(
        `API call to add mapping failed with ${mappingReqResponseStatus} and body : ${mappingReqResponseBody}`
      );
    }

    let uploadCsvEndPoint = `api/event/${eventId}/attendee/import/v2`;
    let uploadCsvPayload = add_aattendee_csv_payload;
    uploadCsvPayload.s3FileUrl = fileName;
    uploadCsvPayload.canSendMail = true;
    uploadCsvPayload.fileExtension = "csv";
    uploadCsvPayload.ticketTypeId = null;
    uploadCsvPayload.csvFieldMapping =
      listOfCsvFieldMapping == undefined
        ? uploadCsvPayload.csvFieldMapping
        : listOfCsvFieldMapping;

    // make the api call
    let uploadCsvResp = await this.orgApiRequestContext.post(
      uploadCsvEndPoint,
      {
        data: uploadCsvPayload,
      }
    );
    if (uploadCsvResp.status() != 200) {
      throw new Error(
        `API call to upload attendee csv failed with ${uploadCsvResp.status()}`
      );
    }
  }

  async processRestrciedEmailCsv(eventId, fileName): Promise<Object> {
    let uploadCsvEndpointForEmailControl = `api/event/landing_page/${eventId}/email-restrictions/import`;
    let uploadCsvResp = await this.orgApiRequestContext.post(
      uploadCsvEndpointForEmailControl,
      {
        data: {
          s3FileUrl: fileName,
          eventId: eventId,
          fileExtension: "csv",
        },
      }
    );
    let uploadCsvStatus = uploadCsvResp.status();
    let uploadCsvRespBody = await uploadCsvResp.body();
    let uploadCsvResJson = await uploadCsvResp.json();
    if (uploadCsvStatus != 200) {
      console.error(`API call to upload csv failed with ${uploadCsvStatus}`);
    }
    console.log(`the response body is ${uploadCsvRespBody}`);
    return uploadCsvResJson;
  }

  async waitForCsvFileProcessingToComplete(
    eventId,
    fileName
  ): Promise<APIResponse> {
    let bulkRegistrationStatusCheckUrl = `/api/bulk-registration/${eventId}/records?fileUrl=${fileName}`;
    let csvProcessingStatusResp;
    await expect
      .poll(
        async () => {
          csvProcessingStatusResp = await this.orgApiRequestContext.get(
            bulkRegistrationStatusCheckUrl
          );
          return await csvProcessingStatusResp.json();
        },
        {
          timeout: 60 * 1000,
          message: "Waiting for csv processing to get completed",
        }
      )
      .toHaveProperty("jobStatus", "COMPLETED");
    return csvProcessingStatusResp;
  }

  async addAttendeeRecordInCsv(csvFileToAddDataIn: string, attendeeEmailList) {
    const csvWriter = require("csv-writer");
    function makeAttendeeObject(attendeeEmail: string) {
      return {
        email: attendeeEmail,
        firstName: "prateek",
        lastName: "parashar",
        headline: "QA automation",
        phoneNumber: "9791035986",
        company: "Zuddl",
        title: "QA",
        bio: "Adding bio by automation",
        country: "India",
      };
    }
    const attendeeRecordsForCsv = attendeeEmailList.map(makeAttendeeObject);

    const writer = csvWriter.createObjectCsvWriter({
      path: csvFileToAddDataIn,
      header: [
        { id: "email", title: "email" },
        { id: "firstName", title: "firstName" },
        { id: "lastName", title: "lastName" },
        { id: "headline", title: "headline" },
        { id: "phoneNumber", title: "phoneNumber" },
        { id: "company", title: "company" },
        { id: "title", title: "title" },
        { id: "bio", title: "bio" },
        { id: "country", title: "country" },
      ],
    });
    writer.writeRecords(attendeeRecordsForCsv).then(() => {
      console.log("Done!");
    });
  }

  async downloadAttendeeCsv(eventId) {
    const downloadCsvEndpoint = `${
      DataUtl.getApplicationTestDataObj().newOrgSideBaseUrl
    }/api/event/${eventId}/registrations/downloadCSV`;
    let downloadCsvAPIResp = await this.orgApiRequestContext.get(
      downloadCsvEndpoint,
      {
        headers: {
          accept: "application/json, text/plain, */*",
        },
      }
    );

    if (downloadCsvAPIResp.status() != 200) {
      throw new Error(
        `API call to download csv for event ${eventId} failed with downloadCsvAPIResp.status()`
      );
    }
    const resp = await downloadCsvAPIResp.body();
    console.log(`the response body is ${resp}`);
  }

  async createCsvWithCustomFields(destinationCsvFile, csvRecordToAdd?) {
    /* Need base csv, from which we will get the default headers
     * then custom fields/condition fields test data
     * based on the test data, we will extend the headers of base csv
     * and then will add the test data as per the test case for custom fields headers
     */

    const baseCsvAsJson = await readCsvFileAsJson(
      "test-data/uploadcsv-customfields-testdata/baseAttendeeCsv.csv"
    );
    let listOfRecords = [];
    console.log(baseCsvAsJson);
    let baseRecord = baseCsvAsJson[0];
    if (csvRecordToAdd != undefined) {
      for (const eachFieldkey in csvRecordToAdd) {
        if (eachFieldkey == "designation") {
          //because designaton header is title
          baseRecord["title"] = csvRecordToAdd[eachFieldkey];
        } else {
          baseRecord[eachFieldkey] = csvRecordToAdd[eachFieldkey];
        }
      }
    }
    listOfRecords.push(baseRecord);
    console.log(listOfRecords);
    // console.log(`base record is ${baseRecord}`);
    (async () => {
      const csv = await json2csvAsync(listOfRecords);
      await writeCSV(destinationCsvFile, csv);
    })();
  }

  async fetchMappingObjectBasedOnGivenCsv(sourceFilePath) {
    const csvDataAsJson = await readCsvFileAsJson(sourceFilePath);
    //fetch the first record of it
    let firstRecordObject = csvDataAsJson[0];
    //then fetch the keys
    let headerKeys = Object.keys(firstRecordObject);
    //create a object with keys and value as string value of keys
    let mappingObject = {};
    for (const eachKey of headerKeys) {
      mappingObject[eachKey] = eachKey.toString();
    }
    console.log("### mapping object retrieved from csv###");
    console.log(mappingObject);
    return mappingObject;
  }
}
