import { APIRequestContext, Page, expect } from "@playwright/test";
import MetBaseQueryMapping from "../metabaseQueryMapping.json";
import { DataUtl } from "./dataUtil";
export class DBUtil {
  envName: any;
  readonly metaBaseConnObj: APIRequestContext;
  readonly metaBaseUserSessionId;
  readonly headers;
  readonly page: Page;
  readonly queryNameList: string[] = [];

  constructor(page: Page) {
    this.envName = process.env.run_env;
    console.log(`setting up DB util for ${this.envName}`);
    this.metaBaseUserSessionId = process.env.METABASE_USER_SESSION_ID;
    this.page = page;
    this.metaBaseConnObj = page.request;
    this.headers = {
      "X-Metabase-Session": this.metaBaseUserSessionId,
      "Content-Type": "application/x-www-form-urlencoded'",
    };
  }

  getQueryId(queryName: string) {
    this.getDBEnv(queryName);
    const queryId = MetBaseQueryMapping[queryName][this.envName];

    // const queryId =  MetBaseQueryMapping.fetchEventOtpQuery[this.envName]
    if (queryId == undefined) {
      throw new Error(`No queryid found for ${this.envName}`);
    }
    return queryId;
  }

  getDBEnv(queryName: string) {
    if (
      this.queryNameList.length > 0 &&
      this.queryNameList.includes(queryName)
    ) {
      if (this.envName.includes("staging")) {
        this.envName = "core-staging";
      } else {
        this.envName = "core-pre-prod";
      }
      console.log(`query Name to execute is from env->${this.envName}`);
    }
  }

  async fetchLoginOtpForEvent(user_email: string, event_id) {
    let otpApiResp;
    // await this.page.waitForTimeout(5000)
    let queryId = this.getQueryId("fetchEventOtpQuery");
    console.log(`running query for ${user_email} and ${event_id}`);
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: user_email,
        target: ["variable", ["template-tag", "email"]],
        id: "232467e3-10e1-ff46-7952-d31a3352ab55",
      },
      {
        type: "category",
        value: event_id,
        target: ["variable", ["template-tag", "event_id"]],
        id: "773f3344-0810-c622-7e82-69488860ceeb",
      },
    ];
    await expect
      .poll(
        async () => {
          otpApiResp = await this.metaBaseConnObj.post(queryEndPoint, {
            form: {
              parameters: JSON.stringify(parameters),
            },
            headers: this.headers,
          });
          return await otpApiResp.json();
        },
        {
          timeout: 20 * 1000,
          message: "Waiting for OTP to get generated in DB",
        }
      )
      .toHaveLength(1);
    let otpApiResJson = await otpApiResp.json();
    let otp = otpApiResJson[0]["otp_code"];
    return otp;
  }

  async fetchLoginOtpByVerificationId(
    user_email: string,
    verification_id: string
  ) {
    let otpApiResp;
    // await this.page.waitForTimeout(5000)
    let queryId = this.getQueryId("fetchLoginOtpByVerificationId");
    console.log(
      `running query for ${user_email} and verification id : ${verification_id}`
    );
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: user_email,
        target: ["variable", ["template-tag", "email"]],
        id: "232467e3-10e1-ff46-7952-d31a3352ab55",
      },
      {
        type: "category",
        value: verification_id,
        target: ["variable", ["template-tag", "verification_id"]],
        id: "773f3344-0810-c622-7e82-69488860ceeb",
      },
    ];
    await expect
      .poll(
        async () => {
          otpApiResp = await this.metaBaseConnObj.post(queryEndPoint, {
            form: {
              parameters: JSON.stringify(parameters),
            },
            headers: this.headers,
          });
          return await otpApiResp.json();
        },
        {
          timeout: 20 * 1000,
          message: "Waiting for OTP to get generated in DB",
        }
      )
      .toHaveLength(1);
    let otpApiResJson = await otpApiResp.json();
    let otp = otpApiResJson[0]["otp_code"];
    return otp;
  }

  async validateEventEmailRestrictionInDB(
    event_id,
    expectedRestrictionType,
    expectedEmailDomainType,
    expectedCsvName
  ) {
    // await this.page.waitForTimeout(5000)
    let queryId = this.getQueryId("eventEmailRestrictionQuery");
    console.log(`running query for ${event_id}`);
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: event_id,
        target: ["variable", ["template-tag", "event_id"]],
      },
    ];

    await expect(async () => {
      let getEventEmailRestrictionResp = await this.metaBaseConnObj.post(
        queryEndPoint,
        {
          form: {
            parameters: JSON.stringify(parameters),
          },
          headers: this.headers,
        }
      );
      expect(
        getEventEmailRestrictionResp.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let eventEmailRestrictionRespJson =
        await getEventEmailRestrictionResp.json();
      expect(
        eventEmailRestrictionRespJson[0]["email_restriction_type"],
        `expecting email restriction in DB to be ${expectedRestrictionType}`
      ).toBe(expectedRestrictionType);
      expect(
        eventEmailRestrictionRespJson[0]["restricted_email_domains_types"],
        `expecting email domain type to be ${expectedEmailDomainType}`
      ).toBe(expectedEmailDomainType);
      expect(
        eventEmailRestrictionRespJson[0]["restricted_email_domains_filename"],
        `expecting email domain filename to be ${expectedCsvName}`
      ).toBe(expectedCsvName);
    }, "Validating DB for applied event email restriction").toPass({
      timeout: 5000,
    });
  }

  async fetchMagicLinkFromDB(eventId, email: string): Promise<string> {
    // await this.page.waitForTimeout(5000)
    let queryId = this.getQueryId("getMagicTokenQuery");
    let magicTokenId;
    let registrationId;
    console.log(`running query for ${eventId}`);
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: eventId,
        target: ["variable", ["template-tag", "event_id"]],
      },
      {
        type: "category",
        value: email,
        target: ["variable", ["template-tag", "email"]],
      },
    ];

    await expect(async () => {
      let getMagicLinkDataRes = await this.metaBaseConnObj.post(queryEndPoint, {
        form: {
          parameters: JSON.stringify(parameters),
        },
        headers: this.headers,
      });
      expect(
        getMagicLinkDataRes.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let magicLinkDataResJson = await getMagicLinkDataRes.json();
      expect(magicLinkDataResJson[0]["registration_id"]).not.toBe(null);
      expect(magicLinkDataResJson[0]["magic_token_id"]).not.toBe(null);
      // storing data in the variables and returning it back
      registrationId = magicLinkDataResJson[0]["registration_id"];
      magicTokenId = magicLinkDataResJson[0]["magic_token_id"];
    }, `Fetching magic token and registration id from DB for ${eventId} and ${email}`).toPass(
      {
        timeout: 10000,
      }
    );
    let baseUrl = DataUtl.getApplicationTestDataObj().baseUrl;
    let magicLinkUrl = `${baseUrl}/p/a/event/${eventId}?re=${registrationId}&mt=${magicTokenId}`;
    console.log("magicLinkUrl: " + magicLinkUrl);
    return magicLinkUrl;
  }

  async fetchDudaAttendeeMagicLinkFromDB(
    eventId,
    email: string,
    dudaPublishedAttendeeLandingPage: string
  ): Promise<string> {
    // await this.page.waitForTimeout(5000)
    let queryId = this.getQueryId("getMagicTokenQuery");
    let magicTokenId;
    let registrationId;
    console.log(`running query for ${eventId}`);
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: eventId,
        target: ["variable", ["template-tag", "event_id"]],
      },
      {
        type: "category",
        value: email,
        target: ["variable", ["template-tag", "email"]],
      },
    ];

    await expect(async () => {
      let getMagicLinkDataRes = await this.metaBaseConnObj.post(queryEndPoint, {
        form: {
          parameters: JSON.stringify(parameters),
        },
        headers: this.headers,
      });
      expect(
        getMagicLinkDataRes.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let magicLinkDataResJson = await getMagicLinkDataRes.json();
      expect(magicLinkDataResJson[0]["registration_id"]).not.toBe(null);
      expect(magicLinkDataResJson[0]["magic_token_id"]).not.toBe(null);
      // storing data in the variables and returning it back
      registrationId = magicLinkDataResJson[0]["registration_id"];
      magicTokenId = magicLinkDataResJson[0]["magic_token_id"];
    }, `Fetching magic token and registration id from DB for ${eventId} and ${email}`).toPass(
      {
        timeout: 5000,
      }
    );

    let baseUrl = dudaPublishedAttendeeLandingPage;
    let magicLinkUrl = `${baseUrl}?re=${registrationId}&mt=${magicTokenId}`;
    return magicLinkUrl;
  }

  async fetchMagicLinkForSpeakerFromDB(
    eventId,
    email: string
  ): Promise<string> {
    // await this.page.waitForTimeout(5000)
    let queryId = this.getQueryId("getSpeakerMagicTokenQuery");
    let magicTokenId;
    let speakerId;
    console.log(
      `running query to fetch speaker magic link  for email : ${email} from event id : ${eventId}`
    );
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: eventId,
        target: ["variable", ["template-tag", "event_id"]],
      },
      {
        type: "category",
        value: email,
        target: ["variable", ["template-tag", "email"]],
      },
    ];

    await expect(async () => {
      let getMagicLinkDataRes = await this.metaBaseConnObj.post(queryEndPoint, {
        form: {
          parameters: JSON.stringify(parameters),
        },
        headers: this.headers,
      });
      expect(
        getMagicLinkDataRes.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let magicLinkDataResJson = await getMagicLinkDataRes.json();
      console.log(magicLinkDataResJson);
      expect(magicLinkDataResJson[0]["speaker_id"]).not.toBe(null);
      expect(magicLinkDataResJson[0]["magic_token_id"]).not.toBe(null);
      // storing data in the variables and returning it back
      speakerId = magicLinkDataResJson[0]["speaker_id"];
      magicTokenId = magicLinkDataResJson[0]["magic_token_id"];
    }, `Fetching magic token and speaker id from DB for ${eventId} and ${email}`).toPass(
      {
        timeout: 5000,
      }
    );
    let baseUrl = DataUtl.getApplicationTestDataObj().baseUrl;
    let magicLinkUrl = `${baseUrl}/p/event/${eventId}?re=${speakerId}&mt=${magicTokenId}`;
    console.log(`speaker magic link is ${magicLinkUrl}`);
    return magicLinkUrl;
  }

  async fetchDudaMagicLinkForSpeakerFromDB(
    eventId,
    email: string,
    dudaSpeakerLandingPage: string
  ): Promise<string> {
    // await this.page.waitForTimeout(5000)
    let queryId = this.getQueryId("getSpeakerMagicTokenQuery");
    let magicTokenId;
    let speakerId;
    console.log(
      `running query to fetch speaker magic link  for email : ${email} from event id : ${eventId}`
    );
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: eventId,
        target: ["variable", ["template-tag", "event_id"]],
      },
      {
        type: "category",
        value: email,
        target: ["variable", ["template-tag", "email"]],
      },
    ];

    await expect(async () => {
      let getMagicLinkDataRes = await this.metaBaseConnObj.post(queryEndPoint, {
        form: {
          parameters: JSON.stringify(parameters),
        },
        headers: this.headers,
      });
      expect(
        getMagicLinkDataRes.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let magicLinkDataResJson = await getMagicLinkDataRes.json();
      expect(magicLinkDataResJson[0]["speaker_id"]).not.toBe(null);
      expect(magicLinkDataResJson[0]["magic_token_id"]).not.toBe(null);
      // storing data in the variables and returning it back
      speakerId = magicLinkDataResJson[0]["speaker_id"];
      magicTokenId = magicLinkDataResJson[0]["magic_token_id"];
    }, `Fetching magic token and speaker id from DB for ${eventId} and ${email}`).toPass(
      {
        timeout: 5000,
      }
    );
    //https://qateamstagingtest-4246-1.site.zuddl.io/?r=s&re=a0e63e8b-93a7-41d1-9e1d-d640f3b74e2a&mt=27ef3df8-6ca0-41a5-8769-18abbf3c4197
    let baseUrl = dudaSpeakerLandingPage;
    let magicLinkUrl = `${baseUrl}&re=${speakerId}&mt=${magicTokenId}`;
    return magicLinkUrl;
  }

  async fetchCustomFieldsFilledByRegisteredUser(eventId, email: string) {
    // await this.page.waitForTimeout(5000)
    let queryId = this.getQueryId("getRegisterUserCustomFields");
    let extractedCustomFields;
    console.log(`running query for ${eventId}`);
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: eventId,
        target: ["variable", ["template-tag", "event_id"]],
      },
      {
        type: "category",
        value: email,
        target: ["variable", ["template-tag", "email"]],
      },
    ];

    await expect(async () => {
      let getUserRegistrationCustomFields = await this.metaBaseConnObj.post(
        queryEndPoint,
        {
          form: {
            parameters: JSON.stringify(parameters),
          },
          headers: this.headers,
        }
      );
      expect(
        getUserRegistrationCustomFields.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let getUserRegistrationCustomFieldsJson =
        await getUserRegistrationCustomFields.json();
      expect(getUserRegistrationCustomFieldsJson[0]["custom_field"]).not.toBe(
        null
      );
      // storing data in the variables and returning it back
      extractedCustomFields =
        getUserRegistrationCustomFieldsJson[0]["custom_field"];
    }, `Fetching registered user custom fields for  ${eventId} and ${email}`).toPass(
      {
        timeout: 5000,
      }
    );
    return JSON.parse(extractedCustomFields);
  }

  async fetchUserEventRegistrationDataFromDb(
    eventId,
    email: string
  ): Promise<object> {
    let queryId = this.getQueryId("getUserEventRegistrationData");
    let userRegData;
    console.log(`running query for ${eventId}`);
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: eventId,
        target: ["variable", ["template-tag", "event_id"]],
      },
      {
        type: "category",
        value: email,
        target: ["variable", ["template-tag", "email"]],
      },
    ];

    await expect(async () => {
      let getUserEventRegData = await this.metaBaseConnObj.post(queryEndPoint, {
        form: {
          parameters: JSON.stringify(parameters),
        },
        headers: this.headers,
      });
      expect(
        getUserEventRegData.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let getUserEventRegDataJson = await getUserEventRegData.json();
      expect(
        getUserEventRegDataJson[0]["registration_id"],
        "expecting registration id to be not null"
      ).not.toBe(null);
      userRegData = await getUserEventRegDataJson[0];
      // console.log(`The fetched data for query is ${await getUserEventRegDataJson}`)
    }, `Fetching user event reg data for  ${eventId} and ${email}`).toPass({
      timeout: 5000,
    });
    console.log("The userreg Data", await userRegData);
    return await userRegData;
  }

  async fetchUserDataForV1BetaFlowValidation(
    eventId,
    email: string
  ): Promise<object> {
    let queryId = this.getQueryId("getUserDataV1BetaRegistration");
    let userRegData: object;
    console.log(`running query for ${eventId}`);
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: eventId,
        target: ["variable", ["template-tag", "event_id"]],
      },
      {
        type: "category",
        value: email,
        target: ["variable", ["template-tag", "email"]],
      },
    ];

    await expect(async () => {
      let getUserDataForV1BetaApiRes = await this.metaBaseConnObj.post(
        queryEndPoint,
        {
          form: {
            parameters: JSON.stringify(parameters),
          },
          headers: this.headers,
        }
      );
      expect(
        getUserDataForV1BetaApiRes.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let getUserDataForV1BetaApiResJson =
        await getUserDataForV1BetaApiRes.json();
      expect(
        getUserDataForV1BetaApiResJson[0]["registration_id"],
        "expecting registration id to be not null"
      ).not.toBe(null);
      expect(
        getUserDataForV1BetaApiResJson[0]["event_role_id"],
        "expecting event role id to not be null"
      ).not.toBe(null);
      expect(
        getUserDataForV1BetaApiResJson[0]["account_id"],
        "expecting account id not to be null"
      ).not.toBe(null);
      userRegData = getUserDataForV1BetaApiResJson[0];
    }, `Fetching user reg, account and event role data from DB for ${eventId} and ${email}`).toPass(
      {
        timeout: 5000,
      }
    );
    return userRegData!;
  }

  async fetchUserDataFromEventRegAccountAndEventRoleTable(
    eventId,
    email: string
  ): Promise<object> {
    let queryId = this.getQueryId("getUserRegAccountEventRoleData");
    let userRegData: object;
    console.log(`running query for ${eventId}`);
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: eventId,
        target: ["variable", ["template-tag", "event_id"]],
      },
      {
        type: "category",
        value: email,
        target: ["variable", ["template-tag", "email"]],
      },
    ];

    await expect(async () => {
      let getUserDataForV1BetaApiRes = await this.metaBaseConnObj.post(
        queryEndPoint,
        {
          form: {
            parameters: JSON.stringify(parameters),
          },
          headers: this.headers,
        }
      );
      expect(
        getUserDataForV1BetaApiRes.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let getUserDataForV1BetaApiResJson =
        await getUserDataForV1BetaApiRes.json();
      expect(
        getUserDataForV1BetaApiResJson[0]["registration_id"],
        "expecting registration id to be not null"
      ).not.toBe(null);
      userRegData = getUserDataForV1BetaApiResJson[0];
    }, `Fetching user reg, account and event role data from DB for ${eventId} and ${email}`).toPass(
      {
        timeout: 5000,
      }
    );
    return userRegData!;
  }

  async fetchUserEventRoleData(eventId, email: string) {
    let queryId = this.getQueryId("getUserEventRoleData");
    let userEventRoleData: object;
    console.log(`running query for ${eventId}`);
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: eventId,
        target: ["variable", ["template-tag", "event_id"]],
      },
      {
        type: "category",
        value: email,
        target: ["variable", ["template-tag", "email"]],
      },
    ];

    await expect(async () => {
      let getUserEventRoleDataRes = await this.metaBaseConnObj.post(
        queryEndPoint,
        {
          form: {
            parameters: JSON.stringify(parameters),
          },
          headers: this.headers,
        }
      );
      expect(
        getUserEventRoleDataRes.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let getUserEventRoleDataResJson = await getUserEventRoleDataRes.json();
      expect(
        getUserEventRoleDataResJson[0]["account_id"],
        "expecting account id not to be null in event role data"
      ).not.toBe(null);
      userEventRoleData = getUserEventRoleDataResJson[0];
    }, `Fetching user event role data  from DB for ${eventId} and ${email}`).toPass(
      {
        timeout: 10000,
      }
    );
    return userEventRoleData!;
  }

  async fetchUserAccessGroupIdForEvent(email: string, eventId: string) {
    let queryId = this.getQueryId("fetchUserAccessGroupIdForEvent");
    let listOfUserAccessGroupForThisEvent: object;
    console.log(`running query for ${eventId} and useremail : ${email}`);
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: eventId,
        target: ["variable", ["template-tag", "event_id"]],
      },
      {
        type: "category",
        value: email,
        target: ["variable", ["template-tag", "email"]],
      },
    ];

    await expect(async () => {
      let getUserAccessGroupForAnEventRes = await this.metaBaseConnObj.post(
        queryEndPoint,
        {
          form: {
            parameters: JSON.stringify(parameters),
          },
          headers: this.headers,
        }
      );
      expect(
        getUserAccessGroupForAnEventRes.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let getUserAccessGroupForAnEventResJson =
        await getUserAccessGroupForAnEventRes.json();
      expect(
        getUserAccessGroupForAnEventResJson[0]["access_group_id"],
        "expecting access group id not to be null in user access group table for this event"
      ).not.toBe(null);
      listOfUserAccessGroupForThisEvent =
        getUserAccessGroupForAnEventResJson[0];
    }, `Fetching useraccess group data from DB for ${eventId} and ${email}`).toPass(
      {
        timeout: 5000,
      }
    );
    return listOfUserAccessGroupForThisEvent!["access_group_id"];
  }

  async verifyUserEmailIsPresentInSpekaerTableForAnEvent(
    email: string,
    eventId: string
  ) {
    let queryId = this.getQueryId("checkIfSpeakerRecordPresentInDBForEvent");
    let isSpeakerEmailPresentInSpeakerTable: boolean = false;
    console.log(`running query for ${eventId} and useremail : ${email}`);
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: eventId,
        target: ["variable", ["template-tag", "event_id"]],
      },
      {
        type: "category",
        value: email,
        target: ["variable", ["template-tag", "email"]],
      },
    ];

    await expect(async () => {
      let getSpeakerTableEntryForEmailRes = await this.metaBaseConnObj.post(
        queryEndPoint,
        {
          form: {
            parameters: JSON.stringify(parameters),
          },
          headers: this.headers,
        }
      );
      expect(
        getSpeakerTableEntryForEmailRes.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let getSpeakerTableEntryForEmailResJson =
        await getSpeakerTableEntryForEmailRes.json();
      expect(
        getSpeakerTableEntryForEmailResJson[0]["speaker_record_count"],
        "expecting speaker record count to be 1 for given email and event id"
      ).toBe(1);
      isSpeakerEmailPresentInSpeakerTable = true;
    }, `expecting speaker email to be present in speaker table for ${eventId} and ${email}`).toPass(
      {
        timeout: 5000,
      }
    );
    return isSpeakerEmailPresentInSpeakerTable;
  }

  async verifyUserEmailIsNotPresentInSpekaerTableForAnEvent(
    email: string,
    eventId: string
  ) {
    let queryId = this.getQueryId("checkIfSpeakerRecordPresentInDBForEvent");
    let isSpeakerEmailNotPresentInSpeakerTable: boolean = false;
    console.log(`running query for ${eventId} and useremail : ${email}`);
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: eventId,
        target: ["variable", ["template-tag", "event_id"]],
      },
      {
        type: "category",
        value: email,
        target: ["variable", ["template-tag", "email"]],
      },
    ];

    await expect(async () => {
      let getSpeakerTableEntryForEmailRes = await this.metaBaseConnObj.post(
        queryEndPoint,
        {
          form: {
            parameters: JSON.stringify(parameters),
          },
          headers: this.headers,
        }
      );
      expect(
        getSpeakerTableEntryForEmailRes.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let getSpeakerTableEntryForEmailResJson =
        await getSpeakerTableEntryForEmailRes.json();
      expect(
        getSpeakerTableEntryForEmailResJson[0]["speaker_record_count"],
        "expecting speaker record count to be 0 for given email and event id in speaker table"
      ).toBe(0);
      isSpeakerEmailNotPresentInSpeakerTable = true;
    }, `expecting speaker email to not be present in speaker table for ${eventId} and ${email}`).toPass(
      {
        timeout: 5000,
      }
    );
    return isSpeakerEmailNotPresentInSpeakerTable;
  }
  async verifyUserEmailIsPresentInEventRegTableForGivenEvent(
    email: string,
    eventId: string
  ) {
    await this.page.waitForTimeout(5000);
    let queryId = this.getQueryId("checkIfUserEntryPresentInEventRegTable");
    let isUserEmailPresentInEventRegTableForGivenEvent: boolean = false;
    console.log(`running query for ${eventId} and useremail : ${email}`);
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: eventId,
        target: ["variable", ["template-tag", "event_id"]],
      },
      {
        type: "category",
        value: email,
        target: ["variable", ["template-tag", "email"]],
      },
    ];

    await expect(async () => {
      let getEventRegTablEntryForEmailRes = await this.metaBaseConnObj.post(
        queryEndPoint,
        {
          form: {
            parameters: JSON.stringify(parameters),
          },
          headers: this.headers,
        }
      );
      expect(
        getEventRegTablEntryForEmailRes.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let getEventRegTablEntryForEmailResJson =
        await getEventRegTablEntryForEmailRes.json();
      expect(
        getEventRegTablEntryForEmailResJson[0]["attendee_record_count"],
        "expecting attendee record count to be 1 for given email and event id in event reg table"
      ).toBe(1);
      isUserEmailPresentInEventRegTableForGivenEvent = true;
    }, `expecting user email to be present in event reg table for ${eventId} and ${email}`).toPass(
      {
        timeout: 5000,
      }
    );
    return isUserEmailPresentInEventRegTableForGivenEvent;
  }

  async verifyUserEmailIsNotPresentInEventRegTableForGivenEvent(
    email: string,
    eventId: string
  ) {
    let queryId = this.getQueryId("checkIfUserEntryPresentInEventRegTable");
    let isUserEmailPresentInEventRegTableForGivenEvent: boolean = false;
    console.log(`running query for ${eventId} and useremail : ${email}`);
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: eventId,
        target: ["variable", ["template-tag", "event_id"]],
      },
      {
        type: "category",
        value: email,
        target: ["variable", ["template-tag", "email"]],
      },
    ];

    await expect(async () => {
      let getEventRegTablEntryForEmailRes = await this.metaBaseConnObj.post(
        queryEndPoint,
        {
          form: {
            parameters: JSON.stringify(parameters),
          },
          headers: this.headers,
        }
      );
      expect(
        getEventRegTablEntryForEmailRes.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let getEventRegTablEntryForEmailResJson =
        await getEventRegTablEntryForEmailRes.json();
      expect(
        getEventRegTablEntryForEmailResJson[0]["attendee_record_count"],
        "expecting attendee record count to be 9 for given email and event id in event reg table"
      ).toBe(0);
      isUserEmailPresentInEventRegTableForGivenEvent = true;
    }, `expecting user email to not be present in event reg table for ${eventId} and ${email}`).toPass(
      {
        timeout: 5000,
      }
    );
    return isUserEmailPresentInEventRegTableForGivenEvent;
  }
  async verifyIfUserEmailIsActiveInEventRegTableForGivenEvent(
    email: string,
    eventId: string
  ) {
    let queryId = this.getQueryId("checkIfUserEmailIsActiveInInEventRegTable");
    let isUserActiveInOrganisation: boolean = false;
    let isUserEmailPresentInEventRegTableForGivenEvent: boolean = false;
    console.log(`running query for ${eventId} and useremail : ${email}`);
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: eventId,
        target: ["variable", ["template-tag", "event_id"]],
      },
      {
        type: "category",
        value: email,
        target: ["variable", ["template-tag", "email"]],
      },
    ];

    await expect(async () => {
      let getEventRegTablEntryForEmailRes = await this.metaBaseConnObj.post(
        queryEndPoint,
        {
          form: {
            parameters: JSON.stringify(parameters),
          },
          headers: this.headers,
        }
      );
      expect(
        getEventRegTablEntryForEmailRes.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let getEventRegTablEntryForEmailResJson =
        await getEventRegTablEntryForEmailRes.json();
      expect(
        getEventRegTablEntryForEmailResJson[0]["is_active"],
        "expecting is_active record count to be true for given email and eventId"
      ).toBe(true);
      isUserActiveInOrganisation = true;
    }, `expecting member email to be active in event table as true for ${email}`).toPass(
      {
        timeout: 10000,
      }
    );
    return isUserEmailPresentInEventRegTableForGivenEvent;
  }

  async verifyIfUserEmailIsNotActiveInEventRegTableForGivenEvent(
    email: string,
    eventId: string
  ) {
    let queryId = this.getQueryId("checkIfUserEmailIsActiveInEventRegTable");
    let isUserActiveInOrganisation: boolean = false;
    let isUserEmailPresentInEventRegTableForGivenEvent: boolean = false;
    console.log(`running query for ${eventId} and useremail : ${email}`);
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: eventId,
        target: ["variable", ["template-tag", "event_id"]],
      },
      {
        type: "category",
        value: email,
        target: ["variable", ["template-tag", "email"]],
      },
    ];

    await expect(async () => {
      let getEventRegTablEntryForEmailRes = await this.metaBaseConnObj.post(
        queryEndPoint,
        {
          form: {
            parameters: JSON.stringify(parameters),
          },
          headers: this.headers,
        }
      );
      expect(
        getEventRegTablEntryForEmailRes.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let getEventRegTablEntryForEmailResJson =
        await getEventRegTablEntryForEmailRes.json();
      expect(
        getEventRegTablEntryForEmailResJson[0]["is_active"],
        "expecting is_active record count to be false for given email and eventId"
      ).toBe(false);
      isUserActiveInOrganisation = false;
    }, `expecting member email to be not-active in event table as true for ${email}`).toPass(
      {
        timeout: 10000,
      }
    );
    return isUserEmailPresentInEventRegTableForGivenEvent;
  }

  async verifyIfUserIsActiveInOrganisation(
    email: string,
    organisationId: string,
    accountId: string
  ) {
    this.queryNameList.push("checkIfUserPresentInOrganisation");
    let queryId = this.getQueryId("checkIfUserPresentInOrganisation");
    let isUserActiveInOrganisation: boolean = false;
    console.log(`running query for ${organisationId} and useremail : ${email}`);
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: organisationId,
        target: ["variable", ["template-tag", "organization_id"]],
      },
      {
        type: "category",
        value: accountId,
        target: ["variable", ["template-tag", "account_id"]],
      },
    ];
    await expect(async () => {
      let userActiveResponse = await this.metaBaseConnObj.post(queryEndPoint, {
        form: {
          parameters: JSON.stringify(parameters),
        },
        headers: this.headers,
      });
      expect(
        userActiveResponse.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let userActiveResponseJson = await userActiveResponse.json();
      expect(
        userActiveResponseJson[0]["is_active"],
        "expecting is_active record count to be true for given email and organization_id"
      ).toBe(true);
      isUserActiveInOrganisation = true;
    }, `expecting member email to be present in organization table as true for ${email}`).toPass(
      {
        timeout: 10000,
      }
    );
    return isUserActiveInOrganisation;
  }

  async verifyIfUserIsInActiveInOrganisation(
    email: string,
    organisationId: string,
    accountId: string
  ) {
    this.queryNameList.push("checkIfUserPresentInOrganisation");
    let queryId = this.getQueryId("checkIfUserPresentInOrganisation");
    let isUserActiveInOrganisation: boolean = true;
    console.log(`running query for ${organisationId} and useremail : ${email}`);
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: organisationId,
        target: ["variable", ["template-tag", "organization_id"]],
      },
      {
        type: "category",
        value: accountId,
        target: ["variable", ["template-tag", "account_id"]],
      },
    ];
    await expect(async () => {
      let userActiveResponse = await this.metaBaseConnObj.post(queryEndPoint, {
        form: {
          parameters: JSON.stringify(parameters),
        },
        headers: this.headers,
      });
      expect(
        userActiveResponse.status(),
        "expecting metabase api to return 200"
      ).toBe(200);
      let userActiveResponseJson = await userActiveResponse.json();
      expect(
        userActiveResponseJson[0]["is_active"],
        "expecting is_active record count to be false for given email and organization_id"
      ).toBe(false);
      isUserActiveInOrganisation = false;
    }, `expecting member email to be present in organization table as false for ${email}`).toPass(
      {
        timeout: 10000,
      }
    );
    return isUserActiveInOrganisation;
  }

  async fetchEventListForNonCancelledEvent() {
    let queryId = this.getQueryId("fetchEventIdListForNotCanceledEvents");
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;

    let getEventList = await this.metaBaseConnObj.post(queryEndPoint, {
      headers: this.headers,
    });
    expect(getEventList.status(), "expecting metabase api to return 200").toBe(
      200
    );

    let getEventListJson = await getEventList.json();
    // Extract unique event_id values and convert them to strings
    const uniqueEventIds: any[] = [
      ...new Set(getEventListJson.map((event) => String(event.event_id))),
    ];

    // Now, uniqueEventIds will be of type string[]
    const eventIdsArray: string[] = uniqueEventIds;
    return eventIdsArray;
  }

  async fetchVivenuTransactionRecord(eventId: string, email: string) {
    let queryId = this.getQueryId("fetchVivenuTransactionRecord");
    let queryEndPoint = `https://metabase.zuddl.com/api/card/${queryId}/query/json`;
    const parameters = [
      {
        type: "category",
        value: eventId,
        target: ["variable", ["template-tag", "event_id"]],
      },
      {
        type: "category",
        value: email,
        target: ["variable", ["template-tag", "email_id"]],
      },
    ];

    let getVivenuTransactionRecord = await this.metaBaseConnObj.post(
      queryEndPoint,
      {
        form: {
          parameters: JSON.stringify(parameters),
        },
        headers: this.headers,
      }
    );

    expect(
      getVivenuTransactionRecord.status(),
      "expecting metabase api to return 200"
    ).toBe(200);

    let getVivenuTransactionRecordJson =
      await getVivenuTransactionRecord.json();

    return getVivenuTransactionRecordJson;
  }
}
