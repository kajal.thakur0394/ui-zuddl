import { APIRequestContext, APIResponse, expect, test } from "@playwright/test";
import EventEntryType from "../enums/eventEntryEnum";
import { readFileSync } from "fs";
import { DataUtl } from "../util/dataUtil";
import { checkIfFileExists } from "../util/commonUtil";
import createEventPayloadData from "../util/create_event_payload.json";
import add_aattendee_csv_payload from "../util/add_attendee_csv_payload.json";
import LandingPageType from "../enums/landingPageEnum";
import EventType from "../enums/eventTypeEnum";
import { EventZones } from "../enums/EventZoneEnum";
import { inspect } from "util";
import BrowserFactory from "./BrowserFactory";

let start_date_from_today: Date | string;
let end_date_from_today: Date | string;
let currentDate: Date;

export async function getStartDateFromToday() {
  return start_date_from_today;
}

export async function getEndDateFromToday() {
  return end_date_from_today;
}

export async function createNewStudio(
  api_request_context: APIRequestContext
): Promise<number> {
  const res = await api_request_context.post(
    "https://setup.staging.zuddl.io/api/studio/add",
    {
      data: {
        name: "add studio via api",
      },
    }
  );
  console.log("the response is " + (await res.json()));
  const resp_body = await res.json();
  const studio_id = resp_body.studioId;
  console.log(`the studio id is ${resp_body.studioId}`);
  return studio_id;
}

export async function getAccountDetailsForLoggedInContext(
  apiRequestContext: APIRequestContext
) {
  const accountDetailsResp = await apiRequestContext.get(
    "/api/account/details"
  );
  if (accountDetailsResp.status() != 200) {
    throw new Error(
      `API to get account detail failed with ${accountDetailsResp.status()}`
    );
  }

  return accountDetailsResp;
}
export async function getAccountIdForLoggedInContext(
  apiRequestContext: APIRequestContext
) {
  let accountDetailsResp = await getAccountDetailsForLoggedInContext(
    apiRequestContext
  );
  let accountDetailsAPIRespJson = await accountDetailsResp.json();
  return await accountDetailsAPIRespJson["accountId"];
}

export async function getListOfTeamsForOrganisation(
  orgApiRequestContext: APIRequestContext,
  organisationId: string
) {
  const listOfTeamsResp = await orgApiRequestContext.get(
    "/api/core/organization/team"
  );

  if (listOfTeamsResp.status() != 200) {
    throw new Error(
      `API to get list of team for orgid: ${organisationId} failed with ${listOfTeamsResp.status()}`
    );
  }
  return listOfTeamsResp;
}

export async function getDefaultTeamForThisOrganiser(
  orgApiRequestContext: APIRequestContext
) {
  let generalTeamId: string | null = null;
  const accountDetailsAPIResp = await getAccountDetailsForLoggedInContext(
    orgApiRequestContext
  );
  const accountDetailsAPIRespJson = await accountDetailsAPIResp.json();
  const organisationId = accountDetailsAPIRespJson["organizationId"];
  //now get the teams
  const listOfTeamsResp = await getListOfTeamsForOrganisation(
    orgApiRequestContext,
    organisationId
  );
  const listOfTeamsRespJson = await listOfTeamsResp.json();
  //fetch the general team
  const myTeams = listOfTeamsRespJson["myTeams"];
  for (const eachTeam of myTeams) {
    if (eachTeam["general"] == true) {
      generalTeamId = eachTeam["teamId"];
      break;
    }
  }
  if (generalTeamId == null) {
    throw new Error(`No general team found in list of teams for this org`);
  }
  return generalTeamId;
}

export async function createNewEvent({
  api_request_context,
  event_title,
  eventDescription = "This is a test event",
  teamId = null,
  default_settings = true, // start_date = cur date and end date = cur date + 3 days
  sameDayEvent = false,
  add_start_date_from_today = 1, // can be in + to show future date and - to create past events
  add_end_date_from_today = 3, // can be in + to show future date and - to create past events
  timezone = "Asia/Kolkata",
  add_hours_to_end_date = 0,
  add_hours_to_start_date = 0,
  addMinutesToEndDate = 0,
  addMinutesToSatrtDate = 0,
  addSecondsToEndDate = 0,
  addSecondsToStartDate = 0,
  isMagicLinkEnabledAttendee = true,
  isMagicLinkEnabledSpeaker = true,
  isPasswordEnabledAttendee = false,
  isPasswordEnabledSpeaker = false,
  isFlex = false,
  seriesEventId = null,
}: CreateEventParamters): Promise<number> {
  // setting event title
  console.log(
    `##################################### EVENT TITLE ##################### `
  );
  console.log(
    `##################################### ${event_title} ##################### `
  );
  console.log(
    `##################################### EVENT TITLE ##################### `
  );

  //get the team Id first
  let current_date = new Date();
  currentDate = current_date;
  console.log(current_date);
  createEventPayloadData.title = event_title;
  createEventPayloadData.tz = timezone;
  if (sameDayEvent) {
    start_date_from_today = current_date.toISOString();
    console.log(`Event start date is ${start_date_from_today}`);
    end_date_from_today = current_date;
    end_date_from_today = new Date(
      end_date_from_today.setHours(end_date_from_today.getHours() + 6)
    );
    end_date_from_today = end_date_from_today.toISOString();
    console.log(`Event end date is ${end_date_from_today}`);
  } else {
    if (default_settings) {
      start_date_from_today = current_date.toISOString().split(".")[0] + "Z";
      console.log(`Event start date is ${start_date_from_today}`);
      end_date_from_today = current_date;
      end_date_from_today.setDate(end_date_from_today.getDate() + 3);
      end_date_from_today =
        end_date_from_today.toISOString().split(".")[0] + "Z";
      console.log(`Event end date is ${end_date_from_today}`);
    } else {
      // write code to create custom start date and end date
      start_date_from_today = current_date;
      start_date_from_today.setDate(
        start_date_from_today.getDate() + add_start_date_from_today
      );
      start_date_from_today.setHours(
        start_date_from_today.getHours() + add_hours_to_start_date
      );
      start_date_from_today.setMinutes(
        start_date_from_today.getMinutes() + addMinutesToSatrtDate
      );
      start_date_from_today.setSeconds(
        start_date_from_today.getSeconds() + addSecondsToStartDate
      );
      start_date_from_today =
        start_date_from_today.toISOString().split(".")[0] + "Z";
      console.log(`the start date is ${start_date_from_today}`);
      console.log(current_date);
      end_date_from_today = new Date();
      end_date_from_today.setDate(
        end_date_from_today.getDate() + add_end_date_from_today
      );
      end_date_from_today.setHours(
        end_date_from_today.getHours() + add_hours_to_end_date
      );
      end_date_from_today.setMinutes(
        end_date_from_today.getMinutes() + addMinutesToEndDate
      );
      end_date_from_today.setSeconds(
        end_date_from_today.getSeconds() + addSecondsToEndDate
      );
      end_date_from_today =
        end_date_from_today.toISOString().split(".")[0] + "Z";
      console.log(`the end date is ${end_date_from_today}`);
    }
  }
  createEventPayloadData.endDateTime = end_date_from_today;
  createEventPayloadData.startDateTime = start_date_from_today;
  createEventPayloadData.description = eventDescription;
  createEventPayloadData.teamId =
    teamId != null
      ? teamId
      : await getDefaultTeamForThisOrganiser(api_request_context);
  createEventPayloadData.isFlex = isFlex;
  createEventPayloadData.seriesEventId = seriesEventId;

  console.log(inspect(createEventPayloadData, { depth: null }));
  const res = await api_request_context.post("/api/event/", {
    data: createEventPayloadData,
    timeout: 0,
  });
  let res_status = res.status();
  if (res_status != 200) {
    console.log("the response failed with " + (await res.body()));
    throw Error("API Call failed to create event with status " + res_status);
  }
  const resp_body = await res.json();
  const event_id = resp_body.eventId;
  // toggleQuickNetworking(api_request_context, event_id, false);
  // toggleSmartNetworking(api_request_context, event_id, false);

  if (
    isPasswordEnabledAttendee ||
    isPasswordEnabledSpeaker ||
    !isMagicLinkEnabledAttendee ||
    !isMagicLinkEnabledSpeaker
  ) {
    let speakerAuthOptions = ["EMAIL_OTP"];
    if (isPasswordEnabledSpeaker) {
      speakerAuthOptions.push("EMAIL");
    }
    let AttendeeAuthOptions = ["EMAIL_OTP"];
    if (isPasswordEnabledAttendee) {
      speakerAuthOptions.push("EMAIL");
    }

    await updateEventLandingPageDetails(api_request_context, event_id, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: isMagicLinkEnabledAttendee,
      isSpeakermagicLinkEnabled: isMagicLinkEnabledSpeaker,
      isGuestAuthEnabled: false,
      speakerAuthOptions: speakerAuthOptions,
      attendeeAuthOptions: AttendeeAuthOptions,
    }); // Use these to enable disable magic link and password for attendee and speaker
  }
  return event_id;
}

export async function cancelEvent(apiRequestContext, eventId) {
  const response = await apiRequestContext.post(`/api/event/${eventId}/cancel`);

  let responseStatus = response.status();
  if (responseStatus != 200) {
    console.log(
      `Failed to cancel the event (${eventId}) with response - ${responseStatus} || ${await response.body()} `
    );
  }
}

export async function toggleQuickNetworking(
  apiRequestContext: APIRequestContext,
  eventId: string,
  toggle: boolean
) {
  const mode = toggle ? "enable" : "disable";

  const quickNetworkingEndPoint = `/api/networking/${eventId}/${mode}`;

  const response = await apiRequestContext.post(quickNetworkingEndPoint);
  if (response.status() != 200) {
    console.log(
      `Failed to toggle quick networking for event (${eventId}) with response - ${response.status()} || ${await response.body()} `
    );
  }
}

export async function toggleSmartNetworking(
  apiRequestContext: APIRequestContext,
  eventId: string,
  toggle: boolean
) {
  const smartNetworkingEndPoint = `api/smart_networking/${eventId}/settings/update`;
  const response = await apiRequestContext.patch(smartNetworkingEndPoint, {
    data: {
      eventId: eventId,
      isActive: toggle,
      hasInterests: toggle,
      hasServices: toggle,
    },
  });

  if (response.status() != 200) {
    console.log(
      `Failed to toggle smart networking for event (${eventId}) with response - ${response.status()} || ${await response.body()} `
    );
  }
}

interface CreateEventParamters {
  api_request_context: APIRequestContext;
  event_title: string;
  eventDescription?: string;
  default_settings?: boolean; // start_date = cur date and end date = cur date + 3 days
  sameDayEvent?: boolean;
  add_start_date_from_today?: number; // can be in + to show future date and - to create past events
  add_end_date_from_today?: number; // can be in + to show future date and - to create past events
  timezone?: string;
  add_hours_to_start_date?: number;
  add_hours_to_end_date?: number;
  addMinutesToSatrtDate?: number;
  addMinutesToEndDate?: number;
  addSecondsToStartDate?: number;
  addSecondsToEndDate?: number;
  teamId?: string | null;
  isMagicLinkEnabledAttendee?: boolean;
  isMagicLinkEnabledSpeaker?: boolean;
  isPasswordEnabledAttendee?: boolean;
  isPasswordEnabledSpeaker?: boolean;
  isFlex?: boolean;
  seriesEventId?: string | null;
}

export async function getEventDetails(
  api_request_context: APIRequestContext,
  event_id: Number | any
): Promise<APIResponse> {
  const res = await api_request_context.get(
    `/api/event/landing_page/${event_id}`
  );
  if (res.status() != 200) {
    throw new Error(
      `API to fetch event details for ${event_id} failed with ${res.status()}`
    );
  }
  return res;
}

export async function updateEmailRestrictionSettings(
  api_request_context: APIRequestContext,
  eventId,
  landingPageId,
  fileName,
  emailRestrictionType,
  listOfrestrictedEmailDomainsTypes
) {
  let landingPageRes = await getLandingPageDetails(
    api_request_context,
    eventId
  );
  let landingPageResJson = await landingPageRes.json();
  //updating the payload
  landingPageResJson.emailRestrictionType = emailRestrictionType;
  landingPageResJson.restrictedEmailDomainsTypes =
    listOfrestrictedEmailDomainsTypes;
  landingPageResJson.restrictedEmailDomainsFilename = fileName;
  //calling patch with updated payload
  console.log(landingPageResJson.toString());
  let updateEventSettingsEndpoint = `/api/event/landing_page/${eventId}/${landingPageId}`;
  let updateEmailRestrictionResp = await api_request_context.patch(
    updateEventSettingsEndpoint,
    {
      data: landingPageResJson,
    }
  );
  let updateEmailRestrictionRespStatus = updateEmailRestrictionResp.status();
  let updateEmailRestrictionRespBody = await updateEmailRestrictionResp.body();
  if (updateEmailRestrictionRespStatus != 200) {
    console.error(
      `API call to update event restriction failed with ${updateEmailRestrictionRespStatus}`
    );
  }
  console.log(`the resp body is ${updateEmailRestrictionRespBody}`);
}

export async function getLandingPageDetails(
  api_request_context: APIRequestContext,
  event_id: Number | string
): Promise<APIResponse> {
  console.log(`getting landing page detauils for ${event_id}`);
  const res = await api_request_context.get(
    `/api/event/landing_page/${event_id}`
  );
  let status = res.status();
  if (status != 200) {
    console.log(`API failed with ${status}`);
  }
  return res;
}

export async function isEmailRestricted(
  api_request_context: APIRequestContext,
  eventId: number,
  email
) {
  let isEmailRestrictedResp = await api_request_context.get(
    `/api/event/user_registration/${eventId}/verify/email?id=${email}`
  );
  let isEmailRestrictedRespStatus = isEmailRestrictedResp.status();
  let isEmailRestrictedRespBody = await isEmailRestrictedResp.body();
  let isEmailRestrictedRespJson = await isEmailRestrictedResp.json();
  if (isEmailRestrictedRespStatus != 200) {
    console.error(
      `API to check email validity failed with ${isEmailRestrictedRespStatus}`
    );
  }
  if (isEmailRestrictedRespBody.toString() == "false") {
    return false;
  } else if (isEmailRestrictedRespBody.toString() == "true") {
    return true;
  }
  console.log(isEmailRestrictedRespBody);
}

export async function updateEventEntryType(
  api_request_context: APIRequestContext,
  event_id: Number,
  landing_page_id: Number,
  entry_type: EventEntryType
) {
  console.log(`Updating event entry type to ${entry_type}`);
  let landingPageRes = await getLandingPageDetails(
    api_request_context,
    event_id
  );
  let landingPageResJson = await landingPageRes.json();
  landingPageResJson["eventEntryType"] = entry_type;

  const updated_landing_page_res = await api_request_context.patch(
    `/api/event/landing_page/${event_id}/${landing_page_id}`,
    {
      data: landingPageResJson,
    }
  );
  console.log(await updated_landing_page_res.json());
}

export async function getEventInfo(
  api_request_context: APIRequestContext,
  event_id: any
): Promise<APIResponse> {
  const res = await api_request_context.get(`/api/event/${event_id}`);
  return res;
}

export async function updateEventDateTime(
  api_request_context: APIRequestContext,
  event_id: any,
  landing_page_id: Number,
  startDateTime: any = null,
  endDateTime: any = null
) {
  console.log(`Updating event start and end time`);
  let landingPageRes = await getEventInfo(api_request_context, event_id);
  let landingPageResJson = await landingPageRes.json();
  console.log(
    "Old Start Date Time:",
    await landingPageResJson["startDateTime"],
    "Old End Date Time: ",
    await landingPageResJson["endDateTime"]
  );
  if (startDateTime != null) {
    landingPageResJson["startDateTime"] = startDateTime;
    console.log("event Start date time updating");
  }
  if (endDateTime != null) {
    landingPageResJson["endDateTime"] = endDateTime;
    console.log("event end date time updating");
  }
  // console.log("Final landing page json: ",landingPageResJson)
  const updated_landing_page_res = await api_request_context.post(
    `/api/event/${event_id}`,
    {
      data: landingPageResJson,
    }
  );
  if (updated_landing_page_res.status() != 200) {
    console.log(
      `Date and time update failed with ${updated_landing_page_res.status()} with body ${await updated_landing_page_res.body()}.`
    );
    throw new Error(
      `Date and time update failed with ${updated_landing_page_res.status()} with body ${await updated_landing_page_res.body()}.`
    );
  }
  console.log(
    "Date Time updated with status: ",
    updated_landing_page_res.status()
  );
  landingPageRes = await getEventInfo(api_request_context, event_id);
  landingPageResJson = await landingPageRes.json();
  console.log(
    "New Start Date Time:",
    landingPageResJson["startDateTime"],
    "New End Date Time: ",
    landingPageResJson["endDateTime"]
  );
}

export async function enableMagicLinkInEvent(
  api_request_context: APIRequestContext,
  event_id: number,
  landing_page_id: number,
  eventEntryType: EventEntryType
) {
  console.log(`Enabling magic link for event: ${event_id}`);
  let landingPageRes = await getLandingPageDetails(
    api_request_context,
    event_id
  );
  let landingPageResJson = await landingPageRes.json();
  landingPageResJson["magicLinkEnabled"] = true;
  landingPageResJson["eventEntryType"] = eventEntryType;
  const updated_landing_page_res = await api_request_context.patch(
    `/api/event/landing_page/${event_id}/${landing_page_id}`,
    {
      data: landingPageResJson,
    }
  );
  console.log(await updated_landing_page_res.json());
}

export interface eventLandingPageOptions {
  eventEntryType?: EventEntryType;
  isMagicLinkEnabled?: boolean;
  isGuestAuthEnabled?: boolean;
  speakerAuthOptions?;
  attendeeAuthOptions?;
  isSpeakermagicLinkEnabled?;
}

export async function updateEventLandingPageDetails(
  api_request_context: APIRequestContext,
  eventId,
  {
    eventEntryType = EventEntryType.REG_BASED,
    isMagicLinkEnabled = true,
    isSpeakermagicLinkEnabled = true,
    isGuestAuthEnabled = true,
    speakerAuthOptions = [
      "EMAIL",
      "EMAIL_OTP",
      "SSO",
      "FACEBOOK",
      "LINKEDIN",
      "GOOGLE",
    ],
    attendeeAuthOptions = [
      "EMAIL",
      "EMAIL_OTP",
      "SSO",
      "FACEBOOK",
      "LINKEDIN",
      "GOOGLE",
    ],
  }: eventLandingPageOptions
) {
  let landingPageRes = await getLandingPageDetails(
    api_request_context,
    eventId
  );
  let landingPageResJson = await landingPageRes.json();
  let landingPageId = landingPageResJson["eventLandingPageId"];
  //update the landing page res json

  landingPageResJson["eventEntryType"] = eventEntryType;
  landingPageResJson["guestModeEnabled"] = isGuestAuthEnabled;
  landingPageResJson["magicLinkEnabled"] = isMagicLinkEnabled;
  landingPageResJson["attendeeAuthType"] = attendeeAuthOptions;
  landingPageResJson["speakerAuthType"] = speakerAuthOptions;
  landingPageResJson["speakerMagicLinkEnabled"] = isSpeakermagicLinkEnabled;

  console.log("Payload to update settings", landingPageResJson);

  // hit patch api call
  const updated_landing_page_res = await api_request_context.patch(
    `/api/event/landing_page/${eventId}/${landingPageId}`,
    {
      data: landingPageResJson,
    }
  );
  if (updated_landing_page_res.status() != 200) {
    throw new Error(
      `patch api call to update landing page failed with ${updated_landing_page_res.status()}, with message ${await updated_landing_page_res.body()}`
    );
  }
  console.log(
    "Updated landing page details",
    await updated_landing_page_res.json()
  );
}

export async function updateEventAuthOptions({
  speakerAuthOptions = [
    "EMAIL",
    "EMAIL_OTP",
    "SSO",
    "FACEBOOK",
    "LINKEDIN",
    "GOOGLE",
  ],
  attendeeAuthOption = [
    "EMAIL",
    "EMAIL_OTP",
    "SSO",
    "FACEBOOK",
    "LINKEDIN",
    "GOOGLE",
  ],
}) {}

export async function enableGuestAuthForEvent(
  api_request_context: APIRequestContext,
  event_id: number,
  eventEntryType: EventEntryType
) {
  console.log(`Enabling guest link for event: ${event_id}`);
  let landingPageRes = await getLandingPageDetails(
    api_request_context,
    event_id
  );
  let landingPageResJson = await landingPageRes.json();
  let landingPageId = landingPageResJson["eventLandingPageId"];
  landingPageResJson["guestModeEnabled"] = true;
  landingPageResJson["eventEntryType"] = eventEntryType;
  const updated_landing_page_res = await api_request_context.patch(
    `/api/event/landing_page/${event_id}/${landingPageId}`,
    {
      data: landingPageResJson,
    }
  );
  console.log(await updated_landing_page_res.json());
}

export async function enableSpeakerMagicLink(
  api_request_context: APIRequestContext,
  event_id: number,
  landing_page_id: number,
  eventEntryType: EventEntryType
) {
  console.log(`Enabling magic link for event: ${event_id}`);
  let landingPageRes = await getLandingPageDetails(
    api_request_context,
    event_id
  );
  let landingPageResJson = await landingPageRes.json();
  landingPageResJson["speakerMagicLinkEnabled"] = true;
  landingPageResJson["eventEntryType"] = eventEntryType;
  const updated_landing_page_res = await api_request_context.patch(
    `/api/event/landing_page/${event_id}/${landing_page_id}`,
    {
      data: landingPageResJson,
    }
  );
  console.log(await updated_landing_page_res.json());
}

export async function enableAttendeeNSpeakerMagickLinks(
  api_request_context: APIRequestContext,
  event_id: number,
  landing_page_id: number,
  eventEntryType: EventEntryType
) {
  console.log(`Enabling magic link for event: ${event_id}`);
  let landingPageRes = await getLandingPageDetails(
    api_request_context,
    event_id
  );
  let landingPageResJson = await landingPageRes.json();
  landingPageResJson["magicLinkEnabled"] = true;
  landingPageResJson["speakerMagicLinkEnabled"] = true;
  landingPageResJson["eventEntryType"] = eventEntryType;
  const updated_landing_page_res = await api_request_context.patch(
    `/api/event/landing_page/${event_id}/${landing_page_id}`,
    {
      data: landingPageResJson,
    }
  );
  console.log(await updated_landing_page_res.json());
}
export async function fetchAttendeeAccessGroupID(
  api_request_context: APIRequestContext,
  event_id
) {
  console.log(`Trying to find attendee accessgroup id for event ${event_id}`);
  const access_group_list_resp = await api_request_context.get(
    `/api/access_groups/${event_id}/groups`
  );

  let access_group_list_resp_json = await access_group_list_resp.json();
  console.log(
    `The access group list api resp is ${await access_group_list_resp.body()}`
  );

  let access_group_id;

  for (let each_access_obj_index in access_group_list_resp_json) {
    let each_access_group_obj =
      access_group_list_resp_json[each_access_obj_index];

    console.log(`AccessGroup name -> ${each_access_group_obj["groupName"]} \n`);

    if (each_access_group_obj["groupName"] == "Attendees") {
      access_group_id = each_access_group_obj["groupId"];
      break;
    }
  }

  console.log(
    `The access group id fetched for attendee group is ${access_group_id}`
  );

  return access_group_id;
}

export async function fetchSpeakerAccessGroupId(
  api_request_context: APIRequestContext,
  event_id
) {
  console.log(`trying to find speaker  access group id for event ${event_id}`);
  const access_group_list_resp = await api_request_context.get(
    `/api/access_groups/${event_id}/groups`
  );
  let access_group_list_resp_json = await access_group_list_resp.json();
  console.log(
    `the access group list api resp is ${await access_group_list_resp.body()}`
  );
  let access_group_id;
  for (let each_access_obj_index in access_group_list_resp_json) {
    let each_access_group_obj =
      access_group_list_resp_json[each_access_obj_index];
    console.log(`access group name is ${each_access_group_obj["groupName"]}`);
    if (each_access_group_obj["groupName"] == "Speakers") {
      access_group_id = each_access_group_obj["groupId"];
      break;
    }
  }
  console.log(
    `the access group id fetched for speaker group is ${access_group_id}`
  );
  return access_group_id;
}

export async function inviteAttendeeByAPI(
  api_request_context: APIRequestContext,
  event_id,
  attendee_email_add,
  canSendEmail: boolean = true,
  firstName = "Automation",
  lastName = "TestAttendee",
  designation = "QA",
  company = "Zuddl"
) {
  console.log(
    `Trying to add Attendee -> ${attendee_email_add} to Event -> ${event_id}`
  );

  let attendeeAccessGroupId = await fetchAttendeeAccessGroupID(
    api_request_context,
    event_id
  );

  let inviteAttendeeResp = await api_request_context.post(
    `/api/attendee/${event_id}/invite/v3?canSendMail=${canSendEmail}`,
    {
      data: {
        firstName: firstName,
        lastName: lastName,
        email: attendee_email_add,
        ticketTypeId: null,
        designation: designation,
        company: company,
        accessGroups: [{ groupId: attendeeAccessGroupId, isActive: true }],
        canSendConfirmationMail: true,
      },
    }
  );
  return inviteAttendeeResp;
}

export async function addCustomFieldsToEvent(
  api_request_context: APIRequestContext,
  eventId,
  landingPageId,
  customFeildsObj: object
) {
  let infoTabUrl = `/api/event/landing_page/${eventId}/${landingPageId}/information_tab_view`;
  console.log(
    `adding custom feilds object to the event ${eventId} with landing page id ${landingPageId}`
  );
  let infoTabRes = await getInformationTabView(
    api_request_context,
    eventId,
    landingPageId
  );
  let infoTabResJson = await infoTabRes.json();
  infoTabResJson.registrationCustomFields = customFeildsObj;
  let infoTabUpdateRes = await api_request_context.post(infoTabUrl, {
    data: infoTabResJson,
  });
  console.log(
    `the response from infoTabUpdate is ${await infoTabUpdateRes.body()}`
  );
  //check for status code
  if (infoTabRes.status() != 200) {
    throw new Error("Adding custom feilds through api failed");
  }
}

export async function getInformationTabView(
  api_request_context: APIRequestContext,
  eventId,
  landingPageId
) {
  let infoTabUrl = `/api/event/landing_page/${eventId}/${landingPageId}/information_tab_view`;
  let infoTabResp = await api_request_context.get(infoTabUrl);
  return infoTabResp;
}

export async function inviteSpeakerByApi(
  api_request_context: APIRequestContext,
  speakerDataObject: object,
  eventId: string | number,
  canSendEmail: boolean = false
) {
  console.log(
    `Inviting Speaker with email:  ${speakerDataObject["email"]} to ${eventId} by API `
  );
  let inviteSpeakerResp = await api_request_context.post(
    `/api/speaker/${eventId}?canSendMail=${canSendEmail}`,
    {
      data: speakerDataObject,
    }
  );
  let inviteSpeakerRespStatus = inviteSpeakerResp.status();

  if (inviteSpeakerRespStatus != 200) {
    throw new Error(
      `API call to invite speaker ${speakerDataObject["email"]} failed with ${inviteSpeakerRespStatus}`
    );
  }
  // sleep for 15 seconds
  await new Promise((r) => setTimeout(r, 15000));
  let listOfSpeakersResJson = await getListOfSpeakersInEvent(
    api_request_context,
    eventId
  );
  // find the speaker in the list of speakers
  let speakerId = null;
  for (let eachSpeakerObj of listOfSpeakersResJson) {
    if (eachSpeakerObj["email"] == speakerDataObject["email"]) {
      speakerId = eachSpeakerObj["speakerId"];
      break;
    }
  }
  console.log(`the speaker id is ${speakerId}`);
}

export async function updateSpeakerBio(
  api_request_context: APIRequestContext,
  eventId: string,
  speakerEmail: string,
  speakerBio: string
) {
  let speakerId = await getSpeakerIdForEvent(
    api_request_context,
    eventId,
    speakerEmail
  );
  let speakerData = await getSpeakerDataForEvent(
    api_request_context,
    eventId,
    speakerId
  );
  //update speakers bio
  let speakerDataJson = await speakerData.json();
  console.log(`speaker data json is ${speakerDataJson}`);
  speakerDataJson["bio"] = speakerBio;

  // make call to post the update for this speaker
  let updateSpeakerResp = await api_request_context.patch(
    `/api/speaker/${eventId}/${speakerId}`,
    {
      data: speakerDataJson,
    }
  );
  if (updateSpeakerResp.status() != 200) {
    throw Error(
      `API call to update speaker data failed with ${updateSpeakerResp.status()}`
    );
  }
}

export async function getSpeakerDataForEvent(
  api_request_context: APIRequestContext,
  eventId: string,
  speakerId: string
) {
  let speakerData = await api_request_context.get(
    `/api/speaker/${eventId}/${speakerId}`
  );
  if (speakerData.status() != 200) {
    throw new Error(
      `API call to fetch speaker data failed with ${speakerData.status()}`
    );
  }
  return speakerData;
}

export async function deleteSpeakerFromEvent(
  api_request_context: APIRequestContext,
  eventId: string,
  speakerEmail: string
) {
  let speakerId = await getSpeakerIdForEvent(
    api_request_context,
    eventId,
    speakerEmail
  );
  let deleteApiResp = await api_request_context.delete(
    `/api/speaker/${eventId}/${speakerId}`
  );
  if (deleteApiResp.status() != 200) {
    throw new Error(
      `API to delete speaker : ${speakerEmail} failed with ${deleteApiResp.status()}`
    );
  }
}

export async function deleteAllSpeakers(
  api_request_context: APIRequestContext,
  eventId: string
) {
  let listOfSpeakersResJson = await getListOfSpeakersInEvent(
    api_request_context,
    eventId
  );
  console.log(`list of speakers ${listOfSpeakersResJson}`);
  console.log(
    `the length of list of speakers is ${listOfSpeakersResJson.length}`
  );
  let i = 0;
  for (let eachSpeakerObj of listOfSpeakersResJson) {
    let speakerId = eachSpeakerObj["speakerId"];
    let deleteApiResp = await api_request_context.delete(
      `/api/speaker/${eventId}/${speakerId}`
    );
    i++;
    console.log(`deleted ${i} speakers`);
    if (deleteApiResp.status() != 200) {
      throw new Error(
        `API to delete speaker : ${speakerId} failed with ${deleteApiResp.status()}`
      );
    }
  }
}

export async function getSpeakerIdForEvent(
  api_request_context: APIRequestContext,
  eventId: string,
  speakerEmail: string
) {
  let listOfSpeakersResJson = await getListOfSpeakersInEvent(
    api_request_context,
    eventId
  );
  console.log(`list of speakers ${listOfSpeakersResJson}`);
  let speakerId = null;
  for (let eachSpeakerObj of listOfSpeakersResJson) {
    console.log(eachSpeakerObj);
    if (eachSpeakerObj["email"] == speakerEmail) {
      speakerId = eachSpeakerObj["speakerId"];
      break;
    }
  }
  if (speakerId == null) {
    throw new Error(
      `Speaker with email : ${speakerEmail} not found in event : ${eventId}`
    );
  }
  console.log(`speaker id for ${speakerEmail} is ${speakerId}`);
  return speakerId;
}

export async function getListOfSpeakersInEvent(
  api_request_context: APIRequestContext,
  eventId: string | number
) {
  let listSpeakerApiRes = await api_request_context.get(
    `/api/speaker/${eventId}/list/search?limit=300&offset=&q=`
  );
  let listSpeakerApiBody = await listSpeakerApiRes.body();

  if (listSpeakerApiRes.status() != 200) {
    throw new Error(`API to fetch speaker list failed with ${listSpeakerApiRes.status} and body is 
        ${listSpeakerApiBody}`);
  }
  return await listSpeakerApiRes.json();
}

export async function getSignedS3Url(
  api_request_context: APIRequestContext,
  csvFileWithAttendeeRecord
) {
  let S3_SIGNED_PUT_URL = "/api/file-upload/signed_put";
  let filename = DataUtl.getRandomS3FileName();
  filename += ".csv";
  console.log(`the random file name is ${filename}`);
  let resp = await api_request_context.post(S3_SIGNED_PUT_URL, {
    data: filename,
  });
  let respStatus = resp.status();
  let respBody = await resp.json();
  if (respStatus != 200) {
    throw new Error(
      `API call to get s3 signed url failed with ${respStatus} and body is ${respBody}`
    );
  }
  // now get the resp json
  let preSignedUrl = respBody["presignedPutUrl"];
  let renderUrl = respBody["renderUrl"];

  console.log(
    `presigned url is ${preSignedUrl} and render url is ${renderUrl}`
  );

  const file = readFileSync(csvFileWithAttendeeRecord, "utf-8");
  let uploadReq = await api_request_context.put(preSignedUrl, {
    data: file,
  });
  console.log(uploadReq.status());
  console.log(uploadReq);
  return filename;
}

export async function processAttendeeCsvOnEndPoint(
  api_request_context: APIRequestContext,
  eventId,
  fileName
) {
  let mappingUrl = `/api/event/${eventId}/csv/mapping`;
  // let csvFieldMapping="{\"email\":\"email\",\"firstName\":\"firstName\",\"lastName\":\"lastName\",\"headline\":\"headline\",\"phoneNumber\":\"phoneNumber\",\"company\":\"company\",\"designation\":\"designation\",\"bio\":\"bio\",\"country\":\"country\"}"
  let csvMappingObject = {
    email: "email",
    firstName: "firstName",
    lastName: "lastName",
    headline: "headline",
    phoneNumber: "phoneNumber",
    company: "company",
    designation: "designation",
    bio: "bio",
    country: "country",
  };
  let mappingReqPaylod = {
    csvFieldMapping: JSON.stringify(csvMappingObject),
    eventId: eventId,
  };

  let mappingReqResponse = await api_request_context.patch(mappingUrl, {
    data: mappingReqPaylod,
  });
  let mappingReqResponseBody = await mappingReqResponse.json();
  let mappingReqResponseStatus = mappingReqResponse.status();
  if (mappingReqResponseStatus != 200) {
    throw new Error(
      `API call to add mapping failed with ${mappingReqResponseStatus} and body : ${mappingReqResponseBody}`
    );
  }

  let uploadCsvEndPoint = `api/event/${eventId}/attendee/import/v2`;
  let uploadCsvPayload = add_aattendee_csv_payload;
  uploadCsvPayload.s3FileUrl = fileName;
  uploadCsvPayload.canSendMail = true;
  uploadCsvPayload.fileExtension = "csv";
  uploadCsvPayload.ticketTypeId = null;

  // make the api call
  let uploadCsvResp = await api_request_context.post(uploadCsvEndPoint, {
    data: uploadCsvPayload,
  });
  if (uploadCsvResp.status() != 200) {
    throw new Error(
      `API call to upload attendee csv failed with ${uploadCsvResp.status()}`
    );
  }
}

export async function waitForCsvFileProcessingToComplete(
  api_request_context: APIRequestContext,
  eventId,
  fileName
) {
  let bulkRegistrationStatusCheckUrl = `/api/bulk-registration/${eventId}/records?fileUrl=${fileName}`;
  let csvProcessingRespJson;
  await expect
    .poll(
      async () => {
        let csvProcessingStatusResp = await api_request_context.get(
          bulkRegistrationStatusCheckUrl
        );
        csvProcessingRespJson = csvProcessingStatusResp.json();
        return csvProcessingRespJson;
      },
      {
        timeout: 60 * 1000,
        message: "Waiting for csv processing to get completed",
      }
    )
    .toHaveProperty("jobStatus", "COMPLETED");
  return csvProcessingRespJson!;
}

export async function addAttendeeRecordInCsv(
  csvFileToAddDataIn: string,
  attendeeEmail: string
) {
  const csvWriter = require("csv-writer");
  const attendeeCsv = [
    {
      email: attendeeEmail,
      firstName: "prateek",
      lastName: "parashar",
      headline: "QA automation",
      phoneNumber: "9791035986",
      company: "Zuddl",
      title: "QA",
      bio: "Adding bio by automation",
      country: "India",
    },
  ];
  const writer = csvWriter.createObjectCsvWriter({
    path: csvFileToAddDataIn,
    header: [
      { id: "email", title: "email" },
      { id: "firstName", title: "firstName" },
      { id: "lastName", title: "lastName" },
      { id: "headline", title: "headline" },
      { id: "phoneNumber", title: "phoneNumber" },
      { id: "company", title: "company" },
      { id: "title", title: "title" },
      { id: "bio", title: "bio" },
      { id: "country", title: "country" },
    ],
  });
  writer.writeRecords(attendeeCsv).then(() => {
    console.log("Done!");
  });
}

export async function addSpeakerRecordInCsv(
  csvFileToAddDataIn: string,
  speakerEmail: string
): Promise<boolean> {
  const csvWriter = require("csv-writer");
  const speakerCsv = [
    {
      email: speakerEmail,
      firstName: "prateekSpeaker",
      lastName: "Automation",
      phoneNumber: "9791035986",
      company: "Zuddl",
      title: "QA",
      bio: "Adding speaker by automation",
    },
  ];
  const writer = csvWriter.createObjectCsvWriter({
    path: csvFileToAddDataIn,
    header: [
      { id: "email", title: "email" },
      { id: "firstName", title: "firstName" },
      { id: "lastName", title: "lastName" },
      { id: "phoneNumber", title: "phoneNumber" },
      { id: "company", title: "company" },
      { id: "title", title: "title" },
      { id: "bio", title: "bio" },
    ],
  });
  console.log("attempting to write records in csv");
  await writer.writeRecords(speakerCsv);
  console.log(`records in csv are added at ${csvFileToAddDataIn}`);
  // writer.writeRecords(speakerCsv).then(() => {
  //     console.log('writing to csv done!');
  // });
  return true;
}

export async function processSpeakerCsvOnEndPoint(
  api_request_context: APIRequestContext,
  eventId,
  fileName
) {
  const fs = require("fs");
  let speakerCsvUploadEndpoiint = `/api/speaker/${eventId}/import?canSendMail=true`;
  // make the api call
  if (checkIfFileExists(fileName)) {
    const file = readFileSync(fileName, "utf-8");
    let uploadCsvResp = await api_request_context.post(
      speakerCsvUploadEndpoiint,
      {
        multipart: {
          file: fs.createReadStream(fileName),
        },
      }
    );
    if (uploadCsvResp.status() != 200) {
      throw new Error(
        `API call to upload speaker csv failed with ${uploadCsvResp.status()}`
      );
    }
  } else {
    throw new Error(`file ${fileName} not found to process for adding speaker`);
  }
}

export async function registerUserToEventbyApi(
  apiContext: APIRequestContext,
  eventId,
  userEmail,
  raiseError = true
): Promise<APIResponse> {
  let registrationEndPoint = `/api/event/user_registration`;
  console.log(`Registering ${userEmail} to ${eventId} by API `);
  const addUserByRegApiResp = await apiContext.post(registrationEndPoint, {
    data: {
      eventId: eventId,
      firstName: "prateek",
      lastName: "test",
      email: userEmail,
      bio: null,
      designation: null,
      company: null,
      phoneNumber: null,
      country: null,
      customField: '{"disclaimers":[]}',
    },
  });
  if (addUserByRegApiResp.status() != 200 && raiseError) {
    throw new Error(
      `API to add registration via api failed with ${addUserByRegApiResp.status()}`
    );
  }
  return addUserByRegApiResp;
}

export async function registerAttendeeByV1BetaAPI(
  requestContext: APIRequestContext,
  eventId,
  attendeeEmail,
  firstName = "automation",
  lastName = "qa"
) {
  let jwtTokenForV1BetaApi =
    DataUtl.getApplicationTestDataObj()["jwtTokenV1BetaApi"];
  const headers = {
    Authorization: `Bearer ${jwtTokenForV1BetaApi}`,
    Accept: "application/json, text/plain, */*",
    "Content-Type": "application/x-www-form-urlencoded",
    "User-Agent":
      "Zuddl, Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko)",
  };

  const payload = {
    firstName: firstName,
    lastName: lastName,
    email: attendeeEmail,
  };

  let emptyBrowserContext = await BrowserFactory.getBrowserContext({
    laodOrganiserCookies: false,
  });
  let emptyApiRequestContext = emptyBrowserContext.request;

  const v1BetaRegApiRes = await emptyApiRequestContext.post(
    `/api/v1beta/event/${eventId}/register`,
    {
      form: payload,
      headers: headers,
    }
  );

  return v1BetaRegApiRes;
}

export async function updateLandingPageType(
  api_request_context: APIRequestContext,
  event_id: Number | string,
  landing_page_id: Number | string,
  landingPageStyle: LandingPageType,
  isMagicLinkEnabled: string,
  eventEntryType: EventEntryType
) {
  // get landing page details
  console.log(
    `Making API call to update the landing page template to ${landingPageStyle} of event ${event_id}`
  );
  let landingPageRes = await getLandingPageDetails(
    api_request_context,
    event_id
  );
  let landingPageResJson = await landingPageRes.json();
  console.log("Landing page res is  " + landingPageResJson);
  landingPageResJson["landingPageTemplateName"] = landingPageStyle;
  landingPageResJson["magicLinkEnabled"] = isMagicLinkEnabled;
  landingPageResJson["eventEntryType"] = eventEntryType;
  landingPageResJson["speakerMagicLinkEnabled"] = isMagicLinkEnabled;

  const updated_landing_page_res = await api_request_context.patch(
    `/api/event/landing_page/${event_id}/${landing_page_id}`,
    {
      data: landingPageResJson,
    }
  );

  let status = updated_landing_page_res.status();
  if (status != 200) {
    console.log(
      `API failed with status ${status} while trying to  change event entry type`
    );
  }
  console.log(await updated_landing_page_res.json());
}
// Function to get registration ID from email
export async function getRegistrationId(
  apiContext: APIRequestContext,
  eventId,
  userEmail
): Promise<any> {
  let registrationEndPoint = `/api/event/user_registration/${eventId}/list?limit=300&offset=0&q=`;
  console.log(`Fetching ${userEmail} registration ID from ${eventId} by API `);
  let registrationIDofUser = null;

  await expect(async () => {
    let registrationIDs = await (
      await apiContext.get(registrationEndPoint)
    ).json();
    for (const registrationId of registrationIDs) {
      if (registrationId["email"].toLowerCase() == userEmail.toLowerCase()) {
        registrationIDofUser = registrationId["registrationId"];
        break;
      }
    }
    expect(
      registrationIDofUser,
      "expecting registration id to not be null"
    ).not.toBeNull();
  }, "polling get registration record api until we find the given email").toPass();
  return registrationIDofUser;
}

// Function to get all the registered attendees of event
export async function getAllRegistrationIdOfEvent(
  apiContext: APIRequestContext,
  eventId
): Promise<any> {
  let registrationEndPoint = `/api/event/user_registration/${eventId}/list?limit=100&offset=0&q=`;
  console.log(`Fetching  registration IDs from ${eventId} by API `);

  let getRegisteredAttendeeDataApiResponse = await apiContext.get(
    registrationEndPoint
  );
  if (getRegisteredAttendeeDataApiResponse.status() != 200) {
    throw new Error(`No registered ID found in the event.`);
  }

  return await getRegisteredAttendeeDataApiResponse.json();
}

export async function addEventLocationForUser(
  apiContext: APIRequestContext,
  eventId,
  location = EventZones.LOBBY.toUpperCase()
) {
  const addEventLocationApiResp = await apiContext.post(
    `/api/event/${eventId}/location`,
    {
      data: { location: location, subLocation: null, subLocationRefId: null },
    }
  );
  if (addEventLocationApiResp.status() != 200) {
    throw new Error(
      `API call to add event location failed with ${addEventLocationApiResp.status()}`
    );
  }
}
// Function to check in the attendee with registration ID.
export async function checkInAttendee(
  apiContext: APIRequestContext,
  eventId,
  registrationID
) {
  let registrationEndPoint = `/api/event/user_registration/${eventId}/${registrationID}/create_account`;
  console.log(
    `Checking in ${registrationID} registration ID of ${eventId} by API `
  );
  let registrationIDs = await apiContext.post(registrationEndPoint);
  if (registrationIDs.status() != 200) {
    throw new Error(`Unable to check in Attendee`);
  }
  // return registrationIDofUser;
}

export async function deleteAttendeeFromEvent(
  api_request_context: APIRequestContext,
  eventId: string,
  attendeeEmail: string,
  block = true
) {
  let registrationID = await getRegistrationId(
    api_request_context,
    eventId,
    attendeeEmail
  );

  let deleteApiResp = await api_request_context.post(
    `/api/event/user_registration/${eventId}/${registrationID}/deactivate?isBlacklistUser=${block}`
  );
  if (deleteApiResp.status() != 200) {
    throw new Error(
      `API to delete attendee : ${attendeeEmail} failed with ${deleteApiResp.status()}`
    );
  }
}

// Function to delete all speakers from event
export async function deleteAllSpeakerFromEvent(
  api_request_context: APIRequestContext,
  eventId: string
) {
  let speakers = await getListOfSpeakersInEvent(api_request_context, eventId);
  for (const speaker of speakers) {
    let deleteApiResp = await api_request_context.delete(
      `/api/speaker/${eventId}/${speaker["speakerId"]}`
    );
    console.log(`deleting speaker ${speaker["email"]}`);
    if (deleteApiResp.status() != 200) {
      throw new Error(
        `API to delete all speaker failed with ${deleteApiResp.status()}`
      );
    }
  }
}

// Function to delete all attendees from event
export async function deleteAllAttendeesFromEvent(
  api_request_context: APIRequestContext,
  eventId: string
) {
  let attendees = await getAllRegistrationIdOfEvent(
    api_request_context,
    eventId
  );
  for (const attendee of attendees) {
    let deleteApiResp = await api_request_context.post(
      `/api/event/user_registration/${eventId}/${attendee["registrationId"]}/deactivate`
    );
    console.log(`deleting attendee ${attendee["registrationId"]}`);
    if (deleteApiResp.status() != 200) {
      throw new Error(
        `API to delete all attendee failed with ${deleteApiResp.status()}`
      );
    }
  }
}

export async function userVerifyTeamInvitationUsingOtp(
  api_request_context: APIRequestContext,
  userEmail: string
) {
  let verificationId: string;
  await test.step(`Initating otp and get the verification id for user ${userEmail}`, async () => {
    const initiateOtpResp = await initateOtpForStudioViaApi(
      api_request_context,
      userEmail
    );
    const intiateOtpRespJson = await initiateOtpResp.json();
    verificationId = intiateOtpRespJson["otpVerificationId"];
    console.log(`verification id for ${userEmail} is ${verificationId}`);
  });
  return verificationId!;
}

export async function initateOtpForStudioViaApi(
  api_request_context: APIRequestContext,
  userEmail: string
) {
  let otpInitateResp: APIResponse;
  await test.step(`Initaiting otp for ${userEmail} to verify his auth for studio`, async () => {
    otpInitateResp = await api_request_context.post(
      `/api/zuddlotp/initiate?authFor=STUDIO`,
      {
        data: {
          email: userEmail,
        },
      }
    );
    if (otpInitateResp.status() != 200) {
      throw new Error(
        `API call to initiate otp for studio failed with ${otpInitateResp.status()}`
      );
    }
  });
  return otpInitateResp!;
}

export async function userVerifyOtpByApi(
  apiRequestContext: APIRequestContext,
  verificationId,
  otpCode
) {
  let verifyOtpResp: APIResponse;
  await test.step(`Verify otp : ${otpCode} for verification id ${verificationId} by API`, async () => {
    verifyOtpResp = await apiRequestContext.post(
      "/api/zuddlotp/verify?authFor=STUDIO",
      {
        data: {
          otpCode: otpCode,
          verificationId: verificationId,
        },
        headers: {
          "csrf-header": "true",
        },
      }
    );
    if (verifyOtpResp.status() != 200) {
      throw new Error(`Verify otp API failed with ${verifyOtpResp.status()}`);
    }
  });
}

export async function updateEventType(
  api_request_context: APIRequestContext,
  event_id: number,
  eventType: EventType
) {
  console.log(`Updating event type for event: ${event_id} to ${eventType}`);
  let landingPageRes = await api_request_context.get(`/api/event/${event_id}`);
  let landingPageResJson = await landingPageRes.json();
  console.log("landingpage Json: ", await landingPageResJson);
  landingPageResJson["eventFormat"] = eventType;
  const updated_landing_page_res = await api_request_context.post(
    `/api/event/${event_id}`,
    {
      data: landingPageResJson,
    }
  );
  console.log(await updated_landing_page_res.json());
  if (updated_landing_page_res.status() != 200) {
    throw new Error(
      `API to update event type ${updated_landing_page_res.status()}`
    );
  }
}

export async function getResponseFromIntegrationURL(
  api_request_context: APIRequestContext
) {
  let res = await api_request_context.get(
    `/api/core/integration/9c66b321-e273-4966-958e-c469fe67760a/embedded_tray_url`
  );
  console.log(res);
  return res.text();
}

export async function updateStatusFromIntegration(
  api_request_context: APIRequestContext
) {
  let res = await api_request_context.put(
    `/api/core/integration/9c66b321-e273-4966-958e-c469fe67760a/solution_instance/status?solutionStatus=ENABLE`
  );
  return;
}

export async function getRegisterUserData(
  api_request_context: APIRequestContext
) {
  let res = await api_request_context.post(
    `https://dba5743b-d3fa-4d05-8914-ab7518d0d7c5.trayapp.io`,
    //`https://73ca20cb-99ff-40a9-a6b6-85b4aa655dbb.trayapp.io`,
    {
      data: {
        eventIntegrationId: "7015j0000011XlbAAE",
      },
    }
  );
  let resp = await res.json();
  // console.log('-=-=-=->',resp);
  return resp;
}

export async function getMappingData(api_request_context: APIRequestContext) {
  let res = await api_request_context.post(
    `/api/event/landing_page/1032134a-57d9-46a7-80ef-181d204fbe49/6c4deff1-2fc8-450c-9bbe-9e7fcfbd872e/information_tab_view`
  );
  console.log(res);
  return res;
}

export async function getSalesforceInstallationData(
  api_request_context: APIRequestContext
) {
  let res = await api_request_context.get("/api/core/integration");
  return await res.json();
}

export async function getCampainIDVerification(
  api_request_context: APIRequestContext,
  url: string
) {
  let res = await api_request_context.get(url);
  console.log(await res.json());
  return await res.json();
}
