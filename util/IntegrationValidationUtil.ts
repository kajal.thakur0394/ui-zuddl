import { test, expect } from "@playwright/test";
import { RegistrationPayload } from "../controller/EventController";
import {
  HubspotPredefinedRegPayload,
  HubspotRegPayload,
} from "../interfaces/IntegrationControllerInterface";

export class IntegrationValidationUtil {
  static async validateLeadDataFetchedFromTray(
    expectedUserdata: RegistrationPayload,
    userRecordFromTray,
    expectedStatus = "Registered"
  ) {
    const attendeeName = `${expectedUserdata.firstName} ${expectedUserdata.lastName}`;
    await test.step(`verifying the record properties recieved from tray`, async () => {
      expect(
        userRecordFromTray["Name"],
        `expecting name to be ${attendeeName}`
      ).toBe(attendeeName);
      expect(
        userRecordFromTray["LastName"],
        `execting lastname to be ${expectedUserdata.lastName}`
      ).toBe(expectedUserdata.lastName);
      expect(
        userRecordFromTray["FirstName"],
        `execting firstname to be ${expectedUserdata.firstName}`
      ).toBe(expectedUserdata.firstName);
      expect(
        userRecordFromTray["Status"],
        `execting status to be ${expectedStatus}`
      ).toBe(expectedStatus);
      try {
        expect(
          userRecordFromTray["CompanyOrAccount"],
          `expecting companyOrAccount be Zuddl`
        ).toBe("Zuddl");
      } catch (error) {
        console.log("--Inside catch block--");
        expect(
          userRecordFromTray["CompanyOrAccount"],
          `expecting companyOrAccount be Unknown`
        ).toBe("Unknown");
      }

      expect(userRecordFromTray["Type"], `expecting Type to be Lead`).toBe(
        "Lead"
      );
    });
  }
  static async validateLeadDataFetchedFromTray2(
    expectedUserdata: RegistrationPayload,
    userRecordFromTray,
    expectedStatus = "Registered"
  ) {
    const attendeeName = `${expectedUserdata.firstName} ${expectedUserdata.lastName}`;
    await test.step(`verifying the record properties recieved from tray`, async () => {
      expect(
        userRecordFromTray["Name"],
        `expecting name to be ${attendeeName}`
      ).toBe(attendeeName);
      expect(
        userRecordFromTray["LastName"],
        `execting lastname to be ${expectedUserdata.lastName}`
      ).toBe(expectedUserdata.lastName);
      expect(
        userRecordFromTray["FirstName"],
        `execting firstname to be ${expectedUserdata.firstName}`
      ).toBe(expectedUserdata.firstName);
      expect(
        userRecordFromTray["Status"],
        `execting status to be ${expectedStatus}`
      ).toBe(expectedStatus);
      expect(
        userRecordFromTray["CompanyOrAccount"],
        `expecting companyOrAccount be UnKnown`
      ).toBe("UnKnown");
      expect(userRecordFromTray["Type"], `expecting Type to be Lead`).toBe(
        "Lead"
      );
    });
  }

  static async validateAggregateDataFetchedFromTray(
    expectedUserdata: RegistrationPayload,
    userRecordFromTray,
    expectedStatus = "Attended"
  ) {
    const attendeeName = `${expectedUserdata.firstName} ${expectedUserdata.lastName}`;
    let expectedPollMap = new Map<String, String>();
    expectedPollMap.set("Q: Poll 1 [Booth]", "A: option1");
    expectedPollMap.set("Q: Poll 1 [Room]", "A: option2");
    expectedPollMap.set("Q: Poll 1 [Stage]", "A: option1");

    await test.step(`verifying the record properties recieved from tray`, async () => {
      expect(
        userRecordFromTray["Name"],
        `expecting name to be ${attendeeName}`
      ).toBe(attendeeName);
      expect(
        userRecordFromTray["LastName"],
        `execting lastname to be ${expectedUserdata.lastName}`
      ).toBe(expectedUserdata.lastName);
      expect(
        userRecordFromTray["FirstName"],
        `execting firstname to be ${expectedUserdata.firstName}`
      ).toBe(expectedUserdata.firstName);
      expect(
        userRecordFromTray["Status"],
        `execting status to be ${expectedStatus}`
      ).toBe(expectedStatus);
      expect(
        userRecordFromTray["CompanyOrAccount"],
        `expecting companyOrAccount to be Zuddl`
      ).toBe("Zuddl");

      expect(userRecordFromTray["Type"], `expecting Type to be Lead`).toBe(
        "Lead"
      );

      expect(
        userRecordFromTray["Zuddl_Questions_Upvoted__c"],
        `expecting Zuddl_Questions_Upvoted to  be:\nQ: Where is Mumbai located [Room]`
      ).toContain("Q: Where is Mumbai located [Room]");

      let actualPollAnsweredJson =
        userRecordFromTray["Zuddl_Polls_Answered__c"];
      const lines = actualPollAnsweredJson.trim().split("\n");
      const dataMap = new Map();

      for (let i = 0; i < lines.length; i += 2) {
        const key = lines[i].trim();
        const value = lines[i + 1].trim();
        dataMap.set(key, value);
      }
      console.log(dataMap);
      const sortedDataMap = new Map([...dataMap.entries()].sort());
      const sortedExpectedPollAnsweredMap = new Map(
        [...expectedPollMap.entries()].sort()
      );
      // Assert that the sorted maps are equal
      expect(
        sortedDataMap,
        `expecting Zuddl_Polls_Answered to be Q:\n Q: Poll 1 [Booth]\n A: option1\n Q: Poll 1 [Room]\n A: option2\n Q: Poll 1 [Stage]\n A: option1`
      ).toEqual(sortedExpectedPollAnsweredMap);
    });
  }

  static async validateHubspotDataFetchedFromTray(
    expectedUserdata: HubspotRegPayload,
    userRecordFromTray: JSON
  ) {
    await test.step(`verifying the record properties recieved from tray`, async () => {
      expect(
        userRecordFromTray["firstname"],
        `execting firstname to be ${expectedUserdata.firstname}`
      ).toBe(expectedUserdata.firstname);

      expect(
        userRecordFromTray["lastname"],
        `execting lastname to be ${expectedUserdata.lastname}`
      ).toBe(expectedUserdata.lastname);

      expect(
        userRecordFromTray["email"],
        `execting email to be ${expectedUserdata.email}`
      ).toBe(expectedUserdata.email);
    });
  }

  static async validateHubspotPredefinedDataFetchedFromTray(
    expectedUserdata: HubspotPredefinedRegPayload,
    userRecordFromTray: JSON
  ) {
    await test.step(`verifying the record properties recieved from tray`, async () => {
      expect(
        userRecordFromTray["company"],
        `execting company to be ${expectedUserdata.company}`
      ).toBe(expectedUserdata.company);

      expect(
        userRecordFromTray["country"],
        `execting country to be ${expectedUserdata.country}`
      ).toBe(expectedUserdata.country);

      expect(
        userRecordFromTray["mobilephone"],
        `execting mobilephone to be ${expectedUserdata.mobilephone}`
      ).toBe(expectedUserdata.mobilephone);

      expect(
        userRecordFromTray["email"],
        `execting email to be ${expectedUserdata.email}`
      ).toBe(expectedUserdata.email);
    });
  }

  static async validateLeadFieldsFetchedFromTray(
    expectedUserdata: RegistrationPayload,
    userRecordFromTray
  ) {
    const attendeeName = `${expectedUserdata.firstName} ${expectedUserdata.lastName}`;
    expect(
      userRecordFromTray["Name"],
      `expecting name to be ${attendeeName}`
    ).toBe(attendeeName);
    expect(
      userRecordFromTray["LastName"],
      `execting lastname to be ${expectedUserdata.lastName}`
    ).toBe(expectedUserdata.lastName);
    expect(
      userRecordFromTray["FirstName"],
      `execting firstname to be ${expectedUserdata.firstName}`
    ).toBe(expectedUserdata.firstName);
    expect(
      userRecordFromTray["Email"],
      `expecting email to be ${expectedUserdata.email}`
    ).toBe(expectedUserdata.email);
  }

  static async validateHubspotFieldsFetchedFromTray(
    expectedUserdata: RegistrationPayload,
    userRecordFromTray
  ) {
    expect(
      userRecordFromTray["lastname"],
      `execting lastname to be ${expectedUserdata.lastName}`
    ).toBe(expectedUserdata.lastName);
    expect(
      userRecordFromTray["firstname"],
      `execting firstname to be ${expectedUserdata.firstName}`
    ).toBe(expectedUserdata.firstName);
    expect(
      userRecordFromTray["email"],
      `expecting email to be ${expectedUserdata.email}`
    ).toBe(expectedUserdata.email);
  }

  static async validateCustomLeadFieldsFetchedFromTray(
    expectedUserdata: RegistrationPayload,
    userRecordFromTray
  ) {
    await this.validateLeadFieldsFetchedFromTray(
      expectedUserdata,
      userRecordFromTray
    );
    expect(
      userRecordFromTray["Company"],
      `expecting company name to be ${expectedUserdata.company}`
    ).toBe(expectedUserdata.company);
  }
  static async validateCustomLeadHubspotFieldsFetchedFromTray(
    expectedUserdata: RegistrationPayload,
    userRecordFromTray
  ) {
    await this.validateHubspotFieldsFetchedFromTray(
      expectedUserdata,
      userRecordFromTray
    );
    expect(
      userRecordFromTray["company"],
      `expecting company name to be ${expectedUserdata.company}`
    ).toBe(expectedUserdata.company);
  }
}
