# UI Automation using Playwright

This repo contains tests written for events, studio, webinar

# How to setup playwright on your machine?

    Pre-requisite

1. Make sure node is installed on your machine
2. You have access to this repository on github
3. Your public ssh keys are added to github profile here (https://github.com/settings/keys)
   Note: How to generate ssh keys for your machine? https://mdl.library.utoronto.ca/technology/tutorials/generating-ssh-key-pairs-mac

## Steps to setup

1. Navigate to the location where you want to setup your local repository
2. Clone the remote repo master branch hosted on github on to your local using `git clone -b master --single-branch git@github.com:zuddl/studio-playwright-qa.git`
3. Navigate inside the directory `cd playwright-qa`
4. Install dependencies with command `npm install`
5. Install playwright browser executable using `npx playwright install --with-deps`
6. verify playwright is installed using `npm playwright -v` , it should return the version of the playwright installed

# Configure your environment

1. Add a `.env` file to root of the directory
2. Add a key `run_env` with value as the env you want to run test on e.g. staging as `run_env=staging`
3. Environment level data like organiser email, password for staging,pre-prod are stored in file `test-data/applicationData.json`
4. To open browser instance with organiser cookies, you will need to use storage state option when opening new context of a browser and pass cookies of organiser from file `org_cookies.json`
   Note: It has cookies from `staging` and `pre-prod`

# How to run test

1. user command `npx playwright test tests/events/*` , here it tells playwright to run all tests located under folder `tests/events`
2. If you want to run only a particular test, use `test.only` to run only that particular test
3. To see report run `npx playwright show-report`
4. To run test in headed mode, pass `--headed` flag e.g. `npx playwright test tests/events/* --headed`

# Must read resources before starting

1. Core of writing automated script is able to identify an element in DOM using available selector strategies
   a. https://www.guru99.com/locators-in-selenium-ide.html
   b. https://playwright.dev/docs/selectors
2. Assertions in playwright : https://playwright.dev/docs/test-assertions
3. command line options in playwright: https://playwright.dev/docs/test-cli
4. Test configuration : https://playwright.dev/docs/test-configuration
5. Timeouts: https://playwright.dev/docs/test-timeouts
6. Actionability assertions : https://playwright.dev/docs/actionability#assertions
7. How ot debug your test script : https://playwright.dev/docs/debug
8. How to handle iframe : https://playwright.dev/docs/frames
9. How to integrate APIs to playwright :
   a) https://playwright.dev/docs/api/class-apirequestcontext
   b) https://playwright.dev/docs/api/class-apiresponse
10. What is browser in terms of playwright : https://playwright.dev/docs/api/class-browser
11. What is browser context : https://playwright.dev/docs/api/class-browsercontext
12. Page object model
    a. https://playwright.dev/docs/pom

# How to refactor your test into page object mode?

Page object model is a design pattern on principle of sepration of concern, will explain by short example

> Scenario: verify a valid user is able to login into our application
>
> > To write this test, we need to have access to login page and user credentials
> >
> > > Design of login page most likely be : 2 input fields to take email and password and 1 submit button to submit the details

## Starting with raw script

    test('login as a valid user',async({page})=>{
        await page.goTo("https://xyz.com/login")
        await page.locator("input#email").fill("abc@gmail.com")
        await page.locator("input#password").fill("123456")
        await page.locator("button.submit").click()
        await expect(page.url).toBe(/stage/)
    })

### Problems we can observe

1. _Maientenance_ , since many tests will be using same set of steps
   to login, hence if we make any changes to locator , each place we will have to update our code
2. _Readability_, when reading test I am not worried How I login that
   is suppose to be a common set of steps, I am worried about end result that I should land on stage If my credentials were right
3. _Reusability_, since the way I login will most likely we same, it
   makes more sense to abstract it out into a function which just does the act of login

### If we try to implement POM , that means

- each page will be seperate class which will contain
- locator strategy for its elements
- actions a user is supposed to perform on this page, like login is an
  action on login apge, similarly clicking on forgot password etc

### POM way

    // split page elements and action into a seperate class
            export class LoginPage{
                readonly page:Page
                readonly emailInputField:Locator
                readonly passwordInputField:Locator
                readonly submitButton:Locator
                constructor(page:Page){
                    this.page = page
                    this.emailInputField = this.page.locator("input#email")
                    this.passwordInputField = this.page.locator("input#password")
                    this.submitButton = this.page.locator("button.submit)
                }

                //actions
                async load(url:string){
                    await this.page.goTo(url)
                }
                async doLogin(userEmail:string, userPassword:string){
                    await this.emailInputField.fill(userEmail)
                    await this.passwordInputField.fill(userPassword)
                    await this.submitButton.click()
                }

            }
    /*
    now inside test , we will create an instance of this Page class so that we have access to all its actions
    */
            test('login as a valid user',async({page})=>{
                let loginPage = new LoginPage(page)
                await loginPage.load("https://xyz.com/login")
                await loginPage.doLogin("prateek@gmail.com","123456")
                await expect(page.url).toBe(/stage/)
            })
# DB setup on framework 
Go to the link mentioned below:
https://coda.io/d/QA-team-new_dvr_J0NC9gd/Postgres-Database-Integration_suN-S#_luv3r