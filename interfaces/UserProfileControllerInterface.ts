export interface UserProfileControllerInterface {
    editProfileDetailsSettings(userProfileDetails: profileDetailsPayload);
}

export interface profileDetailsPayload {
    privateMessagingEnabled?: boolean,
    privateMeetingsEnabled?: boolean,
    profileForNetworkingEnabled?: boolean
}
