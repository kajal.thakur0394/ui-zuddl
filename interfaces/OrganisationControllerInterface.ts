import ProductType from "../enums/productTypeEnum";
import PlanType from "../enums/planTypeEnum";
import BillngPeriodEnum from "../enums/billingPeriodEnum";
import { MemberRolesEnum } from "../enums/MemberRolesEnum";
import { teamId } from "./TeamControllerInterface";
import { APIResponse } from "@playwright/test";

export interface OrganisationControllerInterface {
  createNewOrganization(
    organisationParams: CreateOrganisationParams
  ): Promise<APIResponse>;
  renewPlanInOrganization(
    updateOrganisationPlanParams: UpdateOrganisationPlanParams
  ): Promise<APIResponse>;
  createATeam(teamCreationParams: CreateTeamParams): Promise<void>;
  addNewMemberToOrganisation(
    addMemberInOrganisationParams: AddMemberInOrganisation
  ): Promise<APIResponse>;
  isMemberPartOfThisOrganisation(memberEmail: string): Promise<boolean>;
}

export interface CreateOrganisationParams {
  organizationName?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  licenseDetailsDto?: LicenseDetailsDto;
}

export interface UpdateOrganisationPlanParams {
  organizationId: string;
  licenseDetailsDto?: LicenseDetailsDto;
}

export interface LicenseDetailsDto {
  productType: ProductType;
  planType: PlanType;
  billingPeriodUnit: BillngPeriodEnum;
  currencyCode?: string;
  eventLicenseDetailsDto?: EventLicenseDetailsDto;
  webinarLicenseDetailsDto?: WebinarLicenseDetailsDto;
}

export interface EventLicenseDetailsDto {
  planStartDate?: string;
  planEndDate?: string;
  teamSize?: number;
  cloudStorageSizeInGb?: number;
  noOfAttendees?: number;
  perEventLengthInHours?: number;
  enabledEventTypes: string[];
}

export interface WebinarLicenseDetailsDto {
  planStartDate?: string;
  planEndDate?: string;
  noOfTeams: number;
  noOfAttendees: number;
  restrictionRenewPeriod: BillngPeriodEnum;
  attendeesBufferPercentage: number;
  cloudStorageSizeInGb: number;
}

export interface CreateTeamParams {
  teamName: string;
  orgMemberIds: string[];
}

export interface AddMemberInOrganisation {
  firstName: string;
  lastName: string;
  email?: string;
  productRole?: MemberRolesEnum;
  myTeamList: teamId[];
}

export interface MemberDetailsInOrganisation {
  email: string;
  productRole: string;
  myTeamNameList?: string[];
  label?: string;
}
