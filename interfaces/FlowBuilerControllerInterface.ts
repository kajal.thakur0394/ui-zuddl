import { APIResponse } from "@playwright/test";
import { DiscountType, FlowStatus, FlowType } from "../enums/FlowTypeEnum";
import EventType from "../enums/eventTypeEnum";

export interface FlowBuilderControllerInterface {
  getFlowData(flowtype: FlowType, flowStatus: FlowStatus): Promise<APIResponse>;
  updateFlowData(
    flowType: FlowType,
    flowStatus: FlowStatus
  ): Promise<APIResponse>;
  publishFlowData(dataPayLoad: publishDataPayload): Promise<APIResponse>;
}

export interface publishDataPayload {
  flowId: string;
  flowDraftId: string;
  clientFlowDraftVersion: number;
}

export interface TicketCreationPayload {
  name?: string;
  description?: string;
  autoGenerateCode?: boolean;
  code?: string;
  numberOfTickets: number;
  pricePerTicket: number;
  minTicketsPerOrder?: number;
  maxTicketsPerOrder?: number;
  isBulkPurchaseEnabled?: boolean;
  isHide?: boolean;
  isLocked?: boolean;
  salesStartDate?: String;
  salesEndDate?: String;
  active?: boolean;
  isAvailableForAll?: boolean;
  isInvitedList?: boolean;
  isInvitedListForAll?: boolean;
  isDomainRestriction?: boolean;
  domainRestrictionCsvFileName?: string;
  audienceGroupIds?: string[];
  domains?: string[];
  ticketTagType?: EventType | string;
  isPaidTicket?: boolean;
  linkedTicketTypeIds?: string[];
}

export interface CouponCreationPayload {
  couponId: null;
  couponCode: string;
  description?: string;
  active: boolean;
  discountType: DiscountType;
  singleUsage?: boolean;
  discountValue: number;
  maxDiscount: number;
  maxUsageCount: number;
  maxTickets: number; //could be string
  ticketTypeIds: string[];
  isAvailableForAllTickets?: boolean;
  isAvailableForEveryone?: boolean;
  isInvitedList?: boolean;
  isInvitedListForAll?: boolean;
  isDomainRestriction?: boolean;
  domainRestrictionCsvFileName?: string;
  audienceGroupIds?: any[];
  domains?: string[];
}
export interface ExpectedTicketDetailsData {
  ticketName?: string;
  ticketPrice?: string;
  dicountValue?: string;
  totalPriceAfterDiscount?: string;
}
