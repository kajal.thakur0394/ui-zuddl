import { HubspotIntegrationDirectionType } from "../enums/SalesforceIntegrationDirectionEnum";
import integrationType from "../enums/integrationTYpeEnum";

export interface IntegrationControllerInterface {}

export interface salesforceFieldMappingPaylod {
  customFieldMapping?;
  customPolicyMapping?;
  secondaryCustomFieldMapping?;
  secondaryCustomPolicyMapping?;
  tertiaryCustomFieldMapping?;
  tertiaryCustomPolicyMapping?;
}

export interface HubspotEnableIntegrationPayload {
  integrationType: integrationType;
  status: boolean;
  integrationDirectionType: HubspotIntegrationDirectionType;
  shouldSyncData: boolean;
}
export interface HubspotRegPayload {
  firstname: string;
  lastname: string;
  email: string;
}

export interface CustomFieldMappingPayload {
  customFieldMapping?;
  customPolicyMapping?;
  secondaryCustomFieldMapping?;
  secondaryCustomPolicyMapping?;
  tertiaryCustomFieldMapping?;
  tertiaryCustomPolicyMapping?;
}

export interface HubspotPredefinedRegPayload {
  country: string;
  company: string;
  mobilephone: string;
  email: string;
}
export interface fieldMappingPayLoad {
  organizationFieldMapping: Record<string, string>;
  organizationPolicyMapping: Record<string, string>;
}
