import { APIResponse } from "@playwright/test";

export interface TeamControllerInterface {
  updateTeamName(newNameString: string): Promise<void>;
  getTeamDetailsByApi(): Promise<APIResponse>;
  addNewMemberToTeam(addNewMemberToTeamObject: addMemberInATeam): Promise<void>;
  getListOfEventsForThisTeam(): Promise<[]>;
  getListOfMembersInThisTeam(): Promise<[TeamMemberInfoDto]>;
  isMemberPartOfThisTeam(memberEmail: string): Promise<boolean>;
  getTeamMemberInfo(memberEmail: string): Promise<TeamMemberInfoDto>;
  addExistingOrganisationMemberToTeam(memberEmail: string): Promise<void>;
}

export interface TeamMemberInfoDto {
  fullName: string;
  email: string;
  picUrl?: null | string;
  firstName: string;
  lastName: string;
  accountId: string;
  phoneNumber?: null | string;
  country?: null | string;
  productRole: string;
  organizationRole: string;
  orgMemberId: string;
  teamMemberId: string;
  invitationStatus: string;
}

export interface addMemberInMultipleTeams {
  teamToBeAdd?: string[];
  teamToBeDelete?: string[];
  orgMemberId: string;
}

export interface addMemberInATeam {
  teamId: string;
  orgMemberIds: string[];
}

export interface addMemberToEventsTeam {
  firstName: string;
  lastName: string;
  email: string;
  studioLicenseRole?;
  studioPlan?;
  eventPlan?;
  eventLicensesHeading?;
  studioLicensesHeading?;
  eventLicenseRole;
  eventProductRole;
}

export interface teamId {
  teamId: string;
}
