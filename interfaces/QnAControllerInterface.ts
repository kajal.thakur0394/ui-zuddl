import { APIResponse } from "@playwright/test";

export interface QnAControllerInterface {
    postAQuestion(questionText: string): Promise<APIResponse>;
    getQuestionList(): Promise<APIResponse>;
    deleteAQuestion(questionText: string): Promise<void>;
    upvoteAQuestion(questionId: string): Promise<string>;
}