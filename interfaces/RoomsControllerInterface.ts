export interface RoomsControllerInterface {
    updateRoomDetails(roomId:string,detailsToUpdate: roomDetailsToBeUpdatedPayload);
}

export interface roomDetailsToBeUpdatedPayload {
    capacity?: number;
    name?: string,
    isLargeRoom?: boolean;
}