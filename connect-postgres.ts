import { Client } from "pg";

export class Query {
  static async connectAndQuery(): Promise<void> {
    const client = new Client({
      user: "postgres",
      host: "localhost",
      database: "masteronline",
      password: "Master76543",
      port: 1111,
      ssl: {
        rejectUnauthorized: false,
      },
    });
    try {
      await client.connect();
      const res = await client.query("SELECT count(*) from public.event;");
      console.log(res.rows[0].count); // Output: Hello world!
    } catch (err) {
      console.error("Error executing PostgreSQL query:", err);
    } finally {
      await client.end();
    }
    // const env: string = process.argv[2];
    // console.log(env);
  }
  static async connectAndQuery2(): Promise<void> {
    console.log("inside query2");
    const client = new Client({
      user: "postgres",
      host: "master-test.cq3uu7nigiuo.ap-south-1.rds.amazonaws.com",
      database: "masteronline",
      password: "Master76543",
      port: 5432,
      ssl: {
        rejectUnauthorized: false,
      },
    });
    try {
      console.log("inside query22");
      await client.connect();
      console.log("inside query222");
      const res = await client.query("SELECT count(*) from public.event;");
      console.log("row count-->" + res.rows[0].count); // Output: Hello world!
    } catch (err) {
      console.error("Error executing PostgreSQL query:", err);
    } finally {
      await client.end();
    }
    // const env: string = process.argv[2];
    // console.log(env);
  }
}
Query.connectAndQuery2();
