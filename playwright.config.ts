import { defineConfig } from "@playwright/test";
import { devices } from "@playwright/test";
import { DataUtl } from "./util/dataUtil";
require("dotenv").config();

function calculateEnvUrl() {
  if (process.env.run_env == "staging") {
    return "https://api.staging.zuddl.io";
  } else if (process.env.run_env == "master") {
    return "https://app.master.zuddl.io";
  } else if (process.env.run_env == "pre-prod") {
    return "https://api.pre-prod.zuddl.io";
  }
}

function getReporters(env: string = "pre-prod") {
  let project = "events-web"; // Default value
  for (let i = 0; i < process.argv.length; i++) {
    if (process.argv[i].startsWith("--project")) {
      const parts = process.argv[i].split("=");
      project = parts.length > 1 ? parts[1] : "events-web";
      break;
    }
  }

  console.log(env);
  console.log(project);
  if (process.env.is_on_local == "1") {
    console.log("Local Run");
    return [["./custom-reporter/slackreporter.ts"], ["html"]];
  } else {
    console;
    const key: string =
      DataUtl.getApplicationTestDataObj()["tesultKeys"][project];
    return [
      ["./custom-reporter/slackreporter.ts"],
      ["html"],
      [
        "playwright-tesults-reporter",
        {
          "tesults-target": key,
        },
      ],
    ] as any;
  }
}

// const config: PlaywrightTestConfig = {
//   testDir: "./tests",
//   /* Maximum time one test can run for. */
//   timeout: 240 * 1000, //4 min
//   expect: {
//     /**
//      * Maximum time expect() should wait for the condition to be met.
//      * For example in `await expect(locator).toHaveText();`
//      */
//     toHaveScreenshot: { maxDiffPixelRatio: 0.15 },
//     timeout: 12000,
//   },
//   /* Run tests in files in parallel */
//   fullyParallel: false,
//   retries: process.env.CI ? 0 : 0,
//   /* Opt out of parallel tests on CI. */
//   /* Reporter to use. See https://playwright.dev/docs/test-reporters */
//   reporter: [["./custom-reporter/slackreporter.ts"], ["html"]],
//   use: {
//     /* Maximum time each action such as `click()` can take. Defaults to 0 (no limit). */
//     actionTimeout: 0, // no timeout,
//     navigationTimeout: 0, // 60 seconds
//     // viewport:{ width: 1400, height: 800 },
//     /* Base URL to use in actions like `await page.goto('/')`. */
//     // baseURL: process.env.run_env=="staging"?"https://app.staging.zuddl.io":"https://app.pre-prod.zuddl.io",
//     baseURL: calculateEnvUrl(),
//     screenshot: "only-on-failure",
//     video: "off",
//     /* Collect trace when retrying the failed test. See https://playwright.dev/docs/trace-viewer */
//     trace: "retain-on-failure",
//     acceptDownloads: true,
//   },
//   //demo commit
//   /* Configure projects for major browsers */
//   projects: [
//     {
//       name: "events-api",
//       use: {
//         ...devices["Stable Chrome"],
//         viewport: { width: 1400, height: 800 },
//         video: "off",
//       },
//       testDir: "tests/events-api",
//     },

//     {
//       name: "events-web",
//       use: {
//         ...devices["Desktop Chrome"],
//         viewport: { width: 1400, height: 800 },
//         video: "off",
//       },

//       testDir: "tests/events/",
//     },

//     {
//       name: "studio-suite",
//       use: {
//         ...devices["Desktop Chrome"],
//         viewport: { width: 1280, height: 720 },
//         video: "off",
//       },
//       testDir: "tests/studio",
//     },
//     {
//       name: "webinar-suite",
//       timeout: 60 * 1000 * 3,
//       expect: {
//         timeout: 20 * 1000,
//       },
//       use: {
//         ...devices["Desktop Chrome"],
//         viewport: { width: 1280, height: 720 },
//         video: "off",
//         // channel: "chrome",
//       },
//       testDir: "tests/webinar",
//     },
//     {
//       name: "integration-suite",
//       timeout: 60 * 1000 * 3,
//       expect: {
//         timeout: 20 * 1000,
//       },
//       use: {
//         ...devices["Desktop Chrome"],
//         viewport: { width: 1280, height: 720 },
//         video: "off",
//         // channel: "chrome",
//       },
//       testDir: "tests/integration",
//     },
//   ],

//   /* Folder for test artifacts such as screenshots, videos, traces, etc. */
//   outputDir: "test-results/",

//   /* Run your local dev server before starting the tests */
//   // webServer: {
//   //   command: 'npm run start',
//   //   port: 3000,
//   // },
// };
// Define the setup function to connect to the database before all tests

export default defineConfig({
  testDir: "./tests",
  fullyParallel: false,
  timeout: 240 * 1000, //4 min
  expect: {
    timeout: 15 * 1000, //15seconds
  },
  retries: process.env.CI ? 1 : 0,
  reporter: getReporters(process.env.run_env),
  use: {
    /* Maximum time each action such as `click()` can take. Defaults to 0 (no limit). */
    actionTimeout: 0, // no timeout,
    navigationTimeout: 0, // 60 seconds
    baseURL: calculateEnvUrl(),
    screenshot: "only-on-failure",
    video: "off",
    /* Collect trace when retrying the failed test. See https://playwright.dev/docs/trace-viewer */
    trace: "on-first-retry",
    acceptDownloads: true,
  },
  projects: [
    {
      name: "generate cookies",
      testDir: "./",
      testMatch: "global.setup.ts",
    },
    {
      name: "cleanup events",
      testDir: "./",
      testMatch: "global.teardown.ts",
    },
    {
      name: "events-api",
      use: {
        ...devices["Stable Chrome"],
        viewport: { width: 1400, height: 800 },
        video: "off",
      },
      testDir: "tests/events-api",
      dependencies: ["generate cookies"],
    },

    {
      name: "events-web",
      use: {
        ...devices["Desktop Chrome"],
        viewport: { width: 1400, height: 800 },
        video: "off",
      },
      testDir: "tests/events",
      teardown: "cleanup events",
      dependencies: ["generate cookies"],
    },
    {
      name: "integration",
      use: {
        ...devices["Desktop Chrome"],
        viewport: { width: 1400, height: 800 },
        video: "off",
      },
      testDir: "tests/integration",
      // teardown: "cleanup events",
      dependencies: ["generate cookies"],
    },
    {
      name: "studio-suite",
      use: {
        ...devices["Desktop Chrome"],
        viewport: { width: 1280, height: 720 },
        video: "off",
      },
      testDir: "tests/studio",
      dependencies: ["generate cookies"],
    },
    {
      name: "webinar",
      timeout: 60 * 1000 * 3,
      expect: {
        timeout: 20 * 1000,
      },
      use: {
        ...devices["Desktop Chrome"],
        viewport: { width: 1280, height: 720 },
        video: "off",
        // channel: "chrome",
      },
      testDir: "tests/webinar",
      dependencies: ["generate cookies"],
    },
    {
      name: "smoke-suite",
      use: {
        ...devices["Desktop Chrome"],
        viewport: { width: 1400, height: 800 },
        video: "off",
      },
      grep: RegExp("@smoke"),
    },
  ],
});
