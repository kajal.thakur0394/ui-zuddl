import EventRole from "../enums/eventRoleEnum";
import { EventInfoDTO } from "./eventInfoDto";
import { UserInfoDTO } from "./userInfoDto";

export class UserEventRoleDto{
    public userInfoDto:UserInfoDTO
    public eventInfoDto:EventInfoDTO
    public userRoleInThisEvent:EventRole
    public _hasRegisteredToThisEvent:boolean

    constructor(userInfoDto:UserInfoDTO,eventInfoDto:EventInfoDTO,userRole:EventRole,hasRegisteredToThisEvent=false){
            this.userInfoDto = userInfoDto
            this.eventInfoDto = eventInfoDto
            this.userRoleInThisEvent = userRole
            this._hasRegisteredToThisEvent=hasRegisteredToThisEvent

    }


    get userRoleForThisEvent(){
        return this.userRoleInThisEvent
    }

    set userRoleForThisEvent(userRole:EventRole){
        this.userRoleInThisEvent = userRole
    }

    get hasUserRegisteredToThisEvent(){
        return this._hasRegisteredToThisEvent
    }
    
    set hasUserRegisteredToThisEvent(isUserRegToEvent:boolean){
        this._hasRegisteredToThisEvent = isUserRegToEvent
    }

    setUserRole(role:EventRole){
        console.log(`Updating event role to ${role}`)
        this.userRoleInThisEvent = role
    }
}