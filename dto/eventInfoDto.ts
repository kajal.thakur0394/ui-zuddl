import EventEntryEnum from "../enums/eventEntryEnum";
import { DataUtl } from "../util/dataUtil";
export class EventInfoDTO {
  public readonly _eventId: string;
  public _eventTitle: string;
  public _eventEntryType: EventEntryEnum;
  public _landingPageId;
  public _isMagicLinkEnabled;
  public _speakerLandingPage: string;
  public _attendeeLandingPage: string;
  public _eventRegMailSubj: string;
  public _otpEmailSubj: string;
  public timeZone: string;
  public startDateTime: any;
  private _dudaPublishedLandingPage: string;

  public constructor(
    eventId,
    eventTitle,
    isMagicLinkEnabled = true,
    timezone: string = "Asia/Kolkata",
    startDateTime = undefined,
    dudaPublishedLandingPage?
  ) {
    this._eventId = eventId;
    this._eventTitle = eventTitle;
    this._isMagicLinkEnabled = isMagicLinkEnabled;
    this.timeZone = timezone;
    this.startDateTime = startDateTime;
  }

  get eventId() {
    return this._eventId;
  }

  get eventTitle() {
    return this._eventTitle;
  }

  set eventEntryType(eventEntryType: EventEntryEnum) {
    this._eventEntryType = eventEntryType;
  }
  get eventEntryType() {
    return this._eventEntryType;
  }

  set landingPageId(landingPageId) {
    console.log(`the new landing page id is ${landingPageId}`);
    this._landingPageId = landingPageId;
  }

  get isMagicLinkEnabled() {
    return this._isMagicLinkEnabled;
  }

  set isMagicLinkEnabled(magicLinkEnabled: boolean) {
    this._isMagicLinkEnabled = magicLinkEnabled;
  }

  get speakerLandingPage() {
    if (this.isMagicLinkEnabled) {
      this._speakerLandingPage = `${
        DataUtl.getApplicationTestDataObj()["baseUrl"]
      }/p/m/event/${this.eventId}`;
    } else {
      this._speakerLandingPage = `${
        DataUtl.getApplicationTestDataObj()["baseUrl"]
      }/p/event/${this.eventId}`;
    }
    console.log(`speaker landing page is ${this._speakerLandingPage}`);
    return this._speakerLandingPage;
  }

  get attendeeLandingPage() {
    if (this.isMagicLinkEnabled) {
      this._attendeeLandingPage = `${
        DataUtl.getApplicationTestDataObj()["baseUrl"]
      }/p/m/a/event/${this.eventId}`;
    }else{
      this._attendeeLandingPage = `${
        DataUtl.getApplicationTestDataObj()["baseUrl"]
      }/p/a/event/${this.eventId}`;
    }
    console.log(`attendee landing page is ${this._attendeeLandingPage}`);
    return this._attendeeLandingPage;
  }

  get registrationConfirmationEmailSubj() {
    this._eventRegMailSubj = `Registration Confirmed • ${this.eventTitle}`;
    return this._eventRegMailSubj;
  }

  getInviteEmailSubj(): string {
    return `Event Invite • ${this.eventTitle}`;
  }

  set setDudaPublishedLandingPage(dudaPublishedLandingPageUrl: string) {
    this._dudaPublishedLandingPage = dudaPublishedLandingPageUrl;
  }

  get getDudaPublishedAttendeeLandingPage() {
    return this._dudaPublishedLandingPage;
  }

  get getDudaPublishedSpeakerLandingPage() {
    return `${this._dudaPublishedLandingPage}?r=s`;
  }

  get rescheduleEmailSubj() {
    return `${this.eventTitle} has been updated`;
  }
}
