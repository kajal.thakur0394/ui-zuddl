import { DataUtl } from "../util/dataUtil";

export class WebinarInfoDto {
  constructor(
    public webinarId,
    public webinarName?,
    public isMagicLinkEnabled?,
    public isGuestLoginEnabled?,
    public listOfAuthOptionsEnabled?
  ) {
    this.webinarId = webinarId;
    this.webinarName = webinarName;
    this.isMagicLinkEnabled = isMagicLinkEnabled;
    this.isGuestLoginEnabled = isGuestLoginEnabled;
    this.listOfAuthOptionsEnabled = listOfAuthOptionsEnabled;
  }

  webinarAttendeeLandingPage(): string {
    //dummy string : https://app.staging.zuddl.io/p/a/event/e30fda34-85d0-4404-97f8-2d0470d5072b
    return `${DataUtl.getApplicationTestDataObj().baseUrl}/p/a/event/${
      this.webinarId
    }`;
  }

  webinarSpeakerLandingPage(): string {
    // dummy string : https://app.staging.zuddl.io/p/event/e30fda34-85d0-4404-97f8-2d0470d5072b
    return `${DataUtl.getApplicationTestDataObj().baseUrl}/p/event/${
      this.webinarId
    }`;
  }
}
