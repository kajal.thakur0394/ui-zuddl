import { UserRegFormData } from "../test-data/userRegForm";

export class UserInfoDTO {
  public _userEmail: string;
  public _userPassword: string;
  public _firstName: string;
  public _lastName: string;
  public _hasSetPassword: boolean;
  public _hasEntryInAccountTable: boolean; // has ever signed up on our platform
  public _userRegFormData: UserRegFormData;

  constructor(
    userEmail: string,
    userPassword = "",
    firstName: string,
    lastName: string,
    hasEntryInAccountTable = false
  ) {
    this._userEmail = userEmail;
    this._userPassword = userPassword;
    this._firstName = firstName;
    this._lastName = lastName;
    // this._hasSetPassword = this.userPassword != "" ? true:false
    this._hasEntryInAccountTable = hasEntryInAccountTable;
    // console.log(`user has passowrd set ${this.hasSetPassword}`)
    this._userRegFormData = new UserRegFormData(userEmail);
  }

  get userEmail() {
    console.log("user email is " + this._userEmail);
    return this._userEmail;
  }

  get userPassword() {
    return this._userPassword;
  }

  set userPassword(password: string) {
    this._userPassword = password;
  }

  get userFirstName() {
    return this._firstName;
  }

  get userLastName() {
    return this._lastName;
  }

  set hasSetPassword(isUserHavePassword: boolean) {
    this._hasSetPassword = isUserHavePassword;
  }

  get hasSetPassword() {
    if (this.userPassword == "") {
      return false;
    }
    return true;
  }

  get hasEntryInAccountTable() {
    return this._hasEntryInAccountTable;
  }

  set hasEntryInAccountTable(hasEntry: boolean) {
    this._hasEntryInAccountTable = hasEntry;
  }

  get userRegFormData() {
    return this._userRegFormData;
  }
}
