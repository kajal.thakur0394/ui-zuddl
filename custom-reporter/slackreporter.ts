import { Reporter } from "@playwright/test/reporter";
import axios from "axios";

require("dotenv").config();
const MY_SLACK_WEBHOOK_URL =
  "https://hooks.slack.com/services/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";
let PROJECTNAME = process.env.JOB_NAME || "Events";
let words = PROJECTNAME.split("-");
let PROJECT_NAME = words
  .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
  .join("-");
let PROJECT_ENV = process.env.PROJECT_ENV || "pre-prod";
let BRANCH_NAME = process.env.BRANCH_NAME || "master";
if (BRANCH_NAME.includes("refs")) {
  BRANCH_NAME = BRANCH_NAME.replace("refs/heads/", "");
}
PROJECT_NAME = PROJECT_NAME.concat("-Regression-Suite");
// startTime and endTime should be in the format 'YYYY-MM-DD HH:MM:SS'
const STARTED_AT = new Date().toLocaleTimeString("en-US", {
  timeZone: "Asia/Kolkata",
});

let ENDED_AT: string;

class slackreporter implements Reporter {
  fail_test = 0;
  pass_test = 0;
  skipped_test = 0;
  timedout_test = 0;
  flaky_test = 0;
  testResultHolder = {};

  onBegin(config, suite) {
    console.log(`Starting the run with ${suite.allTests().length} tests`);
    console.log(`Start time is ${STARTED_AT}`);
  }

  onTestBegin(test) {
    console.log(`Starting test ${test.title}`);
  }

  onTestEnd(test, result) {
    let testId = test.id;
    let testCasePath = test.titlePath();
    console.log(testCasePath);
    console.log(`test case id is ${testId}`);
    // let testCaseIdentifier = `${test.parent.title}-${test.title}`;
    let testCaseIdentifier = testId;
    console.log(`Finished test ${testCaseIdentifier}: ${result.status}`);
    this.testResultHolder[testCaseIdentifier] = result.status;
  }

  async onEnd(result) {
    ENDED_AT = new Date().toLocaleTimeString("en-US", {
      timeZone: "Asia/Kolkata",
    });
    console.log(`End time is ${ENDED_AT}`);
    for (const test_identifier in this.testResultHolder) {
      if (this.testResultHolder[test_identifier] == "passed") {
        this.pass_test = this.pass_test + 1;
      } else if (this.testResultHolder[test_identifier] == "skipped") {
        this.skipped_test = this.skipped_test + 1;
      } else {
        this.fail_test = this.fail_test + 1;
      }
    }

    console.log("sending the slack alert");
    console.log(`project name is ${PROJECT_NAME}`);
    console.log(`environment is ${PROJECT_ENV}`);
    console.log(`branch-name is ${BRANCH_NAME}`);
    console.log(`job-name is ${process.env.JOB_NAME}`);

    let fail_slack_data = {
      blocks: [
        {
          type: "section",
          text: {
            type: "mrkdwn",
            text: `${PROJECT_NAME} report:\n*<https://zuddl-regression-tests-bucket.s3.ap-south-1.amazonaws.com/${process.env.JOB_RUN_ID}/${process.env.JOB_NAME}/index.html|${PROJECT_NAME}>*`,
          },
          accessory: {
            type: "image",
            image_url:
              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSnjn_PeZstqoJ1HZ7T_D8ubhUOc5-R0zkouiu3HQIoTg&s",
            alt_text: "failed test",
          },
        },
        {
          type: "section",
          fields: [
            {
              type: "mrkdwn",
              text: `*Pass Test:*\n${this.pass_test}`,
            },
            {
              type: "mrkdwn",
              text: `*Fail Test:*\n${this.fail_test}`,
            },
            {
              type: "mrkdwn",
              text: `*Skipped Test:*\n${this.skipped_test}`,
            },
            {
              type: "mrkdwn",
              text: "*Browser:*\nChromium",
            },
            {
              type: "mrkdwn",
              text: `*Environment:*\n${PROJECT_ENV}`,
            },
            {
              type: "mrkdwn",
              text: `*Branch:*\n${BRANCH_NAME}`,
            },
            {
              type: "mrkdwn",
              text: `*Started At:*\n${STARTED_AT}`,
            },
            {
              type: "mrkdwn",
              text: `*Ended At:*\n${ENDED_AT}`,
            },
          ],
        },
      ],
    };
    let success_slack_data = {
      blocks: [
        {
          type: "section",
          text: {
            type: "mrkdwn",
            text: `${PROJECT_NAME}:\n*<https://zuddl-regression-tests-bucket.s3.ap-south-1.amazonaws.com/${process.env.JOB_RUN_ID}/${process.env.JOB_NAME}/index.html|${PROJECT_NAME}>*`,
          },
          accessory: {
            type: "image",
            image_url:
              "https://t4.ftcdn.net/jpg/04/61/51/21/360_F_461512195_rBzpceWlGBFjOFr2zSK1Qe5obeY5pRCQ.jpg",
            alt_text: "sucess test",
          },
        },
        {
          type: "section",
          fields: [
            {
              type: "mrkdwn",
              text: `*Pass Test:*\n${this.pass_test}`,
            },
            {
              type: "mrkdwn",
              text: `*Fail Test:*\n${this.fail_test}`,
            },
            {
              type: "mrkdwn",
              text: `*Skipped Test:*\n${this.skipped_test}`,
            },
            {
              type: "mrkdwn",
              text: "*Browser:*\nChromium",
            },
            {
              type: "mrkdwn",
              text: `*Environment:*\n${PROJECT_ENV}`,
            },
            {
              type: "mrkdwn",
              text: `*Branch:*\n${BRANCH_NAME}`,
            },
            {
              type: "mrkdwn",
              text: `*Started At:*\n${STARTED_AT}`,
            },
            {
              type: "mrkdwn",
              text: `*Ended At:*\n${ENDED_AT}`,
            },
          ],
        },
      ],
    };
    if (process.env.is_on_local != "1") {
      if (this.fail_test > 0 || this.timedout_test > 0) {
        const resp = await axios.post(
          MY_SLACK_WEBHOOK_URL,
          JSON.stringify(fail_slack_data)
        );
        console.log("response is " + resp);
      } else {
        const resp = await axios.post(
          MY_SLACK_WEBHOOK_URL,
          JSON.stringify(success_slack_data)
        );
      }
      console.log("slack alert sent");
    }
  }
}
export default slackreporter;
