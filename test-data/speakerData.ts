export class SpeakerData {
    speakerEmail: string
    firstName: string
    lastName: string
    bio: string
    company: string
    designation: string
    speakerId: string

    constructor(speakerEmail: string,
        firstName: string,
        lastName: string = 'automation',
        designation = "QA",
        bio = 'Speaker by API',
        company = 'Zuddl') {
        this.speakerEmail = speakerEmail
        this.firstName = firstName
        this.lastName = lastName
        this.designation = designation
        this.bio = bio
        this.company = company
    }

    getSpeakerDataObject(): object {
        console.log('speakerId', this.speakerId);
        let speakerObj = {
            "firstName": this.firstName,
            "lastName": this.lastName,
            "email": this.speakerEmail,
            "ticketTagType": "VIRTUAL",
            "picUrl": "",
            "designation": this.designation,
            "organization": this.company,
            "bio": this.bio,
            "accessGroups": [

            ],
            "accessGroupMappings": [

            ],
            "speakerId": this.speakerId,
            "canSendConfirmationMail": true,
            "company": this.company
        }
        return speakerObj
    }

    updateSpeakerBio(newBio: string) {
        this.bio = newBio
    }

}