export class UserRegFormData {
  userEmailAdd: string;
  _defaultRegFormData: object; //data which just contains email, fname, lname
  _customFieldFormData: object;
  _conditionalFieldFormData1: object;
  _conditionalFieldFormData2: object;
  customConditionalFieldFormDataeditProfile: object;
  customConditionalFieldFormDataAndeditProfile: object;
  customConditionalFieldFormDataChangeinRegField: object;
  customConditionalFieldAndConidtionFormDataChangeinRegField: object;
  TextconditionalFieldFormDataIsAnswered: object;
  TextconditionalFieldFormDataIsNotAnswered: object;
  NumericconditionalFieldFormDataIsAnswered: object;
  NumericconditionalFieldFormDataIsNotAnswered: object;
  DropdownconditionalFieldFormDataIsselected: object;
  DropDownconditionalFieldFormDataIsNotSelected: object;
  MultiSelectconditionalFieldFormDataAllselected: object;
  MultiSelectconditionalFieldFormDataAnySelected: object;
  nestedconditionalFieldFormDataRemmovingmultselectcondition: object;
  nestedconditionalFieldFormDataRemmovingTextCondition: object;
  predefinedFieldsFormData: object;
  predefinedFieldsFormDataForEmbed: object;
  predefinedReRegisterFieldsFormData: object;
  predefinedReRegisterFieldsVerificationFormData: object;
  predefinedFieldsFormDataEditProfile: object;
  predefinedFieldsFormDataForSalesforcePushCase: object;
  predefinedFieldsFormDataForHubspotPushCase: object;
  predefinedAndCustomFieldsFormDataForSalesforcePushCase: object;

  constructor(userEmailAdd: string) {
    this.userEmailAdd = userEmailAdd;
    // setting up default form data for test case usage
    this._defaultRegFormData = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
      },
    };
    // setting up custom field form data for test usage
    this._customFieldFormData = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      phone: {
        label: "phone",
        fieldType: "number",
        dataToEnter: "1234567899",
        isOptionalField: true,
      },
      city: {
        label: "city",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["Delhi", "Chennai"],
        isOptionalField: false,
      },
      "Normal dropdown": {
        label: "Normal dropdown",
        fieldType: "dropdown",
        dataToEnter: "adasd",
        isOptionalField: true,
      },
    };

    // Setting up Conditional field.
    this._conditionalFieldFormData1 = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      "Which city has the best food?": {
        label: "Which city has the best food?",
        fieldType: "dropdown",
        dataToEnter: "Delhi",
        isOptionalField: false,
      },

      // This field comes when you have selected delhi.
      "Please rate Delhi on Scale of 0-10?": {
        label: "Please rate Delhi food on Scale of 0-10?",
        fieldType: "number",
        dataToEnter: "8",
        isOptionalField: false,
      },

      // Comes after rating delhi
      "Why have you rated Delhi food food this much?": {
        label: "Why have you rated Delhi food this much?",
        fieldType: "text",
        dataToEnter: "It deserves this much",
        isOptionalField: false,
        isConditionaField: true,
      },

      "Your best sweet dish combination?": {
        label: "Your best sweet dish combination?",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["Rabri", "Jalebi"],
        isOptionalField: false,
      },

      // This comes when you have selected rabri jalebi.
      "Have you tried Poha Jalebi?": {
        label: "Have you tried Poha Jalebi?",
        fieldType: "dropdown",
        dataToEnter: "Yes",
        isOptionalField: false,
        isConditionaField: true,
      },

      // This comes when you have selected Yes in Poha Jalebi question.
      "Why is rabri Jalebi better than Poha jalebi?": {
        label: "Why is rabri Jalebi better than Poha jalebi?",
        fieldType: "text",
        dataToEnter: "Just like it that way.",
        isOptionalField: true,
        isConditionaField: true,
      },

      "Name the best country you ever visited?": {
        label: "Name the best country you ever visited?",
        fieldType: "text",
        dataToEnter: "India",
        isOptionalField: true,
      },

      "Select the covid vaccine doses you have got?": {
        label: "Select the covid vaccine doses you have got?",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["1st", "2nd"],
        isOptionalField: false,
      },

      // This comes when you select any of the options of covid vaccine.
      "Do you believe in vaccine makers?": {
        label: "Do you believe in vaccine makers?",
        fieldType: "dropdown",
        dataToEnter: "No",
        isOptionalField: true,
        isConditionaField: true,
      },

      // This comes when you select No in believe in vaccine makers.
      "Then why are you taking vaccines?": {
        label: "Then why are you taking vaccines?",
        fieldType: "text",
        dataToEnter: "Because mandated by government",
        isOptionalField: false,
        isConditionaField: true,
      },
    };

    this.customConditionalFieldFormDataeditProfile = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      city: {
        label: "Which city has the best food?",
        fieldType: "dropdown",
        dataToEnter: "Chennai",
        isOptionalField: false,
      },
      // Comes after Selecting any option other than delhi
      "Why this city has best food?": {
        label: "Why this city has best food?",
        fieldType: "text",
        dataToEnter: "It deserves this much",
        isOptionalField: false,
        isConditionaField: true,
      },

      "Your best sweet dish combination?": {
        label: "Your best sweet dish combination?",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["Rabri", "Jalebi"],
        isOptionalField: false,
      },

      // This comes when you have selected rabri jalebi.
      "Have you tried Poha Jalebi?": {
        label: "Have you tried Poha Jalebi?",
        fieldType: "dropdown",
        dataToEnter: "Yes",
        isOptionalField: false,
        isConditionaField: true,
      },

      // This comes when you have selected Yes in Poha Jalebi question.
      "Why is rabri Jalebi better than Poha jalebi?": {
        label: "Why is rabri Jalebi better than Poha jalebi?",
        fieldType: "text",
        dataToEnter: "Just like it that way.",
        isOptionalField: true,
        isConditionaField: true,
      },

      "Name the best country you ever visited?": {
        label: "Name the best country you ever visited?",
        fieldType: "text",
        dataToEnter: "India",
        isOptionalField: true,
      },

      "Select the covid vaccine doses you have got?": {
        label: "Select the covid vaccine doses you have got?",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["1st", "2nd"],
        isOptionalField: false,
      },

      // This comes when you select any of the options of covid vaccine.
      "Do you believe in vaccine makers?": {
        label: "Do you believe in vaccine makers?",
        fieldType: "dropdown",
        dataToEnter: "No",
        isOptionalField: true,
        isConditionaField: true,
      },

      // This comes when you select No in believe in vaccine makers.
      "Then why are you taking vaccines?": {
        label: "Then why are you taking vaccines?",
        fieldType: "text",
        dataToEnter: "Because mandated by government",
        isOptionalField: false,
        isConditionaField: true,
      },
    };

    this.nestedconditionalFieldFormDataRemmovingmultselectcondition = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      "Who is your inspiration? ": {
        label: "Who is your inspiration? ",
        fieldType: "text",
        dataToEnter: "Ms Dhoni",
        isOptionalField: false,
      },
      Tellmeaboutyourself: {
        label: "Tell me about yourself",
        fieldType: "text",
        dataToEnter: "I am Ankur.",
        isOptionalField: false,
      },
      "Do you watch sports?": {
        label: "Do you watch sports?",
        fieldType: "dropdown",
        dataToEnter: "Yes",
        isOptionalField: false,
        isConditionaField: true,
      },
      //Comes after fiiling watch sports
      "Select your fav sport?": {
        label: "Select your fav sport?",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["BasketBall", "Rugby"],
        isOptionalField: false,
        isConditionaField: true,
      },
      "Do you watch American leagues?": {
        label: "Do you watch American leagues?",
        fieldType: "text",
        dataToEnter: "Yes",
        isOptionalField: false,
        isConditionaField: true,
      },
      "Tell me about yourself": {
        label: "Tell me about yourself",
        fieldType: "text",
        dataToEnter: "",
        isOptionalField: false,
      },
    };
    this.customConditionalFieldFormDataAndeditProfile = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      city: {
        label: "Which city has the best food?",
        fieldType: "dropdown",
        dataToEnter: "Chennai",
        isOptionalField: false,
      },
      // Comes after Selecting any option other than delhi
      "Why this city has best food?": {
        label: "Why this city has best food?",
        fieldType: "text",
        dataToEnter: "It deserves this much",
        isOptionalField: false,
        isConditionaField: true,
      },

      "Your best sweet dish combination?": {
        label: "Your best sweet dish combination?",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["Rabri", "Jalebi"],
        isOptionalField: false,
      },

      // This comes when you have selected rabri jalebi.
      "Have you tried Poha Jalebi?": {
        label: "Have you tried Poha Jalebi?",
        fieldType: "dropdown",
        dataToEnter: "Yes",
        isOptionalField: false,
        isConditionaField: true,
      },

      // This comes when you have selected Yes in Poha Jalebi question.
      "Why is rabri Jalebi better than Poha jalebi?": {
        label: "Why is rabri Jalebi better than Poha jalebi?",
        fieldType: "text",
        dataToEnter: "Just like it that way.",
        isOptionalField: true,
        isConditionaField: true,
      },

      "Name the best country you ever visited?": {
        label: "Name the best country you ever visited?",
        fieldType: "text",
        dataToEnter: "India",
        isOptionalField: true,
      },

      "Select the covid vaccine doses you have got?": {
        label: "Select the covid vaccine doses you have got?",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["3rd"],
        isOptionalField: false,
      },
    };

    this.customConditionalFieldFormDataChangeinRegField = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      "Which city has the best food?": {
        label: "Which city has the best food?",
        fieldType: "dropdown",
        dataToEnter: "Delhi",
        isOptionalField: false,
      },

      // This field comes when you have selected delhi.
      "Please rate Delhi on Scale of 0-10?": {
        label: "Please rate Delhi food on Scale of 0-10?",
        fieldType: "number",
        dataToEnter: "8",
        isOptionalField: false,
      },

      city: {
        label: "Which city has the best food?",
        fieldType: "dropdown",
        dataToEnter: "Chennai",
        isOptionalField: false,
      },
      // Comes after Selecting any option other than delhi
      "Why this city has best food?": {
        label: "Why this city has best food?",
        fieldType: "text",
        dataToEnter: "It deserves this much",
        isOptionalField: false,
        isConditionaField: true,
      },

      "Your best sweet dish combination?": {
        label: "Your best sweet dish combination?",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["Rabri", "Jalebi"],
        isOptionalField: false,
      },

      // This comes when you have selected rabri jalebi.
      "Have you tried Poha Jalebi?": {
        label: "Have you tried Poha Jalebi?",
        fieldType: "dropdown",
        dataToEnter: "Yes",
        isOptionalField: false,
        isConditionaField: true,
      },

      // This comes when you have selected Yes in Poha Jalebi question.
      "Why is rabri Jalebi better than Poha jalebi?": {
        label: "Why is rabri Jalebi better than Poha jalebi?",
        fieldType: "text",
        dataToEnter: "Just like it that way.",
        isOptionalField: true,
        isConditionaField: true,
      },

      "Name the best country you ever visited?": {
        label: "Name the best country you ever visited?",
        fieldType: "text",
        dataToEnter: "India",
        isOptionalField: true,
      },

      "Select the covid vaccine doses you have got?": {
        label: "Select the covid vaccine doses you have got?",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["1st", "2nd"],
        isOptionalField: false,
      },

      // This comes when you select any of the options of covid vaccine.
      "Do you believe in vaccine makers?": {
        label: "Do you believe in vaccine makers?",
        fieldType: "dropdown",
        dataToEnter: "No",
        isOptionalField: true,
        isConditionaField: true,
      },

      // This comes when you select No in believe in vaccine makers.
      "Then why are you taking vaccines?": {
        label: "Then why are you taking vaccines?",
        fieldType: "text",
        dataToEnter: "Because mandated by government",
        isOptionalField: false,
        isConditionaField: true,
      },
    };

    this.customConditionalFieldAndConidtionFormDataChangeinRegField = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      "Which city has the best food?": {
        label: "Which city has the best food?",
        fieldType: "dropdown",
        dataToEnter: "Delhi",
        isOptionalField: false,
      },

      // This field comes when you have selected delhi.
      "Please rate Delhi on Scale of 0-10?": {
        label: "Please rate Delhi food on Scale of 0-10?",
        fieldType: "number",
        dataToEnter: "8",
        isOptionalField: false,
      },

      city: {
        label: "Which city has the best food?",
        fieldType: "dropdown",
        dataToEnter: "Chennai",
        isOptionalField: false,
      },
      // Comes after Selecting any option other than delhi
      "Why this city has best food?": {
        label: "Why this city has best food?",
        fieldType: "text",
        dataToEnter: "It deserves this much",
        isOptionalField: false,
        isConditionaField: true,
      },

      "Your best sweet dish combination?": {
        label: "Your best sweet dish combination?",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["Rabri", "Jalebi"],
        isOptionalField: false,
      },

      // This comes when you have selected rabri jalebi.
      "Have you tried Poha Jalebi?": {
        label: "Have you tried Poha Jalebi?",
        fieldType: "dropdown",
        dataToEnter: "Yes",
        isOptionalField: false,
        isConditionaField: true,
      },

      // This comes when you have selected Yes in Poha Jalebi question.
      "Why is rabri Jalebi better than Poha jalebi?": {
        label: "Why is rabri Jalebi better than Poha jalebi?",
        fieldType: "text",
        dataToEnter: "Just like it that way.",
        isOptionalField: true,
        isConditionaField: true,
      },

      "Name the best country you ever visited?": {
        label: "Name the best country you ever visited?",
        fieldType: "text",
        dataToEnter: "India",
        isOptionalField: true,
      },

      "Select the covid vaccine doses you have got?": {
        label: "Select the covid vaccine doses you have got?",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["1st", "3rd"],
        isOptionalField: false,
      },

      // This comes when you select any of the options of covid vaccine.
      "Do you believe in vaccine makers?": {
        label: "Do you believe in vaccine makers?",
        fieldType: "dropdown",
        dataToEnter: "No",
        isOptionalField: true,
        isConditionaField: true,
      },

      // This comes when you select No in believe in vaccine makers.
      "Then why are you taking vaccines?": {
        label: "Then why are you taking vaccines?",
        fieldType: "text",
        dataToEnter: "Because mandated by government",
        isOptionalField: false,
        isConditionaField: true,
      },
      "Repeating Select the covid vaccine doses you have got?": {
        label: "Select the covid vaccine doses you have got?",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["1st"],
        isOptionalField: false,
      },
    };

    this._conditionalFieldFormData2 = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      city: {
        label: "Which city has the best food?",
        fieldType: "dropdown",
        dataToEnter: "Chennai",
        isOptionalField: false,
      },
      // Comes after Selecting any option other than delhi
      "Why this city has best food?": {
        label: "Why this city has best food?",
        fieldType: "text",
        dataToEnter: "It deserves this much",
        isOptionalField: false,
        isConditionaField: true,
      },

      // This comes when you not answer the best country you have visited.
      "Write any country you have visited?": {
        label: "Write any country you have visited?",
        fieldType: "text",
        dataToEnter: "India only.",
        isOptionalField: false,
        isConditionaField: true,
      },
    };

    //Text based conditional field, testing is answered condition.
    this.TextconditionalFieldFormDataIsAnswered = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      "What is your fav city?": {
        label: "What is your fav city?",
        fieldType: "text",
        dataToEnter: "Ranchi",
        isOptionalField: true,
      },
      // Comes after fiiling fav city
      "Why this is your fav city?": {
        label: "Why this is your fav city?",
        fieldType: "text",
        dataToEnter: "I lived and studied here.",
        isOptionalField: false,
        isConditionaField: true,
      },

      // This comes when you not answer the best country you have visited.
      "How do you rate this city?": {
        label: "How do you rate this city?",
        fieldType: "text",
        dataToEnter: "7",
        isOptionalField: true,
        isConditionaField: true,
      },
    };

    //Text based conditional field, testing is not answered condition.
    this.TextconditionalFieldFormDataIsNotAnswered = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      // Comes after not fiiling fav city
      "Tell me any city if not telling fav? ": {
        label: "Tell me any city if not telling fav? ",
        fieldType: "text",
        dataToEnter: "I lived and studied here.",
        isOptionalField: false,
        isConditionaField: true,
      },
    };

    //Numeric based conditional field, testing is answered condition.
    this.NumericconditionalFieldFormDataIsAnswered = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      "What is your fav number?": {
        label: "What is your fav number?",
        fieldType: "number",
        dataToEnter: "7",
        isOptionalField: true,
      },
      // Comes after fiiling fav number
      "Why this is your fav number?": {
        label: "Why this is your fav number?",
        fieldType: "text",
        dataToEnter: "The Jeresey no of MS Dhoni.",
        isOptionalField: false,
        isConditionaField: true,
      },

      // This comes when you  answer why this your fav no.
      "tell me more about this number?": {
        label: "tell me more about this number?",
        fieldType: "text",
        dataToEnter:
          "I just like this number as I also live in ranchi same as Dhoni.",
        isOptionalField: true,
        isConditionaField: true,
      },
    };

    //Numeric based conditional field, testing is not answered condition.
    this.NumericconditionalFieldFormDataIsNotAnswered = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },

      // This comes when you not  answer what is your fav no.
      "Tell me your lucky  number?": {
        label: "Tell me your lucky  number?",
        fieldType: "number",
        dataToEnter: "8",
        isOptionalField: true,
      },
    };

    //DropDown based conditional field, testing is selected condition.
    this.DropdownconditionalFieldFormDataIsselected = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      "Select your fav sport?": {
        label: "Select your fav sport?",
        fieldType: "dropdown",
        dataToEnter: "football",
        isOptionalField: false,
      },
      //Comes after fiiling fav sport
      "Select your fav foot ball player": {
        label: "Select your fav foot ball player",
        fieldType: "dropdown",
        dataToEnter: "Ronaldo",
        isOptionalField: false,
        isConditionaField: true,
      },

      // This comes when you answer Rnaldo and football.
      "Why Ronaldo is better then Messi?": {
        label: "Why Ronaldo is better then Messi?",
        fieldType: "text",
        dataToEnter: "I just like Ronaldo.",
        isOptionalField: true,
        isConditionaField: true,
      },
    };

    //DropDown based conditional field, testing is selected condition.
    this.DropDownconditionalFieldFormDataIsNotSelected = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      "Select your fav sport?": {
        label: "Select your fav sport?",
        fieldType: "dropdown",
        dataToEnter: "cricket",
        isOptionalField: false,
      },
      //Comes after fiiling fav sport as not football
      "Select your fav foot ball player": {
        label: "Select your fav cricket player",
        fieldType: "dropdown",
        dataToEnter: "Dhoni",
        isOptionalField: false,
        isConditionaField: true,
      },

      // This comes when you answer Dhoni and cricket.
      "Why Ronaldo is better then Messi?": {
        label: "Why Dhoni is better then Virat?",
        fieldType: "text",
        dataToEnter: "Dhoni has won all ICC trophies and 4 IPL trophies.",
        isOptionalField: true,
        isConditionaField: true,
      },
    };

    // Multi select DropDown based conditional field, testing is selected condition.
    this.MultiSelectconditionalFieldFormDataAllselected = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      "select your fav countries?": {
        label: "select your fav countries?",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["Italy", "China"],
        isOptionalField: false,
      },
      //Comes after fiiling fav countries
      "Select your fav cuisine out of Italy and China": {
        label: "Select your fav cuisine out of Italy and China",
        fieldType: "dropdown",
        dataToEnter: "China",
        isOptionalField: false,
        isConditionaField: true,
      },
    };

    // Multi select DropDown based conditional field, testing is selected condition.
    this.MultiSelectconditionalFieldFormDataAnySelected = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      "select your fav countries?": {
        label: "select your fav countries?",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["India"],
        isOptionalField: false,
      },
      //Comes after fiiling fav countries
      "Which place you would like to visit?": {
        label: "Which place you would like to visit?",
        fieldType: "dropdown",
        dataToEnter: "Taj mahal(India)",
        isOptionalField: false,
        isConditionaField: true,
      },
    };

    this.nestedconditionalFieldFormDataRemmovingTextCondition = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      "Who is your inspiration? ": {
        label: "Who is your inspiration? ",
        fieldType: "text",
        dataToEnter: "Ms Dhoni",
        isOptionalField: false,
      },
      Tellmeaboutyourself: {
        label: "Tell me about yourself",
        fieldType: "text",
        dataToEnter: "I am Ankur.",
        isOptionalField: false,
      },
      "Do you watch sports?": {
        label: "Do you watch sports?",
        fieldType: "dropdown",
        dataToEnter: "Yes",
        isOptionalField: false,
        isConditionaField: true,
      },
      //Comes after fiiling watch sports
      "Select your fav sport?": {
        label: "Select your fav sport?",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["BasketBall", "Rugby"],
        isOptionalField: false,
        isConditionaField: true,
      },
      "Do you watch American leagues?": {
        label: "Do you watch American leagues?",
        fieldType: "text",
        dataToEnter: "Yes",
        isOptionalField: false,
        isConditionaField: true,
      },
      "Who is yourinspiration? ": {
        label: "Who is your inspiration? ",
        fieldType: "text",
        dataToEnter: "",
        isOptionalField: false,
      },
    };

    this.predefinedFieldsFormData = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      phoneNumber: {
        label: "Phone Number",
        fieldType: "text",
        dataToEnter: "7832732873",
        isOptionalField: false,
      },
      title: {
        label: "Title",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      company: {
        label: "Company",
        fieldType: "text",
        dataToEnter: "Zuddl",
        isOptionalField: false,
      },
      bio: {
        label: "Bio",
        fieldType: "text",
        dataToEnter: "Simple human.",
        isOptionalField: false,
      },
      country: {
        label: "Country",
        fieldType: "text",
        dataToEnter: "India",
        isOptionalField: false,
      },
      "Write any cntry you have visited?": {
        label: "Write any cntry you have visited?",
        fieldType: "text",
        dataToEnter: "India",
        isOptionalField: false,
      },
      "Please rate delhi on Scale of 0-10?": {
        label: "Please rate Delhi on Scale of 0-10?",
        fieldType: "number",
        dataToEnter: "8",
        isOptionalField: false,
      },
      "Do you watch sport?": {
        label: "Do you watch sport?",
        fieldType: "dropdown",
        dataToEnter: "Yes",
        isOptionalField: true,
      },
      "Please select covid vaccine doses you've got?": {
        label: "Please select covid vaccine doses you've got?",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["1st", "2nd"],
        isOptionalField: true,
        isConditionaField: true,
      },
    };

    this.predefinedFieldsFormDataForEmbed = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      phoneNumber: {
        label: "Phone number",
        fieldType: "text",
        dataToEnter: "7832732873",
        isOptionalField: false,
      },
      title: {
        label: "Title",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      company: {
        label: "company",
        fieldType: "text",
        dataToEnter: "Zuddl",
        isOptionalField: false,
      },
      bio: {
        label: "bio",
        fieldType: "text",
        dataToEnter: "Simple human.",
        isOptionalField: false,
      },
      country: {
        label: "countryembed",
        fieldType: "dropdown",
        dataToEnter: "India",
        isOptionalField: false,
      },
      "Do you watch sport?": {
        label: "Do you watch sport?",
        fieldType: "dropdown",
        dataToEnter: "Yes",
        isOptionalField: true,
      },
      "Write any cntry you have visited?": {
        label: "Write any cntry you have visited?",
        fieldType: "text",
        dataToEnter: "India",
        isOptionalField: false,
      },
      "Please rate delhi on Scale of 0-10?": {
        label: "Please rate Delhi on Scale of 0-10?",
        fieldType: "number",
        dataToEnter: "8",
        isOptionalField: false,
      },
      "Please select covid vaccine doses you've got?": {
        label: "Please select covid vaccine doses you've got?",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["1st", "2nd"],
        isOptionalField: true,
        isConditionaField: true,
      },
    };

    this.predefinedReRegisterFieldsFormData = {
      extraMandatoryField: {
        label: "extraMandatoryField",
        fieldType: "dropdown",
        dataToEnter: "chooseme",
        isOptionalField: false,
        isConditionaField: true,
      },
    };

    this.predefinedReRegisterFieldsVerificationFormData = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      phoneNumber: {
        label: "Phone Number",
        fieldType: "text",
        dataToEnter: "7832732873",
        isOptionalField: false,
      },
      title: {
        label: "Title",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      company: {
        label: "Company",
        fieldType: "text",
        dataToEnter: "Zuddl",
        isOptionalField: false,
      },
      bio: {
        label: "Bio",
        fieldType: "text",
        dataToEnter: "Simple human.",
        isOptionalField: false,
      },
      country: {
        label: "Country",
        fieldType: "text",
        dataToEnter: "India",
        isOptionalField: false,
      },
      "Do you watch sport?": {
        label: "Do you watch sport?",
        fieldType: "dropdown",
        dataToEnter: "Yes",
        isOptionalField: true,
      },
      "Write any cntry you have visited?": {
        label: "Write any cntry you have visited?",
        fieldType: "text",
        dataToEnter: "India",
        isOptionalField: false,
      },
      "Please rate delhi on Scale of 0-10?": {
        label: "Please rate Delhi on Scale of 0-10?",
        fieldType: "number",
        dataToEnter: "8",
        isOptionalField: false,
      },
      "Please select covid vaccine doses you've got?": {
        label: "Please select covid vaccine doses you've got?",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["1st", "2nd"],
        isOptionalField: true,
        isConditionaField: true,
      },
      extraMandatoryField: {
        label: "extraMandatoryField",
        fieldType: "dropdown",
        dataToEnter: "chooseme",
        isOptionalField: false,
        isConditionaField: true,
      },
    };

    this.predefinedFieldsFormDataEditProfile = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      company: {
        label: "Company",
        fieldType: "text",
        dataToEnter: "Zuddl",
        isOptionalField: false,
      },
      "What is your age?": {
        label: "What is your age?",
        fieldType: "dropdown",
        dataToEnter: "18-30",
        isOptionalField: true,
      },
      "What is your fav sports": {
        label: "What is your fav sports",
        fieldType: "multiselect-dropdown",
        dataToEnter: ["football", "hockey"],
        isOptionalField: true,
        isConditionaField: true,
      },
      "are you willing to participate in tournament?": {
        label: "are you willing to participate in tournament?",
        fieldType: "dropdown",
        dataToEnter: "yes",
        isOptionalField: true,
      },
    };

    this.predefinedFieldsFormDataForSalesforcePushCase = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "Prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "AutomationQA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      phoneNumber: {
        label: "Phone Number",
        fieldType: "text",
        dataToEnter: "7832732873",
        isOptionalField: false,
      },
      title: {
        label: "Title",
        fieldType: "text",
        dataToEnter: "QA",
        isOptionalField: false,
      },
      company: {
        label: "Company",
        fieldType: "text",
        dataToEnter: "Zuddl",
        isOptionalField: false,
      },
      bio: {
        label: "Bio",
        fieldType: "text",
        dataToEnter: "Simple human.",
        isOptionalField: false,
      },
      country: {
        label: "Country",
        fieldType: "text",
        dataToEnter: "India",
        isOptionalField: false,
      },
    };

    this.predefinedAndCustomFieldsFormDataForSalesforcePushCase = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "PrateekAttendeeOne",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "AutomationQA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      myField: {
        label: "MyField ",
        fieldType: "text",
        dataToEnter: "custom field",
        isOptionalField: false,
      },

    };

    this.predefinedFieldsFormDataForHubspotPushCase = {
      firstName: {
        label: "First Name",
        fieldType: "text",
        dataToEnter: "Prateek",
        isOptionalField: false,
      },
      lastName: {
        label: "Last Name",
        fieldType: "text",
        dataToEnter: "AutomationQA",
        isOptionalField: false,
      },
      email: {
        label: "Email",
        fieldType: "text",
        dataToEnter: this.userEmailAdd,
        isOptionalField: false,
      },
      phoneNumber: {
        label: "Phone Number",
        fieldType: "text",
        dataToEnter: "7832732873",
        isOptionalField: false,
      },
      company: {
        label: "Company",
        fieldType: "text",
        dataToEnter: "Zuddl",
        isOptionalField: false,
      },
      country: {
        label: "Country",
        fieldType: "text",
        dataToEnter: "India",
        isOptionalField: false,
      },
    };
  }

  get userRegFormDataForDefaultFields() {
    return this._defaultRegFormData;
  }

  get userCustomFieldFormData() {
    return this._customFieldFormData;
  }

  // Happy path conditional fields all options .
  get userConditionalFieldFormData1() {
    return this._conditionalFieldFormData1;
  }

  // Skipping mandatory conditional fields.
  get userConditionalFieldFormData2() {
    return this._conditionalFieldFormData2;
  }
}

export class EventCustomFieldFormObj {
  eventId;
  landingPageId;
  _customFieldObj: object;
  constructor(eventId, landingPageId) {
    this.eventId = eventId;
    this.landingPageId = landingPageId;
  }

  get customFieldObj() {
    this._customFieldObj = [
      {
        fieldName: "phone",
        labelName: "phone",
        fieldType: "SHORTANSWER",
        dataType: "LONG",
        isRequired: false,
        dropdownValues: "",
        eventId: `${this.eventId}`,
        eventLandingPageId: `${this.landingPageId}`,
        isPredefinedField: false,
        customFieldName: null,
        key: "Add New Custom Field",
        error: "",
        orderIdx: 0,
      },
      {
        fieldName: "city",
        labelName: "city",
        fieldType: "MULTISELECT",
        dataType: "STRING",
        isRequired: true,
        dropdownValues: "Delhi,Chennai",
        eventId: `${this.eventId}`,
        eventLandingPageId: `${this.landingPageId}`,
        isPredefinedField: false,
        customFieldName: null,
        key: "Add New Custom Field",
        error: "",
        orderIdx: 1,
      },
      {
        fieldName: "Normal dropdown",
        labelName: "Normaldropdown",
        fieldType: "DROPDOWN",
        dataType: "STRING",
        isRequired: false,
        dropdownValues: "adasd",
        eventId: `${this.eventId}`,
        eventLandingPageId: `${this.landingPageId}`,
        isPredefinedField: false,
        customFieldName: null,
        key: "Add New Custom Field",
        error: "",
        orderIdx: 2,
      },
    ];

    return this._customFieldObj;
  }

  getAnExtraMandatoryCustomFieldObj() {
    return [
      {
        fieldName: "extraMandatoryField",
        labelName: "extraMandatoryField",
        fieldType: "DROPDOWN",
        dataType: "STRING",
        isRequired: true,
        dropdownValues: "chooseme",
        eventId: `${this.eventId}`,
        eventLandingPageId: `${this.landingPageId}`,
        isPredefinedField: false,
        customFieldName: null,
        key: "Add New Custom Field",
        error: "",
        orderIdx: 3,
      },
    ];
  }

  getAnExtraMandatoryCustomFieldObjForPreDefinedFields() {
    return [
      {
        fieldName: "extraMandatoryField",
        labelName: "extraMandatoryField",
        fieldType: "DROPDOWN",
        dataType: "STRING",
        isRequired: true,
        dropdownValues: "chooseme",
        eventId: `${this.eventId}`,
        eventLandingPageId: `${this.landingPageId}`,
        isPredefinedField: false,
        customFieldName: null,
        key: "Add New Custom Field",
        error: "",
        orderIdx: 9,
      },
    ];
  }

  getAnExtraOptionalCustomFieldObj() {
    return [
      {
        fieldName: "extraOptionalField",
        labelName: "extraOptionalField",
        fieldType: "DROPDOWN",
        dataType: "STRING",
        isRequired: false,
        dropdownValues: "chooseme",
        eventId: `${this.eventId}`,
        eventLandingPageId: `${this.landingPageId}`,
        isPredefinedField: false,
        customFieldName: null,
        key: "Add New Custom Field",
        error: "",
        orderIdx: 3,
      },
    ];
  }
}
